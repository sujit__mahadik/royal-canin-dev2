public class ServiceRequestDetails{

    public List<SORequestWrapper> lstSer;
    public ServiceRequestDetails(){
        lstSer = new List<SORequestWrapper>();
    }
    public class SORequestWrapper{
        public string SOSFDCID;
        public string SOName;
        public string sPrcessingStatus;
        public string sIntegtrationStatus;
        public string sObservation;
        public string sSAPOrderNumber;
        
        public SORequestWrapper(){
            SOSFDCID ='';
            SOName ='';
            sPrcessingStatus='';
            sIntegtrationStatus='';
            sObservation='';
            sSAPOrderNumber='';
        }
    }
}