Global class ServiceResponseDet{
    public string returnCode;
    public string message;
    public string sErrorMsg;
    public List<SOWrapper> lstSer;
    public ServiceResponseDet(){
        returnCode = '';
        message = '';
        sErrorMsg = '';
        lstSer = new List<SOWrapper>();
    }
    public class SOWrapper{
        public string SOName;
        public string sPrcessingStatus;
        public string sIntegtrationStatus;
        
        public SOWrapper(){
            SOName ='';
            sPrcessingStatus='';
            sIntegtrationStatus='';
        }
    }
}