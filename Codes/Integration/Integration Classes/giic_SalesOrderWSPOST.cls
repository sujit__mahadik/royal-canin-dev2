@RestResource(urlMapping='/SalesOrderPost/v0.1/*')
global with sharing class giic_SalesOrderWSPOST{
    @HttpPost
    global static ServiceResponseDet  updateSalesOrder() {
        ServiceResponseDet objResponse = new ServiceResponseDet();
        List<ServiceRequestDetails.SORequestWrapper> objProcessDetails = new List<ServiceRequestDetails.SORequestWrapper>();
        RestRequest vReq = RestContext.request;
        system.debug('vReq.params::::::' + vReq.params);
        Savepoint sp = Database.setSavepoint();
        try{
            String vReqBody  = vReq.requestBody.tostring();
            system.debug('POST Request Body1 =  ' + vReqBody);
            vReqBody = vReqBody.substringAfter(':');
            vReqBody = vReqBody.substringBeforeLast('}');
            system.debug('POST Request Body2 =  ' + vReqBody);
            
            objProcessDetails =(List<ServiceRequestDetails.SORequestWrapper>)JSON.deserialize(vReqBody,List<ServiceRequestDetails.SORequestWrapper>.class);
            
            system.debug('objProcessDetails-->>>>' + objProcessDetails);
            List<gii_SalesOrder__c> lstSO = new List<gii_SalesOrder__c>();
            if(objProcessDetails != null && objProcessDetails.size()>0){
                gii_SalesOrder__c objSO;
                for(ServiceRequestDetails.SORequestWrapper objWR : objProcessDetails){
                    objSO = new gii_SalesOrder__c();
                    objSO.giic_ProcessingStatus__c = objWR.sPrcessingStatus;
                    objSO.giic_IntegrationOperation__c = objWR.sIntegtrationStatus;
                    objSO.giic_Observation__c = objWR.sObservation;
                    objSO.giic_SAPOrderNum__c = objWR.sSAPOrderNumber;
                    lstSO.add(objSO);
                }
            }
            if(lstSO != null && lstSO.size()>0){
                insert lstSO;
                system.debug('lstSO--->>>>>' + lstSO);
                objResponse.returnCode = '200';
                objResponse.message = 'SUCCESS';
            }
            
        }catch(Exception excp){
            Database.rollback(sp);
            system.debug('Exception Message ---->>>>' + excp.getMessage());
            objResponse.returnCode = '500';
            objResponse.message = 'Failure';
            objResponse.sErrorMsg = excp.getMessage();
        }
        system.debug('objResponse:::::' + objResponse);
        return objResponse;
    } 
    
}