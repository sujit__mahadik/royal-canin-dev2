/**********************************************************************
Company: Fujitsu America 
Date Created: 06/29/2016
Author: Deepak Saini
Description : This Webservice will fetch data for sales order and sales order line

**********************************************************************/

@RestResource(urlMapping='/SalesOrder_BR/v0.1/*')
global with sharing class giic_SalesOrderWS_BR{
    @HttpGet
    global static ServiceResponseDetails getSODetails(){
        ServiceResponseDetails objServiceResponseDetails = new ServiceResponseDetails();
        ServiceResponseDetails.ParentObjectFieldMapping oParentOFM = new ServiceResponseDetails.ParentObjectFieldMapping();
        List<Map<String, String>> lstErrors = new List<Map<String, String>>();
        Map<String, String> mapOfErr = new Map<String, String>();
        
        /************************** Wrapper Class List Variable *****************************/
        RestRequest req = RestContext.request;
        /************************** Wrapper Class object variable *****************************/
        DynamicMappingResponse objReturnsWrapper = new DynamicMappingResponse();
        string sInClauseIDs = '';
        string sConsumer = '';
        String whereClause = ' WHERE ';
        String condition1 = '';
        String condition2 = '';

        try {  
            system.debug('req.params::::::' + req.params);
            if(req.params != null && req.params.containsKey('Consumer')){
                sConsumer = req.params.get('Consumer');
            }
            if (req.params.get('OrderSFDCId') != null && req.params.get('OrderSFDCId') != ''){
                String strOrderId = req.params.get('OrderSFDCId');
                list<string> listIds = new list<string>();
                listIds.addAll(strOrderId.split(system.label.giic_SplitChar));
                if(listIds != null && listIds.size()>0){
                    for(string str : listIds){
                        if(string.IsEmpty(sInClauseIDs)){
                            sInClauseIDs =  '\'' + str + '\'' ;
                        }
                        else{ 
                            sInClauseIDs =  sInClauseIDs + ',' + '\'' + str + '\'' ;
                        }
                    }
                }
                system.debug('sInClauseIDs:::::' + sInClauseIDs);
                condition1 =  ' Id in (' +sInClauseIDs + ')' ;
            }
            
            if (req.params.get('UserId') != null && req.params.get('UserId') != '') {
                condition2 = ' OwnerId = \'' + req.params.get('UserId')  + '\'';  
            }
            
            if(string.isNotEmpty(condition1) && string.isEmpty(condition2)){
                whereClause = whereClause + condition1;
            }
            if(string.isEmpty(condition1) && string.isNotEmpty(condition2)){
                whereClause = whereClause + condition2;
            }
            if(string.isNotEmpty(condition1) && string.isNotEmpty(condition2)){
                whereClause = whereClause + condition1 + ' OR ' + condition2;
            }
            system.debug('whereClause:::::' + whereClause); 
            whereClause = whereClause + ' Limit 20' ;
            system.debug('OrderSFDCId is Empty:::::' + string.isEmpty(req.params.get('OrderSFDCId'))); 
            system.debug('UserId is Empty:::::' + string.isEmpty(req.params.get('UserId'))); 
            
            if (string.isNotEmpty(req.params.get('OrderSFDCId')) || string.isNotEmpty(req.params.get('UserId'))) {
                system.debug('In If Block');
                // fetching the response
                objReturnsWrapper = GloviaAcceleratorUtility.createSalesOrderResponse('SalesOrder_BR', 'GET', whereClause, sConsumer);  
    
                IF(objReturnsWrapper.objectFieldMaps != null && objReturnsWrapper.objectFieldMaps.size()>0){
                    // giic_Success200 = 200
                    objServiceResponseDetails = returnServiceResponseDetails(null, System.Label.giic_Success200, 'SUCCESS', objReturnsWrapper.objectFieldMaps);
                }
                else{
                    // giic_Error500 = 500
                    // giic_Error501 = 501
                    mapOfErr = GloviaAcceleratorUtility.returnsErrorMsgs(System.Label.giic_Error501,'','Required Parameters Missing');
                    objServiceResponseDetails = returnServiceResponseDetails(mapOfErr, System.Label.giic_Error500, 'FAILURE', null);
                }
            }
            else {
                system.debug('In else Block');
                mapOfErr = GloviaAcceleratorUtility.returnsErrorMsgs(System.Label.giic_Error501,'','Required Parameters Missing');
                objServiceResponseDetails = returnServiceResponseDetails(mapOfErr, System.Label.giic_Error500, 'FAILURE', null);
            }
        } catch(Exception ex) {
            mapOfErr = GloviaAcceleratorUtility.returnsErrorMsgs('','',ex.getMessage());
            objServiceResponseDetails = returnServiceResponseDetails(mapOfErr, System.Label.giic_Error500, 'FAILURE', null);
        }
        return objServiceResponseDetails;
    
    }
    
    private static ServiceResponseDetails returnServiceResponseDetails(Map<String, String> mapOfErr, String retCode, String message, List<DynamicMappingResponse.ObjectFieldMapping> objFieldMaps){
        List<Map<String, String>> lstOfErrors = new List<Map<String, String>>();
        ServiceResponseDetails objSRD = new ServiceResponseDetails();
        ServiceResponseDetails.ParentObjectFieldMapping objParentOFM = new ServiceResponseDetails.ParentObjectFieldMapping();
        if(mapOfErr != null && mapOfErr.size() > 0){
            lstOfErrors.add(mapOfErr);
        }
        objParentOFM.errors = lstOfErrors;
        objParentOFM.returnCode = retCode;
        objParentOFM.message = message;
        objSRD.serviceResponse = objParentOFM;
        if(objFieldMaps != null && objFieldMaps.size() > 0 ){
            objSRD.orderDetails = objFieldMaps;
        }
        return objSRD;
    }
    /**************************************************
    Method  : createSalesOrder
    Purpose : This method will Create sales order and sales order line
    *****************************************************/
    @HttpPost
    global static ServiceResponseDetails createSalesOrder() {
        string sConsumer = '';
        RestRequest vReq = RestContext.request;
        system.debug('vReq.params::::::' + vReq);
        system.debug('vReq.params::::::' + vReq.params);
        if(vReq.params != null && vReq.params.containsKey('Consumer')){
            sConsumer = vReq.params.get('Consumer');
        }
        system.debug('sConsumer::::::' + sConsumer);
        // RestRequest vReq = RestContext.request;
        String vReqBody  = vReq.requestBody.tostring();
        system.debug('Post Request Body =  ' + vReqBody);
        ServiceResponseDetails objResponse = new ServiceResponseDetails();
        objResponse = giic_SalesOrderWS_BR_Helper.processObj(vReqBody, 'SalesOrder_BR', 'POST', true, sConsumer); 
        system.debug('objResponse:::::' + objResponse);
        
        return objResponse;
    }
    
    /**************************************************
    Method  : updateSalesOrder
    Purpose : This method will update sales order and sales order line
    *****************************************************/
    
    @HttpPatch
    global static ServiceResponseDetails updateSalesOrder() {
        string sConsumer = '';
        RestRequest vReq = RestContext.request;
        system.debug('vReq.params::::::' + vReq.params);
        if(vReq.params != null && vReq.params.containsKey('Consumer')){
            sConsumer = vReq.params.get('Consumer');
        }
        system.debug('sConsumer::::::' + sConsumer);
        String vReqBody  = vReq.requestBody.tostring();
        system.debug('Patch Request Body =  ' + vReqBody);
        ServiceResponseDetails objResponse = new ServiceResponseDetails();
        objResponse = giic_SalesOrderWS_BR_Helper.processObj(vReqBody, 'SalesOrder_BR', 'PATCH', true, sConsumer); 
        if(objResponse.serviceResponse.returnCode == System.Label.giic_Success200){
            objResponse = processResponse(objResponse);
        }
        
        system.debug('objResponse:::::' + objResponse);
        
        return objResponse;
    }
    
    
    public static ServiceResponseDetails processResponse(ServiceResponseDetails objResponse){
        if(objResponse.orderDetails != null && objResponse.orderDetails.size()>0  && objResponse.serviceResponse.returnCode == System.Label.giic_Success200){
            for(DynamicMappingResponse.ObjectFieldMapping  oOFM : objResponse.orderDetails){
                // SO_RetOrderName  SO_RetOrderSFDCId
                // giic_SO_RetOrderName = SO_RetOrderName
                // giic_SO_RetOrderSFDCId = SO_RetOrderSFDCId
                if( (oOFM.fieldMaps.containsKey(System.Label.giic_SO_RetOrderName) && string.isNotEmpty(oOFM.fieldMaps.get(System.Label.giic_SO_RetOrderName))) || (oOFM.fieldMaps.containsKey(System.Label.giic_SO_RetOrderSFDCId) && string.isNotEmpty(oOFM.fieldMaps.get(System.Label.giic_SO_RetOrderSFDCId))) ){
                    if(objResponse.serviceResponse.returnCode == System.Label.giic_Success200){
                        objResponse.serviceResponse.returnCode = System.Label.giic_Success205;
                    }
                }
            }
        }
        return objResponse;
    }
    
}