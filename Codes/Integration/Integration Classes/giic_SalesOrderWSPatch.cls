@RestResource(urlMapping='/SalesOrderPatch/v0.1/*')
global with sharing class giic_SalesOrderWSPatch{
    @HttpPatch
    global static ServiceResponseDet  updateSalesOrder() {
        List<string> lstErrors = new List<string>();
        ServiceResponseDet objResponse = new ServiceResponseDet();
        List<ServiceRequestDetails.SORequestWrapper> objProcessDetails = new List<ServiceRequestDetails.SORequestWrapper>();
        RestRequest vReq = RestContext.request;
        system.debug('vReq.params::::::' + vReq.params);
        Savepoint sp = Database.setSavepoint();
        try{
            String vReqBody  = vReq.requestBody.tostring();
            system.debug('POST Request Body1 =  ' + vReqBody);
            vReqBody = vReqBody.substringAfter(':');
            vReqBody = vReqBody.substringBeforeLast('}');
            system.debug('POST Request Body2 =  ' + vReqBody);
            
            objProcessDetails =(List<ServiceRequestDetails.SORequestWrapper>)JSON.deserialize(vReqBody,List<ServiceRequestDetails.SORequestWrapper>.class);
            
            system.debug('objProcessDetails-->>>>' + objProcessDetails);
            List<gii_SalesOrder__c> lstSO = new List<gii_SalesOrder__c>();
            if(objProcessDetails != null && objProcessDetails.size()>0){
                gii_SalesOrder__c objSO;
                for(ServiceRequestDetails.SORequestWrapper objWR : objProcessDetails){
                    objSO = new gii_SalesOrder__c();
                    objSO.giic_ProcessingStatus__c = objWR.sPrcessingStatus;
                    objSO.giic_IntegrationOperation__c = objWR.sIntegtrationStatus;
                    objSO.giic_Observation__c = objWR.sObservation;
                    objSO.giic_SAPOrderNum__c = objWR.sSAPOrderNumber;
                    
                    objSO.Id = objWR.SOSFDCID;
                    
                    if(string.isEmpty(objWR.SOSFDCID)){
                        lstErrors.add('SFDC ID Missing');
                    }
                    
                    /*
                    if(string.isEmpty(objWR.sSAPOrderNumber)){
                        lstErrors.add('SFDC SAP Order Number missing ');
                    }
                    */
                    lstSO.add(objSO);
                }
            }
            if(lstSO != null && lstSO.size()>0 && lstErrors != null && lstErrors.size()<1){
                Update lstSO;
                system.debug('lstSO--->>>>>' + lstSO);
                objResponse.returnCode = '200';
                objResponse.message = 'SUCCESS';
            }
            else{
                objResponse.returnCode = '500';
                objResponse.message = 'Failure';
                objResponse.sErrorMsg = 'Their is now row to update ';
                if(lstErrors != null && lstErrors.size()>0){
                    objResponse.sErrorMsg = string.valueOf(lstErrors);
                }
            }
            
        }catch(Exception excp){
            Database.rollback(sp);
            system.debug('Exception Message ---->>>>' + excp.getMessage());
            objResponse.returnCode = '500';
            objResponse.message = 'Failure';
            objResponse.sErrorMsg = excp.getMessage();
        }
        system.debug('objResponse:::::' + objResponse);
        return objResponse;
    }
    
    
    
    
    
    
}