List<CronTrigger> listCronTrigger = [select Id, CronExpression, EndTime, NextFireTime, OwnerId,
        PreviousFireTime, StartTime, State, TimesTriggered, TimeZoneSidKey from CronTrigger 
        where State = 'Waiting' or State='Running'];
        
System.debug('No of jobs: '+listCronTrigger.size());

If (listCronTrigger.size() > 0)
{
    for (Integer i = 0; i < listCronTrigger.size(); i++)
    { 
        System.abortJob(listCronTrigger[i].Id);
        System.debug('Job details ::'+String.valueOf(listCronTrigger[i]));
    }
}