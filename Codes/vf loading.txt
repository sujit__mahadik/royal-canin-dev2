<apex:page standardController="gii__SalesOrder__c" extensions="giic_SalesOrderController" sidebar="false" action="{!applyRecycleFee}">
    <apex:form >
        <apex:inputHidden value="{!gii__SalesOrder__c.gii__TaxLocation__c}"/>
        <apex:outputPanel rendered="{!hasMessages == false}">
            <center><apex:image value="/img/waiting_dots.gif" width="156" height="25"></apex:image></center> 
        </apex:outputPanel>
        <apex:outputPanel layout="block" id="msg_block" rendered="{!hasMessages == true}">         
            <apex:pageMessages ></apex:pageMessages>        
        </apex:outputPanel> 
        <center><apex:commandButton id="continue" style="btn" value="Cancel" action="{!cancel}" /></center> 
    </apex:form>
</apex:page>