/*
 * Date:             - Developer, Company                          - description
 * 2015-08-05        - Stevie Yakkel, Acumen Solutions             - Created
 */
trigger OpportunityTrigger on Opportunity (before insert) {

    if(Trigger.isBefore) {
        if(Trigger.isInsert) {
            OpportunityTriggerHelper.assignPricebook2ID(Trigger.new);
            OpportunityTriggerHelper.changeOpportunityNameOnInsert(Trigger.new);
        }
        if(Trigger.isUpdate) {
            OpportunityTriggerHelper.assignPricebook2ID(Trigger.new);
        }
    } 
}