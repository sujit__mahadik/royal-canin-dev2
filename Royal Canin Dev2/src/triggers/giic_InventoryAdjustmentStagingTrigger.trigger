/* * *******************************************************
* Author               Date                 Remarks
*                     24-September-2018 
* This trigger will create/update inventory Adjustment
***********************************************************/ 

trigger giic_InventoryAdjustmentStagingTrigger on gii__InventoryAdjustmentStaging__c (after insert,after update) {
    
    if(System.isFuture() || System.isBatch() || System.isQueueable()) return;
    
    Long dt1Long = system.now().getTime();    
    if(giic_RecursiveTriggerHandler.handleRecursionInventoryAdjStagingTrg())
    {      
        giic_Disable_Triggers__c dt = giic_Disable_Triggers__c.getOrgDefaults();   
        
        if(dt!=null && !dt.giic_DisableTriggerInventoryConversion__c)
        {    
            
            if(Trigger.isInsert && Trigger.isAfter)
            { 
                giic_InventoryAdjustmentStagingTrgHelper.OnAfterInsert(Trigger.New);
            }    
            
            if(Trigger.isUpdate && Trigger.isAfter)
            { 
                giic_InventoryAdjustmentStagingTrgHelper.OnAfterInsert(Trigger.New);
            }        
        }
        else
        {   // giic_IAS_TriggerDisabledMsg = giic_InventoryAdjustmentStagingTrigger Disabled in custom Settings based on giic_DisableTriggerInventoryConversion__c
            System.debug(system.label.giic_IAS_TriggerDisabledMsg);
        } 
    }
    
    Long dt2Long = system.now().getTime();
    Long milliseconds = dt2Long - dt1Long;    
    system.debug('milliseconds------->>>>>>>> ' + milliseconds);

}