/*************************************************************

This trigger will run on Insert event and will update
product reference 
Created By              Shivdeep Gupta

Update History
By                      Reason

**************************************************************/

trigger giic_ProductUnitofMeasureConversionTrigger on gii__ProductUnitofMeasureConversion__c(After Insert,after Update) 
{  
    giic_Disable_Triggers__c dt = giic_Disable_Triggers__c.getOrgDefaults();  
    if(dt!=null && !dt.giic_DisableTriggerProductUOMConversion__c){
            
        // will save product reference ids
        set<id> productids = new set<id>(); 
        
        //iteration for all new records
        for(gii__ProductUnitofMeasureConversion__c rec : trigger.new)
        {     
            
            // condition if trigger is fired on update event
            if(trigger.isUpdate)
            {    
                // code to get old record values
                gii__ProductUnitofMeasureConversion__c oldvalue = trigger.oldMap.get(rec.id);
                
                // check if Reverse Conversion Factor is changed
                if( rec.gii__ReverseConversionFactor__c != oldvalue.gii__ReverseConversionFactor__c)
                {
                    productids.add(rec.gii__Product__c);                              
                }
                // check if Reverse Conversion Factor is changed
                else if( rec.gii__ConversionFactor__c != oldvalue.gii__ConversionFactor__c)
                {
                    productids.add(rec.gii__Product__c);                
                }
               
                // productids.add(rec.gii__Product__c); 
            }
            // condition if trigger is fired on update event
            else if(trigger.isInsert)
            {
                productids.add(rec.gii__Product__c);            
            }
        }
        
        if(productids != null && productids.size() > 0 ){
            giic_ProductUnitofMeasureConversionHlpr objPUMCH = new giic_ProductUnitofMeasureConversionHlpr();
            objPUMCH.processProductUnitOfMeasureRecords(productids);
        }
    }
}