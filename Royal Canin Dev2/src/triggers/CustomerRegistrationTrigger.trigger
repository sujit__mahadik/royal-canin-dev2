trigger CustomerRegistrationTrigger on Customer_Registration__c (before insert, before update, after update) {
    if(trigger.isBefore){
        if(trigger.isInsert){
            //CustomerRegistrationTriggerHelper.pullShippingAddressFromAccountForCFP(trigger.new);
            CustomerRegistrationTriggerHelper.createCustomEncryptionKey(trigger.new);
            CustomerRegistrationTriggerHelper.formatBillToAndClinicNumbers(trigger.new, null);

            //CustomerRegistrationTriggerHelper.setRegionandDistrict(trigger.new, null);
        } else if(trigger.isUpdate){
            CustomerRegistrationTriggerHelper.formatBillToAndClinicNumbers(trigger.new, null);
            CustomerRegistrationTriggerHelper.pullShippingAddressFromAccountForCFP(trigger.new);
            CustomerRegistrationTriggerHelper.verifyEmployee(trigger.new, trigger.oldMap);
            CustomerRegistrationTriggerHelper.checkForAddressChanges(trigger.new, trigger.oldMap);
            CustomerRegistrationTriggerHelper.requireRejectionComment(trigger.new, trigger.oldMap);
            CustomerRegistrationTriggerHelper.setRegionandDistrict(trigger.new, trigger.oldMap);
            CustomerRegistrationTriggerHelper.updatePaymentMethodOnAdditionalSellTo(trigger.new,trigger.oldMap);
            CustomerRegistrationTriggerHelper.checkDatesInFuture(trigger.new, trigger.oldMap);
            CustomerRegistrationTriggerHelper.setClinicCustNumberforBanfield(trigger.new);
            //CustomerRegistrationTriggerHelper.setTimeAtApproved(trigger.new, trigger.oldMap);
        }
    } else {
        if(trigger.isUpdate){
            CustomerRegistrationTriggerHelper.checkForSubmission(trigger.new, trigger.oldMap);
            CustomerRegistrationTriggerHelper.convertRegistration(trigger.new, trigger.oldMap);
            CustomerRegistrationTriggerHelper.sendRejectedEmails(trigger.new, trigger.oldMap);
        }
    }
}