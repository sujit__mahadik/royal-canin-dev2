/* * *******************************************************
* Author               Date                 Remarks
*                     
* This trigger will check for budget for user who is create PR
***********************************************************/ 

trigger giic_BudgetCheck on gii__PurchaseRequisition__c (Before update) {
    
    if(giic_RecursiveTriggerHandler.handleRecursionWSALTrg())
    {       
        giic_Disable_Triggers__c dt = giic_Disable_Triggers__c.getOrgDefaults();   
        giic_ProductRequisitionTriggerHelper  handler = new giic_ProductRequisitionTriggerHelper();

        if(dt!=null && !dt.giic_DisableProductRequisition__c)
        {    

            if(Trigger.isUpdate && Trigger.isAfter)
            { 
                //handler.OnAfterUpdate(Trigger.New);
            }  
                
        }
        else
        { 
            System.debug('giic_WHSAdviceLineTrigger Disabled in custom Settings based on giic_DisableTriggerWHShipmentAdviceLine__c');
        } 
    }
}