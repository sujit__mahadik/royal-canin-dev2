trigger giic_CaseTrigger on Case (after update) {
    
    if(Trigger.New.size()>1) return;
    if(giic_RecursiveTriggerHandler.run())
    {
       
       giic_CaseTriggerHandler handler = new giic_CaseTriggerHandler();
       
       if(Trigger.isUpdate && Trigger.isAfter)
            { 
                handler.OnAfterUpdate(Trigger.New);       
            }  
        
    }
}