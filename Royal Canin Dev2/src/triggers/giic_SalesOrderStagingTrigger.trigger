/* * *******************************************************
* Author               Date                 Remarks
* Abhishek Tripathi     21-November-2018 
* This trigger will convert Sales Order Staging and line records into Actual records.
***********************************************************/ 
trigger giic_SalesOrderStagingTrigger on gii__SalesOrderStaging__c (after insert,  after update) {
    if(System.isFuture() || System.isBatch() || System.isQueueable()) return;
    if(giic_RecursiveTriggerHandler.run())
    {      
        giic_Disable_Triggers__c dt = giic_Disable_Triggers__c.getOrgDefaults();   
        giic_SalesOrderStagingTriggerHelper handler = new giic_SalesOrderStagingTriggerHelper();
        if(dt!=null && !dt.giic_Disable_Trigger_Sales_Order_Staging__c)
        {    
            if(Trigger.isInsert && Trigger.isAfter)
            {   
               handler.OnAfterInsert(Trigger.New);
            }
            else if(Trigger.isUpdate && Trigger.isAfter)
            { 
              handler.OnAfterUpdate(Trigger.old, Trigger.New, Trigger.newMap, Trigger.oldMap);       
            }            
        }
        else
        { 
            System.debug('giic_SalesOrderStagingTrigger Disabled in custom Settings based on giic_Disable_Triggers_SOLStaging__c');
        } 
    }
}