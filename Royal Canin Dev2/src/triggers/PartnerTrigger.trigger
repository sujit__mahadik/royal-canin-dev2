trigger PartnerTrigger on Partner__c (after insert) {
	if(Trigger.isInsert) {
		if (Trigger.isAfter) {
			PartnerTriggerHelper.createReversePartner(Trigger.new);
		}
	}
}