trigger SalesSummaryTrigger on Sales_Summary__c (before insert, before update) {
    if (Trigger.isBefore && Trigger.isInsert){
        SalesSummaryTriggerHelper.processMaxDate(trigger.new);
    }
    if(Trigger.isBefore && Trigger.isUpdate) {
        SalesSummaryTriggerHelper.processMaxDate(trigger.new);
    }
}