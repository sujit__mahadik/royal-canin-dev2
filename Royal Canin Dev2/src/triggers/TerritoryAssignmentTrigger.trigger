trigger TerritoryAssignmentTrigger on TerritoryAssignment__c (before update) {

	if(Trigger.isBefore && Trigger.isUpdate){
		TerritoryAssignmentTriggerHelper.updateIsSyncNeeded(Trigger.oldMap, Trigger.New);	
	}
}