/************************************************************************************
Version : 1.0
Created Date : 23 Oct 2018
Function : This trigger will check for the Warehouse Group of Account reference and update the Distance json on Account object based on the shipping address
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/
trigger giic_AccountAddTrigger on gii__AccountAdd__c (after update) {
    /************************** Common Utility method to update the Account JSON *************************************/
    giic_CommonUtility.updateWarehouseDistanceOnAccount();
}