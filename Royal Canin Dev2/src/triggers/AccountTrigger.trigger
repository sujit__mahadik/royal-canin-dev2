/*
 * Date:             - Developer, Company                          - Description
 * 2015-04-28        - Mayank Srivastava, Acumen Solutions         - Created
 * 2015-06-18        - Joe Wrabel, Acumen Solutions                - Edited
 * 2015-08-05        - Stevie Yakkel, Acumen Solutions             - Edited
 * 2016-01-08        - Maruthi(Siva) Gotety, Royal Canin           - Edited
 * 2018-04-05        - Bethi Reddy, Royal Canin                    - Commented "checkIntegrationUserAddressEdits" to allow accounts sync job to make updates to address on accounts. This will be uncommented when salesforce is maintained as SOR for addresses  
 * 2018-09-12        - Bethi Reddy, Royal Canin                    - Edited
 * 2018-10-02        - Bethi Reddy, Royal Canin                    - Edited 
 * 2018-10-23        - Pradeep Kumar, Fujitsu                      - Updated the calling location of AccountTriggerHelper.getDistanceOfWarehouses 
*/

trigger AccountTrigger on Account (before insert, before update, after insert, before delete, after update) {
    Global_Parameters__c gp = Global_Parameters__c.getValues('isDeploy');
    String isDeploy;
    if(gp != null) {
        isDeploy = gp.value__c.toLowerCase();
    } else {
        isDeploy = 'false';
    }
    if(Trigger.isBefore) {
        if(Trigger.isInsert) {
            System.debug('----Insert start----'+Trigger.new);
            for(Account accccc: Trigger.new) {
                System.debug('accccc------->'+accccc.Customer_Registration__c);
            }
            AccountTriggerHelper.setUpTerritoryData(Trigger.new, Trigger.oldMap);
            AccountTriggerHelper.priceBookCodeLink(Trigger.new);
            if(isDeploy.equals('false')){
                AccountTriggerHelper.dataDotComReFormat(Trigger.new);
                AccountTriggerHelper.passDownKeyMessage(Trigger.new, null);
            }
            AccountTriggerHelper.formatPhoneFax(Trigger.new, null);
            for(Account accccc : Trigger.new) {
                System.debug('acccccttttt========>'+accccc.Customer_Registration__c);
            }
        } else if (Trigger.isUpdate) {
            System.debug('-------Call update Trigger-----');
            //AccountTriggerHelper.populateAddressChanges(Trigger.new,Trigger.Old,Trigger.oldMap); (uncomment when we deploy Address Approval Process)
            AccountTriggerHelper.setUpTerritoryData(Trigger.new, Trigger.oldMap);
            if(isDeploy.equals('false')){
               // AccountTriggerHelper.checkIntegrationUserAddressEdits(Trigger.new, Trigger.oldMap); (uncomment when we do salesforce as a SOR for address)
                AccountTriggerHelper.checkForAddressChanges(Trigger.new, Trigger.oldMap);
            }   
            AccountTriggerHelper.isSyncWithNavNeeded(Trigger.new,Trigger.oldMap);
            if(isDeploy.equals('false')) {
                AccountTriggerHelper.dataDotComReFormat(Trigger.new);
            } 
            AccountTriggerHelper.formatPhoneFax(Trigger.new, Trigger.oldMap);
            AccountTriggerHelper.updateCustomerStatus(Trigger.new, Trigger.oldMap);
            AccountTriggerHelper.priceBookCodeLink(Trigger.new);
            if(isDeploy.equals('false')) {
                AccountTriggerHelper.passDownKeyMessage(Trigger.new, Trigger.oldMap);
            }
            AccountTriggerHelper.getDistanceOfWarehouses(Trigger.newMap, Trigger.oldMap); // Update the Distance mapping on account 
        }
    } else if (Trigger.isAfter) {
        if(Trigger.isInsert) {
            System.debug('--Enter into---this---');
          //  AccountTriggerHelper.UpdateAccountReference(Trigger.newMap);
            if(isDeploy.equals('false')){
                AccountTriggerHelper.validateAddressOnInsert(Trigger.new);
            }
            
        }
        
        /*****Uncomment the below code when we deploy Address Approval Process*****/
       /* if(Trigger.isUpdate) {
            AccountTriggerHelper.addressChangeSendForApprovalRejected(Trigger.new,Trigger.Old,Trigger.oldMap);
        } */
         if(Trigger.isUpdate) {
            //AccountTriggerHelper.getDistanceOfWarehouses(Trigger.newMap, Trigger.oldMap); commented because it should be called from the before update trigger
            AccountTriggerHelper.UpdateAccountReference(Trigger.newMap);
        } 
         
    }   
}