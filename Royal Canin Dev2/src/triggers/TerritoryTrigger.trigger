trigger TerritoryTrigger on Territory__c (before delete, after update, before insert, before update) {

	TerritoryTriggerHelper helper = new TerritoryTriggerHelper();
	//delete zip codes associoated to this territory from territory assignments object
	if (Trigger.isBefore && Trigger.isDelete) {
		helper.deleteTerritoryAssociation(Trigger.oldMap);
	}

	if (Trigger.isAfter && Trigger.isUpdate && !system.isBatch())
	{
		helper.updateTerritoryOwner(Trigger.newMap , Trigger.oldMap);
	}
	
	if (Trigger.isBefore && Trigger.isInsert)
	{
		helper.setTerritoryOwner(Trigger.New);
	}
	
	if(Trigger.isBefore && Trigger.isUpdate)
	{
	//	helper.updateHiddenOwner(Trigger.New);
		helper.updateSyncNeeded(Trigger.new, Trigger.oldMap);
	}
}