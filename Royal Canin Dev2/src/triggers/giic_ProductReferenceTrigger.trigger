/************************************************************************************
Version : 1.0
Created Date : 23 Oct 2018
Function : Trigger on product reference Object
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/

trigger giic_ProductReferenceTrigger on gii__Product2Add__c (before insert, before update) {
    giic_Disable_Triggers__c dt = giic_Disable_Triggers__c.getOrgDefaults(); 
    if(dt!=null && !dt.giic_DisableTriggerProductReference__c){
        giic_ProductReferenceTriggerHelper objPRTH = new giic_ProductReferenceTriggerHelper();
        if(Trigger.IsBefore && (Trigger.isInsert || Trigger.isUpdate)){
            // mapping fields from product to product reference using metadata
            objPRTH.processFieldMap(trigger.new);
        }
    }
}