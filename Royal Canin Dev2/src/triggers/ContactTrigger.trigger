trigger ContactTrigger on Contact (before insert, before update, after insert) {
    Global_Parameters__c gp = Global_Parameters__c.getValues('isDeploy');
    String isDeploy;
    if(gp != null) {
        isDeploy = gp.value__c.toLowerCase();
    } else {
        isDeploy = 'false';
    }
    
    if(Trigger.isBefore) {
        if(Trigger.isInsert) {
          //  ContactTriggerHelper.preventDuplicateContact(Trigger.new);
            ContactTriggerHelper.processConsumerAccount(Trigger.new);
            if(isDeploy.equals('false')) {
                //System.debug('TEST');
                ContactTriggerHelper.dataDotComReFormat(Trigger.new);
            }
            ContactTriggerHelper.formatPhoneFax(Trigger.new, null);
        } else if (Trigger.isUpdate) {
           
             if(isDeploy.equals('false')) {
                ContactTriggerHelper.checkForAddressChanges(Trigger.new, Trigger.oldMap);
            }  
                 
            ContactTriggerHelper.preventConsumerAccountLkpUpdate(Trigger.new, Trigger.oldMap);
            if(isDeploy.equals('false')) {
                ContactTriggerHelper.dataDotComReFormat(Trigger.new);
            }
            ContactTriggerHelper.formatPhoneFax(Trigger.new, Trigger.oldMap);
            
        }   
    } else if (Trigger.isAfter) {
        if(Trigger.isInsert) {
      
            if(isDeploy.equals('false')) {
                ContactTriggerHelper.validateAddressOnInsert(Trigger.new);
            }           
              
              ContactTriggerHelper.setConsumerContactLkp(Trigger.new);
          //    ContactTriggerHelper.processLoadedConsumerRelationship(Trigger.new);
              ContactTriggerHelper.setContactToMultipleAccounts(Trigger.new);
        }
    }
}