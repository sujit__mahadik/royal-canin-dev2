/* * *******************************************************
* Author               Date                 Remarks
*                     18-Dec-2018 
* This trigger will update Sales order line records from Warehouse Shipment Advice Lines Staging 
create shipment and invoice for fulfilment
***********************************************************/ 

trigger giic_WHSAdviceTrigger on gii__WarehouseShipmentAdviceStaging__c (before insert, after update) {
    
    if(System.isFuture() || System.isBatch() || System.isQueueable()) return;
    system.debug('Check 1------');
    if(giic_RecursiveTriggerHandler.handleRecursionWSALTrg())
    {       
         system.debug('giic_RecursiveTriggerHandler------');
        giic_Disable_Triggers__c dt = giic_Disable_Triggers__c.getOrgDefaults();   
        giic_WHSAdviceTriggerHelper handler = new giic_WHSAdviceTriggerHelper();
        system.debug('Check 1------' + dt);
        if(dt!=null && !dt.giic_DisableTriggerWHShipmentAdvice__c)
        {  
            if(Trigger.isInsert && Trigger.isBefore)
            { 
                handler.OnBeforeInsert(Trigger.New);
            }
            if(Trigger.isUpdate && Trigger.isAfter)
            { 
                handler.OnAfterUpdate(Trigger.New);
            }  
        }
        else
        { 
            System.debug('giic_WHSAdviceTrigger Disabled in custom Settings based on giic_DisableTriggerWHShipmentAdvice__c');
        } 
    }
}