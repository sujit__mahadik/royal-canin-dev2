/*
 * Date:       		 - Developer, Company					       - Description
 * 2015-05-04        - Mayank Srivastava, Acumen Solutions         - Created
 */
trigger FiscalYearTrigger on Fiscal_Year__c (before insert, before update) {
	FiscalYearTriggerHelper.checkActiveFYs(Trigger.new,Trigger.newMap);
	if(Trigger.isInsert){
		FiscalYearTriggerHelper.setActivationDefaultDate(Trigger.new);
	}
}