/********************************************************
* CLASS NAME   ::: giic_LoyaltyPointsTrackerBatch
* PURPOSE      ::: Writtern to calculate Loyalty Poitnts
* CREATED DATE ::: 19 Nov, 2018
* AUTHOR       ::: Devanshu Sharma
* ********************************************************/

global class giic_LoyaltyPointsTrackerBatch implements Database.Batchable<sObject>, Database.Stateful,Schedulable {
    
    /*
    * Method name : start
    * Description : Query all loyalty point record for the account
    * Return Type : Database.QueryLocator
    * Parameter : Database.BatchableContext BC
    */
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query;            
        query = System.label.giic_QueryLoyaltyBatch;
        query+= ' where createddate= '+System.label.giic_FilterLoyaltyBatch+' and gii__Invoice__r.gii__Account__r.giic_SetupLoyaltyMembershipAccount__c = true and id NOT IN (SELECT giic_InvoiceDetail__c from giic_LoyaltyPointTracker__c where CreatedDate = '+System.label.giic_FilterLoyaltyBatchInvoice+') order by gii__Invoice__r.gii__Account__c';
        System.debug('Loyalty Query***'+query);
        return Database.getQueryLocator(query);
    }
    /*
    * Method name : execute
    * Description : Process the Loyalty Point and convert into the Reward point
    * Return Type : nill
    * Parameter : Database.BatchableContext BC, List<gii__OrderInvoiceDetail__c> lstOrderInvoiceDetails
    */
    global void execute(Database.BatchableContext BC,  List<gii__OrderInvoiceDetail__c> lstOrderInvoiceDetails) {
        
        Savepoint sp = Database.setSavepoint();
        Set<id> setAccIdforErrors = new Set<id>();
        try
        {
            for(gii__OrderInvoiceDetail__c obj: lstOrderInvoiceDetails)
            {
                setAccIdforErrors.add(obj.gii__Invoice__r.gii__Account__c);
            }            
            
            gii__GloviaSystemSetting__c GloviaSystemSettingObj = gii__GloviaSystemSetting__c.getOrgDefaults();
            if(GloviaSystemSettingObj.giic_AllowLoyaltyPointInvoiceLine__c)
            {
                Set<String> paymentTypeNotAllowforLoyaltyPoint = new Set<String>();                
                Map<Id,Decimal> map_Account_Amount = new Map<Id,Decimal>();    
                Map<id,id> mapOfInvoiceAndCustomerId = new Map<id,id>();
                Map<id,gii__OrderInvoiceDetail__c> mapInvoiceDetailsRecord = new Map<id,gii__OrderInvoiceDetail__c>();
                Map<Id,Account> mapAccount = new Map<id,Account>();
                Set<Id> setOfCustomerId = new Set<Id>();
                if(GloviaSystemSettingObj.giic_PaymentTypeNotAllowedForLoyaltyPts__c !=null && GloviaSystemSettingObj.giic_PaymentTypeNotAllowedForLoyaltyPts__c != '')
                {
                     paymentTypeNotAllowforLoyaltyPoint.addall(GloviaSystemSettingObj.giic_PaymentTypeNotAllowedForLoyaltyPts__c.split(';'));
                } 
               
                if(lstOrderInvoiceDetails.size()>0)
                {
                    for(gii__OrderInvoiceDetail__c tempObj : lstOrderInvoiceDetails)
                    {   
                        if(tempObj.gii__Invoice__r.gii__Account__r.giic_TypeOfMembership__c != null && paymentTypeNotAllowforLoyaltyPoint.size()>0 && (!(paymentTypeNotAllowforLoyaltyPoint.contains(tempObj.gii__Invoice__r.gii__SalesOrder__r.gii__PaymentMethod__c))))
                        {
                            if(mapInvoiceDetailsRecord.get(tempObj.id)== null)
                            {
                                mapInvoiceDetailsRecord.put(tempObj.id,null);
                            }
                            mapInvoiceDetailsRecord.put(tempObj.id,tempObj);
                            
                            if(mapAccount.get(tempObj.gii__Invoice__r.gii__Account__c)== null)
                            {
                                mapAccount.put(tempObj.gii__Invoice__r.gii__Account__c,null);
                            }
                            mapAccount.put(tempObj.gii__Invoice__r.gii__Account__c,tempObj.gii__Invoice__r.gii__Account__r);
                            
                            mapOfInvoiceAndCustomerId.put(tempObj.id,tempObj.gii__Invoice__r.gii__Account__c);
                            setOfCustomerId.add(tempObj.gii__Invoice__r.gii__Account__c);
                        }
                        else if(tempObj.gii__Invoice__r.gii__Account__r.giic_TypeOfMembership__c != null && paymentTypeNotAllowforLoyaltyPoint.size()==0 )
                        {
                            
                            if(mapInvoiceDetailsRecord.get(tempObj.id)== null)
                            {
                                mapInvoiceDetailsRecord.put(tempObj.id,null);
                            }
                            mapInvoiceDetailsRecord.put(tempObj.id,tempObj);
                            
                            if(mapAccount.get(tempObj.gii__Invoice__r.gii__Account__c)== null)
                            {
                                mapAccount.put(tempObj.gii__Invoice__r.gii__Account__c,null);
                            }
                            mapAccount.put(tempObj.gii__Invoice__r.gii__Account__c,tempObj.gii__Invoice__r.gii__Account__r);
                            
                            mapOfInvoiceAndCustomerId.put(tempObj.id,tempObj.gii__Invoice__r.gii__Account__c);
                            setOfCustomerId.add(tempObj.gii__Invoice__r.gii__Account__c);
                        }
                    }    
                    
                    if(!(mapOfInvoiceAndCustomerId.isempty()) && !(setOfCustomerId.isempty()) && !(mapInvoiceDetailsRecord.isempty()) && !(mapAccount.isempty()))
                    {
                        giic_LoyaltyManagementUtility.InsertLoyaltyPointInTracker(mapOfInvoiceAndCustomerId,setOfCustomerId,mapInvoiceDetailsRecord,mapAccount);
                    }       
                }
            }
        }
        catch(Exception ex) 
        {           
            Database.rollback(sp); 
            List<Integration_Error_Log__c> lstErrors = new List<Integration_Error_Log__c>();
            string errorLogName = giic_Constants.ERROR_LOYALTY;
            string objectAPIName = 'giic_Account__c';
            lstErrors  = giic_IntegrationCustomMetaDataUtil.deActivateCurrentError(setAccIdforErrors,errorLogName,objectAPIName);
                
            for(Id i : setAccIdforErrors)
            {
                Integration_Error_Log__c objErr= new Integration_Error_Log__c();
                objErr.giic_Account__c = i;
                objErr.Name = errorLogName;
                objErr.giic_IsActive__c = true;
                objErr.Error_Code__c   = ex.getMessage();
                objErr.Error_Message__c = ex.getMessage()+'-Line Number '+ex.getLineNumber();
                lstErrors.add(objErr);
            }              
            if(lstErrors.size()>0)
            {
                upsert lstErrors;
            }
            
        }
    }
    
    /*
    * Method name : finish
    * Description : Process any pending work after the batch complete
    * Return Type : nill
    * Parameter : Database.BatchableContext BC,
    */
    global void finish(Database.BatchableContext BC) {
        Database.executeBatch(new giic_RewardPointsTrackerBatch());
    }
    
     /*
    * Method name : execute
    * Description : to schedule loyalty batch
    * Return Type : nill
    * Parameter : SchedulableContext  BC,
    */
    global void execute(SchedulableContext sc)
    {
        giic_LoyaltyPointsTrackerBatch b = new giic_LoyaltyPointsTrackerBatch();       
        database.executebatch(b);
    }
    
}