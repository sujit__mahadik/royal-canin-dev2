/************************************************************************************
Version : 1.0
Created Date : 13 Sep 2018
Function : Tests life cycle of Sales Order in OMS and hence batch performances.
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/
@isTest
public class giic_Test_SOLifeCyclePerformance {
    
    @testSetup
    static void setup(){
       //  List<Account> testConsumerAccountList = giic_Test_DataCreationUtility.insertConsumerAccount();
         List<gii__Warehouse__c> testWareList = giic_Test_DataCreationUtility.insertWarehouse();
        
       // giic_Test_DataCreationUtility.insertExceptionRoute(new list<Id>{testConsumerAccountList[0].id},new list<Id>{testWareList[0].id},1);
       // giic_Test_DataCreationUtility.insertRoute(new list<Id>{testConsumerAccountList[0].id},new list<Id>{testWareList[0].id},1);
        SetupTestData.createCustomSettings();
        giic_Test_DataCreationUtility.testSystemPolicyCreation();
        giic_Test_DataCreationUtility.insertWarehouse_N(6);
        giic_Test_DataCreationUtility.insertConsumerAccount_N(6);        
        giic_Test_DataCreationUtility.insertProduct_N();
        giic_Test_DataCreationUtility.insertSalesOrderStaging_N(20);
        giic_Test_DataCreationUtility.insertSalesOrderLineStaging_N();
        giic_Test_DataCreationUtility.insertSalesOrderPaymentStaging_N();
		/*
      	List<gii__AccountAdd__c> testAccRefList = giic_Test_DataCreationUtility.getAccountReference(testConsumerAccountList);
        Database.insert(testAccRefList);
        List<gii__Product2Add__c> testProductList = giic_Test_DataCreationUtility.insertProduct();
        giic_Test_DataCreationUtility.insertProductInventoryQTYDetails();
        */
        //giic_Test_DataCreationUtility.insertBulkSalesOrder();
        //giic_Test_DataCreationUtility.insertBulkSOLine();
        //giic_Test_DataCreationUtility.insertSalesOrderStaging();
        //giic_Test_DataCreationUtility.insertSalesOrderLineStaging();
         giic_Test_DataCreationUtility.CreateAdminUser();
    }
    
    public static testMethod void testSOStagingConversion(){
         User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
         system.runAs(u){
        
        String sObjectApiName = 'gii__SalesOrderStaging__c';
        Map<String, giic_IntegrationCustomMetaDataWrapper> mapIntegrationSettings = giic_IntegrationCustomMetaDataUtil.getIntegrationSettings(sObjectApiName);
        
        /*
        Map<Id, gii__SalesOrderStaging__c> soStaging = new Map<Id, gii__SalesOrderStaging__c>([Select Id from gii__SalesOrderStaging__c]);
        Map<Id, gii__SalesOrderLineStaging__c> solStaging = new Map<Id, gii__SalesOrderLineStaging__c>([Select Id, giic_ProductID__c from gii__SalesOrderLineStaging__c]);
        */
        //Prepare setting accourding to custom metadata
        Test.startTest();
        giic_SOStagingToSOConversionBatch SOStagingToSO = new giic_SOStagingToSOConversionBatch(mapIntegrationSettings, sObjectApiName);
        Database.executeBatch(SOStagingToSO);
        Test.stopTest();
        
        Map<Id,  gii__SalesOrder__c> soMap = new  Map<Id,  gii__SalesOrder__c>([Select gii__TotalWeight__c, 
                                                                                gii__Carrier__c, 
                                                                                giic_CCAuthorized__c,
                                                                                giic_TaxCal__c,
                                                                                giic_PromoCal__c,
                                                                                giic_AddressValidated__c,
                                                                                gii__PaymentMethod__c,
                                                                                gii__Account__r.Blocked__c,
                                                                                gii__Account__r.Check_if_you_are_Tax_Exempt__c,
                                                                                giic_Source__c,
                                                                                giic_ShipFeeCal__c,
                                                                                giic_SOStatus__c,
                                                                                gii__Released__c,
                                                                                giic_ProcessingStatus__c from gii__SalesOrder__c]);
        Map<Id,  gii__SalesOrderLine__c> solMap = new  Map<Id,  gii__SalesOrderLine__c>([Select Id from gii__SalesOrderLine__c]);
        //System.debug('SO Size is ::::' + soMap.size());
        
       // System.debug('SO Size is ::::' + solMap.size());
        
        for(Id soId : soMap.keySet()){
            soMap.get(soId).giic_SOStatus__c='Open';
            soMap.get(soId).gii__Released__c=true;
        }
    }
       /*Database.update(soMap.values());
        Map<Id,  gii__SalesOrder__c> soMap2 = new  Map<Id,  gii__SalesOrder__c>([Select gii__TotalWeight__c, 
                                                                                gii__Carrier__c, 
                                                                                giic_CCAuthorized__c,
                                                                                giic_TaxCal__c,
                                                                                giic_PromoCal__c,
                                                                                giic_AddressValidated__c,
                                                                                gii__PaymentMethod__c,
                                                                                gii__Account__r.Blocked__c,
                                                                                gii__Account__r.Check_if_you_are_Tax_Exempt__c,
                                                                                giic_Source__c,
                                                                                giic_ShipFeeCal__c,
                                                                                giic_SOStatus__c,
                                                                                gii__Released__c,
                                                                                giic_ProcessingStatus__c from gii__SalesOrder__c]);
        System.debug('SO Size is ::::' + soMap2);*/
        //giic_AllocationBatch allocationBatch = new giic_AllocationBatch();
        //Database.executeBatch(allocationBatch);
        
    } 
}