/************************************************************************************
Version : 1.0
Created Date : 27-Aug-2018
Function : Test class for giic_ExecuteAllocationBatchController
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/
@isTest
private class giic_Test_ExeAllocationBatchController {
    @testSetup
    static void setup(){
         giic_Test_DataCreationUtility.CreateAdminUser(); 
        List<gii__Warehouse__c> testWareList = giic_Test_DataCreationUtility.insertWarehouse();
        giic_Test_DataCreationUtility.testSystemPolicyCreation();
        List<giic_ExecuteAllocationBatchController.BatchJob> testExeWrapperList = new List<giic_ExecuteAllocationBatchController.BatchJob>();
    }
    public static testMethod void testControllerMethods(){
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        System.Runas(u)  
   {
        Test.startTest();
        
        gii__SystemPolicy__c policy = [SELECT Id FROM gii__SystemPolicy__c LIMIT 1];
        ApexPages.StandardController cont = new ApexPages.StandardController(policy);
        giic_ExecuteAllocationBatchController exeObj = new giic_ExecuteAllocationBatchController(cont);
        
		exeObj.executeAllocation();       
        exeObj.getBatchJobs();
        
        Test.stopTest();
    } 
}
}