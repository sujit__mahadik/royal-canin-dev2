/*
 * Date:       		 - Developer, Company					       - Description
 * 2015-05-11        - Mayank Srivatava, Acumen Solutions          - Created
 * 2015-08-10		 - Stevie Yakkel, Acumen Solutions			   - Edited
 */

@isTest
private class ChangeAccountOwnershipBatchTest {
	
	@isTest static void testChangeAccountOwnership() {
		SetupTestData.createCustomSettings();
        User adminUser = TestHelperClass.createAdminUser();
        System.runAs(adminUser){
			TestHelperClass.createPostalCodes();
			Fiscal_Year__c fy = TestHelperClass.createFiscalYears();
			List<PostalCode__c> pcs = TestHelperClass.createPostalCodes();
			User salesRep = TestHelperClass.createSalesRepUserWithManager();
			UserRole salesRepRole = [Select id, ParentRoleId from UserRole where id = : salesRep.UserRoleId Limit 1];
			User ManagerUser = [Select Id, Name from User where UserRoleId = : salesRepRole.ParentRoleId Limit 1];
			salesRep.ManagerId = ManagerUser.id;
			update salesRep;
			Region__c reg = TestHelperClass.createRegion(managerUser);
			Territory__c terr = TestHelperClass.createTerritory(reg.id);
			TerritoryAssignment__c ta = TestHelperClass.createTerrirtoryAssignment(pcs[0].id, terr.id,fy.Id);
			Account acc = TestHelperClass.createAccountData(pcs[0].id);
			Test.startTest();
				ChangeAccountOwnershipBatch cab = new ChangeAccountOwnershipBatch();
				Database.executeBatch(cab);
			Test.stopTest();
			acc = [Select Current_Year_Territory_Assignment__c from Account where id = :acc.id];
			System.assertEquals(acc.Current_Year_Territory_Assignment__c,ta.id);
			ta = [Select Sync_Needed__c from TerritoryAssignment__c where id = : ta.id];
			System.assert(ta.Sync_Needed__c == false);
        }
	}
	
}