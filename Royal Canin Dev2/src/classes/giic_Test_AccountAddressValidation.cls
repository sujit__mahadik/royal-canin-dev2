/************************************************************************************
Version : 1.0
Name : giic_Test_AccountAddressValidation
Created Date : 10 Sep 2018
Created By : Shivdeep Gupta
Function :  This is Test class for classes giic_AccountAddressValidationSchedular, giic_AccountAddressValidationBatch, giic_IntegrationCommonUtil .. class
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/
@isTest(SeeAllData = false)
public class giic_Test_AccountAddressValidation {
    // Testing execute method of giic_AccountAddressValidationSchedular
    
    private static testMethod void  Test_Scheduler_executeMethod(){
        createData();
        Test.startTest(); 
        Test.setMock(WebServiceMock.class, new giic_Test_FedExCalloutMock());
        giic_AccountAddressValidationSchedular objAAVS = new giic_AccountAddressValidationSchedular();
        String sch = '0 0 23 * * ?'; 
        system.schedule('Account Address Validation Check', sch, objAAVS); 
        Test.stopTest(); 
        
    }
    private static testMethod void  Test_scheduleJob(){
        createData();
        Test.startTest(); 
        Test.setMock(WebServiceMock.class, new giic_Test_FedExCalloutMock());
        giic_AccountAddressValidationSchedular objAAVS = new giic_AccountAddressValidationSchedular();
        String sch = '0 0 23 * * ?'; 
        // system.schedule('Account Address Validation Check', sch, objAAVS); 
        
        giic_AccountAddressValidationSchedular.scheduleJob(sch);
        
        Test.stopTest(); 
        
    }
    
    public static void createData(){
        List<gii__Warehouse__c> defaultWHList = giic_Test_DataCreationUtility.insertWarehouse();
        system.debug('defaultWHList--->>> ' + defaultWHList);
        giic_Test_DataCreationUtility.testSystemPolicyCreation2();
        SetupTestData.createCustomSettings();
        List<gii__Warehouse__c> testWareListN = giic_Test_DataCreationUtility.insertWarehouse_N(5); // lstWarehouse_N
        list<giic_CommonUtility.WarehouseDistance> lstWDistence = new list<giic_CommonUtility.WarehouseDistance>();
        giic_CommonUtility.WarehouseDistance objWD;
        // Logic to add JSON in giic_WarehouseDistanceMapping__c field of Account Object starts Here ::: CommentUniqueId = commentSep21_0530
        integer intDistence = 300;
        for(gii__Warehouse__c objW :  giic_Test_DataCreationUtility.lstWarehouse_N){
            objWD = new giic_CommonUtility.WarehouseDistance(objW.Id, intDistence);
            intDistence = intDistence + 20;
            lstWDistence.add(objWD);
        }
        intDistence = intDistence + 55;
        objWD = new giic_CommonUtility.WarehouseDistance(defaultWHList[0].Id, intDistence);
        lstWDistence.add(objWD);
        string sWarehouseDistanceMapping = JSON.serialize(lstWDistence);
        system.debug('sWarehouseDistanceMapping-->>>> ' + sWarehouseDistanceMapping);
        // Account creation 
        List<Account> testConsumerAccountList = giic_Test_DataCreationUtility.insertConsumerAccount_N(5);
        for(Account OAc : giic_Test_DataCreationUtility.lstAccount_N ){
            OAc.giic_WarehouseDistanceMapping__c = sWarehouseDistanceMapping;
        }
        update giic_Test_DataCreationUtility.lstAccount_N;
        
        // Logic to add JSON in giic_WarehouseDistanceMapping__c  field of Account Object Ends Here ::: CommentUniqueId = commentSep21_0530
    
    }
    
}