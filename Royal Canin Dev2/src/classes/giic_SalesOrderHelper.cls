/************************************************************************************
Version : 1.0
Created Date : 24 Aug 2018
Function : Sales Order related functions
Modification Log : 
25 Sept 2018 - Added logic for OMS-146 in submitSalesOrder & bulkReleaseSO
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/
public class giic_SalesOrderHelper {
    /*
* Method name : submitSalesOrder
* Description : submit sales order for allocation
* Return Type : 
* Parameter : 
*/
    @auraEnabled
    public static string submitSalesOrder(String soId){
        if(soId != null){
            try{  
                system.debug('submitSalesOrder sales order helper');
                // Check for permission set - Manage Route Orders
                Integer listSize = [SELECT PermissionSetId FROM PermissionSetAssignment WHERE AssigneeId= :UserInfo.getUserId() AND PermissionSet.Name = 'giic_ManageRouteOrders'].size();

                gii__SalesOrder__c objSalesOrder = [Select gii__TotalWeight__c, 
                                                    gii__Carrier__c,
                                                    gii__Carrier__r.giic_UniqueCarrier__c,
                                                    giic_CCAuthorized__c,
                                                    giic_TaxCal__c,
                                                    giic_PromoCal__c,
                                                    giic_AddressValidated__c,
                                                    gii__PaymentMethod__c,
                                                    gii__Account__r.Blocked__c,
                                                    gii__Account__r.Check_if_you_are_Tax_Exempt__c,
                                                    gii__Account__r.Resale_Sales_Tax_Cert_Expiration_Date__c,
                                                    giic_Source__c,
                                                    giic_ShipFeeCal__c,
                                                    giic_ProcessingStatus__c,
                                                    gii__ShipToStateProvince__c,
                                                    giic_SOStatus__c,
                                                    gii__Released__c,
                                                    (SELECT Id, gii__Product__r.Name, gii__Product__r.giic_ExceptionState__c, gii__LineStatus__c
                                                    FROM gii__SalesOrder__r) //Added sub-query to check product exception state, if any
                                                    from gii__SalesOrder__c where Id= :soId];
                //Check if the order is already released
                if(objSalesOrder.gii__Released__c)
                    return Label.giic_OrderPreReleased;
                //Check if refered account is blocked.
                if(objSalesOrder.gii__Account__r.Blocked__c)
                    return Label.giic_CustomerAccountBlocked;
                // Check if order was not processed successfully.
                //if(!(objSalesOrder.giic_Source__c == null || objSalesOrder.giic_Source__c == Label.giic_SalesOrderOMSSource) && !(objSalesOrder.giic_ProcessingStatus__c == Label.giic_Submitted || objSalesOrder.giic_ProcessingStatus__c == Label.giic_SubmittedwithError))
                if(objSalesOrder.giic_ProcessingStatus__c == giic_Constants.SOSTATUSPROCESSWITHERROR || objSalesOrder.giic_ProcessingStatus__c == giic_Constants.SOSTATUSINPROGRESS)
                    return Label.giic_SalesOrderProcessingError;
                //If sales order is cancelled, no operation will be performed
                if(objSalesOrder.giic_SOStatus__c == System.Label.giic_SalesOrderCancelledStatus)
                    return Label.giic_OrderCancelled;
                    system.debug('submitSalesOrder sales order helper');
                // Checking pre requests completion statuses.
                if((objSalesOrder.gii__PaymentMethod__c == Label.giic_PaymentMethodCreditCard && !objSalesOrder.giic_CCAuthorized__c) || 
                     (!objSalesOrder.gii__Account__r.Check_if_you_are_Tax_Exempt__c && !objSalesOrder.giic_TaxCal__c) || 
                     !objSalesOrder.giic_PromoCal__c || 
                     !objSalesOrder.giic_AddressValidated__c || 
                     !objSalesOrder.giic_ShipFeeCal__c){
                    List<String> errorList = new List<String>();
                    if(objSalesOrder.gii__PaymentMethod__c == Label.giic_PaymentMethodCreditCard && !objSalesOrder.giic_CCAuthorized__c)
                        errorList.add(Label.giic_PaymentAuthorizationFailed);
                    if(!objSalesOrder.gii__Account__r.Check_if_you_are_Tax_Exempt__c && !objSalesOrder.giic_TaxCal__c)
                        errorList.add(Label.giic_TaxCalculationFailed);
                    if(!objSalesOrder.giic_PromoCal__c)
                        errorList.add(Label.giic_PromotionCalculationFailed);
                    if(!objSalesOrder.giic_AddressValidated__c)
                        errorList.add(Label.giic_ShipAddValidationFailed);
                    if(!objSalesOrder.giic_ShipFeeCal__c)
                        errorList.add(Label.giic_SOShippingFeeError);
                    return String.join(errorList,' \n');
                }
                //Check if any of the product is in Exception state
                if(objSalesOrder.gii__SalesOrder__r.size()> 0){
                    //Using for loop as a single SO is involved
                    for(gii__SalesOrderLine__c soLine : objSalesOrder.gii__SalesOrder__r){
                        if(soLine.gii__LineStatus__c != giic_Constants.SOSTATUSCANCELLED && soLine.gii__Product__r.giic_ExceptionState__c != NULL && 
                        soLine.gii__Product__r.giic_ExceptionState__c.contains(objSalesOrder.gii__ShipToStateProvince__c))
                            return System.Label.giic_ExceptionStateError+soLine.gii__Product__r.Name;
                    }
                }
                system.debug('submitSalesOrder sales order helper');
                String msgString = '';
                Map<String, Id> carrierMap = new Map<String, Id>();
                Id carrierId;
                set<string> setCarrierName=new set<string>{label.giic_ParcelCarrier};
                for(gii__Carrier__c objCarrier : [SELECT Id, Name,giic_UniqueCarrier__c FROM gii__Carrier__c where giic_UniqueCarrier__c in :setCarrierName]){
                    carrierMap.put(objCarrier.giic_UniqueCarrier__c, objCarrier.Id);
                }
                if(carrierMap != null && objSalesOrder.gii__Carrier__r.giic_UniqueCarrier__c == giic_Constants.CARRIERROUTE){
                    if(listSize == 0 && objSalesOrder.gii__TotalWeight__c < Integer.valueof(Label.giic_RouteOrderMinWeight)){
                        return Label.giic_RouteOrderWeightValidationError;
                    }else if(listSize >= 0 && objSalesOrder.gii__TotalWeight__c < Integer.valueof(Label.giic_RouteOrderMinWeight) &&
                            carrierMap.containskey(label.giic_ParcelCarrier)){
                        msgString = Label.giic_RouteOrderWeightValidationWarning;
                        objSalesOrder.gii__Carrier__c = carrierMap.get(label.giic_ParcelCarrier);
                    } 
                }
                objSalesOrder.giic_SOStatus__c=giic_Constants.SOSTATUSOPEN;
                objSalesOrder.gii__Released__c=true;
                // if tax exampt, and expiration not happeded 
                if(objSalesOrder.gii__Account__r.Check_if_you_are_Tax_Exempt__c && objSalesOrder.gii__Account__r.Resale_Sales_Tax_Cert_Expiration_Date__c >= System.Today())
                    objSalesOrder.giic_TaxExempt__c = objSalesOrder.gii__Account__r.Check_if_you_are_Tax_Exempt__c;
                else
                    objSalesOrder.giic_TaxExempt__c = false;
                update objSalesOrder;
                return msgString;
            }catch(DmlException e){
                throw new AuraHandledException(e.getMessage());  
            }
        }
        return null;
    }
    
    
    /*
    * Method name : calculateTax
    * Description : Based on Sales Order ID it calculates tax. Returns true in case of success and returns false in case of error.
    * Return Type : boolean
    * Parameter : soId (sales order id)
    */
    @auraEnabled
    public static String CalculateTaxCommitFalse(String soId){        
        return giic_AvalaraServiceProviderClass.CalculateTaxCommitFalse(soId);        
    }
    
     /*
    * Method name : calculateTax
    * Description : Based on Sales Order ID it calculates tax. Returns true in case of success and returns false in case of error.
    * Return Type : boolean
    * Parameter : soId (sales order id)
    */
    @auraEnabled
    public static void CalculateTaxforSO(String soId){   
    	List<gii__SalesOrder__c>  lstobjOrder = new List<gii__SalesOrder__c>();
        lstobjOrder = [select id,gii__Warehouse__r.giic_WarehouseCode__c,
        					  gii__Account__r.Check_if_you_are_Tax_Exempt__c,
        					  gii__Account__r.Resale_Sales_Tax_Cert_Expiration_Date__c,giic_TaxExempt__c        					   
        					  from gii__SalesOrder__c where id =: soId and giic_TaxCal__c != true ];
        if(lstobjOrder.size()>0)
        {
            //Check for Account Tax Exemption before Calculating Tax
             //Check for Account Tax Exemption before Calculating Tax
            /*if(lstobjOrder[0].gii__Account__r.Check_if_you_are_Tax_Exempt__c != true || 
               (lstobjOrder[0].gii__Account__r.Check_if_you_are_Tax_Exempt__c == true && 
                lstobjOrder[0].gii__Account__r.Resale_Sales_Tax_Cert_Expiration_Date__c < System.Today()))*/
            if(!lstobjOrder[0].giic_TaxExempt__c)
            {
            	Map<String,Object> result = new Map<String,Object>();
	            result = giic_AvalaraServiceProviderClass.CalculateTaxforSO(lstobjOrder[0].id,null,lstobjOrder[0].gii__Warehouse__r.giic_WarehouseCode__c,giic_Constants.CREATE_T,True);
	            //COMMIT_T_FALSE
	            if(result.containsKey('ISSUCCESS') && result.get('ISSUCCESS') == false)
				{
					if(result.containsKey('RESPONSE'))
					{						
						List<Integration_Error_Log__c> lstErrors = new List<Integration_Error_Log__c>();
						lstErrors = (List<Integration_Error_Log__c>)  result.get('RESPONSE');   
						if(!lstErrors.isEmpty()){
							insert lstErrors;							
						}
					}
					if(result.containsKey('ERROR'))
					{
						giic_AvalaraServiceProviderClass.CollectErrors(soId, (String)result.get('ERROR'));  
					}
				}
            }
        }
    }
    
    /*
    * Method name : updateSalesOrder
    * Description : update Sales order with updated information
    * Return Type : 
    * Parameter : list<gii__SalesOrder__c>
    */
    
    public static map<string,Object> updateSalesOrder(list<gii__SalesOrder__c> lstSalesOrder){
        if(lstSalesOrder != null && lstSalesOrder.size() > 0){
            Database.SaveResult[] srList = Database.update(lstSalesOrder, false);
            // Iterate through each returned result
            Integer index = 0;
            List<Integration_Error_Log__c> lstErrors = new List<Integration_Error_Log__c>();
            Set<String> setErrorIds = new Set<String>();
            String userStory = system.label.giic_AllocationUserStoryName; 
            Map<String, String> fieldApiForErrorLog = new Map<String, String>{'giic_SalesOrder__c'=>'Id'};
            for (Database.SaveResult sr : srList) {
                if (!sr.isSuccess()) {
                    for(Database.Error err : sr.getErrors()) {
                        giic_IntegrationCustomMetaDataUtil.collectErrors(lstErrors, setErrorIds, userStory, fieldApiForErrorLog, lstSalesOrder[index], String.valueOf(err.getStatusCode()), err.getMessage());
                    }
                }
                index++;
            }
            if(!lstErrors.isEmpty()) insert lstErrors;
        }
        return new map<string,Object>();
    }
    
    
    /*
    * Method name : bulkReleaseSO
    * Description : Based on List of Sales Orders releases them by setting gii__Released__c to true
    * Return Type : void
    * Parameter : list<gii__SalesOrder__c>
    */
    
    public static void bulkReleaseSO(list<gii__SalesOrder__c> lstSalesOrder){
       
        if(lstSalesOrder != null && lstSalesOrder.size() > 0){
            for(gii__SalesOrder__c soItem : lstSalesOrder){
                Boolean skipSO = false;
                if(soItem.gii__SalesOrder__r.size()> 0){
                    for(gii__SalesOrderLine__c soLine : soItem.gii__SalesOrder__r){
                        if(soLine.gii__LineStatus__c != giic_Constants.SOSTATUSCANCELLED && soLine.gii__Product__r.giic_ExceptionState__c != NULL && 
                        soLine.gii__Product__r.giic_ExceptionState__c.contains(soItem.gii__ShipToStateProvince__c)){
                            skipSO = true;
                        }
                    }
                }
                if(skipSO == false){
                    soItem.giic_SOStatus__c=giic_Constants.SOSTATUSOPEN;
                    soItem.gii__Released__c=true; 
                }
            }
            try{
               Database.update(lstSalesOrder);
            }catch(DmlException ex){
                system.debug('Ex:' + ex.getMessage() + ex.getLineNumber()); 
            }
        }
        
    }

    /* crated by - Ranu
     * purpose - cancel sales order and its all lines
     */
     
    @auraEnabled
    public static map<String,Object> cancelSalesOrder(String soid){
        gii__SalesOrder__c so;
        map<String,Object> res = new map<String,Object>();
        system.debug('soid:' + soid);
        list<gii__SalesOrder__c> lstSalesOrder = giic_CommonUtility.getSODetails(new set<id>{soId});
        try
        {
            if(lstSalesOrder != null && lstSalesOrder.size() > 0)
            {
                so = lstSalesOrder[0];
                map<String,Object> response = ValidateSalesOrder(giic_Constants.CANCELSO,so);
                if(response != null && response.containsKey(giic_Constants.SUCCESS))
                {
                    //cancel so line
                    cancelSoLines(new map<String,list<gii__SalesOrderLine__c>>{so.id => so.gii__SalesOrder__r});
                    
                    giic_PromotionHelper.recalculatePromotionOnCancelSO(new map<string,object>{'SOIDS'=>new set<string>{so.id}});
                    res.put(giic_Constants.SUCCESS,true);
                }
                    
                else return response; //return errors
            }else   {  res.put('ERROR' , giic_Constants.SOCANCEL);  }
        }catch(Exception ex)  {  res.put('ERROR',ex.getMessage());}
        //do not delete this debug
        system.debug('1:' + res + '2:' + soid);
        return res;
        
    }
    
    public static void cancelSoLines(map<String,list<gii__SalesOrderLine__c>> mapsolines)
    {
        
        list<gii__SalesOrderLine__c> lstSOLToCancel=new list<gii__SalesOrderLine__c>();
        Id SalesOrderId;
        try
        {
            for(String soid : mapsolines.keySet())
            { SalesOrderId = soid; //NOTE - currently code will work only one one Sales order as below methods were not written for bulk
                for(gii__SalesOrderLine__c objSOL : mapsolines.get(soid)){
                    if((objSOL.gii__ReservedQuantity__c ==  0 || objSOL.gii__ReservedQuantity__c == null ) && objSOL.gii__CancelReason__c == null){
                        lstSOLToCancel.add(new gii__SalesOrderLine__c(Id=objSOL.id,gii__CancelDate__c = System.today(),gii__CancelledQuantity__c = objSOL.gii__OrderQuantity__c,gii__CancelReason__c=system.Label.giic_DefaultCancelReasonSOL, gii__TaxAmount__c = 0.0, giic_LineStatus__c = 'Cancelled',gii__DiscountPercent__c=0 ));
                    }
             
                }
                
            }
            list<gii__SalesOrder__c> soUpdate = new list<gii__SalesOrder__c>();
            if(lstSOLToCancel.size()>0)
            {
                Database.update(lstSOLToCancel);
                for(String soid : mapsolines.keySet())
                {
                    soUpdate.add(new gii__SalesOrder__c
                    (
                        id = soid, giic_CancelReson__c=system.Label.giic_DefaultCancelReasonSOL,
                        giic_CancelDate__c=system.today(), giic_SOStatus__c=system.label.giic_SalesOrderCancelledStatus,
                        gii__Promotion__c=null, gii__DiscountPercent__c=0   ));
                }
                 if(soUpdate.size() > 0)  update soUpdate;
            }
          
        }catch(Exception ex)  { system.debug('Ex:' + ex.getMessage() + ex.getLineNumber()); throw ex; }
        
            
    }
    
    private static map<String,Object> ValidateSalesOrder(String transactionType, gii__SalesOrder__c so)
    {
        list<String> errors = new list<String>();
        map<String,Object> mapRes = new map<String,Object>();
        if(transactionType == giic_Constants.CANCELSO)
        {
            if(so.gii__Released__c) errors.add(giic_Constants.SO_CANCEL_RELEASE);
            else if(so.giic_SOStatus__c==system.label.giic_SalesOrderCancelledStatus) errors.add(giic_Constants.SOCANCEL);
        }
        
        if(errors.size() > 0)  { mapRes.put('ERROR',errors); return mapRes; }
        mapRes.put(giic_Constants.SUCCESS,TRUE); return mapRes;
    }
}