/*
 * Date:       		 - Developer, Company					       - Description
 * 2015-04-28        - Solutions Accelerator, Acumen Solutions     - Created
 * 2015-05-04        - Mayank Srivatava, Acumen Solutions          - Customized 'changeAccountOwnershipWithListOfAccounts' for RC
 */

public with sharing class ChangeAccountOwnership {
	
	
	public String ChangeAccountOnwershipWithAccountUserMap(Map<Id, Id> AccountAndUserIdMap)
	{
		//change the ownership for each acccount based on the matching userid in the map
		String Error = '';
		
		
		return Error;
	}
	
	public String ChangeAccountOwnershipWithZipAndUser(Map<Id, Id> ZipCodeAssignments, Map<Id, Id> managers)
	{
		//when all accounts in a zip needs to be assigned to a user
		String Error = '';
		Set<Id> zipIds = ZipCodeAssignments.keySet();
		String Query = 'Select id, name, OwnerId, PostalCodeId__c from Account where PostalCodeId__c in (\'';
		Integer countOfZips = 0;
		for(id zipid : zipIds)
		{
			if(countOfZips != 0){
				Query += '\', \'';
			}
			Query += zipid;
			countOfZips += 1;
		}
		Query += '\')'; // and ManuallyAssigned__c != true';
		system.debug(Query);
		id cobatchid = database.executeBatch(new ChangePostalCodeAccountOwnershipBatch(Query, ZipCodeAssignments, managers), 1000); 
		return Error;
	}

	public static void changeAccountOwnershipWithListOfAccounts(List<Account> accounts){
		String Error = '';
		List<Id> postalCodeIds = new List<id>();
		Map<String, TerritoryAssignment__c> postalCodeTerritoryOwnerMap = new Map<String, TerritoryAssignment__c>();
		
		
		for(Account acct : accounts){
            if(acct.PostalCodeID__c != null){
				postalCodeIds.add(acct.PostalCodeID__c);	
            }
		}
		
        if(postalCodeIds.size() == 0){
            return;
        }
		//String GlobalUnassingnedUser = Utilities.getGlobalUnassignedUser();

		List<TerritoryAssignment__c> terrAssign = [select id, Name, PostalCode__c, TerritoryID__r.Ownerid, PostalCodeID__c, TerritoryID__r.Region__c from TerritoryAssignment__c 
												   where PostalCodeId__c in : postalCodeIds 
											       and Fiscal_Year__r.isActive__c = :true];
		for(TerritoryAssignment__c ta : terrAssign){
			postalCodeTerritoryOwnerMap.put(ta.PostalCodeID__c, ta);
		}
		
		for(Account acct : accounts){
            //System.debug('in Accts '+acct.postalCodeID__c+' --> '+postalCodeTerritoryOwnerMap);
			if(acct.Exclude_from_Territory_Assignment__c == FALSE
				&& postalCodeTerritoryOwnerMap.containsKey(acct.PostalCodeID__c)){				
				acct.DateAssigned__c = DateTime.now();
				acct.UnassignedLead__c = false;
				acct.Current_Year_Territory_Assignment__c = postalCodeTerritoryOwnerMap.get(acct.PostalCodeID__c).id;
                if(postalCodeTerritoryOwnerMap.get(acct.PostalCodeID__c) != null && postalCodeTerritoryOwnerMap.get(acct.PostalCodeID__c).TerritoryID__r != null){
					acct.Region__c = postalCodeTerritoryOwnerMap.get(acct.PostalCodeID__c).TerritoryID__r.Region__c;
                    acct.OwnerId = postalCodeTerritoryOwnerMap.get(acct.PostalCodeID__c).TerritoryID__r.Ownerid;
                }

			}
			/*else{
				acct.OwnerId = GlobalUnassingnedUser;
				acct.DateAssigned__c = DateTime.now();
				acct.UnassignedLead__c = true;
			}*/
		}
	}

	
	public String UnAssignAccountsInZip(List<String> ZipCodes)
	{
		String Error = '';
		
		
		return Error;
	}
	public String UnAssignAccountsFromList(List<id> AccountIds)
	{
		String Error = '';
		
		
		return Error;
	}
	
}