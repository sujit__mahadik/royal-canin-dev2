@isTest
private class giic_Test_SOConversionBatchController {
    
    @testSetup static void testData(){
        giic_Test_DataCreationUtility.insertWarehouse();
        giic_Test_DataCreationUtility.testSystemPolicyCreation();
        giic_Test_DataCreationUtility.insertGlobalParametersCustomSetting();
        giic_Test_DataCreationUtility.insertConsumerAccount();
        giic_Test_DataCreationUtility.createAdditionalCharge();
        giic_Test_DataCreationUtility.CreateAdminUser();
    }
    
    private static testMethod void positivetest() {
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        giic_Test_DataCreationUtility.lstAccount = [ select id,RecordTypeId,Name,BillingCountry,BillingPostalCode,BillingState,BillingCity,
                                                    BillingStreet,ShippingStreet,ShippingCountry,ShippingPostalCode,ShippingState,ShippingCity,
                                                    ClinicCustomerNo__c,ShipToCustomerNo__c,Business_Email__c,Bill_To_Customer_ID__c from Account];
        system.runAs(u){
            Test.startTest();
            
            gii__SalesOrderStaging__c objStaging = new gii__SalesOrderStaging__c();
            objStaging.giic_Status__c = 'Draft';
            objStaging.giic_Source__c = 'INSITE';
            objStaging.giic_Account__c = giic_Test_DataCreationUtility.lstAccount[0].ShipToCustomerNo__c;// '12345678';
            objStaging.giic_ShipToName__c = 'Test';
            insert objStaging;
            
            List<gii__SalesOrderStaging__c> lstSOS = new List<gii__SalesOrderStaging__c>{objStaging};
                ApexPages.StandardSetController sc = new ApexPages.StandardSetController(lstSOS);
            sc.setSelected(lstSOS);
            giic_SalesOrderConversionBatchController objSOBatch = new giic_SalesOrderConversionBatchController(sc);
            objSOBatch.executeSOConversion();
            objSOBatch.findBatchJobs();
            objSOBatch.goBack();
            
            Test.stopTest();
        }
    }
    
}