@isTest
public class AddressSvcMock implements WebServiceMock {
  public void doInvoke(
    Object stub,
    Object request,
    Map<String, Object> response,
    String endpoint,
    String soapAction,
    String requestName,
    String responseNS,
    String responseName,
    String responseType) {

    if (requestName == 'IsAuthorized'){
      AddressSvc.IsAuthorizedResponse_element responseElement = new AddressSvc.IsAuthorizedResponse_element();
      AddressSvc.IsAuthorizedResult isAuthResult = new AddressSvc.IsAuthorizedResult();
      AddressSvc.IsAuthorized_element requestElement = (AddressSvc.IsAuthorized_element) request;
      isAuthResult.Operations = requestElement.Operations;
      isAuthResult.ResultCode = 'Success';
      responseElement.IsAuthorizedResult = isAuthResult;
      response.put('response_x', responseElement);
    } else if (requestName == 'Validate'){
      AddressSvc.ValidateResponse_element responseElement = new AddressSvc.ValidateResponse_element();
      AddressSvc.ValidateResult validateResult = new AddressSvc.ValidateResult();
      validateResult.ResultCode = 'Success';
      validateResult.ValidAddresses = new AddressSvc.ArrayOfValidAddress();
      AddressSvc.ValidAddress tempValidAddress  = new AddressSvc.ValidAddress();
      tempValidAddress.Line1 = 'junk';
      validateResult.ValidAddresses.ValidAddress = new List <AddressSvc.ValidAddress>{tempValidAddress};
      
      responseElement.ValidateResult = validateResult;
      response.put('response_x', responseElement);
    } else if (requestName == 'Ping'){
      AddressSvc.PingResponse_element responseElement = new AddressSvc.PingResponse_element();
      AddressSvc.PingResult pingResult = new AddressSvc.PingResult();
      pingResult.ResultCode = 'Success';
      responseElement.PingResult = pingResult;
      response.put('response_x', responseElement);
    }
  }
}