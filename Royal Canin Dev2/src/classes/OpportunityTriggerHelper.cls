/*
 * Date:       		 - Developer, Company					       - Description
 * 2015-08-05		 - Stevie Yakkel, Acumen Solutions			   - Created
 */

public with sharing class OpportunityTriggerHelper {

	public static void assignPricebook2ID(List<Opportunity> opportunities) {

		Set<Id> setOfOppAccIdsInLoop = new Set<Id>();
		Map<Id, Account> relevantAccountMap = new Map<Id, Account>();
		Map<Id, PriceBook2> acctToPriceBookMap = new Map<Id, Pricebook2>();
		Map<String, Id> pbcToAcctId = new Map<String, Id>();
		Set<String> pbcSet = new Set<String>();

		for(Opportunity opp : opportunities) {
			setOfOppAccIdsInLoop.add(opp.AccountId);
		}

		for(Account acc : [SELECT Id, PriceBookCode__c FROM Account WHERE Id IN :setOfOppAccIdsInLoop]) {
			if(acc.PriceBookCode__c != null) {
				relevantAccountMap.put(acc.Id, acc);
				pbcSet.add(acc.PriceBookCode__c);
				pbcToAcctId.put(acc.PriceBookCode__c, acc.Id);
			}
		}

		for(Pricebook2 pb : [SELECT Id, PriceBookCode__c FROM Pricebook2 WHERE PriceBookCode__c IN :pbcSet]) {
			if(pb.PriceBookCode__c != null && pbcToAcctId.containsKey(pb.PriceBookCode__c)){
				acctToPriceBookMap.put(pbcToAcctId.get(pb.PriceBookCode__c), pb);
			}

		}

		for(Opportunity opp : opportunities) {
			
			Account acc = new Account();
			PriceBook2 pb = new PriceBook2();
			if(relevantAccountMap.get(opp.AccountId) != null) {
				pb = acctToPriceBookMap.get(opp.AccountId);
			}
			if(pb != null) {
				opp.PriceBook2Id = pb.Id;
			}
		}
	}

	public static void changeOpportunityNameOnInsert(List<Opportunity> opportunities) {

		Set<Id> setOfOppAccIdsInLoop = new Set<Id>();
		Map<Id, Account> relevantAccountMap = new Map<Id, Account>();

		System.debug(opportunities);

		for(Opportunity opp : opportunities) {
			setOfOppAccIdsInLoop.add(opp.AccountId);
		}

		for(Account acc : [SELECT Id, Name FROM Account WHERE Id IN :setOfOppAccIdsInLoop]) {
			relevantAccountMap.put(acc.Id, acc);
			System.debug(relevantAccountMap);
		}

		relevantAccountMap = new Map<Id, Account>([SELECT Id, Name FROM Account WHERE Id IN :setOfOppAccIdsInLoop]);

		for(Opportunity opp : opportunities) {
			if(relevantAccountMap.get(opp.AccountId) != null) {
				String accName = relevantAccountMap.get(opp.AccountId).Name;
				opp.Name = accName + ' - ' + opp.Name;
				System.debug('opp.Name: ' + opp.Name);
			}
		}
	}
}