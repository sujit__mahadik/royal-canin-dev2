/************************************************************************************
Version : 1.0
Created Date : 23 Oct 2018
Function : Helper for product reference trigger
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/
public class giic_ProductReferenceTriggerHelper {
    
    /* Method name : processFieldMap
    * Description : to map field data from product to product reference
    * Return Type : void
    * Parameter : List of product reference
    */
    public void processFieldMap(List<gii__Product2Add__c> lstProductReference )
    {
        List<SObject> lstSourceObject = new  List<SObject>();
        String sObjectApiName = giic_Constants.PRODUCT2; 
        set<string> setProductId = new set<string>();
        Map<String, SObject> mapParentIdChildObject = new Map<String, SObject>();
        if(lstProductReference !=null && lstProductReference.size()>0){
            for(gii__Product2Add__c  objPR : lstProductReference){ 
                if(string.isNotEmpty(objPR.gii__ProductReference__c)){
                    mapParentIdChildObject.put(objPR.gii__ProductReference__c , objPR);
                    setProductId.add(objPR.gii__ProductReference__c);
                }
            }
        }
        if(setProductId != null && setProductId.size() > 0 ){
            // below query must include all those fields of source object which need to be mapped to target object
            lstSourceObject = [select id, name, Weight_Unit__c, Weight__c, SKU__c, giic_StockUM__c,giic_StockingUnitofMeasure__c, giic_SellingUnitofMeasure__c, giic_Substitute__c, giic_ProductStyle__c, giic_ExceptionState__c, giic_DefaultWarehouse__c, giic_Style__c, giic_MerchandiseType__c, giic_NonStock__c from Product2 where id in : setProductId];
        }
        
        if(lstSourceObject != null && lstSourceObject.size() > 0 && mapParentIdChildObject != null && mapParentIdChildObject.size()>0){
            giic_DataMappingUtils.processSObjectField(lstSourceObject,sObjectApiName,mapParentIdChildObject);
        }
        
    }
    
    
}