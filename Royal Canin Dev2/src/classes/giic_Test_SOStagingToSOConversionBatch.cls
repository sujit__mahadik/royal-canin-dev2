/************************************************************************************
Version : 1.0
Name : giic_Test_SOStagingToSOConversionBatch  
Created Date : 14 Aug 2018
Function : this is test class for giic_SOStagingToSOConversionBatch
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/

@isTest(SeeAllData = false)
private class giic_Test_SOStagingToSOConversionBatch {
    
    @testSetup
    static void testData(){
        giic_Test_DataCreationUtility.insertWarehouse();
        giic_Test_DataCreationUtility.testSystemPolicyCreation();
        giic_Test_DataCreationUtility.insertGlobalParametersCustomSetting();
        giic_Test_DataCreationUtility.insertConsumerAccount();
        giic_Test_DataCreationUtility.createCarrier();
        giic_Test_DataCreationUtility.insertUnitofMeasure();
        giic_Test_DataCreationUtility.insertProduct();
        giic_Test_DataCreationUtility.CreateAdminUser();
    }
    public static void querySetupData(){
        giic_Test_DataCreationUtility.lstWarehouse=[select id,Name,giic_WarehouseCode__c from gii__Warehouse__c];
        
        giic_Test_DataCreationUtility.lstAccount=[select id,RecordTypeId,Name,BillingStreet,BillingCountry,BillingPostalCode, BillingState,BillingCity,ShippingStreet,ShippingCountry,ShippingPostalCode,ShippingState,ShippingCity, ClinicCustomerNo__c,ShipToCustomerNo__c,Business_Email__c,Bill_To_Customer_ID__c from Account];
        
        giic_Test_DataCreationUtility.lstCarrier=[select id,Name,giic_UniqueCarrier__c,gii__NoChargeReason__c from gii__Carrier__c];
        
        giic_Test_DataCreationUtility.lstUnitofMeasure=[select id,Name,gii__Description__c,gii__DisplayName__c,gii__UniqueId__c from gii__UnitofMeasure__c];
        
        giic_Test_DataCreationUtility.lstProd=[SELECT Id, gii__LotControlled__c, gii__SellingUnitofMeasure__c, gii__SellingUnitofMeasure__r.Name,  gii__StockingUnitofMeasure__c, gii__StockingUnitofMeasure__r.name, giic_ProductSKU__c, gii__ProductReference__c, gii__ProductReference__r.Name, gii__NonStock__c, gii__ProductReference__r.ProductCode, gii__ProductReference__r.Family, gii__ProductReference__r.Product_Category__c, gii__ProductReference__r.SKU__c, gii__ProductReference__r.Food_Type__c, gii__ProductReference__r.Weight_Unit__c,gii__Style__c, gii__ProductReference__r.Weight__c from gii__Product2Add__c];
        
        List<Product2> lstProducts=[select id,Name,IsActive,ProductCode,Family,Product_Category__c,SKU__c,Food_Type__c,Weight_Unit__c,Weight__c,giic_SellingUnitofMeasure__c, giic_StockingUnitofMeasure__c from Product2];
        
    }
    private static testMethod void positivetest() {
        User oUser = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        querySetupData();
        System.runAs(oUser){
            Test.startTest();
            
            
            List<gii__PriceBook__c> lstPriBook = [select id from gii__PriceBook__c where Name = 'Standard'];
            gii__PriceBook__c objPB;
            if(lstPriBook == null || lstPriBook.size() == 0){
                objPB = new gii__PriceBook__c();
                objPB.Name = 'Standard';
                insert objPB;
            }else{
                objPB = lstPriBook[0];
            }
            // create gii__PriceBookEntry__c for all products
            
            map<Id, gii__PriceBookEntry__c> mapPriceBook = new map<Id, gii__PriceBookEntry__c>([select id, gii__Product__c, gii__PriceBook__c, gii__UnitPrice__c from  gii__PriceBookEntry__c]);
            
            map<string, gii__PriceBookEntry__c> mapProductsHaingPricebook = new map<string, gii__PriceBookEntry__c>();
            
            for(String str: mapPriceBook.keySet()){
                mapProductsHaingPricebook.put(mapPriceBook.get(str).gii__Product__c,mapPriceBook.get(str));
            }
            
            List<gii__Product2Add__c> lstProd = new List<gii__Product2Add__c>();
            lstProd = [SELECT Id, gii__LotControlled__c, giic_ProductSKU__c, gii__ProductReference__c, gii__ProductReference__r.Name, gii__NonStock__c, gii__ProductReference__r.ProductCode, gii__ProductReference__r.Family,gii__ProductReference__r.Product_Category__c, gii__ProductReference__r.SKU__c, gii__ProductReference__r.Food_Type__c,gii__ProductReference__r.Weight_Unit__c,gii__Style__c, gii__ProductReference__r.Weight__c from gii__Product2Add__c ];
            List<gii__PriceBookEntry__c> lstPB  = new List<gii__PriceBookEntry__c>();
            if(lstProd != null && lstProd.size() > 0 ){
                for(gii__Product2Add__c oProdRef : lstProd ){
                    if(mapProductsHaingPricebook != null && ! mapProductsHaingPricebook.containsKey(oProdRef.Id) ){
                        gii__PriceBookEntry__c ObjPriceBookEntry = new gii__PriceBookEntry__c();
                        ObjPriceBookEntry.gii__Product__c= oProdRef.Id;
                        ObjPriceBookEntry.gii__PriceBook__c = objPB.id;
                        ObjPriceBookEntry.gii__UnitPrice__c = 5;
                        lstPB.Add(ObjPriceBookEntry); 
                    }
                }
            }
            
            if(lstPB != null && lstPB.size() > 0 ){
                insert lstPB;
            }
            
            map<Id, gii__PriceBookEntry__c> mapPriceBook11 = new map<Id, gii__PriceBookEntry__c>([select id, gii__Product__c, gii__PriceBook__c, gii__UnitPrice__c from  gii__PriceBookEntry__c]);
            
            gii__SalesOrderStaging__c objStaging_WithNoChild = new gii__SalesOrderStaging__c(giic_ShipToName__c = 'Pet Supply', giic_ShipToZipPostalCode__c = '08083', giic_ShipToStateProvince__c = 'NJ',giic_Carrier__c = giic_Test_DataCreationUtility.lstCarrier[0].giic_UniqueCarrier__c,  giic_CustEmail__c = 'customer@gmail.com', giic_PaymentTerms__c = '30 Days ', giic_OrderDate__c = System.today(), giic_ShipToCity__c = 'LENEXA', giic_OrderOrigin__c = 'Web', giic_OrderNumber__c = 'NewSOS563111', giic_WarehouseCode__c = '901', giic_PromotionCalculated__c = false, giic_ShipToStreet__c = '8 Clement Drive', giic_ExtDocNo__c = '856562', giic_AddressValidated__c = true, giic_TaxCalculated__c = false, giic_BillToCustID__c = '5567', giic_Comments__c = 'comment for shipping', giic_CCAuthorized__c = false, giic_BillingStateProvince__c = 'NY', giic_BillingStreet__c = '11011 LACKMAN RD', giic_AddressType__c = '', giic_ShippingCounty__c = 'Johnson', giic_BillingZipPostalCode__c = '10134', giic_BillingCity__c = 'LENEXA', giic_Source__c = 'OLP', giic_PaymentMethod__c = 'Credit Card', giic_BillingCounty__c = 'Johnson', giic_CustPhone__c = '567890', giic_ShipToCountry__c = 'USA', giic_CustomerPODate__c = System.today(), giic_Status__c = 'Draft', giic_Account__c = giic_Test_DataCreationUtility.lstAccount[0].ShipToCustomerNo__c, giic_ExecuteTrigger__c = true);

            insert objStaging_WithNoChild;

            giic_Test_DataCreationUtility.insertSalesOrderStaging();
            giic_Test_DataCreationUtility.insertSalesOrderLineStaging();
            
            if(giic_Test_DataCreationUtility.lstSOStaging != null && giic_Test_DataCreationUtility.lstSOStaging.size() >0 ){
                for(gii__SalesOrderStaging__c oSOS1 : giic_Test_DataCreationUtility.lstSOStaging){
                    oSOS1.giic_ExecuteTrigger__c = true;
                }
                update giic_Test_DataCreationUtility.lstSOStaging;
                system.assertEquals(giic_Test_DataCreationUtility.lstSOStaging.size() >0,true);
            }
            
            
            String sObjectApiName = 'gii__SalesOrderStaging__c';
            Map<String, giic_IntegrationCustomMetaDataWrapper> mapIntegrationSettings = giic_IntegrationCustomMetaDataUtil.getIntegrationSettings(sObjectApiName);
            System.assertEquals(mapIntegrationSettings.size()>0,true);
            System.assertEquals(sObjectApiName,'gii__SalesOrderStaging__c');
            
            giic_IntegrationCustomMetaDataWrapper cmdObj = mapIntegrationSettings.get(sObjectApiName); 
            string soql = 'Select ' + cmdObj.strSoql.subString(0, cmdObj.strSoql.length()-1) + ' from ' + cmdObj.sourceSobjectApi + ' where giic_Status__c =  \''+ System.Label.giic_Draft + '\'' ;
            soql+= ' and giic_ExecuteTrigger__c = true'; 
            
            
            List<gii__SalesOrderStaging__c> listOfSOstaging = Database.query(soql);
            
            List<gii__SalesOrderStaging__c> listOfSOstaging1 = [select id , giic_ExecuteTrigger__c, giic_Status__c from gii__SalesOrderStaging__c where id in : listOfSOstaging ];
            giic_SOStagingToSOConversionBatch batch = new giic_SOStagingToSOConversionBatch(mapIntegrationSettings, sObjectApiName);
            Database.executeBatch(batch);
            
            
            
            List<gii__SalesOrder__c> lstSalesOrder1 = [select id, name, gii__DropShip__c, giic_DropShipWHMapping__c, gii__Warehouse__c, gii__ShipToZipPostalCode__c, gii__AccountReference__r.giic_WarehouseGroup__c, giic_Source__c from gii__SalesOrder__c];
            
            List<gii__SalesOrder__c> lstSalesOrder2 = [select id, name, gii__DropShip__c, giic_DropShipWHMapping__c, gii__Warehouse__c, gii__ShipToZipPostalCode__c, gii__AccountReference__r.giic_WarehouseGroup__c, giic_Source__c from gii__SalesOrder__c where gii__ShipToZipPostalCode__c != null AND gii__AccountReference__c != null AND gii__AccountReference__r.giic_WarehouseGroup__c != null AND ((giic_Source__c = 'INSITE' AND gii__DropShip__c = true) OR giic_Source__c = 'OLP')];
            
            Test.stopTest();
        }
    }
    
    /*
    private static testMethod void negativetest() {
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        querySetupData();
        System.runAs(u){
            Test.startTest();
            
            String sObjectApiName = 'gii__SalesOrderStaging__c';
            Map<String, giic_IntegrationCustomMetaDataWrapper> mapIntegrationSettings = giic_IntegrationCustomMetaDataUtil.getIntegrationSettings(sObjectApiName);
            
            giic_SOStagingToSOConversionBatch batch = new giic_SOStagingToSOConversionBatch(mapIntegrationSettings, sObjectApiName);
            Database.executeBatch(batch);
            
            Test.stopTest();
        }
    }
    
    */
}