/************************************************************************************
Version : 1.0
Name : giic_TransferOrderWrapper 
Created Date : 29 Oct 2018
Function :  
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/

global class giic_TransferOrderWrapper {
    public list<TransferOrder> TransferOrders;
    public giic_TransferOrderWrapper(){
        TransferOrders = new List<TransferOrder>();
    }
    public class TransferOrder{
        public string TO_FromWarehouse;
        public string TO_ToWarehouse;
        public string TO_ShipToCustomerNumber; 
        public string TO_Carrier;
        public date TO_OrderDate;
        public date TO_RequiredDate;
        public string TO_ShipToCity;
        public string TO_ShipToState;
        public string TO_ShipToCountry;
        public string TO_ShipToStreet;       
        public string TO_ShipToName;    
        public string TO_OrderNumber;    
        public string TO_ShipToZipCode;
        public list<TransferOrderLine> TransferOrderLines;
    }
    public class TransferOrderLine{
        public string TOL_FromWarehouseCode;
        public string TOL_OrderLineNumber;
        public string TOL_ProductCode;
        public decimal TOL_Quantity;
        public decimal TOL_ShippedQuantity;
        public string TOL_StockUOM;
        public boolean TOL_IsNonStock;
        public decimal TOL_NonStockQuantity;       
    }
    
    global class TransferOrderResult{
        public string Code;
        public string Status;
        public List<string> ListOfErrors;   
        
    }
    
    
    
    public list<TransferOrderReceipt> TransferOrderReceipts;
    
    global class TransferOrderReceipt{
        public string TOR_OrderNumber;  
        public string TOR_FromWarehouse;
        public string TOR_ToWarehouse;
        public string TOR_RequiredDate;
        Public String Error;
        Public String code;
        Public String Status = giic_Constants.STR_SUCCESS;
        public list<TransferOrderRecieptLine> TransferOrderReceiptLines;
        
        public TransferOrderReceipt(){}
        public TransferOrderReceipt(String Status, String Error)
        {
            this.Status =Status;
            this.Error =Error;
        }
        
    }
     public class TransferOrderRecieptLine{
        public string TORL_OrderLineNumber;
        public string TORL_ProductCode;
        public decimal TORL_ReceiptQuantity;
        public decimal TORL_ShippedQuantity;
        public string TORL_StockUOM;
        public string TORL_ToWarehouseCode;
     }
}