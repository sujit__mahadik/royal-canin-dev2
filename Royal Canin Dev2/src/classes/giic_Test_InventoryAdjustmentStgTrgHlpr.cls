/************************************************************************************
Version : 1.0
Created Date : 10 Oct 2018
Function : giic_Test_InventoryAdjustmentStagingTrgHelper is test class for class giic_InventoryAdjustmentStagingTrgHelper, trigger giic_InventoryAdjustmentStagingTrigger
Modification Log : NA
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/

@isTest(seeAllData=false)
private class giic_Test_InventoryAdjustmentStgTrgHlpr  {
    public static User objTestClassUser;
    @testSetup static void setupData(){
        // insert default warehouse
        List<gii__Warehouse__c> defaultWHList = giic_Test_DataCreationUtility.insertWarehouse();
        //system.debug('defaultWHList--->>> ' + defaultWHList);
        // create system policy
        giic_Test_DataCreationUtility.testSystemPolicyCreation2();        
        SetupTestData.createCustomSettings();
        // insert 5 warehouse ie. in bluk
        giic_Test_DataCreationUtility.CreateAdminUser();
        List<gii__Warehouse__c> testWareListN = giic_Test_DataCreationUtility.insertWarehouse_N(5);
        List<Account> testConsumerAccountList = giic_Test_DataCreationUtility.insertConsumerAccount_N(5);
        List<gii__Product2Add__c> testProductList = giic_Test_DataCreationUtility.insertProduct_N(); 
        giic_Test_DataCreationUtility.insertUnitofMeasure(); 
        giic_Test_DataCreationUtility.productUnitofMeasureCreation(); 
    }
    public static void QueryData(){
        objTestClassUser = giic_Test_DataCreationUtility.getTestClassUser();
        giic_Test_DataCreationUtility.lstProdRef_N = [SELECT Id, gii__LotControlled__c, gii__ProductReference__c, gii__DefaultWarehouse__c, gii__ProductReference__r.Name, gii__NonStock__c, gii__ProductReference__r.ProductCode, gii__ProductReference__r.Family, gii__ProductReference__r.Product_Category__c, gii__ProductReference__r.SKU__c, gii__ProductReference__r.Food_Type__c, gii__ProductReference__r.Weight_Unit__c, gii__ProductReference__r.Weight__c, giic_ConvFactor__c, giic_RConvFactor__c, gii__SellingUnitofMeasure__c, gii__SellingUnitofMeasure__r.gii__Description__c, gii__SellingUnitofMeasure__r.gii__DisplayName__c, gii__SellingUnitofMeasure__r.gii__UniqueId__c, gii__StockingUnitofMeasure__c, gii__StockingUnitofMeasure__r.gii__Description__c, gii__StockingUnitofMeasure__r.gii__DisplayName__c, gii__StockingUnitofMeasure__r.gii__UniqueId__c   from gii__Product2Add__c];
        
        giic_Test_DataCreationUtility.lstWarehouse_N = [select id, Name, giic_WarehouseCode__c from gii__Warehouse__c];
        
        for(integer i =0; i< giic_Test_DataCreationUtility.lstProdRef_N.size() ; i++){
            giic_Test_DataCreationUtility.lstProdRef_N[i].giic_ConvFactor__c= 1.0; 
            giic_Test_DataCreationUtility.lstProdRef_N[i].giic_RConvFactor__c = 1.0;
        }
        // update giic_ConvFactor__c and giic_ReverseConversionFactorSellToStock__c in gii__Product2Add__c records
        update giic_Test_DataCreationUtility.lstProdRef_N;
        
        List<gii__UnitofMeasure__c> lstUnitofMeasure = [select Id, name,gii__Description__c,  gii__DisplayName__c from gii__UnitofMeasure__c];
        
        List<gii__ProductUnitofMeasureConversion__c> lstProductUnitofMeasure = [select Id, gii__Product__c,gii__UnitofMeasureIn__c,  gii__UnitofMeasureOut__c,gii__QuantityIn__c,gii__QuantityOut__c   from gii__ProductUnitofMeasureConversion__c];
    }
    public static testMethod void test_CreateNewPIQDrecords(){
        QueryData();
        
        system.assertEquals(objTestClassUser.Alias,system.label.giic_TestUserAlias);
        // this test method covers createNewPIQDrecords() and createProductInventory() Method
        //system.debug('lstWarehouse_N ----->>>>> ' + giic_Test_DataCreationUtility.lstWarehouse_N);
        //system.debug('lstProdRef_N  ----->>>>> ' + giic_Test_DataCreationUtility.lstProdRef_N );
        //system.debug('objTestClassUser  ----->>>>> ' +objTestClassUser);
        
        List<gii__Product2Add__c> ListOfPR = new List<gii__Product2Add__c> ();
        Test.startTest();
        List<gii__InventoryAdjustmentStaging__c> lstIAS = new List<gii__InventoryAdjustmentStaging__c>();
        /*
        for(integer i =0; i< giic_Test_DataCreationUtility.lstProdRef_N.size(); i++){
            system.debug('giic_ConvFactor__c--->>>> ' +  giic_Test_DataCreationUtility.lstProdRef_N[i].giic_ConvFactor__c);
            system.debug('giic_RConvFactor__c--->>>> ' +  giic_Test_DataCreationUtility.lstProdRef_N[i].giic_RConvFactor__c);
            
        }
        */
        System.runAs(objTestClassUser){
            for(integer i =0; i< 5 ; i++){
                gii__InventoryAdjustmentStaging__c objIAS1 = new gii__InventoryAdjustmentStaging__c(giic_status__c = system.label.giic_IAS_InitialStatus,giic_AdjustmentQuantity__c = 10, giic_InvOverride__c = false , giic_Product__c = giic_Test_DataCreationUtility.lstProdRef_N[i].gii__ProductReference__r.SKU__c, giic_Warehouse__c = giic_Test_DataCreationUtility.lstWarehouse_N[0].giic_WarehouseCode__c , giic_UnitofMeasure__c = 'EA');
                
                gii__InventoryAdjustmentStaging__c objIAS2 = new gii__InventoryAdjustmentStaging__c(giic_status__c = system.label.giic_IAS_InitialStatus,giic_AdjustmentQuantity__c = 10, giic_InvOverride__c = false , giic_Product__c = giic_Test_DataCreationUtility.lstProdRef_N[i].gii__ProductReference__r.SKU__c, giic_Warehouse__c = giic_Test_DataCreationUtility.lstWarehouse_N[1].giic_WarehouseCode__c , giic_UnitofMeasure__c = 'Box');

                gii__InventoryAdjustmentStaging__c objIAS3 = new gii__InventoryAdjustmentStaging__c(giic_status__c = system.label.giic_IAS_InitialStatus,giic_AdjustmentQuantity__c = 10, giic_InvOverride__c = true , giic_Product__c = giic_Test_DataCreationUtility.lstProdRef_N[i].gii__ProductReference__r.SKU__c, giic_Warehouse__c = giic_Test_DataCreationUtility.lstWarehouse_N[2].giic_WarehouseCode__c , giic_UnitofMeasure__c = 'Bag');

                gii__InventoryAdjustmentStaging__c objIAS4 = new gii__InventoryAdjustmentStaging__c(giic_status__c = system.label.giic_IAS_InitialStatus,giic_AdjustmentQuantity__c = 10, giic_InvOverride__c = true , giic_Product__c = giic_Test_DataCreationUtility.lstProdRef_N[i].gii__ProductReference__r.SKU__c, giic_Warehouse__c = giic_Test_DataCreationUtility.lstWarehouse_N[3].giic_WarehouseCode__c , giic_UnitofMeasure__c = 'Box');

                gii__InventoryAdjustmentStaging__c objIAS5 = new gii__InventoryAdjustmentStaging__c(giic_status__c = system.label.giic_IAS_InitialStatus,giic_AdjustmentQuantity__c = 10, giic_InvOverride__c = true , giic_Product__c = giic_Test_DataCreationUtility.lstProdRef_N[i].gii__ProductReference__r.SKU__c, giic_Warehouse__c = giic_Test_DataCreationUtility.lstWarehouse_N[4].giic_WarehouseCode__c , giic_UnitofMeasure__c = 'BBag');
                
                gii__InventoryAdjustmentStaging__c objIAS6 = new gii__InventoryAdjustmentStaging__c(giic_status__c = system.label.giic_IAS_InitialStatus,giic_AdjustmentQuantity__c = 10, giic_InvOverride__c = false , giic_Product__c = 'RandomValue',giic_Warehouse__c = giic_Test_DataCreationUtility.lstWarehouse_N[4].giic_WarehouseCode__c , giic_UnitofMeasure__c = 'Bag');
                
                lstIAS.add(objIAS1);
                lstIAS.add(objIAS2);
                lstIAS.add(objIAS3);
                lstIAS.add(objIAS4);
                lstIAS.add(objIAS5);
                lstIAS.add(objIAS6);
            }
            // insert lstIAS to create Inventory Adjustment Staging in bulk and trigger to fire
            insert lstIAS;
            system.assert(lstIAS != null );
        }   
        Test.stopTest();

    }
    
    public static testMethod void test_overrideExitingPIQDrecords(){
        // this test method covers createNewPIQDrecords() and createProductInventory() Method
        QueryData();
        system.assertEquals(objTestClassUser.Alias,'standt');
        giic_Test_DataCreationUtility.insertProductInventoryQTYDetails_N(5);
        List<gii__Product2Add__c> ListOfPR = new List<gii__Product2Add__c> ();
        Test.startTest();
        List<gii__InventoryAdjustmentStaging__c> lstIAS = new List<gii__InventoryAdjustmentStaging__c>();
      
        System.runAs(objTestClassUser){
            gii__InventoryAdjustmentStaging__c objIAS1 = new gii__InventoryAdjustmentStaging__c(giic_status__c = system.label.giic_IAS_InitialStatus, giic_AdjustmentQuantity__c = 10, giic_InvOverride__c = false , giic_Product__c = giic_Test_DataCreationUtility.lstProdRef_N[0].gii__ProductReference__r.SKU__c, giic_Warehouse__c = giic_Test_DataCreationUtility.lstWarehouse_N[0].giic_WarehouseCode__c , giic_UnitofMeasure__c = 'EA');
            
            gii__InventoryAdjustmentStaging__c objIAS2 = new gii__InventoryAdjustmentStaging__c(giic_status__c = system.label.giic_IAS_InitialStatus,giic_AdjustmentQuantity__c = 10, giic_InvOverride__c = false , giic_Product__c = giic_Test_DataCreationUtility.lstProdRef_N[1].gii__ProductReference__r.SKU__c, giic_Warehouse__c = giic_Test_DataCreationUtility.lstWarehouse_N[1].giic_WarehouseCode__c , giic_UnitofMeasure__c = 'Box');

            gii__InventoryAdjustmentStaging__c objIAS3 = new gii__InventoryAdjustmentStaging__c(giic_status__c = system.label.giic_IAS_InitialStatus,giic_AdjustmentQuantity__c = 10, giic_InvOverride__c = true , giic_Product__c = giic_Test_DataCreationUtility.lstProdRef_N[2].gii__ProductReference__r.SKU__c, giic_Warehouse__c = giic_Test_DataCreationUtility.lstWarehouse_N[2].giic_WarehouseCode__c , giic_UnitofMeasure__c = 'Bag');

            gii__InventoryAdjustmentStaging__c objIAS4 = new gii__InventoryAdjustmentStaging__c(giic_status__c = system.label.giic_IAS_InitialStatus,giic_AdjustmentQuantity__c = 10, giic_InvOverride__c = true , giic_Product__c = giic_Test_DataCreationUtility.lstProdRef_N[3].gii__ProductReference__r.SKU__c, giic_Warehouse__c = giic_Test_DataCreationUtility.lstWarehouse_N[3].giic_WarehouseCode__c , giic_UnitofMeasure__c = 'Box');

            gii__InventoryAdjustmentStaging__c objIAS5 = new gii__InventoryAdjustmentStaging__c(giic_status__c = system.label.giic_IAS_InitialStatus,giic_AdjustmentQuantity__c = 10, giic_InvOverride__c = true , giic_Product__c = giic_Test_DataCreationUtility.lstProdRef_N[4].gii__ProductReference__r.SKU__c, giic_Warehouse__c = giic_Test_DataCreationUtility.lstWarehouse_N[4].giic_WarehouseCode__c , giic_UnitofMeasure__c = 'BBag');
            
            gii__InventoryAdjustmentStaging__c objIAS6 = new gii__InventoryAdjustmentStaging__c(giic_status__c = system.label.giic_IAS_InitialStatus,giic_AdjustmentQuantity__c = 10, giic_InvOverride__c = true , giic_Product__c = 'RandomValue', giic_Warehouse__c = giic_Test_DataCreationUtility.lstWarehouse_N[4].giic_WarehouseCode__c , giic_UnitofMeasure__c = 'Bag');
            
            lstIAS.add(objIAS1);
            lstIAS.add(objIAS2);
            lstIAS.add(objIAS3);
            lstIAS.add(objIAS4);
            lstIAS.add(objIAS5);
            lstIAS.add(objIAS6);
            // insert lstIAS to create Inventory Adjustment Staging in bulk and trigger to fire
            insert lstIAS;
        }
        
        Test.stopTest();

    }
    
    public static testMethod void test_NegativeScenario(){
        // this test method covers createNewPIQDrecords() and createProductInventory() Method
        QueryData();
        system.assertEquals(objTestClassUser.Alias,'standt');
        giic_Test_DataCreationUtility.insertProductInventoryQTYDetails_N(5);
        Test.startTest();
        List<gii__InventoryAdjustmentStaging__c> lstIAS = new List<gii__InventoryAdjustmentStaging__c>();
      
        System.runAs(objTestClassUser){
            // gii__InventoryAdjustmentStaging__c objIAS1 = new gii__InventoryAdjustmentStaging__c(giic_status__c = system.label.giic_IAS_InitialStatus, giic_AdjustmentQuantity__c = 10, giic_InvOverride__c = false , giic_Product__c = giic_Test_DataCreationUtility.lstProdRef_N[0].gii__ProductReference__r.SKU__c, giic_Warehouse__c = giic_Test_DataCreationUtility.lstWarehouse_N[0].giic_WarehouseCode__c , giic_UnitofMeasure__c = 'EA');
            
            gii__InventoryAdjustmentStaging__c objIAS1 = new gii__InventoryAdjustmentStaging__c(giic_status__c = system.label.giic_IAS_InitialStatus, giic_AdjustmentQuantity__c = 10, giic_InvOverride__c = false );
            /*
            gii__InventoryAdjustmentStaging__c objIAS2 = new gii__InventoryAdjustmentStaging__c(giic_status__c = system.label.giic_IAS_InitialStatus,giic_AdjustmentQuantity__c = 10, giic_InvOverride__c = false , giic_Product__c = giic_Test_DataCreationUtility.lstProdRef_N[1].gii__ProductReference__r.SKU__c, giic_Warehouse__c = giic_Test_DataCreationUtility.lstWarehouse_N[1].giic_WarehouseCode__c , giic_UnitofMeasure__c = 'Box');

            gii__InventoryAdjustmentStaging__c objIAS3 = new gii__InventoryAdjustmentStaging__c(giic_status__c = system.label.giic_IAS_InitialStatus,giic_AdjustmentQuantity__c = 10, giic_InvOverride__c = true , giic_Product__c = giic_Test_DataCreationUtility.lstProdRef_N[2].gii__ProductReference__r.SKU__c, giic_Warehouse__c = giic_Test_DataCreationUtility.lstWarehouse_N[2].giic_WarehouseCode__c , giic_UnitofMeasure__c = 'Bag');

            gii__InventoryAdjustmentStaging__c objIAS4 = new gii__InventoryAdjustmentStaging__c(giic_status__c = system.label.giic_IAS_InitialStatus,giic_AdjustmentQuantity__c = 10, giic_InvOverride__c = true , giic_Product__c = giic_Test_DataCreationUtility.lstProdRef_N[3].gii__ProductReference__r.SKU__c, giic_Warehouse__c = giic_Test_DataCreationUtility.lstWarehouse_N[3].giic_WarehouseCode__c , giic_UnitofMeasure__c = 'Box');

            gii__InventoryAdjustmentStaging__c objIAS5 = new gii__InventoryAdjustmentStaging__c(giic_status__c = system.label.giic_IAS_InitialStatus,giic_AdjustmentQuantity__c = 10, giic_InvOverride__c = true , giic_Product__c = giic_Test_DataCreationUtility.lstProdRef_N[4].gii__ProductReference__r.SKU__c, giic_Warehouse__c = giic_Test_DataCreationUtility.lstWarehouse_N[4].giic_WarehouseCode__c , giic_UnitofMeasure__c = 'BBag');
            
            gii__InventoryAdjustmentStaging__c objIAS6 = new gii__InventoryAdjustmentStaging__c(giic_status__c = system.label.giic_IAS_InitialStatus,giic_AdjustmentQuantity__c = 10, giic_InvOverride__c = true , giic_Product__c = 'RandomValue', giic_Warehouse__c = giic_Test_DataCreationUtility.lstWarehouse_N[4].giic_WarehouseCode__c , giic_UnitofMeasure__c = 'Bag');
            
            
            lstIAS.add(objIAS2);
            lstIAS.add(objIAS3);
            lstIAS.add(objIAS4);
            lstIAS.add(objIAS5);
            lstIAS.add(objIAS6);
            */
            lstIAS.add(objIAS1);
            // insert lstIAS to create Inventory Adjustment Staging in bulk and trigger to fire
            insert lstIAS;
        }
        
        Test.stopTest();

    }
    
}