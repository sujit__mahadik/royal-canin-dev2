/************************************************************************************
Version : 1.0
Name : giic_Test_SOStagingDetailController
Created Date : 10 Sep 2018
Function : test class for giic_SalesOrderStagingDetailController
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/
@isTest(SeeAllData = false)
private class giic_Test_SOStagingDetailController { 

    @testSetup   
    static void setup()
    {
        //Insert warehouse
        giic_Test_DataCreationUtility.insertWarehouse();
        //Create System policy record
        giic_Test_DataCreationUtility.testSystemPolicyCreation();
        //Insert Global param
        giic_Test_DataCreationUtility.insertGlobalParametersCustomSetting();
        //Insert Consumer Accounts
        giic_Test_DataCreationUtility.insertConsumerAccount();
        //Insert warehouse
        giic_Test_DataCreationUtility.insertWarehouse_N(2);
        //Insert Account Reference
        giic_Test_DataCreationUtility.getAccountReference_N(giic_Test_DataCreationUtility.lstAccount);
        //Add products
        giic_Test_DataCreationUtility.insertProduct();
        giic_Test_DataCreationUtility.createCarrier();
        //Add Multiple Sales Order and Sales Order Lines
        giic_Test_DataCreationUtility.insertBulkSalesOrder();
        giic_Test_DataCreationUtility.insertBulkSOLine();
        //Create promotions
        giic_Test_DataCreationUtility.createPromotions();
        giic_Test_DataCreationUtility.createPromotionLines();
        giic_Test_DataCreationUtility.createPromotionLineStyle();
        giic_Test_DataCreationUtility.createAdditionalCharge();
        //Create user record
        giic_Test_DataCreationUtility.CreateAdminUser();
        giic_Test_DataCreationUtility.insertSalesOrderStaging();
        giic_Test_DataCreationUtility.insertSalesOrderLineStaging();
        //Create Account Program
        //giic_Test_DataCreationUtility.createAccProg();
        giic_Test_DataCreationUtility.createRateTable();
        

    }

    private static testMethod void positivetest() {
        
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        
        System.Runas(u)        
        {            
            List<gii__SalesOrderStaging__c> lstSOStaging = [Select id,giic_AddressValidated__c,
                                                            	   giic_ShipToZipPostalCode__c,giic_Source__c,giic_Account__c
                                                            	  from gii__SalesOrderStaging__c];
            lstSOStaging[1].giic_AddressValidated__c = true;            
            update lstSOStaging[1];
            
            Test.startTest();
            Test.setMock(WebServiceMock.class, new giic_Test_FedExCalloutMock());
           
            PageReference pageRef = Page.giic_SalesOrderStagingDetail;
            Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('Id', String.valueOf(lstSOStaging[1].Id));
            ApexPages.StandardController sc = new ApexPages.StandardController(lstSOStaging[1]);
            giic_SalesOrderStagingDetailController objSODetail = new giic_SalesOrderStagingDetailController(sc);
            objSODetail.onLoad();
            List<gii__SalesOrder__c> lstSO = [select id, name,giic_Source__c, gii__DropShip__c, giic_DropShipWHMapping__c, gii__Warehouse__c, 
                                              gii__ShipToZipPostalCode__c,gii__AccountReference__c, gii__AccountReference__r.gii__Account__c,gii__Account__c,
                                              gii__AccountReference__r.giic_WarehouseGroup__c from gii__SalesOrder__c where giic_Source__c = 'OLP' limit 1];
           
            objSODetail.goBack();
            Test.stopTest();
        }
    }
    
    private static testMethod void positivetestwithError() {
        
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        System.Runas(u)        
        {
            List<gii__SalesOrderStaging__c> lstSOStaging = [Select id,giic_Status__c,giic_Source__c,giic_AddressValidated__c,giic_ShipToName__c from gii__SalesOrderStaging__c]; 
            gii__SalesOrderStaging__c objStaging = lstSOStaging[0];
            objStaging.giic_Status__c = 'Draft';
            objStaging.giic_AddressValidated__c = false;
            objStaging.giic_Source__c = 'INSITE';
            objStaging.giic_ShipToName__c = 'Test123';
            update objStaging;
            
            Test.startTest();
            Test.setMock(WebServiceMock.class, new giic_Test_FedExCalloutMock());
            
            PageReference pageRef = Page.giic_SalesOrderStagingDetail;
            Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('Id', String.valueOf(objStaging.Id));
            ApexPages.StandardController sc = new ApexPages.StandardController(objStaging);
            giic_SalesOrderStagingDetailController objSODetail = new giic_SalesOrderStagingDetailController(sc);
            objSODetail.onLoad();
            Test.stopTest();
        }
    }     
    
    private static testMethod void negativetestwithConverted() {
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        
        System.Runas(u)        
        {
            Test.startTest();
            Test.setMock(WebServiceMock.class, new giic_Test_FedExCalloutMock());
            List<gii__SalesOrderStaging__c> lstSOStaging = [Select id,giic_Status__c,giic_Source__c,giic_ShipToName__c,giic_Account__c from gii__SalesOrderStaging__c]; 
            gii__SalesOrderStaging__c objStaging = lstSOStaging[0];
            objStaging.giic_Status__c = 'Converted';
            objStaging.giic_Source__c = 'INSITE';
            objStaging.giic_ShipToName__c = 'Test';
            update objStaging;
            
            PageReference pageRef = Page.giic_SalesOrderStagingDetail;
            Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('Id', String.valueOf(objStaging.Id));
            ApexPages.StandardController sc = new ApexPages.StandardController(objStaging);
            giic_SalesOrderStagingDetailController objSODetail = new giic_SalesOrderStagingDetailController(sc);
            objSODetail.onLoad(); 
            Test.stopTest();   
        }
    }
    
    private static testMethod void negativetestCatch() {
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        
        System.Runas(u)        
        {
            Test.startTest();
            Test.setMock(WebServiceMock.class, new giic_Test_FedExCalloutMock());
            List<gii__SalesOrderStaging__c> lstSOStaging = [Select id,giic_Status__c,giic_Source__c,giic_ShipToName__c,giic_Account__c from gii__SalesOrderStaging__c]; 
            gii__SalesOrderStaging__c objStaging = lstSOStaging[0];
            objStaging.giic_Status__c = 'Draft';
            objStaging.giic_Source__c = 'INSITE';
            objStaging.giic_ShipToName__c = 'Test';
            update objStaging;
            
            PageReference pageRef = Page.giic_SalesOrderStagingDetail;
            Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('Id', String.valueOf(objStaging.Id));
            ApexPages.StandardController sc = new ApexPages.StandardController(objStaging);
            giic_SalesOrderStagingDetailController objSODetail = new giic_SalesOrderStagingDetailController(sc);
            objSODetail.sObjectApiName = 'gii__SalesOrderStagingLine__c';
            objSODetail.onLoad();
            Test.stopTest();
        }
    }

}