/*
 * Date:       		 - Developer, Company					       - Description
 * 2015-04-28        - Solutions Accelerator, Acumen Solutions         - Created
 * 2015-04-29        - Mayank Srivasrava, Acumen Solutions             - Reworked
 */

public with sharing class AccountTriggerUtilities {
    public AccountTriggerUtilities() {
        
    }

    public static void setPostalCodeLookup(List<Account> newAccounts){
        Map<String, List<Account>> shippingPostalCodeToAccount = new map<String, List<Account>>();
        List<String> allZips = new List<String>();
        Map<String, Id> allZipIds = new map<String, Id>();
        List<Registration_Record_Type_Mapping__c> customerTypeMapping = new List<Registration_Record_Type_Mapping__c>();
        Map<String,String> customerTypeToPillar = new Map<String,String>();

        for(Registration_Record_Type_Mapping__c regRecord : customerTypeMapping){
            customerTypeToPillar.put(regRecord.Customer_Type__c,regRecord.Pillar__c);
        }

        Map<String,String> accountTypeToPostalCodePillar = new Map<String,String>{'Influencer'=>'Influencer','Vet'=>'Vet','Retail'=>'Retail'};

        Map<String,String> accoutCustomnerTypeToPostalPillar = new Map<String,String>();

        Map<String,Registration_Record_Type_Mapping__c> registrationRecordTypeMapping = Registration_Record_Type_Mapping__c.getAll();

        for(Registration_Record_Type_Mapping__c regMap : registrationRecordTypeMapping.values()){
            accoutCustomnerTypeToPostalPillar.put(regMap.Customer_Type__c,regMap.Pillar__c);
        }
        System.debug('accoutCustomnerTypeToPostalPillar' +accoutCustomnerTypeToPostalPillar);

        for(Account acct : newAccounts){
           if(!shippingPostalCodeToAccount.ContainsKey(acct.ShippingPostalCode)){
                shippingPostalCodeToAccount.put(acct.ShippingPostalCode,new List<Account>());    
            }
             (shippingPostalCodeToAccount.get(acct.ShippingPostalCode)).add(acct);
              if(acct.ShippingPostalCode != null){
                    allZips.add(acct.ShippingPostalCode);
              }
        }
        
        if(allZips.size() == 0){
            return;
        }
        List<PostalCode__c> allPostalCodes = [select id, name, businessId__c,Pillar__c, Channel__c, ZipCode__c from PostalCode__c where ZipCode__c in : allZips];
        for(PostalCode__c pc : allPostalCodes){
            allZipIds.put(pc.ZipCode__c+pc.Channel__c, pc.Id);
        }
        System.debug('allZipIds '+allZipIds);

        for(String key : shippingPostalCodeToAccount.keySet()){
            for(Account acct : shippingPostalCodeToAccount.get(key)){
                if(accoutCustomnerTypeToPostalPillar.get(acct.Customer_Type__c) != null 
                    && accountTypeToPostalCodePillar.get(accoutCustomnerTypeToPostalPillar.get(acct.Customer_Type__c))!= null){
                    acct.PostalCodeID__c = allZipIds.get(key+accountTypeToPostalCodePillar.get(accoutCustomnerTypeToPostalPillar.get(acct.Customer_Type__c)));

                }
                
            }
        }
    }  
}