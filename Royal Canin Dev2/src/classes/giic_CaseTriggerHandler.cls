/************************************************************************************
Version : 1.0
Name : giic_CaseTriggerHandler
Created Date : 3 Dec 2018
Function : Handler for Case trigger based on context.
Author : Abhishek Tripathi
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/
public class giic_CaseTriggerHandler {
    
    /***********************************************************
* Method name : OnAfterUpdate
* Description : to handle After Update events of trigger
* Return Type : void
* Parameter : List of Case records
***********************************************************/
    public void OnAfterUpdate(List<Case> lstTriggerNew)
    {        
        
        Set<String> rejectedCases = new Set<String>();
        Set<String> approvedCases = new Set<String>();
        for(Case c :lstTriggerNew){
            if(c.Approval_Status__c!= null && c.Approval_Status__c == giic_Constants.APPROVED_STATUS) approvedCases.add(c.Id);
            else if(c.Status != null && c.Status == giic_Constants.REJECTED_STATUS) rejectedCases.add(c.Id);
        } 
        
        if(!rejectedCases.isEmpty()) giic_ReturnCreditsHelper.deleteARCredit(rejectedCases);
        
        if(!approvedCases.isEmpty())
        {
            Map<id, gii__ARCredit__c> arCreditMap = new Map<id, gii__ARCredit__c>([select Id, gii_Case__c, gii_Case__r.giic_TotalReturnAmount__c, gii__Invoice__c,
                                                                                   gii__Invoice__r.giic_WarehouseShippingOrderStaging__r.Name,
                                                                                   gii__SalesOrder__r.Name,
                                                                                   gii__BillingName__c, gii__BillingStateProvince__c,
                                                                                   gii__BillingStreet__c, gii__BillingZipPostalCode__c, gii__BillingCountry__c, gii__Invoice__r.name,
                                                                                   gii__BillingCity__c, gii__NetAmount__c, gii__TotalPaidAmount__c, gii__Account__r.Bill_To_Customer_ID__c, gii__Account__r.ShipToCustomerNo__c 
                                                                                   from gii__ARCredit__c where gii_Case__c in :approvedCases]);
                    if(!arCreditMap.isEmpty()){
                        Set<String> invIds = new Set<String>();
                        for(gii__ARCredit__c ar :arCreditMap.values())
                        {
                            invIds.add(ar.gii__Invoice__c); 
                        }
                        
                        // Fetching gii__ARInvoicePayment__c and already paid gii__ARCreditPayment__c records against invoices.
                        List<AggregateResult> lstAgARCPayment = [Select sum(gii__PaidAmount__c)totalPaid, gii__ARCredit__r.gii__Invoice__c inv, gii__PaymentMethod__c payM from gii__ARCreditPayment__c  where gii__ARCredit__r.gii__Invoice__c in : invIds group by gii__ARCredit__r.gii__Invoice__c, gii__PaymentMethod__c];
                        List<AggregateResult> lstAgARIPayment = [Select sum(gii__PaidAmount__c)totalPaid, gii__Invoice__c inv, gii__PaymentMethod__c payM From gii__ARInvoicePayment__c where gii__Invoice__c in : invIds group by gii__Invoice__c,gii__PaymentMethod__c];
                        
                        Map<id, Decimal> mapARCPaymentAmnt = new Map<id, Decimal>(); // Map to put total amount of aggragted AR credit payment against Invoice
                        Map<id, Map<string, Decimal>> mapInvPayMamountMapARC = new Map<id, Map<string, Decimal>>(); // Map of InvoiceId against grouped Amount by payment method for ARC
                        if(!lstAgARCPayment.isEmpty()){
                            for(AggregateResult agR : lstAgARCPayment){
                                if(mapInvPayMamountMapARC.containsKey((String)agR.get('inv'))){
                                    Map<string, Decimal> tempMap = mapInvPayMamountMapARC.get((String)agR.get('inv'));
                                    tempMap.put((String)agR.get('payM'), (Decimal)agR.get('totalPaid'));
                                    mapInvPayMamountMapARC.put((String)agR.get('inv'), tempMap);
                                }else{
                                    mapInvPayMamountMapARC.put((String)agR.get('inv'), new Map<string, Decimal>{(String)agR.get('payM') => (Decimal)agR.get('totalPaid')});
                                }
                                if(mapARCPaymentAmnt.containsKey((String)agR.get('inv'))){
                                    Decimal temp = mapARCPaymentAmnt.get((String)agR.get('inv')) + (Decimal)agR.get('totalPaid');
                                    mapARCPaymentAmnt.put((String)agR.get('inv'), temp);
                                } else{
                                    mapARCPaymentAmnt.put((String)agR.get('inv'), (Decimal)agR.get('totalPaid')); 
                                }
                                
                            }
                        }
                        Map<id, Decimal> mapARIPaymentAmnt = new Map<id, Decimal>(); // Map to put total amount of aggragted AR Invoice payment against Invoice
                        Map<id, Map<string, Decimal>> mapInvPayMamountMapARI = new Map<id, Map<string, Decimal>>(); // Map of InvoiceId against grouped Amount by payment method for ARI
                        if(!lstAgARIPayment.isEmpty()){
                        for(AggregateResult agR : lstAgARIPayment){
                                if(mapInvPayMamountMapARI.containsKey((String)agR.get('inv'))){
                                    Map<string, Decimal> tempMap = mapInvPayMamountMapARI.get((String)agR.get('inv'));
                                    tempMap.put((String)agR.get('payM'), (Decimal)agR.get('totalPaid'));
                                    mapInvPayMamountMapARI.put((String)agR.get('inv'), tempMap);
                                }else{
                                    mapInvPayMamountMapARI.put((String)agR.get('inv'), new Map<string, Decimal>{(String)agR.get('payM') => (Decimal)agR.get('totalPaid')});
                                }
                                if(mapARIPaymentAmnt.containsKey((String)agR.get('inv'))){
                                    Decimal temp = mapARIPaymentAmnt.get((String)agR.get('inv')) + (Decimal)agR.get('totalPaid');
                                    mapARIPaymentAmnt.put((String)agR.get('inv'), temp);
                                } else{
                                    mapARIPaymentAmnt.put((String)agR.get('inv'), (Decimal)agR.get('totalPaid')); 
                                }
                            }
                        }
            
                        Map<id, decimal> invARCAmountpayMap = new Map<id, decimal>();
                        
                        map<String, Double> lstPaymntSeq = getCreditSequence();
                        list<gii__ARCreditPayment__c> arcPayment = new list<gii__ARCreditPayment__c>();
                        Map<id, String> casesWithErr = new Map<id, String>();
                        
                        for(gii__ARCredit__c arcObj : arCreditMap.values()){
                            if(mapInvPayMamountMapARI.get(arcObj.gii__Invoice__c) == null){
                                casesWithErr.put(arcObj.gii_Case__c, Label.giic_NoPaymentsMade + arcObj.gii__Invoice__r.name);
                                continue;
                            }
            
                            if(mapARCPaymentAmnt.get(arcObj.gii__Invoice__c) == null || (mapARIPaymentAmnt.get(arcObj.gii__Invoice__c) > mapARCPaymentAmnt.get(arcObj.gii__Invoice__c))){
                                Decimal amtToBePaidfrmARC = arcObj.gii__NetAmount__c;
                                Decimal alreadyPaidAmt = (mapARCPaymentAmnt.get(arcObj.gii__Invoice__c) != null)? mapARCPaymentAmnt.get(arcObj.gii__Invoice__c) : 0.00;
                                Decimal iteratorAmount = 0.00;
                                iteratorAmount = alreadyPaidAmt;
                                Boolean currARCStarted = false;
                                if(!lstPaymntSeq.isEmpty()){
                                    for(String method : lstPaymntSeq.keySet()){ // Iterating for sequencing
                                        if(mapInvPayMamountMapARI.get(arcObj.gii__Invoice__c).containsKey(method)){
                                            if(mapInvPayMamountMapARI.get(arcObj.gii__Invoice__c).get(method) >= iteratorAmount){
                                                if(!currARCStarted){
                                                    iteratorAmount = amtToBePaidfrmARC;
                                                    if(iteratorAmount <= (mapInvPayMamountMapARI.get(arcObj.gii__Invoice__c).get(method) - alreadyPaidAmt))
                                                    {
                                                     arcPayment.add(new gii__ARCreditPayment__c(gii__PaymentMethod__c=method, gii__ARCredit__c=arcObj.Id, gii__PaymentDate__c = System.today(), gii__PaidAmount__c = iteratorAmount));
                                                     break;
                                                     
                                                    }else{
                                                          arcPayment.add(new gii__ARCreditPayment__c(gii__PaymentMethod__c=method, gii__ARCredit__c=arcObj.Id, gii__PaymentDate__c = System.today(), gii__PaidAmount__c = (mapInvPayMamountMapARI.get(arcObj.gii__Invoice__c).get(method) - alreadyPaidAmt)));
                                                          iteratorAmount = iteratorAmount - (mapInvPayMamountMapARI.get(arcObj.gii__Invoice__c).get(method) - alreadyPaidAmt);
                                                          currARCStarted = true;
                                                          continue;
                                                         }
                                                }else{
                                                      if(iteratorAmount <= (mapInvPayMamountMapARI.get(arcObj.gii__Invoice__c).get(method)))
                                                      {
                                                       arcPayment.add(new gii__ARCreditPayment__c(gii__PaymentMethod__c=method, gii__ARCredit__c=arcObj.Id, gii__PaymentDate__c = System.today(), gii__PaidAmount__c = (mapInvPayMamountMapARI.get(arcObj.gii__Invoice__c).get(method))));
                                                       break;
                                                       
                                                      }else{
                                                            arcPayment.add(new gii__ARCreditPayment__c(gii__PaymentMethod__c=method, gii__ARCredit__c=arcObj.Id, gii__PaymentDate__c = System.today(), gii__PaidAmount__c = (mapInvPayMamountMapARI.get(arcObj.gii__Invoice__c).get(method))));
                                                            continue;
                                                           }
                                                     }
                                                
                                            } else{
                                                   iteratorAmount = alreadyPaidAmt - mapInvPayMamountMapARI.get(arcObj.gii__Invoice__c).get(method);
                                                   continue;
                                                  }
                                        }
                                    }
                            }
                                
                                
                            } else{
                                casesWithErr.put(arcObj.gii_Case__c, Label.giic_ARCreditExtendsTotalPay + arcObj.gii__Invoice__r.name);
                            }
                            
                        }
                        
                        if(arCreditMap != null && arcPayment != null && arcPayment.size() > 0 && arCreditMap.size() > 0){
                            createARCPayment(arCreditMap, arcPayment); // Call createARCPayment to create ARCPayment records and update ARC records.
                        }
                        
                        for(Case c :lstTriggerNew){
                            if(casesWithErr.containsKey(c.Id)) 
                                c.addError(casesWithErr.get(c.Id));
                        }
                    }
        }
    }
    
    
    /***********************************************************
* Method name : getCreditSequence
* Description : returns map of credit sequence on sorted order
* Return Type : map<String, Double>
* Parameter : None
***********************************************************/
    public map<String, Double> getCreditSequence(){
        List<gii__PaymentMethod__c> lstPaymntM = [SELECT Id, Name, giic_CreditSequence__c, gii__Description__c, giic_PaymentConsumptionSeq__c FROM gii__PaymentMethod__c order by giic_CreditSequence__c ASC];
        map<String, Double> paymentMOrderMap = new map<String, Double>();
        if(!lstPaymntM.isEmpty()){
            for(gii__PaymentMethod__c payMObj : lstPaymntM){
                paymentMOrderMap.put(payMObj.Name, payMObj.giic_CreditSequence__c);
                
            }
        }
        return paymentMOrderMap;
    }
    
    
    /***********************************************************
* Method name : createARCpayRec
* Description : Create AR Credit payment records.
* Return Type : void
* Parameter : Map<id, gii__ARCredit__c> arcMap, List<gii__ARCreditPayment__c> arcPayList
***********************************************************/
    public void createARCPayment(Map<id, gii__ARCredit__c> arcMap, List<gii__ARCreditPayment__c> arcPayList){
        List<gii__ARCreditPayment__c> ccPayList = new List<gii__ARCreditPayment__c>();
        List<gii__ARCreditPayment__c> nonCCPayList = new List<gii__ARCreditPayment__c>();
        Map<id, gii__ARCredit__c> ccARCMap = new Map<id, gii__ARCredit__c>();
        Map<id, gii__ARCredit__c> nonCCARCMap = new Map<id, gii__ARCredit__c>();
        Set<Id> ccInvoiceSet = new Set<Id>();
        
        if(!arcPayList.isEmpty()){
            for(gii__ARCreditPayment__c arcpayObj : arcPayList){
                if(arcpayObj.gii__PaymentMethod__c == giic_Constants.SO_CREDITCARD){
                    ccPayList.add(arcpayObj);
                    if(!ccARCMap.containsKey(arcpayObj.gii__ARCredit__c)){
                        gii__ARCredit__c tempARC = arcMap.get(arcpayObj.gii__ARCredit__c);
                        tempARC.giic_SettlementStatus__c = giic_Constants.CONFIRMED_STATUS;
                        ccARCMap.put(arcpayObj.gii__ARCredit__c, tempARC);
                        ccInvoiceSet.add(arcMap.get(arcpayObj.gii__ARCredit__c).gii__Invoice__c);
                    }
                    // Method to call Queueable.
                }else{
                    nonCCPayList.add(arcpayObj);
                    if(!nonCCARCMap.containsKey(arcpayObj.gii__ARCredit__c)){
                        gii__ARCredit__c tempARC = arcMap.get(arcpayObj.gii__ARCredit__c);
                        tempARC.giic_SettlementStatus__c = giic_Constants.CONFIRMED_STATUS;
                        nonCCARCMap.put(arcpayObj.gii__ARCredit__c, tempARC);
                    }
                }
            }
        }
        
        if(!nonCCPayList.isEmpty()) insert nonCCPayList;
        if(!nonCCARCMap.isEmpty()) update nonCCARCMap.values();
     if(!ccARCMap.isEmpty()){
                ID jobID = System.enqueueJob(new giic_ReturnCreditsQueueable(ccARCMap, ccPayList));
        }
        
        
    }
}