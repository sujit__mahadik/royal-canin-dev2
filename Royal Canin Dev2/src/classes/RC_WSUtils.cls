/* 
 * 2/06/2018       - Bethi Reddy                                 - Edited to change the Orderhistory services end point to support MD phase1 project
 */
 
global with sharing class RC_WSUtils {


global static OrderDetailsWrapper callOrderHistory(string stcn, integer startDirect, integer endDirect, integer startOp, integer endOp){
        OrderDetailsWrapper odw;
        HttpRequest req = new HttpRequest();
        req.setEndpoint(RC_WSUtils.buildUrl(stcn, startDirect, endDirect, startOp, endOp));
        req.setTimeout(119999);
        req.setMethod('GET');

        HTTPResponse res;
        try {
            Http http = new Http();
            res = http.send(req);
            System.debug(res.getBody());
        } catch(Exception ex) {
            
            res = new HttpResponse();
            system.debug('Webservice callout failed.');
        }

        if(res.getStatusCode() != 200) {
            
            system.debug('Webservice status code:' + res.getStatusCode());
        }
        
        try{
            odw = OrderDetailsWrapper.parse(res.getBody());
            system.debug('odw' + odw.OrderHistoryDetails.get(0));
            
        } catch(Exception ex) {
            
            system.debug('Webservice returned invalid response.');
            system.debug('res.getBody()' + res.getBody());
        }
         return odw;
}

public static String buildUrl(String sellTo, Integer startIndexD, Integer endIndexD, Integer startIndexO, Integer endIndexO) {
        string rVal = Global_Parameters__c.getValues('RCOrderDetailAPIEndpoint').Value__c + '/v1/Ancillary/Order/GetOrderHistory/?format=json&sellToCustomerNumber=' + sellTo + '&start=' + startIndexD + '&end=' + endIndexD + '&startoph=' + startIndexO + '&endoph=' + endIndexO + '&client_id=' + Global_Parameters__c.getValues('client_id').Value__c + '&client_secret=' + Global_Parameters__c.getValues('client_secret').Value__c;
        return rVal;
    } 
/*
public static String buildUrl(String sellTo, Integer startIndexD, Integer endIndexD, Integer startIndexO, Integer endIndexO) {
        system.debug('BUILDstartindD:' + startIndexD);
        system.debug('BUILDendindD:' + endIndexD);
        system.debug('BUILDstartindO:' + startIndexO);
        system.debug('BUILDendindO:' + endIndexO);
        //new: http://royalcanin-us-orderhistory-api-test.cloudhub.io/api/orders?sellToCustomerNumber=POS-0000023-001&start=0&end=10&startoph=0&endoph=10
        system.debug('url:' + Global_Parameters__c.getValues('RCOrderDetailAPIEndpoint').Value__c + '/api/orders/?sellToCustomerNumber=' + sellTo + '&start=' + startIndexD + '&end=' + endIndexD + '&startoph=' + startIndexO + '&endoph=' + endIndexO + '&client_id=' + Global_Parameters__c.getValues('client_id').Value__c + '&client_secret=' + Global_Parameters__c.getValues('client_secret').Value__c);
        return Global_Parameters__c.getValues('RCOrderDetailAPIEndpoint').Value__c + '/api/orders/?sellToCustomerNumber=' + sellTo + '&start=' + startIndexD + '&end=' + endIndexD + '&startoph=' + startIndexO + '&endoph=' + endIndexO + '&client_id=' + Global_Parameters__c.getValues('client_id').Value__c + '&client_secret=' + Global_Parameters__c.getValues('client_secret').Value__c;
    } */
}