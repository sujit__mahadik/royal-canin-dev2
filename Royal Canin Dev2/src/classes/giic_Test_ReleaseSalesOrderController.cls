/************************************************************************************
Version : 1.0
Created Date : 20 Sep 2018
Function : Tests life cycle of Sales Order in OMS and hence batch performances.
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/
@isTest(SeeAllData = false)
public class giic_Test_ReleaseSalesOrderController {

    @testSetup
    static void setup(){
        List<gii__Warehouse__c> defaultWHList = giic_Test_DataCreationUtility.insertWarehouse();
        giic_Test_DataCreationUtility.testSystemPolicyCreation();
        giic_Test_DataCreationUtility.testSystemPolicyCreation2();
        giic_Test_DataCreationUtility.createAdditionalCharge();
        giic_Test_DataCreationUtility.CreateAdminUser();
        SetupTestData.createCustomSettings();
        List<gii__Warehouse__c> testWareListN = giic_Test_DataCreationUtility.insertWarehouse_N(5); // lstWarehouse_N
        list<giic_CommonUtility.WarehouseDistance> lstWDistance = new list<giic_CommonUtility.WarehouseDistance>();
        giic_CommonUtility.WarehouseDistance objWD;
        integer intDistence = 300;
        for(gii__Warehouse__c objW :  giic_Test_DataCreationUtility.lstWarehouse_N){
            objWD = new giic_CommonUtility.WarehouseDistance(objW.Id, intDistence);
            intDistence = intDistence + 20;
            lstWDistance.add(objWD);
        }
        intDistence = intDistence + 55;
        objWD = new giic_CommonUtility.WarehouseDistance(defaultWHList[0].Id, intDistence);
        lstWDistance.add(objWD);
        string sWarehouseDistanceMapping = JSON.serialize(lstWDistance);
        List<Account> testConsumerAccountList = giic_Test_DataCreationUtility.insertConsumerAccount_N(5);
        for(Account OAc : giic_Test_DataCreationUtility.lstAccount_N ){
            OAc.giic_WarehouseDistanceMapping__c = sWarehouseDistanceMapping;
        }
        update giic_Test_DataCreationUtility.lstAccount_N;
        List<gii__Product2Add__c> testProductList = giic_Test_DataCreationUtility.insertProduct_N(); 
        giic_Test_DataCreationUtility.createCarrier(); 
        
        integer intCount = 0;
        for(gii__Product2Add__c objPR : giic_Test_DataCreationUtility.lstProdRef_N ){
            objPR.gii__DefaultWarehouse__c = testWareListN[math.mod(intCount,5)].Id;
            intCount++;
        }
        giic_Test_DataCreationUtility.lstProdRef_N[3].gii__NonStock__c = true;
        update giic_Test_DataCreationUtility.lstProdRef_N;
        List<gii__SalesOrder__c> testSOList1 = giic_Test_DataCreationUtility.insertSalesOrder_N(10);
    }
    
    public static testMethod void testRelease()
    {
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        System.Runas(u)  
        {
        List<gii__SalesOrder__c> soList = [Select Id from gii__SalesOrder__c where giic_SOStatus__c = 'Open'];
        for(gii__SalesOrder__c soItem : soList){
            //soItem.gii__Released__c = false;
            soItem.giic_CCAuthorized__c = true;
            soItem.giic_AddressValidated__c = true;
            soItem.giic_TaxCal__c = true;
            soItem.giic_ShipFeeCal__c = true;
        }
        update soList;
        gii__SystemPolicy__c SPNeg = new gii__SystemPolicy__c();
        gii__SystemPolicy__c SP = [select Name, giic_JobTime__c from gii__SystemPolicy__c limit 1];
        ApexPages.StandardController stdController = new ApexPages.StandardController(SP);
        ApexPages.StandardController stdControllerNeg = new ApexPages.StandardController(SPNeg);
        giic_ReleaseSalesOrderController releaseSOC = new giic_ReleaseSalesOrderController(stdController);
        giic_ReleaseSalesOrderController releaseSOCNeg = new giic_ReleaseSalesOrderController(stdControllerNeg);
        Test.startTest();
        releaseSOC.releaseSO();
        releaseSOCNeg.releaseSO();
        releaseSOC.getBatchJobs();
        releaseSOC.scheduleBatch();
        releaseSOCNeg.scheduleBatch();
        SP.giic_JobTime__c = null;
        update SP;
        releaseSOC.scheduleBatch();
        giic_SOReleaseBatch releaseBatch = new giic_SOReleaseBatch();
        releaseBatch.execute(null, soList);
        Test.stopTest();
        }
    } 
    
    
   public static testMethod void test_SOSAddressValidationSchedulerpositive(){
       User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
       System.Runas(u)  
        {
            List<gii__SystemPolicy__c> lstSystemPolicy = [select Name, gii__Description__c, gii__PriceBookName__c, gii__StockUM__c, gii__ServiceTicketPricingOverriddenReason__c, gii__AutoReleaseOrder__c, gii__AllowZeroUnitPrice__c, gii__OppConversionPricingOverriddenReason__c, gii__SQConversionPricingOverriddenReason__c, gii__Warehouse__c, gii__AutoApprovePurchaseOrders__c, giic_JobTime__c,giic_AddressValidationJobTime__c 
                                                    from gii__SystemPolicy__c ];            
            lstSystemPolicy[0].giic_AddressValidationJobTime__c = 120;
            update lstSystemPolicy;            
            apexpages.standardcontroller stdController = new apexpages.standardcontroller(lstSystemPolicy[0]);
            giic_SOSAddressValidationScheduler obj = new giic_SOSAddressValidationScheduler(stdController);
            giic_SOSAddressValidationScheduler obj1 = new giic_SOSAddressValidationScheduler(stdController);
            Test.startTest(); 
            obj.scheduleBatch();
            obj1.scheduleBatch();
            Test.stopTest();        
    } 
   }
    
    public static testMethod void test_SOSAddressValidationSchedulernegative(){
           User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
           System.Runas(u)  
          {
             Test.startTest();          
             List<gii__SystemPolicy__c> lstSystemPolicy = [select Name, gii__Description__c, gii__PriceBookName__c, gii__StockUM__c, gii__ServiceTicketPricingOverriddenReason__c, gii__AutoReleaseOrder__c, gii__AllowZeroUnitPrice__c, gii__OppConversionPricingOverriddenReason__c, gii__SQConversionPricingOverriddenReason__c, gii__Warehouse__c, gii__AutoApprovePurchaseOrders__c, giic_JobTime__c,giic_AddressValidationJobTime__c 
                                                    from gii__SystemPolicy__c ];           
            lstSystemPolicy[0].giic_AddressValidationJobTime__c = 5;
            update lstSystemPolicy;            
            apexpages.standardcontroller stdController = new apexpages.standardcontroller(lstSystemPolicy[0]);
            giic_SOSAddressValidationScheduler obj = new giic_SOSAddressValidationScheduler(stdController);
            obj.scheduleBatch();
             Test.stopTest();
    }
}
}