/*
* 09-27-2017   -Bethi Reddy, Royal Canin     - Edited (Included shiptoId check and Updated batch size to 1 (SCH-0051))
*/


global class UpdateProspectAccountBatch implements Database.Batchable<sObject>, schedulable {
    
    global final String query;
    
    global UpdateProspectAccountBatch() {
        system.debug('#####constr');
        string temp;
        temp = 'select id, Phone_Number__c, Name_of_Business_Organization__c, Checked_for_Prospects__c, Customer_Account_ID__c, ShipToCustomerNo__c ';
        temp +='from Customer_Registration__c ';
        temp +='where Checked_for_Prospects__c = false and Phone_Number__c != null and Name_of_Business_Organization__c != null';

        query = temp;
    }
    
    global UpdateProspectAccountBatch(Boolean isTest) {
        system.debug('#####constr');
        string temp;
        temp = 'select id, Phone_Number__c, Name_of_Business_Organization__c, Checked_for_Prospects__c, Customer_Account_ID__c, ShipToCustomerNo__c ';
        temp +='from Customer_Registration__c ';
        temp +='where Checked_for_Prospects__c = false and Phone_Number__c != null and Name_of_Business_Organization__c != null';
        if (isTest){
            temp += ' order by id desc limit 2';
        }
        query = temp;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        
        
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Customer_Registration__c> registrations) {
        system.debug('&&&&&execute()');
        Set<string> phoneNumbers = new Set<string>();
        Map<string, List<Customer_Registration__c>> phoneToReg = new Map<string, List<Customer_Registration__c>>();

        Set<string> businessNames = new Set<string>();
        Map<string, List<Customer_Registration__c>> nameToReg = new Map<string, List<Customer_Registration__c>>();
        
      Set<string> shipToCustomerId = new Set<string>();
       Map<string, List<Customer_Registration__c>> shipIdToReg = new Map<string, List<Customer_Registration__c>>();

        for(Customer_Registration__c reg : registrations) {
            phoneNumbers.add(reg.Phone_Number__c);
            
            if(phoneToReg.containsKey(reg.Phone_Number__c)) {
                phoneToReg.get(reg.Phone_Number__c).add(reg);
            } else {
                List<Customer_Registration__c> regs = new List<Customer_Registration__c>();
                regs.add(reg);
                phoneToReg.put(reg.Phone_Number__c, regs);
            }

            businessNames.add(reg.Name_of_Business_Organization__c);

            if(nameToReg.containsKey(reg.Name_of_Business_Organization__c)) {
                nameToReg.get(reg.Name_of_Business_Organization__c).add(reg);
            } else {
                List<Customer_Registration__c> regs = new List<Customer_Registration__c>();
                regs.add(reg);
                nameToReg.put(reg.Name_of_Business_Organization__c, regs);
            }

             shipToCustomerId.add(reg.ShipToCustomerNo__c);

            if(shipIdToReg.containsKey(reg.ShipToCustomerNo__c)) {
                shipIdToReg.get(reg.ShipToCustomerNo__c).add(reg);
            } else {
                List<Customer_Registration__c> regs = new List<Customer_Registration__c>();
                regs.add(reg);
                shipIdToReg.put(reg.ShipToCustomerNo__c, regs);
            } 
            
            reg.Checked_for_Prospects__c = true;
        }

      
                                  
        List<Account> accounts = [select Id, Name, Phone, Customer_registration__c, Bill_To_Customer_ID__c, ShipToCustomerNo__c, Start_Onboarding_Chatter_Post__c
                                  from Account 
                                  where Name in: businessNames and Phone in: phoneNumbers and ShipToCustomerNo__c in: shipToCustomerId  and Account_Status__c = 'Prospect' and Start_Onboarding_Chatter_Post__c = false]; 
        
        for(Account acc : accounts) {
            if(phoneToReg.containsKey(acc.Phone) && nameToReg.containsKey(acc.Name) && shipIdToReg.containsKey(acc.ShipToCustomerNo__c)) {
            
                for(Customer_Registration__c reg1 : phoneToReg.get(acc.Phone)) {
                    for(Customer_Registration__c reg2 : nameToReg.get(acc.Name)) {
                        for(Customer_Registration__c reg3: shipIdToReg.get(acc.ShipToCustomerNo__c)) {
                         if(reg1.Id == reg2.Id && reg1.Id == reg3.Id && reg2.Id == reg3.Id && acc.Phone == reg1.Phone_Number__c && acc.Name == reg1.Name_of_Business_Organization__c && acc.ShipToCustomerNo__c == reg1.ShipToCustomerNo__c) {
                          
                            acc.Customer_Registration__c = reg1.Id;
                            acc.ShipToCustomerNo__c = reg1.ShipToCustomerNo__c;
                            acc.Bill_To_Customer_ID__c = reg1.Customer_Account_ID__c;
                            string message = 'This prospect has started the onboarding application and is in the process of becoming a Royal Canin customer! The prospect’s application may be viewed with this link: ' + System.URL.getSalesforceBaseUrl().toExternalForm() + '/' + reg1.Id + ' in the event the prospect needs assistance in completing the onboarding application.';
                            createChatterPost(acc.Id, message);
                            acc.Start_Onboarding_Chatter_Post__c = true;
                            system.debug('#####match found. reg:' + reg1.Id + ':acc:' + acc.Id); 
                        }
                    }
                }
            }
        }
        
        }

        try {
            update registrations;
        } catch(Exception ex) {
            system.debug('#####registrationupdate' + ex.getMessage());
        }
        
        try {
            update accounts;
        } catch(Exception ex) {
            system.debug('#####accountupdate' + ex.getMessage());
        }
    }
    
    global void finish(Database.BatchableContext BC) {
    
    }
    
    global void execute(SchedulableContext sc) {
      ID BatchId = Database.executeBatch(new UpdateProspectAccountBatch(), 1);
   }

   
    private void createChatterPost(Id idOfObject, string message) {
        ConnectApi.MessageBodyInput messageInput = new ConnectApi.MessageBodyInput();
        messageInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();

        ConnectApi.TextSegmentInput textSegment = new ConnectApi.TextSegmentInput();
        textSegment.text = message;
        messageInput.messageSegments.add(textSegment);

        ConnectApi.FeedItemInput input = new ConnectApi.FeedItemInput();
        input.body = messageInput;

        //ConnectApi.ChatterFeeds.postFeedItem(null, ConnectApi.FeedType.Record, idOfObject, input, null);
        ConnectApi.ChatterFeeds.postFeedElement(null, idOfObject, ConnectApi.FeedElementType.FeedItem, message);
        
    }
}