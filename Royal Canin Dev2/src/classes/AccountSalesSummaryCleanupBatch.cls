global class AccountSalesSummaryCleanupBatch implements Database.Batchable<sObject> {
    /*
    Execute the appropriate commands below in 'Execute Anonymous' window

AccountSalesSummaryCleanupBatch a = new AccountSalesSummaryCleanupBatch();
Database.executeBatch(a);

    If you want to increase the batch size to the max of 2,000 you can use the following command.

AccountSalesSummaryCleanupBatch a = new AccountSalesSummaryCleanupBatch();
Database.executeBatch(a, 2000);

    */
    
    global String query;

    global AccountSalesSummaryCleanupBatch(){
        this.query ='SELECT Id, Sales_Summarization_Completed__c ';
        this.query += ' FROM Account ';
        this.query += ' WHERE Bill_To_Customer_ID__c != null';
        this.query += ' AND Sales_Summarization_Completed__c = true ';
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        List<Account> updatedAccounts = new List<Account>();
        
        for(sObject s : scope){
            Account a = (Account)s;
            a.Sales_Summarization_Completed__c = false; 
            updatedAccounts.add(a);
        }
        update updatedAccounts;
    }
    
    global void finish(Database.BatchableContext BC){
        
        AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,TotalJobItems, CreatedBy.Email FROM AsyncApexJob WHERE Id = :BC.getJobId()];
        
        if(a.status == 'Failed') {
            Job_Failure_Notification_Email__c myemail = Job_Failure_Notification_Email__c.getValues('Email Id');
            String myEmails = myemail.Notification_Email__c;
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {myemails};
            mail.setToAddresses(toAddresses);
            mail.setSubject('Apex Sharing Recalculation ' + a.Status);
            mail.setPlainTextBody('The AccountSalesSummaryCleanupBatch job has Failed');
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
        
        if(a.status == 'Completed' || a.TotalJobItems == 0 || a.NumberOfErrors > 0) {
            Job_Failure_Notification_Email__c myemail = Job_Failure_Notification_Email__c.getValues('Email Id');
            String myEmails = myemail.Notification_Email__c;
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {myemails};
            mail.setToAddresses(toAddresses);
            
            if(test.isrunningtest()) {
                mail.setSubject('Test execution mail Apex Sharing Recalculation ' + a.Status);
            }else {
                mail.setSubject('Apex Sharing Recalculation ' + a.Status);
            }
            
            mail.setPlainTextBody('The AccountSalesSummaryCleanupBatch job processed ' + a.TotalJobItems + ' batches with '+ a.NumberOfErrors + ' failures.');
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }  
         
        if(a.status !='Failed' && a.TotalJobItems != 0 && a.TotalJobItems != a.NumberOfErrors && !test.isrunningtest()) {
            AccountSalesSummaryBatch obj = new AccountSalesSummaryBatch();
            Database.executeBatch(obj,20);
        }                
    }
}