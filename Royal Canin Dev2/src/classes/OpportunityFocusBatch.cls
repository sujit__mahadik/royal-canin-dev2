global class OpportunityFocusBatch implements Database.Batchable<sObject> {
	
	global final String query;

	private final List<Focused_Opportunities__c> focusedOpps;
	private static Map<String,Id> oppRecTypeMap;
	private static Map<String,Id> accRecTypeMap;
	private static Map<String,Id> channelToOppRecTypeMap;
	private static Map<String,Id> channelToAccRecTypeMap;

	static {
		oppRecTypeMap = new Map<String, Id>();
		for(RecordType rec : [select id, developerName from RecordType where SObjectType = 'Opportunity']) {
			oppRecTypeMap.put(rec.DeveloperName, rec.Id);
		}

		accRecTypeMap = new Map<String, Id>();
		for(RecordType rec : [select id, developerName from RecordType where SObjectType = 'Account']) {
			accRecTypeMap.put(rec.DeveloperName, rec.Id);
		}

		channelToOppRecTypeMap = new Map<String, Id>();
		channelToAccRecTypeMap = new Map<String, Id>();
		for(Focused_opp_RecType_Mapping__c mapping : Focused_opp_RecType_Mapping__c.getAll().values()) {
			channelToOppRecTypeMap.put(mapping.Name, oppRecTypeMap.get(mapping.OppRecType__c));
			channelToAccRecTypeMap.put(mapping.Name, accRecTypeMap.get(mapping.AccRecType__c));
		}
	}

	global OpportunityFocusBatch() {
		Date today = Date.today();
		this.focusedOpps = [select Id, Channel__c, Effective_Date__c, Name, Opportunity_Close_Date__c, Opportunity_Type__c, Processed__c, Product_Category__c 
							from Focused_Opportunities__c 
							where Processed__c = false and (Effective_Date__c <: today or Effective_Date__c =: today)];

		if(!this.focusedOpps.isEmpty()) {
			query = buildQuery(focusedOpps);
		} else {
			query = '';
		}
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		System.debug('heyo: ' + query);
		return Database.getQueryLocator(query);
	}

	global void execute(Database.BatchableContext BC, List<Account> accounts) {
		List<Opportunity> oppsToInsert = new List<Opportunity>();

		for(Focused_Opportunities__c focusedOpp : this.focusedOpps){
			for(Account acc : accounts){
				oppsToInsert.add(createOpportunity(acc, focusedOpp));
			}
		}

		insert oppsToInsert;

		for(Focused_Opportunities__c focusedOpp : this.focusedOpps){
			focusedOpp.Processed__c = true;
		}

		update this.focusedOpps;
	}
	
	global void finish(Database.BatchableContext BC) {}

	private Opportunity createOpportunity(Account acc, Focused_Opportunities__c focusedOpp) {
		Opportunity opp = new Opportunity();
		opp.AccountId = acc.Id;
		opp.Name = focusedOpp.Name;
		opp.RecordTypeId = channelToOppRecTypeMap.get(focusedOpp.Channel__c);
		opp.OwnerId = acc.OwnerId;
		opp.Type = focusedOpp.Opportunity_Type__c;
		opp.Product_Category__c = focusedOpp.Product_Category__c;
		opp.CloseDate = focusedOpp.Opportunity_Close_Date__c;
		opp.StageName = 'Planning';
		return opp;
	}

	private String buildQuery(List<Focused_Opportunities__c> focusedOpps) {
		Set<Id> accRecTypeIds = new Set<Id>();
		string recTypeIds = '(\'';
		for(Focused_Opportunities__c focusedOpp : this.focusedOpps) {
			System.debug(focusedOpp.channel__c);
			accRecTypeIds.add(channelToAccRecTypeMap.get(focusedOpp.Channel__c));
			System.debug(channelToAccRecTypeMap);
			recTypeIds += channelToAccRecTypeMap.get(focusedOpp.Channel__c) + '\',';
		}

		System.debug('accRecTypeIds: ' + accRecTypeIds);
		System.debug('recTypeIds: ' + recTypeIds);
		recTypeIds = recTypeIds.substring(0,recTypeIds.length()-1);
		recTypeIds += ')';

		return 'select id, Name, ownerId from account where (Account_Status__c = \'Prospect\' or Account_Status__c = \'Customer\') and RecordTypeId in ' + recTypeIds;
	}
}