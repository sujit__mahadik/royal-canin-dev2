/************************************************************************************
Version : 1.0
Name : giic_CreditCardWrapper
Created Date : 12 Sep 2018
Function : Maintain all card related information.
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/
public class giic_CreditCardWrapper {
    @AuraEnabled public string CardType;
    @AuraEnabled public string CardTypeValue;
    //@AuraEnabled public string CCVNumber;
    @AuraEnabled public Boolean IsDefaultCard;
    @AuraEnabled public string NameOnCard;
    @AuraEnabled public string CardNumber;
    @AuraEnabled public string MaskedCardNumber;
    @AuraEnabled public string ExpiryMonth;
    @AuraEnabled public string ExpiryYear;
    @AuraEnabled public Boolean isSelectedCard;
    @AuraEnabled public String SubscriptionID;
    @AuraEnabled public String ShipToCustomerNo;
    //@AuraEnabled public String AuthId;
    //@AuraEnabled public String AuthToken;
    @AuraEnabled public Boolean IsDeleted;
    //@AuraEnabled public Map<String,String> utilFields;
    //Added after receiving RC-CC APIs
    @AuraEnabled public Integer CreditCardHistoryID;
    @AuraEnabled public Integer CreditCardTypeID;
    @AuraEnabled public Integer ProcessorID;
    @AuraEnabled public String BillToCustomerNo; 
    //@AuraEnabled public String Token; //Not in use
    @AuraEnabled public String DateTimeCreate;
    @AuraEnabled public Boolean IsActive;
    @AuraEnabled public String AuthCode;
    //@AuraEnabled public Integer CCExpDateMonth;
    //@AuraEnabled public Integer CCExpDateYear;
    @AuraEnabled public String OrderNumber;
    @AuraEnabled public String sObjName;
    @AuraEnabled public String recordId;
    public giic_CreditCardWrapper(){
        CardType = '';
        CardTypeValue = '';
        //CCVNumber = '';
        IsDefaultCard = false;
        NameOnCard = '';
        CardNumber = '';
        MaskedCardNumber = '';
        ExpiryMonth = '';
        ExpiryYear = '';
        isSelectedCard = false;
        SubscriptionID = '';
        ShipToCustomerNo='';
        //AuthId = '';
        //AuthToken = '';
        IsDeleted = false;
        //utilFields= new Map<String, String>();
        CreditCardHistoryID = 0;
        CreditCardTypeID = 0;
        ProcessorID = 0;
        BillToCustomerNo = '';
        //Token = '';
        DateTimeCreate = '';
        IsActive = true;
        AuthCode = '';
        //CCExpDateMonth = 1;
        //CCExpDateYear = 2018;
        OrderNumber = '';
        sObjName = '';
        recordId = '';
    }
}