/**********************************************************
* Purpose    : The purpose of this class to handle giic_SalesOrderStagingTrigger event by extending Triggerhandler.  
*              
* *******************************************************
* Author                        Date            Remarks
* Devanshu Sharma               20/09/2018     
*********************************************************/
public class giic_SalesOrderLineStagingTriggerHelper {
    
    /***********************************************************
    * Method name : OnAfterInsert
    * Description : to handle After Insert events of trigger
    * Return Type : void
    * Parameter : List of gii__SalesOrderLineStaging__c records
    ***********************************************************/
    public void OnAfterInsert(List<gii__SalesOrderLineStaging__c > lstTriggerNew)
    {        
        List<gii__SalesOrderStaging__c> lstalesOrderStaging = new List<gii__SalesOrderStaging__c>();
        Set<ID> setSOSID = new Set<Id>();
        Set<ID> newsetSOSID = new Set<Id>();
        System.debug('lstTriggerNew==='+lstTriggerNew);
        for(gii__SalesOrderLineStaging__c obj : lstTriggerNew)
        {
            setSOSID.add(obj.giic_SalesOrderStaging__c);
        }
        if(setSOSID.size()>0)
        {
            for(gii__SalesOrderStaging__c obj : [select id, giic_Status__c,giic_AddressValidated__c from gii__SalesOrderStaging__c where id in : setSOSID and giic_Status__c != : System.Label.giic_Converted and giic_AddressValidated__c = true])
            {                
                newsetSOSID.add(obj.id);                
            }           
        }
        if(newsetSOSID.size()>0)
        {
            ProcessSalesOrderStaging(newsetSOSID);    
        }          
    }
    /***********************************************************
    * Method name : OnAfterUpdate
    * Description : to handle After Update events of trigger
    * Return Type : void
    * Parameter : List of gii__SalesOrderLineStaging__c records
    ***********************************************************/
    public void OnAfterUpdate(List<gii__SalesOrderLineStaging__c> lstTriggerold, List<gii__SalesOrderLineStaging__c> lstTriggerNew, Map<Id,gii__SalesOrderLineStaging__c> mapTriggernew,Map<Id,gii__SalesOrderLineStaging__c>  mapTriggernold)
    {        
        
        List<gii__SalesOrderStaging__c> lstalesOrderStaging = new List<gii__SalesOrderStaging__c>();
        Set<ID> setSOSID = new Set<Id>();
        Set<ID> newsetSOSID = new Set<Id>();        
        for(gii__SalesOrderLineStaging__c obj : lstTriggerNew)
        {
            setSOSID.add(obj.giic_SalesOrderStaging__c);
        }
        if(setSOSID.size()>0)
        {
            for(gii__SalesOrderStaging__c obj : [select id, giic_Status__c,giic_AddressValidated__c from gii__SalesOrderStaging__c where id in : setSOSID and giic_Status__c != : System.Label.giic_Converted and giic_AddressValidated__c = true])
            {                
                newsetSOSID.add(obj.id);                
            }
        }
        if(newsetSOSID.size()>0)
        {
            ProcessSalesOrderStaging(newsetSOSID);       
        }        
    }    
    /**********************************************************************************************************************
    * Method name : ProcessSalesOrderStaging
    * Description : to convert Sales Order Staging records to Sales Orders if Drop Ship = true and Address Validation = true
    * Return Type : void
    * Parameter : Set of gii__SalesOrderLineStaging__c ids
    **********************************************************************************************************************/
    @future
    public static void ProcessSalesOrderStaging(Set<ID> setSOSID) 
    {        
        //DeActivate Errors
        giic_IntegrationCustomMetaDataUtil.deActivateCurrentError(setSOSID);        
       
        Set<String> setErrorIds = new Set<String>();
        String sObjectApiName = 'gii__SalesOrderStaging__c'; 
        //Getting Intergarion Details for Sobject
        Map<String, giic_IntegrationCustomMetaDataWrapper> mapIntegrationSettings = giic_IntegrationCustomMetaDataUtil.getIntegrationSettings(sObjectApiName);            
        String soql = '';
        //Creating filter clause for SOQL for related Sales Order Staging Id's
        String inClause = String.format( '(\'\'{0}\'\')',new List<String> { String.join( new List<Id>(setSOSID) , '\',\'') });            
        if(mapIntegrationSettings.containsKey(sObjectApiName)){
            giic_IntegrationCustomMetaDataWrapper cmdObj = mapIntegrationSettings.get(sObjectApiName); 
            soql = 'Select ' + cmdObj.strSoql.subString(0, cmdObj.strSoql.length()-1) + ' from ' + cmdObj.sourceSobjectApi + ' where id in '+ inClause;            
        }        
        List<gii__SalesOrderStaging__c> lstSalesOrderStaging = Database.query(soql);        
        if(lstSalesOrderStaging.size()>0)
        {
            
            //Calling utility method do conversion for conversting SOS to SO and storing result in a map
            Map<String, giic_IntegrationCMDResultWrapper> mpNewSObjectId = giic_IntegrationCustomMetaDataUtil.doConversion(sObjectApiName, mapIntegrationSettings, lstSalesOrderStaging, setErrorIds);
            List<gii__SalesOrder__c> soList = new List<gii__SalesOrder__c>();
            for(gii__SalesOrderStaging__c objSOS :(List<gii__SalesOrderStaging__c>) lstSalesOrderStaging)
            {                
                if(setErrorIds.contains(''+objSOS.id)) 
                {
                    objSOS.giic_Status__c = System.Label.giic_Error;
                }                       
                else if(mpNewSObjectId.get(objSOS.Id).totalChilds != 0)
                { 
                    //Updating SOS Status
                    objSOS.giic_Status__c = System.Label.giic_InProgress; 
                    //Updating SO Status
                    gii__SalesOrder__c so = new gii__SalesOrder__c(Id=mpNewSObjectId.get(objSOS.Id).targetObj.Id,giic_ProcessingStatus__c=System.Label.giic_ProcessedwithError,giic_SOStatus__c=System.Label.giic_Draft); 
                    soList.add(so); 
                }
                else 
                { 
                    //Updating SOS Status
                    objSOS.giic_Status__c = System.Label.giic_Converted; 
                    //Updating SOS Status
                    gii__SalesOrder__c so = new gii__SalesOrder__c(Id=mpNewSObjectId.get(objSOS.Id).targetObj.Id,giic_ProcessingStatus__c=System.Label.giic_Processed); 
                    soList.add(so); 
                }
            }
            if(soList.size()>0)
            {
                //Updating list of SO
                update soList;
            }    
            //Updating list of SOS
            update lstSalesOrderStaging;
            
            //Check for exception state in the converted SO
            set<string> SOIds = new set<string>();
            if(mpNewSObjectId.values() != null && mpNewSObjectId.values().size() > 0)
            {
               for(giic_IntegrationCMDResultWrapper oICMDRW : mpNewSObjectId.values()){
                   // 
                   system.debug('oICMDRW.targetObj.getSObjectType()---->>>>>1111 ' + oICMDRW.targetObj.getSObjectType());
                   if(oICMDRW.targetObj != null && oICMDRW.targetObj.getSObjectType() == Schema.gii__SalesOrder__c.getSObjectType() ){
                       system.debug('oICMDRW.targetObj.getSObjectType()---->>>>>2222' + oICMDRW.targetObj.getSObjectType()+ ' <<<<--->>>> ' + Schema.gii__SalesOrder__c.getSObjectType());
                       if(string.isNotEmpty(string.valueOf( oICMDRW.targetObj.get('Id')))){
                           SOIds.add(string.valueOf( oICMDRW.targetObj.get('Id')));
                       }
                   }
               }
            }          
           //Check for exception state of the converted orders
           giic_IntegrationCommonUtil.checkForExceptionState(SOIds);           
        
        }       
    }  
}