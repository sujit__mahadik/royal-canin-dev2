/************************************************************************************
Version : 1.0
Created Date : 14 Aug 2018
Function : Cancel Sales Order lines based on the selected on related list
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/

public class giic_CancelSOLController {
    Map<Id,gii__SalesOrderLine__c> recordsToUpdateMap = new Map<Id,gii__SalesOrderLine__c>();
    List<gii__SalesOrderLine__c> lstSOLToDelete = new List<gii__SalesOrderLine__c>();
    public boolean isSOReserved;
    public boolean isSOReleased;
    public static string salesOrderId = '';
    public static List<string> lstErrorMessages  = new List<string>();
    public static List<string> lstSuccessMessages  = new List<string>();
    public static List<string> lstMessages  = new List<string>();
    public giic_CancelSOLController(ApexPages.StandardSetController stdSetController){
        isSOReserved=true;
        isSOReleased = false;
        
        List<gii__SalesOrderLine__c> recordsFetched = [SELECT Id,Name, gii__SalesOrder__c,gii__SalesOrder__r.giic_SOStatus__c,gii__OrderQuantity__c, gii__ReservedQuantity__c, giic_LineStatus__c, gii__CancelDate__c, gii__CancelledQuantity__c, gii__CancelReason__c, gii__TaxAmount__c,gii__SalesOrder__r.giic_OrderLineCancelled__c, gii__SalesOrder__r.gii__Released__c  FROM gii__SalesOrderLine__c  WHERE Id IN : stdSetController.getSelected()];
        for(gii__SalesOrderLine__c sOrderLi : recordsFetched){
            salesOrderId = sOrderLi.gii__SalesOrder__c;
            recordsToUpdateMap.put(sOrderLi.Id, sOrderLi);
            if(sOrderLi.gii__SalesOrder__r.giic_SOStatus__c == giic_Constants.SOL_DRAFTSTATUS
               || sOrderLi.gii__SalesOrder__r.giic_SOStatus__c == giic_Constants.SOL_OPENSTATUS){
                   isSOReserved=false;
               }
            if( sOrderLi.gii__SalesOrder__r.gii__Released__c == true){
                isSOReleased = true;
            }
        }
    }
    /*
    * Method name : cancelSalesOrderLines 
    * Description : cancel sales order lines which is selected from sales order screen
    * Return Type : redirect to Sales order record
    * Parameter : 
    */
    public PageReference cancelSalesOrderLines(){
        List<gii__SalesOrderLine__c> recordsToUpdate = new List<gii__SalesOrderLine__c>();
        // System.debug('recordsToUpdateMap'+recordsToUpdateMap);
        if(recordsToUpdateMap.values().isEmpty()){
            lstErrorMessages.add(giic_Constants.SELECT_REC);
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR,giic_Constants.SELECT_REC)); 
            return null;
        }
        try{
            for(gii__SalesOrderLine__c sOrderLi : recordsToUpdateMap.values()){
                
                if(!isSOReleased){
                    lstSOLToDelete.add(sOrderLi);
                    lstSuccessMessages.add(sOrderLi.name + label.giic_CancelledSOLDeleted );
                }
                else{
                    if(sOrderLi.giic_LineStatus__c != giic_Constants.SOL_BACKORDERSTATUS){
                        lstErrorMessages.add( sOrderLi.name + label.giic_LineCanNotBeCancelled );
                    }
                    else{
                        // ie. SOL is backorder
                        sOrderLi.gii__CancelDate__c = System.today();
                        sOrderLi.gii__CancelledQuantity__c = sOrderLi.gii__OrderQuantity__c; //Order quantity
                        sOrderLi.gii__CancelReason__c= system.Label.giic_DefaultCancelReasonSOL; 
                        sOrderLi.gii__TaxAmount__c = 0.0;//Tax Amount to be set to 0: OMS-154
                        sOrderLi.giic_LineStatus__c = giic_Constants.SOSTATUSCANCELLED ;
                        sOrderLi.gii__DiscountPercent__c=0;
                        recordsToUpdate.add(sOrderLi);
                        lstSuccessMessages.add( sOrderLi.name + label.giic_SOLCancelled );
                    }
                }
                
            }
            if(lstSOLToDelete != null && lstSOLToDelete.size() > 0){
                delete lstSOLToDelete;
            }
            if(recordsToUpdate.size()>0){
                Database.update(recordsToUpdate);
            }
			
			if(!isSOReleased)
			{				
				if(lstSuccessMessages != null && lstSuccessMessages.size() > 0)
				{
					ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.INFO, String.join(lstSuccessMessages,'<br>')));
				}
				return null;
			}			
        
            boolean isAllCancelled=false;
            list<gii__SalesOrderLine__c> lstAllLineItems=[SELECT Id, gii__LineStatus__c FROM gii__SalesOrderLine__c where gii__SalesOrder__c=:recordsToUpdateMap.values()[0].gii__SalesOrder__c];
            list<gii__SalesOrderLine__c> lstAllSOL = new list<gii__SalesOrderLine__c>();
            for(gii__SalesOrderLine__c soL : lstAllLineItems){
                if(soL.gii__LineStatus__c != giic_Constants.SOLI_CANCEL ){
                    lstAllSOL.add(soL);
                }
            }
            //Update Sales Order status to cancelled when all the lines are cancelled
            if(lstAllSOL.size() == 0 && isSOReleased){
                gii__SalesOrder__c objSalesOrder=new gii__SalesOrder__c(Id=recordsToUpdateMap.values()[0].gii__SalesOrder__c);
                objSalesOrder.giic_CancelReson__c=system.Label.giic_DefaultCancelReasonSOL;
                objSalesOrder.giic_CancelDate__c=system.today();
                objSalesOrder.giic_SOStatus__c=system.label.giic_SalesOrderCancelledStatus;
                
                
                if(recordsToUpdateMap.values()[0].gii__SalesOrder__r.giic_OrderLineCancelled__c == true)
				{
					//gii__SalesOrder__c objSalesOrder=new gii__SalesOrder__c(Id=recordsToUpdateMap.values()[0].gii__SalesOrder__c);
					objSalesOrder.giic_OrderLineCancelled__c =  false;
					update objSalesOrder;                       
					objSalesOrder.giic_OrderLineCancelled__c =  true;
					update objSalesOrder;
				}
				else
				{
					//gii__SalesOrder__c objSalesOrder=new gii__SalesOrder__c(Id=recordsToUpdateMap.values()[0].gii__SalesOrder__c);
					objSalesOrder.giic_OrderLineCancelled__c =  true;
					update objSalesOrder;
				}
					
                
                
                //update objSalesOrder;
                
                isAllCancelled = true;
                
            }
            else
            {
                if(isSOReleased)
				{
					if(recordsToUpdateMap.values()[0].gii__SalesOrder__r.giic_OrderLineCancelled__c == true)
					{
						gii__SalesOrder__c objSalesOrder=new gii__SalesOrder__c(Id=recordsToUpdateMap.values()[0].gii__SalesOrder__c);
						objSalesOrder.giic_OrderLineCancelled__c =  false;
						update objSalesOrder;                       
						objSalesOrder.giic_OrderLineCancelled__c =  true;
						update objSalesOrder;
					}
					else
					{
						gii__SalesOrder__c objSalesOrder=new gii__SalesOrder__c(Id=recordsToUpdateMap.values()[0].gii__SalesOrder__c);
						objSalesOrder.giic_OrderLineCancelled__c =  true;
						update objSalesOrder;
					}
				}
                
            }
            if(!isSOReserved){
                //Recalculate promotions again
                map<string,object> mapRequest= new map<string,object>{'SOIDS'=>new set<string>{recordsToUpdateMap.values()[0].gii__SalesOrder__c},'ISNEWORDER'=>false};
                map<string,object> mapResponse = giic_PromotionHelper.applyPromotion(mapRequest);
            }
            
            if(lstErrorMessages != null && lstErrorMessages.size() > 0){
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, String.join(lstErrorMessages,'<br>')));
            }
            if(lstSuccessMessages != null && lstSuccessMessages.size() > 0){
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.INFO, String.join(lstSuccessMessages,'<br>')));
            }
            
        }catch(DmlException e){
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR,e.getMessage()));
        }
        
        return null;
    }
    
    /*
    * Method name : goBack
    * Description : Back button
    * Return Type : redirect to Sales order record
    * Parameter : 
    */
    public PageReference goBack(){
        PageReference pObj;
        try{          
            pObj = new PageReference('/'+ApexPages.currentPage().getParameters().get('id')); // redirect to sales order 
            pObj.setRedirect(true);            
        }catch(Exception ex){ 
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getMessage())); 
            return null;  
        }
        return pObj;
        
    }
    
}