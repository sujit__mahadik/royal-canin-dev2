/*
* 09-27-2017   -Bethi Reddy, Royal Canin     - Edited (Updated batch size to 1 (SCH-0051))
*/

global class UpdateProspectAccountBatchSchedule implements Schedulable {

  public static string chron = '0 00 * * * ?';

  global void execute(SchedulableContext sc) {
    UpdateProspectAccountBatch u = new UpdateProspectAccountBatch();
    Database.executeBatch(u,1);
  }

  //call this from exec anon after code deploy
  global static String schedule() {
    UpdateProspectAccountBatchSchedule schedule = new UpdateProspectAccountBatchSchedule();
    return System.Schedule('UpdateProspectAccountBatch', chron, schedule);
  }
}