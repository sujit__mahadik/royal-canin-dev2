public with sharing class SetupTestData {
    
    public static List<Account> testAccounts;
    public static List<Sales_Summary__c> testSalesSummaryRecords;
    
    public static void removeCustomSettings(){
        Boolean done = false;
        while (!done){
            List<Global_Parameters__c> gpList = [SELECT id from Global_Parameters__c limit 1000];
            if (gpList.size() > 0){
                delete gpList;
            }else{
                done = true;
            }
            
        }
        delete [SELECT id from reg_steps__c];
        delete [SELECT id from Registration_Record_Type_Lookup__c];
        delete [SELECT id from Registration_Record_Type_Mapping__c];
        
    }
    public static void createCustomSettings(){
        
        User user = [Select Id From User Where Id =: UserInfo.getUserId()];
        System.runAs(user){
            List<Global_Parameters__c> globalParametersList = new List<Global_Parameters__c>();
            
            Global_Parameters__c encryptionKey = new Global_Parameters__c();
            encryptionKey.Name = 'EncryptionKey';
            encryptionKey.Value__c = '12345678901234567890123456789012';//Must be exactly 32 characters
            globalParametersList.add(encryptionKey);

            Global_Parameters__c appURLStub = new Global_Parameters__c();
            appURLStub.Name = 'ApplicationURLStub';
            appURLStub.Value__c = 'dev-royalcanindev03272015.cs17.force.com';
            globalParametersList.add(appURLStub);

            Global_Parameters__c avalaraPassword = new Global_Parameters__c();
            avalaraPassword.Name = 'Avalara_Password';
            avalaraPassword.Value__c = '06F426E5D5985E8F';
            globalParametersList.add(avalaraPassword);

            Global_Parameters__c avalara_ProfileAdapter = new Global_Parameters__c();
            avalara_ProfileAdapter.Name = 'Avalara_ProfileAdapter';
            avalara_ProfileAdapter.Value__c = 'customAdapter';
            globalParametersList.add(avalara_ProfileAdapter);

            Global_Parameters__c avalara_ProfileClient = new Global_Parameters__c();
            avalara_ProfileClient.Name = 'Avalara_ProfileClient';
            avalara_ProfileClient.Value__c = 'AvaTaxSample';
            globalParametersList.add(avalara_ProfileClient);

            Global_Parameters__c avalara_ProfileName = new Global_Parameters__c();
            avalara_ProfileName.Name = 'Avalara_ProfileName';
            avalara_ProfileName.Value__c = 'Development';
            globalParametersList.add(avalara_ProfileName);

            Global_Parameters__c avalara_UserName = new Global_Parameters__c();
            avalara_UserName.Name = 'Avalara_UserName';
            avalara_UserName.Value__c = '1100122265';
            globalParametersList.add(avalara_UserName);
            
            Global_Parameters__c avalara_EndPoint = new Global_Parameters__c();
            avalara_EndPoint.Name = 'Avalara_EndPoint';
            avalara_EndPoint.Value__c = 'https://development.avalara.net/';
            globalParametersList.add(avalara_EndPoint);

            Global_Parameters__c isDeploy = new Global_Parameters__c();
            isDeploy.Name = 'isDeploy';
            isDeploy.Value__c = 'false';
            globalParametersList.add(isDeploy);

      // GJK: 01/12/06  - Updated maxSalesSummaryDate Custom Settng to meet Phase 2 - Sales Summary Enhancements 
      //          - Added Attribure4__c to hold the Week
            Global_Parameters__c maxSalesSummaryDate = new Global_Parameters__c();
            maxSalesSummaryDate.Name = 'maxSalesSummaryDate';
            maxSalesSummaryDate.Value__c = '2016-2';    // <Year>-<Period>
            maxSalesSummaryDate.Attribute1__c = '2';     // Period
            maxSalesSummaryDate.Attribute2__c = 'Q1';    // Quarter
            maxSalesSummaryDate.Attribute3__c = '2016';    // Year
            maxSalesSummaryDate.Attribute4__c = 'W12';    // Week
            maxSalesSummaryDate.Date__c = Date.newInstance(2016, 1, 1);
            globalParametersList.add(maxSalesSummaryDate);
            
            insert globalParametersList;

            List<reg_steps__c> registrationStepsList = new List<reg_steps__c>();

            reg_steps__c banfieldSteps = new reg_steps__c();
            banfieldSteps.Name = 'Banfield';
            banfieldSteps.Steps__c = 'Banfield_Employee_Account_Info';
            registrationStepsList.add(banfieldSteps);

            reg_steps__c catBreederSteps = new reg_steps__c();
            catBreederSteps.Name = 'CatBreeder';
            catBreederSteps.Steps__c = 'Breeder_Account_Info,Breeder_Business_Information';
            registrationStepsList.add(catBreederSteps);

            reg_steps__c clinicSteps = new reg_steps__c();
            clinicSteps.Name = 'Clinic';
            clinicSteps.Steps__c = 'Clinic_Feeding_Account_Info,Clinic_Feeding_Business_Information';
            registrationStepsList.add(clinicSteps);

            reg_steps__c dogBreederSteps = new reg_steps__c();
            dogBreederSteps.Name = 'DogBreeder';
            dogBreederSteps.Steps__c = 'Breeder_Account_Info,Breeder_Business_Information';
            registrationStepsList.add(dogBreederSteps);

            reg_steps__c rcSteps = new reg_steps__c();
            rcSteps.Name = 'RC';
            rcSteps.Steps__c = 'RC_Employee_Account_Info';
            registrationStepsList.add(rcSteps);

            reg_steps__c retailSteps = new reg_steps__c();
            retailSteps.Name = 'Retail';
            retailSteps.Steps__c = 'Retail_Account_Info,Retail_Account_Information';
            registrationStepsList.add(retailSteps);

            reg_steps__c shelterSteps = new reg_steps__c();
            shelterSteps.Name = 'Shelter';
            shelterSteps.Steps__c = 'Shelter_Account_Info,Shelter_Business_Information';
            registrationStepsList.add(shelterSteps);

            reg_steps__c universitySteps = new reg_steps__c();
            universitySteps.Name = 'University';
            universitySteps.Steps__c = 'University_Feeding_Account_Info,University_Feeding_Business_Information';
            registrationStepsList.add(universitySteps);

            reg_steps__c vetSteps = new reg_steps__c();
            vetSteps.Name = 'Vet';
            vetSteps.Steps__c = 'Vet_Account_Info,Vet_Business_Info,Vet_Payment_Options';
            registrationStepsList.add(vetSteps);

            reg_steps__c workingDogSteps = new reg_steps__c();
            workingDogSteps.Name = 'WorkingDog';
            workingDogSteps.Steps__c = 'Working_Dog_Account_Info,Working_Dog_Business_Information';
            registrationStepsList.add(workingDogSteps);

            insert registrationStepsList;

            List<Registration_Record_Type_Lookup__c> registrationRecordTypeLookupList = new List<Registration_Record_Type_Lookup__c>();

            Registration_Record_Type_Lookup__c banfieldLookup = new Registration_Record_Type_Lookup__c();
            banfieldLookup.Name = 'Banfield';
            banfieldLookup.Record_Type_Dev_Name__c = 'Banfield_Associate';
            registrationRecordTypeLookupList.add(banfieldLookup);

            Registration_Record_Type_Lookup__c catBreederLookup = new Registration_Record_Type_Lookup__c();
            catBreederLookup.Name = 'CatBreeder';
            catBreederLookup.Record_Type_Dev_Name__c = 'Cat_Breeder';
            registrationRecordTypeLookupList.add(catBreederLookup);

            Registration_Record_Type_Lookup__c clinicLookup = new Registration_Record_Type_Lookup__c();
            clinicLookup.Name = 'Clinic';
            clinicLookup.Record_Type_Dev_Name__c = 'Clinic_Feeding';
            registrationRecordTypeLookupList.add(clinicLookup);

            Registration_Record_Type_Lookup__c dogBreederLookup = new Registration_Record_Type_Lookup__c();
            dogBreederLookup.Name = 'DogBreeder';
            dogBreederLookup.Record_Type_Dev_Name__c = 'Banfield_Associate';
            registrationRecordTypeLookupList.add(dogBreederLookup);

            Registration_Record_Type_Lookup__c rcLookup = new Registration_Record_Type_Lookup__c();
            rcLookup.Name = 'RC';
            rcLookup.Record_Type_Dev_Name__c = 'RC_Associate';
            registrationRecordTypeLookupList.add(rcLookup);

            Registration_Record_Type_Lookup__c retailLookup = new Registration_Record_Type_Lookup__c();
            retailLookup.Name = 'Retail';
            retailLookup.Record_Type_Dev_Name__c = 'Retailer';
            registrationRecordTypeLookupList.add(retailLookup);

            Registration_Record_Type_Lookup__c shelterLookup = new Registration_Record_Type_Lookup__c();
            shelterLookup.Name = 'Shelter';
            shelterLookup.Record_Type_Dev_Name__c = 'Shelter';
            registrationRecordTypeLookupList.add(shelterLookup);

            Registration_Record_Type_Lookup__c universityLookup = new Registration_Record_Type_Lookup__c();
            universityLookup.Name = 'University';
            universityLookup.Record_Type_Dev_Name__c = 'University_Feeding';
            registrationRecordTypeLookupList.add(universityLookup);

            Registration_Record_Type_Lookup__c vetLookup = new Registration_Record_Type_Lookup__c();
            vetLookup.Name = 'Vet';
            vetLookup.Record_Type_Dev_Name__c = 'Veterinary';
            registrationRecordTypeLookupList.add(vetLookup);

            Registration_Record_Type_Lookup__c workingDogLookup = new Registration_Record_Type_Lookup__c();
            workingDogLookup.Name = 'WorkingDog';
            workingDogLookup.Record_Type_Dev_Name__c = 'Working_Dog';
            registrationRecordTypeLookupList.add(workingDogLookup);

            insert registrationRecordTypeLookupList;

            List<Registration_Record_Type_Mapping__c> registrationRecordTypeMappingList = new List<Registration_Record_Type_Mapping__c>();

            Registration_Record_Type_Mapping__c banfieldMapping = new Registration_Record_Type_Mapping__c();
            banfieldMapping.Name = 'Banfield_Associate';
            banfieldMapping.Customer_Type__c = 'Banfield';
            banfieldMapping.Pillar__c = 'Vet';
            registrationRecordTypeMappingList.add(banfieldMapping);

            Registration_Record_Type_Mapping__c catBreederMapping = new Registration_Record_Type_Mapping__c();
            catBreederMapping.Name = 'Cat_Breeder';
            catBreederMapping.Customer_Type__c = 'CatBreeder';
            catBreederMapping.Pillar__c = 'Influencer';
            registrationRecordTypeMappingList.add(catBreederMapping);

            Registration_Record_Type_Mapping__c clinicMapping = new Registration_Record_Type_Mapping__c();
            clinicMapping.Name = 'Clinic_Feeding';
            clinicMapping.Customer_Type__c = 'Clinic';
            clinicMapping.Pillar__c = 'Vet';
            registrationRecordTypeMappingList.add(clinicMapping);

            Registration_Record_Type_Mapping__c dogBreederMapping = new Registration_Record_Type_Mapping__c();
            dogBreederMapping.Name = 'Dog_Breeder';
            dogBreederMapping.Customer_Type__c = 'DogBreeder';
            dogBreederMapping.Pillar__c = 'Influencer';
            registrationRecordTypeMappingList.add(dogBreederMapping);

            Registration_Record_Type_Mapping__c rcMapping = new Registration_Record_Type_Mapping__c();
            rcMapping.Name = 'RC_Associate';
            rcMapping.Customer_Type__c = 'RC';
            rcMapping.Pillar__c = 'Vet';
            registrationRecordTypeMappingList.add(rcMapping);

            Registration_Record_Type_Mapping__c retailerMapping = new Registration_Record_Type_Mapping__c();
            retailerMapping.Name = 'Retailer';
            retailerMapping.Customer_Type__c = 'Retail';
            retailerMapping.Pillar__c = 'Retail';
            registrationRecordTypeMappingList.add(retailerMapping);

            Registration_Record_Type_Mapping__c shelterMapping = new Registration_Record_Type_Mapping__c();
            shelterMapping.Name = 'Shelter';
            shelterMapping.Customer_Type__c = 'Shelter';
            shelterMapping.Pillar__c = 'Vet';
            registrationRecordTypeMappingList.add(shelterMapping);

            Registration_Record_Type_Mapping__c universityMapping = new Registration_Record_Type_Mapping__c();
            universityMapping.Name = 'University_Feeding';
            universityMapping.Customer_Type__c = 'University';
            universityMapping.Pillar__c = 'Vet';
            registrationRecordTypeMappingList.add(universityMapping);

            Registration_Record_Type_Mapping__c vetMapping = new Registration_Record_Type_Mapping__c();
            vetMapping.Name = 'Veterinary';
            vetMapping.Customer_Type__c = 'Vet';
            vetMapping.Pillar__c = 'Vet';
            registrationRecordTypeMappingList.add(vetMapping);

            Registration_Record_Type_Mapping__c workingDogMapping = new Registration_Record_Type_Mapping__c();
            workingDogMapping.Name = 'Working_Dog';
            workingDogMapping.Customer_Type__c = 'WorkingDog';
            workingDogMapping.Pillar__c = 'Vet';
            registrationRecordTypeMappingList.add(workingDogMapping);

            insert registrationRecordTypeMappingList;

            List<Focused_opp_RecType_Mapping__c> focusedOppRecTypeMappingList = new List<Focused_opp_RecType_Mapping__c>();

            Focused_opp_RecType_Mapping__c veterinarianMapping = new Focused_opp_RecType_Mapping__c();
            veterinarianMapping.Name = 'Veterinary';
            veterinarianMapping.AccRecType__c = 'Veterinarian';
            veterinarianMapping.OppRecType__c = 'Vet_Channel';
            focusedOppRecTypeMappingList.add(veterinarianMapping);

            insert focusedOppRecTypeMappingList;
        }
    } 

    public static void createChannelRelatedCustomSettings() {
        User user = [Select Id From User Where Id =: UserInfo.getUserId()];
        System.runAs(user){
            List<channel_related_recordtypes__c> channelRecordTypesList = new List<channel_related_recordtypes__c>();

            channel_related_recordtypes__c chanellRecorType = new channel_related_recordtypes__c();
            chanellRecorType.Name = 'Retaillll';
            chanellRecorType.Record_Types__c = 'Retailer,Retail Sell-to';
            channelRecordTypesList.add(chanellRecorType);

            channel_related_recordtypes__c chanellRecorType2 = new channel_related_recordtypes__c();
            chanellRecorType2.Name = 'Vetttt';
            chanellRecorType2.Record_Types__c = 'Veterinary,Vet Sell-To';
            channelRecordTypesList.add(chanellRecorType2);

            insert channelRecordTypesList;
         }
    }

    public static void createMetricTypesforSalesSummaryCustomSettings() {
        User user = [Select Id From User Where Id =: UserInfo.getUserId()];
        System.runAs(user){
            List<Metric_Types_for_Sales_Summary__c> metricTypesList = new List<Metric_Types_for_Sales_Summary__c>();
            
            Metric_Types_for_Sales_Summary__c metricType;

            metricType = new Metric_Types_for_Sales_Summary__c();
            metricType.Name = 'DM Full Truck Load Sales';
            metricTypesList.add(metricType);

            metricType = new Metric_Types_for_Sales_Summary__c();
            metricType.Name = 'DM Independent Retailer Sales';
            metricTypesList.add(metricType);

            metricType = new Metric_Types_for_Sales_Summary__c();
            metricType.Name = 'DM PRO Sales';
            metricTypesList.add(metricType);

            metricType = new Metric_Types_for_Sales_Summary__c();
            metricType.Name = 'DM Vet Clinic CFP Sales';
            metricTypesList.add(metricType);

            metricType = new Metric_Types_for_Sales_Summary__c();
            metricType.Name = 'DM Vet Diet Sales (at PetSmart)';
            metricTypesList.add(metricType);

            metricType = new Metric_Types_for_Sales_Summary__c();
            metricType.Name = 'OLP DM Sales';
            metricTypesList.add(metricType);

            metricType = new Metric_Types_for_Sales_Summary__c();
            metricType.Name = 'Vet DM OLP Drop Ship Sales';
            metricTypesList.add(metricType);

            insert metricTypesList;
         }
    }

    public static Customer_Registration__c createCustomerRegistration() {
        Customer_Registration__c reg = new Customer_Registration__c();
        reg.Email_Address__c = 'test@test.com';
        RecordType rt = [select id from RecordType where sobjectType = 'Customer_Registration__c' and developerName='Retailer' limit 1 ];
        reg.RecordTypeId = rt.id;
        return reg;
    }

    public static Customer_Registration__c createBanefieldCustomerRegistration() {
        Customer_Registration__c reg = new Customer_Registration__c();
        reg.Email_Address__c = 'test@test.com';
        RecordType rt = [select id from RecordType where sobjectType = 'Customer_Registration__c' and developerName='Banfield_Associate' limit 1 ];
        reg.RecordTypeId = rt.id;
        reg.Dept_Store_Number__c = '1234';
        reg.Customer_Type__c = 'Banfield Associate';
        return reg;
    }
    
    public static Customer_Registration__c createVetCustomerRegistration() {
        Customer_Registration__c reg = new Customer_Registration__c();
        reg.Email_Address__c = 'jtester1@gmail.com';
        reg.Name_of_Business_Organization__c = 'test reg';
        reg.First_Name__c = 'Joe';
        reg.Last_Name__c = 'Tester';
        reg.Phone_Number__c = '(234) 432-3456';
        reg.Fax_Number__c = '(987) 485-8675';
        reg.Salutation_Preference__c = 'Dr.';
        reg.Practioner_First_Name__c = 'myPracFName';
        reg.Practioner_Last_Name__c = 'myPracLName';
        reg.DVM_Vet_License_Number__c = '475886';
        reg.DVM_Vet_License_Exp_Date__c = Date.today().addDays(3);
        reg.Practioner_Email_Address__c = 'jtester2@gmail.com';
        reg.Billing_Street_Address1__c = '123 Main St';
        reg.Billing_City__c = 'Cleveland';
        reg.Bill_To_State__c = 'OH';
        reg.Billing_Zip_Code__c = '44444';
        reg.Shipping_Street_Address1__c = reg.Billing_Street_Address1__c;
        reg.Shipping_City__c = reg.Billing_City__c;
        reg.Ship_To_State__c = reg.Bill_To_State__c;
        reg.Shipping_Zip_Code__c = reg.Billing_Zip_Code__c;
        reg.Tax_ID__c = '12-1234567';
        reg.Estimated_Monthly_Purchases__c = 'Up to $1,000';
        reg.Payment_Options__c = 'ACH Bank Debit';
        reg.Accounts_Payable_Contact_First_Name__c = 'APFName';
        reg.Accounts_Payable_Contact_Last_Name__c = 'APLName';
        reg.AP_Email__c = 'APemail@google.com';
        reg.Signee_Name__c = 'Signee Name';
        reg.Signee_Title__c = 'SigneeTitle';
        reg.Upload_Complete_Practitioner__c = true;
        reg.Upload_Complete_Resale__c = true;
        reg.Upload_Complete_Tax__c = true;
        return reg;
    }
    
    public static void createSalesSummaryData(integer numAccounts){
    Datetime ssTime;
    String ssYear, ssQuarter, ssPeriod, ssWeek;
    Datetime ssPeriodStartDate, ssPeriodEndDate;
    integer numSSRecords = 50;

        createAccounts(numAccounts);
        
        List <Sales_Summary__c> ssList = new List <Sales_Summary__c>();
        integer i=0;
        for (Account a: testAccounts){

      // GJK: 01/12/06  - Updated Sales Summary data to meet Phase 2 - Sales Summary Enhancements 
      //          - Add data for current year
      //          - Add data for prior year

      integer currentYear = integer.valueOf(Datetime.now().format('y'));
      Date currentYearStartDate  = Date.newInstance(currentYear, 1, 1);

            //for (integer j = 1; j <= 365; j++){
            for (integer j = 1; j <= numSSRecords; j++){
        ssTime = currentYearStartDate.addDays(j);
        
        ssYear        = String.valueOf(ssTime.year());
        ssQuarter      = String.valueOf(integer.valueOf(math.ceil(ssTime.month()/3)));
        ssPeriod      = String.valueOf(ssTime.month());
        ssWeek        = 'W' + ssTime.format('w');
        ssPeriodStartDate  = Date.newInstance(currentYear, integer.valueOf(ssTime.month()), 1);
        ssPeriodEndDate    = ssPeriodStartDate.addMonths(1).addDays(-1);

                Sales_Summary__c ss = new Sales_Summary__c(customer__c=a.id);

                //ss.Type__c           = 'Fiscal';
                ss.Year__c          = ssYear;
                ss.quarter__c        = ssQuarter;
                ss.Period__c        = integer.valueOf(ssPeriod);
                ss.week__c          = ssWeek;
                ss.period_start_date__c    = ssPeriodStartDate.date();
                ss.period_end_date__c    = ssPeriodEndDate.date();

        if (Math.mod(j, 10) > 0) {
          ss.Metric_Type__c    = 'DM PRO Sales';
        } 
                ss.Product_Category__c    = string.valueOf(j);
                ss.Tonnage__c         = j;
                ss.Total_Sales_Amount__c   = 1100;
                ss.Sales_Discount__c     = 100;
        if (Math.mod(j, 7) > 0) {
          ss.OLP__c        = true;
        } else {
          ss.OLP__c        = false;
        }

                ssList.add(ss);
            }

      integer priorYear = integer.valueOf(Datetime.now().addYears(-1).format('y'));
      Date priorYearStartDate  = Date.newInstance(priorYear, 1, 1);

            //for (integer j = 1; j <= 365; j++){
            for (integer j = 1; j <= numSSRecords; j++){
        ssTime = priorYearStartDate.addDays(j);
        
        ssYear        = String.valueOf(ssTime.year());
        ssQuarter      = String.valueOf(integer.valueOf(math.ceil(ssTime.month()/3)));
        ssPeriod      = String.valueOf(ssTime.month());
        ssWeek        = 'W' + ssTime.format('w');
        ssPeriodStartDate  = Date.newInstance(currentYear, integer.valueOf(ssTime.month()), 1);
        ssPeriodEndDate    = ssPeriodStartDate.addMonths(1).addDays(-1);

                Sales_Summary__c ss = new Sales_Summary__c(customer__c=a.id);

                //ss.Type__c           = 'Fiscal';
                ss.Year__c          = ssYear;
                ss.quarter__c        = ssQuarter;
                ss.Period__c        = integer.valueOf(ssPeriod);
                ss.week__c          = ssWeek;
                ss.period_start_date__c    = ssPeriodStartDate.date();
                ss.period_end_date__c    = ssPeriodEndDate.date();

        if (Math.mod(j, 10) > 0) {
          ss.Metric_Type__c    = 'DM PRO Sales';
        } 
                ss.Product_Category__c    = string.valueOf(j);
                ss.Tonnage__c         = j;
                ss.Total_Sales_Amount__c   = 1100;
                ss.Sales_Discount__c     = 100;
        if (Math.mod(j, 7) > 0) {
          ss.OLP__c        = true;
        } else {
          ss.OLP__c        = false;
        }

                ssList.add(ss);
            }

            i++;
        }
        insert ssList;
        testSalesSummaryRecords = ssList;
    }
    
    
    public static void createAccounts(integer numAccounts){
        User user = [Select Id From User Where Id =: UserInfo.getUserId()];
            RecordType rt = [select id from RecordType where SobjectType = 'Account' and developerName='Retailer'];
            List <Account> acctList = new List <Account>();
            for (integer i=0; i< numAccounts; i++){
                Account a = new Account( );
                a.OwnerId = user.id;
                a.Name = 'Test Account'+i;
                a.RecordTypeId = rt.id;
                a.Bill_To_Customer_ID__c = 'RC-TEST-00'+i;
                a.ShipToCustomerNo__c = 'RC-TEST-001-00'+i;
                a.BillingStreet = '123 Main Street';
                a.BillingCity = 'Apple';
                a.BillingState = 'MN';
                a.BillingPostalCode ='1234'+i;
                a.ShippingStreet = '123 Main Street';
                a.ShippingCity = 'Apple';
                a.ShippingState = 'MN';
                a.ShippingPostalCode ='1234'+i;
                a.Current_Fiscal_YTD__c = 5000*i;
                a.Customer_Type__c = 'Vet';
          // GJK: 02/03/16  - Updated Sales_Summarization_Completed__c to false to meet Phase 2 - Sales Summary Enhancements 
                a.Sales_Summarization_Completed__c = false;
                acctList.add(a);
            }
            insert acctList;
            testAccounts = acctList;
         
    }
}