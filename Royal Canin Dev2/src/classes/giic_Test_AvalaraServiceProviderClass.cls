/************************************************************************************
Version : 1.0
Name : giic_Test_AvalaraServiceProviderClass
Created Date : 05 Oct 2018
Function : test class for giic_AvalaraServiceProviderClass
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/

@isTest(SeeAllData = false)
public class giic_Test_AvalaraServiceProviderClass {
    
     // below is setup data 
    @testSetup
    static void setup(){
        //Create Admin User 
        giic_Test_DataCreationUtility.CreateAdminUser(); 
        // Create consumer account
        List<Account> testConsumerAccountList = giic_Test_DataCreationUtility.insertConsumerAccount();
        // create warehouse
        List<gii__Warehouse__c> testWareList = giic_Test_DataCreationUtility.insertWarehouse();
        SetupTestData.createCustomSettings();
        // create system policy
        giic_Test_DataCreationUtility.testSystemPolicyCreation2();
        // create  Account refrence
        List<gii__AccountAdd__c> testAccRefList = giic_Test_DataCreationUtility.getAccountReference(testConsumerAccountList);
        Database.insert(testAccRefList);
        // create products and product reference
        List<gii__Product2Add__c> testProductList = giic_Test_DataCreationUtility.insertProduct();
        
        // create Sales Order
        List<gii__SalesOrder__c> testSOList = giic_Test_DataCreationUtility.insertSalesOrderUnreleased(testConsumerAccountList[0].Id, testAccRefList[0].Id);
        
        // create Sales Order line
        List<gii__SalesOrderLine__c> testSOLList = giic_Test_DataCreationUtility.insertSOLine(testSOList[0].Id, testProductList[0].Id);
    }   
    
    //To test for To test for postive scenarios    
    private static testMethod void test_CalTaxCommitFalse_Scenario1() {        
        
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];        
        System.Runas(u)        
        {        
            gii__SalesOrder__c objSO = [SELECT Id, gii__Released__c,giic_CCAuthorized__c,gii__PaymentMethod__c,gii__ShipToStateProvince__c, gii__TotalWeight__c, gii__Account__r.Blocked__c FROM gii__SalesOrder__c LIMIT 1];            
            objSO.giic_TaxCal__c = false;
            update objSO;
            Test.startTest();
                Test.setMock(HttpCalloutMock.class, new giic_Test_AvalaraCalloutMockImpl());  
                string result = giic_AvalaraServiceProviderClass.CalculateTaxCommitFalse(objSO.id); 
                System.assertEquals(giic_Constants.AVARALA_SUCCESS, result);  
            Test.stopTest();
        }
    }  
    
    //To test for released sales order
    private static testMethod void test_CalTaxCommitFalse_Scenario2() {
          
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];        
        System.Runas(u)        
        {       
            List<gii__SalesOrder__c> objSOList = [SELECT Id, gii__Released__c,giic_CCAuthorized__c,gii__PaymentMethod__c,gii__ShipToStateProvince__c, gii__TotalWeight__c, gii__Account__r.Blocked__c FROM gii__SalesOrder__c ];
            objSOList[0].gii__Released__c = True;
            objSOList[0].giic_SOStatus__c = giic_Constants.SOSTATUSOPEN;
            update objSOList[0];
            Test.startTest();
                string result = giic_AvalaraServiceProviderClass.CalculateTaxCommitFalse(objSOList[0].id);   
                System.assertEquals(giic_Constants.SO_Tax_Released, result);  
            Test.stopTest();
    	}
    }
    
    //To test for already tax calculated sales order
    private static testMethod void test_CalTaxCommitFalse_Scenario3() {
          
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];        
        System.Runas(u)        
        {        
            gii__SalesOrder__c objSO = [SELECT Id, gii__Released__c,giic_CCAuthorized__c,gii__PaymentMethod__c,gii__ShipToStateProvince__c, gii__TotalWeight__c, gii__Account__r.Blocked__c FROM gii__SalesOrder__c LIMIT 1];            
            objSO.giic_TaxCal__c = true;
            update objSO;
            Test.startTest();
                string result = giic_AvalaraServiceProviderClass.CalculateTaxCommitFalse(objSO.id);  
                System.assertEquals(giic_Constants.AVARALA_ERROR_TAX_CALCUATED, result);  
            Test.stopTest();
    	}
    }
    
    //To test for errror in Avalara Response
    private static testMethod void test_CalTaxCommitFalse_Scenario4() {
          
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];        
        System.Runas(u)        
        {        
            gii__SalesOrder__c objSO = [SELECT Id, gii__Released__c,giic_CCAuthorized__c,gii__PaymentMethod__c,gii__ShipToStateProvince__c, gii__TotalWeight__c, gii__Account__r.Blocked__c FROM gii__SalesOrder__c LIMIT 1];
            objSO.giic_TaxCal__c = false;
            update objSO;
            Test.startTest();
                Test.setMock(HttpCalloutMock.class, new giic_Test_ErrAvalaraCalloutMockImpl());  
                string result = giic_AvalaraServiceProviderClass.CalculateTaxCommitFalse(objSO.id);   
                System.assertEquals(giic_Constants.AVARALA_ERROR, result);  
            Test.stopTest();
    	}
    }  
    
    //To test for exception
    private static testMethod void test_CalTaxCommitFalse_Scenario5() {
          
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        
        System.Runas(u)        
        {        
            gii__SalesOrder__c objSO = [SELECT Id, gii__Released__c,giic_CCAuthorized__c,gii__PaymentMethod__c,gii__ShipToStateProvince__c, gii__TotalWeight__c, gii__Account__r.Blocked__c FROM gii__SalesOrder__c LIMIT 1];
            Test.startTest();
                giic_AvalaraServiceProviderClass.CalculateTaxCommitFalse(objSO.id);   
            Test.stopTest();
    	}
    }
    
    // To test for postive scenarios
    private static testMethod void test_CalculateTaxforSO_Scenario1() {
        
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];        
        System.Runas(u)        
        {    
            gii__Warehouse__c objWH = [select id, giic_WarehouseCode__c from gii__Warehouse__c limit 1];
            gii__SalesOrder__c objSO = [SELECT Id, gii__Released__c,giic_CCAuthorized__c,gii__PaymentMethod__c,gii__ShipToStateProvince__c, gii__TotalWeight__c, gii__Account__r.Blocked__c FROM gii__SalesOrder__c LIMIT 1];
            
            gii__SalesOrderLine__c objSOL = [SELECT Id,Name, gii__OrderQuantity__c, gii__CancelledQuantity__c, gii__ExtendedWeight__c, gii__UnitWeight__c   
                                                    FROM gii__SalesOrderLine__c where gii__SalesOrder__c =: objSO.Id LIMIT 1];
        
            List<String> lstSOLName = new List<String>();
            lstSOLName.add(objSOL.Name);
            Test.startTest();
                Test.setMock(HttpCalloutMock.class, new giic_Test_AvalaraCalloutMockImpl());  
                giic_AvalaraServiceProviderClass.CalculateTaxforSO(objSO.id, lstSOLName,objWH.giic_WarehouseCode__c, giic_Constants.CREATE_T, false);
            Test.stopTest();
        }
    }
    
    // to test for no SOL
    private static testMethod void test_CalculateTaxforSO_Scenario2() {
        
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        
        System.Runas(u)        
        {    
            gii__Warehouse__c objWH = [select id, giic_WarehouseCode__c from gii__Warehouse__c limit 1];
            gii__SalesOrder__c objSO = [SELECT Id, gii__Released__c,giic_CCAuthorized__c,gii__PaymentMethod__c,gii__ShipToStateProvince__c, gii__TotalWeight__c, gii__Account__r.Blocked__c FROM gii__SalesOrder__c LIMIT 1];
                        
            gii__SalesOrderLine__c objSOL = [SELECT Id,Name, gii__OrderQuantity__c, gii__CancelledQuantity__c, gii__ExtendedWeight__c, gii__UnitWeight__c   
                                                    FROM gii__SalesOrderLine__c where gii__SalesOrder__c =: objSO.Id LIMIT 1];
        
            List<String> lstSOLName = new List<String>();
            lstSOLName.add(objSOL.Name);
            Test.startTest();
                Test.setMock(HttpCalloutMock.class, new giic_Test_AvalaraCalloutMockImpl());
                giic_AvalaraServiceProviderClass.CalculateTaxforSO(objSO.id, null,objWH.giic_WarehouseCode__c, giic_Constants.CREATE_T, false);
            Test.stopTest();
        }
    }
    
    // To test for exception
    private static testMethod void test_CalculateTaxforSO_Scenario3() {
        
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        System.Runas(u)        
        {
            gii__Warehouse__c objWH = [select id, giic_WarehouseCode__c from gii__Warehouse__c limit 1];
            gii__SalesOrder__c objSO = [SELECT Id, gii__Released__c,giic_CCAuthorized__c,gii__PaymentMethod__c,gii__ShipToStateProvince__c, gii__TotalWeight__c, gii__Account__r.Blocked__c FROM gii__SalesOrder__c LIMIT 1];

            gii__SalesOrderLine__c objSOL = [SELECT Id,Name, gii__OrderQuantity__c, gii__CancelledQuantity__c, gii__ExtendedWeight__c, gii__UnitWeight__c   
                                                    FROM gii__SalesOrderLine__c where gii__SalesOrder__c =: objSO.Id LIMIT 1];
            
            List<String> lstSOLName = new List<String>();
            lstSOLName.add(objSOL.Name);
            Test.startTest();  
                Test.setMock(HttpCalloutMock.class, new giic_Test_AvalaraCalloutMockImpl());
                giic_AvalaraServiceProviderClass.CalculateTaxforSO(objSO.id, lstSOLName,objWH.giic_WarehouseCode__c, giic_Constants.CREATE_T, false);
            Test.stopTest();
            
        }
    }
    
    private static testMethod void test_CalculateTaxforSO_Scenario4() {
        
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];        
        System.Runas(u)        
        {    
            gii__Warehouse__c objWH = [select id, giic_WarehouseCode__c from gii__Warehouse__c limit 1];
            gii__SalesOrder__c objSO = [SELECT Id, gii__Released__c,giic_CCAuthorized__c,gii__PaymentMethod__c,gii__ShipToStateProvince__c, gii__TotalWeight__c, gii__Account__r.Blocked__c FROM gii__SalesOrder__c LIMIT 1];
            
            gii__SalesOrderLine__c objSOL = [SELECT Id,Name, gii__OrderQuantity__c, gii__CancelledQuantity__c, gii__ExtendedWeight__c, gii__UnitWeight__c   
                                                    FROM gii__SalesOrderLine__c where gii__SalesOrder__c =: objSO.Id LIMIT 1];
        
            List<String> lstSOLName = new List<String>();
            lstSOLName.add(objSOL.Name);
            Test.startTest();
                Test.setMock(HttpCalloutMock.class, new giic_Test_ErrAvalaraCalloutMockImpl());  
                Map<String,Object> returnMap = new Map<String,Object>();
                returnMap = giic_AvalaraServiceProviderClass.CalculateTaxforSO(objSO.id, lstSOLName,objWH.giic_WarehouseCode__c, giic_Constants.CREATE_T, false);
            Test.stopTest();
        }
    }
    
    //To test for error in avalara response
    private static testMethod void test_negative_getAvalaraResponse() {
          
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        System.Runas(u)        
        { 
            gii__SalesOrder__c objSO = [SELECT Id, gii__Released__c,giic_CCAuthorized__c,gii__PaymentMethod__c,gii__ShipToStateProvince__c, gii__TotalWeight__c, gii__Account__r.Blocked__c FROM gii__SalesOrder__c LIMIT 1];
            
            String body ='{"error":""}';        
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody(body);
            res.setStatusCode(200);
            Test.startTest();   
                giic_AvalaraServiceProviderClass.getAvalaraResponse(res,objSO.id,true);
            Test.stopTest();
        }
    }
    
    //To test for without error in avalara response
    private static testMethod void test_negative_getAvalaraResponse1() {
          
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        System.Runas(u)        
        { 
            gii__SalesOrder__c objSO = [SELECT Id, gii__Released__c,giic_CCAuthorized__c,gii__PaymentMethod__c,gii__ShipToStateProvince__c, gii__TotalWeight__c, gii__Account__r.Blocked__c FROM gii__SalesOrder__c LIMIT 1];
            
            String body = '{"id":123456789,"code":"f81200e2-5f08-43dd-97df-156e93b5796a","companyId":12345,"date":"2018-10-02","status":"Committed","type":"SalesInvoice","currencyCode":"USD","customerVendorCode":"ABC","customerCode":"ABC","reconciled":true,"locationCode":"DEFAULT","salespersonCode":"DEF","taxOverrideType":"None","totalAmount":1000,"totalTax":62.5,"totalTaxable":1000,"totalTaxCalculated":62.5,"adjustmentReason":"NotAdjusted","region":"CA","country":"US","originAddressId":123456789,"destinationAddressId":123456789,"description":"Yarn","taxDate":"2018-10-02T00:00:00+00:00","lines":[{"id":123456789,"transactionId":123456789,"lineNumber":"1","description":"Yarn","destinationAddressId":12345,"originAddressId":123456789,"discountAmount":100,"isItemTaxable":true,"itemCode":"116292","lineAmount":1000,"quantity":1,"ref1":"Note: Deliver to Bob","reportingDate":"2018-10-02","sourcing":"Destination","tax":62.5,"taxableAmount":1000,"taxCalculated":62.5,"taxCode":"PS081282","taxDate":"2018-10-02","taxOverrideType":"None","details":[{"id":123456789,"transactionLineId":123456789,"transactionId":123456789,"addressId":12345,"country":"US","region":"CA","stateFIPS":"06","exemptReasonId":4,"jurisCode":"06","jurisName":"CALIFORNIA","jurisdictionId":5000531,"signatureCode":"AGAM","jurisType":"STA","nonTaxableType":"BaseRule","rate":0.0625,"rateRuleId":1321915,"rateSourceId":3,"sourcing":"Destination","tax":62.5,"taxableAmount":1000,"taxType":"Sales","taxName":"CA STATE TAX","taxAuthorityTypeId":45,"taxRegionId":2127184,"taxCalculated":62.5,"rateType":"General"}]}],"addresses":[{"boundaryLevel":"Address","line1":"100 Ravine Lane Northeast #220","city":"Bainbridge Island","region":"WA","postalCode":"98110","country":"US"}]}';
            
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody(body);
            res.setStatusCode(200);
            
            Test.startTest();   
                giic_AvalaraServiceProviderClass.getAvalaraResponse(res,objSO.id,true);
            Test.stopTest();
        }
    }
    
    //To test for To test for CollectErrors
    private static testMethod void test_CollectErrors() {
          
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        System.Runas(u)        
        {
            gii__SalesOrder__c objSO = [SELECT Id, gii__Released__c,giic_CCAuthorized__c,gii__PaymentMethod__c,gii__ShipToStateProvince__c, gii__TotalWeight__c, gii__Account__r.Blocked__c FROM gii__SalesOrder__c LIMIT 1];
            objSO.giic_TaxCal__c = false;
            update objSO;
            Test.startTest();            
                giic_AvalaraServiceProviderClass.CollectErrors(objSO.id,'ErrorTest');   
            Test.stopTest();
        }   
    }
}