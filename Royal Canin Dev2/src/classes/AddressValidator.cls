public class AddressValidator {
	public static boolean Pending_Avalara_Response = false;
	public static string StandardizedAddr = null;
	public static boolean addressValidated = true;

	@future(callout=true)
	public static void addressToValidate_Future(Set<Id> custRegIds){
		addressToValidate(custRegIds);
	}
		
	public static void addressToValidate(Set<Id> custRegIds){
	 	List<Customer_Registration__c> custRegToUpdate = new List<Customer_Registration__c>();
		Set<Id> chunkedIdsSet = new Set<Id>();
		for(Id custRegId: custRegIds){
			chunkedIdsSet.add(custRegId);
			List<Customer_Registration__c> custRegUpdateList = new List<Customer_Registration__c>();
			if(chunkedIdsSet.size()==10){
				custRegUpdateList = makeCallOut(chunkedIdsSet);
					for(Customer_Registration__c custReg : custRegUpdateList){
					custRegToUpdate.add(custReg);
				}
				chunkedIdsSet.clear();
			}
		}
		if(!chunkedIdsSet.isEmpty()){
			List<Customer_Registration__c> custRegUpdateList = new List<Customer_Registration__c>();
			custRegUpdateList = makeCallOut(chunkedIdsSet);
			for(Customer_Registration__c custReg : custRegUpdateList){
				custRegToUpdate.add(custReg);
			}
		}	

		try{
			if(Pending_Avalara_Response){
				if(custRegToUpdate != null && custRegToUpdate.size()>0){
					update custRegToUpdate;
				}
				Pending_Avalara_Response = false;
			}
		} catch(Exception ex) {
			System.debug('Error standardizing the address :'+ex.getMessage());
		}
	}
	
	public static List<Customer_Registration__c> makeCallOut(Set<ID> custRegIds){
		if(!custRegIds.isEmpty()){
			List<Customer_Registration__c> custRegList = [Select Id, Billing_Street_Address1__c, Billing_Street_Address2__c, Billing_City__c, Bill_To_State__c, Billing_Zip_Code__c, Shipping_Street_Address1__c, Shipping_Street_Address2__c, Shipping_City__c, Ship_To_State__c, Shipping_Zip_Code__c, Validated__c, Validation_Error_Reason__c, Pending_Avalara_Response__c From Customer_Registration__c Where Id IN: custRegIds];
			List<Customer_Registration__c> custRegToUpdate = new List<Customer_Registration__c>();

			for(Customer_Registration__c custReg : custRegList){
				Customer_Registration__c cr = validateAddress(custReg);	
				Pending_Avalara_Response = true;
				custRegToUpdate.add(cr);
			}
			return custRegToUpdate;
		}
		return null;		
	}
	
	public static Customer_Registration__c validateAddress(Customer_Registration__c custReg){
		if(custReg != null){
			AddressSvc.AddressSvcSoap addressSvc = new AddressSvc.AddressSvcSoap(Global_Parameters__c.getValues('Avalara_EndPoint').Value__c);
			addressSvc.Security = new AddressSvc.Security();
	        addressSvc.Security.UsernameToken.Username = Global_Parameters__c.getValues('Avalara_UserName').Value__c;
			addressSvc.Security.UsernameToken.Password = Global_Parameters__c.getValues('Avalara_Password').Value__c;
			addressSvc.Profile.Client = Global_Parameters__c.getValues('Avalara_ProfileClient').Value__c;
			addressSvc.Profile.Adapter = Global_Parameters__c.getValues('Avalara_ProfileAdapter').Value__c;
			addressSvc.Profile.Name = Global_Parameters__c.getValues('Avalara_ProfileName').Value__c;
			 
			AddressSvc.ValidateResult vres1 = null;
			AddressSvc.ValidateResult vres2 = null;
			String SampleResult1;
			String SampleResult2;
			String PostalCode;
			String StdAddr;
			List<String> Zipcode;

			AddressSvc.ValidateRequest vreq1 = new AddressSvc.ValidateRequest();
			AddressSvc.ValidateRequest vreq2 = new AddressSvc.ValidateRequest();

			vreq1.Address = new AddressSvc.BaseAddress();
			vreq1.Address.Line1 = custReg.Billing_Street_Address1__c;
			vreq1.Address.Line2 = custReg.Billing_Street_Address2__c;
			vreq1.Address.City = custReg.Billing_City__c;
			vreq1.Address.Region = custReg.Bill_To_State__c;
			vreq1.Address.PostalCode = custReg.Billing_Zip_Code__c;

			try{
				if(vreq1 != null&!Test.isRunningTest()){
					System.debug('THIS IS VREQ ' + vreq1);
					vres1=addressSvc.Validate(vreq1);
				}
			} catch(Exception ex) {
				System.debug('Billing Address Validation Excepted: ' + ex.getmessage());
				System.debug('Stack Trace');
				System.debug(ex.getStackTraceString());
			}

			if(vres1 != null){
				if(vres1.ResultCode == 'Success'){
					if(custReg.Shipping_Street_Address1__c != null){
						vreq2.Address = new AddressSvc.BaseAddress();
						vreq2.Address.Line1 = custReg.Shipping_Street_Address1__c;
						vreq2.Address.Line2 = custReg.Shipping_Street_Address2__c;
						vreq2.Address.City = custReg.Shipping_City__c;
						vreq2.Address.Region = custReg.Ship_To_State__c;
						vreq2.Address.PostalCode = custReg.Shipping_Zip_Code__c;

						try{
							if(vreq2 != null&!Test.isRunningTest()){
								vres2=addressSvc.Validate(vreq2);
							}
						} catch(Exception ex) {
							System.debug('Shipping Address Validation Excepted: ' + ex.getmessage());
							System.debug('Stack Trace');
							System.debug(ex.getStackTraceString());
						}

						if(vres2.ResultCode == 'Success' && ((vres1.ValidAddresses.ValidAddress[0].Line1.length() > 30) || 
															 (vres1.ValidAddresses.ValidAddress[0].Line2.length() > 30) ||
															 (vres2.ValidAddresses.ValidAddress[0].Line1.length() > 30) ||
															 (vres2.ValidAddresses.ValidAddress[0].Line2.length() > 30))) {
							system.debug('Response billing/shipping address line 1 or 2 is greater than 30 characters');
							custReg.Validated__c = false;
							custReg.Validation_Error_Reason__c = 'Response billing/shipping address line 1 or 2 is greater than 30 characters.';
						} else if(vres2.ResultCode == 'Success'){
							System.debug('Billing and Shipping Address Validations Returned Success');
							system.debug('response~~~~'+vres1.ValidAddresses.ValidAddress[0]);
							custReg.Billing_Street_Address1__c = vres1.ValidAddresses.ValidAddress[0].Line1;
							custReg.Billing_Street_Address2__c = vres1.ValidAddresses.ValidAddress[0].Line2;
							custReg.Billing_City__c = vres1.ValidAddresses.ValidAddress[0].City;
							custReg.Bill_To_State__c = vres1.ValidAddresses.ValidAddress[0].Region;
							PostalCode = vres1.ValidAddresses.ValidAddress[0].PostalCode;
							if (PostalCode.contains('-')){
								Zipcode = PostalCode.trim().split('-');
								custReg.Billing_Zip_Code__c = Zipcode[0];
							} else {
								custReg.Billing_Zip_Code__c = vres1.ValidAddresses.ValidAddress[0].PostalCode;
							}
							custReg.Shipping_Street_Address1__c = vres2.ValidAddresses.ValidAddress[0].Line1;
							custReg.Shipping_Street_Address2__c = vres2.ValidAddresses.ValidAddress[0].Line2;
							custReg.Shipping_City__c = vres2.ValidAddresses.ValidAddress[0].City;
							custReg.Ship_To_State__c = vres2.ValidAddresses.ValidAddress[0].Region;
							PostalCode = vres2.ValidAddresses.ValidAddress[0].PostalCode;
							if (PostalCode.contains('-')){
								Zipcode = PostalCode.trim().split('-');
								custReg.Shipping_Zip_Code__c = Zipcode[0];
							} else {
								custReg.Shipping_Zip_Code__c = vres2.ValidAddresses.ValidAddress[0].PostalCode;
							}
						
							custReg.Validated__c = true;
							custReg.Validation_Error_Reason__c = null;
							custReg.Pending_Avalara_Response__c = false;
						} else {//Error validating shipping address
							SampleResult2 = 'Shipping Address Validation Returned Other Than Success: ' + vres2.ResultCode + 
							' : ' + vres2.Messages.Message[0].Name + ' : ' + vres2.Messages.Message[0].Summary;
							system.debug(LoggingLevel.info, SampleResult2);
							custReg.Validated__c = false;
							custReg.Validation_Error_Reason__c = 'Shipping Address Validation Returned Other Than Success: '+ vres2.Messages.Message[0].Summary;
						}
					} else {//shipping address is null
						custReg.Billing_Street_Address1__c = vres1.ValidAddresses.ValidAddress[0].Line1;
						custReg.Billing_Street_Address2__c = vres1.ValidAddresses.ValidAddress[0].Line2;
						custReg.Billing_City__c = vres1.ValidAddresses.ValidAddress[0].City;
						custReg.Bill_To_State__c = vres1.ValidAddresses.ValidAddress[0].Region;
						PostalCode = vres1.ValidAddresses.ValidAddress[0].PostalCode;
						if (PostalCode.contains('-')){
							Zipcode = PostalCode.trim().split('-');
							custReg.Billing_Zip_Code__c = Zipcode[0];
						} else {
							custReg.Billing_Zip_Code__c = vres1.ValidAddresses.ValidAddress[0].PostalCode;
						}
					
						custReg.Validated__c = true;
						custReg.Validation_Error_Reason__c = null;
						custReg.Pending_Avalara_Response__c = false;
						System.debug('Shipping Address is null - Success');
					}
				} else {//Error validating billing address
					SampleResult1 = 'Billing Address Validation Returned Other Than Success: ' + vres1.ResultCode + 
					' : ' + vres1.Messages.Message[0].Name + ' : ' + vres1.Messages.Message[0].Summary;
					system.debug(LoggingLevel.info, SampleResult1);
					custReg.Validated__c = false;
					custReg.Validation_Error_Reason__c = 'Billing Address Validation Returned Other Than Success: ' + vres1.Messages.Message[0].Summary;
				}
			} else {
				System.debug('No Addresses to Validate');
			}
			return custReg;
		}
		return null; 
	}

	//for accounts
	@future(callout=true)
	public static void accountAddressToValidate_Future(Set<Id> accIds){
		system.debug('%%%%%accountAddressToValidateFuture');
		accountAddressToValidate(accIds);
	}
		
	public static void accountAddressToValidate(Set<Id> accIds){
		system.debug('%%%%%accountAddressToValidate');
	 	List<Account> accountsToUpdate = new List<Account>();
		Set<Id> chunkedIdsSet = new Set<Id>();
		for(Id accId: accIds){
			chunkedIdsSet.add(accId);
			List<Account> accountUpdateList = new List<Account>();
			if(chunkedIdsSet.size()==10){
				accountUpdateList = accountMakeCallOut(chunkedIdsSet);
					for(Account acc : accountUpdateList){
					accountsToUpdate.add(acc);
				}
				chunkedIdsSet.clear();
			}
		}
		if(!chunkedIdsSet.isEmpty()){
			List<Account> accountUpdateList = new List<Account>();
			accountUpdateList = accountMakeCallOut(chunkedIdsSet);
			for(Account acc : accountUpdateList){
				accountsToUpdate.add(acc);
			}
		}	

		try{
			if(Pending_Avalara_Response){
				if(accountsToUpdate != null && accountsToUpdate.size()>0){
					update accountsToUpdate;
				}
				Pending_Avalara_Response = false;
			}
		} catch(Exception ex) {
			System.debug('Error standardizing the address :'+ex.getMessage());
		}
	}

	public static List<Account> accountMakeCallOut(Set<ID> accIds){
		if(!accIds.isEmpty()){
			List<Account> accounts = [Select Id, BillingStreet, BillingCity, BillingState, BillingPostalCode, ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, Validated__c, Validation_Error_Reason__c, Pending_Avalara_Response__c From Account Where Id IN: accIds];
			List<Account> accountsToUpdate = new List<Account>();

			for(Account acc : accounts){
				Account ac = accountValidateAddress(acc);	
				Pending_Avalara_Response = true;
				accountsToUpdate.add(ac);
			}
			return accountsToUpdate;
		}
		return null;		
	}

	public static Account accountValidateAddress(Account acc){
		if(acc != null){
			AddressSvc.AddressSvcSoap addressSvc = new AddressSvc.AddressSvcSoap('https://development.avalara.net/');
			addressSvc.Security = new AddressSvc.Security();
	        addressSvc.Security.UsernameToken.Username = Global_Parameters__c.getValues('Avalara_UserName').Value__c;
			addressSvc.Security.UsernameToken.Password = Global_Parameters__c.getValues('Avalara_Password').Value__c;
			addressSvc.Profile.Client = Global_Parameters__c.getValues('Avalara_ProfileClient').Value__c;
			addressSvc.Profile.Adapter = Global_Parameters__c.getValues('Avalara_ProfileAdapter').Value__c;
			addressSvc.Profile.Name = Global_Parameters__c.getValues('Avalara_ProfileName').Value__c;
			 
			AddressSvc.ValidateResult vres1 = null;
			AddressSvc.ValidateResult vres2 = null;
			String SampleResult1;
			String SampleResult2;
			String PostalCode;
			String StdAddr;
			List<String> Zipcode;

			AddressSvc.ValidateRequest vreq1 = new AddressSvc.ValidateRequest();
			AddressSvc.ValidateRequest vreq2 = new AddressSvc.ValidateRequest();

			vreq1.Address = new AddressSvc.BaseAddress();
			vreq1.Address.Line1 = acc.BillingStreet;
			vreq1.Address.City = acc.BillingCity;
			vreq1.Address.Region = acc.BillingState;
			vreq1.Address.PostalCode = acc.BillingPostalCode;

			try{
				if(vreq1 != null&!Test.isRunningTest()){
					vres1=addressSvc.Validate(vreq1);
				}
			} catch(Exception ex) {
				System.debug('Billing Address Validation Excepted: ' + ex.getmessage());
				System.debug('Stack Trace');
				System.debug(ex.getStackTraceString());
			}

			if(vres1 != null){
				if(vres1.ResultCode == 'Success'){
					if(acc.ShippingStreet != null){
						vreq2.Address = new AddressSvc.BaseAddress();
						vreq2.Address.Line1 = acc.ShippingStreet;
						vreq2.Address.City = acc.ShippingCity;
						vreq2.Address.Region = acc.ShippingState;
						vreq2.Address.PostalCode = acc.ShippingPostalCode;

						try{
							if(vreq2 != null&!Test.isRunningTest()){
								vres2=addressSvc.Validate(vreq2);
							}
						} catch(Exception ex) {
							System.debug('Shipping Address Validation Exception: ' + ex.getmessage());
							System.debug('Stack Trace');
							System.debug(ex.getStackTraceString());
						}

						if(vres2.ResultCode == 'Success' && ((vres1.ValidAddresses.ValidAddress[0].Line1.length() > 30) || 
															 (vres1.ValidAddresses.ValidAddress[0].Line2.length() > 30) ||
															 (vres2.ValidAddresses.ValidAddress[0].Line1.length() > 30) ||
															 (vres2.ValidAddresses.ValidAddress[0].Line2.length() > 30))) {
							system.debug('Response billing/shipping address line 1 or 2 is greater than 30 characters');
							acc.Validated__c = false;
							acc.Validation_Error_Reason__c = 'Response billing/shipping address line 1 or 2 is greater than 30 characters.';
						} else if(vres2.ResultCode == 'Success'){
							System.debug('Billing and Shipping Address Validations Returned Success');
							system.debug('response~~~~'+vres1.ValidAddresses.ValidAddress[0]);
							acc.BillingStreet = vres1.ValidAddresses.ValidAddress[0].Line1;
							system.debug(vres1.ValidAddresses.ValidAddress[0].Line1);
							acc.BillingCity = vres1.ValidAddresses.ValidAddress[0].City;
							system.debug(vres1.ValidAddresses.ValidAddress[0].City);
							acc.BillingState = vres1.ValidAddresses.ValidAddress[0].Region;
							system.debug(vres1.ValidAddresses.ValidAddress[0].Region);
							PostalCode = vres1.ValidAddresses.ValidAddress[0].PostalCode;
							system.debug(vres1.ValidAddresses.ValidAddress[0].PostalCode);
							if (PostalCode.contains('-')){
								Zipcode = PostalCode.trim().split('-');
								acc.BillingPostalCode = Zipcode[0];
							} else {
								acc.BillingPostalCode = vres1.ValidAddresses.ValidAddress[0].PostalCode;
							}
							acc.ShippingStreet = vres2.ValidAddresses.ValidAddress[0].Line1;
							acc.ShippingCity = vres2.ValidAddresses.ValidAddress[0].City;
							acc.ShippingState = vres2.ValidAddresses.ValidAddress[0].Region;
							PostalCode = vres2.ValidAddresses.ValidAddress[0].PostalCode;
							if (PostalCode.contains('-')){
								Zipcode = PostalCode.trim().split('-');
								acc.ShippingPostalCode = Zipcode[0];
							} else {
								acc.ShippingPostalCode = vres2.ValidAddresses.ValidAddress[0].PostalCode;
							}
						
							acc.Validated__c = true;
							acc.Validation_Error_Reason__c = null;
							acc.Pending_Avalara_Response__c = false;
						} else {//Error validating shipping address
							SampleResult2 = 'Shipping Address Validation Returned Other Than Success: ' + vres2.ResultCode + 
							' : ' + vres2.Messages.Message[0].Name + ' : ' + vres2.Messages.Message[0].Summary;
							system.debug(LoggingLevel.info, SampleResult2);
							acc.Validated__c = false;
							acc.Validation_Error_Reason__c = 'Shipping Address Validation Returned Other Than Success: '+ vres2.Messages.Message[0].Summary;
						}
					} else {//shipping address is null
						acc.BillingStreet = vres1.ValidAddresses.ValidAddress[0].Line1;
						acc.BillingCity = vres1.ValidAddresses.ValidAddress[0].City;
						acc.BillingState = vres1.ValidAddresses.ValidAddress[0].Region;
						PostalCode = vres1.ValidAddresses.ValidAddress[0].PostalCode;
						if (PostalCode.contains('-')){
							Zipcode = PostalCode.trim().split('-');
							acc.BillingPostalCode = Zipcode[0];
						} else {
							acc.BillingPostalCode = vres1.ValidAddresses.ValidAddress[0].PostalCode;
						}
					
						acc.Validated__c = true;
						acc.Validation_Error_Reason__c = null;
						acc.Pending_Avalara_Response__c = false;
						System.debug('Shipping Address is null - Success');
					}
				} else {//Error validating billing address
					SampleResult1 = 'Billing Address Validation Returned Other Than Success: ' + vres1.ResultCode + 
					' : ' + vres1.Messages.Message[0].Name + ' : ' + vres1.Messages.Message[0].Summary;
					system.debug(LoggingLevel.info, SampleResult1);
					acc.Validated__c = false;
					acc.Validation_Error_Reason__c = 'Billing Address Validation Returned Other Than Success: ' + vres1.Messages.Message[0].Summary;
				}
			} else {
				System.debug('No Addresses to Validate');
			}
			return acc;
		}
		return null; 
	}

	//for contacts
	@future(callout=true)
	public static void contactAddressToValidate_Future(Set<Id> conIds){
		system.debug('%%%%%contactAddressToValidateFuture');
		contactAddressToValidate(conIds);
	}
		
	public static void contactAddressToValidate(Set<Id> conIds){
		system.debug('%%%%%contactAddressToValidate');
	 	List<Contact> contactsToUpdate = new List<Contact>();
		Set<Id> chunkedIdsSet = new Set<Id>();
		for(Id conId: conIds){
			chunkedIdsSet.add(conId);
			List<Contact> contactUpdateList = new List<Contact>();
			if(chunkedIdsSet.size()==10){
				contactUpdateList = contactMakeCallOut(chunkedIdsSet);
					for(Contact con : contactUpdateList){
					contactsToUpdate.add(con);
				}
				chunkedIdsSet.clear();
			}
		}
		if(!chunkedIdsSet.isEmpty()){
			List<Contact> contactUpdateList = new List<Contact>();
			contactUpdateList = contactMakeCallout(chunkedIdsSet);
			for(Contact con : contactUpdateList){
				contactsToUpdate.add(con);
			}
		}	

		try{
			if(Pending_Avalara_Response){
				if(contactsToUpdate != null && contactsToUpdate.size()>0){
					update contactsToUpdate;
				}
				Pending_Avalara_Response = false;
			}
		} catch(Exception ex) {
			System.debug('Error standardizing the address :'+ex.getMessage());
		}
	}

	public static List<Contact> contactMakeCallout(Set<ID> conIds){
		if(!conIds.isEmpty()){
			List<Contact> contacts = [Select Id, MailingStreet, MailingCity, MailingState, MailingPostalCode, OtherStreet, OtherCity, OtherState, OtherPostalCode, Validated__c, Validation_Error_Reason__c, Pending_Avalara_Response__c From Contact Where Id IN: conIds];
			List<Contact> contactsToUpdate = new List<Contact>();

			for(Contact contact : contacts){
				Contact con = contactValidateAddress(contact);	
				Pending_Avalara_Response = true;
				contactsToUpdate.add(con);
			}
			return contactsToUpdate;
		}
		return null;		
	}

	public static Contact contactValidateAddress(Contact con){
		if(con != null){
			AddressSvc.AddressSvcSoap addressSvc = new AddressSvc.AddressSvcSoap('https://development.avalara.net/');
			addressSvc.Security = new AddressSvc.Security();
	        addressSvc.Security.UsernameToken.Username = Global_Parameters__c.getValues('Avalara_UserName').Value__c;
			addressSvc.Security.UsernameToken.Password = Global_Parameters__c.getValues('Avalara_Password').Value__c;
			addressSvc.Profile.Client = Global_Parameters__c.getValues('Avalara_ProfileClient').Value__c;
			addressSvc.Profile.Adapter = Global_Parameters__c.getValues('Avalara_ProfileAdapter').Value__c;
			addressSvc.Profile.Name = Global_Parameters__c.getValues('Avalara_ProfileName').Value__c;
			 
			AddressSvc.ValidateResult vres1 = null;
			AddressSvc.ValidateResult vres2 = null;
			String SampleResult1;
			String SampleResult2;
			String PostalCode;
			String StdAddr;
			List<String> Zipcode;

			AddressSvc.ValidateRequest vreq1 = new AddressSvc.ValidateRequest();
			AddressSvc.ValidateRequest vreq2 = new AddressSvc.ValidateRequest();

			vreq1.Address = new AddressSvc.BaseAddress();
			vreq1.Address.Line1 = con.MailingStreet;
			vreq1.Address.City = con.MailingCity;
			vreq1.Address.Region = con.MailingState;
			vreq1.Address.PostalCode = con.MailingPostalCode;

			try{
				if(vreq1 != null&!Test.isRunningTest()){
					vres1=addressSvc.Validate(vreq1);
				}
			} catch(Exception ex) {
				System.debug('Billing Address Validation Excepted: ' + ex.getmessage());
				System.debug('Stack Trace');
				System.debug(ex.getStackTraceString());
			}

			if(vres1 != null){
				if(vres1.ResultCode == 'Success'){
					if(con.OtherStreet != null){
						vreq2.Address = new AddressSvc.BaseAddress();
						vreq2.Address.Line1 = con.OtherStreet;
						vreq2.Address.City = con.OtherCity;
						vreq2.Address.Region = con.OtherState;
						vreq2.Address.PostalCode = con.OtherPostalCode;

						try{
							if(vreq2 != null&!Test.isRunningTest()){
								vres2=addressSvc.Validate(vreq2);
							}
						} catch(Exception ex) {
							System.debug('Shipping Address Validation Exception: ' + ex.getmessage());
							System.debug('Stack Trace');
							System.debug(ex.getStackTraceString());
						}

						if(vres2.ResultCode == 'Success' && ((vres1.ValidAddresses.ValidAddress[0].Line1.length() > 30) || 
															 (vres1.ValidAddresses.ValidAddress[0].Line2.length() > 30) ||
															 (vres2.ValidAddresses.ValidAddress[0].Line1.length() > 30) ||
															 (vres2.ValidAddresses.ValidAddress[0].Line2.length() > 30))) {
							system.debug('Response billing/shipping address line 1 or 2 is greater than 30 characters');
							con.Validated__c = false;
							con.Validation_Error_Reason__c = 'Response billing/shipping address line 1 or 2 is greater than 30 characters.';
						} else if(vres2.ResultCode == 'Success'){
							System.debug('Billing and Shipping Address Validations Returned Success');
							system.debug('response~~~~'+vres1.ValidAddresses.ValidAddress[0]);
							con.MailingStreet = vres1.ValidAddresses.ValidAddress[0].Line1;
							system.debug(vres1.ValidAddresses.ValidAddress[0].Line1);
							con.MailingCity = vres1.ValidAddresses.ValidAddress[0].City;
							system.debug(vres1.ValidAddresses.ValidAddress[0].City);
							con.MailingState = vres1.ValidAddresses.ValidAddress[0].Region;
							system.debug(vres1.ValidAddresses.ValidAddress[0].Region);
							PostalCode = vres1.ValidAddresses.ValidAddress[0].PostalCode;
							system.debug(vres1.ValidAddresses.ValidAddress[0].PostalCode);
							if (PostalCode.contains('-')){
								Zipcode = PostalCode.trim().split('-');
								con.MailingPostalCode = Zipcode[0];
							} else {
								con.MailingPostalCode = vres1.ValidAddresses.ValidAddress[0].PostalCode;
							}
							con.OtherStreet = vres2.ValidAddresses.ValidAddress[0].Line1;
							con.OtherCity = vres2.ValidAddresses.ValidAddress[0].City;
							con.OtherState = vres2.ValidAddresses.ValidAddress[0].Region;
							PostalCode = vres2.ValidAddresses.ValidAddress[0].PostalCode;
							if (PostalCode.contains('-')){
								Zipcode = PostalCode.trim().split('-');
								con.OtherPostalCode = Zipcode[0];
							} else {
								con.OtherPostalCode = vres2.ValidAddresses.ValidAddress[0].PostalCode;
							}
						
							con.Validated__c = true;
							con.Validation_Error_Reason__c = null;
							con.Pending_Avalara_Response__c = false;
						} else {//Error validating shipping address
							SampleResult2 = 'Shipping Address Validation Returned Other Than Success: ' + vres2.ResultCode + 
							' : ' + vres2.Messages.Message[0].Name + ' : ' + vres2.Messages.Message[0].Summary;
							system.debug(LoggingLevel.info, SampleResult2);
							con.Validated__c = false;
							con.Validation_Error_Reason__c = 'Shipping Address Validation Returned Other Than Success: '+ vres2.Messages.Message[0].Summary;
						}
					} else {//shipping address is null
						con.MailingStreet = vres1.ValidAddresses.ValidAddress[0].Line1;
						con.MailingCity = vres1.ValidAddresses.ValidAddress[0].City;
						con.MailingState = vres1.ValidAddresses.ValidAddress[0].Region;
						PostalCode = vres1.ValidAddresses.ValidAddress[0].PostalCode;
						if (PostalCode.contains('-')){
							Zipcode = PostalCode.trim().split('-');
							con.MailingPostalCode = Zipcode[0];
						} else {
							con.MailingPostalCode = vres1.ValidAddresses.ValidAddress[0].PostalCode;
						}
					
						con.Validated__c = true;
						con.Validation_Error_Reason__c = null;
						con.Pending_Avalara_Response__c = false;
						System.debug('Shipping Address is null - Success');
					}
				} else {//Error validating billing address
					SampleResult1 = 'Billing Address Validation Returned Other Than Success: ' + vres1.ResultCode + 
					' : ' + vres1.Messages.Message[0].Name + ' : ' + vres1.Messages.Message[0].Summary;
					system.debug(LoggingLevel.info, SampleResult1);
					con.Validated__c = false;
					con.Validation_Error_Reason__c = 'Billing Address Validation Returned Other Than Success: ' + vres1.Messages.Message[0].Summary;
				}
			} else {
				System.debug('No Addresses to Validate');
			}
			return con;
		}
		return null; 
	}
}