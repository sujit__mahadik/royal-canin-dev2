/************************************************************************************
Version : 1.0
Name : giic_AccountAddressValidationSchedular
Created Date : 17 Aug 2018
Function : Schedule giic_AccountAddressValidationBatch class
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/

global with sharing class giic_AccountAddressValidationSchedular implements Schedulable{
    
    public static final String jobName = 'giic_AccountAddressValidationJob';
    
    global void execute(SchedulableContext ctx) {
        giic_AccountAddressValidationBatch batch = new giic_AccountAddressValidationBatch();
        Database.executeBatch(batch);
    }
    global static String scheduleJob(String cronExp){
        giic_AccountAddressValidationSchedular schedular = new giic_AccountAddressValidationSchedular();
        return System.schedule(jobName, cronExp, schedular); 
    }
    
}