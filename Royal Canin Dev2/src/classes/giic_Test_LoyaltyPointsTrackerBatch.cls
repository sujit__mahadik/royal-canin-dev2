/************************************************************************************
Version : 1.0
Created Date : 22 Nov 2018
Function : Tests class for giic_LoyaltyPointsTrackerBatch
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/
@isTest(SeeAllData = false)
private class giic_Test_LoyaltyPointsTrackerBatch {
    
    @testSetup
    static void setup()
    {
        giic_Test_DataCreationUtility.insertWarehouse();        
        giic_Test_DataCreationUtility.testSystemPolicyCreation();
        giic_Test_DataCreationUtility.insertGlobalParametersCustomSetting();
        giic_Test_DataCreationUtility.insertConsumerAccount();
        
        giic_Test_DataCreationUtility.createCarrier();
        giic_Test_DataCreationUtility.insertUnitofMeasure();        
        giic_Test_DataCreationUtility.insertProduct();
        giic_Test_DataCreationUtility.insertLocations();
        giic_Test_DataCreationUtility.insertProductInventory(1);
        giic_Test_DataCreationUtility.insertProductInventoryByLoc();
        giic_Test_DataCreationUtility.insertProductInventoryQTYDetails();
        giic_Test_DataCreationUtility.insertSalesOrder(); // 0 - open sales order , 1 - release sales order        
        giic_Test_DataCreationUtility.lstSalesOrder[1].giic_TaxCal__c = false;
        giic_Test_DataCreationUtility.lstSalesOrder[1].giic_SOStatus__c = giic_Constants.SO_PICKED;
        update giic_Test_DataCreationUtility.lstSalesOrder[1];
        giic_Test_DataCreationUtility.insertSOLine(new list<gii__SalesOrder__c>{giic_Test_DataCreationUtility.lstSalesOrder[1]});
        
        List<gii__SalesOrderLine__c>  lstSalesOrderLines = [select id,name,gii__SalesOrder__c,gii__Warehouse__c,gii__OrderQuantity__c,gii__Status__c,gii__Product__c from gii__SalesOrderLine__c where gii__SalesOrder__c = : giic_Test_DataCreationUtility.lstSalesOrder[1].id];
        for(gii__SalesOrderLine__c soline  : lstSalesOrderLines)
        {
            soline.giic_LineStatus__c = giic_Constants.SHIPPED ;
        }
        update lstSalesOrderLines;
    
        
        List<gii__SalesOrderStaging__c> lstSOStaging = giic_Test_DataCreationUtility.insertSalesOrderStaging();
        giic_Test_DataCreationUtility.insertSalesOrderLineStaging();
        
        list<giic_Disable_Triggers__c> lstDTCS = giic_Test_DataCreationUtility.insertDisableTriggersCustomSetting();
        lstDTCS[0].giic_DisableTriggerWHShipmentAdviceLine__c = true;
        update lstDTCS[0]; 
        
        list<gii__SalesOrder__c>  lstSalesOrder = [select id,gii__Released__c, gii__Account__c,name,gii__Carrier__c,gii__Carrier__r.giic_UniqueCarrier__c,gii__Warehouse__r.giic_WarehouseCode__c from gii__SalesOrder__c where id = : giic_Test_DataCreationUtility.lstSalesOrder[1].id ];
        giic_Test_DataCreationUtility.insertWarehouseShipmentAdviceStaging(lstSalesOrder);
        giic_Test_DataCreationUtility.insertWarehouseShipmentAdviceLinesStaging(lstSalesOrderLines);
        giic_Test_DataCreationUtility.CreateAdminUser();   
        
         // create Reward Points
        List<giic_RewardPoint__c> testROList = giic_Test_DataCreationUtility.insertRewardPoints();
        //List<gii__OrderInvoiceDetail__c> testOIDList = giic_Test_DataCreationUtility.insertOrderInvoiceDetail();
        List<Id> selectedRecordIds = new List<Id>();
        for(gii__InventoryReserve__c obj : [select id,gii__SalesOrderLine__c,gii__ReserveQuantity__c from gii__InventoryReserve__c])
        {
            selectedRecordIds.add(obj.id);
        }
        // get ship date string value in yyyy-MM-DD format and 
        // pass it to packlist ship confirm class along with 
        // groupBy with semi-colon ';' as  delimeter
        String groupBy = 'SalesOrder';
        String shippedDateString = system.today().year() + '-' + system.today().month() + '-' + system.today().day() ;   
        String groupByAndShippedDate = groupBy + ';' + shippedDateString;  
        try {
            gii.OrderShipment.quickShip(selectedRecordIds, groupByAndShippedDate);
        }
        catch(exception e){
            
        } 

    }    
    
    public static testMethod void testExecute()
    {      
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        System.Runas(u)        
        { 
            gii__GloviaSystemSetting__c setting = new gii__GloviaSystemSetting__c();
            setting.giic_AllowLoyaltyPointInvoiceLine__c = true;  
            setting.giic_PaymentTypeNotAllowedForLoyaltyPts__c = '';
            insert setting;
            
            List<Account> lstAccount = [select id,Bill_To_Customer_ID__c from Account limit 1];            
            giic_MembershipType__c objMT = new giic_MembershipType__c ();
            objMT .Name = 'Breeder';
            objMT.giic_QualifyingPoints__c = 0;
            insert objMT ;
            
            giic_LoyaltyConfiguration__c objLC = new giic_LoyaltyConfiguration__c ();
            objLC.giic_Frequency__c = 'Quarterly';
            objLC.giic_MembershipType__c = objMT.id;
            objLC.giic_PointsEquivToAmt__c = 0.5;
            objLC.giic_Amount__c = 1;            
            insert objLC;
            
            giic_RewardPointConfig__c objRPC = new giic_RewardPointConfig__c();
            objRPC.giic_MembershipType__c = objMT.id;
            objRPC.giic_ApplyFixedRewardPointValue__c =  true;
            objRPC.giic_NumOfPtsToGenRewardPts__c = 0.00;
            objRPC.giic_Tier1PtsToGenRewardPts__c = 0.00;
            objRPC.giic_Tier2PtsToGenRewardPts__c = 0.00;
            objRPC.giic_Tier3PtsToGenRewardPts__c = 0.00;
            objRPC.giic_Tier4PtsToGenRewardPts__c = 0.00;
            objRPC.giic_Tier5PtsToGenRewardPts__c = 0.00;
            objRPC.giic_Tier6PtsToGenRewardPts__c = 0.00;
            insert objRPC;
            
            lstAccount[0].giic_TypeOfMembership__c = objMT.id;
            lstAccount[0].giic_SetupLoyaltyMembershipAccount__c = true;
            update lstAccount;
            
            gii__SalesOrder__c so;
            
            list<gii__SalesOrder__c> solist = [select id,name,gii__Account__c,gii__Released__c,gii__NetAmount__c,gii__PaymentMethod__c  from gii__SalesOrder__c];
            if(solist.size() > 1)
                so = solist[1];
            gii__SalesOrderPayment__c soPayment = new gii__SalesOrderPayment__c ();
            soPayment.gii__SalesOrder__c  =  so.id;            
            soPayment.gii__PaymentMethod__c = 'Cash Only';
            soPayment.gii__PaidAmount__c = so.gii__NetAmount__c ;
            insert soPayment;
            
            list<gii__WarehouseShipmentAdviceStaging__c> lstWarehouseshpmentStaging = [select id,Name,giic_isTaxCommitted__c from gii__WarehouseShipmentAdviceStaging__c];
         
            Test.startTest();
            
            Map<String, Object> mapResult;
            if(lstWarehouseshpmentStaging != null && !lstWarehouseshpmentStaging.isEmpty())
            {
                Map<String, Object> mapUpdateResult = giic_CreateInvoiceHelper.updateSOLandIR(lstWarehouseshpmentStaging[0].id); //  first update SoL and IR
                try
                {
                    Map<String,Object> res = giic_CreateInvoiceHelper.CalculateTax(lstWarehouseshpmentStaging[0].id); // calculate tax
                }
                catch(exception ex)
                {
                    
                }
                
                gii__WarehouseShipmentAdviceStaging__c ws = new gii__WarehouseShipmentAdviceStaging__c(id = lstWarehouseshpmentStaging[0].id, giic_isTaxCommitted__c = true );
                update ws;
                 
                List<gii__SalesOrderLine__c>  lstSalesOrderLines = [select id,name,gii__Warehouse__c,gii__OrderQuantity__c,gii__Status__c,gii__SalesOrder__c,gii__SalesOrder__r.gii__Account__c,gii__Product__c from gii__SalesOrderLine__c where gii__SalesOrder__c = : so.id];
                giic_Test_DataCreationUtility.createInventoryReservation(lstSalesOrderLines);
                list<gii__InventoryReserve__c> lstInvRes = [select id,gii__ProductInventorySequence__c,gii__Status__c,gii__ProductInventorybyLocation__c,gii__Location__c,gii__ProductInventoryQuantityDetail__c,gii__SalesOrder__c,gii__SalesOrderLine__r.gii__Product__c,gii__SalesOrderLine__r.giic_LineStatus__c ,gii__ReserveQuantity__c from gii__InventoryReserve__c ];
                
                for(gii__InventoryReserve__c invRes : lstInvRes)
                {                    
                    invRes.gii__ReserveQuantity__c = 2; invRes.gii__Status__c = giic_Constants.Reserved;
                }                
                update lstInvRes;
                
                mapResult = giic_CreateInvoiceHelper.createShipment(lstWarehouseshpmentStaging[0].id);                 
                giic_Test_DataCreationUtility.insertShipment(new list<gii__SalesOrder__c>{so});                
                giic_Test_DataCreationUtility.insertShipmentDetails(lstSalesOrderLines);
                gii__Shipment__c sp  =  [select id,gii__Status__c from gii__Shipment__c limit 1];                
                giic_SOShipmentUtility.createInvoice(sp.id);    
                
                List<gii__OrderInvoiceDetail__c > lstOID = new List<gii__OrderInvoiceDetail__c >();
                lstOID = [select id,gii__ProductAmount__c,gii__Invoice__r.gii__Account__c,
                                                                   gii__Invoice__r.gii__Account__r.giic_TypeOfMembership__c,
                                                                   gii__InvoicedQuantity__c,gii__NetInvoiceAmount__c,
                                                                   gii__ARCreditQuantity__c from gii__OrderInvoiceDetail__c ] ;
               
                
                giic_LoyaltyPointsTrackerBatch obj = new giic_LoyaltyPointsTrackerBatch ();
                Database.QueryLocator ql = obj.start(null);
                obj.execute(null,lstOID);
                obj.Finish(null);            
                
                List<giic_RewardPoint__c> lstRP= [select id,giic_IsOneTimeUse__c,giic_RewardPointValue__c from giic_RewardPoint__c];                
                List<String> rewardPointsIds = new List<String>();
                rewardPointsIds.add(lstRP[0].id);
                giic_LoyaltyManagementUtility.getAvailableRewardPoints(lstAccount[0].Bill_To_Customer_ID__c,rewardPointsIds);
                lstRP[0].giic_IsOneTimeUse__c = true;
                update lstRP[0];
                List<giic_MembersLoyaltyPoints__c> lstMLP = new List<giic_MembersLoyaltyPoints__c>();
                lstMLP =[select id,name, giic_Account__c,giic_Account__r.name,giic_Account__r.giic_TypeOfMembership__c,giic_Account__r.giic_TypeOfMembership__r.name,giic_CurrentPoints__c,
                    giic_LifeTimePoints__c,(select id,name,giic_MembersLoyaltyPts__c,giic_SalesOrderPayment__c,giic_PointRedeemed__c,giic_PointsEarned__c from LoyaltyPointTracker__r 
                    where giic_PointRedeemed__c = false ORDER BY giic_PointsEarned__c DESC) 
                    from giic_MembersLoyaltyPoints__c];
              
                giic_LoyaltyManagementUtility.updateRewardPoints(lstRP,1.0);
                set<string> setAccountIds = new set<string>();
                setAccountIds.add(lstAccount[0].id);
                map<id,giic_RewardPoint__c> mapRewardPointsForAccount=giic_LoyaltyManagementUtility.getAvailableRewardPoints(setAccountIds,null);
                
                giic_RewardPointsTrackerBatch obj1 = new giic_RewardPointsTrackerBatch ();
                Database.QueryLocator ql1 = obj1.start(null);
                obj1.execute(null,lstMLP);
                obj1.Finish(null);   
                
            }         
            Test.stopTest();
        }              
    }  
    
    public static testMethod void testExecuteNegative()
    {      
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        System.Runas(u)        
        { 
            gii__GloviaSystemSetting__c setting = new gii__GloviaSystemSetting__c();
            setting.giic_AllowLoyaltyPointInvoiceLine__c = true;  
            setting.giic_PaymentTypeNotAllowedForLoyaltyPts__c = 'Test';
            insert setting;
            
            List<Account> lstAccount = [select id,Bill_To_Customer_ID__c from Account limit 1];            
            giic_MembershipType__c objMT = new giic_MembershipType__c ();
            objMT .Name = 'Breeder';
            objMT.giic_QualifyingPoints__c = 0;
            insert objMT ;
            
            giic_LoyaltyConfiguration__c objLC = new giic_LoyaltyConfiguration__c ();
            objLC.giic_Frequency__c = 'Quarterly';
            objLC.giic_MembershipType__c = objMT.id;
            objLC.giic_PointsEquivToAmt__c = 0.5;
            objLC.giic_Amount__c = 1;            
            insert objLC;
            
            giic_RewardPointConfig__c objRPC = new giic_RewardPointConfig__c();
            objRPC.giic_MembershipType__c = objMT.id;
            objRPC.giic_ApplyFixedRewardPointValue__c =  true;
            objRPC.giic_NumOfPtsToGenRewardPts__c = 0.00;
            objRPC.giic_Tier1PtsToGenRewardPts__c = 0.00;
            objRPC.giic_Tier2PtsToGenRewardPts__c = 0.00;
            objRPC.giic_Tier3PtsToGenRewardPts__c = 0.00;
            objRPC.giic_Tier4PtsToGenRewardPts__c = 0.00;
            objRPC.giic_Tier5PtsToGenRewardPts__c = 0.00;
            objRPC.giic_Tier6PtsToGenRewardPts__c = 0.00;
            insert objRPC;
            
            lstAccount[0].giic_TypeOfMembership__c = objMT.id;
            lstAccount[0].giic_SetupLoyaltyMembershipAccount__c = true;
            update lstAccount;
            
            gii__SalesOrder__c so;
            
            list<gii__SalesOrder__c> solist = [select id,name,gii__Account__c,gii__Released__c from gii__SalesOrder__c];
            if(solist.size() > 1)
                so = solist[1];
            
            list<gii__WarehouseShipmentAdviceStaging__c> lstWarehouseshpmentStaging = [select id,Name,giic_isTaxCommitted__c from gii__WarehouseShipmentAdviceStaging__c];
            
         
            Test.startTest();
            
            Map<String, Object> mapResult;
            if(lstWarehouseshpmentStaging != null && !lstWarehouseshpmentStaging.isEmpty())
            {
                Map<String, Object> mapUpdateResult = giic_CreateInvoiceHelper.updateSOLandIR(lstWarehouseshpmentStaging[0].id); //  first update SoL and IR
                try
                {
                    Map<String,Object> res = giic_CreateInvoiceHelper.CalculateTax(lstWarehouseshpmentStaging[0].id); // calculate tax
                }
                catch(exception ex)
                {
                    
                }
                
                gii__WarehouseShipmentAdviceStaging__c ws = new gii__WarehouseShipmentAdviceStaging__c(id = lstWarehouseshpmentStaging[0].id, giic_isTaxCommitted__c = true );
                update ws;
                 
                List<gii__SalesOrderLine__c>  lstSalesOrderLines = [select id,name,gii__Warehouse__c,gii__OrderQuantity__c,gii__Status__c,gii__SalesOrder__c,gii__SalesOrder__r.gii__Account__c,gii__Product__c from gii__SalesOrderLine__c where gii__SalesOrder__c = : so.id];
                giic_Test_DataCreationUtility.createInventoryReservation(lstSalesOrderLines);
                list<gii__InventoryReserve__c> lstInvRes = [select id,gii__ProductInventorySequence__c,gii__Status__c,gii__ProductInventorybyLocation__c,gii__Location__c,gii__ProductInventoryQuantityDetail__c,gii__SalesOrder__c,gii__SalesOrderLine__r.gii__Product__c,gii__SalesOrderLine__r.giic_LineStatus__c ,gii__ReserveQuantity__c from gii__InventoryReserve__c ];
                
                for(gii__InventoryReserve__c invRes : lstInvRes)
                {                    
                    invRes.gii__ReserveQuantity__c = 2; invRes.gii__Status__c = giic_Constants.Reserved;
                }                
                update lstInvRes;
                
                mapResult = giic_CreateInvoiceHelper.createShipment(lstWarehouseshpmentStaging[0].id);                 
                giic_Test_DataCreationUtility.insertShipment(new list<gii__SalesOrder__c>{so});                
                giic_Test_DataCreationUtility.insertShipmentDetails(lstSalesOrderLines);
                gii__Shipment__c sp  =  [select id,gii__Status__c from gii__Shipment__c limit 1];                
                giic_SOShipmentUtility.createInvoice(sp.id);          
                
                List<gii__OrderInvoiceDetail__c > lstOID = new List<gii__OrderInvoiceDetail__c >();
                lstOID = [select id,gii__ProductAmount__c,gii__Invoice__r.gii__Account__c,
                                                                   gii__Invoice__r.gii__Account__r.giic_TypeOfMembership__c,
                                                                   gii__InvoicedQuantity__c,gii__NetInvoiceAmount__c,
                                                                   gii__ARCreditQuantity__c,gii__Invoice__r.gii__SalesOrder__r.gii__PaymentMethod__c from gii__OrderInvoiceDetail__c ] ;
                
                giic_LoyaltyPointsTrackerBatch obj = new giic_LoyaltyPointsTrackerBatch ();
                Database.QueryLocator ql = obj.start(null);
                obj.execute(null,lstOID);
                obj.Finish(null);                  
                
            }         
            Test.stopTest();
        }
    }
    
    
    public static testMethod void testExecuteNegativeOther()
    {      
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        System.Runas(u)        
        { 
            gii__GloviaSystemSetting__c setting = new gii__GloviaSystemSetting__c();
            setting.giic_AllowLoyaltyPointInvoiceLine__c = true;  
            setting.giic_PaymentTypeNotAllowedForLoyaltyPts__c = 'Test';
            insert setting;
            
            List<Account> lstAccount = [select id,Bill_To_Customer_ID__c from Account limit 1];            
            giic_MembershipType__c objMT = new giic_MembershipType__c ();
            objMT .Name = 'Breeder';
            objMT.giic_QualifyingPoints__c = 0;
            insert objMT ;
            
            giic_LoyaltyConfiguration__c objLC = new giic_LoyaltyConfiguration__c ();
            objLC.giic_Frequency__c = 'Quarterly';
            objLC.giic_MembershipType__c = objMT.id;
            objLC.giic_PointsEquivToAmt__c = 0.5;
            objLC.giic_Amount__c = 1;            
            insert objLC;
            
            giic_RewardPointConfig__c objRPC = new giic_RewardPointConfig__c();
            objRPC.giic_MembershipType__c = objMT.id;
            objRPC.giic_ApplyFixedRewardPointValue__c =  true;
            objRPC.giic_NumOfPtsToGenRewardPts__c = 0.00;
            objRPC.giic_Tier1PtsToGenRewardPts__c = 0.00;
            objRPC.giic_Tier2PtsToGenRewardPts__c = 0.00;
            objRPC.giic_Tier3PtsToGenRewardPts__c = 0.00;
            objRPC.giic_Tier4PtsToGenRewardPts__c = 0.00;
            objRPC.giic_Tier5PtsToGenRewardPts__c = 0.00;
            objRPC.giic_Tier6PtsToGenRewardPts__c = 0.00;
            insert objRPC;
            
            lstAccount[0].giic_TypeOfMembership__c = objMT.id;
            lstAccount[0].giic_SetupLoyaltyMembershipAccount__c = true;
            update lstAccount;
            
            gii__SalesOrder__c so;
            
            list<gii__SalesOrder__c> solist = [select id,name,gii__Account__c,gii__Released__c from gii__SalesOrder__c];
            if(solist.size() > 1)
                so = solist[1];
            
            list<gii__WarehouseShipmentAdviceStaging__c> lstWarehouseshpmentStaging = [select id,Name,giic_isTaxCommitted__c from gii__WarehouseShipmentAdviceStaging__c];
           
         
            Test.startTest();
            
            Map<String, Object> mapResult;
            if(lstWarehouseshpmentStaging != null && !lstWarehouseshpmentStaging.isEmpty())
            {
                Map<String, Object> mapUpdateResult = giic_CreateInvoiceHelper.updateSOLandIR(lstWarehouseshpmentStaging[0].id); //  first update SoL and IR
                try
                {
                    Map<String,Object> res = giic_CreateInvoiceHelper.CalculateTax(lstWarehouseshpmentStaging[0].id); // calculate tax
                }
                catch(exception ex)
                {
                    
                }
                
                gii__WarehouseShipmentAdviceStaging__c ws = new gii__WarehouseShipmentAdviceStaging__c(id = lstWarehouseshpmentStaging[0].id, giic_isTaxCommitted__c = true );
                update ws;
                 
                List<gii__SalesOrderLine__c>  lstSalesOrderLines = [select id,name,gii__Warehouse__c,gii__OrderQuantity__c,gii__Status__c,gii__SalesOrder__c,gii__SalesOrder__r.gii__Account__c,gii__Product__c from gii__SalesOrderLine__c where gii__SalesOrder__c = : so.id];
                giic_Test_DataCreationUtility.createInventoryReservation(lstSalesOrderLines);
                list<gii__InventoryReserve__c> lstInvRes = [select id,gii__ProductInventorySequence__c,gii__Status__c,gii__ProductInventorybyLocation__c,gii__Location__c,gii__ProductInventoryQuantityDetail__c,gii__SalesOrder__c,gii__SalesOrderLine__r.gii__Product__c,gii__SalesOrderLine__r.giic_LineStatus__c ,gii__ReserveQuantity__c from gii__InventoryReserve__c ];
                
                for(gii__InventoryReserve__c invRes : lstInvRes)
                {                    
                    invRes.gii__ReserveQuantity__c = 2; invRes.gii__Status__c = giic_Constants.Reserved;
                }                
                update lstInvRes;
                
                mapResult = giic_CreateInvoiceHelper.createShipment(lstWarehouseshpmentStaging[0].id);                 
                giic_Test_DataCreationUtility.insertShipment(new list<gii__SalesOrder__c>{so});                
                giic_Test_DataCreationUtility.insertShipmentDetails(lstSalesOrderLines);
                gii__Shipment__c sp  =  [select id,gii__Status__c from gii__Shipment__c limit 1];                
                giic_SOShipmentUtility.createInvoice(sp.id);          
                
                List<gii__OrderInvoiceDetail__c > lstOID = new List<gii__OrderInvoiceDetail__c >();
                lstOID = [select id,gii__ProductAmount__c,gii__Invoice__r.gii__Account__c,
                                                                   gii__Invoice__r.gii__Account__r.giic_TypeOfMembership__c,
                                                                   gii__InvoicedQuantity__c,gii__NetInvoiceAmount__c,
                                                                   gii__ARCreditQuantity__c from gii__OrderInvoiceDetail__c ] ;
                
                giic_LoyaltyPointsTrackerBatch obj = new giic_LoyaltyPointsTrackerBatch ();
                Database.QueryLocator ql = obj.start(null);
                obj.execute(null,lstOID);
                obj.Finish(null);            
                
                List<giic_RewardPoint__c> lstRP= [select id,giic_IsOneTimeUse__c,giic_RewardPointValue__c from giic_RewardPoint__c];                
                List<String> rewardPointsIds = new List<String>();
                rewardPointsIds.add(lstRP[0].id);
                
                giic_LoyaltyManagementUtility.getAvailableRewardPoints(lstAccount[0].Bill_To_Customer_ID__c,rewardPointsIds);
                lstRP[0].giic_IsOneTimeUse__c = false;
                lstRP[0].giic_RewardPointValue__c = 2.0;
                update lstRP[0];
                giic_LoyaltyManagementUtility.updateRewardPoints(lstRP,1.0);
            }         
            Test.stopTest();
        }
    }
}