/************************************************************************************
Version : 1.0
Name : giic_Test_FedExCalloutNegativeMock 
Created Date : 10 Sep 2018
Function : create negative mock responce for fedEx WebServiceCallout
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/
@isTest
global class giic_Test_FedExCalloutNegativeMock implements WebServiceMock {
   global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
        // start - specify the response you want to send
        
        giic_FedExAddressValidation.Address EffectiveAddress = new giic_FedExAddressValidation.Address();
        EffectiveAddress.City='SOMERDALE'; 
        EffectiveAddress.CountryCode='US'; 
        EffectiveAddress.CountryName=null;
        EffectiveAddress.PostalCode='08083-2202'; 
        EffectiveAddress.Residential=false;
        EffectiveAddress.StateOrProvinceCode='NJ';
        EffectiveAddress.StreetLines=new List<String>{'8 CLEMENT DR'}; 
        EffectiveAddress.UrbanizationCode='';
        
        
        List<giic_FedExAddressValidation.AddressAttribute> Attributes = new List<giic_FedExAddressValidation.AddressAttribute>();
        giic_FedExAddressValidation.AddressAttribute attrCountrySupported = new giic_FedExAddressValidation.AddressAttribute();
        attrCountrySupported.Name = 'CountrySupported';
        attrCountrySupported.Value = 'true';
        Attributes.add(attrCountrySupported);
        giic_FedExAddressValidation.AddressAttribute attrZIP11Match = new giic_FedExAddressValidation.AddressAttribute();
        attrZIP11Match.Name = 'ZIP11Match';
        attrZIP11Match.Value = 'true';
        Attributes.add(attrZIP11Match);
        giic_FedExAddressValidation.AddressAttribute attrMultipleMatches = new giic_FedExAddressValidation.AddressAttribute();
        attrMultipleMatches.Name = 'MultipleMatches';
        attrMultipleMatches.Value = 'false';
        Attributes.add(attrMultipleMatches);
        giic_FedExAddressValidation.AddressAttribute attrResolved = new giic_FedExAddressValidation.AddressAttribute();
        attrResolved.Name = 'Resolved';
        attrResolved.Value = 'false';
        Attributes.add(attrResolved);
        giic_FedExAddressValidation.AddressAttribute attrDPV = new giic_FedExAddressValidation.AddressAttribute();
        attrDPV.Name = 'DPV';
        attrDPV.Value = 'true';
        Attributes.add(attrDPV);
        
        List<giic_FedExAddressValidation.AddressValidationResult> lstResult = new List<giic_FedExAddressValidation.AddressValidationResult>();
        giic_FedExAddressValidation.AddressValidationResult objResult = new giic_FedExAddressValidation.AddressValidationResult();
        objResult.ClientReferenceId = '12314';
        objResult.State = 'STANDARDIZED';
        objResult.Classification='RESIDENTIAL';
        objResult.EffectiveAddress =  EffectiveAddress;
        objResult.Attributes = Attributes;
        lstResult.add(objResult);
        
        giic_FedExAddressValidation.AddressValidationReply response_x = new giic_FedExAddressValidation.AddressValidationReply();
        response_x.HighestSeverity = 'Success';
        response_x.AddressResults = lstResult;
     // String successResponce = '{"AddressValidationReply":"[AddressResults=(AddressValidationResult:[Attributes=(AddressAttribute:[Name=CountrySupported, Name_type_info=(Name, http://fedex.com/ws/addressvalidation/v4, null, 0, 1, false), Value=true, Value_type_info=(Value, http://fedex.com/ws/addressvalidation/v4, null, 0, 1, false), apex_schema_type_info=(http://fedex.com/ws/addressvalidation/v4, true, true), field_order_type_info=(Name, Value)], AddressAttribute:[Name=ZIP11Match, Name_type_info=(Name, http://fedex.com/ws/addressvalidation/v4, null, 0, 1, false), Value=true, Value_type_info=(Value, http://fedex.com/ws/addressvalidation/v4, null, 0, 1, false), apex_schema_type_info=(http://fedex.com/ws/addressvalidation/v4, true, true), field_order_type_info=(Name, Value)], AddressAttribute:[Name=SuiteRequiredButMissing, Name_type_info=(Name, http://fedex.com/ws/addressvalidation/v4, null, 0, 1, false), Value=false, Value_type_info=(Value, http://fedex.com/ws/addressvalidation/v4, null, 0, 1, false), apex_schema_type_info=(http://fedex.com/ws/addressvalidation/v4, true, true), field_order_type_info=(Name, Value)], AddressAttribute:[Name=InvalidSuiteNumber, Name_type_info=(Name, http://fedex.com/ws/addressvalidation/v4, null, 0, 1, false), Value=false, Value_type_info=(Value, http://fedex.com/ws/addressvalidation/v4, null, 0, 1, false), apex_schema_type_info=(http://fedex.com/ws/addressvalidation/v4, true, true), field_order_type_info=(Name, Value)], AddressAttribute:[Name=MultipleMatches, Name_type_info=(Name, http://fedex.com/ws/addressvalidation/v4, null, 0, 1, false), Value=false, Value_type_info=(Value, http://fedex.com/ws/addressvalidation/v4, null, 0, 1, false), apex_schema_type_info=(http://fedex.com/ws/addressvalidation/v4, true, true), field_order_type_info=(Name, Value)], AddressAttribute:[Name=Resolved, Name_type_info=(Name, http://fedex.com/ws/addressvalidation/v4, null, 0, 1, false), Value=true, Value_type_info=(Value, http://fedex.com/ws/addressvalidation/v4, null, 0, 1, false), apex_schema_type_info=(http://fedex.com/ws/addressvalidation/v4, true, true), field_order_type_info=(Name, Value)], AddressAttribute:[Name=ZIP4Match, Name_type_info=(Name, http://fedex.com/ws/addressvalidation/v4, null, 0, 1, false), Value=true, Value_type_info=(Value, http://fedex.com/ws/addressvalidation/v4, null, 0, 1, false), apex_schema_type_info=(http://fedex.com/ws/addressvalidation/v4, true, true), field_order_type_info=(Name, Value)], AddressAttribute:[Name=DPV, Name_type_info=(Name, http://fedex.com/ws/addressvalidation/v4, null, 0, 1, false), Value=true, Value_type_info=(Value, http://fedex.com/ws/addressvalidation/v4, null, 0, 1, false), apex_schema_type_info=(http://fedex.com/ws/addressvalidation/v4, true, true), field_order_type_info=(Name, Value)], AddressAttribute:[Name=ValidMultiUnit, Name_type_info=(Name, http://fedex.com/ws/addressvalidation/v4, null, 0, 1, false), Value=false, Value_type_info=(Value, http://fedex.com/ws/addressvalidation/v4, null, 0, 1, false), apex_schema_type_info=(http://fedex.com/ws/addressvalidation/v4, true, true), field_order_type_info=(Name, Value)], AddressAttribute:[Name=POBox, Name_type_info=(Name, http://fedex.com/ws/addressvalidation/v4, null, 0, 1, false), Value=false, Value_type_info=(Value, http://fedex.com/ws/addressvalidation/v4, null, 0, 1, false), apex_schema_type_info=(http://fedex.com/ws/addressvalidation/v4, true, true), field_order_type_info=(Name, Value)], ...), Attributes_type_info=(Attributes, http://fedex.com/ws/addressvalidation/v4, null, 0, -1, false), Classification=RESIDENTIAL, Classification_type_info=(Classification, http://fedex.com/ws/addressvalidation/v4, null, 0, 1, false), ClientReferenceId=456789, ClientReferenceId_type_info=(ClientReferenceId, http://fedex.com/ws/addressvalidation/v4, null, 0, 1, false), EffectiveAddress=Address:[City=SOMERDALE, City_type_info=(City, http://fedex.com/ws/addressvalidation/v4, null, 0, 1, false), CountryCode=US, CountryCode_type_info=(CountryCode, http://fedex.com/ws/addressvalidation/v4, null, 0, 1, false), CountryName=null, CountryName_type_info=(CountryName, http://fedex.com/ws/addressvalidation/v4, null, 0, 1, false), PostalCode=08083-2202, PostalCode_type_info=(PostalCode, http://fedex.com/ws/addressvalidation/v4, null, 0, 1, false), Residential=null, Residential_type_info=(Residential, http://fedex.com/ws/addressvalidation/v4, null, 0, 1, false), StateOrProvinceCode=NJ, StateOrProvinceCode_type_info=(StateOrProvinceCode, http://fedex.com/ws/addressvalidation/v4, null, 0, 1, false), StreetLines=(8 CLEMENT DR), StreetLines_type_info=(StreetLines, http://fedex.com/ws/addressvalidation/v4, null, 0, -1, false), UrbanizationCode=, UrbanizationCode_type_info=(UrbanizationCode, http://fedex.com/ws/addressvalidation/v4, null, 0, 1, false), apex_schema_type_info=(http://fedex.com/ws/addressvalidation/v4, true, true), field_order_type_info=(StreetLines, City, StateOrProvinceCode, PostalCode, UrbanizationCode, CountryCode, CountryName, Residential)], EffectiveAddress_type_info=(EffectiveAddress, http://fedex.com/ws/addressvalidation/v4, null, 0, 1, false), EffectiveContact=null, EffectiveContact_type_info=(EffectiveContact, http://fedex.com/ws/addressvalidation/v4, null, 0, 1, false), ParsedAddressPartsDetail=ParsedAddressPartsDetail:[ParsedPostalCode=ParsedPostalCodeDetail:[AddOn=2202, AddOn_type_info=(AddOn, http://fedex.com/ws/addressvalidation/v4, null, 0, 1, false), Base=08083, Base_type_info=(Base, http://fedex.com/ws/addressvalidation/v4, null, 0, 1, false), DeliveryPoint=08, DeliveryPoint_type_info=(DeliveryPoint, http://fedex.com/ws/addressvalidation/v4, null, 0, 1, false), apex_schema_type_info=(http://fedex.com/ws/addressvalidation/v4, true, true), field_order_type_info=(Base, AddOn, DeliveryPoint)], ParsedPostalCode_type_info=(ParsedPostalCode, http://fedex.com/ws/addressvalidation/v4, null, 0, 1, false), ParsedStreetLine=ParsedStreetLineDetail:[Building=null, Building_type_info=(Building, http://fedex.com/ws/addressvalidation/v4, null, 0, 1, false), HouseNumber=8, HouseNumber_type_info=(HouseNumber, http://fedex.com/ws/addressvalidation/v4, null, 0, 1, false), LeadingDirectional=null, LeadingDirectional_type_info=(LeadingDirectional, http://fedex.com/ws/addressvalidation/v4, null, 0, 1, false), Organization=null, Organization_type_info=(Organization, http://fedex.com/ws/addressvalidation/v4, null, 0, 1, false), POBox=null, POBox_type_info=(POBox, http://fedex.com/ws/addressvalidation/v4, null, 0, 1, false), PreStreetType=null, PreStreetType_type_info=(PreStreetType, http://fedex.com/ws/addressvalidation/v4, null, 0, 1, false), RuralRoute=null, RuralRoute_type_info=(RuralRoute, http://fedex.com/ws/addressvalidation/v4, null, 0, 1, false), StreetName=CLEMENT, StreetName_type_info=(StreetName, http://fedex.com/ws/addressvalidation/v4, null, 0, 1, false), StreetSuffix=DR, StreetSuffix_type_info=(StreetSuffix, http://fedex.com/ws/addressvalidation/v4, null, 0, 1, false), TrailingDirectional=null, TrailingDirectional_type_info=(TrailingDirectional, http://fedex.com/ws/addressvalidation/v4, null, 0, 1, false), UnitLabel=null, UnitLabel_type_info=(UnitLabel, http://fedex.com/ws/addressvalidation/v4, null, 0, 1, false), UnitNumber=null, UnitNumber_type_info=(UnitNumber, http://fedex.com/ws/addressvalidation/v4, null, 0, 1, false), apex_schema_type_info=(http://fedex.com/ws/addressvalidation/v4, true, true), field_order_type_info=(HouseNumber, PreStreetType, LeadingDirectional, StreetName, StreetSuffix, TrailingDirectional, UnitLabel, UnitNumber, RuralRoute, POBox, ...)], ParsedStreetLine_type_info=(ParsedStreetLine, http://fedex.com/ws/addressvalidation/v4, null, 0, 1, false), apex_schema_type_info=(http://fedex.com/ws/addressvalidation/v4, true, true), field_order_type_info=(ParsedStreetLine, ParsedPostalCode)], ParsedAddressPartsDetail_type_info=(ParsedAddressPartsDetail, http://fedex.com/ws/addressvalidation/v4, null, 0, 1, false), State=STANDARDIZED, State_type_info=(State, http://fedex.com/ws/addressvalidation/v4, null, 0, 1, false), apex_schema_type_info=(http://fedex.com/ws/addressvalidation/v4, true, true), field_order_type_info=(ClientReferenceId, State, Classification, EffectiveContact, EffectiveAddress, ParsedAddressPartsDetail, Attributes)]), AddressResults_type_info=(AddressResults, http://fedex.com/ws/addressvalidation/v4, null, 0, -1, false), HighestSeverity=SUCCESS, HighestSeverity_type_info=(HighestSeverity, http://fedex.com/ws/addressvalidation/v4, null, 1, 1, false), Notifications=(Notification:[Code=0, Code_type_info=(Code, http://fedex.com/ws/addressvalidation/v4, null, 0, 1, false), LocalizedMessage=null, LocalizedMessage_type_info=(LocalizedMessage, http://fedex.com/ws/addressvalidation/v4, null, 0, 1, false), Message=Success, MessageParameters=null, MessageParameters_type_info=(MessageParameters, http://fedex.com/ws/addressvalidation/v4, null, 0, -1, false), Message_type_info=(Message, http://fedex.com/ws/addressvalidation/v4, null, 0, 1, false), Severity=SUCCESS, Severity_type_info=(Severity, http://fedex.com/ws/addressvalidation/v4, null, 0, 1, false), Source=wsi, Source_type_info=(Source, http://fedex.com/ws/addressvalidation/v4, null, 0, 1, false), apex_schema_type_info=(http://fedex.com/ws/addressvalidation/v4, true, true), field_order_type_info=(Severity, Source, Code, Message, LocalizedMessage, MessageParameters)]), Notifications_type_info=(Notifications, http://fedex.com/ws/addressvalidation/v4, null, 1, -1, false), ReplyTimestamp=2018-09-10 06:34:04, ReplyTimestamp_type_info=(ReplyTimestamp, http://fedex.com/ws/addressvalidation/v4, null, 0, 1, false), TransactionDetail=null, TransactionDetail_type_info=(TransactionDetail, http://fedex.com/ws/addressvalidation/v4, null, 0, 1, false), Version=VersionId:[Intermediate=0, Intermediate_type_info=(Intermediate, http://fedex.com/ws/addressvalidation/v4, null, 1, 1, false), Major=4, Major_type_info=(Major, http://fedex.com/ws/addressvalidation/v4, null, 1, 1, false), Minor=0, Minor_type_info=(Minor, http://fedex.com/ws/addressvalidation/v4, null, 1, 1, false), ServiceId=aval, ServiceId_type_info=(ServiceId, http://fedex.com/ws/addressvalidation/v4, null, 1, 1, false), apex_schema_type_info=(http://fedex.com/ws/addressvalidation/v4, true, true), field_order_type_info=(ServiceId, Major, Intermediate, Minor)], Version_type_info=(Version, http://fedex.com/ws/addressvalidation/v4, null, 1, 1, false), apex_schema_type_info=(http://fedex.com/ws/addressvalidation/v4, true, true), field_order_type_info=(HighestSeverity, Notifications, TransactionDetail, Version, ReplyTimestamp, AddressResults)]"}';
      /// response_x = (giic_FedExAddressValidation.AddressValidationReply) JSON.deserialize(successResponce, giic_FedExAddressValidation.AddressValidationReply.class);
     
        System.debug('response_x::' + response_x);
        // end
        response.put('response_x', response_x); 
   }
}