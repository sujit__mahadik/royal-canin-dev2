/************************************************************************************
Version : 1.0
Created Date : 27-Sep-2018
Function : Mock Test class for HTTPCallout
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/


@isTest
global class giic_Test_AvalaraCalloutMockImpl implements HttpCalloutMock {
    
    global HTTPResponse respond(HTTPRequest req) {        
        
        String body = '{"id":123456789,"code":"f81200e2-5f08-43dd-97df-156e93b5796a","companyId":12345,"date":"2018-10-02","status":"Committed","type":"SalesInvoice","currencyCode":"USD","customerVendorCode":"ABC","customerCode":"ABC","reconciled":true,"locationCode":"DEFAULT","salespersonCode":"DEF","taxOverrideType":"None","totalAmount":1000,"totalTax":62.5,"totalTaxable":1000,"totalTaxCalculated":62.5,"adjustmentReason":"NotAdjusted","region":"CA","country":"US","originAddressId":123456789,"destinationAddressId":123456789,"description":"Yarn","taxDate":"2018-10-02T00:00:00+00:00","lines":[{"id":123456789,"transactionId":123456789,"lineNumber":"1","description":"Yarn","destinationAddressId":12345,"originAddressId":123456789,"discountAmount":100,"isItemTaxable":true,"itemCode":"116292","lineAmount":1000,"quantity":1,"ref1":"Note: Deliver to Bob","reportingDate":"2018-10-02","sourcing":"Destination","tax":62.5,"taxableAmount":1000,"taxCalculated":62.5,"taxCode":"PS081282","taxDate":"2018-10-02","taxOverrideType":"None","details":[{"id":123456789,"transactionLineId":123456789,"transactionId":123456789,"addressId":12345,"country":"US","region":"CA","stateFIPS":"06","exemptReasonId":4,"jurisCode":"06","jurisName":"CALIFORNIA","jurisdictionId":5000531,"signatureCode":"AGAM","jurisType":"STA","nonTaxableType":"BaseRule","rate":0.0625,"rateRuleId":1321915,"rateSourceId":3,"sourcing":"Destination","tax":62.5,"taxableAmount":1000,"taxType":"Sales","taxName":"CA STATE TAX","taxAuthorityTypeId":45,"taxRegionId":2127184,"taxCalculated":62.5,"rateType":"General"}]}],"addresses":[{"boundaryLevel":"Address","line1":"100 Ravine Lane Northeast #220","city":"Bainbridge Island","region":"WA","postalCode":"98110","country":"US"}]}';   
        
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody(body);
        res.setStatusCode(200);
        return res;
    }
}