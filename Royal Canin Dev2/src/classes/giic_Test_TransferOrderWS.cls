/************************************************************************************
Version : 1.0
Name : giic_Test_TransferOrderWS
Created Date : 13 Nov 2018
Function : this is test class for giic_TransferOrderWS, giic_TransferOrderRecieptWS , giic_TransferOrderWrapper
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/

@isTest(seeAllData = false)
private class giic_Test_TransferOrderWS{
    public static User objTestClassUser;
    @testSetup
    static void setup(){
        // insert default warehouse
        List<gii__Warehouse__c> defaultWHList = giic_Test_DataCreationUtility.insertWarehouse();
        // create system policy
        giic_Test_DataCreationUtility.testSystemPolicyCreation2();        
        SetupTestData.createCustomSettings();
        giic_Test_DataCreationUtility.CreateAdminUser();
        // insert 5 warehouse ie. in bluk
        List<gii__Warehouse__c> testWareListN = giic_Test_DataCreationUtility.insertWarehouse_N(5);
        giic_Test_DataCreationUtility.lstWarehouse.addAll(testWareListN);
        giic_Test_DataCreationUtility.lstWarehouse.addAll(defaultWHList);
        giic_Test_DataCreationUtility.insertLocations();
        // create Account 
        giic_Test_DataCreationUtility.insertConsumerAccount_N(5);
        
        List<gii__Product2Add__c> testProductList = giic_Test_DataCreationUtility.insertProduct_N(); 
        
        // create carrier 
        giic_Test_DataCreationUtility.createCarrier(); 
        
        integer intCount = 0;
        for(gii__Product2Add__c objPR : giic_Test_DataCreationUtility.lstProdRef_N ){
            objPR.gii__DefaultWarehouse__c = testWareListN[math.mod(intCount,5)].Id;
            intCount++;
        }
        
        // Make 4th product gii__NonStock__c = true
        giic_Test_DataCreationUtility.lstProdRef_N[3].gii__NonStock__c = true;
        
        update giic_Test_DataCreationUtility.lstProdRef_N;
        giic_Test_DataCreationUtility.insertProductInventoryQTYDetails_N(5);
    }
    
    public static void QueryData(){
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        System.Runas(u)  
       {

        objTestClassUser = giic_Test_DataCreationUtility.getTestClassUser();
        giic_Test_DataCreationUtility.lstProdRef_N = [SELECT Id, giic_ProductSKU__c, gii__LotControlled__c, gii__ProductReference__c, gii__DefaultWarehouse__c, gii__ProductReference__r.Name, gii__NonStock__c, gii__ProductReference__r.ProductCode, gii__ProductReference__r.Family, gii__ProductReference__r.Product_Category__c, gii__ProductReference__r.SKU__c, gii__ProductReference__r.Food_Type__c, gii__ProductReference__r.Weight_Unit__c, gii__ProductReference__r.Weight__c, giic_ConvFactor__c, giic_RConvFactor__c, gii__SellingUnitofMeasure__c, gii__SellingUnitofMeasure__r.gii__Description__c, gii__SellingUnitofMeasure__r.gii__DisplayName__c, gii__SellingUnitofMeasure__r.gii__UniqueId__c, gii__StockingUnitofMeasure__c, gii__StockingUnitofMeasure__r.gii__Description__c, gii__StockingUnitofMeasure__r.gii__DisplayName__c, gii__StockingUnitofMeasure__r.gii__UniqueId__c   from gii__Product2Add__c]; 
        //0th element - default warehouse 
        giic_Test_DataCreationUtility.lstWarehouse_N = [select id, Name, giic_WarehouseCode__c from gii__Warehouse__c];
        giic_Test_DataCreationUtility.lstLocations = [select id, Name,gii__Warehouse__c from gii__Location__c];
        giic_Test_DataCreationUtility.lstAccount_N =  [select Name, BillingCountry , BillingPostalCode, BillingState , BillingCity , BillingStreet ,  ShippingCountry , ShippingPostalCode , ShippingState , ShippingCity , ShippingStreet , ClinicCustomerNo__c , ShipToCustomerNo__c , giic_WarehouseDistanceMapping__c from Account];
        giic_Test_DataCreationUtility.lstPIQD_N = [select id, name, gii__ProductInventory__c, gii__ProductInventorybyLocation__c, gii__Product__c, gii__Warehouse__c, gii__OnHandQuantity__c , gii__UniqueRecordKey__c from gii__ProductInventoryQuantityDetail__c];
    }
    }
    public static testMethod void test_createTransferOrder(){
       
        QueryData();
        Test.startTest();
        
        system.runAs(objTestClassUser){
            
            List<gii__Carrier__c> lstCarrier = [select Name, gii__NoChargeReason__c from gii__Carrier__c];
            string strToWarehouseCode = giic_Test_DataCreationUtility.lstWarehouse_N[0].giic_WarehouseCode__c;
            string strShipToCustomerNumber = giic_Test_DataCreationUtility.lstAccount_N[0].ShipToCustomerNo__c;
            string strFromWarehouse = giic_Test_DataCreationUtility.lstWarehouse_N[1].giic_WarehouseCode__c;
            string strProductCode1 = giic_Test_DataCreationUtility.lstProdRef_N[0].giic_ProductSKU__c;
            string strProductCode2 = giic_Test_DataCreationUtility.lstProdRef_N[1].giic_ProductSKU__c;
            string strCarrier = lstCarrier[0].Name;
            
            string strJSON = '{"TransferOrders":[{"TO_ToWarehouse":"904","TO_ShipToZipCode":"27410","TO_ShipToStreet":"420 NORTH CHIMNEY ROCK RD","TO_ShipToState":"NC","TO_ShipToName":"ASC- GREENSBORO","TO_ShipToCustomerNumber":"VET-0014045-001","TO_ShipToCountry":"United States","TO_ShipToCity":"GREENSBORO","TO_RequiredDate":"2018-10-30","TO_OrderNumber":"954550","TO_OrderDate":"2018-10-30","TO_FromWarehouse":"901","TO_Carrier":"Parcel","TransferOrderLines":[{"TOL_FromWarehouseCode":"901","TOL_StockUOM":"EA","TOL_ShippedQuantity":1.00,"TOL_Quantity":1.00,"TOL_ProductCode":"8528","TOL_OrderLineNumber":"954550-001","TOL_NonStockQuantity":0.00,"TOL_IsNonStock":false},{"TOL_FromWarehouseCode":"901","TOL_StockUOM":"EA","TOL_ShippedQuantity":1.00,"TOL_Quantity":1.00,"TOL_ProductCode":"8528","TOL_OrderLineNumber":"954550-002","TOL_NonStockQuantity":0.00,"TOL_IsNonStock":false}]}]}';
            
            
            giic_TransferOrderWrapper transferOrderWrapper =(giic_TransferOrderWrapper)json.deserialize(strJSON, giic_TransferOrderWrapper.class);
            
            if( transferOrderWrapper != null && transferOrderWrapper.TransferOrders!= null && transferOrderWrapper.TransferOrders.size() > 0 ){
                for(giic_TransferOrderWrapper.TransferOrder objTO : transferOrderWrapper.TransferOrders ){
                    objTO.TO_FromWarehouse = strFromWarehouse;
                    objTO.TO_ToWarehouse = strToWarehouseCode;
                    objTO.TO_ShipToCustomerNumber = strShipToCustomerNumber;
                    objTO.TO_Carrier = strCarrier;
                    for(giic_TransferOrderWrapper.TransferOrderLine oTOL : objTO.TransferOrderLines){
                        oTOL.TOL_FromWarehouseCode = strFromWarehouse;
                        oTOL.TOL_ProductCode = strProductCode1;                     
                    }
                }
            }
            
            string sTOJSON = json.serialize(transferOrderWrapper);
            RestRequest req = new RestRequest();
            req.requestBody = Blob.valueOf(sTOJSON);
            RestContext.request = req;
            giic_TransferOrderWS.createTransferOrder();
            
            // creation of recipts 
         strJSON = '{ "TransferOrderReceipts": [ { "TOR_fromWarehouse": "901", "TOR_toWarehouse": "904", "TOR_requiredDate": "2018-10-30", "TOR_orderNumber": "954550", "TransferOrderReceiptLines": [ { "TORL_toWarehouseCode": "904", "TORL_receiptQuantity": 1.00, "TORL_shippedQuantity": 1.00, "TORL_productCode": "8528", "TORL_orderLineNumber": "954550-001", "TORL_StockUOM": "EA" }, { "TORL_warehouseCode": "904", "TORL_receiptQuantity": 1.00, "TORL_shippedQuantity": 1.00, "TORL_productCode": "8528", "TORL_orderLineNumber": "954550-002", "TORL_StockUOM": "EA" } ] } ] }';
         transferOrderWrapper =(giic_TransferOrderWrapper)json.deserialize(strJSON, giic_TransferOrderWrapper.class);
         
         if( transferOrderWrapper != null && transferOrderWrapper.TransferOrderReceipts!= null && transferOrderWrapper.TransferOrderReceipts.size() > 0 ){
                for(giic_TransferOrderWrapper.TransferOrderReceipt objTO : transferOrderWrapper.TransferOrderReceipts ){
                    objTO.TOR_FromWarehouse = strFromWarehouse;
                    objTO.TOR_ToWarehouse = strToWarehouseCode;
                    for(giic_TransferOrderWrapper.TransferOrderRecieptLine oTOL : objTO.TransferOrderReceiptLines){
                        oTOL.TORL_ToWarehouseCode = strFromWarehouse;
                        oTOL.TORL_ProductCode = strProductCode1;                     
                    }
                }
            }
         
         sTOJSON = json.serialize(transferOrderWrapper);
         req = new RestRequest();
         req.requestBody = Blob.valueOf(sTOJSON);
         RestContext.request = req;
         giic_TransferOrderRecieptWS.createTransferOrderReceipt();   
          
         //negative test case  - 1
         req = new RestRequest();
         req.requestBody = null;
         RestContext.request = req;
         giic_TransferOrderRecieptWS.createTransferOrderReceipt();
         
         //negative test case  - 2
         
          if( transferOrderWrapper != null && transferOrderWrapper.TransferOrderReceipts!= null && transferOrderWrapper.TransferOrderReceipts.size() > 0 ){
                for(giic_TransferOrderWrapper.TransferOrderReceipt objTO : transferOrderWrapper.TransferOrderReceipts ){
                    objTO.TOR_orderNumber = '1234';
                }
            }
         
          sTOJSON = json.serialize(transferOrderWrapper);  
        }
        
        Test.stopTest();
       
       
        
    }
    
    public static testMethod void test_createTransferOrder_validateRequest(){
        QueryData();
        Test.startTest();
        
        system.runAs(objTestClassUser){
            
            List<gii__Carrier__c> lstCarrier = [select Name, gii__NoChargeReason__c from gii__Carrier__c];
            string strToWarehouseCode = giic_Test_DataCreationUtility.lstWarehouse_N[0].giic_WarehouseCode__c;
            string strShipToCustomerNumber = giic_Test_DataCreationUtility.lstAccount_N[0].ShipToCustomerNo__c;
            string strFromWarehouse = giic_Test_DataCreationUtility.lstWarehouse_N[1].giic_WarehouseCode__c;
            string strProductCode1 = giic_Test_DataCreationUtility.lstProdRef_N[0].giic_ProductSKU__c;
            string strProductCode2 = giic_Test_DataCreationUtility.lstProdRef_N[1].giic_ProductSKU__c;
            string strCarrier = lstCarrier[0].Name;
            
            string strJSON = '{"TransferOrders":[{"TO_ToWarehouse":"904","TO_ShipToZipCode":"27410","TO_ShipToStreet":"420 NORTH CHIMNEY ROCK RD","TO_ShipToState":"NC","TO_ShipToName":"ASC- GREENSBORO","TO_ShipToCustomerNumber":"VET-0014045-001","TO_ShipToCountry":"United States","TO_ShipToCity":"GREENSBORO","TO_RequiredDate":"2018-10-30","TO_OrderNumber":"954550","TO_OrderDate":"2018-10-30","TO_FromWarehouse":"901","TO_Carrier":"Parcel","TransferOrderLines":[{"TOL_FromWarehouseCode":"901","TOL_StockUOM":"EA","TOL_ShippedQuantity":1.00,"TOL_Quantity":1.00,"TOL_ProductCode":"8528","TOL_OrderLineNumber":"954550-001","TOL_NonStockQuantity":0.00,"TOL_IsNonStock":false},{"TOL_FromWarehouseCode":"901","TOL_StockUOM":"EA","TOL_ShippedQuantity":1.00,"TOL_Quantity":1.00,"TOL_ProductCode":"8528","TOL_OrderLineNumber":"954550-002","TOL_NonStockQuantity":0.00,"TOL_IsNonStock":false}]}]}';
            
            
            giic_TransferOrderWrapper transferOrderWrapper =(giic_TransferOrderWrapper)json.deserialize(strJSON, giic_TransferOrderWrapper.class);
            
            if( transferOrderWrapper != null && transferOrderWrapper.TransferOrders!= null && transferOrderWrapper.TransferOrders.size() > 0 ){
                for(giic_TransferOrderWrapper.TransferOrder objTO : transferOrderWrapper.TransferOrders ){
                    objTO.TO_FromWarehouse = null;
                    objTO.TO_ToWarehouse = null;
                    objTO.TO_ShipToCustomerNumber = null;
                    objTO.TO_Carrier = null;
                    // setting to large value in order number to create error
                    objTO.TO_OrderNumber = null;
                    for(giic_TransferOrderWrapper.TransferOrderLine oTOL : objTO.TransferOrderLines){
                        oTOL.TOL_FromWarehouseCode = null;
                        oTOL.TOL_ProductCode = null;                     
                    }
                }
            }
            
            string sTOJSON = json.serialize(transferOrderWrapper);
            RestRequest req = new RestRequest();
            req.requestBody = Blob.valueOf(sTOJSON);
            RestContext.request = req;
            giic_TransferOrderWS.createTransferOrder();
            
        }
        Test.stopTest();
       
       
        
    }
    
    
    public static testMethod void test_createTransferOrder_NegativeCase1(){
        QueryData();
        Test.startTest();
        
        system.runAs(objTestClassUser){
            
            List<gii__Carrier__c> lstCarrier = [select Name, gii__NoChargeReason__c from gii__Carrier__c];
            string strToWarehouseCode = giic_Test_DataCreationUtility.lstWarehouse_N[0].giic_WarehouseCode__c;
            string strShipToCustomerNumber = giic_Test_DataCreationUtility.lstAccount_N[0].ShipToCustomerNo__c;
            string strFromWarehouse = giic_Test_DataCreationUtility.lstWarehouse_N[1].giic_WarehouseCode__c;
            string strProductCode1 = giic_Test_DataCreationUtility.lstProdRef_N[0].giic_ProductSKU__c;
            string strProductCode2 = giic_Test_DataCreationUtility.lstProdRef_N[1].giic_ProductSKU__c;
            string strCarrier = lstCarrier[0].Name;
            
            string strJSON = '{"TransferOrders":[{"TO_ToWarehouse":"904","TO_ShipToZipCode":"27410","TO_ShipToStreet":"420 NORTH CHIMNEY ROCK RD","TO_ShipToState":"NC","TO_ShipToName":"ASC- GREENSBORO","TO_ShipToCustomerNumber":"VET-0014045-001","TO_ShipToCountry":"United States","TO_ShipToCity":"GREENSBORO","TO_RequiredDate":"2018-10-30","TO_OrderNumber":"954550","TO_OrderDate":"2018-10-30","TO_FromWarehouse":"901","TO_Carrier":"Parcel","TransferOrderLines":[{"TOL_FromWarehouseCode":"901","TOL_StockUOM":"EA","TOL_ShippedQuantity":1.00,"TOL_Quantity":1.00,"TOL_ProductCode":"8528","TOL_OrderLineNumber":"954550-001","TOL_NonStockQuantity":0.00,"TOL_IsNonStock":false},{"TOL_FromWarehouseCode":"901","TOL_StockUOM":"EA","TOL_ShippedQuantity":1.00,"TOL_Quantity":1.00,"TOL_ProductCode":"8528","TOL_OrderLineNumber":"954550-002","TOL_NonStockQuantity":0.00,"TOL_IsNonStock":false}]}]}';
            
            
            giic_TransferOrderWrapper transferOrderWrapper =(giic_TransferOrderWrapper)json.deserialize(strJSON, giic_TransferOrderWrapper.class);
            
            if( transferOrderWrapper != null && transferOrderWrapper.TransferOrders!= null && transferOrderWrapper.TransferOrders.size() > 0 ){
                for(giic_TransferOrderWrapper.TransferOrder objTO : transferOrderWrapper.TransferOrders ){
                    objTO.TO_FromWarehouse = strFromWarehouse;
                    objTO.TO_ToWarehouse = strToWarehouseCode;
                    objTO.TO_ShipToCustomerNumber = strShipToCustomerNumber;
                    objTO.TO_Carrier = strCarrier;
                    // setting to large value in order number to create error
                    objTO.TO_OrderNumber = '11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111';
                    for(giic_TransferOrderWrapper.TransferOrderLine oTOL : objTO.TransferOrderLines){
                        oTOL.TOL_FromWarehouseCode = strFromWarehouse;
                        oTOL.TOL_ProductCode = strProductCode1;                     
                    }
                }
            }
            
            string sTOJSON = json.serialize(transferOrderWrapper);
            RestRequest req = new RestRequest();
            req.requestBody = Blob.valueOf(sTOJSON);
            RestContext.request = req;
            giic_TransferOrderWS.createTransferOrder();
            
            strJSON = '{"TransferOrders":[{"TO_ToWarehouse":"904","TO_ShipToZipCode":"27410","TO_ShipToStreet":"420 NORTH CHIMNEY ROCK RD","TO_ShipToState":"NC","TO_ShipToName":"ASC- GREENSBORO","TO_ShipToCustomerNumber":"VET-0014045-001","TO_ShipToCountry":"United States","TO_ShipToCity":"GREENSBORO","TO_RequiredDate":"2018-10-30","TO_OrderNumber":"954550","TO_OrderDate":"2018-10-30","TO_FromWarehouse":"901","TO_Carrier":"Parcel","TransferOrderLines":[{"TOL_FromWarehouseCode":"901","TOL_StockUOM":"EA","TOL_ShippedQuantity":100000.00,"TOL_Quantity":100000.00,"TOL_ProductCode":"8528","TOL_OrderLineNumber":"954550-001","TOL_NonStockQuantity":0.00,"TOL_IsNonStock":false},{"TOL_FromWarehouseCode":"901","TOL_StockUOM":"EA","TOL_ShippedQuantity":1.00,"TOL_Quantity":1.00,"TOL_ProductCode":"8528","TOL_OrderLineNumber":"954550-002","TOL_NonStockQuantity":0.00,"TOL_IsNonStock":false}]}]}';
            
            
            transferOrderWrapper =(giic_TransferOrderWrapper)json.deserialize(strJSON, giic_TransferOrderWrapper.class);
            
            if( transferOrderWrapper != null && transferOrderWrapper.TransferOrders!= null && transferOrderWrapper.TransferOrders.size() > 0 ){
                for(giic_TransferOrderWrapper.TransferOrder objTO : transferOrderWrapper.TransferOrders ){
                    objTO.TO_FromWarehouse = strFromWarehouse;
                    objTO.TO_ToWarehouse = strToWarehouseCode;
                    objTO.TO_ShipToCustomerNumber = strShipToCustomerNumber;
                    objTO.TO_Carrier = strCarrier;
                    for(giic_TransferOrderWrapper.TransferOrderLine oTOL : objTO.TransferOrderLines){
                        oTOL.TOL_FromWarehouseCode = strFromWarehouse;
                        oTOL.TOL_ProductCode = strProductCode1;                     
                    }
                }
            }
            
            sTOJSON = json.serialize(transferOrderWrapper);
            
            req = new RestRequest();
            req.requestBody = Blob.valueOf(sTOJSON);
            RestContext.request = req;
            giic_TransferOrderWS.createTransferOrder();
             //  creation of recipts  
            strJSON = '{ "TransferOrderReceipts": [ { "TOR_fromWarehouse": "901", "TOR_toWarehouse": "904", "TOR_requiredDate": "2018-10-30", "TOR_orderNumber": "954550", "TransferOrderReceiptLines": [ { "TORL_toWarehouseCode": "904", "TORL_receiptQuantity": 1.00, "TORL_shippedQuantity": 1.00, "TORL_productCode": "8528", "TORL_orderLineNumber": "954550-001", "TORL_StockUOM": "EA" }, { "TORL_warehouseCode": "904", "TORL_receiptQuantity": 1.00, "TORL_shippedQuantity": 1.00, "TORL_productCode": "8528", "TORL_orderLineNumber": "954550-002", "TORL_StockUOM": "EA" } ] } ] }';
            transferOrderWrapper =(giic_TransferOrderWrapper)json.deserialize(strJSON, giic_TransferOrderWrapper.class);
         
            if( transferOrderWrapper != null && transferOrderWrapper.TransferOrderReceipts!= null && transferOrderWrapper.TransferOrderReceipts.size() > 0 ){
                for(giic_TransferOrderWrapper.TransferOrderReceipt objTO : transferOrderWrapper.TransferOrderReceipts ){
                    objTO.TOR_FromWarehouse = strFromWarehouse;
                    objTO.TOR_ToWarehouse = strToWarehouseCode;
                    for(giic_TransferOrderWrapper.TransferOrderRecieptLine oTOL : objTO.TransferOrderReceiptLines){
                        oTOL.TORL_ToWarehouseCode = strFromWarehouse;
                        oTOL.TORL_ProductCode = strProductCode1;                     
                    }
                }
            }
         
            sTOJSON = json.serialize(transferOrderWrapper);
            req = new RestRequest();
            req.requestBody = Blob.valueOf(sTOJSON);
            RestContext.request = req;
            giic_TransferOrderRecieptWS.createTransferOrderReceipt();   

            gii.TransferOrderReceipt.TransferOrderReceiptsResult resultObj = new gii.TransferOrderReceipt.TransferOrderReceiptsResult();
            List<gii.TransferOrderReceipt.GOMException> lstGOMExc = new List<gii.TransferOrderReceipt.GOMException>();
            resultObj.Exceptions = lstGOMExc;
            giic_TransferOrderRecieptWS.handleExceptions(resultObj);


            // giic_TransferOrderWrapper objTOW = new giic_TransferOrderWrapper();
            giic_TransferOrderWrapper.TransferOrderReceipt  objTOR2 = new giic_TransferOrderWrapper.TransferOrderReceipt();
            giic_TransferOrderWrapper.TransferOrderReceipt  objTOR = new giic_TransferOrderWrapper.TransferOrderReceipt('xxx','Error');
          
        }
        Test.stopTest();
       
       
        
    }
    
    public static testMethod void test_createTransferOrder_NegativeCase2(){
        QueryData();
        Test.startTest();
        
        system.runAs(objTestClassUser){
            
            List<gii__Carrier__c> lstCarrier = [select Name, gii__NoChargeReason__c from gii__Carrier__c];
            string strToWarehouseCode = giic_Test_DataCreationUtility.lstWarehouse_N[0].giic_WarehouseCode__c;
            string strShipToCustomerNumber = giic_Test_DataCreationUtility.lstAccount_N[0].ShipToCustomerNo__c;
            string strFromWarehouse = giic_Test_DataCreationUtility.lstWarehouse_N[1].giic_WarehouseCode__c;
            string strProductCode1 = giic_Test_DataCreationUtility.lstProdRef_N[0].giic_ProductSKU__c;
            string strProductCode2 = giic_Test_DataCreationUtility.lstProdRef_N[1].giic_ProductSKU__c;
            string strCarrier = lstCarrier[0].Name;
            
            string strJSON = '{"TransferOrders":[{"TO_ToWarehouse":"904","TO_ShipToZipCode":"27410","TO_ShipToStreet":"420 NORTH CHIMNEY ROCK RD","TO_ShipToState":"NC","TO_ShipToName":"ASC- GREENSBORO","TO_ShipToCustomerNumber":"VET-0014045-001","TO_ShipToCountry":"United States","TO_ShipToCity":"GREENSBORO","TO_RequiredDate":"2018-10-30","TO_OrderNumber":"954550","TO_OrderDate":"2018-10-30","TO_FromWarehouse":"901","TO_Carrier":"Parcel","TransferOrderLines":[{"TOL_FromWarehouseCode":"901","TOL_StockUOM":"EA","TOL_ShippedQuantity":1.00,"TOL_Quantity":1.00,"TOL_ProductCode":"8528","TOL_OrderLineNumber":"954550-001","TOL_NonStockQuantity":0.00,"TOL_IsNonStock":false},{"TOL_FromWarehouseCode":"901","TOL_StockUOM":"EA","TOL_ShippedQuantity":1.00,"TOL_Quantity":1.00,"TOL_ProductCode":"8528","TOL_OrderLineNumber":"954550-002","TOL_NonStockQuantity":0.00,"TOL_IsNonStock":false}]}]}';
            
            
            giic_TransferOrderWrapper transferOrderWrapper =(giic_TransferOrderWrapper)json.deserialize(strJSON, giic_TransferOrderWrapper.class);
            
            if( transferOrderWrapper != null && transferOrderWrapper.TransferOrders!= null && transferOrderWrapper.TransferOrders.size() > 0 ){
                for(giic_TransferOrderWrapper.TransferOrder objTO : transferOrderWrapper.TransferOrders ){
                    objTO.TO_FromWarehouse = strFromWarehouse;
                    objTO.TO_ToWarehouse = strToWarehouseCode;
                    objTO.TO_ShipToCustomerNumber = strShipToCustomerNumber;
                    objTO.TO_Carrier = strCarrier;
                    integer counter = 1;
                    for(giic_TransferOrderWrapper.TransferOrderLine oTOL : objTO.TransferOrderLines){
                        oTOL.TOL_FromWarehouseCode = strFromWarehouse;
                        oTOL.TOL_ProductCode = strProductCode1;    
                        oTOL.TOL_OrderLineNumber = '1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111' + counter; 
                        counter ++;
                    }
                }
            }
            
            string sTOJSON = json.serialize(transferOrderWrapper);
            
            RestRequest req = new RestRequest();
            req.requestBody = Blob.valueOf(sTOJSON);
            
            RestContext.request = req;
            
            giic_TransferOrderWrapper.TransferOrderResult objrespo = giic_TransferOrderWS.createTransferOrder();
        }
        Test.stopTest();
       
       
        
    }
    
    public static testMethod void test_NegativeCase2_TryToCreateException(){
        QueryData();
        Test.startTest();
        
        system.runAs(objTestClassUser){
            
            List<gii__Carrier__c> lstCarrier = [select Name, gii__NoChargeReason__c from gii__Carrier__c];
            string strToWarehouseCode = giic_Test_DataCreationUtility.lstWarehouse_N[0].giic_WarehouseCode__c;
            string strShipToCustomerNumber = giic_Test_DataCreationUtility.lstAccount_N[0].ShipToCustomerNo__c;
            string strFromWarehouse = giic_Test_DataCreationUtility.lstWarehouse_N[1].giic_WarehouseCode__c;
            string strProductCode1 = giic_Test_DataCreationUtility.lstProdRef_N[0].giic_ProductSKU__c;
            string strProductCode2 = giic_Test_DataCreationUtility.lstProdRef_N[1].giic_ProductSKU__c;
            string strCarrier = lstCarrier[0].Name;
            
            string strJSON = '{"TransferOrders":[]}';
            
            
            giic_TransferOrderWrapper transferOrderWrapper =(giic_TransferOrderWrapper)json.deserialize(strJSON, giic_TransferOrderWrapper.class);
            
            if( transferOrderWrapper != null && transferOrderWrapper.TransferOrders!= null && transferOrderWrapper.TransferOrders.size() > 0 ){
                for(giic_TransferOrderWrapper.TransferOrder objTO : transferOrderWrapper.TransferOrders ){
                    objTO.TO_FromWarehouse = strFromWarehouse;
                    objTO.TO_ToWarehouse = strToWarehouseCode;
                    objTO.TO_ShipToCustomerNumber = strShipToCustomerNumber;
                    objTO.TO_Carrier = strCarrier;
                    integer counter = 1;
                    for(giic_TransferOrderWrapper.TransferOrderLine oTOL : objTO.TransferOrderLines){
                        oTOL.TOL_FromWarehouseCode = strFromWarehouse;
                        oTOL.TOL_ProductCode = strProductCode1;    
                        oTOL.TOL_OrderLineNumber = '23423111' + counter; 
                        oTOL.TOL_Quantity = 999999999; 
                        counter ++;
                    }
                }
            }
            
            string sTOJSON = json.serialize(transferOrderWrapper);
            RestRequest req = new RestRequest();
            req.requestBody = Blob.valueOf(sTOJSON);
            
            RestContext.request = req;
            
            giic_TransferOrderWrapper.TransferOrderResult objrespo = giic_TransferOrderWS.createTransferOrder();

        }
        Test.stopTest();
       
       
        
    }
    
}