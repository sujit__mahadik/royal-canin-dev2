/*
* 09-27-2017   -Bethi Reddy, Royal Canin     - Edited (Changed error message display popup (SCH-0052))
*/

public with sharing class CustomerOnboardingController {

    public Customer_Registration__c registration { get; set; }
    public pageState state { get; set; }
    public Map<string, reg_steps__c> registrationSteps { get; set; }
    public Map<string, Registration_Record_Type_Lookup__c> registrationRecTypeLkp { get; set; }
    public final Map<String, Id> registrationRecordTypes = new Map<String, Id>();
    public final Map<String, Id> accountRecordTypes = new Map<String, Id>();
    public string selectedAttachmentId { get; set; }
    public List<Attachment> attachments { get; set; }
    public AttachmentUploadController auc {get;set;}
    public final static Set <String> readOnlyStatusSet;
    //public final static Set<string> applicationStatusMulesoftValues { get; }
    public final static Set<string> applicationStatusDuplicateEmailValues { get; }
    public final static Map<string, string> errorMessages { get; }
    public boolean displayPopup {get; set;}
    public boolean disableStartApp {get; set;}
    public boolean proceedToCompletion {get;set;}
    public boolean attachmentProcessed {get;set;}
   
    static {
        
        applicationStatusDuplicateEmailValues = new Set<String>();
        applicationStatusDuplicateEmailValues.add('In Process');
        applicationStatusDuplicateEmailValues.add('Open');
        applicationStatusDuplicateEmailValues.add('Submitted');
        applicationStatusDuplicateEmailValues.add('Approved');
        applicationStatusDuplicateEmailValues.add('Processed');
        applicationStatusDuplicateEmailValues.add('Processing Failed');
        applicationStatusDuplicateEmailValues.add('Rejected');

        errorMessages = new Map<string, string>();
        //errorMessages.put('account', 'You already have an account. Please <a href="https://my.royalcanin.com/contactusorderservices">contact customer service.</a>');
        errorMessages.put('account', 'You already have an account. Please <apex:outputLink  onclick=\"openWindow()\" style=\"text-decoration: underline;\">contact customer service.</apex:outputLink>');                
        errorMessages.put('billto', 'There is no account with that Bill-To Customer Id.');
        errorMessages.put('cliniccustomer', 'No existing account with that Clinic Customer Number.'); 
        errorMessages.put('email' , 'You already have an account. Please <apex:outputLink  onclick=\"openWindow()\" style=\"text-decoration: underline;\">contact order services.</apex:outputLink>');
        errorMessages.put('invalid_email','Please enter a valid email address');
        errorMessages.put('pending' , 'You already have an account with Royal Canin, please contact order services');

        readOnlyStatusSet = new Set<string>();
        readOnlyStatusSet.add('Processed');
        readOnlyStatusSet.add('Processing Failed');
        readOnlyStatusSet.add('Approved');
        readOnlyStatusSet.add('Submitted');
        readOnlyStatusSet.add('In-Process');
        readOnlyStatusSet.add('Duplicate');
    }

    public boolean accepted { get; set; }

    public string applicationId { get; set; }

    public string applicationIdURLEncoded {
        get{
            if (applicationid != null){ return encodingUtil.urlencode(applicationId, 'UTF-8'); }
            else { return null; }
        }
    }

    public Set<String>needsAttachmentCustomerTypes = new Set<String>{'Veterinary','Retailer','Shelter','VET-Sell To','RET-Sell To','Working Dog'};

    public boolean needsAttachments {
        get{
            if (this.state != null && (needsAttachmentCustomerTypes.contains(this.state.type))) {
                return true;
            }
            else {
                return false;
            }
        }
    }

    /*public boolean needsAttachments {
        get{
            if (this.state != null && (this.state.type == 'Veterinary' || this.state.type == 'Retailer' || this.state.type == 'Shelter')) {
                return true;
            }
            else {
                return false;
            }
        }
    }*/

    public CustomerOnBoardingController getPageCont(){
        return this;
    }

    public boolean isVetRetail {
        get {
            if(this.state != null && (this.state.type == 'Veterinary' || this.state.type == 'Retailer')) {
                return true;
            } else {
                return false;
            }
        }
    }

    public boolean isRetail {
        get {
            if(this.state != null && this.state.type == 'Retailer') { return true; }
            else { return false; }
        }
    }

    public boolean isVet {
        get {
            if(this.state != null && this.state.type == 'Veterinary') { return true; }
            else { return false; }
        }
    }
    
     public boolean isVetRetailSellTo {
        get {
            if(this.state != null && (this.state.type == 'VET-Sell To' || this.state.type == 'RET-Sell To')) {
                return true;
            } else {
                return false;
            }
        }
    }

    public string termsURL {
        get {
            Global_Parameters__c appURLStub = Global_Parameters__c.getValues('ApplicationURLStub');
            String baseUrl = appURLStub.value__c +'TermsandConditions';
            if(this.state != null){
                if(this.state.type == 'Retailer' || this.state.type == 'RET-Sell To') {
                    return baseUrl + '?res=Retail';
                } else if(this.state.type == 'Cat Breeder' || this.state.type == 'Dog Breeder') {
                    return baseUrl + '?res=CatAndDogBreeder';
                } else if(this.state.type == 'Working Dog') {
                    return baseUrl + '?res=WorkingDog';
                } else if(this.state.type == 'Shelter') {
                    return baseUrl + '?res=Shelter';
                } else if(this.state.type == 'Veterinary' || this.state.type == 'VET-Sell To' || this.state.type == 'University Feeding Participant' || this.state.type == 'Clinic Feeding Participant') {
                    return baseUrl + '?res=VetClinicUniFeeding';
                } else if(this.state.type == 'Banfield Associate') {
                    return baseUrl + '?res=Banfield';
                } else if(this.state.type == 'RC Associate') {
                    return baseUrl + '?res=RC';
                }
            }
            return '';
        }
    }

    public CustomerOnboardingController(ApexPages.StandardController controller) {
        if (ApexPages.currentPage().getParameters().get('applicationid') != null) {
            applicationId = ApexPages.currentPage().getParameters().get('applicationid');
        }

        state = new pageState(this);
        registrationSteps = reg_steps__c.getAll();
        registrationRecTypeLkp = Registration_Record_Type_Lookup__c.getAll();
        proceedToCompletion = false;

        try {
            System.debug('application id '+applicationId);
            registration= [select id, customer_type__c, recordType.developername, Application_Status__c, Terms_and_Conditions_Accepted__c from Customer_Registration__c where application_id__c = :applicationId];
            string appType = registration_record_type_mapping__c.getAll().get(registration.recordType.developerName).customer_type__c;

            if (readOnlyStatusSet.contains(registration.Application_Status__c)){
                this.state.isReadOnly = true;
            }
            else {
                this.state.isReadOnly = false;
            }

            this.accepted = registration.Terms_and_Conditions_Accepted__c;

            setType(appType);
            initPaging();
            registration = getRegistration();
        } catch (Exception ex) {
            registration = new Customer_Registration__c();
        }
        getAttachments();

        try {
            List<RecordType> rtypes = [select developerName, id from RecordType where sObjectType = 'Customer_Registration__c'];
            for (RecordType recType : rtypes) {
                registrationRecordTypes.put(recType.developerName, recType.Id);
            }
        } catch (Exception ex) {}

        try {
            List<RecordType> rtypes = [select developerName, id from RecordType where sObjectType = 'Account'];
            for (RecordType recType : rtypes) {
                accountRecordTypes.put(recType.developerName, recType.Id);
            }
        } catch (Exception ex) {}
    }

    public List<Schema.FieldSetMember> getFields() {
        System.debug('CurrentStep '+this.state.getCurrentStep());
        if(this.state.getCurrentStep() != 'attachments' && SObjectType.Customer_Registration__c.fieldSets.getMap().get(this.state.getCurrentStep()) != null) {
            return SObjectType.Customer_Registration__c.fieldSets.getMap().get(this.state.getCurrentStep()).getFields();
        }
        else {
            return null;
        }
    }

    public List <Schema.SobjectField> getAllFields(){
        SObjectType accountType = Schema.getGlobalDescribe().get('Customer_Registration__c');
        Map<String,Schema.SObjectField> mfields = accountType.getDescribe().fields.getMap();
        return mfields.values();
    }

    private Customer_Registration__c getRegistration() {
        String query = 'SELECT ';

        for (Schema.SObjectField f : this.getAllFields()) {
            if (f.getDescribe().getName()!='Id'&&f.getDescribe().getName()!='Name' && f.getDescribe().getName() != 'Validated__c') {
                query += f + ', ';
            }
        }

        query += 'Id, Name FROM Customer_Registration__c WHERE Application_ID__c = \'';
        query += applicationId + '\' LIMIT 1';
        return Database.query(query);
    }

    public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Banfield Associate', 'Banfield Associate'));
        options.add(new SelectOption('Cat Breeder', 'Cat Breeder'));
        options.add(new SelectOption('Clinic Feeding Participant', 'Clinic Feeding Participant'));
        options.add(new SelectOption('Dog Breeder', 'Dog Breeder'));
        options.add(new SelectOption('Retailer', 'Retailer'));
        options.add(new SelectOption('RC Associate', 'Royal Canin Associate'));
        options.add(new SelectOption('Shelter', 'Shelter'));
        options.add(new SelectOption('University Feeding Participant', 'University Feeding Participant'));
        options.add(new SelectOption('Veterinary', 'Veterinary Clinic'));
        options.add(new SelectOption('Working Dog', 'Working Dog Professional'));
        options.add(new SelectOption('VET-Sell To', 'Additional Vet Sell-To'));
        options.add(new SelectOption('RET-Sell To', 'Additional Retail Sell-To'));
        return options;
    }

    public void initPaging() {
        this.state.steps = registrationSteps.get(this.state.type).Steps__c.split(',', 0);
        this.state.render('main');
    }

    public pageReference setDefaultRedirect(){
        PageReference pr = new PageReference('/apex/CustomerOnboarding?applicationId='+applicationId);
        pr.setRedirect(true);
        return pr;
    }

    public pageReference start() {
                 disableStartApp = true;
        if (this.state.email == null) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, errorMessages.get('email')));
            showError();
            return ApexPages.currentPage();
        }

       // if (isEmailDupe()) {
       //   ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, errorMessages.get('pending')));
       //     showError();
        //    return ApexPages.currentPage();
       //  }

       // checkIfPreviousRegistrationCustomerTypeIsSame();

        registration.recordTypeId = registrationRecordTypes.get( registrationRecTypeLkp.get(this.state.type).Record_Type_Dev_Name__c );
        system.debug('#$#$$state.type:' + this.state.type);
        system.debug('#$#$$registrationRecTypeLkp.get(this.state.type):' + registrationRecTypeLkp.get(this.state.type));
        system.debug('#$#$$registrationRecordTypes.get( registrationRecTypeLkp.get(this.state.type):' + registrationRecordTypes.get( registrationRecTypeLkp.get(this.state.type).Record_Type_Dev_Name__c));
        system.debug('#$#$$registrationRecordTypes.get( registrationRecTypeLkp.get(this.state.type).Record_Type_Dev_Name__c:' + registrationRecordTypes.get( registrationRecTypeLkp.get(this.state.type).Record_Type_Dev_Name__c));
        system.debug('#$#$$rectypeid:' + registration.recordTypeId);
        registration.Application_Status__c = 'Open';
        registration.Email_Address__c = this.state.email;

        this.state.isReadOnly = false;

        try {
        validateEmail();
            upsert registration;
        } catch (ApplicationFoundException apx){ 
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, errorMessages.get('email')));
        showError();
        return ApexPages.currentPage();
        } catch (exception ex) {
        system.debug(ex);
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, errorMessages.get('invalid_email')));
                showError();
            return ApexPages.currentPage();
        }
        initPaging();

        Customer_Registration__c reg = [select Link_to_Application__c, Application_ID__c, Application_Status__c, Email_Address__c from Customer_Registration__c where id =: registration.Id];
        string linkToApp = reg.Link_to_Application__c;
        applicationid = reg.Application_ID__c;

        registration = getRegistration();

        PageReference pr = new PageReference('/apex/customeronboarding?applicationId='+EncodingUtil.URLEncode(applicationid, 'UTF-8'));
        //PageReference pr = new PageReference(linkToApp);
        pr.setRedirect(true);
        return pr;
    }

    public void next() {
         if (readOnlyStatusSet.contains(registration.Application_Status__c)){
                this.state.isReadOnly = true;
         }
        if (!this.state.isReadOnly){

            try {
                validateClinicCustomerNumber();
                validateBillToAccountIds();
                validateAccountAlreadyExists();
                System.debug('$$');
            } catch (BillToException ex) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, errorMessages.get('billto')));
                showError();
                return;
            } catch (ClinicCustomerException ex) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, errorMessages.get('cliniccustomer')));
                showError();
                return;
            } catch (CustomerFoundException ex){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, errorMessages.get('account')));
                showError();
                updateApplitionToDuplicate();
                return;
         /*   } catch (ClinicCustomerFoundException ex){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, errorMessages.get('cliniccustomer')));
                showError();
                updateApplitionToDuplicate();
                return;          */         
            }catch (QueryException ex){
                return;
            }

            checkForUploads();

            try {
                registration.Last_Page_Saved__c = state.curStep;
                upsert registration;
            } catch (DmlException ex) {
                addError(ex);
                return;
            }
        }

        state.next();
        registration = getRegistration();
    }

    public void save() {
        if (!this.state.isReadOnly){

            try {
                validateClinicCustomerNumber();
                validateBillToAccountIds();
            } catch (BillToException ex) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, errorMessages.get('billto')));
                showError();
                return;
            } catch (ClinicCustomerException ex) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, errorMessages.get('cliniccustomer')));
                showError();
                return;
            } catch (QueryException ex){
                return;
            }

            checkForUploads();

            try {
                upsert registration;
            } catch (DmlException ex) {
                addError(ex);
                return;
            }
        }

        registration = getRegistration();
    }

    public pageReference prev_pr(){
        prev();
        return null;
    }

     public pageReference next_pr(){
        next();
        return ApexPages.currentPage();
    }

      public pageReference save_pr(){
        save();
        return null;
    }

      public pageReference submit_pr(){
        submit();
        if(this.state.renderError){
            return ApexPages.currentPage();
        }
        PageReference pageRef = new PageReference(ApexPages.currentPage().getUrl());
        pageRef.setRedirect(true);
        return pageRef;
    }

    public void prev() {
        if (!this.state.isReadOnly){
            ApexPages.getMessages().clear();

            try {
                validateClinicCustomerNumber();
                validateBillToAccountIds();
            } catch (BillToException ex) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, errorMessages.get('billto')));
                showError();
                return;
            } catch (ClinicCustomerException ex) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, errorMessages.get('cliniccustomer')));
                showError();
                return;
            } catch (QueryException ex){
                return;
            }

            checkForUploads();

            try {
                upsert registration;
            } catch (DmlException ex) {
                addError(ex);
                return;
            }
        }

        state.prev();
        registration = getRegistration();
    }

    public void submit() {

        if (!this.state.isReadOnly){

            try {
        
        validateEmail();
                validateClinicCustomerNumber();
                validateBillToAccountIds();
            } catch (BillToException ex) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, errorMessages.get('billto')));
                showError();
                return;
            } catch (ClinicCustomerException ex) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, errorMessages.get('cliniccustomer')));
                showError();
                return;
        
            } catch (ApplicationFoundException ex) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, errorMessages.get('email')));
                showError();
                return;
                
                
            } catch (QueryException ex){
                return;
            }

            checkForUploads();

            registration.Application_Status__c = 'Submitted';
            registration.Terms_and_Conditions_Accepted__c = this.accepted;

            try {
                upsert registration;
                this.state.isReadOnly = true;
                this.state.render('complete');
            } catch (Exception ex) {
                System.debug('ERROR:' + ex.getMessage());
                addError(ex);
                registration.Application_Status__c = 'Open';
            }

        }
    }

   // public Boolean isEmailDupe() {

   //     List<Customer_Registration__c> previousRegistrations;

   //    //  previousRegistrations = [select Id, Application_Status__c, RecordType.DeveloperName
    //                              from Customer_Registration__c
   //    //                         where Email_Address__c = : state.email and
   //    //                           Application_Status__c not in: applicationStatusMulesoftValues];




    //   previousRegistrations = [
    //      SELECT Id, Application_Status__c, RecordType.DeveloperName, Customer_Type__c
    //       FROM Customer_Registration__c
    //       WHERE Email_Address__c = :state.email
    //       AND Application_Status__c IN :applicationStatusDuplicateEmailValues
    //       AND Customer_Type__c = :state.type];


    //   // System.debug('STATE TYPE: ' + state.type + ' CUSTOMER TYPE: ' + previousRegistrations[0].Customer_Type__c);

    //    if (this.state.type == 'RET-Sell To' || this.state.type == 'VET-Sell To') {
     //       return false;
    //    } else if(previousRegistrations.size() > 0 ) {
    //       return true;
    //    } else {
    //       return false;
    //   }
   // }

   // public void checkIfPreviousRegistrationCustomerTypeIsSame(){
    //    Customer_Registration__c previousRegistration = [SELECT Id, Application_Status__c, RecordType.DeveloperName, Customer_Type__c
    //                                                    FROM Customer_Registration__c
    //                                                   WHERE Email_Address__c = :state.email
    //                                                    AND Application_Status__c IN :applicationStatusDuplicateEmailValues
    //                                                    AND Customer_Type__c = :state.type LIMIT 1];

    //   registration = previousRegistration;
    //}

    public String getType() {
        return this.state.type;
    }

    public void setType(String accountType) {
        this.state.type = accountType;
    }

    public void enableStartBtn() {
        if (this.state.email != null) {
            this.state.renderStart = true;
        } else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter a valid email address.'));
            showError();
        }
    }

    public void showError() {
        this.state.renderError = true;
        this.displayPopup = false;
    }

    public List<Attachment> getAttachments() {
        if (null == attachments) {
            attachments = [select Id, ParentId, Name, Description from Attachment where parentId = :registration.id];
        }

        return attachments;
    }

    public void deleteAttachment() {

        delete [select id from Attachment where id = :selectedAttachmentId];

        getattachments();
    }

    public void checkForUploads(){
        Customer_Registration__c registrationAfterUpload;

        try {
            registrationAfterUpload = [select Upload_Complete_Tax__c, Upload_Complete_Resale__c, Upload_Complete_Practitioner__c
                                       from Customer_Registration__c
                                       where Id =: registration.Id];
        }
        catch(Exception ex){
            registrationAfterUpload = new Customer_Registration__c();
        }

        //this is to check if file upload has happened since last registration query
        //can happen inbetween page refreshes here
        registration.Upload_Complete_Practitioner__c = registrationAfterUpload.Upload_Complete_Practitioner__c;
        registration.Upload_Complete_Tax__c = registrationAfterUpload.Upload_Complete_Tax__c;
        registration.Upload_Complete_Resale__c = registrationAfterUpload.Upload_Complete_Resale__c;
    }

    public void addError(Exception ex) {

        //System.debug('ex.getMessage() '+ex.getMessage());
        //if(ex.getMessage().contains('CANNOT_EXECUTE_FLOW_TRIGGER')){
        //    return;
        //}
        string message = ex.getMessage().substringAfter('FIELD_CUSTOM_VALIDATION_EXCEPTION, ');
        System.debug('MSG: ' + message);
        message = message.substring(0, message.indexOf(':'));
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,  message));
        showError();
    }

    public pageReference uploadNext(){
        PageReference uploadPage = new PageReference('/apex/AttachmentUpload?applicationId='+applicationIdURLEncoded);
        uploadPage.setRedirect(true);
        return uploadPage;
    }

    
    public void validateEmail() {
    if(!isVetRetail){
    String stateType = (state.type == 'Working Dog') ? 'Working Dog Professional' : state.type;
    Integer count = [select count() from Customer_Registration__c where Email_Address__c = : state.email and  Customer_Type__c =:stateType];   
    if(count != 0){
        throw new ApplicationFoundException();
    }
    }
    }


    public void validateClinicCustomerNumber() {
        if(this.state.type == 'Clinic Feeding Participant' && registration.ClinicCustomerNo__c != null) {
            List<Account> accounts;
            try {
                accounts = [select id, Account_Status__c
                            from Account
                            where ShipToCustomerNo__c =: registration.ClinicCustomerNo__c and
                            RecordTypeId =: accountRecordTypes.get('Veterinarian')];
            } catch(QueryException qex) {
                throw qex;
            }

            if(accounts.size() == 0) {
                throw new ClinicCustomerException();
            }
        }
    }

    public void validateBillToAccountIds() {
        if(registration.Customer_Account_ID__c != null) {
            List<Account> accounts;
            try {
                accounts = [select id, Account_Status__c
                            from Account
                            where Bill_To_Customer_ID__c =: registration.Customer_Account_ID__c and
                            ParentId = null];
            } catch(QueryException qex) {
                throw qex;
            }

            if(accounts.size() == 0) {
                throw new BillToException();
            }
        }
    }

    private static String[]  splitAddress(String streetAddress){
        System.debug('StreetSplit '+  streetAddress.split('[\r\n]'));
        return streetAddress.split('[\r\n]');

    }

   @TestVisible private List<Account> validateSellToAccountAlreadyExists(){
        List<Account> accounts;  
        List<Account> duplicateAccounts = new List<Account>();
        
             try {
                String shipStreet = registration.Shipping_Street_Address1__c +'%';
                accounts = [select id, Account_Status__c, Name, ShippingStreet, Customer_Type__c, Bill_To_Customer_ID__c
                            from Account
                            where ShippingStreet like :shipStreet and
                                  ShippingCity =:registration.Shipping_City__c and
                                  ShippingState =: registration.Ship_To_State__c and 
                                  Bill_To_Customer_ID__c != null and
                                  Do_Not_use__c = false and
                                  Customer_Type__c = :registration.Customer_Type__c] ;
            } catch(QueryException qex) {
                throw qex;
            }

            if(accounts!= null && accounts.size()>0){
                for(Account currentAccount : accounts){
                    List<String>addressLines = splitAddress(currentAccount.ShippingStreet);
                    if(addressLines != null && addressLines.size()>0){
                       if(registration.Shipping_Street_Address1__c == addressLines.get(0)){
                            duplicateAccounts.add(currentAccount);
                       }
                    }
                }
            }
    

        return duplicateAccounts;   
    }

    private void validateAccountAlreadyExists(){
        System.debug('$$ENTERED$$');
        List<Account> accounts = new List<Account>();
        
        if(registration.Customer_Type__c == 'Veterinary' || registration.Customer_Type__c == 'Retailer' ){
            return;
        }
        else if (this.state.type == 'RET-Sell To' || this.state.type == 'VET-Sell To') {
                accounts = validateSellToAccountAlreadyExists();
        }
        
        else if(registration.Customer_Type__c.contains('RC Associate')
                 || registration.Customer_Type__c.contains('Banfield Associate')) {
            String regName = registration.First_Name__c + ' '+ registration.Last_Name__c;
            try {
                    accounts = [select id, Account_Status__c, Name, Customer_Type__c, Bill_To_Customer_ID__c
                                from Account
                                where Phone = :registration.Phone_Number__c and
                                      Name = :regName and
                                      Bill_To_Customer_ID__c != null ] ;
            } catch(QueryException qex) {
                throw qex;
            }
            
        } else {
            String regName = registration.Name_of_Business_Organization__c ;
            if(registration.Customer_Type__c.contains('Feeding Participant') ){
                regName = registration.First_Name__c + ' '+ registration.Last_Name__c;
            }
            try {
                    accounts = [select id, Account_Status__c, Name, Customer_Type__c, Bill_To_Customer_ID__c
                                from Account
                                where Phone = :registration.Phone_Number__c and
                                      Name = :regName and
                                      Bill_To_Customer_ID__c != null and
                                      Do_Not_use__c = false] ;
            } catch(QueryException qex) {
                throw qex;
            }

          }

        for(Account acc : accounts){
            if(registration.User_Logon__c != null && (registration.Customer_Type__c.contains('Banfield Associate') || registration.Customer_Type__c.contains('RC Associate'))){
                if(acc.Bill_To_Customer_ID__c.contains(registration.User_Logon__c)){
                    throw new CustomerFoundException();
                }
                            
            } else if(registration.Customer_Type__c == acc.Customer_Type__c && registration.Customer_Type__c.contains('Clinic Feeding Participant') &&
             (acc.Account_Status__c == 'Prospect' || acc.Account_Status__c == NULL)) {
                throw new CustomerFoundException();
                
            } else if(registration.Customer_Type__c == acc.Customer_Type__c && (acc.Account_Status__c == 'Customer' || (acc.Account_Status__c == 'Prospect'))) {
                throw new CustomerFoundException();
            }
        }           
    }
    
    public void setDocumentUploaded(){

            checkForUploads();
            proceedToCompletion = true;
            registration.Application_Status__c = 'DocumentUploaded';

            try {
                upsert registration;
            } catch (Exception ex) {
                addError(ex);
                proceedToCompletion = false;
                return;
            }

          registration = getRegistration();

    }

    public class pageState {
        public CustomerOnboardingController controller { get; set; }
        public boolean renderRadio { get; set; }
        public boolean renderMain { get; set; }
        public boolean renderComplete { get; set; }
        public boolean renderStart { get; set; }
        public boolean renderError { get; set; }
        public boolean isReadOnly { get; set; }
        public string type { get; set; }
        public string email { get; set; }
        public integer curStep { get; set; }
        public List<string> steps {
            get;
            set {
                curStep = 0;
                steps = value;
                if (controller.needsAttachments == true){
                    steps.add('attachments');
                }
                //steps.add('submission');
            }
        }

        public boolean hasNext {
            get{
                if (steps != null && curStep + 1 < steps.size()) {
                    return true;
                } else { return false; }
            }
        }

        public boolean hasPrev {
            get{
                if (curStep - 1 >= 0) {
                    return true;
                } else { return false; }
            }
        }

        public boolean renderSubmit {
            get{
                if (steps != null && curStep == steps.size()-1 && !this.isReadOnly) {
                    return true;
                }
                else {
                    return false;
                }
            }
        }

        public boolean renderButtonsPanel {

            get{
                return !this.renderAttachment;
            }
        }


        public boolean renderAttachment {

            get {
                          System.debug('controller.proceedToCompletion '+controller.proceedToCompletion+' '+getCurrentStep()+''+controller.needsAttachments+''+renderComplete+''+controller.needsAttachments);
                if (controller.needsAttachments == true && getCurrentStep() == 'attachments' && renderComplete != true && !controller.proceedToCompletion) {
                    return true;
                }
                else { return false; }
            }
        }

        public string getCurrentStepForRerender {
            get{
                return this.getCurrentStep();
            }
        }

        public pageState(CustomerOnboardingController controller) {
            this.controller = controller;
            this.type = null;
            this.render('radio');
            this.renderStart = false;
            this.renderError = false;
            steps = new List<string>();
        }

        public void render(string toRender) {
            if (toRender == 'radio') {
                this.renderRadio = true;
                this.renderMain = false;
                this.renderComplete = false;
            } else if (toRender == 'main') {
                this.renderRadio = false;
                this.renderMain = true;
                this.renderComplete = false;
            } else if (toRender == 'complete') {
                this.renderRadio = false;
                this.renderMain = false;
                this.renderComplete = true;
            }
        }
        

        public void next() {
            if (hasNext) {
                curStep++;
            }
        }

        public void prev() {
            if (hasPrev) {
                curStep--;
            }
            controller.proceedToCompletion = false;
        }

        public string getCurrentStep() {
            if(curStep < steps.size()){
                return steps.get(curStep);
            }
            else {
                return '';
            }
        }
    }

    public class ProspectAccountException extends Exception {}
    public class BillToException extends Exception {}
    public class ClinicCustomerException extends Exception {}
    public class CustomerFoundException extends Exception {}

    public class ApplicationFoundException extends Exception {}
    public class InvalidEmailException extends Exception {}
    //public class ClinicCustomerFoundException extends Exception {}

    public void closePopup() {
        displayPopup = false;
    }
    public void showPopup() {
        disableStartApp = false;
        displayPopup = true;
    }

    public void updateApplitionToDuplicate(){
        registration.Application_Status__c = 'Duplicate';
        update registration;
    }
}