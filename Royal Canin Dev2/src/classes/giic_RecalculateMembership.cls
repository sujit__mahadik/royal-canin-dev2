public Class giic_RecalculateMembership
{
    public list<giic_MembershipType__c> MembershipSegmentLst{get;set;}
    public map<Id,AccountWrap> AccountWrapMap{get;set;}
    Map<Id,giic_MembershipType__c> PointToUpUpgrade = new map<Id,giic_MembershipType__c>();
    public giic_RecalculateMembership()
    {
       init();
    }
    
    public void init()
    {
       PointToUpUpgrade = new map<id,giic_MembershipType__c>();
       MembershipSegmentLst = new list<giic_MembershipType__c>();
       MembershipSegmentLst = [select id,name,giic_QualifyingPoints__c from giic_MembershipType__c order by giic_QualifyingPoints__c desc];
       
       AccountWrapMap = new map<Id,AccountWrap>();
       list<Account> LstOfAccount = new list<Account>();
       LstOfAccount = [select id,name,giic_LifeTimePoints__c,giic_TypeOfMembership__c,giic_TypeOfMembership__r.name from Account where giic_TypeOfMembership__c != null];
       
      if(!(LstOfAccount.isempty()))
      {
          for(Account AccObj : LstOfAccount)
          {
              AccountWrap TempObj = new AccountWrap();
              TempObj.AccountObj = Accobj;
              TempObj.RowColor = 'White';
              AccountWrapMap.put(AccObj.id,TempObj);
          }
      }
      
      for(integer i=MembershipSegmentLst.size()-1; i>0; i--)
      {
          PointToUpUpgrade.put(MembershipSegmentLst[i].id,MembershipSegmentLst[i-1]);
      }
    }
    
    public void StartRecalculateMembership()
    {
        ExecuteRecalculateMembership();
    }
    
    public void ExecuteRecalculateMembership()
    {
        list<Account> LstOfAccounToUpdate = new list<Account>();
        for(Id TempWrapAccountId : AccountWrapMap.keyset())
        {
            if(PointToUpUpgrade.containskey(AccountWrapMap.get(TempWrapAccountId).AccountObj.giic_TypeOfMembership__c))
            {
                if(AccountWrapMap.get(TempWrapAccountId).AccountObj.giic_LifeTimePoints__c >= PointToUpUpgrade.get(AccountWrapMap.get(TempWrapAccountId).AccountObj.giic_TypeOfMembership__c).giic_QualifyingPoints__c)
                {
                    AccountWrapMap.get(TempWrapAccountId).AccountObj.giic_TypeOfMembership__c = PointToUpUpgrade.get(AccountWrapMap.get(TempWrapAccountId).AccountObj.giic_TypeOfMembership__c).id;
                    LstOfAccounToUpdate.add(AccountWrapMap.get(TempWrapAccountId).AccountObj); 
                }
            }
        }
        if(!(LstOfAccounToUpdate.isempty()))
            update LstOfAccounToUpdate;
            
    FinishRecalculateMembership(LstOfAccounToUpdate);
    
    }
    
    public void FinishRecalculateMembership(list<Account> FinishedAccountLst)
    {
        if(!(FinishedAccountLst.isempty()))
        {
            for(Account TempAccountObj : FinishedAccountLst)
            {
                if(AccountWrapMap.containskey(TempAccountObj.id))
                {
                    AccountWrapMap.get(TempAccountObj.id).RowColor = 'lightblue';
                }
            }
        }
        for(Id DataMapKey : AccountWrapMap.keyset())
        {
            if(AccountWrapMap.get(DataMapKey).RowColor != 'lightblue')
            {
                AccountWrapMap.get(DataMapKey).RowColor = 'lightcoral';
            }
        }
    }
    
    public class AccountWrap
    {
        public Account AccountObj{get;set;}
        public String RowColor{get;set;}
        public AccountWrap()
        {
            AccountObj = new Account();
            RowColor = null;
        }
    }
}