/************************************************************************************
Version : 1.0
Created Date : 21-Aug-2018
Function : Test class for giic_CommonUtility
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/

@isTest
private class giic_Test_CommonUtility {
    
    @testSetup
    static void setup(){
       
        //Create Admin User 
        giic_Test_DataCreationUtility.CreateAdminUser(); 
        // Create consumer account
        List<Account> testConsumerAccountList = giic_Test_DataCreationUtility.insertConsumerAccount();
        // create warehouse
        List<gii__Warehouse__c> testWareList = giic_Test_DataCreationUtility.insertWarehouse();
        SetupTestData.createCustomSettings();
        // create system policy
        giic_Test_DataCreationUtility.testSystemPolicyCreation();
        // create  Account refrence
        List<gii__AccountAdd__c> testAccRefList = giic_Test_DataCreationUtility.getAccountReference(testConsumerAccountList);
        Database.insert(testAccRefList);
        // create products and product reference
        List<gii__Product2Add__c> testProductList = giic_Test_DataCreationUtility.insertProduct();
        // create Sales Order
        List<gii__SalesOrder__c> testSOList = giic_Test_DataCreationUtility.insertSalesOrderUnreleased(testConsumerAccountList[0].Id, testAccRefList[0].Id);
        // create Sales Order line
        List<gii__SalesOrderLine__c> testSOLList = giic_Test_DataCreationUtility.insertSOLine(testSOList[0].Id, testProductList[0].Id);
    }
    
    public static testMethod void getDistanceOfWareHouses_Test(){
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        System.Runas(u)  
        {

        Test.startTest();
        giic_CommonUtility.getDistanceOfWarehouses([SELECT Id, ShippingLatitude, ShippingLongitude, giic_WarehouseDistanceMapping__c FROM Account],false);
        giic_CommonUtility.getDistanceOfWarehouses([SELECT Id, ShippingLatitude, ShippingLongitude, giic_WarehouseDistanceMapping__c FROM Account ]);
        List<giic_CommonUtility.WarehouseDistance> testWarehouseDist = new List<giic_CommonUtility.WarehouseDistance>();
        Test.stopTest();
    } 
    }
    
    public static testMethod void updateSOSOLStatusFromDC_Test(){
          User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        System.Runas(u)  
        {

        Test.startTest();
        Map<string, string> idStatusMap = new Map<string, string>();
        for(gii__SalesOrder__c so : [Select Id from gii__SalesOrder__c Limit 10]){
            idStatusMap.put(so.id, 'open');
        }
        for(gii__SalesOrderLine__c sol : [Select Id from gii__SalesOrderLine__c Limit 10]){
            idStatusMap.put(sol.id, 'Initial');
        }
        //idStatusMap.put('a3b2C000fg5LhKyQAK', 'open');
         giic_CommonUtility.updateSOSOLStatusFromDC(idStatusMap);
        List<gii__SalesOrderLine__c> soLinetoUpdate = [Select Id, giic_FulfillmentAttempts__c from gii__SalesOrderLine__c where Id IN : idStatusMap.keySet()];
        for(gii__SalesOrderLine__c solObj : soLinetoUpdate){
            solObj.giic_FulfillmentAttempts__c=1;
        }
        Database.update(soLinetoUpdate);
        giic_CommonUtility.updateSOSOLStatusFromDC(idStatusMap);
        Test.stopTest();
    }
    }
     public static testMethod void getSOwithSolines_Test(){
           User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        System.Runas(u)  
        {

        Test.startTest();
        List<gii__SalesOrder__c> lstSO = [select id from gii__SalesOrder__c];
            List<Id> lstSOID = new List<Id>();
            for(gii__SalesOrder__c obj :lstSO)
            {
                lstSOID.add(obj.id);
            }
            giic_CommonUtility.getSOwithSolines(lstSOID,giic_Constants.CREATE_T);
        Test.stopTest();
            
    }
     }
    
    public static testMethod void getDistancefromWHUsingLattiLongitude_Test(){
          User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        System.Runas(u)  
        {
        Test.startTest();
            map<string,object> mapRequest = new map<string,object>();
            List<gii__Warehouse__c> lstWH = new List<gii__Warehouse__c>();
            lstWH = [select id,gii__Geolocation__Latitude__s,gii__Geolocation__Longitude__s from gii__Warehouse__c];
            mapRequest.put('WAREHOUSES',lstWH);
            giic_CommonUtility.getDistancefromWHUsingLattiLongitude(mapRequest);
            set<id> warehouseIds = new set<id>();
            warehouseIds.add(lstWH[0].id);
            giic_CommonUtility.getLocations(warehouseIds);
        Test.stopTest();
            
    }
    }
    
    public static testMethod void getgetDistanceOfWarehousesFromZipCode_Test(){
          User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        System.Runas(u)  
        {

        Test.startTest();
            map<string,object> mapRequest = new map<string,object>();
            giic_CommonUtility.getDistanceOfWarehousesFromZipCode(mapRequest);
        Test.stopTest();
            
    }
    }
    
    public static testMethod void getSOwithSolinesShipping_Test(){
          User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        System.Runas(u)  
        {

        Test.startTest();
            List<gii__SalesOrder__c> lstSO = [select id from gii__SalesOrder__c];
            List<String> lstSOLName = new List<String>();
            for(gii__SalesOrderLine__c obj : [Select Id,name from gii__SalesOrderLine__c])
            {
                lstSOLName.add(obj.Name);
            }
            giic_CommonUtility.getSOwithSolinesShipping(lstSO[0].id,lstSOLName,giic_Constants.CREATE_T);
        Test.stopTest();
    }
    }
    
    public static testMethod void testmultiplemethods()
    {
          User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        System.Runas(u)  
        {

        Test.startTest();
            List<gii__SalesOrder__c> lstSO = [select id from gii__SalesOrder__c];
            set<id> soids = new set<id>();
            soids.add(lstSO[0].id);
            giic_CommonUtility.getSystemPolicyData();
            giic_CommonUtility.getGloviaSystemSetting();
            List<gii__PaymentMethod__c> lstPM = new List<gii__PaymentMethod__c>();
            gii__PaymentMethod__c objPM1 = new gii__PaymentMethod__c();
            objPM1.Name = 'Credit Card';
            objPM1.giic_CreditSequence__c = 1;
            objPM1.giic_PaymentConsumptionSeq__c = 2;
            lstPM.add(objPM1);
            gii__PaymentMethod__c objPM2 = new gii__PaymentMethod__c();
            objPM2.Name = 'Reward Points';
            objPM2.giic_CreditSequence__c = 5;
            objPM2.giic_PaymentConsumptionSeq__c = 1;
            lstPM.add(objPM2);
            insert lstPM;
            giic_CommonUtility.getPaymentMethods();
            giic_CommonUtility.getSalesOrderQuery(lstSO[0].id);
            giic_CommonUtility.getSODetails(soids);
            map<string,object> mapRequest = new map<string,object>();
            giic_CommonUtility.getUserEligibility(mapRequest);
            decimal qty= 1; decimal roundValue = 2;
            giic_CommonUtility.getGloviaRoundingvalue(qty,roundValue);
            set<id> prodRefs = new set<id>();
            giic_CommonUtility.getProdConversionFactors(prodRefs);
            list<String> listSKU = new list<String>();
            giic_CommonUtility.getProductDetails(listSKU);
            
            list<String> transferOrderIds = new list<String>();
            giic_CommonUtility.getRecieptQueues(transferOrderIds);
            list<id> soids1 = new list<id>();
            set<String> ordernumbers = new set<String>();
            giic_CommonUtility.getSalesOrderPayments(soids1,ordernumbers);
            
        Test.stopTest();
    }
    }
    
    
    public static testMethod void test_createandSendHTTPPostRequest()
    {
          User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        System.Runas(u)  
        {

         Test.startTest();
             Test.setMock(HttpCalloutMock.class, new giic_Test_CalloutMockImpl());  
             giic_CommonUtility.createandSendHTTPPostRequest('','');  
         Test.stopTest();
    }
    }
    public static testMethod void test_scheduleBatchJobs_Scenario1()
    {
          User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        System.Runas(u)  
        {

         Test.startTest();
            try{
                giic_CommonUtility.scheduleBatchJobs(15, new giic_AllocationBatch(),System.Label.giic_AllocationBatchName, true);  
            }
            catch(Exception ex)
            {
            }
         Test.stopTest();
    }
    }
    
    public static testMethod void test_scheduleBatchJobs_Scenario2()
    {
          User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        System.Runas(u)  
        {

         Test.startTest();
             giic_CommonUtility.scheduleBatchJobs(60, new giic_AllocationBatch(),System.Label.giic_AllocationBatchName, true);  
         Test.stopTest();
    }
    }
    
}