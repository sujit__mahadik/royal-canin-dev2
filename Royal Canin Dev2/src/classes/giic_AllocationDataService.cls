/************************************************************************************
Version : 1.0
Created Date : 08 Jan 2019
Function : all the SOQL/SOSL method related to allocation
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/
public class giic_AllocationDataService {
       /*
    * Method name : getPIQDInfo
    * Description : get Product Inventory related to Warehouse and Product
    * Return Type : INQTYDTL = > Product ,Warehouse wise Quantity
    * Parameter : Warehouse Ids, Product Ids
    */
    public static List<gii__ProductInventory__c> getPIQDInfo(set<Id> setSOLWarehouseIds,set<Id> setProductRefIds){
        return [Select Id,gii__Product__c,gii__Warehouse__c,gii__AvailableQuantity__c FROM gii__ProductInventory__c
                                                                           Where gii__Product__c in:setProductRefIds AND gii__Warehouse__c in:setSOLWarehouseIds LIMIT 49999];
    }
     /*
    * Method name : getRouteInfo
    * Description : get Route table info for customer and warehouse
    * Return Type : list<giic_Route__c>
    * Parameter : Warehouse Ids, Account Ids
    */
    public static list<giic_Route__c> getRouteInfo(set<Id> setWarehouseIds,set<Id> setAccountIds){
        list<giic_Route__c> lstRoute=new list<giic_Route__c>([select id,giic_PickDayOfWeek__c,giic_CustomerName__c,giic_WarehouseLocation__c from giic_Route__c 
                                      where giic_CustomerName__c in:setAccountIds and giic_WarehouseLocation__c in :setWarehouseIds]);
        
        return lstRoute;
    }
    /*
    * Method name : getExceptionRouteInfo
    * Description : get Exception Route table info for customer and warehouse
    * Return Type : list<giic_ExceptionRoute__c>
    * Parameter : Warehouse Ids, Account Ids
    */
    public static list<giic_ExceptionRoute__c> getExceptionRouteInfo(set<Id> setWarehouseIds,set<Id> setAccountIds){
        list<giic_ExceptionRoute__c> lstExceptionRoute=new list<giic_ExceptionRoute__c>([select id,giic_NewPickDate__c,giic_OriginalShipDate__c,giic_WarehouseLocation__c,giic_CustomerName__c from giic_ExceptionRoute__c 
                                                         where giic_CustomerName__c in:setAccountIds and giic_WarehouseLocation__c in :setWarehouseIds]);
        
        return lstExceptionRoute;
    }
     /*
    * Method name : getStockSalesOrderLines
    * Description : return all sales order line which is open or in back order
    * Return Type : list<gii__SalesOrderLine__c>
    * Parameter : set<id> setSOIds,
    */
    public static list<gii__SalesOrderLine__c> getStockProductSalesOrderLines(set<Id> setSOIds){
        return [select id,gii__Warehouse__c,gii__LineStatus__c, gii__OrderQuantity__c,gii__ConversionFactor__c,gii__BackOrderQuantity__c,gii__Product__c,gii__Product__r.gii__NonStock__c,gii__salesorder__c,gii__salesorder__r.gii__Warehouse__c,gii__salesorder__r.gii__Account__c,gii__salesorder__r.giic_CustomerPriority__c,gii__ReservedQuantity__c from gii__SalesOrderLine__c where (((giic_LineStatus__c =: giic_Constants.SOL_INITIALSTATUS or giic_LineStatus__c=: giic_Constants.SOL_BACKORDERSTATUS) and gii__ReservedQuantity__c =0) or giic_LineStatus__c=: giic_Constants.LINESTATUS_REJECTEDBYDC )  and gii__LineStatus__c=: giic_Constants.SOL_OPENSTATUS and gii__Product__r.gii__NonStock__c =false and gii__SalesOrder__c in :setSOIds];
    }
   /*
    * Method name : getNonStockSalesOrderLines
    * Description : return all sales order line which is open or in back order
    * Return Type : list<gii__SalesOrderLine__c>
    * Parameter : set<id> setSOLIds,
    */
    public static list<gii__SalesOrderLine__c> getNonStockSalesOrderLines(set<Id> setSOIds){
        return [select id,gii__SalesOrder__c, gii__Warehouse__c,gii__Product__c,Name,gii__Product__r.gii__DefaultWarehouse__c,gii__OrderQuantity__c  from gii__SalesOrderLine__c where (((giic_LineStatus__c =:giic_Constants.SOL_INITIALSTATUS or giic_LineStatus__c=:giic_Constants.SOL_BACKORDERSTATUS) and gii__NonStockQuantity__c =0) or giic_LineStatus__c=:giic_Constants.LINESTATUS_REJECTEDBYDC )  and gii__LineStatus__c=:giic_Constants.SOL_OPENSTATUS and gii__Product__r.gii__NonStock__c =true and gii__SalesOrder__c in: setSOIds];
    }
    /*
    * Method name : getInventoryReserveForSOL
    * Description : return all sales order line which is open or in back order
    * Return Type : list<gii__SalesOrderLine__c>
    * Parameter : set<id> setSOLIds,
    */
    public static list<gii__InventoryReserve__c> getInventoryReserveForSOL(set<Id> setSOLIds){
        return [select id,gii__SalesOrderLine__c from gii__InventoryReserve__c where gii__SalesOrderLine__c in:setSOLIds];
    }
    /*
    * Method name : getReservedSalesOrder
    * Description : return all sales order which is reserved in allocation
    * Return Type : list<gii__SalesOrder__c>
    * Parameter : set<id> setSOIds,
    */
    public static list<gii__SalesOrder__c> getReservedSalesOrder(set<Id> setReservedSOIds,set<Id> setSOLIds){
        return [select id,giic_CustEmail__c,giic_TaxExempt__c,giic_SOStatus__c,giic_CustomerPriority__c,giic_OrderNo__c,gii__Carrier__c,(select id,gii__Warehouse__c,gii__Product__c,giic_LineNo__c from gii__SalesOrder__r where id in:setSOLIds order by gii__Warehouse__c) from gii__SalesOrder__c where id in:setReservedSOIds];
    }
}