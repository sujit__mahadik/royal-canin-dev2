public class TerritoryTriggerHelper {
	
	public void DeleteTerritoryAssociation(Map<Id, Territory__c> DeletedTerritories)
	{
		Set<Id> DeletedTerritoryIds = DeletedTerritories.keySet();
		List<TerritoryAssignment__c> TAs_ToBeDeleted = [Select id from TerritoryAssignment__c where TerritoryId__c in : DeletedTerritoryIds];
		delete TAs_ToBeDeleted;
	}
	
	public void updateHiddenOwner(List<Territory__c> newTerritory)
	{
		for(Territory__c terr : newTerritory)
		{
			terr.OwnerHidden__c = terr.OwnerId;
		}
	}

	public void updateSyncNeeded(List<Territory__c> newList, Map<Id,Territory__c> oldRecs){
		for(Territory__c terr : newList){
			if(oldRecs.get(terr.id).OwnerId != terr.OwnerId){
				terr.Sync_Needed__c = TRUE;
			}
		}
	}
	
	public void setTerritoryOwner(List<Territory__c> newTerritoriesMap)
	{
		/*String dataConversionUser = Utilities.getDataConversionUser();
		if(ResaleGlobalVariables__c.getinstance('DataConversion').value__c == 'TRUE')
		{
			List<Integer> agentIds = new List<Integer>();
			for(Territory__c territory : newTerritoriesMap)
			{
				if(territory.AgentId__c	!= null && territory.AgentId__c != '')
				{
					agentIds.add(Integer.valueOf(territory.AgentId__c));
				}
				else
				{
					territory.addError('No agent id for territory record. Cannot assign onwership. Insert failed.');
				}
			}
			List<User> users = [select id, name, ASAP_Agent_Id__c from User where ASAP_Agent_Id__c in : agentIds];
			Map<Integer, User> usersMap = new Map<Integer, User>();
			for(User usr : users)
			{
				usersMap.put(Integer.valueOf(usr.ASAP_Agent_Id__c), usr);
			} 
			for(Territory__c territory : newTerritoriesMap)
			{
				if(usersMap.get(Integer.valueOf(territory.AgentId__c)) != null)
				{
					territory.OwnerId = usersMap.get(Integer.valueOf(territory.AgentId__c)).Id;
					territory.OwnerHidden__c = usersMap.get(Integer.valueOf(territory.AgentId__c)).Id;
				}
				else
				{
					//territory.OwnerId = dataConversionUser;
					//territory.OwnerHidden__c = dataConversionUser;
					territory.addError('No user found with the agent id specified. Insert failed.');
				}
			}
		}*/
	}
	
	public void UpdateTerritoryOwner(Map<Id, Territory__c> newRecs, Map<Id, Territory__c> oldRecs)
	{
		Map<id, List<TerritoryAssignment__c>> terrAssignMap = new Map<id, List<TerritoryAssignment__c>>();
		List<TerritoryAssignment__c> TAS = [Select id, TerritoryId__c, OwnerId from TerritoryAssignment__c where TerritoryId__c in : newRecs.keySet()];
		List<TerritoryAssignment__c> updatedTAs = new List<TerritoryAssignment__c>();
		for(TerritoryAssignment__c TA : TAS)
		{
			List<TerritoryAssignment__c> terrAssigns;
			if(terrAssignMap.get(TA.TerritoryId__c) == null)
			{
				terrAssigns = new List<TerritoryAssignment__c>();
				terrAssigns.add(TA);
				terrAssignMap.put(TA.TerritoryId__c, terrAssigns);
			}
			else
			{
				terrAssigns = terrAssignMap.get(TA.TerritoryId__c);
				terrAssigns.add(TA);
				terrAssignMap.put(TA.TerritoryId__c, terrAssigns);
			}
		}
		for(id terrId : newRecs.keySet())
		{
			if(newRecs.get(terrId).OwnerId != oldRecs.get(terrId).OwnerId)
			{	
				if(terrAssignMap.get(terrId) != null)
				{
					for(TerritoryAssignment__c ta : terrAssignMap.get(terrId))
					{
						ta.OwnerId = newRecs.get(terrId).OwnerId;
						updatedTAs.add(ta);
					}
				}
			}
		}
		update updatedTAs;
	}

}