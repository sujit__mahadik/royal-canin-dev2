/************************************************************************************
Version : 1.0
Name : giic_SalesOrderStagingDetailController
Created Date : 16 Aug 2018
Function : Convert the Sales Order Staging Record to Sales Order and Redirect to Sales Order
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/
public class giic_SalesOrderStagingDetailController {
    
    public gii__SalesOrderStaging__c objSOS{get; set;}
    public String sObjectApiName{get; set;}
    public Boolean isError{get; set;}
    
    public giic_SalesOrderStagingDetailController(ApexPages.StandardController controller) {
        objSOS = (gii__SalesOrderStaging__c) controller.getRecord();
        sObjectApiName = 'gii__SalesOrderStaging__c';
        isError = false;
    }
    
    public PageReference onLoad(){
        PageReference pObj ;
        List<Integration_Error_Log__c> lstErrorLog = new List<Integration_Error_Log__c>();
        set<Id> setSOSId = new set<Id>();
        try{
            giic_RecursiveTriggerHandler.isFirstTime = true; // dont run trigger on update call
            //prepare setting accourding to custom metadata
            Map<String, giic_IntegrationCustomMetaDataWrapper> mapIntegrationSettings = giic_IntegrationCustomMetaDataUtil.getIntegrationSettings(sObjectApiName);
            //System.debug('push button +++mapIntegrationSettings+++'+mapIntegrationSettings);
            //Get Paraent Record
            giic_IntegrationCustomMetaDataWrapper cmdObj = mapIntegrationSettings.get(sObjectApiName); 
            String soql = 'Select ' + cmdObj.strSoql.subString(0, cmdObj.strSoql.length()-1) + ' from ' + cmdObj.sourceSobjectApi +  ' where giic_Status__c != \''+ System.Label.giic_Converted + '\' and id = \''+objSOS.id+'\''; 
            
            // System.debug(':soql--->>>>>> ' + soql); 
            
            List<SObject> lstSourceObject = Database.query(soql);
            
            if(lstSourceObject.isEmpty()){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, System.Label.giic_DetailPageSuccessMsg));
                isError = true;
                return null;
            }
            
            //DeActivate Errors
            giic_IntegrationCustomMetaDataUtil.deActivateCurrentError(new Set<Id>{objSOS.id});
            
            // address validation removed
            /*//Validate Address
            Boolean isValidAddress = Boolean.valueOf(lstSourceObject[0].get('giic_AddressValidated__c'));
            if(!isValidAddress){
            Map<Id , giic_ValShippingAddrFedExController.AddressValidation> mapResult = giic_IntegrationCommonUtil.validateAddress((List<gii__SalesOrderStaging__c>) lstSourceObject);
            isValidAddress =  (mapResult.containsKey(String.valueOf(lstSourceObject[0].get('Id'))) && mapResult.get(String.valueOf(lstSourceObject[0].get('Id'))).isValidAddress == true) ? true : false;
            }
            if(!isValidAddress){ // if invalid address throw error
            objSOS.giic_Status__c = System.Label.giic_Error;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, System.Label.giic_DetailPageErrorMsg));
            isError = true; return null;
            }*/
            
            //Convert selected sales order staging record to sales orders
            Set<String> setErrorIds = new Set<String>();
            Map<String, giic_IntegrationCMDResultWrapper> mpNewSObjectId = giic_IntegrationCustomMetaDataUtil.doConversion(sObjectApiName, mapIntegrationSettings, lstSourceObject, setErrorIds);

             system.debug('lstSOStaging-->>>> ' + objSOS.SalesOrderLineStagings__r);
            
            if(setErrorIds.isEmpty()){
                boolean bNoChildRecord =  false;
                List<gii__SalesOrderLineStaging__c> lstSOS = [select id from gii__SalesOrderLineStaging__c where giic_SalesOrderStaging__c =: objSOS.Id];
                if(lstSOS == null || lstSOS.size() == 0 ){
                    bNoChildRecord = true;
                }
                gii__SalesOrder__c so = new gii__SalesOrder__c(Id=mpNewSObjectId.get(objSOS.Id).targetObj.Id ); 
                
                if(bNoChildRecord){
                    objSOS.giic_Status__c = System.Label.giic_Error;
                    so.giic_SOStatus__c=System.Label.giic_Draft;
                    so.giic_ProcessingStatus__c=System.Label.giic_ProcessedwithError;
                    
                    Integration_Error_Log__c objEL = new Integration_Error_Log__c();
                    objEL.giic_SalesOrder__c = so.Id;
                    objEL.giic_SalesOrderStaging__c = objSOS.Id;
                    setSOSId.add(objSOS.Id);
                    objEL.Error_Code__c = giic_Constants.NO_LINE_ERROR;
                    objEL.Name = giic_Constants.NO_LINE_ERROR;
                    objEL.giic_IsActive__c = true;
                    objEL.Error_Message__c = giic_Constants.NO_LINE_AVAILABLE;
                    lstErrorLog.add(objEL);
                        
                }else{
                    so.giic_ProcessingStatus__c=System.Label.giic_Processed;
                    objSOS.giic_Status__c = System.Label.giic_Converted;
                }
                
                system.debug('so-->>>> ' + so);
                
                update so;
                update objSOS;
                
                giic_IntegrationCustomMetaDataUtil.deActivateCurrentError(setSOSId);
                if(lstErrorLog != null && lstErrorLog.size() > 0 ){
                    insert lstErrorLog;
                }
                
                /************************************/
                
                set<string> SOIds = new set<string>();
                list<gii__SalesOrder__c> lstSalesOrder = new list<gii__SalesOrder__c>();
                set<string> setAccountId = new set<string>();
                
                if(mpNewSObjectId.values() != null && mpNewSObjectId.values().size() > 0){
                    for(giic_IntegrationCMDResultWrapper oICMDRW : mpNewSObjectId.values()){
                        // 
                        //system.debug('oICMDRW.targetObj.getSObjectType()---->>>>>1111 ' + oICMDRW.targetObj.getSObjectType());
                        if(oICMDRW.targetObj != null && oICMDRW.targetObj.getSObjectType() == Schema.gii__SalesOrder__c.getSObjectType() ){
                            //system.debug('oICMDRW.targetObj.getSObjectType()---->>>>>2222' + oICMDRW.targetObj.getSObjectType()+ ' <<<<--->>>> ' + Schema.gii__SalesOrder__c.getSObjectType());
                            if(string.isNotEmpty(string.valueOf( oICMDRW.targetObj.get('Id')))){
                                SOIds.add(string.valueOf( oICMDRW.targetObj.get('Id')));
                                if(oICMDRW.targetObj.get('gii__AccountReference__c') != null && oICMDRW.targetObj.get('gii__Account__c') != null) lstSalesOrder.add((gii__SalesOrder__c) oICMDRW.targetObj);
                                if(oICMDRW.targetObj.get('gii__Account__c') != null) setAccountId.add(string.valueOf(oICMDRW.targetObj.get('gii__Account__c')));
                            }
                        }
                    }
                }
                
                system.debug('SOIds------>>>>>> ' + SOIds);
                system.debug('lstSalesOrder------>>>>>> ' + lstSalesOrder);
                system.debug('setAccountId------>>>>>> ' + setAccountId);
                //insert update promotions on sales order
                try{
                    map<string,object> mapPromoResponce = giic_PromotionHelper.getAccProgramForSOStoSO(lstSalesOrder, setAccountId);
                    list<gii__AccountProgram__c> lstAccountProgramToInsert = (list<gii__AccountProgram__c>) mapPromoResponce.get('ACCPROINSERT');
                    list<gii__AccountProgram__c> lstAccountProgramToUpdate = (list<gii__AccountProgram__c>) mapPromoResponce.get('ACCPROUPDATE');
                    list<giic_PromotionWrapper.PromoResponseWrapper> lstPromoResponseWrapper = (list<giic_PromotionWrapper.PromoResponseWrapper>) mapPromoResponce.get('ERRORINFO');
                    
                    if(lstAccountProgramToInsert != null && !lstAccountProgramToInsert.isEmpty()) insert lstAccountProgramToInsert;
                    if(lstAccountProgramToUpdate != null && !lstAccountProgramToUpdate.isEmpty()) update lstAccountProgramToUpdate;
                    if(lstPromoResponseWrapper != null && !lstPromoResponseWrapper.isEmpty()){
                        List<Integration_Error_Log__c> lstUOMErrors = new List<Integration_Error_Log__c>();
                        Set<String> setErrorPromoIds = new Set<String>();
                        String userStory = giic_Constants.USER_STORY_PROMOTIONCALCULATION; //'Promotion Calculation';
                        Map<String, String> fieldApiForErrorLogMaster = new Map<String, String>{'giic_SalesOrder__c'=>'Id'};
                            for(giic_PromotionWrapper.PromoResponseWrapper objWrapper : lstPromoResponseWrapper){
                                giic_IntegrationCustomMetaDataUtil.collectErrors(lstUOMErrors, setErrorPromoIds, userStory, fieldApiForErrorLogMaster, new gii__SalesOrder__c(Id = objWrapper.orderId), 'Promotion Calculation Error', objWrapper.lstErrorInformation[0].errorMessage); 
                                
                            }
                        
                        if(!lstUOMErrors.isEmpty()) insert lstUOMErrors;
                    }
                    
                }catch(exception e){ System.debug('Exp:' + e.getLineNumber() + e.getMessage());}
                
                
                //Check for exception state of the converted orders
                giic_IntegrationCommonUtil.checkForExceptionState(SOIds);
                
                set<string> ZIPCODES = new set<string>();
                set<string> WHGIDS = new set<string>();
                set<string> ZIPWHKEY = new set<string>();
                map<string,object> mapRequest = new map<string,object>();
                map<string, List<gii__SalesOrder__c>> mapZipCodeWHGroupIdVsLstSO = new map<string, List<gii__SalesOrder__c>>();
                List<gii__SalesOrder__c> lstSOToUpdate = new List<gii__SalesOrder__c>();
                List<gii__SalesOrder__c> lstSO =  [select id, name, gii__DropShip__c, giic_DropShipWHMapping__c, gii__Warehouse__c, gii__ShipToZipPostalCode__c, gii__AccountReference__r.giic_WarehouseGroup__c from gii__SalesOrder__c where gii__ShipToZipPostalCode__c != null AND gii__AccountReference__c != null AND gii__AccountReference__r.giic_WarehouseGroup__c != null AND id in : SOIds AND ((giic_Source__c = 'INSITE' AND gii__DropShip__c = true) OR giic_Source__c = 'OLP')];
                // condition giic_Source__c = 'INSITE' AND gii__DropShip__c = true means it is insiteOrder and dropship is true so need to calculate "dropship warehouse distance"
                // condition giic_Source__c = 'OLP' means it is OLP Order and  "dropship warehouse distance" need to be calculated for all OLP Order
                // system.debug('lstSO Size = ' + lstSO.size() + ' --->>>> ' + lstSO);
                for(gii__SalesOrder__c objSO :  lstSO ){
                    ZIPCODES.add(objSO.gii__ShipToZipPostalCode__c);
                    WHGIDS.add(objSO.gii__AccountReference__r.giic_WarehouseGroup__c);
                    ZIPWHKEY.add(objSO.gii__ShipToZipPostalCode__c+'~'+objSO.gii__AccountReference__r.giic_WarehouseGroup__c);
                    if(!mapZipCodeWHGroupIdVsLstSO.containsKey(objSO.gii__ShipToZipPostalCode__c+'~'+objSO.gii__AccountReference__r.giic_WarehouseGroup__c)){
                        mapZipCodeWHGroupIdVsLstSO.put(objSO.gii__ShipToZipPostalCode__c+'~'+objSO.gii__AccountReference__r.giic_WarehouseGroup__c, new List<gii__SalesOrder__c>{});
                    }
                    mapZipCodeWHGroupIdVsLstSO.get(objSO.gii__ShipToZipPostalCode__c+'~'+objSO.gii__AccountReference__r.giic_WarehouseGroup__c).add(objSO);
                }
                mapRequest.put('ZIPCODES', ZIPCODES);
                mapRequest.put('WHGIDS', WHGIDS);
                mapRequest.put('ZIPWHKEY', ZIPWHKEY);
                
                
                map<string,Object> mapZipCodesResponse = giic_CommonUtility.getDistanceOfWarehousesFromZipCode(mapRequest);
                map<string,string> mapZipCode = new map<string,string>();
                if(mapZipCodesResponse.containsKey('ZIPCODESMAP')){
                    // mapZipCode contains dropship warehouse distance based on ZipCode+WarehouseGroupId
                    mapZipCode=(map<string,string>)mapZipCodesResponse.get('ZIPCODESMAP');
                }
                if(mapZipCodeWHGroupIdVsLstSO !=null && mapZipCodeWHGroupIdVsLstSO.keySet()!=null && mapZipCodeWHGroupIdVsLstSO.keySet().size()>0 && mapZipCode !=null && mapZipCode.size()>0){
                    for(string sZipCodeWHGId: mapZipCodeWHGroupIdVsLstSO.keySet() ){
                        if(mapZipCode.containsKey(sZipCodeWHGId)){
                            for(gii__SalesOrder__c objSO : mapZipCodeWHGroupIdVsLstSO.get(sZipCodeWHGId)){
                                objSO.giic_DropShipWHMapping__c = mapZipCode.get(sZipCodeWHGId);
                                lstSOToUpdate.add(objSO);
                            }
                        }
                    }
                }
                if(lstSOToUpdate != null && lstSOToUpdate.size() > 0){ update lstSOToUpdate; }
                
                /*************************************/
                pObj = new PageReference('/' + mpNewSObjectId.get(objSOS.Id).targetObj.Id); // redirect to created sales order record
                pObj.setRedirect(true);
            }else{
                giic_CommonUtility.updateSOSErrorOnSalesOrder(new Set<String>{objSOS.id}); // Update error logs from sales order staging to sales orders
                if(setErrorIds.contains(''+objSOS.id)) objSOS.giic_Status__c = System.Label.giic_Error;
                else if(mpNewSObjectId.get(objSOS.Id).totalChilds != 0) {objSOS.giic_Status__c = System.Label.giic_InProgress; gii__SalesOrder__c so = new 
                    gii__SalesOrder__c(Id=mpNewSObjectId.get(objSOS.Id).targetObj.Id, giic_ProcessingStatus__c=System.Label.giic_ProcessedwithError,giic_SOStatus__c=System.Label.giic_Draft); update so;
                                                                        }
                update objSOS;
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, System.Label.giic_DetailPageErrorMsg));
                isError = true;
            }
            
        }catch(Exception ex){
            objSOS.giic_Status__c = System.Label.giic_Error;
            update objSOS;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getMessage()));
            return null;
        }
        return pObj;
    }
    
    public PageReference goBack(){
        PageReference pObj ;
        try{          
            pObj = new PageReference('/' + objSOS.id); // redirect to sales order Staging record
            pObj.setRedirect(true);            
        }catch(Exception ex){ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getMessage())); return null; }
        return pObj;
    }
}