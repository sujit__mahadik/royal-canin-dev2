global class OneTimeConsumerAccountCleanupBatch implements Database.Batchable<sObject> {
	/*
	**
	**	FOR EACH Consumer Account
	**		Does Account have a Related Account/Partner Record of Role 'Employee'	
	**		IF There is NO Related Account THEN
	**			Create Related Account/Partner Recod of Role 'Employee'
	**		ENDIF
	**		UPDATE Account
	**			SET ParentId = NULL 
	**	ENDFOR
	**
	*/
	
	/*
	Execute the appropriate commands below in 'Execute Anonymous' window
1. Test run to check debug logs. Does not do any updates to the system
OneTimeConsumerAccountCleanupBatch otcacb = new OneTimeConsumerAccountCleanupBatch();
Database.executeBatch(otcacb);

2. Run job to insert Related Account/Partner records only
OneTimeConsumerAccountCleanupBatch otcacb = new OneTimeConsumerAccountCleanupBatch(false, true);
Database.executeBatch(otcacb);

3. Run job to clear/null the Account.ParentId field only
OneTimeConsumerAccountCleanupBatch otcacb = new OneTimeConsumerAccountCleanupBatch(false, true);
Database.executeBatch(otcacb);
	*/

	global String query;
	global Boolean updateAccounts = false;
	global Boolean insertPartners = false;

	global OneTimeConsumerAccountCleanupBatch() {
		query =	'SELECT Id, ParentId FROM Account ';
		query += ' WHERE RecordType.DeveloperName =\'Consumer\'';
		query += ' AND ParentId != NULL';
	}

	global OneTimeConsumerAccountCleanupBatch(Boolean updateAccountsFlag, Boolean insertPartnersFlag) {
		query =	'SELECT Id, ParentId FROM Account ';
		query += ' WHERE RecordType.DeveloperName =\'Consumer\'';
		query += ' AND ParentId != NULL';
		
		updateAccounts = updateAccountsFlag;
		insertPartners = insertPartnersFlag;
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC){
		return Database.getQueryLocator(query);
	}
	
	global void execute(Database.BatchableContext BC, List<sObject> scope){
		List<Account> updatedAccounts = new List<Account>();
		List<Partner__c> insertedPartners = new List<Partner__c>();
		Map<Id, Partner__c> partnerMap = new Map<Id, Partner__c>();
		Set <Id> accountIds = new Set <Id>();
		Id parentId;
		
		System.debug(loggingLevel.DEBUG, '********** GJK: Batch Size: '+scope.size());
		
		for(sObject s : scope) {
			Account a = (Account)s;
			accountIds.add(a.id);
		}

		for (Partner__c p: [Select Id, AccountFromId__c, AccountToId__c, Role__c
								FROM Partner__c
								WHERE AccountFromId__c in :accountIds
								AND Role__c = 'Employee']) {
			partnerMap.put(p.AccountFromId__c, p);	
		}
		System.debug(loggingLevel.DEBUG, '********** GJK: partnerMap Size: '+partnerMap.size());

		for(sObject s : scope) {
			Account a = (Account)s;
			parentId = a.ParentId;

			if (partnerMap.get(a.Id) == null) {
				insertedPartners.add(new Partner__c(AccountFromId__c = a.id
													, AccountToId__c = a.parentId
													, Role__c = 'Employee'));
			}

			updatedAccounts.add(new Account(id=a.id, ParentId=NULL ));
		}
		
		System.debug(loggingLevel.DEBUG, '********** GJK: updatedAccounts Size: '+updatedAccounts.size());
		if (updateAccounts && updatedAccounts.size() > 0) {
			System.debug(loggingLevel.DEBUG, '********** GJK: Updating Accounts');
			update updatedAccounts;
		}

		System.debug(loggingLevel.DEBUG, '********** GJK: InsertedPartners Size: '+insertedPartners.size());
		if (insertPartners && insertedPartners.size() > 0) {
			System.debug(loggingLevel.DEBUG, '********** GJK: Inserting Partners');
			insert insertedPartners;
		}
	}
	
	global void finish(Database.BatchableContext BC) {
	}

}