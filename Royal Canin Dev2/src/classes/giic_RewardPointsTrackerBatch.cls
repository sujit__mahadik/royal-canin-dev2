/************************************************************************************
Version : 1.0
Name : giic_RewardPointsTrackerBatch
Created Date : 19 Nov 2018
Function : Convert the Loyalty point for the account to reward points
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/
global class giic_RewardPointsTrackerBatch implements Database.Batchable<sObject>{
    /*
    * Method name : start
    * Description : Query all loyalty point record for the account
    * Return Type : Database.QueryLocator
    * Parameter : Database.BatchableContext BC
    */
    global Database.QueryLocator start(Database.BatchableContext BC){
        string query;
        //string query='select id,name, giic_Account__c,giic_Account__r.name,giic_Account__r.giic_TypeOfMembership__c,giic_Account__r.giic_TypeOfMembership__r.name,giic_CurrentPoints__c,giic_LifeTimePoints__c,(select id,name,giic_MembersLoyaltyPts__c,giic_SalesOrderPayment__c,giic_PointRedeemed__c,giic_PointsEarned__c from LoyaltyPointTracker__r where giic_PointRedeemed__c = false ORDER BY giic_PointsEarned__c DESC) from giic_MembersLoyaltyPoints__c 
        query = System.label.giic_QueryRewardsBatch;
        query +=' where createddate = '+System.label.giic_FilterRewardsBatch+' and giic_CurrentPoints__c > 0 ';
        return Database.getQueryLocator(query);
    } 
    /*
    * Method name : execute
    * Description : Process the Loyalty Point and convert into the Reward point
    * Return Type : nill
    * Parameter : Database.BatchableContext BC, List<sObject> scope
    */
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        set<id> setTypeOfMembershipId=new set<Id>();
        for(giic_MembersLoyaltyPoints__c objMemberLoyaltyPoints :  (list<giic_MembersLoyaltyPoints__c>)scope){
            if(objMemberLoyaltyPoints.giic_Account__c != null && objMemberLoyaltyPoints.giic_Account__r.giic_TypeOfMembership__c != null)
                setTypeOfMembershipId.add(objMemberLoyaltyPoints.giic_Account__r.giic_TypeOfMembership__c);
        }
        giic_LoyaltyManagementUtility.executeRewardPointsGeneration((list<giic_MembersLoyaltyPoints__c>)scope,setTypeOfMembershipId);
    }
    /*
    * Method name : finish
    * Description : Process any pending work after the batch complete
    * Return Type : nill
    * Parameter : Database.BatchableContext BC,
    */
    global void finish(Database.BatchableContext BC){
       
    }
    
}