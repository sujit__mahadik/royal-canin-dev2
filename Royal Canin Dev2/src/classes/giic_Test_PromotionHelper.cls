/************************************************************************************
Version : 1.0
Created Date : 14 Sep 2018
Function : Test class for giic_PromotionHelper
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/
@isTest
private class giic_Test_PromotionHelper {
    public static List<gii__AccountAdd__c>  lstAccountRef;
    public static List<gii__Promotion__c> lstPromotion;
    @testSetup
    static void setup(){
        //Insert warehouse
        giic_Test_DataCreationUtility.insertWarehouse();
        //Create System policy record
        giic_Test_DataCreationUtility.testSystemPolicyCreation();
        //Insert Global param
        giic_Test_DataCreationUtility.insertGlobalParametersCustomSetting();
        //Insert Consumer Accounts
        giic_Test_DataCreationUtility.insertConsumerAccount();
        //Insert warehouse
        giic_Test_DataCreationUtility.insertWarehouse_N(2);
        //Insert Account Reference
        lstAccountRef = giic_Test_DataCreationUtility.getAccountReference_N(giic_Test_DataCreationUtility.lstAccount);
        //Add products
        giic_Test_DataCreationUtility.insertProduct();
        
        //Add Multiple Sales Order and Sales Order Lines
        giic_Test_DataCreationUtility.insertBulkSalesOrder();
        giic_Test_DataCreationUtility.insertBulkSOLine();
        
        //Create promotions
        lstPromotion=giic_Test_DataCreationUtility.createPromotions();
        giic_Test_DataCreationUtility.createPromotionLines();
        giic_Test_DataCreationUtility.createPromotionLineStyle();
        
        giic_Test_DataCreationUtility.createAdditionalCharge();
        //Create user record
        giic_Test_DataCreationUtility.CreateAdminUser();
        giic_Test_DataCreationUtility.insertSalesOrderStaging();
        giic_Test_DataCreationUtility.insertSalesOrderLineStaging();
        //Create Account Program
        
        giic_Test_DataCreationUtility.createRateTable();
        

        
       
    }
    private static testMethod void test_AutoPromotion() { 
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        System.RunAs(u)        
        {
            list<gii__Promotion__c> lstPromo= [select id from gii__Promotion__c];
            
            list<gii__salesorder__c> lstSalesOrder = [select id,gii__Released__c,gii__AccountReference__c,gii__account__c from gii__salesorder__c];
            lstSalesOrder[0].gii__Released__c=true;            
            update lstSalesOrder; 

            Test.startTest();
            giic_PromotionController.applyAutoPromotion(new map<string,object>{'soId'=>lstSalesOrder[0].id});
            giic_PromotionHelper.getAccProgramForSOStoSO(lstSalesOrder,new set<string>{lstSalesOrder[0].gii__account__c});
  
            Test.stopTest();
        }
    }
    
    private static testMethod void test_AutoPromotion_Scenario1() { 
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        System.RunAs(u)        
        {
            list<gii__Promotion__c> lstPromo= [select id from gii__Promotion__c];
            
            list<gii__salesorder__c> lstSalesOrder = [select id,gii__Released__c,gii__AccountReference__c,gii__account__c from gii__salesorder__c];
            lstSalesOrder[0].gii__Released__c=true;
            if(lstPromo[0].id !=  null)
            {
                lstSalesOrder[0].giic_HPROMO1__c = lstPromo[0].id;
            }
            if(lstPromo[1].id !=  null)
            {
                lstSalesOrder[0].giic_HPROMO2__c = lstPromo[1].id;
            }
            lstSalesOrder[0].gii__DropShip__c = true;
            
            update lstSalesOrder; 
            
            gii__AdditionalCharge__c objAddCharge = new gii__AdditionalCharge__c();
            objAddCharge.Name= 'Shipping Fee';
            objAddCharge.gii__UnitPrice__c = 4.99;
            insert   objAddCharge;              
            gii__SalesOrderAdditionalCharge__c objSOAddCharge = new gii__SalesOrderAdditionalCharge__c();
            objSOAddCharge.gii__AdditionalCharge__c = objAddCharge.Id;
            objSOAddCharge.gii__SalesOrder__c = lstSalesOrder[0].id;
            insert objSOAddCharge;
            Test.startTest();
            giic_PromotionController.applyAutoPromotion(new map<string,object>{'soId'=>lstSalesOrder[0].id});
            giic_PromotionHelper.getAccProgramForSOStoSO(lstSalesOrder,new set<string>{lstSalesOrder[0].gii__account__c});
  
            Test.stopTest();
        }
    }
    
    private static testMethod void test_AutoPromotion_Scenario2() { 
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        System.RunAs(u)        
        {
            list<gii__Promotion__c> lstPromo= [select id from gii__Promotion__c];
            
            list<gii__salesorder__c> lstSalesOrder = [select id,gii__Released__c,gii__AccountReference__c,gii__account__c from gii__salesorder__c];
            lstSalesOrder[0].gii__Released__c=true;
            if(lstPromo[0].id !=  null)
            {
                lstSalesOrder[0].giic_HPROMO1__c = lstPromo[0].id;
            }
            if(lstPromo[1].id !=  null)
            {
                lstSalesOrder[0].giic_HPROMO2__c = lstPromo[1].id;
            }
            if(lstPromo[2].id !=  null)
            {
                lstSalesOrder[0].giic_FREESHIP__c = lstPromo[2].id;
            }
            
            update lstSalesOrder; 
            
            gii__AdditionalCharge__c objAddCharge = new gii__AdditionalCharge__c();
            objAddCharge.Name= 'Shipping Fee';
            objAddCharge.gii__UnitPrice__c = 4.99;
            insert   objAddCharge;              
            gii__SalesOrderAdditionalCharge__c objSOAddCharge = new gii__SalesOrderAdditionalCharge__c();
            objSOAddCharge.gii__AdditionalCharge__c = objAddCharge.Id;
            objSOAddCharge.gii__SalesOrder__c = lstSalesOrder[0].id;
            insert objSOAddCharge;
            Test.startTest();
            giic_PromotionController.applyAutoPromotion(new map<string,object>{'soId'=>lstSalesOrder[0].id});
            giic_PromotionHelper.getAccProgramForSOStoSO(lstSalesOrder,new set<string>{lstSalesOrder[0].gii__account__c});
  
            Test.stopTest();
        }
    }
    
    private static testMethod void test_AutoPromotion_Scenario3() { 
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        System.RunAs(u)        
        {
            list<gii__Promotion__c> lstPromo= [select id from gii__Promotion__c];
            
            list<gii__salesorder__c> lstSalesOrder = [select id,gii__Released__c,gii__AccountReference__c,gii__account__c from gii__salesorder__c];
            lstSalesOrder[0].gii__Released__c=true;
            if(lstPromo[0].id !=  null)
            {
                lstSalesOrder[0].giic_HPROMO1__c = lstPromo[0].id;
            }
            if(lstPromo[1].id !=  null)
            {
                lstSalesOrder[0].giic_HPROMO2__c = lstPromo[1].id;
            }         
            
            update lstSalesOrder; 
            
            gii__AdditionalCharge__c objAddCharge = new gii__AdditionalCharge__c();
            objAddCharge.Name= 'Shipping Fee';
            objAddCharge.gii__UnitPrice__c = 4.99;
            insert   objAddCharge;              
            gii__SalesOrderAdditionalCharge__c objSOAddCharge = new gii__SalesOrderAdditionalCharge__c();
            objSOAddCharge.gii__AdditionalCharge__c = objAddCharge.Id;
            objSOAddCharge.gii__SalesOrder__c = lstSalesOrder[0].id;
            objSOAddCharge.gii__CalculatedShippingAmount__c = objAddCharge.gii__UnitPrice__c;
            insert objSOAddCharge;
            Test.startTest();
            giic_PromotionController.applyAutoPromotion(new map<string,object>{'soId'=>lstSalesOrder[0].id});
            giic_PromotionHelper.getAccProgramForSOStoSO(lstSalesOrder,new set<string>{lstSalesOrder[0].gii__account__c});
  
            Test.stopTest();
        }
    }
    
    
    private static testMethod void test_AccProgram() { 
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        System.RunAs(u)        
        {
            giic_Test_DataCreationUtility.accRefList_N=[select id,gii__Account__c from gii__AccountAdd__c];
            giic_Test_DataCreationUtility.lstPromotion=[select id,giic_IsHeaderPromo__c,giic_PromoFrequency__c,giic_IsCustomerBased__c,giic_IsAutoPromo__c, giic_PromotionCode__c,Name,giic_CustomerType__c,giic_MaxUsedLimit__c,giic_MaxLimit__c,giic_PromotionType__c,(select id,gii__Tier1DiscountPercent__c,gii__DiscountType__c,gii__MinimumAmount__c from gii__Promotion_Details__r where gii__Tier1DiscountPercent__c != null) from gii__Promotion__c];
            List<gii__AccountProgram__c> lstAccProgramTemp= giic_Test_DataCreationUtility.createAccProg();
            map<string,gii__AccountProgram__c> mapAccountPromoWithAccProgram=new map<string,gii__AccountProgram__c>();
            List<gii__AccountProgram__c> lstAccProgram=[select id,giic_TotalPromoRemain__c,giic_QualifiedDate__c,giic_TotalPromoUsed__c,gii__AccountReference__r.gii__Account__c,giic_PromoEndDate__c,giic_Promotion__c from gii__AccountProgram__c where id in :lstAccProgramTemp];
            giic_PromotionWrapper.PromoResponseWrapper objPromoResponseWrapper=new giic_PromotionWrapper.PromoResponseWrapper();
            list<gii__AccountProgram__c> lstAccountProgramToUpdate=new list<gii__AccountProgram__c>();
            list<gii__AccountProgram__c> lstAccountProgramToInsert=new list<gii__AccountProgram__c>();
            objPromoResponseWrapper.lstErrorInformation=new list<giic_PromotionWrapper.ErrorInformation>();
            Test.startTest();
            
            lstAccProgram[0].giic_PromoEndDate__c=null;
            string accPromoKey= giic_Test_DataCreationUtility.accRefList_N[0].gii__Account__c +'~' + giic_Test_DataCreationUtility.lstPromotion[0].Id;
            mapAccountPromoWithAccProgram.put(accPromoKey,lstAccProgram[0]);
            giic_PromotionHelper.getAccountProgramToInsertUpdate(giic_Test_DataCreationUtility.accRefList_N[0].id,accPromoKey,mapAccountPromoWithAccProgram,giic_Test_DataCreationUtility.lstPromotion[0],
                                                                objPromoResponseWrapper,lstAccountProgramToUpdate,lstAccountProgramToInsert);
            giic_Test_DataCreationUtility.lstPromotion[0].giic_IsCustomerBased__c=true;
            accPromoKey= giic_Test_DataCreationUtility.accRefList_N[0].gii__Account__c +'' + giic_Test_DataCreationUtility.lstPromotion[0].Id;
            mapAccountPromoWithAccProgram.put(giic_Test_DataCreationUtility.accRefList_N[0].gii__Account__c +'~' + giic_Test_DataCreationUtility.lstPromotion[0].Id,lstAccProgram[0]);
             giic_PromotionHelper.getAccountProgramToInsertUpdate(giic_Test_DataCreationUtility.accRefList_N[0].id,accPromoKey,mapAccountPromoWithAccProgram,giic_Test_DataCreationUtility.lstPromotion[0],
                                                                objPromoResponseWrapper,lstAccountProgramToUpdate,lstAccountProgramToInsert);
            giic_Test_DataCreationUtility.lstPromotion[0].giic_IsCustomerBased__c=false;
            giic_Test_DataCreationUtility.lstPromotion[0].giic_MaxUsedLimit__c=1;
            mapAccountPromoWithAccProgram.put(giic_Test_DataCreationUtility.accRefList_N[0].gii__Account__c +'~' + giic_Test_DataCreationUtility.lstPromotion[0].Id,lstAccProgram[0]);
            giic_PromotionHelper.getAccountProgramToInsertUpdate(giic_Test_DataCreationUtility.accRefList_N[0].id,accPromoKey,mapAccountPromoWithAccProgram,giic_Test_DataCreationUtility.lstPromotion[0],
                                                                objPromoResponseWrapper,lstAccountProgramToUpdate,lstAccountProgramToInsert);
            
            
            Test.stopTest();
        }
    }
    private static testMethod void test_applyPromotion() { 
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        System.RunAs(u)        
        {
            list<gii__salesorder__c> lstSalesOrder = [select id,gii__Released__c from gii__salesorder__c];
            lstSalesOrder[0].gii__Released__c=true;
            update lstSalesOrder; 
            list<gii__Promotion__c> lstPromo=[select id from gii__Promotion__c where giic_PromotionCode__c='RFE'];
            Test.startTest();
            giic_PromotionController.applyPromotion(new map<string,object>{giic_Constants.SOIDS=>lstSalesOrder[0].id,giic_Constants.PROMOCODE=>'RFE'});
            giic_PromotionController.deletePromotion(new map<string,object>{giic_Constants.SOIDS=>lstSalesOrder[0].id,giic_Constants.PROMOCODE=>'RFE','PCDELETEIDS'=>lstPromo[0].id});
            giic_PromotionHelper.getPromoFrequency(giic_Constants.PROMOFREQTYPE_MONTHLY);
            giic_PromotionHelper.getPromoFrequency(giic_Constants.PROMOFREQTYPE_QUARTERLY);
            giic_PromotionHelper.getPromoFrequency(giic_Constants.PROMOFREQTYPE_YEARLY);
            Test.stopTest();
        }
    }
    private static testMethod void test_recalculatePromotion() { 
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        System.RunAs(u)        
        {
            list<gii__salesorder__c> lstSalesOrder = [select id,gii__Released__c from gii__salesorder__c];
            lstSalesOrder[0].gii__Released__c=true;
            update lstSalesOrder;             
                        
            Test.startTest();
            giic_PromotionController.applyPromotion(new map<string,object>{giic_Constants.SOIDS=>lstSalesOrder[0].id,giic_Constants.PROMOCODE=>'RFE'});
            giic_PromotionHelper.insertDropShipCharge(new map<string,decimal>{lstSalesOrder[0].id =>25});
            giic_PromotionHelper.deleteAppliedPromotion(new map<string,object>{giic_Constants.SOIDS=>new set<string>{lstSalesOrder[0].id},'ISCANCELSO'=>false});
            Test.stopTest();
        }
    }
    
     private static testMethod void test_recalculatePromotion_scenario2() { 
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        System.RunAs(u)        
        {
            list<gii__salesorder__c> lstSalesOrder = [select id,gii__Released__c from gii__salesorder__c];
            lstSalesOrder[0].gii__Released__c=true;
            update lstSalesOrder; 
            
            gii__AdditionalCharge__c objAddCharge = new gii__AdditionalCharge__c();
            objAddCharge.Name= 'Shipping Fee';
            objAddCharge.gii__UnitPrice__c = 4.99;
            objAddCharge.giic_AdditionalChargeCode__c = giic_Constants.DROPSHIPADDIONTALCHARGE;
            insert   objAddCharge;              
            gii__SalesOrderAdditionalCharge__c objSOAddCharge = new gii__SalesOrderAdditionalCharge__c();
            objSOAddCharge.gii__AdditionalCharge__c = objAddCharge.Id;
            objSOAddCharge.gii__SalesOrder__c = lstSalesOrder[0].id;
            //objSOAddCharge.gii__CalculatedShippingAmount__c = objAddCharge.gii__UnitPrice__c;
            insert objSOAddCharge;
            
            Test.startTest();
            giic_PromotionController.applyPromotion(new map<string,object>{giic_Constants.SOIDS=>lstSalesOrder[0].id,giic_Constants.PROMOCODE=>'RFE'});
            
            giic_PromotionHelper.deleteAppliedPromotion(new map<string,object>{giic_Constants.SOIDS=>new set<string>{lstSalesOrder[0].id},'ISCANCELSO'=>false});
            try{giic_PromotionHelper.insertDropShipCharge(new map<string,decimal>{lstSalesOrder[0].id =>25});}
            catch(exception ex ){}
            Test.stopTest();
        }
    }
    private static testMethod void test_calculateTax() { 
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        System.RunAs(u)        
        {
            list<gii__salesorder__c> lstSalesOrder = [select id,gii__Released__c from gii__salesorder__c];
            system.assertEquals(lstSalesOrder.size() > 0, lstSalesOrder.size() > 0);
            Test.startTest();
            giic_PromotionController.calculateTax(lstSalesOrder[0].id);
            Test.stopTest();
        }
    }
    private static testMethod void test_PromotionFromAPI() { 
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        System.RunAs(u)        
        {
            Test.startTest();
            string promorequest = '[{"userRole":"CSR","totalWeight":15,"isAdditonalCharge":true,"totalOrderAmount":300,"paymentMethod":"Credit Card","orderNumber":"SO1234","isDropShip":false,"customer":"12345678","SalesOrderLine":[{"unitPrice":20,"solNumber":"SOL1234","productTotalWeight":15,"productCode":"740035EA","productCategory":null,"orderQuantity":1,"lineTotalAmount":300,"cancelledQuantity":0}],"AppliedPromotion":[{"promoCode":"CSR100OFF"},{"promoCode":"test"}]}]';
            RestRequest request = new RestRequest();
            request.requestUri ='/services/apexrest/PromotionDetails';
            request.httpMethod = 'POST';
            request.requestBody = Blob.valueof(promorequest);
            RestContext.request = request;
            giic_PromotionDetailAPI.getPromotionDetails();
            Test.stopTest();
        }
    }
    
    private static testMethod void test_recalculatePromotionOnCancelSO() { 
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        System.RunAs(u)        
        {
            map<string,object> mapRequest = new map<string,object>();
            Test.startTest();               
                giic_PromotionHelper.recalculatePromotionOnCancelSO(mapRequest);
            Test.stopTest();
        }
    }   
    
    private static testMethod void test_recalculatePromotion_Scneario2() { 
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        System.RunAs(u)        
        {
            Test.startTest();
                map<string,object> mapRequest = new map<string,object>();
                giic_PromotionHelper.recalculatePromotion(mapRequest);
            Test.stopTest();
        }
    }
    
}