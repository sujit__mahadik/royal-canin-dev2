global class OpportunityFocusBatchSchedule implements Schedulable {

	//call this from exec anon after code deploy
	global static String schedule(String chron) {
		OpportunityFocusBatchSchedule schedule = new OpportunityFocusBatchSchedule();
		return System.Schedule('OpportunityFocusBatch', chron, schedule);
	}

	global void execute(SchedulableContext sc) {
		OpportunityFocusBatch qfb = new OpportunityFocusBatch();
		Database.executeBatch(qfb);
	}
}