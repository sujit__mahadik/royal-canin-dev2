public with sharing class TermsAndConditionsController {

	public String content {get;set;}

	public TermsAndConditionsController() {
		String res = ApexPages.currentPage().getParameters().get('res');
		system.debug('@@@@@res' + res);

		StaticResource terms = [SELECT Name, NamespacePrefix, SystemModstamp
								FROM StaticResource
								WHERE Name = :res LIMIT 1];

		String namespace = terms.NamespacePrefix;
		String filePath = '/resource/' + terms.SystemModStamp.getTime() + '/' + (namespace != null && namespace != '' ? namespace + '__' : '') + res; 
		PageReference resFileRef = new PageReference(filePath);
		content = resFileRef.getContent().toString();

		/*StaticResource termsZip =[SELECT Name, NamespacePrefix, SystemModstamp
								    FROM StaticResource
								    WHERE Name = 'TermsAndConditions' LIMIT 1];

		String namespace2 = termsZip.NamespacePrefix;
		String fp = '/resource/' + termsZip.SystemModStamp.getTime() + '/' + (namespace2 != null && namespace2 != '' ? namespace2 + '__' : '') + 'terms/Retail.txt'; 
		PageReference pr = new PageReference(fp);
		String retTnC = pr.getContent().toString();
		system.debug('@@@@@retTnC' + retTnC);*/
	}
}