/*
 * Date:             - Developer, Company                          - Description
 * 2015-08-12        - Stevie Yakkel, Acumen Solutions             - Created
 */
@isTest
private class ChangePostalCodeAccOwnerBatchTest
{
	@isTest
	static void changePostalCodeTest() {
		setupTestData.createCustomSettings();
		String query = 'SELECT PostalCodeID__c FROM Account';
		Map<Id, Id> zipOwners = new Map<Id, Id>();
		Map<Id, Id> managers = new Map<Id, Id>();
		PostalCode__c pc = new PostalCode__c();
		pc.ZipCode__c = '44133';
		insert pc;
		Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
		User u = new User(Alias = 'syakk', Email='syakkel123321123@rc.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Chicago', UserName='syakkel123321123@rc.com');
		insert u;
		SetupTestData.createAccounts(1);
		Account acc2 = SetupTestData.testAccounts[0];
		acc2.Name = 'account';
		acc2.PostalCodeID__c = pc.Id;
		update acc2;
		managers.put(acc2.OwnerId, acc2.Id);
		zipOwners.put(pc.Id, u.Id);
        test.startTest();
        ChangePostalCodeAccountOwnershipBatch b = new ChangePostalCodeAccountOwnershipBatch(query, zipOwners, managers);
		database.executeBatch(b,200);
		test.stopTest();
		Account acc = [SELECT OwnerId FROM Account WHERE Id=:acc2.Id];
		//System.assertEquals(acc.OwnerId, u.Id);
	}
}