/************************************************************************************
Version : 1.0
Name : giic_Test_GenericRCCCServiceMock 
Created Date : 27 Dec 2018
Function : create mock responce for all the webservices used in RC-CC Service
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/

@isTest
global class giic_Test_GenericRCCCServiceMock implements WebServiceMock {
    //implements the interface method
    global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
        //Based on the request object create the response element           
        Boolean WriteHistory = String.valueOf(request).contains('WriteHistory_element') ? true : false;
        Boolean GetCardHistoryByBillToCustomerOMS = String.valueOf(request).contains('GetCardHistoryByBillToCustomerOMS_element') ? true : false;
        Boolean UpdateCreditCardHistory = String.valueOf(request).contains('UpdateCreditCardHistory_element') ? true : false;
        Boolean DeleteCreditCardHistory = String.valueOf(request).contains('DeleteCreditCardHistory_element') ? true : false;
        Boolean SetDefaultCreditCardHistory = String.valueOf(request).contains('SetDefaultCreditCardHistory_element') ? true : false;
        Boolean AuthorizationOMS = String.valueOf(request).contains('AuthorizationOMS_element') ? true : false;
        Boolean ProcessCCOMS_ReverseAuth = String.valueOf(request).contains('ProcessCCOMS_element') && String.valueOf(request).contains('status=4') ? true : false;
        Boolean ProcessCCOMS_Settlement = String.valueOf(request).contains('ProcessCCOMS_element') && String.valueOf(request).contains('status=2') ? true : false;
        Boolean ProcessCCOMS_Refund = String.valueOf(request).contains('ProcessCCOMS_element') && String.valueOf(request).contains('status=3') ? true : false;
        //String.valueOf(request).contains('
        //String.valueOf(request).contains('
        if(WriteHistory){
            System.debug('##Object request: '+request); //WriteHistory_element 
            giic_tempuriOrgD.WriteHistoryResponse_element respElement = new giic_tempuriOrgD.WriteHistoryResponse_element();
            response.put('response_x', respElement);
        }
        else if(GetCardHistoryByBillToCustomerOMS){
            giic_tempuriOrgD.GetCardHistoryByBillToCustomerOMSResponse_element respElement = new giic_tempuriOrgD.GetCardHistoryByBillToCustomerOMSResponse_element();
            respElement.GetCardHistoryByBillToCustomerOMSResult = new giic_schemasDatacontractOrg200407RcCommD.ArrayOfCreditCardHistoryOMS();
            response.put('response_x', respElement);
        }
        else if(UpdateCreditCardHistory){
            giic_tempuriOrgD.UpdateCreditCardHistoryResponse_element respElement = new giic_tempuriOrgD.UpdateCreditCardHistoryResponse_element();
            respElement.UpdateCreditCardHistoryResult = 'http://tempuri.org/,null,0,1,true';
            response.put('response_x', respElement);
        }
        else if(DeleteCreditCardHistory){
            giic_tempuriOrgD.DeleteCreditCardHistoryResponse_element respElement = new giic_tempuriOrgD.DeleteCreditCardHistoryResponse_element();
            respElement.DeleteCreditCardHistoryResult = 'http://tempuri.org/,null,0,1,true';
            response.put('response_x', respElement);
        }
        else if(SetDefaultCreditCardHistory){
            giic_tempuriOrgD.SetDefaultCreditCardHistoryResponse_element respElement = new giic_tempuriOrgD.SetDefaultCreditCardHistoryResponse_element();
            respElement.SetDefaultCreditCardHistoryResult = new giic_schemasDatacontractOrg200407RcCommD.ArrayOfCreditCardHistory();
            response.put('response_x', respElement);
        }
        else if(AuthorizationOMS){
            giic_tempuriOrgD.AuthorizationOMSResponse_element respElement = new giic_tempuriOrgD.AuthorizationOMSResponse_element();
            respElement.AuthorizationOMSResult = new giic_schemasDatacontractOrg200407RcCommD.OMSResponse();
            respElement.AuthorizationOMSResult.Status = 'Success';
            respElement.AuthorizationOMSResult.Message = 'ACCEPT';
            response.put('response_x', respElement);
        }
        else if(ProcessCCOMS_ReverseAuth){
            System.debug('##Object request ProcessCCOMS_ReverseAuth: '+request); 
            giic_tempuriOrgD.ProcessCCOMSResponse_element respElement = new giic_tempuriOrgD.ProcessCCOMSResponse_element();
            respElement.ProcessCCOMSResult = new giic_schemasDatacontractOrg200407RcCommD.OMSResponse();
            respElement.ProcessCCOMSResult.Status = 'Success';
            respElement.ProcessCCOMSResult.Message = 'ACCEPT';
            response.put('response_x', respElement); 
        }
        else if(ProcessCCOMS_Settlement){
            System.debug('##Object request ProcessCCOMS_Settlement: '+request); 
            giic_tempuriOrgD.ProcessCCOMSResponse_element respElement = new giic_tempuriOrgD.ProcessCCOMSResponse_element();
            respElement.ProcessCCOMSResult = new giic_schemasDatacontractOrg200407RcCommD.OMSResponse();
            respElement.ProcessCCOMSResult.Status = 'Success';
            respElement.ProcessCCOMSResult.Message = 'ACCEPT';
            response.put('response_x', respElement); 
        }
        else if(ProcessCCOMS_Refund){
            System.debug('##Object request ProcessCCOMS_Refund: '+request); 
            giic_tempuriOrgD.ProcessCCOMSResponse_element respElement = new giic_tempuriOrgD.ProcessCCOMSResponse_element();
            respElement.ProcessCCOMSResult = new giic_schemasDatacontractOrg200407RcCommD.OMSResponse();
            respElement.ProcessCCOMSResult.Status = 'Success';
            respElement.ProcessCCOMSResult.Message = 'ACCEPT';
            response.put('response_x', respElement); 
        }
        else{
            System.debug('##Object request else: '+request); 
        }
   }

}