/************************************************************************************
Version : 1.0
Name : giic_ReturnCreditsHelper
Created Date : 27 Sep 2018
Function : Handle all return credit services.
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/
public class giic_ReturnCreditsHelper {
    
        /***********************************************************
* Method name : getProductList
* Description : get all product list invoiced by account and return
* Return Type : Map<String, Object>
* Parameter : String recordId, String searchKeyword
***********************************************************/

    @AuraEnabled
    public static Map<String, Object> getProductList(String recordId, String searchKeyword){
        
        Map<String, Object> mapResult = new Map<String, Object>();
        List<giic_InvoicedProductWrapper> lstIPW = new List<giic_InvoicedProductWrapper>();
        set<id> salesorderIds = new set<id>();
        set<id> invoiceIds = new set<id>();
        map<id,decimal> mapSOTOCreditPayment = new map<id,decimal>();
        list<SOPayment> lstSOPayment = new list<SOPayment>();
        List<gii__PaymentMethod__c> lstPaymentMethod = new List<gii__PaymentMethod__c>();

            List<Case> lstCase = [Select Id, AccountId, Account.Name, Approval_Status__c,Status from Case 
                                  where Id = :recordId and Approval_Status__c =: giic_Constants.PENDING_CASE and Status =: giic_Constants.DRAFT_CASE ];
            if(!lstCase.isEmpty() )
            {
                String accId = lstCase[0].AccountId;
                if(accId != null && accId != ''){
                    List<gii__OrderInvoiceDetail__c> lstInvoices = getInvoiceList(accId, searchKeyword);
                    if(!lstInvoices.isEmpty()){                    
                        for(gii__OrderInvoiceDetail__c objIDetail : lstInvoices){
                            invoiceIds.add(objIDetail.gii__Invoice__c);
                            salesorderIds.add(objIDetail.gii__SalesOrder__c);
                            giic_InvoicedProductWrapper objIPW = new giic_InvoicedProductWrapper();
                            objIPW.productId = objIDetail.gii__ProductReference__c != null ? objIDetail.gii__ProductReference__c : '';
                            objIPW.productSKU = objIDetail.gii__ProductReference__r.gii__ProductReference__r.SKU__c != null ? objIDetail.gii__ProductReference__r.gii__ProductReference__r.SKU__c : '';
                            objIPW.productDescription = objIDetail.gii__ProductReference__r.gii__Description__c != null ? objIDetail.gii__ProductReference__r.gii__Description__c : '';
                            objIPW.invoiceId = objIDetail.gii__Invoice__c  != null ? objIDetail.gii__Invoice__c : '';
                            objIPW.invoiceNumber = objIDetail.gii__Invoice__r.giic_RefInvoiceNo__c != null ? objIDetail.gii__Invoice__r.giic_RefInvoiceNo__c : '';
                            objIPW.soId = objIDetail.gii__SalesOrder__c  != null ? objIDetail.gii__SalesOrder__c : '';
                            objIPW.soNumber = objIDetail.gii__SalesOrder__r.Name != null ? objIDetail.gii__SalesOrder__r.Name : '';
                            objIPW.soPaymentMethod = objIDetail.gii__Invoice__r.gii__SalesOrder__r.gii__PaymentMethod__c;
                            objIPW.invoiceDetailId = objIDetail.Id  != null ? objIDetail.Id : '';
                            objIPW.orderDate = objIDetail.gii__SalesOrder__r.gii__OrderDate__c != null ? String.valueOf(objIDetail.gii__SalesOrder__r.gii__OrderDate__c) : '';
                            objIPW.orderQuantity = objIDetail.gii__InvoicedQuantity__c != null ? objIDetail.gii__InvoicedQuantity__c : 0;
                            objIPW.qtyAvailableForReturn = objIDetail.gii__RemainingCreditQuantity__c != null ? objIDetail.gii__RemainingCreditQuantity__c : 0;
                            objIPW.invoiceAmount = objIDetail.gii__NetInvoiceAmount__c != null ? objIDetail.gii__NetInvoiceAmount__c : 0;
                            objIPW.invoiceAmountStocking = objIPW.invoiceAmount;
                            objIPW.sellingUOM = objIDetail.gii__ProductReference__r.gii__SellingUnitofMeasure__r.Name != null  ? objIDetail.gii__ProductReference__r.gii__SellingUnitofMeasure__r.Name : '';
                            objIPW.sellingUOMId = objIDetail.gii__ProductReference__r.gii__SellingUnitofMeasure__c != null ? objIDetail.gii__ProductReference__r.gii__SellingUnitofMeasure__c : null;
                            objIPW.stockingUOM = objIDetail.gii__ProductReference__r.gii__StockingUnitofMeasure__r.Name != null ? objIDetail.gii__ProductReference__r.gii__StockingUnitofMeasure__r.Name : '';
                            objIPW.stockingUOMId = objIDetail.gii__ProductReference__r.gii__StockingUnitofMeasure__c != null ? objIDetail.gii__ProductReference__r.gii__StockingUnitofMeasure__c : null;
                            if(objIPW.sellingUOMId != null && objIPW.stockingUOM != null && objIPW.productId != ''){
                                Decimal convertedQty = giic_IntegrationCommonUtil.convertUOMAPI(objIPW.productId, objIPW.sellingUOMId, objIPW.stockingUOMId, objIPW.qtyAvailableForReturn);
                                if(convertedQty == objIPW.qtyAvailableForReturn) objIPW.stockingUOM = objIPW.sellingUOM;
                            }
                            if(objIDetail.gii__ProductReference__r.gii__SellingUnitofMeasure__r.Name == 'EA'){
                                objIPW.qtyAvailableForReturnEA = ''+objIPW.qtyAvailableForReturn;
                            }else if(objIDetail.gii__ProductReference__r.gii__StockingUnitofMeasure__r.Name == 'EA'){
                                if(objIPW.productId != '' && objIPW.sellingUOMId != null && objIPW.stockingUOMId != null && objIPW.qtyAvailableForReturn != 0){
                                    Decimal convertedQty = giic_IntegrationCommonUtil.convertUOMAPI(objIPW.productId, objIPW.sellingUOMId, objIPW.stockingUOMId, objIPW.qtyAvailableForReturn);
                                    if(convertedQty != objIPW.qtyAvailableForReturn) objIPW.qtyAvailableForReturnEA = ''+convertedQty;
                                }
                            }else{
                                List<gii__UnitofMeasure__c> lstUOM = [select Id, Name, gii__UniqueId__c from gii__UnitofMeasure__c where Name = 'EA'];
                                if(!lstUOM.isEmpty() && objIPW.productId != '' && objIPW.sellingUOMId != null){
                                    Decimal convertedQty = giic_IntegrationCommonUtil.convertUOMAPI(objIPW.productId, objIPW.sellingUOMId, lstUOM[0].Id, objIPW.qtyAvailableForReturn);
                                    if(convertedQty != objIPW.qtyAvailableForReturn) objIPW.qtyAvailableForReturnEA = ''+convertedQty;
                                }
                            }
                            
                            lstIPW.add(objIPW);
                        }
                        SOPayment sop;                        
                        
                        map<id,List<gii__ARCreditPayment__c>> map_Invoice_ARP = new map<id,List<gii__ARCreditPayment__c>>();
                        for(gii__ARCreditPayment__c objARP : [select id,gii__PaidAmount__c,gii__PaymentMethod__c,gii__ARCredit__r.gii__Invoice__c from gii__ARCreditPayment__c where gii__ARCredit__r.gii__Invoice__c in: invoiceIds ])
                        {
                            
                            if(map_Invoice_ARP.get(objARP.gii__ARCredit__r.gii__Invoice__c) == null)
                            {
                                map_Invoice_ARP.put(objARP.gii__ARCredit__r.gii__Invoice__c, new List<gii__ARCreditPayment__c>());
                            }
                            map_Invoice_ARP.get(objARP.gii__ARCredit__r.gii__Invoice__c).add(objARP);
                        }
                        
                        for(gii__ARInvoicePayment__c arinpmt : [select id,gii__PaymentMethod__c,gii__PaidAmount__c,gii__Invoice__c,gii__Invoice__r.gii__SalesOrder__c 
                                                                from gii__ARInvoicePayment__c 
                                                                where gii__Invoice__c IN:invoiceIds order by gii__Invoice__c])
                        {
                            sop = new SOPayment();
                            sop.soid = arinpmt.gii__Invoice__r.gii__SalesOrder__c;
                            sop.invid = arinpmt.gii__Invoice__c;
                            sop.paymentAmount = arinpmt.gii__PaidAmount__c;
                            sop.paymentMethod = arinpmt.gii__PaymentMethod__c;
                            lstSOPayment.add(sop);
                        }                        
                        
                        lstPaymentMethod = [select id,Name,giic_CreditSequence__c,giic_PaymentConsumptionSeq__c 
                                            from gii__PaymentMethod__c order by giic_CreditSequence__c asc];
                    }
                }else{
                    throw new AuraHandledException(Label.giic_AccountNotFound); 
                }
                
                mapResult.put('lstReturnReason', getReasons());
                mapResult.put('productList', lstIPW);
                mapResult.put('AccountName', lstCase[0].Account.Name);
                mapResult.put('SOPayment',lstSOPayment);
                mapResult.put('lstPaymentMethod',lstPaymentMethod);
                mapResult.put('ApprovalStatus',giic_Constants.PENDING_CASE);
            }
            else
            {
                mapResult.put('ApprovalStatus',giic_Constants.NONPENDING_CASE);
                throw new AuraHandledException(Label.giic_ApprovalStatusPending);
            }
        return mapResult;
        
    }
    public class SOPayment
    {
        @AuraEnabled public String soid;
        @AuraEnabled public String invid;
        @AuraEnabled public decimal paymentAmount;
        @AuraEnabled public String paymentMethod;       
    }
    
/***********************************************************
* Method name : getInvoiceList
* Description : get invoice list for account
* Return Type : List<gii__OrderInvoiceDetail__c>
* Parameter : String accId, String searchKeyword
***********************************************************/
    
    public static List<gii__OrderInvoiceDetail__c> getInvoiceList(String accId, String searchKeyword){
        
        String queryString = 'Select Id, Name, gii__ProductReference__c, gii__ProductReference__r.Name, gii__ProductReference__r.gii__ProductReference__r.SKU__c, gii__ProductReference__r.gii__SellingUnitofMeasure__c, gii__ProductReference__r.gii__SellingUnitofMeasure__r.Name, gii__ProductReference__r.gii__StockingUnitofMeasure__c, gii__ProductReference__r.gii__StockingUnitofMeasure__r.Name,  gii__ProductReference__r.gii__Description__c, gii__Invoice__c, gii__Invoice__r.Name, gii__Invoice__r.giic_RefInvoiceNo__c, gii__SalesOrder__c, gii__SalesOrder__r.Name, gii__SalesOrder__r.gii__OrderDate__c, gii__InvoicedQuantity__c, gii__RemainingCreditQuantity__c,  gii__NetInvoiceAmount__c,gii__Invoice__r.gii__SalesOrder__r.gii__PaymentMethod__c From gii__OrderInvoiceDetail__c '
            + ' where gii__Invoice__r.gii__Account__c =: accId and  gii__RemainingCreditQuantity__c > 0 and gii__Invoice__r.gii__InvoiceCreationCompleted__c = true and gii__Invoice__r.giic_RefInvoiceNo__c != null ';
        
        if(searchKeyword != null && searchKeyword != ''){
            String searchKey = '%' + searchKeyWord + '%';
            queryString += ' and ( gii__Invoice__r.giic_RefInvoiceNo__c Like : searchKey '; 
            queryString += ' or  gii__Invoice__r.gii__SalesOrder__r.Name Like : searchKey or gii__ProductReference__r.gii__ProductReference__r.SKU__c like : searchKey ) '; 
        }

        List<gii__OrderInvoiceDetail__c> lstInvoices = Database.query(queryString);
        return lstInvoices;
    }
    
    
    /***********************************************************
* Method name : getReasons
* Description : get reasons for return.
* Return Type : List<String>
* Parameter : none
***********************************************************/
    public static List<String> getReasons(){
        List<String> options = new List<String>();
        options.add('--None--');
        Schema.DescribeFieldResult fieldResult = giic_CaseProduct__c.giic_ReturnReason__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple){
            options.add(f.getValue());
        }       
        return options;
    }
    
        /***********************************************************
* Method name : getInvoiceAdditionalCharge
* Description : get Additional Charge of Invoice 
* Return Type : Map<String, Object>
* Parameter : String invId
***********************************************************/
    
    @AuraEnabled
    public static Map<String, Object> getInvoiceAdditionalCharge(String invId){
        List<giic_InvoicedProductWrapper> lstInvAC = new List<giic_InvoicedProductWrapper>();
        Map<String, Object> mapResult = new Map<String, Object>();
        
        // Checking if AdditionalCharge has already been returned
        
        List<giic_CaseProduct__c> caseProdList = [select id from giic_CaseProduct__c where giic_Invoice__c =: invId and giic_InvoiceAdditionalCharge__c != null];
        if(caseProdList.isEmpty()){
             for(gii__InvoiceAdditionalCharge__c objAC :[Select Name, gii__AdditionalCharge__c, gii__AdditionalCharge__r.Name, gii__UnitPrice__c, gii__UnitCost__c, gii__Quantity__c, gii__AdditionalChargeAmount__c, gii__TaxAmount__c, gii__TotalAmount__c, gii__SalesOrder__c, gii__SalesOrder__r.Name, gii__Invoice__c, gii__Invoice__r.Name, gii__Invoice__r.giic_RefInvoiceNo__c from gii__InvoiceAdditionalCharge__c where gii__Invoice__c = :invId ]){//and Id not in (select giic_InvoiceAdditionalCharge__c from giic_CaseProduct__c where giic_Invoice__c=:invId)
            giic_InvoicedProductWrapper objIPW = new giic_InvoicedProductWrapper();
            objIPW.invoiceId                    = objAC.gii__Invoice__c  != null ? objAC.gii__Invoice__c : '';
            objIPW.invoiceNumber                = objAC.gii__Invoice__r.giic_RefInvoiceNo__c != null ? objAC.gii__Invoice__r.giic_RefInvoiceNo__c : '';
            objIPW.soId                         = objAC.gii__SalesOrder__c  != null ? objAC.gii__SalesOrder__c : null;
            objIPW.soNumber                     = objAC.gii__SalesOrder__r.Name != null ? objAC.gii__SalesOrder__r.Name : '';
            objIPW.additionalChargeInvNo        = objAC.Name;
            objIPW.additionalChargeInvId        = objAC.Id ;
            objIPW.additionalChargeId           = objAC.gii__AdditionalCharge__c  != null ? objAC.gii__AdditionalCharge__c : '';
            objIPW.additionalChargeName         = objAC.gii__AdditionalCharge__c  != null ? objAC.gii__AdditionalCharge__r.Name : '';
            objIPW.additionalChargeUnitPrice    = objAC.gii__UnitPrice__c != null ? objAC.gii__UnitPrice__c : 0;
            objIPW.additionalChargeUnitCost     = objAC.gii__UnitCost__c != null ? objAC.gii__UnitCost__c : 0;
            objIPW.additionalChargeQuantity     = objAC.gii__Quantity__c != null ? objAC.gii__Quantity__c : 0;
            objIPW.additionalChargeAmount       = objAC.gii__AdditionalChargeAmount__c != null ? objAC.gii__AdditionalChargeAmount__c : 0;
            objIPW.additionalChargeTaxAmount    = objAC.gii__TaxAmount__c != null ? objAC.gii__TaxAmount__c : 0;
            objIPW.additionalChargeTotalAmount  = objAC.gii__TotalAmount__c != null ? objAC.gii__TotalAmount__c : 0;
            lstInvAC.add(objIPW);    
             } 
        }
        mapResult.put('additionalChargeList', lstInvAC);
        return mapResult;
    }
    
            /***********************************************************
* Method name : getProductwithChangedUOM
* Description : get converted UOM qty 
* Return Type : Map<String, Object>
* Parameter : String strWrapper
***********************************************************/
    
    @AuraEnabled
    public static Map<String, Object> getProductwithChangedUOM(String strWrapper){
        Map<String, Object> mapResult = new Map<String, Object>();
        giic_InvoicedProductWrapper objIPW = (giic_InvoicedProductWrapper) JSON.deserialize(strWrapper, giic_InvoicedProductWrapper.class);
        
        if(objIPW.sellingUOMId != null && objIPW.stockingUOM != null){
            Decimal qtyForConversion = objIPW.qtyAvailableForReturn;
            if(objIPW.UOM == objIPW.stockingUOM){
                objIPW.qtyAvailableForReturn = giic_IntegrationCommonUtil.convertUOMAPI(objIPW.productId, objIPW.sellingUOMId, objIPW.stockingUOMId, qtyForConversion);
                objIPW.invoiceAmountStocking = (qtyForConversion / objIPW.qtyAvailableForReturn) * objIPW.invoiceAmount;
            }else{
                objIPW.qtyAvailableForReturn = giic_IntegrationCommonUtil.convertUOMAPI(objIPW.productId, objIPW.stockingUOMId, objIPW.sellingUOMId, qtyForConversion); 
                objIPW.invoiceAmountStocking = objIPW.invoiceAmount;
            } 
            objIPW.returnQty = 0;
            objIPW.returnAmount = 0;
            
        }else{
            throw new AuraHandledException(Label.giic_UOMNotConfigured);
        }
        mapResult.put('productWrapper', objIPW);
        return mapResult;
    }
                /***********************************************************
* Method name : saveReturnList
* Description : save selected return products in case products 
* Return Type : Map<String, Object>
* Parameter : String recordId, String strProdList, String strInvACList
***********************************************************/

    @AuraEnabled
    public static Map<String, Object> saveReturnList(String recordId, String strProdList, String strInvACList){
        Map<String, Object> mapResult = new Map<String, Object>();
        List<giic_InvoicedProductWrapper> lstInvAC = (List<giic_InvoicedProductWrapper>) JSON.deserialize(strInvACList, List<giic_InvoicedProductWrapper>.class);
        List<giic_InvoicedProductWrapper> lstIPW = (List<giic_InvoicedProductWrapper>) JSON.deserialize(strProdList, List<giic_InvoicedProductWrapper>.class);
                
        List<giic_CaseProduct__c> lstCaseProduct = new List<giic_CaseProduct__c>();
        if(!lstIPW.isEmpty()){            
            for(giic_InvoicedProductWrapper objIPW : lstIPW){
                if(objIPW.isSelected){
                    giic_CaseProduct__c objCaseProduct = new giic_CaseProduct__c();
                    objCaseProduct.giic_Case__c = recordId;
                    objCaseProduct.giic_Invoice__c = objIPW.invoiceId;
                    objCaseProduct.giic_InvoiceDetail__c = objIPW.invoiceDetailId;
                    objCaseProduct.giic_ProductReference__c = objIPW.productId;
                    objCaseProduct.giic_ReturnAmount__c = objIPW.returnAmount;
                    objCaseProduct.giic_ReturnQuantity__c = objIPW.returnQty;
                    objCaseProduct.giic_ReturnReason__c = objIPW.returnReason;
                    objCaseProduct.giic_SalesOrder__c = objIPW.soId;
                    objCaseProduct.giic_UoM__c = objIPW.UOM == objIPW.sellingUOM ? objIPW.sellingUOMId : objIPW.stockingUOMId;
                    lstCaseProduct.add(objCaseProduct);                    
                }
            }
        }
        
        if(!lstInvAC.isEmpty()){            
            for(giic_InvoicedProductWrapper objIPW : lstInvAC){
                if(objIPW.isSelected){
                    giic_CaseProduct__c objCaseProduct = new giic_CaseProduct__c();
                    objCaseProduct.giic_Case__c = recordId;
                    objCaseProduct.giic_Invoice__c = objIPW.invoiceId; 
                    objCaseProduct.giic_SalesOrder__c = objIPW.soId;
                    objCaseProduct.giic_InvoiceAdditionalCharge__c = objIPW.additionalChargeInvId;
                    objCaseProduct.giic_AdditionalCharge__c = objIPW.additionalChargeId;
                    objCaseProduct.giic_ReturnAmount__c = objIPW.additionalChargeAmount;
                    objCaseProduct.giic_ReturnQuantity__c = objIPW.additionalChargeQuantity;
                    //objCaseProduct.giic_ReturnReason__c = objIPW.returnReason;
                    lstCaseProduct.add(objCaseProduct);
                }
            }
        }
        
        if(!lstCaseProduct.isEmpty()){
            insert lstCaseProduct;                       
            createAR(recordId);              
        }               
        mapResult.put('productList', lstIPW);
        return mapResult;
    }   
   
                   /***********************************************************
* Method name : createAR
* Description : Create AR records
* Return Type : none
* Parameter : String recordId
***********************************************************/
   
        
    public static void createAR(String recordId){
        Set<String> caseIds = new Set<String>();
        caseIds.add(recordId);
        List<giic_CaseProduct__c> cpList = new List<giic_CaseProduct__c>([Select giic_Case__c, giic_UoM__r.Name, giic_Invoice__c, giic_Invoice__r.gii__Account__c, giic_InvoiceDetail__r.gii__ProductReference__c, 
                                                                          giic_InvoiceDetail__r.gii__UnitPrice__c, giic_InvoiceDetail__r.gii__ShippedQuantity__c,
                                                                          giic_ReturnQuantity__c, giic_ReturnAmount__c, giic_ReturnReason__c, 
                                                                          giic_SalesOrder__r.gii__Warehouse__c, giic_SalesOrder__c, giic_UoM__c, giic_InvoiceDetail__r.gii__Warehouse__c,
                                                                          giic_Invoice__r.gii__BillingZipPostalCode__c, giic_Invoice__r.gii__BillingStreet__c, 
                                                                          giic_Invoice__r.gii__BillingStateProvince__c, giic_Invoice__r.gii__BillingName__c, 
                                                                          giic_Invoice__r.gii__BillingCountry__c, giic_Invoice__r.gii__BillingCity__c, 
                                                                          giic_SalesOrder__r.gii__ShipToZipPostalCode__c, 
                                                                          giic_SalesOrder__r.gii__ShipToStreet__c, giic_SalesOrder__r.gii__ShipToStateProvince__c, 
                                                                          giic_SalesOrder__r.gii__ShipToName__c, giic_SalesOrder__r.gii__DiscountPercent__c,
                                                                          giic_SalesOrder__r.gii__ShipToCountry__c, giic_SalesOrder__r.gii__ShipToCity__c, 
                                                                          giic_SalesOrder__r.gii__CustomerPONumber__c, 
                                                                          giic_InvoiceDetail__r.gii__VAT__c, giic_InvoiceDetail__r.gii__VATTaxable__c, 
                                                                          giic_InvoiceDetail__r.gii__VATAmount__c, giic_InvoiceDetail__r.gii__UsageTaxable__c, 
                                                                          giic_InvoiceDetail__r.gii__TaxAmount__c, giic_InvoiceDetail__r.gii__StockUM__c, 
                                                                          giic_InvoiceDetail__r.gii__SalesTaxable__c, giic_InvoiceDetail__r.gii__SalesOrderLine__c, 
                                                                          giic_InvoiceDetail__r.gii__ProductAmount__c, giic_InvoiceDetail__r.gii__OrderDiscountPercent__c, 
                                                                          giic_InvoiceDetail__r.gii__OrderDiscountAmount__c, giic_InvoiceDetail__r.gii__ExciseTaxable__c, 
                                                                          giic_InvoiceDetail__r.gii__DiscountPercent__c, giic_InvoiceDetail__r.gii__DiscountAmount__c,giic_InvoiceAdditionalCharge__c,giic_AdditionalCharge__c                                                            
                                                                          ,giic_InvoiceDetail__r.gii__NetInvoiceAmount__c, giic_InvoiceDetail__r.gii__InvoicedQuantity__c                                                                         
                                                                          from giic_CaseProduct__c
                                                                          where giic_Case__c in : caseIds]);
        
        //One credit header corresponding to every Account.

        Integer roundOff = gii__GloviaSystemSetting__c.getOrgDefaults().gii__DecimalsWhenRoundingQuantities__c != null? Integer.valueOf(gii__GloviaSystemSetting__c.getOrgDefaults().gii__DecimalsWhenRoundingQuantities__c) : 2;
        
        Map<Id, Id> mpInvCase = new Map<Id, Id>();
        Map<id, gii.ARCreditCreation.ARCreditHeader > creditHeadMap = new Map<id, gii.ARCreditCreation.ARCreditHeader >();
        for(giic_CaseProduct__c cp : cpList){
            if(!creditHeadMap.containsKey(cp.giic_Invoice__c)){  
                mpInvCase.put(cp.giic_Invoice__c, cp.giic_Case__c);
                gii.ARCreditCreation.ARCreditHeader ARCHeader = new gii.ARCreditCreation.ARCreditHeader();
                ARCHeader.ARCreditSalesLines = new List<gii.ARCreditCreation.ARCreditSalesLine>();
                ARCHeader.ARCreditAdditionalCharges = new List<gii.ARCreditCreation.ARCreditAdditionalCharge>();
                
                ARCHeader.BillingCity = cp.giic_Invoice__r.gii__BillingCity__c;
                ARCHeader.BillingCountry = cp.giic_Invoice__r.gii__BillingCountry__c;
                ARCHeader.BillingName = cp.giic_Invoice__r.gii__BillingName__c;
                ARCHeader.BillingStateProvince = cp.giic_Invoice__r.gii__BillingStateProvince__c;
                ARCHeader.BillingStreet = cp.giic_Invoice__r.gii__BillingStreet__c;
                ARCHeader.BillingZipPostalCode = cp.giic_Invoice__r.gii__BillingZipPostalCode__c;
                ARCHeader.ShiptoCity = cp.giic_SalesOrder__r.gii__ShipToCity__c;
                ARCHeader.ShiptoCountry = cp.giic_SalesOrder__r.gii__ShipToCountry__c;
                ARCHeader.ShiptoName = cp.giic_SalesOrder__r.gii__ShipToName__c;
                ARCHeader.ShiptoStateProvince = cp.giic_SalesOrder__r.gii__ShipToStateProvince__c;
                ARCHeader.ShiptoStreet = cp.giic_SalesOrder__r.gii__ShipToStreet__c;
                ARCHeader.ShiptoZipPostalCode = cp.giic_SalesOrder__r.gii__ShipToZipPostalCode__c;
                ARCHeader.InvoiceId = cp.giic_Invoice__c;
                ARCHeader.WarehouseId = cp.giic_SalesOrder__r.gii__Warehouse__c;
                ARCHeader.SalesOrderId = cp.giic_SalesOrder__c;
                ARCHeader.AccountId=cp.giic_Invoice__r.gii__Account__c;
                ARCHeader.Reason = cp.giic_ReturnReason__c; //set AR Credit Reason
                ARCHeader.CreditDate = system.today(); // this is mandatory as well.
                ARCHeader.OrderDiscountPercent = cp.giic_SalesOrder__r.gii__DiscountPercent__c;
                creditHeadMap.put(cp.giic_Invoice__c, ARCHeader);
            }
            
            if(cp.giic_InvoiceDetail__c != null){
                gii.ARCreditCreation.ARCreditSalesLine ObjSalesLine  = new gii.ARCreditCreation.ARCreditSalesLine();
                ObjSalesLine.AGC = cp.giic_InvoiceDetail__c;
                ObjSalesLine.InvoiceDetailId = cp.giic_InvoiceDetail__c;
                ObjSalesLine.WarehouseId = cp.giic_InvoiceDetail__r.gii__Warehouse__c;
                ObjSalesLine.ProductId = cp.giic_InvoiceDetail__r.gii__ProductReference__c;
                
                ObjSalesLine.OrderQuantity = cp.giic_ReturnQuantity__c; // set OrderQuantity (Selling UM)
                ObjSalesLine.UnitPrice = cp.giic_InvoiceDetail__r.gii__UnitPrice__c; //(cp.giic_InvoiceDetail__r.gii__NetInvoiceAmount__c/cp.giic_InvoiceDetail__r.gii__InvoicedQuantity__c);//cp.giic_InvoiceDetail__r.gii__UnitPrice__c;
                ObjSalesLine.ProductAmount = (cp.giic_ReturnQuantity__c*cp.giic_InvoiceDetail__r.gii__UnitPrice__c).setScale(roundOff, System.RoundingMode.HALF_UP);//cp.giic_ReturnQuantity__c*(cp.giic_InvoiceDetail__r.gii__NetInvoiceAmount__c/cp.giic_InvoiceDetail__r.gii__InvoicedQuantity__c);//cp.giic_ReturnQuantity__c*cp.giic_InvoiceDetail__r.gii__UnitPrice__c;
                
                ObjSalesLine.DiscountPercent = cp.giic_InvoiceDetail__r.gii__DiscountPercent__c;
                ObjSalesLine.DiscountAmount = (ObjSalesLine.DiscountPercent!= null? ((ObjSalesLine.ProductAmount * ObjSalesLine.DiscountPercent)/100) : 0).setScale(roundOff, System.RoundingMode.HALF_UP);
                ObjSalesLine.OrderDiscountPercent = cp.giic_InvoiceDetail__r.gii__OrderDiscountPercent__c;
                ObjSalesLine.OrderDiscountAmount = (ObjSalesLine.OrderDiscountPercent!= null? (((ObjSalesLine.ProductAmount - ObjSalesLine.DiscountAmount) * ObjSalesLine.OrderDiscountPercent )/100): 0).setScale(roundOff, System.RoundingMode.HALF_UP);
                
                ObjSalesLine.TaxAmount = (cp.giic_InvoiceDetail__r.gii__TaxAmount__c != null ? (ObjSalesLine.OrderQuantity *(cp.giic_InvoiceDetail__r.gii__TaxAmount__c/cp.giic_InvoiceDetail__r.gii__InvoicedQuantity__c)) : 0).setScale(roundOff, System.RoundingMode.HALF_UP);//cp.giic_ReturnQuantity__c*(cp.giic_InvoiceDetail__r.gii__TaxAmount__c/cp.giic_InvoiceDetail__r.gii__InvoicedQuantity__c);//cp.giic_InvoiceDetail__r.gii__TaxAmount__c;
                
                ObjSalesLine.SalesOrderLineId = cp.giic_InvoiceDetail__r.gii__SalesOrderLine__c;
                ObjSalesLine.SalesTaxable = cp.giic_InvoiceDetail__r.gii__SalesTaxable__c;
                ObjSalesLine.StockUM = cp.giic_UoM__r.Name;//cp.giic_InvoiceDetail__r.gii__StockUM__c;

                ObjSalesLine.Reason = cp.giic_ReturnReason__c; //set Reason for AR Credit Sales Line
                creditHeadMap.get(cp.giic_Invoice__c).ARCreditSalesLines.add(ObjSalesLine);
            }else if(cp.giic_InvoiceAdditionalCharge__c != null){
                //create AR Credit Additional Charge
                gii.ARCreditCreation.ARCreditAdditionalCharge ObjAddCharge = new gii.ARCreditCreation.ARCreditAdditionalCharge();
                // Set Additional Charge Id
                ObjAddCharge.AdditionalChargeId = cp.giic_AdditionalCharge__c;
                ObjAddCharge.UnitPrice = cp.giic_ReturnAmount__c; //set Unit Price
                ObjAddCharge.Quantity = cp.giic_ReturnQuantity__c;// set Quantity
                ObjAddCharge.InvoiceAdditionalChargeId = cp.giic_InvoiceAdditionalCharge__c;

                // List of ARCreditAdditionalCharge obj
                creditHeadMap.get(cp.giic_Invoice__c).ARCreditAdditionalCharges.add(ObjAddCharge);
            }
        }

        List<gii__ARCredit__c> ARC = new List<gii__ARCredit__c>();
        
        for(Id invId :creditHeadMap.keySet()){
            List<gii.ARCreditCreation.ARCreditHeader> achs = new List<gii.ARCreditCreation.ARCreditHeader>{creditHeadMap.get(invId)};
            List<gii__ARCredit__c> ARC1 = gii.ARCreditCreation.createARCreditRecords(achs);
            ARC.addAll(ARC1);
        }
        
        for(gii__ARCredit__c ar :ARC){
            if(ar.gii__Invoice__c != null && mpInvCase.containsKey(ar.gii__Invoice__c)){
                ar.gii_Case__c = mpInvCase.get(ar.gii__Invoice__c);     
            }
        }
        
        if(!ARC.isEmpty()) update ARC;
    }


      /***********************************************************
* Method name : deleteARCredit
* Description : Delete AR records
* Return Type : none
* Parameter : Set<String> rejectedCases
***********************************************************/

    public static void deleteARCredit(Set<String> rejectedCases){
        List<gii__ARCredit__c> lstAR = [select Id from gii__ARCredit__c where gii_Case__c in :rejectedCases];   
        
        if(!lstAR.isEmpty()) {
            List<gii__ARCreditAdditionalCharge__c> lstARAc = [select Id from gii__ARCreditAdditionalCharge__c where gii__ARCredit__c in :lstAR];
            delete lstARAc;
            
            List<gii__ARCreditSalesLine__c> lstARAl = [select Id from gii__ARCreditSalesLine__c where gii__ARCredit__c in :lstAR];
            delete lstARAl;
            
            delete lstAR;
        }        
    }

}