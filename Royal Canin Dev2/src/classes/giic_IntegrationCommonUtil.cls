/************************************************************************************
Version : 1.0
Name : giic_IntegrationCommonUtil
Created Date : 04 Sep 2018
Function : Common methods for integration
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/
public class giic_IntegrationCommonUtil {
    //Validate Address of Account records
    public static Map<Id , giic_ValShippingAddrFedExController.AddressValidation> validateAccountAddressAddress(List<Account> lstAccounts){
        List<Account> accountToUpdate = new List<Account>();
        //prepare list of address for validation
        List<giic_ValShippingAddrFedExController.AddressToValidateWrapper> lstAddress = new List<giic_ValShippingAddrFedExController.AddressToValidateWrapper>();
        for(Account objAct : lstAccounts){
            giic_ValShippingAddrFedExController.AddressToValidateWrapper objAddress = new giic_ValShippingAddrFedExController.AddressToValidateWrapper();
            objAddress.recordId      = objAct.Id;
            objAddress.ClientReferenceId = objAct.ShipToCustomerNo__c;
            objAddress.StreetLines   = objAct.ShippingStreet;
            objAddress.City          = objAct.ShippingCity;
            objAddress.PostalCode    = objAct.ShippingPostalCode;
            objAddress.CountryName   = objAct.ShippingCountry;
            lstAddress.add(objAddress);
        }
        //initialize Adress to validate and call webservice
        giic_FedExAddressValidation.AddressValidationReply response = giic_ValShippingAddrFedExController.processAddressToValidate(lstAddress);
        
        //prepare result of address validation
        Map<Id, String> mpAddresssType = new Map<Id, String>();
        Map<Id , giic_ValShippingAddrFedExController.AddressValidation> mapResult = giic_ValShippingAddrFedExController.validateAddress(response, lstAddress, mpAddresssType);
        system.debug('mapResult::' + mapResult); // imp
        List<Integration_Error_Log__c> lstErrors = new List<Integration_Error_Log__c>();
        Set<String> setErrorIds = new Set<String>();
        Set<Id> errIdSet = new Set<Id>();
        String userStory = System.Label.giic_AddressValidation;
        Map<String, String> fieldApiForErrorLog = new Map<String, String>{'giic_Account__c'=>'Id'};
        for(Account objAccount : lstAccounts){
            if(mapResult.containsKey(objAccount.Id) && mapResult.get(objAccount.Id).isValidAddress == true){
                objAccount.Validated__c = true;
                if(mpAddresssType.containsKey(objAccount.Id)) objAccount.giic_AddressType__c = mpAddresssType.get(objAccount.Id);
                accountToUpdate.add(objAccount);
            }
            else if(mapResult!= null && mapResult.containsKey(objAccount.Id)){ 
                errIdSet.add(objAccount.Id);
                //objAccount.giic_Status__c = System.Label.giic_Error;
                String errorMsg = System.Label.giic_ShippingAddInvalid;
                errorMsg += '\n Suggested Address :';
                errorMsg += '\n Street : ' + mapResult.get(objAccount.Id).suggestedAddress.StreetLines;
                errorMsg += '\n City : ' + mapResult.get(objAccount.Id).suggestedAddress.City;
                errorMsg += '\n PostalCode : ' + mapResult.get(objAccount.Id).suggestedAddress.PostalCode;
                errorMsg += '\n CountryCode : ' + mapResult.get(objAccount.Id).suggestedAddress.CountryCode;
                giic_IntegrationCustomMetaDataUtil.collectErrors(lstErrors, setErrorIds, userStory, fieldApiForErrorLog, objAccount, System.Label.giic_InvalidAddress, errorMsg);
            }
            else{
                errIdSet.add(objAccount.Id);
                String errorMsg = System.Label.giic_ShippingAddInvalid;
                giic_IntegrationCustomMetaDataUtil.collectErrors(lstErrors, setErrorIds, userStory, fieldApiForErrorLog, objAccount, System.Label.giic_InvalidAddress, errorMsg);
            }
        }
        // Deactivate old error logs
        
        list<Integration_Error_Log__c> upErrList = giic_IntegrationCustomMetaDataUtil.deActivateCurrentError(errIdSet, userStory, 'giic_Account__c');
       if(!upErrList.isEmpty()) update upErrList; 
        //update Account records and insert error logs
        if(!accountToUpdate.isEmpty()) update accountToUpdate;
        if(!lstErrors.isEmpty()) insert lstErrors;
        return mapResult;
    }

    //Validate Address of Sales order Staging records
    public static Map<Id , giic_ValShippingAddrFedExController.AddressValidation> validateAddress(List<gii__SalesOrderStaging__c> lstSOStaging){
        Map<Id , giic_ValShippingAddrFedExController.AddressValidation> mapResultAct = new Map<Id , giic_ValShippingAddrFedExController.AddressValidation>();
        Set<String> setActExtrnlIds = new Set<String>();
        
		//Address validation will not be called upon these records
		List<gii__SalesOrderStaging__c> preValidatedRecs = new List<gii__SalesOrderStaging__c>();
		List<gii__SalesOrderStaging__c> toBeValidatedRecs = new List<gii__SalesOrderStaging__c>();
		List<gii__SalesOrderStaging__c> combinedResult = new List<gii__SalesOrderStaging__c>();
        for(gii__SalesOrderStaging__c objSOS : lstSOStaging){
			if(objSOS.giic_DropShip__c){
				setActExtrnlIds.add(objSOS.giic_Account__c);
				objSOS.giic_AddressValidated__c = false;
				toBeValidatedRecs.add(objSOS);
			}else{
				objSOS.giic_AddressValidated__c = true;
				preValidatedRecs.add(objSOS); //Non-dropship records are pre-validated
                giic_ValShippingAddrFedExController.AddressValidation wObj = new giic_ValShippingAddrFedExController.AddressValidation();
                wObj.isValidAddress = true;
                mapResultAct.put(objSOS.Id, wObj);
			}
        }
        Map<String, Account> mapValidatedAccount = new Map<String, Account>();
        for(Account act :[select Id, ShipToCustomerNo__c, giic_AddressType__c, ShippingStreet, ShippingCity, ShippingPostalCode, ShippingCountry from Account where ShipToCustomerNo__c in :setActExtrnlIds and Validated__c = true]){
            mapValidatedAccount.put(act.ShipToCustomerNo__c, act);        
        }
        
        
        //prepare list of address for validation
        List<giic_ValShippingAddrFedExController.AddressToValidateWrapper> lstAddress = new List<giic_ValShippingAddrFedExController.AddressToValidateWrapper>();
        for(gii__SalesOrderStaging__c objSOS : toBeValidatedRecs){
            Boolean matchAddress = false;
            if(objSOS.giic_Account__c != null && mapValidatedAccount.containsKey(objSOS.giic_Account__c)){
                Account objAct = mapValidatedAccount.get(objSOS.giic_Account__c);
                if(objAct.ShippingStreet != null && objSOS.giic_ShipToStreet__c != null && objAct.ShippingStreet.equalsIgnoreCase(objSOS.giic_ShipToStreet__c)
                   && objAct.ShippingCity != null && objSOS.giic_ShipToCity__c != null && objAct.ShippingCity.equalsIgnoreCase(objSOS.giic_ShipToCity__c)
                   && objAct.ShippingPostalCode != null && objSOS.giic_ShipToZipPostalCode__c != null && objAct.ShippingPostalCode.equalsIgnoreCase(objSOS.giic_ShipToZipPostalCode__c)
                   && objAct.ShippingCountry != null && objSOS.giic_ShipToCountry__c != null && objAct.ShippingCountry.equalsIgnoreCase(objSOS.giic_ShipToCountry__c)
                ){
                    matchAddress = true; objSOS.giic_AddressValidated__c = true; objSOS.giic_AddressType__c = objAct.giic_AddressType__c;
                    giic_ValShippingAddrFedExController.AddressValidation wObj = new giic_ValShippingAddrFedExController.AddressValidation();
                    wObj.isValidAddress = true;
                    mapResultAct.put(objSOS.Id, wObj);
                }
            }
            
            giic_ValShippingAddrFedExController.AddressToValidateWrapper objAddress = new giic_ValShippingAddrFedExController.AddressToValidateWrapper();
            objAddress.recordId      = objSOS.Id;
            objAddress.ClientReferenceId = objSOS.giic_Account__c;
            objAddress.StreetLines   = objSOS.giic_ShipToStreet__c;
            objAddress.City          = objSOS.giic_ShipToCity__c;
            objAddress.PostalCode    = objSOS.giic_ShipToZipPostalCode__c;
            objAddress.CountryName   = objSOS.giic_ShipToCountry__c;
            if(matchAddress == false) lstAddress.add(objAddress);
        }
        //initialize Adress to validate and call webservice
        Map<Id , giic_ValShippingAddrFedExController.AddressValidation> mapResult = new Map<Id , giic_ValShippingAddrFedExController.AddressValidation>();
        Map<Id, String> mpAddresssType = new Map<Id, String>();
        if(!lstAddress.isEmpty()){
            giic_FedExAddressValidation.AddressValidationReply response = giic_ValShippingAddrFedExController.processAddressToValidate(lstAddress);
            //prepare result of address validation
            mapResult = giic_ValShippingAddrFedExController.validateAddress(response, lstAddress, mpAddresssType);
    	}
        mapResult.putAll(mapResultAct);
        system.debug('mapResult::' + mapResult); // imp
        List<Integration_Error_Log__c> lstErrors = new List<Integration_Error_Log__c>();
        Set<String> setErrorIds = new Set<String>();
        Set<Id> errIdSet = new Set<Id>();
        String userStory = System.Label.giic_AddressValidation;
        Map<String, String> fieldApiForErrorLog = new Map<String, String>{'giic_SalesOrderStaging__c'=>'Id'};
        for(gii__SalesOrderStaging__c objStaging : toBeValidatedRecs){
            if(objStaging.giic_AddressValidated__c == true) continue;
            
            if(mapResult.containsKey(objStaging.Id) && mapResult.get(objStaging.Id).isValidAddress == true){
                objStaging.giic_AddressValidated__c = true;
                if(mpAddresssType.containsKey(objStaging.Id)) objStaging.giic_AddressType__c = mpAddresssType.get(objStaging.Id);
            }
            else{ 
                 errIdSet.add(objStaging.Id);
                objStaging.giic_Status__c = System.Label.giic_Error;
                String errorMsg = System.Label.giic_ShippingAddInvalid;
                if(mapResult.containsKey(objStaging.Id)){
                    errorMsg += '\n Suggested Address :';
                    errorMsg += '\n Street : ' + mapResult.get(objStaging.Id).suggestedAddress.StreetLines;
                    errorMsg += '\n City : ' + mapResult.get(objStaging.Id).suggestedAddress.City;
                    errorMsg += '\n PostalCode : ' + mapResult.get(objStaging.Id).suggestedAddress.PostalCode;
                    errorMsg += '\n CountryCode : ' + mapResult.get(objStaging.Id).suggestedAddress.CountryCode;
                }
                giic_IntegrationCustomMetaDataUtil.collectErrors(lstErrors, setErrorIds, userStory, fieldApiForErrorLog, objStaging, System.Label.giic_InvalidAddress, errorMsg);
            }
        }
        list<Integration_Error_Log__c> upErrList = giic_IntegrationCustomMetaDataUtil.deActivateCurrentError(errIdSet, userStory, 'giic_SalesOrderStaging__c');
       if(!upErrList.isEmpty()) update upErrList;
        
        //update sales order staging records and insert error logs
        if(!toBeValidatedRecs.isEmpty()) combinedResult.addAll(toBeValidatedRecs);
		if(!preValidatedRecs.isEmpty())  combinedResult.addAll(preValidatedRecs);
		if(!combinedResult.isEmpty()) update combinedResult;
        if(!lstErrors.isEmpty()) insert lstErrors;
        return mapResult;
    }
    
    
    //Method to create the Key product reference id + ' ' + source UoM record id + ' ' + target UoM record id 
    public static String createProductUoMUniqueKey(Id idProductRef, Id idSourceUoM, Id idTargetUoM)
    {
        String strProdUoMKey = '';
        strProdUoMKey = idProductRef + ' ' + idSourceUoM + ' ' + idTargetUoM;
        return strProdUoMKey;
    }
   
   // Method to convert the UOM values (UoM & Decimal)
   public static decimal convertUOMAPI(Id idProductRef, Id idSourceUoM, Id idTargetUoM, decimal quantityForConvert)
    { 
    // set and map for product unit of measure conversion
    Set<String> setUnitofMeasuresUniqueIds = new Set<String>();
    Map<String, gii__ProductUnitofMeasureConversion__c> mapProductUnitofMeasureConversions
     = new Map<String, gii__ProductUnitofMeasureConversion__c>(); 
    
    // create set of unique ids to get product unit of measure conversion factor    
    // Unique Id = Product Reference record Id + ' ' + Record Id for Unit of Measure In + ' ' + Record Id for Unit of Measure Out
    string UMConversionUniqueId = createProductUoMUniqueKey(idProductRef, idSourceUoM, idTargetUoM);
    string UMReverseConversionUniqueId = createProductUoMUniqueKey(idProductRef, idTargetUoM, idSourceUoM);
    // add both of the unique id and the Reverse Unique Id                                                    
    setUnitofMeasuresUniqueIds.add(UMConversionUniqueId);
    setUnitofMeasuresUniqueIds.add(UMReverseConversionUniqueId);
    
    // create map of product unit of measure conversion by utilizing the following method
    mapProductUnitofMeasureConversions = 
    gii.UnitofMeasureHelper.getMapProductUnitofMeasureConversion(setUnitofMeasuresUniqueIds);            
   
    // Create class objects for input and output   
    gii.UnitofMeasureHelper.ProductUnitofMeasureConversionDetail inputPUoMConversionObject 
     = new gii.UnitofMeasureHelper.ProductUnitofMeasureConversionDetail();          
    gii.UnitofMeasureHelper.ProductUnitofMeasureConversionDetail outputPUoMConversionObject 
     = new gii.UnitofMeasureHelper.ProductUnitofMeasureConversionDetail();
    
    // Add record ids for creating unique id of product unit of measure conversion 
    inputPUoMConversionObject.productId = idProductRef;    // Product Reference id
    inputPUoMConversionObject.SourceUnitofMeasureId =idSourceUoM;// Source UM id
    inputPUoMConversionObject.TargetUnitofMeasureId =idTargetUoM; // Target UM id
    inputPUoMConversionObject.quantityToBeConverted = quantityForConvert;  // Quantity To Be Converted
    // specify any rounding decimals - default is set 
    // from the glovia custom setting if none specified
    inputPUoMConversionObject.RoundingDecimals = 2;
    
    outputPUoMConversionObject = gii.UnitofMeasureHelper.getConversionFactorandConvertedQuantity(
    mapProductUnitofMeasureConversions,inputPUoMConversionObject);
    
    system.debug('convertedQuantity=' + outputPUoMConversionObject.convertedQuantity); // imp
    return outputPUoMConversionObject.convertedQuantity;
}
   
    /*
	* Method name : checkForExceptionState 
	* Description : Chek if any of the products of the converted order lies in exception state 
	* Return Type : void 
	* Parameter : Set of Sales Order Ids
	*/
    public static void checkForExceptionState(Set<String> soIds){
        if(soIds.isEmpty()) return; 
        List<gii__SalesOrder__c> soToUpdate = new List<gii__SalesOrder__c>();
        List<Integration_Error_Log__c> lstErrors = new List<Integration_Error_Log__c>();
        Set<String> setErrorIds = new Set<String>();
        String userStory = System.Label.giic_OrderConversion;
        String errorCode = System.Label.giic_ProductExceptionState;
        Map<String, String> fieldApiForErrorLog = new Map<String, String>{'giic_SalesOrder__c'=>'Id'};
        List<gii__SalesOrder__c> lstSO = new List<gii__SalesOrder__c>([SELECT Id, gii__ShipToStateProvince__c, giic_SOStatus__c,
                                                (SELECT Id, gii__Product__r.Name, gii__Product__r.giic_ExceptionState__c, gii__LineStatus__c
                                                 FROM gii__SalesOrder__r) //Added sub-query to check product exception state, if any
                                             FROM gii__SalesOrder__c 
                                             WHERE Id IN :soIds]);
        
        for(gii__SalesOrder__c salesOrder : lstSO){
            if(salesOrder.gii__SalesOrder__r.size()>0){
                for(gii__SalesOrderLine__c soLine : salesOrder.gii__SalesOrder__r){
                    String errorMessage = System.Label.giic_ExceptionStateError;
                        if(soLine.gii__LineStatus__c != giic_Constants.SOLI_CANCEL  && soLine.gii__Product__r.giic_ExceptionState__c != NULL && 
                        soLine.gii__Product__r.giic_ExceptionState__c.contains(salesOrder.gii__ShipToStateProvince__c)){
                            salesOrder.giic_SOStatus__c = System.Label.giic_Draft;
                            soToUpdate.add(salesOrder);
                            errorMessage =+ soLine.gii__Product__r.Name;
                            //public static void collectErrors(List<Integration_Error_Log__c> lstErrors, Set<String> stErrorIds, String userStory, Map<String, String> relatedToFieldAPi, Sobject sourceRecord, String errorCode, String errorMessage)
                            giic_IntegrationCustomMetaDataUtil.collectErrors(lstErrors, setErrorIds, userStory,fieldApiForErrorLog, salesOrder, errorCode, errorMessage );
                        }
                    }
            }                                                     
        }
        try{
            if(!lstErrors.isEmpty()) Database.insert(lstErrors);
            if(!soToUpdate.isEmpty()) Database.update(soToUpdate);
        }
        catch(DmlException e){
            System.debug(e.getMessage());
        }
                                                         
    }
  
}