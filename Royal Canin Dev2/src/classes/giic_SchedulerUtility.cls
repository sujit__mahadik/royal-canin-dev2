/************************************************************************************
Version : 1.0
Created Date : 13 Dec 2018
Function : all common method which is used in OMS application
Author: Abhishek Tripathi
Modification Log :
* Developer : Date : Description
* ----------------------------------------------------------------------------
*
*************************************************************************************/
public class giic_SchedulerUtility {
    /*
* Method name : scheduleBatchJobs
* Description : schedule the batch jobs. It will reshcedule already scheduled jobs based on flag.
* Return Type : void
* Parameter : Integer timeInMinutes, Schedulable batchObject, String jobName, Boolean Reschedule
*/
    public static void scheduleBatchJobs(Integer timeInMinutes, Schedulable batchObject, String jobName, Boolean Reschedule){
        try{
            String query = 'Select Id From CronTrigger WHERE CronJobDetail.Name Like \'%' + jobName + '%\'';
            List<CronTrigger> cronList = Database.query(query);
            
            if(cronList.size() > 0 && cronList != null){
                
                if(Reschedule){
                    for(CronTrigger cT : cronList){
                        System.abortJob(cT.Id);
                    }
                }else{
                    ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, Label.giic_BatchAlreadyScheduled));
                    return;
                }
                
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.INFO, Label.giic_BatchReScheduled));
            }
        }catch(DmlException e){
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR,e.getMessage()));
        }
        
        // Logic to take care if time is below 60 minutes
        if(timeInMinutes < 60){
            DateTime myDateTime = System.Now() ;                
            Integer iHour = myDateTime.hour() ;             
            Integer iMinute = myDateTime.minute() ;                
            Integer iSecond = myDateTime.second() ;                             
            for(Integer i = 0; i < 60 ; i = i + timeInMinutes){ 
                if(iSecond > 59){
                    iSecond = 0 ;
                }  
                if(iMinute > 60-timeInMinutes){
                    iMinute = 0 ;
                    iHour = iHour + 1 ;
                } else{
                    iMinute = iMinute + timeInMinutes ;
                }
                String sExpression = String.valueOf(iSecond) + ' ' + String.valueOf(iMinute) + ' ' + '*' +  ' ' + '*' + ' ' + '*' + ' ' + '?' + ' ' + '*' ;
                jobName =  jobName+i;
                System.schedule(jobName , sExpression, batchObject) ;
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.CONFIRM, Label.giic_BatchScheduled));
            }
        }
        else{ // Logic to tackle when time is in multiples of 60 minutes
            
            String hr = ((timeInMinutes/60) > 0) ? '*/' + String.valueOf(timeInMinutes/60) : '*' ;
            String mnt=(timeInMinutes/60 > 0) ? String.valueOf(Math.mod(timeInMinutes, 60)) : String.valueOf(timeInMinutes);
            String cronExp = '0 '+ mnt +' '+hr+' ? * *';
            System.schedule(jobName , cronExp , batchObject) ;
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.CONFIRM, Label.giic_BatchScheduled));
        }
        
    }
    
    
    /*
* Method name : processUnPaidSO
* Description : Filter SO records for non payment records.
* Return Type : Map<String , List<sObject>>
* Parameter : List<sObject> soList
*/
    public static Map<String , List<sObject>> processUnPaidSO(List<sObject> soList){
        Map<String , List<sObject>> retrunMap = new Map<String , List<sObject>>();
        if(!soList.isEmpty()){
            try{
                List<sObject> validSOList = new List<sObject>();
                String[] orderSources = Label.giic_OrderSource.split(';');
                List<gii__SalesOrderPayment__c> updatedSOPayments = new List<gii__SalesOrderPayment__c>();
                List<Integration_Error_Log__c> lstErrors = new List<Integration_Error_Log__c>();
                Set<String> setErrorIds = new Set<String>();
                String userStory = giic_Constants.USERSTORY_SORELEASE;
                Map<String, String> fieldApiForErrorLog = new Map<String, String>{'giic_SalesOrder__c'=>'Id'};
                    
                    for(sObject soObj : soList){
                        gii__SalesOrder__c so = (gii__SalesOrder__c) soObj;
                        if(!so.gii__SalesOrderPayments__r.isEmpty()){
                            validSOList.add(soObj); // Added SO which have SO payments available.
                            if(orderSources.contains(so.giic_Source__c) && (so.gii__PaymentMethod__c == so.gii__SalesOrderPayments__r[0].gii__PaymentMethod__c) && ((so.gii__SalesOrderPayments__r[0].gii__PaidAmount__c == 0) || so.gii__SalesOrderPayments__r[0].gii__PaidAmount__c == null)){
                                so.gii__SalesOrderPayments__r[0].gii__PaidAmount__c = so.gii__NetAmount__c;
                                updatedSOPayments.add(so.gii__SalesOrderPayments__r[0]);
                            }
                        }else{
                            giic_IntegrationCustomMetaDataUtil.collectErrors(lstErrors, setErrorIds, userStory, fieldApiForErrorLog, soObj,giic_Constants.USERSTORY_SORELEASE, Label.giic_NoPaymentAgainstSO); 
                        }
                    }
                if(!validSOList.isEmpty()) retrunMap.put('validSO', validSOList); // Sales Orders with Payment records.
                if(!lstErrors.isEmpty()) retrunMap.put('lstErrors', lstErrors); // SO with no Payment records.
                if(!updatedSOPayments.isEmpty()) retrunMap.put('sopToUpdate', updatedSOPayments); // Sales Order payment records to update
                
            }catch(Exception ex){
                system.debug('Ex:' + ex.getMessage() + ex.getLineNumber()); 
            }
            
            
        }   
        return retrunMap;
    }
    
    
    
}