/************************************************************************************
Version : 1.0
Created Date : 30 Oct 2018
Function : Apex controller for giic_SOLineItems component
Modification Log : 
* Developer                         Date             Description
* Fujitsu Consulting India      30 Oct 2018             v1.0
* ----------------------------------------------------------------------------
*************************************************************************************/

public with sharing class giic_SOLineItemsController {
    
    
    /* 
    * @description: to get the sales order and the related line items  
    * @param: String
    * @return: Map<String, Object>
    */
    @AuraEnabled
    public static Map<String, Object> getSOLineWithHeader(String soId){
        Map<String, Object> mapResult = new Map<String, Object>();
        List<gii__SalesOrder__c> salesOrderList = [SELECT Id, Name, gii__Warehouse__r.Name, gii__Account__r.Name, gii__OrderDate__c,
                                                    (SELECT Id, Name, gii__Product__r.Name, gii__Product__r.giic_ProductSKU__c, gii__OrderQuantity__c, gii__ShippedQuantity__c
                                                    FROM gii__SalesOrder__r)
                                                  FROM gii__SalesOrder__c
                                                  WHERE Id =: soId];
                                                  
        HeaderInfo header = new HeaderInfo();  //Initialize the wrappers
        List<LineItemInfo> lineItems = new List<LineItemInfo>(); //Initialize the wrappers
        if(salesOrderList != null && salesOrderList.size()>0){
            header.SOName = salesOrderList.get(0).Name;
            header.WHName = salesOrderList.get(0).gii__Warehouse__r.Name;
            header.AccountName = salesOrderList.get(0).gii__Account__r.Name;
            header.OrderDate = salesOrderList.get(0).gii__OrderDate__c;
            mapResult.put('HeaderInfo', header); //Contains the header info
            for(gii__SalesOrderLine__c soLine : salesOrderList.get(0).gii__SalesOrder__r){
                LineItemInfo line = new  LineItemInfo();
                line.SOLName = soLine.Name;
                line.ProdName = soLine.gii__Product__r.Name;
                line.ProdSKU = soLine.gii__Product__r.giic_ProductSKU__c;
                line.OrderQty = soLine.gii__OrderQuantity__c;
                line.ShippedQty = soLine.gii__ShippedQuantity__c;
                
                lineItems.add(line);
            }
            mapResult.put('LineItemInfo', lineItems);  //Contains the list of line item info
        }
                                    
        return mapResult;
    } 
    
    /* 
    * @description: wrapper class for header 
    * @param: NA
    * @return: NA
    */
    
    public class HeaderInfo{
        @AuraEnabled public String SOName;
        @AuraEnabled public String WHName;
        @AuraEnabled public String AccountName;
        @AuraEnabled public Date OrderDate;
        
        public HeaderInfo(){
            this.SOName = '';
            this.WHName = '';
            this.AccountName = '';
            this.OrderDate = null;
            
        }
        
    }
    /* 
    * @description: wrapper class for line item 
    * @param: NA
    * @return: NA
    */
    public class LineItemInfo{
        @AuraEnabled public String SOLName;
        @AuraEnabled public String ProdName;
        @AuraEnabled public String ProdSKU;
        @AuraEnabled public Double OrderQty;
        @AuraEnabled public Double ShippedQty;
        
        public LineItemInfo(){
            this.SOLName = '';
            this.ProdName = '';
            this.ProdSKU = '';
            this.OrderQty = 0;
            this.ShippedQty = 0;
        }
    }

}