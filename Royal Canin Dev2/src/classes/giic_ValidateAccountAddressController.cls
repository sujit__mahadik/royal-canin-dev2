/************************************************************************************
Version : 1.0
Name : giic_ValidateAccountAddressController 
Created Date : 05 ASep 2018
Function : Validate Address
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/

public class giic_ValidateAccountAddressController {
    
    public Account objAct{get; set;}
    public Boolean isError{get; set;}
   
    
    public giic_ValidateAccountAddressController(ApexPages.StandardController controller) {
        objAct = (Account) controller.getRecord();
        isError = false;
    }
    
    public PageReference onLoad(){
        PageReference pObj ;
        try{
            Map<Id , giic_ValShippingAddrFedExController.AddressValidation> mapResult = new Map<Id , giic_ValShippingAddrFedExController.AddressValidation>();
            System.debug(':::objAct=' + objAct);
            List<Account> lstAcnts = [select Id, ShipToCustomerNo__c, ShippingStreet, ShippingCity, ShippingPostalCode, ShippingCountry from Account where id =:objAct.id];
            if(!lstAcnts.isEmpty()){
                mapResult = giic_IntegrationCommonUtil.validateAccountAddressAddress(lstAcnts);
            }
            system.debug('  mapResult--->>>> ' + mapResult);
            if(mapResult.containsKey(objAct.Id) && mapResult.get(objAct.Id).isValidAddress == true){                
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM, System.Label.giic_AddressRevalidated));
                return null;
            }else{
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, System.Label.giic_AddressValidationFailed));
                isError = true;
                if(mapResult.containsKey(objAct.Id)){
                    objAct.ShippingStreet = mapResult.get(objAct.Id).suggestedAddress.StreetLines;
                    objAct.ShippingCity = mapResult.get(objAct.Id).suggestedAddress.City;
                    objAct.ShippingPostalCode =  mapResult.get(objAct.Id).suggestedAddress.PostalCode;
                    objAct.ShippingCountry = mapResult.get(objAct.Id).suggestedAddress.CountryCode;  
                }else{
                    objAct.ShippingStreet = '';
                    objAct.ShippingCity = '';
                    objAct.ShippingPostalCode =  '';
                    objAct.ShippingCountry = '';  
                }
                return null;
            }
        }
        catch(Exception ex){ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getMessage())); return null;}
    }
    
    public PageReference goBack(){
        PageReference pObj ;
        try{          
            pObj = new PageReference('/' + objAct.id); // redirect to sales order Staging record
            pObj.setRedirect(true);            
        }catch(Exception ex){ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getMessage())); return null; }
        return pObj;
    }
    
    public PageReference applySuggestedAddress(){
        PageReference pObj ;
        try{
            update objAct;          
            pObj = new PageReference('/' + objAct.id); // redirect to sales order Staging record
            pObj.setRedirect(true);            
        }catch(Exception ex){ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getMessage())); return null; }
        return pObj;
    }
}