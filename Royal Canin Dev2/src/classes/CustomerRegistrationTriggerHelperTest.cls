/*
 * Date:             - Developer, Company                          - description
 * 2015-08-05        - Stevie Yakkel, Acumen Solutions             - Edited
 */
 
@isTest
private class CustomerRegistrationTriggerHelperTest
{
    
   @isTest
    static void testpullShippingAddressFromAccountForCFP() {
        SetupTestData.createCustomSettings();
        SetupTestData.createAccounts(1);
        Customer_Registration__c reg = new Customer_Registration__c ();      
        reg.Name_of_Business_Organization__c = 'Test';
        reg.First_Name__c = 'Convert';
        reg.Last_Name__c = 'Feeding';reg.ClinicCustomerNo__c = 'VET-541389-001';
        reg.Shipping_Street_Address1__c  = 'Test123';
        reg.Shipping_Street_Address2__c = 'Test123';
        reg.Shipping_City__c = 'Test123';
        reg.Ship_To_State__c = 'OH';
        reg.Shipping_Zip_Code__c = '44133';
        reg.Email_Address__c = 'syakkel@acumensolutions.com';
        reg.Customer_Type__c = 'Clinic Feeding Participant';
        reg.ClinicCustomerNo__c = '1';
        insert reg;
        
        Account acc = SetupTestData.testAccounts[0];
        acc.Name = 'acc';
        acc.Payment_Method__c = 'Credit/Debit Card';
        acc.Bill_To_Customer_ID__c = '1';
        acc.Customer_Type__c = 'Clinic Feeding Participant';
        acc.Statement_Type_Preference__c = 'Electronic';
        acc.recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Veterinary').getRecordTypeId();
        acc.ShipToCustomerNo__c = '1';
        update acc;
        
        reg.Application_Status__c = 'Processed';
        Test.startTest();
        update reg;
        Test.stopTest();
    } 
    
    @isTest
    static void testConvertFeeding() {
        SetupTestData.createCustomSettings();
        Customer_Registration__c reg = new Customer_Registration__c ();
        reg.Name_of_Business_Organization__c = 'Test';
        reg.First_Name__c = 'Convert';
        reg.Last_Name__c = 'Feeding';
        reg.Billing_Street_Address1__c = 'Test123'; 
        reg.Billing_Street_Address2__c = 'Test123';
        reg.Billing_City__c = 'Test123';
        reg.Bill_To_State__c = 'OH';
        reg.Billing_Zip_Code__c = '44133';
        reg.Shipping_Street_Address1__c  = 'Test123';
        reg.Shipping_Street_Address2__c = 'Test123';
        reg.Shipping_City__c = 'Test123';
        reg.Ship_To_State__c = 'OH';
        reg.Shipping_Zip_Code__c = '44133';
        reg.Phone_Number__c = '(123) 456-7890';
        reg.Email_Address__c = 'syakkel@acumensolutions.com';
        reg.Customer_Type__c = 'Clinic Feeding';
        reg.RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Customer_Registration__c' AND DeveloperName = 'Clinic_Feeding'].Id;
       
        insert reg;
        reg.Application_Status__c = 'Processed';
        Test.startTest();
        update reg;
        Test.stopTest();
    }
    
      @isTest
    static void testConvertSellTos() {
        SetupTestData.createCustomSettings();
        Customer_Registration__c reg = new Customer_Registration__c ();
        reg.Name_of_Business_Organization__c = 'Test';
        reg.First_Name__c = 'Convert';
        reg.Last_Name__c = 'SellTo';
        reg.Billing_Street_Address1__c = 'Test123'; 
        reg.Billing_Street_Address2__c = 'Test123';
        reg.Billing_City__c = 'Test123';
        reg.Bill_To_State__c = 'OH';
        reg.Billing_Zip_Code__c = '44133';
        reg.Shipping_Street_Address1__c  = 'Test123';
        reg.Shipping_Street_Address2__c = 'Test123';
        reg.Shipping_City__c = 'Test123';
        reg.Ship_To_State__c = 'OH';
        reg.Shipping_Zip_Code__c = '44133';
        reg.Phone_Number__c = '(123) 456-7890';
        reg.Email_Address__c = 'syakkel@acumensolutions.com';
        reg.Customer_Type__c = 'VET-Sell To';
        reg.RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Customer_Registration__c' AND DeveloperName = 'Vet_Sell_To'].Id;
       
        insert reg;
        reg.Application_Status__c = 'Processed';
        Test.startTest();
        update reg;
        Test.stopTest();
    }
    
    
    
    @isTest
    static void testConvertEmployee() {
        SetupTestData.createCustomSettings();
        Customer_Registration__c reg = new Customer_Registration__c ();
        reg.Name_of_Business_Organization__c = 'Test';
        reg.First_Name__c = 'Convert';
        reg.Last_Name__c = 'Feeding';
        reg.Billing_Street_Address1__c = 'Test123'; 
        reg.Billing_Street_Address2__c = 'Test123';
        reg.Billing_City__c = 'Test123';
        reg.Bill_To_State__c = 'OH';
        reg.Billing_Zip_Code__c = '44133';
        reg.Shipping_Street_Address1__c  = 'Test123';
        reg.Shipping_Street_Address2__c = 'Test123';
        reg.Shipping_City__c = 'Test123';
        reg.Ship_To_State__c = 'OH';
        reg.Shipping_Zip_Code__c = '44133';
        reg.Phone_Number__c = '(123) 456-7890';
        reg.Email_Address__c = 'syakkel@acumensolutions.com';
        reg.Customer_Type__c = 'RC Associate';
        reg.RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Customer_Registration__c' AND DeveloperName = 'RC_Associate'].Id;
        insert reg;
        reg.Application_Status__c = 'Processed';
        Test.startTest();
        update reg;
        Test.stopTest();
    }

     @isTest
    static void testConvertBreeder() {
        SetupTestData.createCustomSettings();
        Customer_Registration__c reg = new Customer_Registration__c ();
        reg.Name_of_Business_Organization__c = 'Test';
        reg.First_Name__c = 'Testtttt';
        reg.Last_Name__c = 'Testerrrr';
        reg.Billing_Street_Address1__c = 'Test123'; 
        reg.Billing_Street_Address2__c = 'Test123';
        reg.Billing_City__c = 'Test123';
        reg.Bill_To_State__c = 'OH';
        reg.Billing_Zip_Code__c = '44133';
        reg.Shipping_Street_Address1__c  = 'Test123';
        reg.Shipping_Street_Address2__c = 'Test123';
        reg.Shipping_City__c = 'Test123';
        reg.Ship_To_State__c = 'OH';
        reg.Shipping_Zip_Code__c = '44133';
        reg.Phone_Number__c = '(123) 456-7890';
        reg.Email_Address__c = 'syakkel@acumensolutions.com';
        reg.Customer_Type__c = 'Cat Breeder';
        reg.Number_of_Litters__c = 1;
        reg.RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Customer_Registration__c' AND DeveloperName = 'Cat_Breeder'].Id;
        insert reg;
        reg.Application_Status__c = 'Processed';
        Test.startTest();
        update reg;
        Test.stopTest();
    }


    @isTest
    static void testCreateCustomEncryptionKey(){
        SetupTestData.createCustomSettings();

        Test.startTest();
        String urlStub = Global_Parameters__c.getValues('ApplicationURLStub').Value__c;
        Blob privateKey = Blob.valueOf(Global_Parameters__c.getValues('EncryptionKey').Value__c);

        Schema.DescribeSObjectResult rcSchema = Schema.SObjectType.Customer_Registration__c;
        Map<String,Schema.RecordTypeInfo> CustomerRegistrationRecordTypeInfo = rcSchema.getRecordTypeInfosByName();
        
        Customer_Registration__c testCustReg = new Customer_Registration__c();
        testCustReg.RecordTypeId = CustomerRegistrationRecordTypeInfo.get('Shelter').getRecordTypeId();
        testCustReg.First_Name__c = 'TestFirstName';
        testCustReg.Last_Name__c = 'TestLastName';
        testCustReg.Email_Address__c = 'test@test.com';
        testCustReg.Customer_Account_Id__c = '123456789';
        testCustReg.Billing_Street_Address1__c = '1234 High Street';
        testCustReg.Billing_Street_Address2__c = 'Apt. 1A';
        testCustReg.Billing_City__c = 'New York';
        testCustReg.Bill_To_State__c = 'NY';
        testCustReg.Billing_Zip_Code__c = '99999';
        testCustReg.Payment_Options__c = 'Credit/Debit Card';
        insert testCustReg;

        Customer_Registration__c newCustReg = new Customer_Registration__c();
        newCustReg = [Select Id, Application_ID__c, Link_to_Application__c From Customer_Registration__c Where Id =: testCustReg.Id];
        Test.stopTest();

        System.assertNotEquals(null, newCustReg.Application_ID__c);
        System.assertNotEquals(null, newCustReg.Link_to_Application__c);
    }

    @isTest
    static void testCheckForAddressChanges(){
        SetupTestData.createCustomSettings();
        Schema.DescribeSObjectResult rcSchema = Schema.SObjectType.Customer_Registration__c;
        Map<String,Schema.RecordTypeInfo> CustomerRegistrationRecordTypeInfo = rcSchema.getRecordTypeInfosByName();
        
        Customer_Registration__c testCustReg = new Customer_Registration__c();
        testCustReg.RecordTypeId = CustomerRegistrationRecordTypeInfo.get('Shelter').getRecordTypeId();
        testCustReg.First_Name__c = 'TestFirstName';
        testCustReg.Last_Name__c = 'TestLastName';
        testCustReg.Email_Address__c = 'test@test.com';
        testCustReg.Customer_Account_Id__c = '123456789';
        testCustReg.Payment_Options__c = 'Credit/Debit Card';
        insert testCustReg;

        testCustReg = [Select Id, First_Name__c, Last_Name__c, Email_Address__c, Billing_Street_Address1__c, Billing_Street_Address2__c, Billing_City__c, Bill_To_State__c, Billing_Zip_Code__c, Validated__c, Validation_Error_Reason__c, Pending_Avalara_Response__c From Customer_Registration__c Where First_Name__c = 'TestFirstName' limit 1];

        //verify nothing is happening with Avalara when Address fields aren't changed
        Test.startTest();
        testCustReg.Email_Address__c = 'newTest@test.com';
        update testCustReg;
        Test.stopTest();

        testCustReg = [Select Id, First_Name__c, Last_Name__c, Email_Address__c, Billing_Street_Address1__c, Billing_Street_Address2__c, Billing_City__c, Bill_To_State__c, Billing_Zip_Code__c, Validated__c, Validation_Error_Reason__c, Pending_Avalara_Response__c From Customer_Registration__c Where First_Name__c = 'TestFirstName' limit 1];
        System.assertEquals(false, testCustReg.Pending_Avalara_Response__c);
        System.assertEquals(false, testCustReg.Validated__c);
        System.assertEquals(null, testCustReg.Validation_Error_Reason__c);
    }

    @isTest
    static void testCheckForSubmission(){
        SetupTestData.createCustomSettings();
        
        Test.startTest();
        Schema.DescribeSObjectResult rcSchema = Schema.SObjectType.Customer_Registration__c;
        Map<String,Schema.RecordTypeInfo> CustomerRegistrationRecordTypeInfo = rcSchema.getRecordTypeInfosByName();
        
        Customer_Registration__c testCustReg = new Customer_Registration__c();
        testCustReg.RecordTypeId = CustomerRegistrationRecordTypeInfo.get('Dog Breeder').getRecordTypeId();
        testCustReg.First_Name__c = 'TestFirstName';
        testCustReg.Last_Name__c = 'TestLastName';
        testCustReg.Email_Address__c = 'test@test.com';
        testCustReg.Customer_Account_Id__c = '123456789';
        testCustReg.Application_Status__c = 'Open';
        testCustReg.Terms_and_Conditions_Accepted__c = true;
        insert testCustReg;
        testCustReg = [Select Id, Application_Status__c From Customer_Registration__c Where First_Name__c = 'TestFirstName' limit 1];
        testCustReg.Application_Status__c = 'Submitted';
        update testCustReg;
        Test.stopTest();

        Customer_Registration__c checkCustReg = [Select (Select Id From ProcessInstances Order By CreatedDate DESC Limit 1) From Customer_Registration__c Where Id =: testCustReg.Id];
        String processId = checkCustReg.ProcessInstances[0].Id;
        ProcessInstance pi = [Select (Select Id, StepStatus From Steps Order By CreatedDate DESC Limit 1) From ProcessInstance Where Id =: processId Order By CreatedDate DESC limit 1];
        //System.assertEquals('Started',pi.Steps[0].StepStatus);
    }

    @isTest
    static void testRequireRejectionComment(){
        SetupTestData.createCustomSettings();
        
        Test.startTest();
        Schema.DescribeSObjectResult rcSchema = Schema.SObjectType.Customer_Registration__c;
        Map<String,Schema.RecordTypeInfo> CustomerRegistrationRecordTypeInfo = rcSchema.getRecordTypeInfosByName();
        
        Customer_Registration__c testCustReg = new Customer_Registration__c();
        testCustReg.RecordTypeId = CustomerRegistrationRecordTypeInfo.get('Retailer').getRecordTypeId();
        testCustReg.First_Name__c = 'TestFirstName';
        testCustReg.Last_Name__c = 'TestLastName';
        testCustReg.Email_Address__c = 'test@test.com';
        testCustReg.Customer_Account_Id__c = '123456789';
        testCustReg.Application_Status__c = 'Open';
        insert testCustReg;

        if([select count() from ProcessInstance where targetobjectid=:testCustReg.id] < 1)
        {       
            Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
            req.setComments('Approve.');
            req.setNextApproverIds(new Id[] {UserInfo.getUserId()});
            req.setObjectId(testCustReg.Id);

            //Submit the approval request
            Approval.ProcessResult result = Approval.process(req);

        }

        Customer_Registration__c testCustRetrievedReg = new Customer_Registration__c();
        testCustRetrievedReg = [Select Id, Application_Status__c From Customer_Registration__c Where Id =: testCustReg.Id];
        testCustRetrievedReg.Upload_Complete_Resale__c  = true;
        testCustRetrievedReg.Application_Status__c = 'Rejected';
        update testCustRetrievedReg;

        Test.stopTest();

        System.assert(ApexPages.getMessages().size() == 0);
    }

    @isTest
    static void testCreateAccount(){
        Customer_Registration__c reg = new Customer_Registration__c ();
        reg.Name_of_Business_Organization__c = 'Test';
        reg.Billing_Street_Address1__c = 'Test'; 
        reg.Billing_Street_Address2__c = 'Test';
        reg.Billing_City__c = 'Test';
        reg.Bill_To_State__c = 'OH';
        reg.Billing_Zip_Code__c = 'Test';
        reg.Shipping_Street_Address1__c  = 'Test';
        reg.Shipping_Street_Address2__c = 'Test';
        reg.Shipping_City__c = 'Test';
        reg.Ship_To_State__c = 'OH';
        reg.Shipping_Zip_Code__c = 'Test';
        reg.Phone_Number__c = 'Test';
        reg.Website__c = 'Test';

        reg.Affiliations__c = 'Test';
        reg.Average_pet_adoptions_per_month__c = 2;
        reg.Average_pets_fed_per_month__c = 2;
        reg.Customer_Account_ID__c = 'Test';
        reg.Breed_Type__c = 'Test';
        reg.ClinicCustomerNo__c = 'Test';
        reg.Customer_Type__c = 'Test';
        reg.Dept_Store_Number__c = 'Test';
        reg.Estimated_Monthly_Purchases__c = 'Test';
        reg.Food_brands__c = 'Test';
        reg.I_also_provide_the_following_services__c = 'Test';
        reg.Insite_Customer_Type__c = 'Test';
        reg.Number_of_Adults__c = 2;
        reg.Number_of_Litters__c = 2;
        reg.Other_Affiliation__c = 'Test';
        reg.Other_Food__c = 'Test';
        reg.Other__c = 'Test';
        reg.Payment_Options__c = 'Test';
        reg.Pending_Avalara_Response__c = true;
        reg.Pet_Participation_Activities__c = 'Test';
        reg.Resale_Tax_Exempt_Status_ExpirationDate__c = Date.today();
        reg.Resale_Sales_Tax_Exemption_ID__c = 'Test';
        reg.ShipToCustomerNo__c = 'Test';
        reg.Statement_Option_Type__c = 'Test';
        reg.Tax_ID__c = 'Test';
        reg.Type_of_Facility__c = 'Test';
        reg.Validated__c = false;
        reg.Working_dog_type__c = 'Test';

        Account newAcc = new Account();
        Boolean accExists = false;
        Test.startTest();
        newAcc = CustomerRegistrationTriggerHelper.populateAccount(reg, newAcc, accExists);
        Test.stopTest();
    }

    @isTest
    static void testSetRegionAndDistrict(){
        SetupTestData.createCustomSettings();
        
        Test.startTest();
        Schema.DescribeSObjectResult rcSchema = Schema.SObjectType.Customer_Registration__c;
        Map<String,Schema.RecordTypeInfo> CustomerRegistrationRecordTypeInfo = rcSchema.getRecordTypeInfosByName();
        
        PostalCode__c postal = new PostalCode__c();
        postal.ZipCode__c = '55555';
        insert postal;

        Customer_Registration__c testCustReg = new Customer_Registration__c();
        testCustReg.RecordTypeId = CustomerRegistrationRecordTypeInfo.get('Retailer').getRecordTypeId();
        testCustReg.First_Name__c = 'TestFirstName';
        testCustReg.Last_Name__c = 'TestLastName';
        testCustReg.Email_Address__c = 'test@test.com';
        testCustReg.Customer_Account_Id__c = '123456789';
        testCustReg.Application_Status__c = 'Open';
        testCustReg.Shipping_Zip_Code__c = '12345';
        testCustReg.Related_Postal_Code__c = postal.Id;
        insert testCustReg;

        Customer_Registration__c testCustRetrievedReg = new Customer_Registration__c();
        testCustRetrievedReg = [Select Id, Application_Status__c, Shipping_Zip_Code__c From Customer_Registration__c Where Id =: testCustReg.Id];
        testCustRetrievedReg.Shipping_Zip_Code__c = '55555';
        update testCustRetrievedReg;

        Test.stopTest();

        Customer_Registration__c custRetrievedReg = new Customer_Registration__c();
        custRetrievedReg = [Select Id, Application_Status__c, Related_Postal_Code__c From Customer_Registration__c Where Id =: testCustRetrievedReg.Id];

        System.assertEquals(null, custRetrievedReg.Related_Postal_Code__c);
    }

    @isTest
    static void testVerifyRCAssociate() {
        SetupTestData.createCustomSettings();
        Customer_Registration__c reg = new Customer_Registration__c ();
        reg.Name_of_Business_Organization__c = 'Test';
        reg.First_Name__c = 'verify';
        reg.Last_Name__c = 'employee';
        reg.Billing_Street_Address1__c = 'Test123'; 
        reg.Billing_Street_Address2__c = 'Test123';
        reg.Billing_City__c = 'Test123';
        reg.Bill_To_State__c = 'OH';
        reg.Billing_Zip_Code__c = '44133';
        reg.Shipping_Street_Address1__c  = 'Test123';
        reg.Shipping_Street_Address2__c = 'Test123';
        reg.Shipping_City__c = 'Test123';
        reg.Ship_To_State__c = 'OH';
        reg.Shipping_Zip_Code__c = '44133';
        reg.Phone_Number__c = '(123) 456-7890';
        reg.Email_Address__c = 'syakkel@acumensolutions.com';
        reg.RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Customer_Registration__c' AND DeveloperName = 'RC_Associate'].Id;
        reg.Customer_Type__c = 'RC Associate';
        reg.User_Logon__c = 'userlogon1';
        reg.Dept_Store_Number__c = '1';
        reg.Terms_and_Conditions_Accepted__c = true;
        insert reg;
        reg.Application_Status__c = 'Submitted';
        reg.Upload_Complete_Resale__c = true;

        Employee__c regEmployee = new Employee__c();
        regEmployee.UserLogon__c = 'userlogon1';
        regEmployee.CostCenter__c = '1';
        regEmployee.FirstName__c = 'verify';
        regEmployee.LastName__c = 'employee';
        regEmployee.Zip_Code__c = '44133';
        insert regEmployee;
        Test.startTest();
        reg.Dept_Store_Number__c = '12';
        regEmployee.CostCenter__c = '12';
        update regEmployee;
        update reg;
        
        Test.stopTest();
    }

    @isTest
    static void testVerifyBanfieldEmployee() {
        SetupTestData.createCustomSettings();
        Customer_Registration__c reg = new Customer_Registration__c ();
        reg.Name_of_Business_Organization__c = 'Test';
        reg.First_Name__c = 'ver';
        reg.Last_Name__c = 'employee';
        reg.Billing_Street_Address1__c = 'Test123'; 
        reg.Billing_Street_Address2__c = 'Test123';
        reg.Billing_City__c = 'Test123';
        reg.Bill_To_State__c = 'OH';
        reg.Billing_Zip_Code__c = '44133';
        reg.Shipping_Street_Address1__c  = 'Test123';
        reg.Shipping_Street_Address2__c = 'Test123';
        reg.Shipping_City__c = 'Test123';
        reg.Ship_To_State__c = 'OH';
        reg.Shipping_Zip_Code__c = '44133';
        reg.Phone_Number__c = '(123) 456-7890';
        reg.Email_Address__c = 'syakkel@acumensolutions.com';
        reg.RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Customer_Registration__c' AND DeveloperName = 'Banfield_Associate'].Id;
        reg.Customer_Type__c = 'Banfield Associate';
        reg.User_Logon__c = 'userlogon2';
        reg.Dept_Store_Number__c = '1234';
        reg.Terms_and_Conditions_Accepted__c = true;
        insert reg;
        reg.Application_Status__c = 'Submitted';
        reg.Upload_Complete_Resale__c = true;

        Employee__c regEmployee = new Employee__c();
        regEmployee.UserLogon__c = 'userlogon2';
        regEmployee.Location_Code__c = 'abc1234';
        regEmployee.FirstName__c = 'ver';
        regEmployee.LastName__c = 'employee';
        regEmployee.Zip_Code__c = '44133';  
        insert regEmployee;
        Test.startTest();
        reg.Dept_Store_Number__c = '12';
        regEmployee.Location_Code__c = '12';
        update regEmployee;
        
        try{
        
        update reg;
        
        } catch(Exception E) { }
        
        Test.stopTest();
    }

    @isTest
    static void testUpdatePaymentMethodOnAdditionalSellTo() {
        SetupTestData.createCustomSettings();
        SetupTestData.createAccounts(1);
        Account acc = SetupTestData.testAccounts[0];
        acc.Name = 'acc';
        acc.Payment_Method__c = 'Credit/Debit Card';
        acc.Bill_To_Customer_ID__c = '1';
        acc.Customer_Type__c = 'Retailer';
        acc.Statement_Type_Preference__c = 'Electronic';
        acc.ShipToCustomerNo__c = '1';
        update acc;

        Customer_Registration__c reg = new Customer_Registration__c ();
        reg.Name_of_Business_Organization__c = 'Test';
        reg.First_Name__c = 'test';
        reg.Last_Name__c = 'reg';
        reg.Billing_Street_Address1__c = 'Test123'; 
        reg.Billing_Street_Address2__c = 'Test123';
        reg.Billing_City__c = 'Test123';
        reg.Bill_To_State__c = 'OH';
        reg.Billing_Zip_Code__c = '44133';
        reg.Shipping_Street_Address1__c  = 'Test123';
        reg.Shipping_Street_Address2__c = 'Test123';
        reg.Shipping_City__c = 'Test123';
        reg.Ship_To_State__c = 'OH';
        reg.Shipping_Zip_Code__c = '44133';
        reg.Phone_Number__c = '(123) 456-7890';
        reg.Email_Address__c = 'syakkel@acumensolutions.com';
        reg.Customer_Type__c = 'RET-Sell To';
        insert reg;
        reg.Customer_Account_ID__c = '1';
        Test.startTest();
        update reg;
        Test.stopTest();

        Customer_Registration__c reg2 = [SELECT Payment_options__c, Statement_Option_Type__c, Billing_Street_Address1__c, Billing_City__c, Bill_To_State__c, Billing_Zip_Code__c, ShipToCustomerNo__c FROM Customer_Registration__c WHERE Id=:reg.Id];

        System.assertEquals(reg2.Payment_options__c, acc.Payment_Method__c);
        System.assertEquals(reg2.Statement_Option_Type__c, acc.Statement_Type_Preference__c);
        System.assertEquals(reg2.Billing_Street_Address1__c, acc.BillingStreet);
        System.assertEquals(reg2.Billing_City__c, acc.BillingCity);
        System.assertEquals(reg2.Bill_To_State__c, acc.BillingState);
        System.assertEquals(reg2.Billing_Zip_Code__c, acc.BillingPostalCode);
      //  System.assertEquals(reg2.ShipToCustomerNo__c, acc.ShipToCustomerNo__c);
                
    }

    @isTest
    static void testCheckDatesInFuture() {
        SetupTestData.createCustomSettings();
        Customer_Registration__c reg = new Customer_Registration__c ();
        reg.Name_of_Business_Organization__c = 'Test';
        reg.First_Name__c = 'check';
        reg.Last_Name__c = 'date';
        reg.Billing_Street_Address1__c = 'Test123'; 
        reg.Billing_Street_Address2__c = 'Test123';
        reg.Billing_City__c = 'Test123';
        reg.Bill_To_State__c = 'OH';
        reg.Billing_Zip_Code__c = '44133';
        reg.Shipping_Street_Address1__c  = 'Test123';
        reg.Shipping_Street_Address2__c = 'Test123';
        reg.Shipping_City__c = 'Test123';
        reg.Ship_To_State__c = 'OH';
        reg.Shipping_Zip_Code__c = '44133';
        reg.Phone_Number__c = '(123) 456-7890';
        reg.Email_Address__c = 'syakkel@acumensolutions.com';
        reg.DVM_Vet_License_Exp_Date__c = Date.today().addDays(3);
        reg.Resale_Tax_Exempt_Status_ExpirationDate__c = Date.today().addDays(3);
        reg.Resale_Sales_Tax_Exemption_ID__c = '99-9999999';
        reg.Check_if_you_are_Tax_Exempt__c = true;

        insert reg;
        reg.DVM_Vet_License_Exp_Date__c = Date.newInstance(2015, 7, 30);
        reg.Resale_Tax_Exempt_Status_ExpirationDate__c = Date.newInstance(2015, 7, 30);
        reg.Application_Status__c = 'Approved';

        try {
            Test.startTest();
            update reg;
            Test.stopTest();
        } catch(Exception e) {
            boolean expectedException;
            if(e.getMessage().contains('Please enter a date in the future'))
                expectedException = true;
            else
                expectedException = false;
            System.assert(expectedException);
        }
    }
        
    @isTest(seealldata = true)
    static void testsendRejectedEmails() {
        EmailTemplate template = [select Id, Subject, HtmlValue from EmailTemplate where name = 'Customer_Registration_Rejected_Email' LIMIT 1];
        EmailTemplate template_Processed = [select Id, Subject, HtmlValue from EmailTemplate where name = 'Customer_Registration_Processed_Email' LIMIT 1];
        Recordtype rtt = [SELECT Id,name FROM RecordType WHERE SObjectType='Customer_Registration__c' and name='Retailer' Limit 1];
        User ussss = [SELECT Id,name FROM user WHERE email='bethi.reddy.external@royalcanin.com' Limit 1];
        //SetupTestData.createChannelRelatedCustomSettings();
        Map<String,channel_related_recordtypes__c> channelRecordTypes = channel_related_recordtypes__c.getAll();
        SetupTestData.createAccounts(2);
        Account acc = SetupTestData.testAccounts[0];
        acc.Name = 'acc';
        acc.Payment_Method__c = 'Credit/Debit Card';
        acc.Bill_To_Customer_ID__c = '1';
        acc.Customer_Type__c = 'Retailer';
        acc.Statement_Type_Preference__c = 'Electronic';
        acc.ShipToCustomerNo__c = '1';
     
        update acc;

        Customer_Registration__c reg = new Customer_Registration__c ();
        reg.Name_of_Business_Organization__c = 'Test';
        reg.First_Name__c = 'test';
        reg.Last_Name__c = 'reg';
        reg.Billing_Street_Address1__c = 'Test123'; 
        reg.Billing_Street_Address2__c = 'Test123';
        reg.Billing_City__c = 'Test123';
        reg.Bill_To_State__c = 'OH';
        reg.Billing_Zip_Code__c = '44133';
        reg.Shipping_Street_Address1__c  = 'Test123';
        reg.Shipping_Street_Address2__c = 'Test123';
        reg.Shipping_City__c = 'Test123';
        reg.Ship_To_State__c = 'OH';
        reg.Shipping_Zip_Code__c = '44133';
        reg.Phone_Number__c = '(123) 456-7890';
        reg.Email_Address__c = 'syakkel@acumensolutions.com';
        reg.Customer_Type__c = 'RET-Sell To';
        reg.recordtypeId=rtt.Id;
        reg.ownerId = ussss.Id;
                
        insert reg;
               
        reg.Application_Status__c = 'Processed';
        reg.Customer_Account_ID__c = '1';
        
        //update reg;
        
        Customer_Registration__c reg3 = new Customer_Registration__c ();
        reg3.Name_of_Business_Organization__c = 'Test';
        reg3.First_Name__c = 'test';
        reg3.Last_Name__c = 'reg';
        reg3.Billing_Street_Address1__c = 'Test123'; 
        reg3.Billing_Street_Address2__c = 'Test123';
        reg3.Billing_City__c = 'Test123';
        reg3.Bill_To_State__c = 'OH';
        reg3.Billing_Zip_Code__c = '44133';
        reg3.Shipping_Street_Address1__c  = 'Test123';
        reg3.Shipping_Street_Address2__c = 'Test123';
        reg3.Shipping_City__c = 'Test123';
        reg3.Ship_To_State__c = 'OH';
        reg3.Shipping_Zip_Code__c = '44133';
        reg3.Phone_Number__c = '(123) 456-7890';
        reg3.Email_Address__c = 'syakkel@acumensolutions.com';
        reg3.Customer_Type__c = 'RET-Sell To';
        reg3.Application_Status__c = 'Processed';
        reg3.recordtypeId=rtt.Id;
        reg3.ownerId = ussss.Id;
        
        insert reg3;
    } 
    
    /*
    
    @isTest
    static void testvalidateRetailer() {
        SetupTestData.createCustomSettings();
        SetupTestData.createAccounts(1);
        Account acc = SetupTestData.testAccounts[0];
        acc.Name = 'test';
        acc.Payment_Method__c = 'Credit/Debit Card';
        acc.Bill_To_Customer_ID__c = '1';
        acc.Customer_Type__c = 'Retailer';
        acc.Statement_Type_Preference__c = 'Electronic';
        acc.ShipToCustomerNo__c = '1';
        acc.Phone = '(123) 456-7890';
        acc.Do_Not_Use__c = false;
        acc.Account_Status__c = 'Prospect';
        acc.recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Retailer').getRecordTypeId();
        update acc;

        Customer_Registration__c reg = new Customer_Registration__c ();
        reg.Name_of_Business_Organization__c = 'Test';
        reg.First_Name__c = 'test';
        reg.Last_Name__c = 'reg';
        reg.Billing_Street_Address1__c = 'Test123'; 
        reg.Billing_Street_Address2__c = 'Test123';
        reg.Billing_City__c = 'Test123';
        reg.Bill_To_State__c = 'OH';
        reg.Billing_Zip_Code__c = '44133';
        reg.Shipping_Street_Address1__c  = 'Test123';
        reg.Shipping_Street_Address2__c = 'Test123';
        reg.Shipping_City__c = 'Test123';
        reg.Ship_To_State__c = 'OH';
        reg.Shipping_Zip_Code__c = '44133';
        reg.Phone_Number__c = '(123) 456-7890';
        reg.Email_Address__c = 'syakkel@acumensolutions.com';
        reg.Customer_Type__c = 'Retailer';
        reg.recordTypeId = Schema.SObjectType.Customer_Registration__c.getRecordTypeInfosByName().get('Retailer').getRecordTypeId();
        insert reg;
        reg.Customer_Account_ID__c = '1';
        reg.Application_Status__c = 'Processed';
        Test.startTest();
        update reg;
        
        acc.Bill_To_Customer_ID__c = null;
        acc.ShipToCustomerNo__c = null;
        update acc;
        reg.Application_Status__c = 'Prospect';
        update reg;
        reg.customer_type__c= 'Veternary';
        reg.Application_Status__c = 'Processed';
        update reg;
        Test.stopTest();  */

        /*Customer_Registration__c reg2 = [SELECT Payment_options__c, Statement_Option_Type__c, Billing_Street_Address1__c, Billing_City__c, Bill_To_State__c, Billing_Zip_Code__c, ShipToCustomerNo__c FROM Customer_Registration__c WHERE Id=:reg.Id];

        System.assertEquals(reg2.Payment_options__c, acc.Payment_Method__c);
        System.assertEquals(reg2.Statement_Option_Type__c, acc.Statement_Type_Preference__c);
        System.assertEquals(reg2.Billing_Street_Address1__c, acc.BillingStreet);
        System.assertEquals(reg2.Billing_City__c, acc.BillingCity);
        System.assertEquals(reg2.Bill_To_State__c, acc.BillingState);
        System.assertEquals(reg2.Billing_Zip_Code__c, acc.BillingPostalCode);*/
      //  System.assertEquals(reg2.ShipToCustomerNo__c, acc.ShipToCustomerNo__c);
                
  //  }
    
    @isTest
    static void testClinicFeeding() {
        SetupTestData.createCustomSettings();
        SetupTestData.createAccounts(2);
        Account acc = SetupTestData.testAccounts[0];
        acc.Name = 'test';
        acc.Payment_Method__c = 'Credit/Debit Card';
        acc.Bill_To_Customer_ID__c = '1';
        acc.Customer_Type__c = 'Veterinary';
        acc.Statement_Type_Preference__c = 'Electronic';
        acc.ShipToCustomerNo__c = '1';
        acc.Phone = '(123) 456-7890';
        acc.Do_Not_Use__c = false;
        acc.Account_Status__c = 'Prospect';
        acc.recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Veterinary').getRecordTypeId();
        acc.ShipToCustomerNo__c = '1';
        update acc;

        
        Account acc1 = SetupTestData.testAccounts[1];
        acc1.Name = 'test reg';
        acc1.Payment_Method__c = 'Credit/Debit Card';
        acc1.Bill_To_Customer_ID__c = '1';
        acc1.Customer_Type__c = 'Prospect';
        acc1.Statement_Type_Preference__c = 'Electronic';
        acc1.ShipToCustomerNo__c = '1';
        acc1.Phone = '(123) 456-7890';
        acc1.Do_Not_Use__c = false;
        acc1.Account_Status__c = 'Prospect';
        acc1.recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Consumer').getRecordTypeId();
        acc1.ShipToCustomerNo__c = '1';
        update acc1;
        
        Customer_Registration__c reg = new Customer_Registration__c ();
        reg.Name_of_Business_Organization__c = 'Test';
        reg.First_Name__c = 'test';
        reg.Last_Name__c = 'reg';
        reg.Billing_Street_Address1__c = 'Test123'; 
        reg.Billing_Street_Address2__c = 'Test123';
        reg.Billing_City__c = 'Test123';
        reg.Bill_To_State__c = 'OH';
        reg.Billing_Zip_Code__c = '44133';
        reg.Shipping_Street_Address1__c  = 'Test123';
        reg.Shipping_Street_Address2__c = 'Test123';
        reg.Shipping_City__c = 'Test123';
        reg.Ship_To_State__c = 'OH';
        reg.Shipping_Zip_Code__c = '44133';
        reg.Phone_Number__c = '(123) 456-7890';
        reg.Email_Address__c = 'syakkel@acumensolutions.com';
        reg.Customer_Type__c = 'Retailer';
        reg.ClinicCustomerNo__c = '1';
        reg.recordTypeId = Schema.SObjectType.Customer_Registration__c.getRecordTypeInfosByName().get('Clinic Feeding').getRecordTypeId();
        insert reg;
        
        Attachment atc = new Attachment();
        atc.parentId = reg.id;
        //atc.subject = 'test';
        atc.name='abc.txt';
        atc.body = Blob.valueOf('Unit Test Attachment Body');
        insert atc;
        
          Attachment atc1 = new Attachment();
        atc1.parentId = acc.id;
        //atc.subject = 'test';
        atc1.name='abc.txt';
        atc1.body = Blob.valueOf('Unit Test Attachment Body');
        insert atc1;
        
        reg.Customer_Account_ID__c = '1';
        reg.Application_Status__c = 'Processed';
        Test.startTest();
        update reg;
        //acc1.Name = 'testreg';
        //update acc1;
        
        Test.stopTest();
                
    }
}