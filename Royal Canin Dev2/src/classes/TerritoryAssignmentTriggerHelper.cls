public with sharing class TerritoryAssignmentTriggerHelper {

	
	public static void updateIsSyncNeeded(Map<Id,TerritoryAssignment__c> oldRecs,List<TerritoryAssignment__c> newRecs){
		for(TerritoryAssignment__c ta: newRecs){
			if(oldRecs.get(ta.id).OwnerId != ta.OwnerId){
				ta.Sync_Needed__c  = true;
			}
		}

	}

}