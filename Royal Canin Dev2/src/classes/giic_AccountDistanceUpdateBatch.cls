/************************************************************************************
Version : 1.0
Created Date : 22 Oct 2018
Function : Process the 
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/
global class giic_AccountDistanceUpdateBatch implements Database.Batchable<sObject>, Schedulable{
    
    public static final String jobName = 'Account Distance Update Batch';
    /*
    * Method name : start
    * Description : Process the account for distance update
    * Return Type : Database.QueryLocator
    * Parameter : Database.BatchableContext BC
    */
    global Database.QueryLocator start(Database.BatchableContext BC){
        string query='select id from Account';
        return Database.getQueryLocator(query);
    } 
    /*
    * Method name : execute
    * Description : Process the distance of warehouses
    * Return Type : nill
    * Parameter : Database.BatchableContext BC, List<sObject> scope
    */
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        giic_CommonUtility.getDistanceOfWarehouses(scope,false);
    }
    /*
    * Method name : finish
    * Description : Process the distance 
    * Return Type : nill
    * Parameter : Database.BatchableContext BC,
    */
    global void finish(Database.BatchableContext BC){
    }
    /*
    * Method name : schedular
    * Description : schedle the account distance update batch
    * Return Type : Returns the job in String format
    * Parameter : String cronExp
    */
    
    global static String scheduler(String cronExp){
        giic_AccountDistanceUpdateBatch objAccDisBatch = new giic_AccountDistanceUpdateBatch();
        return System.schedule(jobName, cronExp, objAccDisBatch); 
    }
    /*
    * Method name : Schedule classes
    * Description : Process the account distance update batch
    * Return Type : void
    * Parameter : SchedulableContext ctx
    */
    global void execute(SchedulableContext ctx) {
        giic_AccountDistanceUpdateBatch batch = new giic_AccountDistanceUpdateBatch();
        Database.executeBatch(batch);
    }
}