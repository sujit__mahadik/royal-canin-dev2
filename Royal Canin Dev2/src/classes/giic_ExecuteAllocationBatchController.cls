/************************************************************************************
Version : 1.0
Created Date : 24 Aug 2018
Function : execute the allocation batch
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/
public class giic_ExecuteAllocationBatchController {
    public gii__SystemPolicy__c objSystemPolicy;
    public List<BatchJob> batchJobs;
    public boolean isExecuteAllocationCompleted {get;set;}
    private Id batchId;
    public giic_ExecuteAllocationBatchController(ApexPages.StandardController stdController){
        objSystemPolicy=(gii__SystemPolicy__c)stdController.getRecord();
        isExecuteAllocationCompleted=false;
    }
    /*
    * Method name : executeAllocation
    * Description : execute the allocation batch
    * Return Type : 
    * Parameter : 
    */
    public PageReference executeAllocation(){
        try{
            batchId=Database.executeBatch(new giic_AllocationBatch());
            isExecuteAllocationCompleted=false;
        }catch(DmlException e){
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR,e.getMessage()));
            return null;
        }
        return null;
    }
    

    public List<BatchJob> getBatchJobs(){
        //Create new list of BatchJobs, a wrapper class that includes the job and percent complete.
        batchJobs = new List<BatchJob>();
    
        map<string,string> bgColorMap=new map<string,string>();
        bgColorMap.put('Queued','#f8f8f8');
        bgColorMap.put('Processing','#f8f8f8');
        bgColorMap.put('Aborted','#551A8B');
        bgColorMap.put('Completed','#f8f8f8');
        bgColorMap.put('Failed','#9E0508');
        bgColorMap.put('Preparing','#f8f8f8');
    
        map<string,string> fgColorMap=new map<string,string>();
        fgColorMap.put('Queued','#F7B64B');
        fgColorMap.put('Processing','#F7B64B');
        fgColorMap.put('Aborted','#B23AEE');
        fgColorMap.put('Completed','#20F472');
        fgColorMap.put('Failed','#FFB6C1');
        fgColorMap.put('Preparing','#F7B64B');
    
        //Query the Batch apex jobs
        for(AsyncApexJob a : [select TotalJobItems, Status, NumberOfErrors, MethodName, JobType, JobItemsProcessed, ExtendedStatus, Id, CreatedDate, CreatedById, CompletedDate, ApexClassId, ApexClass.Name From AsyncApexJob WHERE JobType='BatchApex' and id=: batchId ]){
            Double itemsProcessed = a.JobItemsProcessed;
            Double totalItems = a.TotalJobItems;
    
            BatchJob j = new BatchJob();
            j.job = a;
    
            //Determine the pecent complete based on the number of batches complete
            if(totalItems == 0){
                //A little check here as we don't want to divide by 0.
                j.percentComplete = 0;
            }else{
                j.percentComplete = ((itemsProcessed  / totalItems) * 100.0).intValue();
            }
            j.bgStatusColor=bgColorMap.get(a.Status);
            j.fgStatusColor=fgColorMap.get(a.Status);
            if(a.status == 'Completed'){
                isExecuteAllocationCompleted=true;
            }
            batchJobs.add(j);
        }
        return batchJobs;
    }
    //This is the wrapper class the includes the job itself and a value for the percent complete
    public Class BatchJob{
        public AsyncApexJob job {get; set;}
        public Integer percentComplete {get; set;}
        public string bgStatusColor {get;set;}
        public string fgStatusColor {get;set;}
    
        public BatchJob(){
            this.job=null;
            this.percentComplete=0;
            bgStatusColor='';
            fgStatusColor='';
        }
    }
}