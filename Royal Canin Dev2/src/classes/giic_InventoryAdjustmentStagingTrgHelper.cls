/**********************************************************
* Purpose    : The purpose of this class to handle gii__InventoryAdjustmentStaging__c event by extending Triggerhandler.  
*              
* *******************************************************
* Author                        Date            Remarks
*                               24/09/2018     
*********************************************************/
public class giic_InventoryAdjustmentStagingTrgHelper {
    
    /***********************************************************
    * Method name : OnAfterInsert
    * Description : to handle After Insert events of trigger
    * Return Type : void
    * Parameter : List of gii__InventoryAdjustmentStaging__c records
    ***********************************************************/
    public static map<string,gii__Warehouse__c> mapWhWithLocation = new map<string,gii__Warehouse__c>();
    public static List<gii__InventoryAdjustmentStaging__c> lstErrorUpdate = new List<gii__InventoryAdjustmentStaging__c>();
    public static List<gii__InventoryAdjustmentStaging__c> lstIASUpdate = new List<gii__InventoryAdjustmentStaging__c>();
    public static List<Integration_Error_Log__c> lstError = new List<Integration_Error_Log__c>();
    public static Map<String, gii__Product2Add__c> mapOfProds = new Map<String, gii__Product2Add__c>();
    public static Map<String, gii__ProductInventory__c> mapProdCodeWhCodeVsPI = new Map<String, gii__ProductInventory__c>();
    public static Map<String, gii__ProductInventory__c> mapProdCodeWareHCodeVsPI = new Map<String, gii__ProductInventory__c>();
    public static Map<String, String> mapWHCodeIds = new Map<String, String>();
    public static Map<String, String> mapProdSKUIds = new Map<String, String>();
    public static set<string> setOfProd = new set<string>();
    public static set<string> setOfWHCodes = new set<string>();
    // Below method is being called from trigger
    public static void OnAfterInsert(List<gii__InventoryAdjustmentStaging__c> lstTriggerNew){
        
        set<string> setProd = new set<string>();
        set<string> setWHCodes = new set<string>();
        map<String, List<gii__InventoryAdjustmentStaging__c>> mapProdWH = new map<String, List<gii__InventoryAdjustmentStaging__c>>();
        List<gii__InventoryAdjustmentStaging__c> lstOfTriggerNew = new List<gii__InventoryAdjustmentStaging__c>();
        
        if(lstTriggerNew != null && lstTriggerNew.size() > 0){
            lstOfTriggerNew = ValidateIAS(lstTriggerNew);
            
            if(lstOfTriggerNew != null && lstOfTriggerNew.size() >0 ){
                for(gii__InventoryAdjustmentStaging__c objIAS : lstOfTriggerNew ){
                    // Process only initial status records
                    if(objIAS.giic_status__c == system.label.giic_IAS_InitialStatus){
                        if(objIAS.giic_Product__c != null && objIAS.giic_Warehouse__c != null && objIAS.giic_UnitofMeasure__c != null){
                            if(objIAS.giic_AdjustmentQuantity__c == null){
                                objIAS.giic_AdjustmentQuantity__c = 0;
                            }
                            
                            if(mapProdWH.containsKey(objIAS.giic_Product__c + '~' + objIAS.giic_Warehouse__c)){
                                if(objIAS.giic_InvOverride__c == false){
                                    // objIAS.giic_InvOverride__c == false means ths is case of adjustment 
                                    mapProdWH.get(objIAS.giic_Product__c + '~' + objIAS.giic_Warehouse__c).add(objIAS);
                                }
                            }
                            else{
                                mapProdWH.put(objIAS.giic_Product__c + '~' + objIAS.giic_Warehouse__c , new List<gii__InventoryAdjustmentStaging__c>{objIAS});
                            }
                        }
                    }
                }
            }
            else{
                if(lstErrorUpdate != null && lstErrorUpdate.size() > 0){
                    lstIASUpdate.addAll(lstErrorUpdate);
                }
                CreateErrorLogAndUpdateIAS();
            }
            
            if(setOfProd != null && setOfProd.size() > 0){
                setProd = setOfProd;
            }
            if(setOfWHCodes != null && setOfWHCodes.size() > 0){
                setWHCodes = setOfWHCodes;
            }
        }
        
        
        // Process new PIQD 
        if(!mapProdWH.isEmpty() ){
            createNewPIQDrecords(setProd, setWHCodes, mapProdWH);  
        }
    }
    
    public static void processMapProdWHCase(map<String, List<gii__InventoryAdjustmentStaging__c>> mapProdWH){
        if(mapProdWH != null && mapProdWH.size() >0 ){
            for(string strPCodWCod : mapProdWH.keySet()){
                List<gii__InventoryAdjustmentStaging__c> listOFInvAS = mapProdWH.get(strPCodWCod);
                if(listOFInvAS != null && listOFInvAS.size() > 0 ){
                    gii__InventoryAdjustmentStaging__c objIAS = listOFInvAS[0];
                    if(objIAS.giic_InvOverride__c == false){ // this condition means that this process is appliable only for adjustment when giic_InvOverride__c == false
                        Decimal IntTotalQtyCountOfPI = 0.0;
                        for( gii__InventoryAdjustmentStaging__c ObjecIAS : listOFInvAS ){
                            IntTotalQtyCountOfPI = IntTotalQtyCountOfPI + ObjecIAS.giic_AdjustmentQuantity__c;
                        }
                        if(IntTotalQtyCountOfPI == 0.0 && mapProdCodeWhCodeVsPI != null && mapProdCodeWhCodeVsPI.containsKey(objIAS.giic_Product__c + '~' + objIAS.giic_Warehouse__c)){
                            mapProdCodeWareHCodeVsPI.put(objIAS.giic_Product__c+'~'+objIAS.giic_Warehouse__c,mapProdCodeWhCodeVsPI.get(objIAS.giic_Product__c+'~'+objIAS.giic_Warehouse__c));
                        }
                    }
                }
            }
        }
    }
    
    // below method validate IAS records having correct data
    public static List<gii__InventoryAdjustmentStaging__c> ValidateIAS(List<gii__InventoryAdjustmentStaging__c> lstTriggerNew){
        List<gii__InventoryAdjustmentStaging__c> lstOfTriggerNew = new List<gii__InventoryAdjustmentStaging__c>();
        set<string> setOfProds = new set<string>();
        set<string> setOfWareH = new set<string>();
        
        for(gii__InventoryAdjustmentStaging__c objIAS : lstTriggerNew ){
            if(string.isNotEmpty(objIAS.giic_Product__c)){
                setOfProds.add(objIAS.giic_Product__c);
            }
            if(string.isNotEmpty(objIAS.giic_Warehouse__c)){
                setOfWareH.add(objIAS.giic_Warehouse__c);
            }
        }
        
        if(setOfProds != null && setOfProds.size() > 0){
            getProdIds(setOfProds);
        }
        if(setOfWareH != null && setOfWareH.size() > 0){
            getWarehouseIds(setOfWareH);
        }
        
        for(gii__InventoryAdjustmentStaging__c objIAS : lstTriggerNew ){
            string sErrorMsg = '';

            if(string.isEmpty(objIAS.giic_status__c) ){
                sErrorMsg = label.giic_IAS_StatusMNNull ;
            }
            if(string.isEmpty(objIAS.giic_Product__c)){
                sErrorMsg = sErrorMsg + label.giic_IAS_PCodeMNNull ;
            }
            
            if(string.isEmpty(objIAS.giic_Warehouse__c)){
                sErrorMsg = sErrorMsg + label.giic_IAS_WhCodeMNNull; 
            }
            if(string.isEmpty(objIAS.giic_UnitofMeasure__c)){
                sErrorMsg = sErrorMsg + label.giic_IASUOMMNNull;
            }
            else{
                set<string> setProdUOMName = new set<string>();
                if(mapOfProds != null && mapOfProds.containsKey(objIAS.giic_Product__c)){
                    if(mapOfProds.get(objIAS.giic_Product__c).gii__StockingUnitofMeasure__c != null){
                        setProdUOMName.add(mapOfProds.get(objIAS.giic_Product__c).gii__StockingUnitofMeasure__r.Name);
                    }
                    if(mapOfProds.get(objIAS.giic_Product__c).gii__SellingUnitofMeasure__c != null){
                        setProdUOMName.add(mapOfProds.get(objIAS.giic_Product__c).gii__SellingUnitofMeasure__r.Name);
                    }
                }
                if(objIAS.giic_UnitofMeasure__c != null && setProdUOMName != null&& setProdUOMName.size()>0 && !setProdUOMName.contains(objIAS.giic_UnitofMeasure__c) ){
                    sErrorMsg = sErrorMsg + label.giic_IAS_UOMMismatch; 
                }
            }
            
            if(objIAS.giic_Product__c != null && mapOfProds!= null && mapOfProds.containsKey(objIAS.giic_Product__c) && mapOfProds.get(objIAS.giic_Product__c).gii__NonStock__c == true ){
                sErrorMsg = sErrorMsg + label.giic_IAS_NonStockAdj;
            }
            
            
            if(string.isNotEmpty(sErrorMsg) ){
                lstErrorUpdate.add(new gii__InventoryAdjustmentStaging__c(id = objIAS.Id, giic_Status__c =  system.label.giic_IAS_Error));
                collectErrorLog_new(sErrorMsg,objIAS.Id);
            }
            else{
                
                if(objIAS.giic_Product__c != null ){
                    setOfProd.add(objIAS.giic_Product__c);
                }
                if(objIAS.giic_Warehouse__c != null ){
                    setOfWHCodes.add(objIAS.giic_Warehouse__c);
                }
                lstOfTriggerNew.add(objIAS);
            }
            
        }
        return lstOfTriggerNew;
    }
    
    // below method is used to collect Integration_Error_Log__c
    public static void collectErrorLog_new(string errormsg, String invAdjStagingId){
        Integration_Error_Log__c objErrLog = new Integration_Error_Log__c();
        objErrLog.giic_Inventory_Adjustment_Staging__c = invAdjStagingId;
        objErrLog.Error_Message__c = errormsg; 
        objErrLog.giic_IsActive__c = true; 
        objErrLog.Name = giic_Constants.INVENTORY_ADJUSTMENT; 
        lstError.add(objErrLog);
    }
    
    // below method is used for creating PIQD
    public static void createNewPIQDrecords(set<string> setProd,set<string> setWHCodes, map<String, List<gii__InventoryAdjustmentStaging__c>> mapProdWH){  
        List<Integration_Error_Log__c> lstError = new List<Integration_Error_Log__c>();
        map<String, List<gii__InventoryAdjustmentStaging__c>> mapProdWHToCreateInentoryAdj = new map<String, List<gii__InventoryAdjustmentStaging__c>>();
        
        if(!mapProdWH.isEmpty()){  
            
            Map<String, gii__ProductInventory__c> mapPI = getProductInventory(setProd, setWHCodes);
            set<string> setProdWH = new set<string>();
            for(String strProdWH : mapProdWH.keySet()){
                String prodSKU = strProdWH.split('~')[0].trim();
                String whCode = strProdWH.split('~')[1].trim();
                if(mapProdSKUIds.containsKey(prodSKU) && mapWHCodeIds.containsKey(whCode)){
                    if(!mapPI.containsKey(mapProdSKUIds.get(prodSKU) + '~' + mapWHCodeIds.get(whCode))){
                        setProdWH.add(strProdWH);
                    }
                    mapProdWHToCreateInentoryAdj.put(strProdWH,mapProdWH.get(strProdWH));
                }else{
                    for(gii__InventoryAdjustmentStaging__c objecIAS : mapProdWH.get(strProdWH)){
                        lstErrorUpdate.add(new gii__InventoryAdjustmentStaging__c(id = objecIAS.Id, giic_Status__c =  system.label.giic_IAS_Error));
                        collectErrorLog_new( mapProdSKUIds.containsKey(prodSKU) ? giic_Constants.Warehouse_Not_found : giic_Constants.Product_Not_found, objecIAS.Id);
                    }
                }
            }
            if(!setProdWH.isEmpty()){ 
                
                mapPI.putAll(createProductInventory(setProdWH, mapProdSKUIds, mapWHCodeIds));
            }
            
            processMapProdWHCase(mapProdWH);
            
            createInventoryAdjustment(mapProdWHToCreateInentoryAdj, mapPI);

        }
    }
    // below method is ued to call glovia API and create Inventory Adjustment
    public static void createInventoryAdjustment(map<String, List<gii__InventoryAdjustmentStaging__c>> mapProdWH, Map<String, gii__ProductInventory__c> mapPI){

        if(!mapProdWH.isEmpty()){
            Set<Id> setProdIds = new Set<Id>();
            if(!mapProdSKUIds.isEmpty()){
                for(String strProdId : mapProdSKUIds.values()){
                    setProdIds.add(strProdId);
                }
            }
            
            Map<String, decimal> mapConversionFactors = giic_CommonUtility.getProdConversionFactors(setProdIds);
            Decimal scaleValue  = giic_CommonUtility.getGloviaSystemSetting().gii__DecimalsWhenRoundingQuantities__c;
            
            // create list for Inventory Adjustment
            List<gii__InventoryAdjustment__c> listInventoryAdj = new List<gii__InventoryAdjustment__c>();
            String reverseConversionKey;
            String conversionKey;
            
            
            Map<String, gii__ProductInventoryQuantityDetail__c> mapPIQD = new Map<String, gii__ProductInventoryQuantityDetail__c>();
            List<gii__ProductInventoryQuantityDetail__c> lstPIQDs = [Select Id, gii__Location__c,gii__LocationBin__c, gii__MovableQuantity__c,gii__Blockedquantity__c, gii__OnHandquantity__c,gii__Product__c, gii__ProductLot__r.gii__Restricted__c,
                                                                        gii__ProductLot__c, gii__Warehouse__c, gii__Available__c, gii__LotRestricted__c, gii__ProductInventory__c,gii__ProductInventorybyLocation__c, gii__ProductSerial__c, gii__ReservedQuantity__c, gii__RestrictedQuantity__c
                                                                    From gii__ProductInventoryQuantityDetail__c  
                                                                    Where gii__Product__c In : mapProdSKUIds.values() and gii__Warehouse__c In : mapWHCodeIds.values()];
            if(!lstPIQDs.isEmpty()){
                for(gii__ProductInventoryQuantityDetail__c objP : lstPIQDs){
                   mapPIQD.put(objP.gii__Product__c + '~' + objP.gii__Warehouse__c + '~' +objP.gii__ProductInventory__c, objP); 
                }
            }
            
            List<gii__ProductInventoryQuantityDetail__c> lstProductInvQtyDetails = new List<gii__ProductInventoryQuantityDetail__c>();
            map<string, gii__ProductInventoryQuantityDetail__c> mapProdCodeWhCodeVsPIQD = new map<string, gii__ProductInventoryQuantityDetail__c>();
            for(String strProdWH : mapProdWH.keySet()){
                String prodSKU = strProdWH.split('~')[0].trim();
                String whCode = strProdWH.split('~')[1].trim();
                if(mapProdSKUIds.containsKey(prodSKU) && mapWHCodeIds.containsKey(whCode)){
                    String strProdWHKey = mapProdSKUIds.get(prodSKU) + '~' + mapWHCodeIds.get(whCode);
                    if(mapPI.containsKey(strProdWHKey)){
                        List<gii__InventoryAdjustmentStaging__c> listOfIAStag = mapProdWH.get(strProdWH);
                        if(listOfIAStag != null && listOfIAStag.size() > 0 ){
                            for(gii__InventoryAdjustmentStaging__c objIAS : listOfIAStag){
                                Decimal adjQty = 0;
                                Boolean isUOMError = false;
                                //if product refrence is not have StockingUnitofMeasure(i.e.ProductReference.gii__StockingUnitofMeasure__r == null), No conversion is required
                                if(objIAS.giic_UnitofMeasure__c.equalsIgnoreCase(mapPI.get(strProdWHKey).gii__Product__r.gii__StockingUnitofMeasure__r.Name) || 
                                mapPI.get(strProdWHKey).gii__Product__r.gii__StockingUnitofMeasure__r == null ){
                                    adjQty = giic_CommonUtility.getGloviaRoundingvalue(objIAS.giic_AdjustmentQuantity__c,scaleValue );
                                }else{
                                    // controle will come in this block when UnitofMeasure of IAS is not same as stocking unit of measure of Product Ref. So we need to get reverse conversion factor. 
                                     conversionKey = mapPI.get(strProdWHKey).gii__Product__c + '~' + mapPI.get(strProdWHKey).gii__Product__r.gii__SellingUnitofMeasure__r.Name.trim()  + '~' + mapPI.get(strProdWHKey).gii__Product__r.gii__StockingUnitofMeasure__r.Name.trim() ;

                                     if(mapConversionFactors != null && mapConversionFactors.containsKey(conversionKey)){
                                          adjQty = objIAS.giic_AdjustmentQuantity__c != null ? giic_CommonUtility.getGloviaRoundingvalue(objIAS.giic_AdjustmentQuantity__c * mapConversionFactors.get(conversionKey),scaleValue ) : 0;
                                     }
                                     else{
                                        isUOMError = true;
                                        collectErrorLog_new(giic_Constants.UOM_Not_configured, objIAS.Id);
                                     } 
                                    
                                }
            
                                if(!isUOMError){ //adjQty != 0
                                    String key = mapProdSKUIds.get(prodSKU) + '~' + mapWHCodeIds.get(whCode) + '~' + mapPI.get(strProdWHKey).id;
                                    gii__ProductInventoryQuantityDetail__c productInvQtyDetail;
                                    if(mapPIQD.containsKey(key)){
                                        productInvQtyDetail = mapPIQD.get(key);
                                    }else{
                                        productInvQtyDetail = new gii__ProductInventoryQuantityDetail__c();
                                        productInvQtyDetail.gii__ProductInventory__c = mapPI.get(strProdWHKey).id;
                                        productInvQtyDetail.gii__Product__c = mapProdSKUIds.get(prodSKU);
                                        productInvQtyDetail.gii__Warehouse__c = mapWHCodeIds.get(whCode);
                                        if(mapWhWithLocation.ContainsKey(whCode) && mapWhWithLocation.get(whCode).gii__Locations__r != null && mapWhWithLocation.get(whCode).gii__Locations__r.size() > 0 ){
                                            productInvQtyDetail.gii__Location__c = mapWhWithLocation.get(whCode).gii__Locations__r[0].Id;
                                        }
                                    }
                                    
                                    Decimal onHandQuantity = 0;
                                    if(objIAS.giic_InvOverride__c){
                                        onHandQuantity = giic_CommonUtility.getGloviaRoundingvalue(adjQty,scaleValue);
                                        // in case of sync (ie. giic_InvOverride__c == true, update PIQD.gii__BlockedQuantity__c = objIAS.giic_OnHoldBlockedQty__c )
                                        // if giic_IAS_Reason_QH = 'QH'
                                        if(objIAS.giic_Reason__c != null && objIAS.giic_Reason__c == system.label.giic_IAS_Reason_QH ){
                                            productInvQtyDetail.gii__BlockedQuantity__c = objIAS.giic_AdjustmentQuantity__c ;
                                        } 
                                    }else{
                                        onHandQuantity = productInvQtyDetail.gii__OnHandQuantity__c != null ? giic_CommonUtility.getGloviaRoundingvalue(productInvQtyDetail.gii__OnHandQuantity__c + adjQty,scaleValue ) : giic_CommonUtility.getGloviaRoundingvalue(adjQty,scaleValue);
                                    }                             
                                    productInvQtyDetail.gii__OnHandQuantity__c = onHandQuantity;
                                    
                                    mapProdCodeWhCodeVsPIQD.put(objIAS.giic_Product__c + '~' + objIAS.giic_Warehouse__c, productInvQtyDetail);
                                }
                            }
                        }
                    }
                }
            }
            lstProductInvQtyDetails.addAll(mapProdCodeWhCodeVsPIQD.values());

            Map<string, gii__ProductInventory__c> mapProdWhVsPI = new Map<string, gii__ProductInventory__c>();
            
            if(lstProductInvQtyDetails != null && lstProductInvQtyDetails.size() > 0 ){
                // create input parameters for inventory adjustment creation for location
                gii.InventoryAdjustmentforLocation.ProductInventoryQtyDetails QuantityDatsilsObj = new gii.InventoryAdjustmentforLocation.ProductInventoryQtyDetails();
                QuantityDatsilsObj.listProductInventoryQuantityDetails = lstProductInvQtyDetails;
                QuantityDatsilsObj.AdjustmentDate = system.today();
                QuantityDatsilsObj.AdjustmentReason = giic_Constants.INCORRECT_INVENTORY_COUNT;
                // call the createInventoryAdjustments method
                gii.InventoryAdjustmentforLocation.InventoryAdjustmentsResult ResultObj = gii.InventoryAdjustmentforLocation.CreateInventoryAdustments(QuantityDatsilsObj);

                
                // Exception Handling
                if (ResultObj.Exceptions != null) {
                    if (ResultObj.Exceptions.size() > 0) {
                       List<String> ErrorMsgList = new List<String>();
                       for(gii.InventoryAdjustmentforLocation.GOMException e : ResultObj.Exceptions) {
                           system.debug(e.getMessage());
                            ErrorMsgList.add(e.getMessage());
                       }
                       if(ErrorMsgList.size()>0){
                            system.debug(ErrorMsgList);
                       }
                    }          
                }
                
                if( ResultObj.listInventoryAdjustment != null && ResultObj.listInventoryAdjustment.size() > 0 ){
                    
                    set<string> setPI = new set<string>();
                    List<gii__ProductInventory__c> listOfPI = new List<gii__ProductInventory__c>();
                    for(gii__InventoryAdjustment__c objIA : ResultObj.listInventoryAdjustment ){
                        setPI.add(objIA.gii__ProductInventory__c);
                    }
                    
                    if(setPI != null && setPI.size() >0){
                        listOfPI = [select id, gii__Product__c,  gii__Product__r.gii__ProductReference__r.SKU__c, gii__Warehouse__c, gii__Warehouse__r.giic_WarehouseCode__c from gii__ProductInventory__c where id in : setPI];
                    }
                    if(listOfPI != null && listOfPI.size() > 0 ){
                        for(gii__ProductInventory__c objProdI : listOfPI){
                            mapProdWhVsPI.put(objProdI.gii__Product__r.gii__ProductReference__r.SKU__c +'~'+ objProdI.gii__Warehouse__r.giic_WarehouseCode__c, objProdI);
                        }
                    }
                }
            }
            
            List<gii__InventoryAdjustmentStaging__c> lstIASToUpdate = new List<gii__InventoryAdjustmentStaging__c>();
            // sort staging records for update on the basis of Processed and Error 
            if(mapProdCodeWareHCodeVsPI != null && mapProdCodeWareHCodeVsPI.size() > 0){
                mapProdWhVsPI.putAll(mapProdCodeWareHCodeVsPI);
            }
            
            for(string sProdcdWHCd : mapProdWH.keySet() ){
                if(mapProdWhVsPI.containsKey(sProdcdWHCd)){
                    List<gii__InventoryAdjustmentStaging__c> listOfIAStag1 = mapProdWH.get(sProdcdWHCd);
                    if(listOfIAStag1 != null && listOfIAStag1.size() > 0 ){
                        for(gii__InventoryAdjustmentStaging__c objIAS1 : listOfIAStag1){
                            lstIASToUpdate.add(new gii__InventoryAdjustmentStaging__c(id = objIAS1.id, giic_Status__c = system.label.giic_IAS_Success, giic_ProductInventory__c = mapProdWhVsPI.get(sProdcdWHCd).id));
                        }
                    }
                    
                }
                else{
                    List<gii__InventoryAdjustmentStaging__c> listOfIAStag1 = mapProdWH.get(sProdcdWHCd);
                    if(listOfIAStag1 != null && listOfIAStag1.size() > 0 ){
                        for(gii__InventoryAdjustmentStaging__c objIAS1 : listOfIAStag1){
                            lstIASToUpdate.add(new gii__InventoryAdjustmentStaging__c(id = objIAS1.id, giic_Status__c = system.label.giic_IAS_Error));
                        }
                    }
                    
                }
            }
            
            if(lstIASToUpdate != null && lstIASToUpdate.size() > 0 ){
                lstIASUpdate.addAll(lstIASToUpdate);
            }
            if(lstErrorUpdate != null && lstErrorUpdate.size() > 0 ){
                lstIASUpdate.addAll(lstErrorUpdate);
            }
            
            // call CreateErrorLogAndUpdateIAS to create error log and update IAS
            CreateErrorLogAndUpdateIAS();
           
        }
    }
    
    // below method CreateErrorLogAndUpdateIAS: create error log and update IAS
    public static void CreateErrorLogAndUpdateIAS() {
        if(!lstIASUpdate.isEmpty()) update lstIASUpdate;    
        if(!lstError.isEmpty()) insert lstError;
    }
    
    // below method is used to get Products
    public static Map<String, String> getProdIds(set<string> setProd){
        List<gii__Product2Add__c> lstProd = [Select id, Name, gii__ProductReference__r.SKU__c, gii__NonStock__c, gii__StockingUnitofMeasure__c, gii__StockingUnitofMeasure__r.name, gii__SellingUnitofMeasure__c, gii__SellingUnitofMeasure__r.Name from gii__Product2Add__c where gii__ProductReference__r.SKU__c in :setProd];
        if(!lstProd.isEmpty()){
            for(gii__Product2Add__c objProd : lstProd){
                mapProdSKUIds.put(objProd.gii__ProductReference__r.SKU__c, objProd.id);
                mapOfProds.put(objProd.gii__ProductReference__r.SKU__c,objProd);
            }
        }
        return mapProdSKUIds;
    }
    // below method is used to get warehouse
    public static Map<String, String> getWarehouseIds(set<string> setWHCodes){
        List<gii__Warehouse__c> lstWH = [Select id, Name, giic_WarehouseCode__c, (select id, gii__Sequence__c from gii__Locations__r order by gii__Sequence__c ASC) from gii__Warehouse__c where giic_WarehouseCode__c in :setWHCodes];
        if(!lstWH.isEmpty()){
            for(gii__Warehouse__c objWH : lstWH){
                mapWHCodeIds.put(objWH.giic_WarehouseCode__c, objWH.id);
                mapWhWithLocation.put(objWH.giic_WarehouseCode__c, objWH);
                mapWhWithLocation.put(objWH.Id, objWH);
            }
        }
        return mapWHCodeIds;
    }
    // below method is used to get Product Inventory
    public static Map<String, gii__ProductInventory__c> getProductInventory(set<string> setProd,set<string> setWHCodes){
        Map<String, gii__ProductInventory__c> mapPI = new Map<String, gii__ProductInventory__c>();
        List<gii__ProductInventory__c> lstPI = [select id, gii__Product__c, gii__Product__r.gii__SellingUnitofMeasure__c, gii__Product__r.gii__SellingUnitofMeasure__r.Name, gii__Product__r.gii__StockingUnitofMeasure__c, gii__Product__r.gii__StockingUnitofMeasure__r.Name, gii__Product__r.gii__ProductReference__r.SKU__c, gii__Warehouse__c, gii__Warehouse__r.giic_WarehouseCode__c from gii__ProductInventory__c where gii__Product__r.gii__ProductReference__r.SKU__c in :setProd AND gii__Warehouse__r.giic_WarehouseCode__c in : setWHCodes];
        
        if(!lstPI.isEmpty()){
            for(gii__ProductInventory__c objPI : lstPI){
                mapPI.put(objPI.gii__Product__c + '~' + objPI.gii__Warehouse__c, objPI);
                mapProdCodeWhCodeVsPI.put(objPI.gii__Product__r.gii__ProductReference__r.SKU__c + '~' + objPI.gii__Warehouse__r.giic_WarehouseCode__c, objPI);
            }
        }
         return mapPI;
    }
      // below method is used to create Product Inventory  
    public static Map<String, gii__ProductInventory__c> createProductInventory(set<string> setProdWH, Map<String, String> mapProdSKUIds, Map<String, String> mapWHCodeIds){

         Map<String, gii__ProductInventory__c> mapPI = new Map<String, gii__ProductInventory__c>();
         if(!setProdWH.isEmpty()){
            // create list for Product Inventory
            List<gii__ProductInventory__c> listProductInv = new List<gii__ProductInventory__c>();
            set<string> setProd = new set<string>();
            set<string> setWHCodes = new set<string>();

            // create the Product Inventory sObject and populate its required fields, and optionally other fields.
            for(String strProdWH : setProdWH){
                String prodSKU = strProdWH.split('~')[0].trim();
                String whCode = strProdWH.split('~')[1].trim();
                if(mapProdSKUIds.containsKey(prodSKU) && mapWHCodeIds.containsKey(whCode)){
                    gii__ProductInventory__c ProductInv = new gii__ProductInventory__c();
                    ProductInv.gii__Product__c   = mapProdSKUIds.get(prodSKU); //Exist Product Reference's Id 
                    ProductInv.gii__Warehouse__c = mapWHCodeIds.get(whCode); //Exist Warehouse's Id 
                    listProductInv.add(ProductInv); 
                    setProd.add(prodSKU);
                    setWHCodes.add(whCode);
                } 
            }
            // insert Product Inventory 
            DataBase.SaveResult[] result = Database.Insert(listProductInv, false);
            
            for (Database.SaveResult sr:result) {
                 if (!sr.isSuccess()){
                     Database.Error err = sr.getErrors()[0];
                     // Create error log
                 }
            }
            
            mapPI.putAll(getProductInventory(setProd, setWHCodes));
         }
        return mapPI;
     }
}