/**********************************************************
* Purpose    : The purpose of this class to schedule loyalty and reward points Batch.  
*              
* *******************************************************/

public class giic_LoyaltyBatchScheduler
{
    public gii__SystemPolicy__c objSystemPolicy;

    public giic_LoyaltyBatchScheduler(ApexPages.StandardController stdController){
        objSystemPolicy=(gii__SystemPolicy__c)stdController.getRecord();
    }
     /*
    * Method name : scheduleBatch
    * Description : Schedule batches after checking for duplicates.
    * Return Type : PageReference
    * Parameter : 
    */
    public PageReference scheduleBatch()
    {          
        Map<Id, CronTrigger> cronMap = new Map<Id, CronTrigger>([Select Id From CronTrigger WHERE CronJobDetail.Name like: giic_Constants.STR_LOYALTY_BATCH ]);        
        if(cronMap.size() == 0 && cronMap != null)
        {
          scheduler();
          ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.CONFIRM, giic_Constants.Batch_MSG_SCHEDULED));
        } 
        else 
        {
          ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.INFO, giic_Constants.Batch_MSG_ALREADY_SCHEDULED));      
        }      
        return null;
    }
     /*
    * Method name : scheduler
    * Description : Schedules the batch through dynamic scheduling.
    * Return Type : PageReference
    * Parameter : 
    */
    
    private void scheduler()
    {
        giic_LoyaltyPointsTrackerBatch obj = new giic_LoyaltyPointsTrackerBatch();
        Database.executeBatch(obj);
        
        /*String cronExp = System.Label.giic_CronLoyaltyBatch;              
        String jobName =  'giic_LoyaltyPointsTrackerBatch';
        giic_LoyaltyPointsTrackerBatch oScheduledBatchJob_APIName = new giic_LoyaltyPointsTrackerBatch() ;
        System.schedule(jobName , cronExp , oScheduledBatchJob_APIName) ;*/
        
    }

}