/************************************************************************************
Version : 1.0
Name : giic_ValidateAccAddressBatchController
Created Date : 17 Aug 2018
Function : Controller class for giic_ValidateAccAddressBatch VF page
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/

public with sharing class giic_ValidateAccAddressBatchController {
    
    private Id batchprocessid;  
    public List<BatchJob> batchJobs {get;set;}
    public boolean isExecuteAllocationCompleted {get;set;}      
    public Boolean goBack{get;set;}
    
    public giic_ValidateAccAddressBatchController(ApexPages.StandardSetController controller) {
        isExecuteAllocationCompleted=false;
        batchJobs = new List<BatchJob>();
    }
    
    public void executeAddressValidation(){
        try{          
            giic_AccountAddressValidationBatch batch = new giic_AccountAddressValidationBatch();
            batchprocessid = Database.executeBatch(batch);
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM, System.Label.giic_BatchJobSubmitted));
            goBack = false;
        }catch(Exception ex){ ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getMessage())); }
    }
    
    public void findBatchJobs(){
        //Create new list of BatchJobs, a wrapper class that includes the job and percent complete.
        batchJobs = new List<BatchJob>();
      
        map<string,string> bgColorMap=new map<string,string>();
        map<string,string> fgColorMap=new map<string,string>();
        
        for(giic_BatchColorCode__mdt colorCode : [SELECT Id, DeveloperName, QualifiedApiName, giic_fgColor__c, giic_bgColor__c 
                                                  FROM giic_BatchColorCode__mdt]){
            bgColorMap.put(colorCode.DeveloperName, colorCode.giic_bgColor__c);
            fgColorMap.put(colorCode.DeveloperName, colorCode.giic_fgColor__c);
                                                            
        }
        
        /*bgColorMap.put('Queued','#f8f8f8');
        bgColorMap.put('Processing','#f8f8f8');
        bgColorMap.put('Aborted','#551A8B');
        bgColorMap.put('Completed','#f8f8f8');
        bgColorMap.put('Failed','#9E0508');
        bgColorMap.put('Preparing','#f8f8f8');
        
        
        fgColorMap.put('Queued','#F7B64B');
        fgColorMap.put('Processing','#F7B64B');
        fgColorMap.put('Aborted','#B23AEE');
        fgColorMap.put('Completed','#20F472');
        fgColorMap.put('Failed','#FFB6C1');
        fgColorMap.put('Preparing','#F7B64B');*/
        
        //Query the Batch apex jobs
        for(AsyncApexJob async : [SELECT Id, TotalJobItems, Status, NumberOfErrors, 
                              MethodName, JobType, JobItemsProcessed, ExtendedStatus, 
                              CreatedDate, CreatedById, CompletedDate, ApexClassId, 
                              ApexClass.Name 
                              FROM AsyncApexJob 
                              WHERE JobType='BatchApex' 
                              AND Id =: batchprocessid]){
            Double itemsProcessed = async.JobItemsProcessed;
            Double totalItems = async.TotalJobItems;
            
            BatchJob batch = new BatchJob();
            batch.job = async;
            
            //Determine the pecent complete based on the number of batches complete
            if(totalItems == 0){
                //A little check here as we don't want to divide by 0.
                batch.percentComplete = 0;
            }else{ batch.percentComplete = ((itemsProcessed  / totalItems) * 100.0).intValue(); }
            
            batch.bgStatusColor=bgColorMap.get(async.Status);
            batch.fgStatusColor=fgColorMap.get(async.Status);
            if(async.status == 'Completed'){
                isExecuteAllocationCompleted=true;
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM, System.Label.giic_BatchJobCompleted));
            }
            batchJobs.add(batch);
        }
        //return batchJobs;
    }
    //This is the wrapper class the includes the job itself and a value for the percent complete
    public Class BatchJob{
        public AsyncApexJob job {get; set;}
        public Integer percentComplete {get; set;}
        public string bgStatusColor {get;set;}
        public string fgStatusColor {get;set;}
        
        public BatchJob(){
            this.job=null;
            this.percentComplete=0;
            bgStatusColor='';
            fgStatusColor='';
        }
    }
    
    public PageReference goBack(){
        PageReference pObj ;
        try{          
            Schema.DescribeSObjectResult result = Account.SObjectType.getDescribe();               
            pObj = new PageReference('/' + result.getKeyPrefix() + '/o'); // redirect to sales order Staging list view
            pObj.setRedirect(true);            
        }catch(Exception ex){ ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getMessage())); return null;  }
        return pObj;
    }
    
}