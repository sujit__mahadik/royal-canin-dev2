/************************************************************************************
Version : 1.0
Created Date : 29 Oct 2018
Function : transfer order reciept service for the request from Insite
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/
@RestResource(urlMapping='/TransferOrderReciept/v0.1/*')
global class giic_TransferOrderRecieptWS {
    /******************************************************
POST : To create the transfer Order Reciept
*******************************************************/
    @HttpPost global static list<giic_TransferOrderWrapper.TransferOrderReceipt>  createTransferOrderReceipt(){
        
        list<String> listTransferOrders = new list<String>();
        List<String> errorMsgList = new  List<String>(); 
        giic_TransferOrderWrapper.TransferOrderResult tor_response;
        map<String,gii__TransferOrder__c> mapTransferOrders = new map<String,gii__TransferOrder__c>();
        /************************** RestRequest from Insite *****************************/
        
        RestRequest req = RestContext.request;
        try
        { 
            
            if(req != null){
                String request  = req.requestBody.tostring();
                giic_TransferOrderWrapper transferOrderWrapper =(giic_TransferOrderWrapper)json.deserialize(request, giic_TransferOrderWrapper.class);
                
                if(transferOrderWrapper != null && transferOrderWrapper.TransferOrderReceipts != null)
                {
                    for(giic_TransferOrderWrapper.TransferOrderReceipt rec: transferOrderWrapper.TransferOrderReceipts)
                    {
                        if(rec.TOR_OrderNumber.trim() != null && rec.TOR_OrderNumber.trim() != '')
                        {
                            listTransferOrders.add(rec.TOR_OrderNumber.trim()); //order number is external id 
                        }                       
                    }
                   
                    if(listTransferOrders != null && !listTransferOrders.isEmpty())
                    {
                        list<gii__ReceiptQueue__c>  recieptQueues = giic_CommonUtility.getRecieptQueues(listTransferOrders);
                       
                        if(recieptQueues != null && !recieptQueues.isEmpty())
                        {
                            map<String,gii__ReceiptQueue__c> mapTOLineRecQueue = new map<String,gii__ReceiptQueue__c>();
                            //creating a map to compare input request reciepts and reciept queues
                            for(gii__ReceiptQueue__c recQ : recieptQueues)
                            {
                                mapTOLineRecQueue.put(recQ.gii__TransferOrderLine__r.giic_OrderLineNo__c,recQ);
                                mapTransferOrders.put(recQ.gii__TransferOrder__r.giic_OrderNo__c,new gii__TransferOrder__c(id = recQ.gii__TransferOrder__c, gii__Status__c = recQ.gii__TransferOrder__r.gii__Status__c));
                            }
                            
                            transferOrderWrapper.TransferOrderReceipts = validateTransferOrder(transferOrderWrapper.TransferOrderReceipts,mapTransferOrders);
                            
                            updateRecipetQueues(mapTOLineRecQueue,transferOrderWrapper.TransferOrderReceipts);
                            if(listTransferOrders != null && !listTransferOrders.isEmpty())
                            {
                                Map<String,Object> result = createTransferOrderReceiptGlovia(listTransferOrders,recieptQueues);
                                
                                if(result != null && result.containsKey(giic_Constants.SUCCESS) && result.get(giic_Constants.SUCCESS) == true)
                                {
                                    return returnResponse(giic_Constants.SUCCESS_CODE_200,giic_Constants.SUCCESS_MSG,null,transferOrderWrapper.TransferOrderReceipts);
                                }
                                else
                                {
                                    if(result.containsKey(giic_Constants.ERRORLIST))
                                        errorMsgList.addAll((List<String>)result.get(giic_Constants.ERRORLIST));
                                    
                                    insertIntegrationErrorLogs(errorMsgList,listTransferOrders);
                                    return returnResponse(giic_Constants.ERROR_CODE_400, giic_Constants.ERROR_MSG_FAILURE, giic_Constants.TRANSFER_ORDER_ERROR_CODE_InvalidRequest ,transferOrderWrapper.TransferOrderReceipts); 
                                }
                                
                            }
                            else { return returnResponse(giic_Constants.ERROR_CODE_400, giic_Constants.ERROR_MSG_FAILURE, giic_Constants.TRANSFER_ORDER_ERROR_CODE_NotFound ,transferOrderWrapper.TransferOrderReceipts); }
                        }
                        else
                        {
                            insertIntegrationErrorLogs(new list<String>{giic_Constants.TRANSFER_ORDER_ERROR_CODE_InvalidRequest},listTransferOrders);
                            return returnResponse(giic_Constants.ERROR_CODE_400, giic_Constants.ERROR_MSG_FAILURE, giic_Constants.TRANSFER_ORDER_ERROR_CODE_InvalidRequest,transferOrderWrapper.TransferOrderReceipts);
                            
                        }
                    }
                    else { return returnResponse(giic_Constants.ERROR_CODE_400, giic_Constants.ERROR_MSG_FAILURE, giic_Constants.TRANSFER_ORDER_ERROR_CODE_NotFound ,transferOrderWrapper.TransferOrderReceipts); }                  
                    
                }//end of if 
                else  { return returnResponse(giic_Constants.ERROR_CODE_400, giic_Constants.ERROR_MSG_FAILURE, giic_Constants.TRANSFER_ORDER_ERROR_CODE_NotAvailable ,transferOrderWrapper.TransferOrderReceipts);  }
                
                
            }//end of if 
            else{    return returnResponse(giic_Constants.ERROR_CODE_400, giic_Constants.ERROR_MSG_FAILURE, giic_Constants.TRANSFER_ORDER_ERROR_CODE_InvalidRequest,null); } 
        }
        catch(Exception ex)
        {
            insertIntegrationErrorLogs(new list<String>{ex.getMessage() + 'at line no:' +  ex.getLineNumber()},listTransferOrders);
            return returnResponse(giic_Constants.ERROR_CODE_417, giic_Constants.ERROR_MSG_FAILURE, ex.getMessage(),null);
        }       
    }
    
    public class CustomException extends Exception
    {
        
    }
    private static list<giic_TransferOrderWrapper.TransferOrderReceipt> validateTransferOrder(list<giic_TransferOrderWrapper.TransferOrderReceipt> transferOrderReceipts,map<String,gii__TransferOrder__c> mapTransferOrders)
    {
        list<String> listTransferOrders = new list<String>();
        list<giic_TransferOrderWrapper.TransferOrderReceipt> transferOrderReceiptstemp = transferOrderReceipts;
        for(giic_TransferOrderWrapper.TransferOrderReceipt tor : transferOrderReceiptstemp)
        {
            if(!mapTransferOrders.containsKey(tor.TOR_OrderNumber))
            {
                tor.Error = giic_Constants.TRANSFER_ORDER_ERRORS;
                tor.Status = giic_Constants.ERROR_MSG_FAILURE;
                listTransferOrders.add(tor.TOR_OrderNumber);
                
            }
            
        }        
        if(!listTransferOrders.isEmpty())
            insertIntegrationErrorLogs(new list<String>{giic_Constants.TRANSFER_ORDER_ERRORS},listTransferOrders);
        
        return transferOrderReceiptstemp;
    }  
    
    private static void insertIntegrationErrorLogs(list<String> errorMsgList, list<String> listTransferOrders)
    {
        
        if(errorMsgList != null && !errorMsgList.isEmpty())
        {
            List<Integration_Error_Log__c> lstErrors = new List<Integration_Error_Log__c>();
            Map<String, String> fieldApiForErrorLog;
            
            map<id,gii__TransferOrder__c> transferOrder =  new map<id,gii__TransferOrder__c>([select id from gii__TransferOrder__c where  giic_OrderNo__c IN : listTransferOrders]);
            if(transferOrder != null && !transferOrder.isEmpty()) 
                lstErrors  = giic_IntegrationCustomMetaDataUtil.deActivateCurrentError(transferOrder.keySet(),giic_Constants.USER_STORY_TRANSFER_ORDER,'giic_TransferOrder__c');
            
            for(Id transferOrderId : transferOrder.keySet())
            {
                gii__TransferOrder__c objSO =  new gii__TransferOrder__c(Id=transferOrderId);          
                fieldApiForErrorLog = new Map<String, String>{'giic_TransferOrder__c'=>'Id'};
                    giic_IntegrationCustomMetaDataUtil.collectErrors(lstErrors, new Set<String>(),giic_Constants.USER_STORY_TRANSFER_ORDER, fieldApiForErrorLog, objSO, giic_Constants.TRANSFER_ORDER_ERROR_CODE, errorMsgList[0]);        
            }
            
            if(!lstErrors.isEmpty())  upsert lstErrors;            
        }
    }
    
    
    
    public static list<giic_TransferOrderWrapper.TransferOrderReceipt> returnResponse(string code, string status, string error,list<giic_TransferOrderWrapper.TransferOrderReceipt> TransferOrderReceipts){
        
        if(TransferOrderReceipts == null)
            TransferOrderReceipts = new list<giic_TransferOrderWrapper.TransferOrderReceipt>();
        
        for(giic_TransferOrderWrapper.TransferOrderReceipt tor : TransferOrderReceipts)
        {
            tor.Status = status;
            tor.error = error ;
            tor.code = code ;
        }
        
        return  TransferOrderReceipts;               
    }
    
   
    private static void updateRecipetQueues(map<String,gii__ReceiptQueue__c> recieptQueues,list<giic_TransferOrderWrapper.TransferOrderReceipt>  recieptLines)
    {
        try{
            list<gii__ReceiptQueue__c> lstUpdateRecieptQueues = new list<gii__ReceiptQueue__c>();
            gii__ReceiptQueue__c recQueue;
            for(giic_TransferOrderWrapper.TransferOrderReceipt reciept : recieptLines) //request reciepts 
            {
                if(reciept.Error == null)
                {
                    for(giic_TransferOrderWrapper.TransferOrderRecieptLine recLine: reciept.TransferOrderReceiptLines)
                    {
                        if(recieptQueues != null && recieptQueues.containsKey(recLine.TORL_OrderLineNumber))
                        {
                            recQueue  = recieptQueues.get(recLine.TORL_OrderLineNumber);
                            if(recQueue != null && recLine != null && recLine.TORL_receiptQuantity != recQueue.gii__ReceiptQuantity__c)
                            {
                                recQueue.gii__ReceiptQuantity__c = recLine.TORL_receiptQuantity;
                                lstUpdateRecieptQueues.add(recQueue);
                            }
                        }
                    }
                }
                
            }
            
            update lstUpdateRecieptQueues;
        }catch(Exception ex) { throw ex; }
    }
    
    public static  List<String> handleExceptions(gii.TransferOrderReceipt.TransferOrderReceiptsResult resultObj)
    {
        List<String> errorMsgList = new List<String>();
        for(gii.TransferOrderReceipt.GOMException e : resultObj.Exceptions) { 
            //errorMsgList.add(e.getMessage());
            
        }
        return errorMsgList;
        
    }
    
    
    
    public static Map<String,Object> createTransferOrderReceiptGlovia(list<String> transferOrderIds,list<gii__ReceiptQueue__c>  recieptQueues) 
    {
        //input object for reciept creation 
        gii.TransferOrderReceipt.TransferOrderReceiptsResult gloviaResult = new gii.TransferOrderReceipt.TransferOrderReceiptsResult();
        List<String> errorMsgList = new  List<String>(); 
        map<String,object> mapResult = new map<String,object>();
        try
        {
            set<id> setRecievingWarehouseIds = new set<id> ();
            map<id,id> mapWarehouseLoc = new map<id,id>();
            map<id,id> mapReceiptQueueLocation = new map<id,id>();
            if(recieptQueues !=null && !recieptQueues.isEmpty())
            {
                for(gii__ReceiptQueue__c recQueue : recieptQueues)
                { setRecievingWarehouseIds.add(recQueue.gii__TransferOrder__r.gii__TransferToWarehouse__c); }
               
                list<gii__Location__c> lstLocations = giic_CommonUtility.getLocations(setRecievingWarehouseIds);
                for(gii__Location__c loc  : lstLocations)
                { mapWarehouseLoc.put(loc.gii__Warehouse__c,loc.id); }
                if(mapWarehouseLoc != null && !mapWarehouseLoc.isEmpty())
                {
                    for(gii__ReceiptQueue__c recQueue : recieptQueues)
                    {
                        mapReceiptQueueLocation.put(recQueue.id,mapWarehouseLoc.get(recQueue.gii__TransferOrder__r.gii__TransferToWarehouse__c)); 
                    }
                    
                    //input object for reciept creation 
                    gii.TransferOrderReceipt.ReceiptCreationInput inputObj = new gii.TransferOrderReceipt.ReceiptCreationInput  ();
                    inputObj.mapReceiptQueueLocation = mapReceiptQueueLocation;
                    inputObj.ReceiptDate = system.today();
                    gloviaResult = gii.TransferOrderReceipt.getReceiptsForReceiptQueues(inputObj);
                    
                    if(gloviaResult.Exceptions != null && !gloviaResult.Exceptions.isEmpty())
                    { errorMsgList = handleExceptions(gloviaResult); }
                    else  { } //generate success result 
                }
                else { errorMsgList.add(giic_Constants.WAREHOUSE_LOC_NOT_AVALAIBLE);  } //for all transfer orders 
            }
            else  { errorMsgList.add(giic_Constants.NO_RECIEPT);} //no reciept queues comes in query //for all transfer orders
            
            
        }
        catch(exception ex) { errorMsgList.add('ex:' + ex.getMessage() + 'at line no:' + ex.getLineNumber()); }
        
        if(errorMsgList != null && !errorMsgList.isEmpty()) mapResult.put(giic_Constants.ERRORLIST,errorMsgList);
        else mapResult.put(giic_Constants.SUCCESS,true);
        
        return mapResult;
        
    }    
}