Public class giic_GenerateRewardPoints
{
    public list<giic_MembersLoyaltyPoints__c> LstOFMembersLoyaltyPoints{get;set;}
    public Map<id,WrapRewardPointAccount> DataMap{get;set;}
    public string RewardPointsGenerationFinished{get;set;}
    public list<giic_MembershipType__c> MembershipSegmentLst{get;set;}
    public giic_GenerateRewardPoints()
    {
       init();
    }
    public static void updateRewardPoints(list<giic_MembersLoyaltyPoints__c> lstMembersLoyaltyPoints,list<Account> lstAccount,list<Id> setMembershipSegmentId){
		  list<giic_RewardPointConfig__c> lstRewardPointConfig=[select id,giic_MembershipType__c,giic_NumOfPtsToGenRewardPts__c,giic_RewardPointVal__c from giic_RewardPointConfig__c where  giic_MembershipType__c in :setMembershipSegmentId];
         Map<id,list<giic_RewardPointConfig__c>> MembershipTypeRewardPointConfig = new Map<id,list<giic_RewardPointConfig__c>>();
        for(giic_RewardPointConfig__c RewardPointConfigObj : lstRewardPointConfig)
        {
            list<giic_RewardPointConfig__c> TempLst = new list<giic_RewardPointConfig__c>();
            TempLst.add(RewardPointConfigObj);
            if(MembershipTypeRewardPointConfig.containskey(RewardPointConfigObj.giic_MembershipType__c))
            {
                TempLst.addall(MembershipTypeRewardPointConfig.get(RewardPointConfigObj.giic_MembershipType__c));
                MembershipTypeRewardPointConfig.put(RewardPointConfigObj.giic_MembershipType__c,TempLst);
            }
            else
            {
                MembershipTypeRewardPointConfig.put(RewardPointConfigObj.giic_MembershipType__c,TempLst);
            }
        }
        Map<giic_MembersLoyaltyPoints__c,giic_RewardPointConfig__c> QualifiedMembersForRewardPoint = new Map<giic_MembersLoyaltyPoints__c,giic_RewardPointConfig__c>();
        for(giic_MembersLoyaltyPoints__c TempObj : lstMembersLoyaltyPoints)
        {
           // Members_Loyalty_Points__c TempObj = (Members_Loyalty_Points__c) s;
            if(MembershipTypeRewardPointConfig.containskey(TempObj.giic_Account__r.giic_TypeOfMembership__c))
            {
                for(giic_RewardPointConfig__c RewardPointConfigTempObj : MembershipTypeRewardPointConfig.get(TempObj.giic_Account__r.giic_TypeOfMembership__c))
                {
                    if(TempObj.giic_CurrentPoints__c >= RewardPointConfigTempObj.giic_NumOfPtsToGenRewardPts__c)
                    {
                        QualifiedMembersForRewardPoint.put(TempObj,RewardPointConfigTempObj);
                        break;
                    }
                }
            }
        }
    }
    public void init()
    {
       RewardPointsGenerationFinished = 'none';
       DataMap = new map<id,WrapRewardPointAccount>();
       LstOFMembersLoyaltyPoints = new list<giic_MembersLoyaltyPoints__c>();
       String query= 'select id,name, giic_Account__c,giic_Account__r.name,giic_Account__r.giic_TypeOfMembership__c,giic_Account__r.giic_TypeOfMembership__r.name,giic_CurrentPoints__c,giic_LifeTimePoints__c,(select id,name,giic_MembersLoyaltyPts__c,giic_SalesOrderPayment__c,giic_PointRedeemed__c,giic_PointsEarned__c from LoyaltyPointTracker__r where giic_PointRedeemed__c = false ORDER BY giic_PointsEarned__c DESC) from giic_MembersLoyaltyPoints__c';
       LstOFMembersLoyaltyPoints = (list<giic_MembersLoyaltyPoints__c>)Database.Query(query);
       if(!(LstOFMembersLoyaltyPoints.isempty()))
       for(giic_MembersLoyaltyPoints__c tempObj :LstOFMembersLoyaltyPoints)
       {
           WrapRewardPointAccount TempWrapObj = new WrapRewardPointAccount();
           TempWrapObj.LoyaltyPointObj = tempObj;
           TempWrapObj.RowColor = 'white';
           DataMap.put(tempObj.giic_Account__c,TempWrapObj);
       }
       MembershipSegmentLst = new list<giic_MembershipType__c>();
       MembershipSegmentLst = [select id,name,giic_QualifyingPoints__c from giic_MembershipType__c];
    }
    public void StartRewardPointsGeneration()
    {
        ExecuteRewardPointsGeneration(LstOFMembersLoyaltyPoints);
    }
    
    public void ExecuteRewardPointsGeneration(List<giic_MembersLoyaltyPoints__c> scope)
    {
        list<giic_RewardPointConfig__c> LstOfRewardPointConfig = new list<giic_RewardPointConfig__c>();
        LstOfRewardPointConfig  = [select id,name,giic_RewardPointVal__c,giic_NumOfPtsToGenRewardPts__c,giic_MembershipType__c from giic_RewardPointConfig__c limit 50000];
        Map<id,list<giic_RewardPointConfig__c>> MembershipTypeRewardPointConfig = new Map<id,list<giic_RewardPointConfig__c>>();
        for(giic_RewardPointConfig__c RewardPointConfigObj : LstOfRewardPointConfig)
        {
            list<giic_RewardPointConfig__c> TempLst = new list<giic_RewardPointConfig__c>();
            TempLst.add(RewardPointConfigObj);
            if(MembershipTypeRewardPointConfig.containskey(RewardPointConfigObj.giic_MembershipType__c))
            {
                TempLst.addall(MembershipTypeRewardPointConfig.get(RewardPointConfigObj.giic_MembershipType__c));
                MembershipTypeRewardPointConfig.put(RewardPointConfigObj.giic_MembershipType__c,TempLst);
            }
            else
            {
                MembershipTypeRewardPointConfig.put(RewardPointConfigObj.giic_MembershipType__c,TempLst);
            }
        }
        Map<giic_MembersLoyaltyPoints__c,giic_RewardPointConfig__c> QualifiedMembersForRewardPoint = new Map<giic_MembersLoyaltyPoints__c,giic_RewardPointConfig__c>();
        for(giic_MembersLoyaltyPoints__c TempObj : scope)
        {
           // Members_Loyalty_Points__c TempObj = (Members_Loyalty_Points__c) s;
            if(MembershipTypeRewardPointConfig.containskey(TempObj.giic_Account__r.giic_TypeOfMembership__c))
            {
                for(giic_RewardPointConfig__c RewardPointConfigTempObj : MembershipTypeRewardPointConfig.get(TempObj.giic_Account__r.giic_TypeOfMembership__c))
                {
                    if(TempObj.giic_CurrentPoints__c >= RewardPointConfigTempObj.giic_NumOfPtsToGenRewardPts__c)
                    {
                        QualifiedMembersForRewardPoint.put(TempObj,RewardPointConfigTempObj);
                        break;
                    }
                }
            }
        }
        list<giic_RewardPoint__c> LstOfRewardPointToInsert = new list<giic_RewardPoint__c>();
        if(!(QualifiedMembersForRewardPoint.isempty()))
        {
            
            list<giic_LoyaltyPointTracker__c> LoyaltyPointToUpdate = new list<giic_LoyaltyPointTracker__c>();
            list<giic_LoyaltyPointTracker__c> LoyaltyPointToinsert = new list<giic_LoyaltyPointTracker__c>();
            for(giic_MembersLoyaltyPoints__c LoyaltyObj : QualifiedMembersForRewardPoint.keyset())
            {
                decimal CurrentPoints = LoyaltyObj.giic_CurrentPoints__c;
                decimal PointsToBeDeducted = 0.00;
                decimal RewardPointValue = 0.00;
                while(CurrentPoints >= QualifiedMembersForRewardPoint.get(LoyaltyObj).giic_NumOfPtsToGenRewardPts__c)
                {
                    RewardPointValue = RewardPointValue + QualifiedMembersForRewardPoint.get(LoyaltyObj).giic_RewardPointVal__c;
                    CurrentPoints = CurrentPoints - QualifiedMembersForRewardPoint.get(LoyaltyObj).giic_NumOfPtsToGenRewardPts__c;
                    PointsToBeDeducted = PointsToBeDeducted + QualifiedMembersForRewardPoint.get(LoyaltyObj).giic_NumOfPtsToGenRewardPts__c;
                }
                if(RewardPointValue != 0.00)
                {
                    giic_RewardPoint__c RewardPointObj = new giic_RewardPoint__c();
                    RewardPointObj.giic_Account__c = LoyaltyObj.giic_Account__c;
                    RewardPointObj.giic_RewardPointValue__c = RewardPointValue;
                    RewardPointObj.giic_ExpiryDate__c = system.today().addyears(2);
                    LstOfRewardPointToInsert.add(RewardPointObj);
                }
                Decimal TotalPointsDeducted = 0.00;
                giic_LoyaltyPointTracker__c TempRecord = new giic_LoyaltyPointTracker__c();
                while(TotalPointsDeducted < PointsToBeDeducted)
                {
                    for(giic_LoyaltyPointTracker__c LoyaltyPointTrackerObj : LoyaltyObj.LoyaltyPointTracker__r)
                    {
                        LoyaltyPointTrackerObj.giic_PointRedeemed__c = true;
                        LoyaltyPointToUpdate.add(LoyaltyPointTrackerObj);
                        TempRecord = LoyaltyPointTrackerObj;
                        TotalPointsDeducted = TotalPointsDeducted + LoyaltyPointTrackerObj.giic_PointsEarned__c;
                    }
                }
                if(TotalPointsDeducted - PointsToBeDeducted > 0)
                {
                    giic_LoyaltyPointTracker__c RemaningPointRecord = new giic_LoyaltyPointTracker__c();
                    RemaningPointRecord = TempRecord.clone();
                    RemaningPointRecord.giic_PointsEarned__c = TotalPointsDeducted - PointsToBeDeducted;
                    RemaningPointRecord.giic_PointRedeemed__c = false;
                    RemaningPointRecord.giic_RemainingAmtFrmRewardGen__c = true;
                    LoyaltyPointToinsert.add(RemaningPointRecord);
                }
            }
            if(!(LoyaltyPointToUpdate.isempty()))
                    update LoyaltyPointToUpdate;
                    
             if(!(LoyaltyPointToinsert.isempty()))
             insert LoyaltyPointToinsert;
             
            if(!(LstOfRewardPointToInsert.isempty()))
                    insert LstOfRewardPointToInsert;          
        }
        init();
        FinishRewardPointsGeneration(LstOfRewardPointToInsert);
    }
    
    public void FinishRewardPointsGeneration(list<giic_RewardPoint__c> InsertedRewardPoints)
    {
        RewardPointsGenerationFinished = ''; // keep it blank only it will show the column do not set to block, block effect the style
        if(!(InsertedRewardPoints.isempty()))
        {
            for(giic_RewardPoint__c RewardPointTempObj : InsertedRewardPoints)
            {
                if(DataMap.containskey(RewardPointTempObj.giic_Account__c))
                {
                    DataMap.get(RewardPointTempObj.giic_Account__c).RowColor = 'lightblue';
                    DataMap.get(RewardPointTempObj.giic_Account__c).NumberOfRewardPointGenerated= DataMap.get(RewardPointTempObj.giic_Account__c).NumberOfRewardPointGenerated +1;
                    DataMap.get(RewardPointTempObj.giic_Account__c).TotalRewardPointValue= DataMap.get(RewardPointTempObj.giic_Account__c).TotalRewardPointValue + RewardPointTempObj.giic_RewardPointValue__c;
                }
            }
        }
        for(Id DataMapKey : DataMap.keyset())
        {
            if(DataMap.get(DataMapKey).RowColor != 'lightblue')
            {
                DataMap.get(DataMapKey).RowColor = 'lightcoral';
                DataMap.get(DataMapKey).NumberOfRewardPointGenerated= 0;
                DataMap.get(DataMapKey).TotalRewardPointValue= 0.00;
            }
        }
    }
    
    public class WrapRewardPointAccount{
        public giic_MembersLoyaltyPoints__c LoyaltyPointObj{get;set;}
        public string RowColor{get;set;}
        public integer NumberOfRewardPointGenerated{get;set;}
        public decimal TotalRewardPointValue{get;set;}
        public WrapRewardPointAccount()
        {
           LoyaltyPointObj = new  giic_MembersLoyaltyPoints__c ();
           RowColor = null;
           NumberOfRewardPointGenerated = 0;
           TotalRewardPointValue = 0.00;
        }
    }
}