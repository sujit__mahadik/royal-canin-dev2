/************************************************************************************
Version : 1.0
Name : giic_BillToWrapper
Created Date : 18 Sep 2018
Function : Maintain all bill to information.
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/
public class giic_BillToWrapper {
    @AuraEnabled public string firstName;
    @AuraEnabled public string lastName;
    @AuraEnabled public string street;
    @AuraEnabled public string city;
    @AuraEnabled public string state;
    @AuraEnabled public string postalCode;
    @AuraEnabled public string country; 
    @AuraEnabled public string email;
    @AuraEnabled public Boolean IsResidential;
    @AuraEnabled public string OrderNumber; 
    @AuraEnabled public string FulfillmentNumber; 
    @AuraEnabled public string PaymentCurrency; 
    
    public giic_BillToWrapper(){
        firstName = '';
        lastName = '';
        street = '';
        city = '';
        state = '';
        postalCode = '';
        country = '';
        email = '';
        IsResidential = false; 
        OrderNumber = '';
        FulfillmentNumber = '';
        PaymentCurrency = '';
    }
}