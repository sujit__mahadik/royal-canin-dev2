/************************************************************************************
Version : 1.0
Name : giic_IntegrationCMDResultWrapper
Created Date : 20 Aug 2018
Function :  Wrapper class for Conversion of Sales Order Staging to Sales Order
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/
public class giic_IntegrationCMDResultWrapper{
    public SObject targetObj{get;set;}
    public Integer totalChilds{get;set;}
    public String parentFieldApi {get;set;}
    public giic_IntegrationCMDResultWrapper(){
        totalChilds = 0;  
        parentFieldApi = '';  
    }
}