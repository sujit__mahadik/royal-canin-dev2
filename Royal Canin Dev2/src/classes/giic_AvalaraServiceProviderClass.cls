/************************************************************************************
Version : 1.0
Created Date : 17 Sep 2018
Function : Main Class to habdle Avalara Tax Calculations
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/
global class giic_AvalaraServiceProviderClass 
{
    
    
    /* Method name : CalculateTaxCommitFalse
    * Description : This method is used to calculate temp tax for salesorders from Calculate Tax Button
    * Return Type : string
    * Parameter : Sales Order Id
    */
    webservice static string CalculateTaxCommitFalse(String recordId) {
        String result = '';
        try
        {            
            List<gii__SalesOrder__c> lst = new List<gii__SalesOrder__c>();
            lst = [select id,giic_TaxCal__c, gii__Warehouse__r.giic_WarehouseCode__c,
                   gii__Account__r.Check_if_you_are_Tax_Exempt__c,
                   gii__Account__r.Resale_Sales_Tax_Cert_Expiration_Date__c,gii__Released__c,giic_SOStatus__c,giic_TaxExempt__c 
                   from gii__SalesOrder__c where id =: recordId and giic_Source__c !=: giic_Constants.ORDER_TYPE_OLP limit 1];
            if(lst.size()>0)
            {                
                
                if(lst[0].gii__Released__c)
                {
                    result = giic_Constants.SO_Tax_Released;
                }
                else if(lst[0].giic_SOStatus__c == System.Label.giic_SalesOrderCancelledStatus)
                {
                    result = giic_Constants.SO_Tax_Cancelled;
                }
                else if(lst[0].giic_TaxCal__c)
                {
                    result = giic_Constants.AVARALA_ERROR_TAX_CALCUATED;
                }
                else if(lst[0].giic_TaxCal__c != true)
                {                 
                    //Check for Account Tax Exemption before Calculating Tax
                    if(!lst[0].giic_TaxExempt__c)
                    {
                        String endPoint = '';            
                        String body;
                        Boolean isSuccess;
                        Map<String,Object>  returnMap = new Map<String,Object>();
                        List<gii__SalesOrder__c> lstSOs = giic_CommonUtility.getSOwithSolines(new list<id>{recordId},giic_Constants.CREATE_T); 
                        
                        // COMMIT_T_FALSE
                        if(lstSOs != null)
                        {
                            Object[] queryRes = serializeSObjects(lstSOs);                                            
                            body = convertJsonStringToObject(queryRes,giic_Constants.COMMIT_T_FALSE,lst[0].gii__Warehouse__r.giic_WarehouseCode__c);
                            HttpResponse response = giic_CommonUtility.createandSendHTTPPostRequest(endPoint,body);
                            returnMap = getAvalaraResponse(response,recordId,true);
                            //System.debug('returnMap***'+returnMap);
                            isSuccess = (Boolean)returnMap.get('ISSUCCESS');    
                            if(isSuccess)
                            {
                                result = giic_Constants.AVARALA_SUCCESS ;
                            }
                            else
                            {
                                result = giic_Constants.AVARALA_ERROR ;
                                if(returnMap.containsKey('lstError'))
                                {                       
                                    List<Integration_Error_Log__c> lstErrors = new List<Integration_Error_Log__c>();
                                    lstErrors = (List<Integration_Error_Log__c>)returnMap.get('lstError');   
                                    if(!lstErrors.isEmpty()){
                                        insert lstErrors;                           
                                    }
                                }
                            }                    
                        }
                        else
                        {
                            result = giic_Constants.AVARALA_ERROR ;
                        } 
                    }
                    else
                    {
                        result = giic_Constants.AVARALA_ERROR_EXEMPTION ;
                    }
                }                
            }                      
        }
        catch(Exception ex)
        {            
            result = giic_Constants.AVARALA_ERROR ;
            CollectErrors(recordId,ex.getMessage()+'-Line Number '+ex.getLineNumber());
        }
        
        return result;        
    }
    
    /* Method name : CalculateTaxforSO
    * Description : This is utility metyhod to handle avalara tax calculations
    * Return Type : Map<String,Object> - This method returns a map which contains the tarsnation sucess and a RESPONSE from the getAvalaraResponse method
    * Parameter : Sales Order Id (required), Map of SOL and Quantity (optional), Warehouse Code (required) , 
                  TransactionType(required) (values - CREATE_T ,COMMIT_T_FALSE,COMMIT_T_TRUE ) ,
                  UpdateTaxOnSO (to decide whether to update tax on SOL and SO, from the method itself or not)
    */
    public static Map<String,Object> CalculateTaxforSO(String SOId, List<String> lstSOLName, String WarehouseCode, String TransactionType, Boolean UpdateTaxOnSO)
    {       
        Map<String,Object> returnMap = new Map<String,Object>();
        returnMap.put('ISSUCCESS',false);   
        try
        {           
            List<gii__SalesOrder__c> lstSOs = new List<gii__SalesOrder__c>();
            String endPoint= '';
            String body;
            String SOLId;                                        
            if(lstSOLName != null && lstSOLName.size()>0)
            {
                lstSOs = giic_CommonUtility.getSOwithSolinesShipping(SOId,lstSOLName,TransactionType);   
            }
            else
            {
                lstSOs = giic_CommonUtility.getSOwithSolines(new list<id>{SOId},TransactionType);   
            } 
            
            if(lstSOs != null)
            {
                Map<String,Object> mapAvalaraResponse = new Map<String,Object>();
                Object[] queryRes = serializeSObjects(lstSOs);                
                body = convertJsonStringToObject(queryRes,TransactionType,WarehouseCode);
                HttpResponse response = giic_CommonUtility.createandSendHTTPPostRequest(endPoint,body);
                mapAvalaraResponse = getAvalaraResponse(response,SOId,UpdateTaxOnSO); 
                
                if(mapAvalaraResponse.containsKey('ISSUCCESS') && mapAvalaraResponse.get('ISSUCCESS') == true)
                {
                    returnMap.put('ISSUCCESS',true);    
                    returnMap.put('RESPONSE',mapAvalaraResponse);
                }
                else if (mapAvalaraResponse.containsKey('ISSUCCESS') && mapAvalaraResponse.get('ISSUCCESS') == false && mapAvalaraResponse.get('lstError') != null)
                {
                    returnMap.put('ISSUCCESS',false); 
                    returnMap.put('RESPONSE',mapAvalaraResponse.get('lstError'));
                }
            }           
        }
        catch(Exception ex)
        {  
            returnMap.put('ERROR', ex.getMessage()+'-Line Number '+ex.getLineNumber());
        }
        
        return returnMap;
    }   
    
    /* Method name : getAvalaraResponse
    * Description : This method get the avalara response and update SOL and SO accordingly
    * Return Type : Map<String,Object> - This method returns a map which contains the tarsnation sucess and list of SOL and SO
    * Parameter : response (response from avalara service) , soIds (Id of sales order) , UpdateTaxOnSO (to handle whether to update SOL and SO)
                  TransactionType(required) (values - CREATE_T ,COMMIT_T_FALSE,COMMIT_T_TRUE ) ,
                  UpdateTaxOnSO (to decide whether to update tax on SOL and SO, from the method itself or not)
    */
    public static Map<String,Object> getAvalaraResponse(HttpResponse response, String soIds, Boolean UpdateTaxOnSO)
    {
        Map<String,Object>  returnMap = new Map<String,Object>();
        returnMap.put('ISSUCCESS',false);
        Map<String,Object> mapHandleError = new Map<String,Object>();
        try
        {            
            if(response != null)
            {                
                String resBody = response.getBody();
                //System.debug('resBody***'+resBody);
                if(resBody.contains('"error":')){                    
                    mapHandleError = handleError(resBody,soIds); 
                    mapHandleError.put('ISSUCCESS',false);
                    //System.debug('mapHandleError***'+mapHandleError);
                    gii__SalesOrder__c obj = new gii__SalesOrder__c();
                    obj.id = soIds;
                    obj.giic_TaxCal__c = false;
                    update obj;
                    return mapHandleError;
                }
                else
                {                    
                    AvalaraResponseParent avalaraResWrap = parse(resBody);                    
                    returnMap.put('AvalaraResponseParent',avalaraResWrap); 
                    if(avalaraResWrap != null)
                    {   
                        list<gii__SalesOrderLine__c> solines = new list<gii__SalesOrderLine__c>();
                        String soNumber = avalaraResWrap.code;
                        Map<String,AvalaraReponceChild> mapSOLIToAvalaraRespose = new Map<String,AvalaraReponceChild>();
                        for(AvalaraReponceChild avaChild : avalaraResWrap.lines)
                        {
                            mapSOLIToAvalaraRespose.put(avaChild.lineNumber, avaChild);
                        }      
                        returnMap.put('mapSOLIToAvalaraRespose',mapSOLIToAvalaraRespose); 
                        solines = [select id,name, gii__LineTaxRateId__c, gii__SalesOrder__c from gii__SalesOrderLine__c where Name IN : mapSOLIToAvalaraRespose.keySet() and gii__SalesOrder__c = :soIds and gii__CancelReason__c = null];
                        list<gii__SalesOrder__c> lstSOUpdate = new list<gii__SalesOrder__c>();                      
                        set<id> setsotoupdateTemp = new set<id>();
                        for(gii__SalesOrderLine__c soline : solines)
                        {
                            soline.gii__TaxAmount__c =  (mapSOLIToAvalaraRespose != null && mapSOLIToAvalaraRespose.containsKey(soline.Name))? mapSOLIToAvalaraRespose.get(soline.Name).tax : 0 ;                         
                            if(setsotoupdateTemp != null && !setsotoupdateTemp.contains(soline.gii__SalesOrder__c))
                            {                                
                                setsotoupdateTemp.add(soline.gii__SalesOrder__c); 
                                lstSOUpdate.add(new gii__SalesOrder__c(id = soline.gii__SalesOrder__c, giic_TaxCal__c = true));
                            }
                        }                      
                        if(UpdateTaxOnSO)
                        {
                            if(solines != null && solines.size() > 0)
                                update solines;
                            if(lstSOUpdate != null && lstSOUpdate.size() > 0)
                                update lstSOUpdate;
                        }
                        else
                        {
                            returnMap.put('solines',solines);   
                            returnMap.put('lstSOUpdate',solines); 
                        }
                        returnMap.put('ISSUCCESS',true);                        
                        return returnMap;
                    }                   
                }
                
            }
        }
        catch(Exception ex)
        {            
            throw ex;
        }
        return returnMap;
        
    }
    
    
    /* Method name : serializeSObjects
    * Description : This method is used to serialize the sobject into json
    * Return Type : Object[] - an array of serialized JSON object
    * Parameter : an array of SObject[]
    */
    public static Object[] serializeSObjects(SObject[] recs)
    {
        Object[] recObjects;
        if(recs != null)  recObjects = (Object[])JSON.deserializeUntyped(JSON.serialize(recs));
        return recObjects;
        
    } 
    
    /* Method name : handleError
    * Description : This method is used to handle error during avalara service response and store it in integration log
    * Return Type : void
    * Parameter : resbody of avalara error response, id of sales order
    */
    public static Map<String,Object> handleError(String resBody , String soId)
    {     
        Map<String,Object> mapHandleError = new Map<String,Object>();
        giic_IntegrationCustomMetaDataUtil.deActivateCurrentErrorSO(soId,giic_Constants.USER_STORY_TAX_CALC);
        List<Integration_Error_Log__c> lstErrors = new List<Integration_Error_Log__c>();
        Set<String> setErrorIds = new Set<String>();
        String userStory = giic_Constants.USER_STORY_TAX_CALC;
        Map<String, String> fieldApiForErrorLog = new Map<String, String>{'giic_SalesOrder__c'=>'Id'};
            gii__SalesOrder__c objSO =  new gii__SalesOrder__c(Id=soId);    
        Map<String, Object> errorList = (Map<String, Object>)JSON.deserializeUntyped(resBody);     
        //giic_IntegrationCustomMetaDataUtil.collectErrors(lstErrors, setErrorIds, userStory, fieldApiForErrorLog, objSO, 'AuthenticationException', 'Authentication failed.');      
        giic_IntegrationCustomMetaDataUtil.collectErrors(lstErrors, setErrorIds, userStory, fieldApiForErrorLog, objSO,giic_Constants.TAX_CALC_ERROR_CODE, resBody);        
        if(!lstErrors.isEmpty()){
            //insert lstErrors;
            mapHandleError.put('lstError',lstErrors);
        }
        return mapHandleError;
    }
    
    /* Method name : CollectErrors
    * Description : This method is used to log all errors into integration log
    * Return Type : void
    * Parameter : id of sales order, error string
    */
    public static void CollectErrors(String soId, String error)
    { 
        Map<String,Object> mapHandleError = new Map<String,Object>();
        giic_IntegrationCustomMetaDataUtil.deActivateCurrentErrorSO(soId,giic_Constants.USER_STORY_TAX_CALC);
        List<Integration_Error_Log__c> lstErrors = new List<Integration_Error_Log__c>();
        Set<String> setErrorIds = new Set<String>();
        String userStory = giic_Constants.USER_STORY_TAX_CALC;
        Map<String, String> fieldApiForErrorLog = new Map<String, String>{'giic_SalesOrder__c'=>'Id'};
            gii__SalesOrder__c objSO =  new gii__SalesOrder__c(Id=soId);          
        giic_IntegrationCustomMetaDataUtil.collectErrors(lstErrors, setErrorIds, userStory, fieldApiForErrorLog, objSO,giic_Constants.TAX_CALC_ERROR_CODE, error);        
        if(!lstErrors.isEmpty()){           
            insert lstErrors;
        }
    }
    
    /* Method name : convertJsonStringToObject
    * Description : This method is used to create avalara request body
    * Return Type : String - json avalara request
    * Parameter : json object list, transaction type (whetheer create, non commmit or commit), map of SOL and its quanitity for tax calulation, WarehouseCode
    */
    public static String convertJsonStringToObject(List<Object> jsonObject, String transactionType,String WarehouseCode){
        String jsonOut;
        try
        {
            SORecord soRecord; 
            gii__SystemPolicy__c systemPolicyObj = giic_CommonUtility.getSystemPolicyData();
            List<gii__Warehouse__c> lstWarehouse = new List<gii__Warehouse__c>();            
            lstWarehouse = [select id,gii__WareHouseStreet__c,gii__WareHouseCity__c,gii__WareHouseStateProvince__c,gii__WareHouseCountry__c,giic_WarehouseCode__c,gii__WareHouseZipPostalCode__c from gii__Warehouse__c where giic_WarehouseCode__c =:WarehouseCode limit 1];            
            if(lstWarehouse.size()>0)
            {                
                //Since we get only 1 object in list so we can directly get element 0 without iteration, so get Map out of jsonObject
                //Create SO Object through SORecord Wrapper class through initialising values
                Map<String, Object> mapSO = (Map<String, Object>) jsonObject[0]; 
               
                AddressWrapper shipToLocation = new AddressWrapper(String.valueOf(mapSO.get('gii__ShipToName__c')), String.valueOf(mapSO.get('gii__ShipToCity__c')), String.valueOf(mapSO.get('gii__ShipToStateProvince__c')), String.valueOf(mapSO.get('gii__ShipToCountry__c')), String.valueOf(mapSO.get('gii__ShipToZipPostalCode__c')));
                
                AddressWrapper shipFromLocation = new AddressWrapper(String.valueOf(lstWarehouse[0].giic_WarehouseCode__c),
                                                                     String.valueOf(lstWarehouse[0].gii__WareHouseCity__c),
                                                                     String.valueOf(lstWarehouse[0].gii__WareHouseStateProvince__c),
                                                                     giic_Constants.SHIP_COUNTRY,
                                                                     String.valueOf(lstWarehouse[0].gii__WareHouseZipPostalCode__c));
                
                
                AddressType address = new AddressType(shipToLocation,shipFromLocation);         
                Map<String, Object> mapAccount = (Map<String, Object>)mapSO.get('gii__Account__r');     
                
                String customerCode = '' ;
                if(mapAccount.get('ShipToCustomerNo__c') != null && mapAccount.get('ShipToCustomerNo__c') != '')
                {
                     customerCode = String.valueOf(mapAccount.get('ShipToCustomerNo__c'));       
                }
                
                if(transactionType.EqualsIgnoreCase(giic_Constants.CREATE_T)){
                    soRecord = new SORecord(systemPolicyObj.giic_AvalaraCompanyCode__c, String.valueOf(mapSO.get('gii__PauseCode__c')),giic_Constants.AVALARA_TNS_SO,System.today(),customerCode,address,false);
                }else if(transactionType.EqualsIgnoreCase(giic_Constants.COMMIT_T_FALSE)){
                    soRecord = new SORecord(systemPolicyObj.giic_AvalaraCompanyCode__c, String.valueOf(mapSO.get('gii__PauseCode__c')), giic_Constants.AVALARA_TNS_INV,System.today(),customerCode,address,false);
                }
                else if(transactionType.EqualsIgnoreCase(giic_Constants.COMMIT_T_TRUE)){
                    soRecord = new SORecord(systemPolicyObj.giic_AvalaraCompanyCode__c, String.valueOf(mapSO.get('gii__PauseCode__c')), giic_Constants.AVALARA_TNS_INV,System.today(),customerCode,address,true);
                }
                
                //Initialising SOL line now, get attribute 'SalesOrederLine__r'
                Map<String, Object> mapSOL = (Map<String, Object>)mapSO.get('gii__SalesOrder__r');
                SOLRecord[] solRecords =  new SOLRecord[]{};                     
                List<Object> recordsList = (mapSOL != null && mapSOL.containsKey('records'))? (List<Object>)mapSOL.get('records') : null;                
                Integer quantity;
                Integer amount;
                if(recordsList != null)
                {
                    //iterate through records of mapSOL to get line details
                    for(Object objSOL: recordsList)
                    {
                        Map<String, Object> mapSOLRecords = (Map<String, Object>)objSOL;                      
                        if(transactionType.equals(giic_Constants.CREATE_T)){
                            quantity = Integer.valueOf(mapSOLRecords.get('gii__OrderQuantity__c'));
                        }else if(transactionType.equals(giic_Constants.COMMIT_T_FALSE)){
                            quantity = Integer.valueOf(mapSOLRecords.get('gii__OrderQuantity__c'));    
                        }
                        else if(transactionType.equals(giic_Constants.COMMIT_T_TRUE)){
                            quantity = Integer.valueOf(mapSOLRecords.get('gii__ShippedQuantity__c')); //in case of commit, tax is calculated for shipped Qty
                        }
                        amount = Integer.valueOf(mapSOLRecords.get('gii__ProductAmount__c')) - Integer.valueOf(mapSOLRecords.get('gii__DiscountAmount__c'));
                        SOLRecord solRecord =  new SOLRecord(String.valueOf(mapSOLRecords.get('Name')),quantity,amount,address);
                        solRecords.add(solRecord);                                          
                    }
                    
                    soRecord.lines = (solRecords != null ) ? solRecords.clone() : null;
                    jsonOut = Json.serialize(soRecord);
                }
                /* we need to replace some variable as per avalara request */
                if(jsonOut != null)
                {
                    jsonOut = jsonOut.replaceAll('"lineNumber":','"number":');
                    jsonOut = jsonOut.replaceAll('"transactiondate":','"date":');
                    jsonOut = jsonOut.replaceAll('"commitTransaction":','"commit":');
                }
            }
        }
        catch(Exception ex) { throw ex; }
        return jsonOut;
    }     
    
    /********************************* Response Wrapper **************************************************************************/
    
    
    /* Wrapper class to store Avalara Parent Response */    
    public class AvalaraResponseParent{
        public Long id; 
        public String code; 
        public String companyId;    
        public Date transactiondate;
        public String status;   
        public String type; 
        public String currencyCode;
        public String customerVendorCode;   
        public String customerCode;
        public boolean reconciled;
        public String locationCode; 
        public String salespersonCode;  
        public String taxOverrideType;  
        public Double totalAmount;  
        public Double totalTax; 
        public Double totalTaxable;
        public Double totalTaxCalculated;   
        public String adjustmentReason; 
        public String region;   
        public String country;  
        public String originAddressId;  
        public String destinationAddressId; 
        public String description;  
        public String taxDate;  
        public AvalaraReponceChild[] lines;
        public Addresses[] addresses;
        public LocationTypes[] locationTypes;
        public Summary[] summary;
        public Messages[] messages;
        public InvoiceMessages[] invoiceMessages;
        
    }
    
    /* Wrapper class to store Avalara Child Response */  
    public class AvalaraReponceChild {
        public String id;   
        public String transactionId;    
        public String lineNumber;   
        public String description;  
        public String destinationAddressId; 
        public String originAddressId;  
        public String discountAmount;   
        public boolean isItemTaxable;
        public String itemCode; 
        public String lineAmount;   
        public String quantity; 
        public String ref1; 
        public String reportingDate;    
        public String sourcing; 
        public Double tax;  
        public Double taxableAmount;    
        public Double taxCalculated;    
        public String taxCode;  
        public String taxDate;  
        public String taxOverrideType;  
        public Details[] details;
        public NonPassthroughDetails[] nonPassthroughDetails;
        public LineLocationTypes[] lineLocationTypes;
    }
    
    /* Wrapper class to store Avalara Details */  
    class Details {
        public String id;   
        public String transactionLineId;    
        public String transactionId;    
        public String addressId;    
        public String country;  
        public String region;   
        public String stateFIPS;    
        public String exemptReasonId;   
        public String jurisCode;    
        public String jurisName;    
        public String jurisdictionId;   
        public String signatureCode;    
        public String jurisType;    
        public String nonTaxableType;   
        public Double rate; 
        public String rateRuleId;   
        public String rateSourceId; 
        public String sourcing; 
        public Double tax;  
        public String taxableAmount;    
        public String taxType;  
        public String taxName;  
        public String taxAuthorityTypeId;   
        public String taxRegionId;  
        public Double taxCalculated;    
        public String rateType; 
    }
    
    /* Wrapper class to store Avalara NonPassthrough Details */  
    class NonPassthroughDetails {
        public String id;   
        public String transactionLineId;    
        public String transactionId;    
        public String addressId;    
        public String country;  
        public String region;   
        public String stateFIPS;    
        public String exemptReasonId;   
        public String jurisCode;    
        public String jurisName;    
        public String jurisdictionId;   
        public String signatureCode;    
        public String jurisType;    
        public String nonTaxableType;   
        public Double rate; 
        public String rateRuleId;   
        public String rateSourceId; 
        public String sourcing; 
        public Double tax;  
        public Double taxableAmount;    
        public String taxType;  
        public String taxName;  
        public String taxAuthorityTypeId;   
        public String taxRegionId;  
        public Double taxCalculated;    
        public String rateType; 
    }
    
    /* Wrapper class to store Avalara LineLocationTypes  */  
    class LineLocationTypes {
        public String documentLineLocationTypeId;   
        public String documentLineId;   
        public String documentAddressId;    
        public String locationTypeCode; 
    }
    
    /* Wrapper class to store Avalara Addresses  */  
    class Addresses {
        public String boundaryLevel;    
        public String line1;    
        public String city; 
        public String region;   
        public String postalCode;   
        public String country;  
    }
    
    /* Wrapper class to store Avalara LocationTypes  */
    class LocationTypes {
        public String documentLocationTypeId;   
        public String documentId;   
        public String documentAddressId;    
        public String locationTypeCode; 
    }
    
    /* Wrapper class to store Avalara Summary  */
    class Summary {
        public String country;  
        public String region;   
        public String jurisType;    
        public String jurisCode;    
        public String jurisName;    
        public String taxAuthorityType; 
        public String taxType;  
        public String taxName;  
        public String rateType; 
        public String rateTypeCode; 
        public Double taxable;  
        public Double rate;
        public Double tax;  
        public Double taxCalculated;    
    }
    
    /* Wrapper class to store Avalara Messages  */
    class Messages {
        public String summary;  
        public String details;  
        public String refersTo; 
        public String severity; 
        public String source;   
    }
    
    /* Wrapper class to store Avalara InvoiceMessages  */
    class InvoiceMessages {
        public String content;  
        public LineNumbers[] lineNumbers;
    }
    
    
    class LineNumbers {
    }
    
   /* Method name : AvalaraResponseParent
    * Description : This method is used to parse json to AvalaraResponseParent
    * Return Type : AvalaraResponseParent
    * Parameter : json object list, transaction type (whetheer create, non commmit or commit), map of SOL and its quanitity for tax calulation, WarehouseCode
    */      
    public static AvalaraResponseParent parse(String json){
        if(json.contains('"date"')){
            json = json.replaceAll('"date":','"transactiondate":');
        }
        return (AvalaraResponseParent) System.JSON.deserialize(json, AvalaraResponseParent.class);
    }
    
    /* Wrapper class to store Sales Order Details  */
    public class SORecord {
        public String companyCode, code, type,customerCode;
        public boolean commitTransaction = false;
        public date transactiondate;        
        public AddressType addresses;       
        public SOLRecord[] lines;
        public SORecord(String companyCode, String code, String type, Date transactiondate,String customerCode,AddressType addresses,Boolean commitTransaction) {
            this.companyCode = companyCode;
            this.code = code;
            this.type = type;
            this.transactiondate = transactiondate;
            this.customerCode = customerCode;            
            this.addresses = addresses;         
            this.commitTransaction = commitTransaction;
            
        }
    }
    
    /* Wrapper class to store Sales Order Lines Details  */
    public class SOLRecord {
        
        public String lineNumber;
        public Decimal quantity, amount;
        public AddressType addresses;
        public SOLRecord(String lineNumber, Decimal quantity, Decimal amount,AddressType addresses) {
            this.lineNumber = lineNumber;
            this.quantity = quantity;
            this.amount = amount;
            this.addresses = addresses; 
        }
    } 
    
    public Class AddressType
    {
        public AddressWrapper shipFrom;     
        public AddressWrapper shipTo;
        public AddressType(AddressWrapper shipFromLocation,AddressWrapper shipToLocation) {
            this.shipFrom = shipFromLocation;
            this.shipTo = shipToLocation;
        }
    }
    
    /* Wrapper class to store Address Details  */
    public Class AddressWrapper
    {
        public String  line1;
        public String  city;
        public String  region;
        public String  country;
        public String  postalCode;
        
        public AddressWrapper(String line1, String city, String region ,String country, String postalcode) {
            this.line1 = line1;
            this.city = city;
            this.region = region;
            this.country = country;
            this.postalcode = postalcode;
        }
    }
    
    Public Class AvaralaBatch
    {
        Public String id;
        Public String accountId;
        Public String companyId;
        Public String name;
        Public String type;
        Public String batchAgent;
        Public list<File> files;
    }
    
    Public Class File
    {
        public String name;
        public String content;
        public String contentType;
        public String fileExtension;
        public String filePath;
    }
}