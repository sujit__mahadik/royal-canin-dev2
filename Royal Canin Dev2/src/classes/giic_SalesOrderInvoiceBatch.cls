/************************************************************************************
Version : 1.0
Created Date : 19 Sep 2018
Function : Batch class to create bulk invoices from Forward to Invoice Queue records
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/
global class giic_SalesOrderInvoiceBatch implements Database.Batchable<sObject>
{
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        string query;
        query= 'Select Name, Id, gii__SalesOrder__c, gii__SalesOrderLine__c,gii__ServiceOrderLine__c, gii__SalesOrderAdditionalCharge__c';
        query+=' From gii__ForwardToInvoiceQueue__c';
        System.debug('query==='+query);
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC,List<sObject> scope)
    {
        Set<Id> setSOID = new Set<Id>();
        for(sObject s : scope)
        {
            gii__ForwardToInvoiceQueue__c FTIQ = (gii__ForwardToInvoiceQueue__c)s;                        
            setSOID.add(FTIQ.gii__SalesOrder__c);              
        }   
        if(setSOID.size()>0)
        {
            giic_InvoiceUtility.CreateInvoices(setSOID);    
        }
    }    
    global void finish(Database.BatchableContext BC){        
        
    }
}