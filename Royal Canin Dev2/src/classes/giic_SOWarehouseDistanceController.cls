/************************************************************************************
Version : 1.0
Created Date : 21-Aug-2018
Function : Apex controller to incorporate the server side functionality
		   for giic_SOWarehouseDistance component
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/

public class giic_SOWarehouseDistanceController {  
    /*
    * Method name : getData
    * Description : get Warehouse Information with distances
    * Return Type : AccountWarehouseWrapper
    * Parameter : salesOrderId = > sales order id
    */
    @AuraEnabled
    public Static AccountWarehouseWrapper getData( String salesOrderId){
        AccountWarehouseWrapper objWrapper = new AccountWarehouseWrapper(); 
        list<gii__SalesOrder__c> lstSalesOrderInfo = [SELECT Id,gii__DropShip__c,gii__ShipToZipPostalCode__c, gii__Account__c,gii__AccountReference__r.giic_WarehouseGroup__c, gii__Account__r.Name,gii__Account__r.ShippingLatitude,gii__Account__r.ShippingLongitude
                                         FROM gii__SalesOrder__c
                                         WHERE Id =: salesOrderId];
        if(lstSalesOrderInfo != NULL && lstSalesOrderInfo.size() > 0){
            objWrapper.AccountId = lstSalesOrderInfo[0].gii__Account__c; 
            objWrapper.AccountName = lstSalesOrderInfo[0].gii__Account__r.Name; 
            decimal latitude=lstSalesOrderInfo[0].gii__Account__r.ShippingLatitude;
            decimal longitude=lstSalesOrderInfo[0].gii__Account__r.ShippingLongitude;
            if(!lstSalesOrderInfo[0].gii__DropShip__c && latitude!= null && longitude != null){
                objWrapper.lstWarehouseWrapper = getWarehouseList(latitude,longitude,lstSalesOrderInfo[0].gii__AccountReference__r.giic_WarehouseGroup__c);
            }
            else if(lstSalesOrderInfo[0].gii__ShipToZipPostalCode__c != null ){
                string zipCode=(lstSalesOrderInfo[0].gii__ShipToZipPostalCode__c).left(5);
                list<giic_ZipGeolocation__c> lstZipGeoLoc = [select id,giic_Geolocation__Latitude__s,giic_Geolocation__Longitude__s,giic_ZipCode__c from giic_ZipGeolocation__c where giic_ZipCode__c = :zipCode and giic_Geolocation__Latitude__s != null and giic_Geolocation__Longitude__s != null];
                if(lstZipGeoLoc != null && lstZipGeoLoc.size() > 0)
                    objWrapper.lstWarehouseWrapper = getWarehouseList(lstZipGeoLoc[0].giic_Geolocation__Latitude__s,lstZipGeoLoc[0].giic_Geolocation__Longitude__s,lstSalesOrderInfo[0].gii__AccountReference__r.giic_WarehouseGroup__c);
            }
        }    
        return objWrapper; 
    }
    /*
    * Method name : getWarehouseList
    * Description : get warehouse list with distances
    * Return Type : AccountWarehouseWrapper
    * Parameter : latitude = > lattitude of main warehouse, longitude = > longitude of main warehouse ,whGroupId=> warehouse group id
    */ 
    @AuraEnabled
    public static list<WarehouseWrapper> getWarehouseList(decimal latitude,Decimal longitude,Id whGroupId){
        List<gii__Warehouse__c> lstWarehouse = new List<gii__Warehouse__c>();
        List<WarehouseWrapper> lstWHWrap = new List<WarehouseWrapper>();
        lstWarehouse = [Select Id, Name, gii__WareHouseStreet__c, gii__WareHouseCity__c, gii__WareHouseStateProvince__c, gii__WareHouseZipPostalCode__c, 
                        gii__WareHouseCountry__c, gii__Geolocation__c, DISTANCE(gii__Geolocation__c, GEOLOCATION(:latitude,:longitude), 'mi') totalDistance
                        from gii__Warehouse__c where gii__WarehouseGroup__c =: whGroupId
                        order by DISTANCE(gii__Geolocation__c, GEOLOCATION(:latitude,:longitude), 'mi') asc]; // 'mi'  //'km'*/
        
        if(!lstWarehouse.isEmpty()){
            for(gii__Warehouse__c objWarehouse : lstWarehouse){
                if(objWarehouse.get('totalDistance') != Null){
                    System.debug(':::objWarehouse='+ objWarehouse);
                    WarehouseWrapper objW = new WarehouseWrapper();
                    objW.Id = objWarehouse.Id;
                    objW.Name = objWarehouse.Name;                        
                    objW.ShipmentDistance = Decimal.valueOf(String.valueOf(objWarehouse.get('totalDistance'))).setScale(0);
                    String address =  objWarehouse.gii__WareHouseStreet__c != Null ? objWarehouse.gii__WareHouseStreet__c  + ', ' : '';
                    address +=  objWarehouse.gii__WareHouseCity__c != Null ? objWarehouse.gii__WareHouseCity__c + ', ' : '';
                    address += objWarehouse.gii__WareHouseStateProvince__c  != Null ? objWarehouse.gii__WareHouseStateProvince__c + ', ' : '';
                    address += objWarehouse.gii__WareHouseZipPostalCode__c  != Null ? objWarehouse.gii__WareHouseZipPostalCode__c + ', ' : '';
                    address += objWarehouse.gii__WareHouseCountry__c  != Null ? objWarehouse.gii__WareHouseCountry__c : '';
                    objW.Address = address;
                    objW.Geolocation = String.valueOf(objWarehouse.gii__Geolocation__c);
                    lstWHWrap.add(objW); 
                }
            }
        }
        return lstWHWrap;
    }
    
    public class AccountWarehouseWrapper {
        @AuraEnabled Public String AccountId;
        @AuraEnabled Public String AccountName;
        @AuraEnabled Public List<WarehouseWrapper> lstWarehouseWrapper;    
        
        
        public AccountWarehouseWrapper(){
            this.AccountId = '';
            this.AccountName = '';           
            this.lstWarehouseWrapper = new List<WarehouseWrapper>();
        }
    }
    public class WarehouseWrapper {
        @AuraEnabled Public String Id;
        @AuraEnabled Public String Name;
        @AuraEnabled Public Decimal ShipmentDistance;
        @AuraEnabled Public String Address;
        @AuraEnabled Public String Geolocation;
        
        public WarehouseWrapper(){
            
            this.Id = '';
            this.Name = '';
            this.ShipmentDistance = 0;
            this.Address = '';
            this.Geolocation = '';
        }
    }
    
}