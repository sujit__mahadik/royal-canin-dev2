@IsTest(SeeAllData=true)
private class UpdateProspectAccountBatchtest {
  
 @IsTest static void testUpdateBatch() {
    SetupTestData.removeCustomSettings();
    SetupTestData.createCustomSettings();
    
    Customer_Registration__c reg = new Customer_Registration__c();
    reg.Checked_for_Prospects__c = false;
    reg.Phone_Number__c = '(123) 456-1234';
    reg.Email_Address__c = 'updatetest@test.com';
    reg.Name_of_Business_Organization__c = 'Test Batch';
    reg.ShipToCustomerNo__c = 'VET-090909-001';
    insert reg;

    SetupTestData.createAccounts(2);
    Account acc = SetupTestData.testAccounts[0];
    acc.Name = 'Test Batch';
    acc.Phone = '(123) 456-1234';
    acc.Account_Status__c = 'Prospect';
    acc.Start_Onboarding_Chatter_Post__c = false;
    acc.ShipToCustomerNo__c = 'VET-090909-001';
    update acc;
    
  
 Account acc1 = SetupTestData.testAccounts[1];
    acc1.Name = 'Chatter';
    acc1.Phone = '(123) 456-4444';
    acc1.Account_Status__c = 'Prospect';
    acc1.Start_Onboarding_Chatter_Post__c = false;
    acc1.ShipToCustomerNo__c = 'VET-765432-001';
    update acc1;
    
        Test.startTest();
    Database.executeBatch(new UpdateProspectAccountBatch(true),2);
    Test.stopTest();
    //System.assert(reg.Checked_for_Prospects__c);

  }
}