/************************************************************************************
Version : 1.0
Created Date : 22 Oct 2018
Function : Controller for CreateShipmentOrder component.
Author : Abhishek Tripathi
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/
public with sharing class giic_CreateShipmentOrderController {
    
    /*
* Method name : convertShipmentOrder
* Description : Convert gii__WarehouseShippingOrderStaging__c to gii__WarehouseShipmentAdviceStaging__c
* Return Type : void
* Parameter : Id RecordId
*/
    @auraEnabled
    public static void convertShipmentOrder(String recordId){
        if(recordId != null){
            List<gii__WarehouseShipmentAdviceStaging__c> wsaObjList = [Select Id from gii__WarehouseShipmentAdviceStaging__c where giic_WHSShipNo__c=: recordId];
            if( wsaObjList.size() > 0  ) {
                throw new AuraHandledException(Label.giic_ShipmentAlreadyExists);
            }
            Map<Id, gii__WarehouseShippingOrderLinesStaging__c> shippingOrderLinesMap = new Map<Id, gii__WarehouseShippingOrderLinesStaging__c>([SELECT giic_LineNo__c, 
                                                                                                                                              giic_LineStatus__c, 
                                                                                                                                              giic_Product__r.giic_ProductSKU__c, 
                                                                                                                                              giic_SalesOrderLine__r.name,
                                                                                                                                              giic_SalesOrderLine__r.gii__Carrier__r.giic_UniqueCarrier__c,
                                                                                                                                              giic_SalesOrderLine__r.gii__CancelledQuantity__c,
                                                                                                                                              giic_SalesOrderLine__r.gii__CancelReason__c,
                                                                                                                                              giic_SalesOrderLine__r.gii__OrderQuantity__c,
                                                                                                                                              giic_SalesOrderLine__r.gii__ConversionFactor__c,
                                                                                                                                              giic_SalesOrderLine__r.giic_ProductLot__c,
                                                                                                                                              giic_WHShippingOrderStaging__r.Name, 
                                                                                                                                              giic_WHShippingOrderStaging__r.giic_Carrier__r.giic_UniqueCarrier__c,
                                                                                                                                              giic_WHShippingOrderStaging__r.giic_Warehouse__r.giic_WarehouseCode__c,
                                                                                                                                              giic_WHShippingOrderStaging__r.giic_SalesOrder__r.Name,
                                                                                                                                              giic_WHShippingOrderStaging__r.giic_SalesOrder__r.gii__Account__r.name,
                                                                                                                                              giic_Product__r.gii__StockingUnitofMeasure__r.Name,
                                                                                                                                              Name,
                                                                                                                                              giic_Product__r.giic_ConvFactor__c,
                                                                                                                                              giic_WHShippingOrderStaging__c,
                                                                                                                                              Id 
                                                                                                                                              FROM gii__WarehouseShippingOrderLinesStaging__c where giic_WHShippingOrderStaging__c=:recordId]);
            Map<id, gii__WarehouseShipmentAdviceStaging__c> wsaMap = new Map<id, gii__WarehouseShipmentAdviceStaging__c>();
            Map<id, id> wsoLinetoWsoMap = new Map<Id, id>();
            for(Id wsoLineId : shippingOrderLinesMap.keySet()){
                if(!wsaMap.containsKey(shippingOrderLinesMap.get(wsoLineId).giic_WHShippingOrderStaging__c)){
                    gii__WarehouseShipmentAdviceStaging__c wsaObj = new gii__WarehouseShipmentAdviceStaging__c();
                    wsaObj.giic_WHSShipNo__c = shippingOrderLinesMap.get(wsoLineId).giic_WHShippingOrderStaging__c;
                    wsaObj.giic_Carrier__c = shippingOrderLinesMap.get(wsoLineId).giic_WHShippingOrderStaging__r.giic_Carrier__r.giic_UniqueCarrier__c;
                    wsaObj.giic_Warehouse__c = shippingOrderLinesMap.get(wsoLineId).giic_WHShippingOrderStaging__r.giic_Warehouse__r.giic_WarehouseCode__c;
                    wsaObj.giic_ShipDate__c = System.now().date();
                    wsaObj.giic_isTaxCommitted__c = false;
                    wsaMap.put(shippingOrderLinesMap.get(wsoLineId).giic_WHShippingOrderStaging__c, wsaObj);
                }
                wsoLinetoWsoMap.put(wsoLineId, shippingOrderLinesMap.get(wsoLineId).giic_WHShippingOrderStaging__c);
            }
            

            try{
                insert wsaMap.values();
            }catch(DmlException e){
                System.debug(e.getMessage());
            }
            
            List<gii__WarehouseShipmentAdviceLinesStaging__c> whsAdviceLinesList = new List<gii__WarehouseShipmentAdviceLinesStaging__c>();
            for(gii__WarehouseShippingOrderLinesStaging__c SOLObj : shippingOrderLinesMap.values()){
                gii__WarehouseShipmentAdviceLinesStaging__c wsaLineObject = new gii__WarehouseShipmentAdviceLinesStaging__c();
                wsaLineObject.giic_WHSShipAdviceStaging__c = wsaMap.get(wsoLinetoWsoMap.get(SOLObj.Id)).Id;
                wsaLineObject.giic_WHSShipLineNo__c = SOLObj.Id;
                wsaLineObject.giic_OrderLineNo__c = SOLObj.giic_SalesOrderLine__r.name;
                wsaLineObject.giic_ProductSKU__c = SOLObj.giic_Product__r.giic_ProductSKU__c;
                wsaLineObject.giic_UOM__c = SOLObj.giic_Product__r.gii__StockingUnitofMeasure__r.Name;
            //    wsaLineObject.giic_CancelQty__c = (SOLObj.giic_SalesOrderLine__r.gii__CancelledQuantity__c != null && SOLObj.giic_Product__r.giic_ConvFactor__c != null)? SOLObj.giic_SalesOrderLine__r.gii__CancelledQuantity__c*SOLObj.giic_Product__r.giic_ConvFactor__c : SOLObj.giic_SalesOrderLine__r.gii__CancelledQuantity__c;
            //    wsaLineObject.giic_CancelReason__c = SOLObj.giic_SalesOrderLine__r.gii__CancelReason__c;
                wsaLineObject.giic_OrderQty__c = (SOLObj.giic_SalesOrderLine__r.gii__OrderQuantity__c != null && SOLObj.giic_SalesOrderLine__r.gii__ConversionFactor__c != null)? SOLObj.giic_SalesOrderLine__r.gii__OrderQuantity__c*SOLObj.giic_SalesOrderLine__r.gii__ConversionFactor__c : SOLObj.giic_SalesOrderLine__r.gii__OrderQuantity__c;
                wsaLineObject.giic_ShipQty__c = (SOLObj.giic_SalesOrderLine__r.gii__OrderQuantity__c != null && SOLObj.giic_SalesOrderLine__r.gii__ConversionFactor__c != null)? SOLObj.giic_SalesOrderLine__r.gii__OrderQuantity__c*SOLObj.giic_SalesOrderLine__r.gii__ConversionFactor__c : SOLObj.giic_SalesOrderLine__r.gii__OrderQuantity__c;
                //giic_UOM__c
                //giic_Comments__c
                //giic_TrackingNumber__c
                wsaLineObject.giic_ProductLot__c =  SOLObj.giic_SalesOrderLine__r.giic_ProductLot__c;
                wsaLineObject.giic_Carrier__c = SOLObj.giic_SalesOrderLine__r.gii__Carrier__r.giic_UniqueCarrier__c;
                
                whsAdviceLinesList.add(wsaLineObject);
            }
            
     
            
            try{
                insert whsAdviceLinesList;
            }catch(Exception e){
                System.debug(e.getMessage());
            }
        }
        
    }    
}