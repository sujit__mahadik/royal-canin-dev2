global class giic_SOSAddressValidationBatchScheduler implements Schedulable
{
    global void execute(SchedulableContext sc)
    {
       /* Dont run address validation batch as address validation is commented multiple execute run so directly run conversion batch
       giic_SOSAddressValidationBatch b = new giic_SOSAddressValidationBatch();       
        database.executebatch(b);*/
        
        //call Sales order Staging to sales order conversion batch
        string jobName = 'giic_SOStagingToSOConversionBatch';
        string Status = 'Processing';        
        String query = 'SELECT Id,Status FROM AsyncApexJob WHERE ApexClass.Name Like \'%' + jobName + '%\'';
        query+= 'and Status =  \''+ Status + '\'' ;        
        List<AsyncApexJob > jobList = Database.query(query);
        if(jobList.size()==0)
        {
            String sObjectApiName = 'gii__SalesOrderStaging__c';
            Map<String, giic_IntegrationCustomMetaDataWrapper> mapIntegrationSettings = giic_IntegrationCustomMetaDataUtil.getIntegrationSettings(sObjectApiName);
            giic_SOStagingToSOConversionBatch SOStagingToSO = new giic_SOStagingToSOConversionBatch(mapIntegrationSettings, sObjectApiName);
            Database.executeBatch(SOStagingToSO);
        }
    }
   
}