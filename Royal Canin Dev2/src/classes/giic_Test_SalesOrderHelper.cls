/************************************************************************************
Version : 1.0
Created Date : 27-Aug-2018
Function : Test class for giic_SalesOrderPathStatusController
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/
@isTest(SeeAllData = false)
private class giic_Test_SalesOrderHelper {
    // below is setup data
    @testSetup
    static void setup(){
        //create admin user
        giic_Test_DataCreationUtility.CreateAdminUser(); 
        // Create consumer account
        List<Account> testConsumerAccountList = giic_Test_DataCreationUtility.insertConsumerAccount();
        // Create Carrier
        List<gii__Carrier__c> testCarrierList = giic_Test_DataCreationUtility.createCarrier();
        // create warehouse
        List<gii__Warehouse__c> testWareList = giic_Test_DataCreationUtility.insertWarehouse();
        SetupTestData.createCustomSettings();
        // create system policy
        giic_Test_DataCreationUtility.testSystemPolicyCreation2();
        // create  Account refrence
        List<gii__AccountAdd__c> testAccRefList = giic_Test_DataCreationUtility.getAccountReference(testConsumerAccountList);
        Database.insert(testAccRefList);
        // create products and product reference
        List<gii__Product2Add__c> testProductList = giic_Test_DataCreationUtility.insertProduct();
        // create Sales Order
        List<gii__SalesOrder__c> testSOList = giic_Test_DataCreationUtility.insertSalesOrderUnreleased(testConsumerAccountList[0].Id, testAccRefList[0].Id);
        // create Sales Order line
        List<gii__SalesOrderLine__c> testSOLList = giic_Test_DataCreationUtility.insertSOLine(testSOList[0].Id, testProductList[0].Id);
    }
    
    // covers multiple scenarios of submitSalesOrder() method
    public static testMethod void test_submitSalesOrder_Scenario1(){        
           
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        
        System.Runas(u)   
        {
            gii__SalesOrder__c objSOExc = [SELECT Id,giic_CCAuthorized__c,giic_TaxCal__c,giic_PromoCal__c,giic_AddressValidated__c,gii__Carrier__c FROM gii__SalesOrder__c LIMIT 1];
            objSOExc.giic_CCAuthorized__c = true;
            objSOExc.giic_TaxCal__c = true;
            objSOExc.giic_PromoCal__c = true;
            objSOExc.giic_AddressValidated__c = true;
            objSOExc.giic_ShipFeeCal__c = true;
            objSOExc.gii__Carrier__c = [Select Id from gii__Carrier__c where Name = 'Route'].Id;
            //objSOExc.gii__Released__c = false;
            update objSOExc ;
            
            Test.startTest();
                giic_SalesOrderHelper.submitSalesOrder(objSOExc.Id);       
            Test.stopTest();
        }
        
    }
    
    // covers multiple scenarios of submitSalesOrder() method
    public static testMethod void test_submitSalesOrder_Scenario2()
    {       
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        
        System.Runas(u)   
        {
            List<Product2> lstProducts = new List<Product2>(); 
            lstProducts = [select id,Name,IsActive,
                                           ProductCode,
                                           Family,
                                           Product_Category__c,
                                           SKU__c,
                                           Food_Type__c,
                                           Weight_Unit__c,
                                           Weight__c from Product2 limit 1];
            
            List<gii__Product2Add__c> lstProd = new List<gii__Product2Add__c>();
            lstProd = [SELECT Id, gii__LotControlled__c, gii__ProductReference__c, gii__ProductReference__r.Name, gii__NonStock__c, gii__ProductReference__r.ProductCode, gii__ProductReference__r.Family,
                       gii__ProductReference__r.Product_Category__c, gii__ProductReference__r.SKU__c, gii__ProductReference__r.Food_Type__c,
                       gii__ProductReference__r.Weight_Unit__c, gii__ProductReference__r.Weight__c,giic_ExceptionState__c from gii__Product2Add__c 
                       WHERE gii__ProductReference__c = :lstProducts[0].id limit 1];
            
            lstProd[0].giic_ExceptionState__c = 'NC';
            update lstProd;
                
            gii__SalesOrder__c objSO = [SELECT Id, gii__Released__c, gii__Account__r.Blocked__c, giic_SOStatus__c,gii__ShipToStateProvince__c  
                                        ,(SELECT Id, gii__OrderQuantity__c, gii__CancelledQuantity__c, gii__ExtendedWeight__c, 
                                          gii__UnitWeight__c,gii__Product__r.giic_ExceptionState__c,gii__LineStatus__c,gii__Status__c 
                                          FROM gii__SalesOrder__r)
                                         FROM gii__SalesOrder__c LIMIT 1];
            objSO.gii__ShipToStateProvince__c = 'NC';
            objSO.giic_CCAuthorized__c = true;
            objSO.giic_TaxCal__c = true;
            objSO.giic_PromoCal__c = true;
            objSO.giic_AddressValidated__c = true;
            objSO.giic_SOStatus__c = 'Draft';
            //objSO.gii__Released__c = false;
            objSO.giic_ShipFeeCal__c = true;

            update objSO;    
            
            objSO.gii__SalesOrder__r[0].gii__Status__c = giic_Constants.SOSTATUSCANCELLED;
            objSO.gii__SalesOrder__r[0].gii__UnitWeight__c = Integer.valueof(Label.giic_RouteOrderMinWeight) + 1 ;
            update objSO.gii__SalesOrder__r;
            
            Test.startTest();
                giic_SalesOrderHelper.submitSalesOrder(objSO.Id);
            Test.stopTest();
        }
    }
    
    // covers multiple scenarios of submitSalesOrder() method
    public static testMethod void test_submitSalesOrder_Scenario3(){        
        
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        
        System.Runas(u)   
        {
            List<Product2> lstProducts = new List<Product2>(); 
            lstProducts = [select id,Name,IsActive,
                                           ProductCode,
                                           Family,
                                           Product_Category__c,
                                           SKU__c,
                                           Food_Type__c,
                                           Weight_Unit__c,
                                           Weight__c from Product2 limit 1];
            
            List<gii__Product2Add__c> lstProd = new List<gii__Product2Add__c>();
            lstProd = [SELECT Id, gii__LotControlled__c, gii__ProductReference__c, gii__ProductReference__r.Name, gii__NonStock__c, gii__ProductReference__r.ProductCode, gii__ProductReference__r.Family,
                       gii__ProductReference__r.Product_Category__c, gii__ProductReference__r.SKU__c, gii__ProductReference__r.Food_Type__c,
                       gii__ProductReference__r.Weight_Unit__c, gii__ProductReference__r.Weight__c,giic_ExceptionState__c from gii__Product2Add__c 
                       WHERE gii__ProductReference__c = :lstProducts[0].id limit 1];
            
            lstProd[0].giic_ExceptionState__c = 'NC';
            update lstProd;
            
            
            gii__SalesOrder__c objSO = [SELECT Id, gii__Released__c,giic_CCAuthorized__c,gii__PaymentMethod__c,gii__ShipToStateProvince__c, gii__TotalWeight__c, gii__Account__r.Blocked__c FROM gii__SalesOrder__c LIMIT 1];
            objSO.giic_CCAuthorized__c = false;
            objSO.gii__PaymentMethod__c = Label.giic_PaymentMethodCreditCard ;
            objSO.giic_AddressValidated__c = false;
            objSO.gii__ShipToStateProvince__c = 'NC';
            objSO.giic_SOStatus__c = 'Draft';
            //objSO.gii__Released__c = false;
            update objSO;       
            
            gii__SalesOrderLine__c objSOL = [SELECT Id, gii__OrderQuantity__c, gii__CancelledQuantity__c, gii__ExtendedWeight__c, gii__UnitWeight__c   FROM gii__SalesOrderLine__c where gii__SalesOrder__c =: objSO.Id LIMIT 1];
            objSOL.gii__UnitWeight__c = Integer.valueof(Label.giic_RouteOrderMinWeight) + 1 ;         
            update objSOL;
            
            Test.startTest();
                giic_SalesOrderHelper.submitSalesOrder(objSO.Id);       
            Test.stopTest();
        }
        
    }
    
    // covers multiple scenarios of submitSalesOrder() method
    public static testMethod void test_submitSalesOrder_Scenario4(){        
           
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        
        System.Runas(u)   
        {
            gii__SalesOrder__c objSOExc = [SELECT Id,giic_CCAuthorized__c,giic_TaxCal__c,giic_PromoCal__c,giic_AddressValidated__c,gii__Carrier__c FROM gii__SalesOrder__c LIMIT 1];
            objSOExc.gii__Released__c = false;
            objSOExc.giic_CCAuthorized__c = true;
            objSOExc.giic_TaxCal__c = true;
            objSOExc.giic_PromoCal__c = true;
            objSOExc.giic_AddressValidated__c = true;
            objSOExc.giic_ShipFeeCal__c = true;
            objSOExc.gii__Carrier__c = [Select Id from gii__Carrier__c where Name = 'Route'].Id;
            //
            update objSOExc ;
            
            Test.startTest();
                giic_SalesOrderHelper.submitSalesOrder(objSOExc.Id);      
            Test.stopTest();   
        }
        
    }
    
    
    
    // covers multiple scenarios of submitSalesOrder() method
    public static testMethod void test_submitSalesOrder_Scenario5(){
        
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        
        System.Runas(u)   
        {
            gii__SalesOrder__c objSO = [SELECT Id, gii__Released__c,  gii__Account__c, gii__Account__r.Blocked__c FROM gii__SalesOrder__c LIMIT 1];
            //objSO.gii__Released__c = false;
            //update objSO;
            gii__SalesOrderLine__c objSOL = [SELECT Id, gii__OrderQuantity__c, gii__CancelledQuantity__c, gii__ExtendedWeight__c, gii__UnitWeight__c   FROM gii__SalesOrderLine__c where gii__SalesOrder__c =: objSO.Id LIMIT 1];
            Account OAcc = new Account (Id =objSO.gii__Account__c, Blocked__c = true  );
            update OAcc;        
            // When SO Account is blocked  
            giic_SalesOrderHelper.submitSalesOrder(objSO.Id);
            
            OAcc.Blocked__c = false;
            update OAcc;
            // when ProcessingStatus is 'Processed with Error'
            objSO.giic_ProcessingStatus__c = 'Processed with Error';
            update objSO;       
            giic_SalesOrderHelper.submitSalesOrder(objSO.Id);
            
            
            // when ProcessingStatus is 'In Progress'
            objSO.giic_ProcessingStatus__c = 'In Progress';
            update objSO;       
            giic_SalesOrderHelper.submitSalesOrder(objSO.Id);
                
            // when SO is null 
            giic_SalesOrderHelper.submitSalesOrder(null); 
        }       
         
    }   
    
    // covers multiple scenarios of submitSalesOrder() method
    public static testMethod void test_submitSalesOrder_Scenario6(){        
           
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        
        System.Runas(u)   
        {
            gii__SalesOrder__c objSOExc = [SELECT Id,giic_CCAuthorized__c,giic_TaxCal__c,giic_PromoCal__c,giic_AddressValidated__c,gii__Carrier__c FROM gii__SalesOrder__c LIMIT 1];
            objSOExc.giic_CCAuthorized__c = true;
            objSOExc.giic_TaxCal__c = true;
            objSOExc.giic_PromoCal__c = true;
            objSOExc.giic_AddressValidated__c = true;
            objSOExc.giic_ShipFeeCal__c = true;
            objSOExc.gii__Carrier__c = [Select Id from gii__Carrier__c where Name = 'Route'].Id;
            //objSOExc.gii__Released__c = false;
            update objSOExc ;
            
            Test.startTest();
                giic_SalesOrderHelper.submitSalesOrder(objSOExc.Id);      
            Test.stopTest();   
        }
        
    }
    
    // covers multiple scenarios of submitSalesOrder() method
    public static testMethod void test_submitSalesOrder_Scenario7(){        
           
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        
        System.Runas(u)   
        {
            gii__SalesOrder__c objSOExc = [SELECT Id,giic_CCAuthorized__c,giic_TaxCal__c,giic_PromoCal__c,giic_AddressValidated__c,gii__Carrier__c FROM gii__SalesOrder__c LIMIT 1];
            objSOExc.giic_CCAuthorized__c = true;
            objSOExc.giic_TaxCal__c = true;
            objSOExc.giic_PromoCal__c = true;
            objSOExc.giic_AddressValidated__c = true;
            objSOExc.giic_ShipFeeCal__c = true;
            objSOExc.gii__Carrier__c = [Select Id from gii__Carrier__c where Name = 'Route'].Id;
            //objSOExc.gii__Released__c = false;
            objSOExc.giic_SOStatus__c = System.Label.giic_SalesOrderCancelledStatus;
            update objSOExc ;
            
            Test.startTest();
                giic_SalesOrderHelper.submitSalesOrder(objSOExc.Id);      
            Test.stopTest();   
        }
        
    }
     
    
    // test method for bulkReleaseSO method
    public static testMethod void test_bulkReleaseSO_Scenario1(){
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        
        System.Runas(u)   
        {
            gii__SalesOrder__c objSO = [SELECT Id, gii__Released__c, gii__Account__r.Blocked__c, giic_SOStatus__c,gii__ShipToStateProvince__c                                    
                                               FROM gii__SalesOrder__c LIMIT 1];
                
            
            gii__SalesOrderLine__c objSOL = [SELECT Id, gii__OrderQuantity__c, gii__CancelledQuantity__c, gii__ExtendedWeight__c, gii__UnitWeight__c 
                                                    FROM gii__SalesOrderLine__c where gii__SalesOrder__c =: objSO.Id LIMIT 1];
            giic_SalesOrderHelper.bulkReleaseSO(new List<gii__SalesOrder__c>{objSO});
            Test.startTest();
                giic_SalesOrderHelper.updateSalesOrder(new List<gii__SalesOrder__c>{objSO});      
            Test.stopTest(); 
        }
    }
    
    // test method for bulkReleaseSO method
    public static testMethod void test_bulkReleaseSO_Scenario2(){

        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        
        System.Runas(u)   
        {
            List<Product2> lstProducts = new List<Product2>(); 
            lstProducts = [select id,Name,IsActive,
                                           ProductCode,
                                           Family,
                                           Product_Category__c,
                                           SKU__c,
                                           Food_Type__c,
                                           Weight_Unit__c,
                                           Weight__c from Product2 limit 1];

            List<gii__Product2Add__c> lstProd = new List<gii__Product2Add__c>();
            lstProd = [SELECT Id, gii__LotControlled__c, gii__ProductReference__c, gii__ProductReference__r.Name, gii__NonStock__c, gii__ProductReference__r.ProductCode, gii__ProductReference__r.Family,
                       gii__ProductReference__r.Product_Category__c, gii__ProductReference__r.SKU__c, gii__ProductReference__r.Food_Type__c,
                       gii__ProductReference__r.Weight_Unit__c, gii__ProductReference__r.Weight__c,giic_ExceptionState__c from gii__Product2Add__c 
                       WHERE gii__ProductReference__c = :lstProducts[0].id limit 1];

            lstProd[0].giic_ExceptionState__c = 'NC'; 
            update lstProd;
                
            gii__SalesOrder__c objSO = [SELECT Id, gii__Released__c, gii__Account__r.Blocked__c, giic_SOStatus__c,gii__ShipToStateProvince__c  
                                        ,(SELECT Id, gii__OrderQuantity__c, gii__CancelledQuantity__c, gii__ExtendedWeight__c, 
                                          gii__UnitWeight__c,gii__Product__r.giic_ExceptionState__c,gii__LineStatus__c,gii__Status__c 
                                          FROM gii__SalesOrder__r)
                                         FROM gii__SalesOrder__c LIMIT 1];
            
          
            //objSO.gii__Released__c = false; 
            objSO.gii__ShipToStateProvince__c = 'NC';
            update objSO;    

            objSO.gii__SalesOrder__r[0].gii__Status__c = giic_Constants.SOSTATUSCANCELLED;
            update objSO.gii__SalesOrder__r;
            Test.startTest();
                giic_SalesOrderHelper.bulkReleaseSO(new List<gii__SalesOrder__c>{objSO});
                giic_SalesOrderHelper.updateSalesOrder(new List<gii__SalesOrder__c>{objSO});
             Test.stopTest();
        }
        
    }
    
    // test method for bulkReleaseSO method
    public static testMethod void test_bulkReleaseSO_Scenario3(){
        Test.startTest();
            giic_SalesOrderHelper.bulkReleaseSO(new List<gii__SalesOrder__c>());
        Test.stopTest();
    
    }
    // test method for calculateTax method 
    public static testMethod void test_calculateTax_Scenario1(){
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        
        System.Runas(u)   
        {
            Test.startTest();
                Test.setMock(HttpCalloutMock.class, new giic_Test_AvalaraCalloutMockImpl());
                gii__SalesOrder__c objSO = [SELECT Id, gii__Released__c, gii__Account__r.Blocked__c, giic_SOStatus__c  FROM gii__SalesOrder__c LIMIT 1];
                giic_SalesOrderHelper.CalculateTaxforSO(objSO.Id);
             Test.stopTest();
        }
        
    }
    
    // test method for calculateTax method 
    public static testMethod void test_calculateTax_Scenario2(){
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        
        System.Runas(u)   
        {
            List<Account> lstAccount = new List<Account>();
            lstAccount = [select id,Check_if_you_are_Tax_Exempt__c,Resale_Sales_Tax_Cert_Expiration_Date__c from Account LIMIT 1];
            lstAccount[0].Check_if_you_are_Tax_Exempt__c = true;
            lstAccount[0].Resale_Sales_Tax_Cert_Expiration_Date__c = System.today().addDays(-1);
            update lstAccount;
            Test.setMock(HttpCalloutMock.class, new giic_Test_AvalaraCalloutMockImpl());
            gii__SalesOrder__c objSO = [SELECT Id, gii__Released__c, gii__Account__r.Blocked__c, giic_SOStatus__c  FROM gii__SalesOrder__c LIMIT 1];
            Test.startTest();
                giic_SalesOrderHelper.CalculateTaxforSO(objSO.Id);        
             Test.stopTest();
        }        
    } 
    
     // test method for calculateTax method 
    public static testMethod void test_calculateTax_Scenario3(){
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        
        System.Runas(u)   
        {
            List<Account> lstAccount = new List<Account>();
            lstAccount = [select id,Check_if_you_are_Tax_Exempt__c,Resale_Sales_Tax_Cert_Expiration_Date__c from Account LIMIT 1];
            lstAccount[0].Check_if_you_are_Tax_Exempt__c = true;
            lstAccount[0].Resale_Sales_Tax_Cert_Expiration_Date__c = System.today().addDays(-1);
            update lstAccount;
            Test.setMock(HttpCalloutMock.class, new giic_Test_ErrAvalaraCalloutMockImpl());
            gii__SalesOrder__c objSO = [SELECT Id, gii__Released__c, gii__Account__r.Blocked__c, giic_SOStatus__c  FROM gii__SalesOrder__c LIMIT 1];
            Test.startTest();
                giic_SalesOrderHelper.CalculateTaxforSO(objSO.Id);        
             Test.stopTest();
        }        
    } 
    
    
    

    // test method for CalculateTaxCommitFalse method 
    public static testMethod void test_CalculateTaxCommitFalse(){
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        
        System.Runas(u)   
        {
            List<Account> lstAccount = new List<Account>();
            lstAccount = [select id,Check_if_you_are_Tax_Exempt__c,Resale_Sales_Tax_Cert_Expiration_Date__c from Account LIMIT 1];
            lstAccount[0].Check_if_you_are_Tax_Exempt__c = true;
            lstAccount[0].Resale_Sales_Tax_Cert_Expiration_Date__c = System.today().addDays(-1);
            update lstAccount;
            Test.setMock(HttpCalloutMock.class, new giic_Test_AvalaraCalloutMockImpl());
            gii__SalesOrder__c objSO = [SELECT Id, gii__Released__c, gii__Account__r.Blocked__c, giic_SOStatus__c  FROM gii__SalesOrder__c LIMIT 1];
            Test.startTest();
                giic_SalesOrderHelper.CalculateTaxCommitFalse(objSO.Id);        
            Test.stopTest();  
        }        
    }
    
    //To test for cancelSalesOrder method for non released order
    public static testMethod void test_cancelSalesOrder_Scenario1(){
         User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        
        System.Runas(u)   
        {
        gii__SalesOrder__c objSO = [SELECT Id, gii__Released__c, gii__Account__r.Blocked__c, giic_SOStatus__c  FROM gii__SalesOrder__c LIMIT 1];    
        //objSO.gii__Released__c = false;
        //update objSO;
        Test.startTest();                    
            giic_SalesOrderHelper.cancelSalesOrder(objSO.id);            
         Test.stopTest();
        
    }
    }
    
    //To test for cancelSalesOrder method for released order
     public static testMethod void test_cancelSalesOrder_Scenario2(){
          User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        
        System.Runas(u)   
        {
        gii__SalesOrder__c objSO = [SELECT Id, gii__Released__c, gii__Account__r.Blocked__c, giic_SOStatus__c  FROM gii__SalesOrder__c LIMIT 1];   
        Test.startTest();                    
            giic_SalesOrderHelper.cancelSalesOrder(objSO.id);            
         Test.stopTest();
        }
    }
    //To test for cancelSalesOrder method for for already cancelled order
     public static testMethod void test_cancelSalesOrder_Scenario3(){
          User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        
        System.Runas(u)   
        {
        gii__SalesOrder__c objSO = [SELECT Id, gii__Released__c, gii__Account__r.Blocked__c, giic_SOStatus__c  FROM gii__SalesOrder__c LIMIT 1];    
        //objSO.gii__Released__c = false;
        objSO.giic_SOStatus__c=system.label.giic_SalesOrderCancelledStatus;
        update objSO;
        Test.startTest();                    
            giic_SalesOrderHelper.cancelSalesOrder(objSO.id);            
         Test.stopTest();
        }
    }
    //To test for cancelSalesOrder method negative scenarios
     public static testMethod void test_cancelSalesOrder_Scenario4(){
          User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        
        System.Runas(u)   
        {
        Test.startTest();                    
            giic_SalesOrderHelper.cancelSalesOrder(null);            
         Test.stopTest();
        
    }
     }
    
    //To test for cancelSoLines method for positive scenarios
    public static testMethod void testpostivecancelSoLines(){
         User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        
        System.Runas(u)   
        {
        gii__SalesOrder__c objSO = [SELECT Id, gii__Released__c, gii__Account__r.Blocked__c, giic_SOStatus__c  FROM gii__SalesOrder__c LIMIT 1];    
        List<gii__SalesOrderLine__c> lstSOL = [SELECT Id, gii__OrderQuantity__c, gii__CancelledQuantity__c, gii__ExtendedWeight__c, gii__UnitWeight__c,
                                               gii__ReservedQuantity__c,gii__CancelReason__c FROM gii__SalesOrderLine__c where gii__SalesOrder__c =: objSO.Id LIMIT 1];
        map<String,list<gii__SalesOrderLine__c>> mapsolines = new map<String,list<gii__SalesOrderLine__c>>();
        mapsolines.put(objSO.id, lstSOL);
        
        Test.startTest();                    
            giic_SalesOrderHelper.cancelSoLines(mapsolines);            
         Test.stopTest();
        
    }   
    }
    
    //To test for cancelSoLines method for negative scenarios
     public static testMethod void testnegativecancelSoLines(){
          User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        
        System.Runas(u)   
        {
        gii__SalesOrder__c objSO = [SELECT Id, gii__Released__c, gii__Account__r.Blocked__c, giic_SOStatus__c  FROM gii__SalesOrder__c LIMIT 1];    
        List<gii__SalesOrderLine__c> lstSOL = [SELECT Id, gii__OrderQuantity__c, gii__CancelledQuantity__c, gii__ExtendedWeight__c, gii__UnitWeight__c,
                                               gii__ReservedQuantity__c,gii__CancelReason__c FROM gii__SalesOrderLine__c where gii__SalesOrder__c =: objSO.Id LIMIT 1];
        map<String,list<gii__SalesOrderLine__c>> mapsolines = new map<String,list<gii__SalesOrderLine__c>>();
        //mapsolines.put(objSO.id, lstSOL);
        
        Test.startTest();      
        try
        {
            giic_SalesOrderHelper.cancelSoLines(mapsolines);           
        }
        catch(Exception ex)
        {}
         Test.stopTest();
        }
    }  
}