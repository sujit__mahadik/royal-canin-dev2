/*
 * Date:       		 - Developer, Company					       - Description
 * 2015-08-10		 - Stevie Yakkel, Acumen Solutions			   - Created
 */
@isTest
private class ChangeAccountOwnershipHelperUtilTest
{
	@isTest
	static void getCurrentFiscalYearTest() {
		Fiscal_Year__c fy = new Fiscal_Year__c();
		fy.isActive__c = true;
		insert fy;
		System.assertEquals(ChangeAccountOwnershipHelperUtil.getCurrentFiscalYear().Id, fy.Id);
	}

	@isTest
	static void getFiscalYearForPromotionTest() {
		Fiscal_Year__c fy = new Fiscal_Year__c();
		fy.isActive__c = false;
		fy.Start_Date__c = Date.today();
		fy.Activation_Date__c = Date.today();
		fy.End_Date__c = Date.today().addDays(10);
		insert fy;
		System.assertEquals(ChangeAccountOwnershipHelperUtil.getFiscalYearForPromotion().Id, fy.Id);
	}

	@isTest
	static void promoteFiscalYearToActiveTest() {
		Fiscal_Year__c fy = new Fiscal_Year__c();
		fy.isActive__c = true;
		fy.Activation_Date__c = Date.today().addDays(3);
		fy.End_Date__c = Date.today().addDays(10);
		insert fy;
		Fiscal_Year__c fy2 = new Fiscal_Year__c();
		insert fy2;
		ChangeAccountOwnershipHelperUtil.promoteFiscalYearToActive(fy2);
		Fiscal_Year__c fyCheck = [SELECT isActive__c FROM Fiscal_Year__c WHERE Id=:fy.Id];
		System.assert(!fyCheck.isActive__c);
	}
}