/************************************************************************************
Version : 1.0
Name : giic_ReValidateAddress
Created Date : 05 ASep 2018
Function : ReValidate Address
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/

public class giic_ReValidateAddressController {
   
    public gii__SalesOrderStaging__c objSOS{get; set;}
    public Boolean isError{get; set;}

    public giic_ReValidateAddressController (ApexPages.StandardController controller) {
        objSOS = (gii__SalesOrderStaging__c) controller.getRecord();
        isError = false;
    }
    
  
    public PageReference onLoad(){
        PageReference pObj ;
        try{
            Map<Id , giic_ValShippingAddrFedExController.AddressValidation> mapResult = new Map<Id , giic_ValShippingAddrFedExController.AddressValidation>();
            System.debug(':::objSOS=' + objSOS);
            List<gii__SalesOrderStaging__c> lstSOS = [select Id, giic_Account__c, giic_DropShip__c, giic_ShipToStreet__c, giic_ShipToCity__c, giic_ShipToZipPostalCode__c, giic_ShipToCountry__c from gii__SalesOrderStaging__c where id =:objSOS.id];
            if(!lstSOS.isEmpty()){
                mapResult = giic_IntegrationCommonUtil.validateAddress(lstSOS);
            }
            system.debug('  mapResult--->>>> ' + mapResult);
            if(mapResult.containsKey(objSOS.Id) && mapResult.get(objSOS.Id).isValidAddress == true){                
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM, System.Label.giic_AddressRevalidated));
                return null;
            }else{
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, System.Label.giic_AddressValidationFailed));
                isError = true;
                if(mapResult.containsKey(objSOS.Id)){
                    objSOS.giic_ShipToStreet__c = mapResult.get(objSOS.Id).suggestedAddress.StreetLines;
                    objSOS.giic_ShipToCity__c = mapResult.get(objSOS.Id).suggestedAddress.City;
                    objSOS.giic_ShipToZipPostalCode__c =  mapResult.get(objSOS.Id).suggestedAddress.PostalCode;
                    objSOS.giic_ShipToCountry__c = mapResult.get(objSOS.Id).suggestedAddress.CountryCode;  
                    objSOS.giic_AddressType__c = mapResult.get(objSOS.Id).Classification;  
                }else{
                    objSOS.giic_ShipToStreet__c = '';
                    objSOS.giic_ShipToCity__c = '';
                    objSOS.giic_ShipToZipPostalCode__c =  '';
                    objSOS.giic_ShipToCountry__c = '';  
                    objSOS.giic_AddressType__c = '';
                }
                return null;
            }
        }
        catch(Exception ex){ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getMessage())); return null;}
    }
    
    public PageReference goBack(){
        PageReference pObj ;
        try{          
            pObj = new PageReference('/' + objSOS.id); // redirect to sales order Staging record
            pObj.setRedirect(true);            
        }catch(Exception ex){ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getMessage())); return null; }
        return pObj;
    }
    
    public PageReference applySuggestedAddress(){
        PageReference pObj ;
        try{
            update objSOS;          
            pObj = new PageReference('/' + objSOS.id); // redirect to sales order Staging record
            pObj.setRedirect(true);            
        }catch(Exception ex){ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getMessage())); return null; }
        return pObj;
    }
}