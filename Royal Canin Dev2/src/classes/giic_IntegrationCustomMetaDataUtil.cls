/************************************************************************************
Version : 1.0
Name : giic_IntegrationCustomMetaDataUtil
Created Date : 14 Aug 2018
Function : Provide Utility for Conversion of Sales Order Staging to Sales Order
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/
public class giic_IntegrationCustomMetaDataUtil {
    //Start this method for taking source list
    public static Map<String, giic_IntegrationCMDResultWrapper> doConversion(String sObjectApiName, Map<String, giic_IntegrationCustomMetaDataWrapper> mapIntegrationSettings, List<SObject> lstSourceObject, Set<String> setErrorIds){
        Savepoint sp = Database.setSavepoint();
        try{
            //Step-2 -B : get Child Records
            Map<String, List<SObject>> mpChildRecord = readChildRecords(mapIntegrationSettings, lstSourceObject);
            //Step3 : mapping fields and check validation
            Map<String, String> mpSource = new Map<String, String>();
            List<Integration_Error_Log__c> lstErrors = new List<Integration_Error_Log__c>();
            //for Parent
            if(lstSourceObject.isEmpty()){
                return new Map<String, giic_IntegrationCMDResultWrapper>();
            }
            
            Map<String, giic_IntegrationCMDResultWrapper> mpNewSObjectId = createSObjectRecords(lstSourceObject, sObjectApiName, mapIntegrationSettings, new Map<String, giic_IntegrationCMDResultWrapper>(), mpSource, lstErrors, setErrorIds);
            //for Child
            for(String strSObjectApiName :mpChildRecord.keySet()){
                createSObjectRecords(mpChildRecord.get(strSObjectApiName), strSObjectApiName, mapIntegrationSettings, mpNewSObjectId, mpSource, lstErrors, setErrorIds);
            }
            //create Error Log
            if(!lstErrors.isEmpty()){
                insert lstErrors;
            }
            return mpNewSObjectId;
        }catch(exception e){
            Database.rollback(sp);
            System.debug('Exception==doConversion='+e.getMessage()+'~'+e.getLineNumber());
            throw e;
        }
    }
    
    //Step-3-A Method : mapping fields
    public static Map<String, giic_IntegrationCMDResultWrapper> createSObjectRecords (List<SObject> lstSourceObject, String sObjectApiName, Map<String, giic_IntegrationCustomMetaDataWrapper> mapIntegrationSettings, Map<String, giic_IntegrationCMDResultWrapper> mpNewSObjectId, Map<String, String> mpSource, List<Integration_Error_Log__c> lstErrors, Set<String> setErrorIds){
        giic_IntegrationCustomMetaDataWrapper objCMD = mapIntegrationSettings.get(sObjectApiName);        
        Schema.sObjectType targetType = Schema.getGlobalDescribe().get(objCMD.targetSobjectApi);
        List<SObject> lstNewTargetSobject = new List<SObject>();
        List<SObject> lstSourceFiltered = new List<SObject>();
        Integer count = 0;
        for(SObject sourceObj :lstSourceObject)
        {
            count++;            
            SObject targetObj = targetType.newSObject();
            giic_IntegrationCMDResultWrapper objRslt = new giic_IntegrationCMDResultWrapper();            
            if(sourceObj.get(objCMD.convertedFieldApi) != null) 
            {
                targetObj.put('Id', String.valueOf(sourceObj.get(objCMD.convertedFieldApi))); 
                objRslt.targetObj =targetObj;  
                mpNewSObjectId.put(sourceObj.Id, objRslt); 
                continue; 
            }
            
            Boolean readyToCreate = true;           
            for(String fieldapi :objCMD.mapIDetail.keySet())
            {
                giic_IntegrationMapping__mdt objIDtl =  objCMD.mapIDetail.get(fieldapi);          
                if(objIDtl.giic_IsParentField__c == true && ( !(mpNewSObjectId.containsKey(''+ sourceObj.get(objIDtl.giic_SourceField__r.QualifiedApiName))) || mpNewSObjectId.get(''+ sourceObj.get(objIDtl.giic_SourceField__r.QualifiedApiName)).targetObj.get('Id') == null) ){readyToCreate = false; continue;}
                if(objIDtl.giic_IsConvertedField__c) continue;
                if(objIDtl.giic_UseDefault__c) continue;
                if(targetObj.get(objIDtl.giic_TargetField__r.QualifiedApiName) != null || sourceObj.get(objIDtl.giic_SourceField__r.QualifiedApiName) == null) continue;
                if(objIDtl.giic_IsExternal__c)
                {   
                    Schema.sObjectType externalObjType = Schema.getGlobalDescribe().get(objIDtl.giic_ExternalSObject__r.QualifiedApiName); 
                    SObject externalObj = externalObjType.newSObject();   
                    externalObj.put(objIDtl.giic_ExternalField__r.QualifiedApiName, sourceObj.get(objIDtl.giic_SourceField__r.QualifiedApiName));
                    targetObj.putSobject(objIDtl.giic_TargetField__r.QualifiedApiName.replace('__c','__r'), externalObj);
                    
                }
                else 
                {
                    
                    if(objIDtl.giic_IsParentField__c){
                        targetObj.put(objIDtl.giic_TargetField__r.QualifiedApiName, mpNewSObjectId.get(''+ sourceObj.get(objIDtl.giic_SourceField__r.QualifiedApiName)).targetObj.Id);
                    }else if(sourceObj.get(objIDtl.giic_SourceField__r.QualifiedApiName) != null){
                        targetObj.put(objIDtl.giic_TargetField__r.QualifiedApiName, sourceObj.get(objIDtl.giic_SourceField__r.QualifiedApiName));
                    }
                }
                
                if(objIDtl.giic_IsSourceField__c) mpSource.put(sourceObj.Id, sourceObj.get(objIDtl.giic_SourceField__r.QualifiedApiName) != null ? String.valueOf(sourceObj.get(objIDtl.giic_SourceField__r.QualifiedApiName)):'');
                if(objIDtl.giic_IsParentField__c) {
                    mpSource.put(sourceObj.Id, mpSource.get(''+ sourceObj.get(objIDtl.giic_SourceField__r.QualifiedApiName)));
                    String parentId = ''+ sourceObj.get(objIDtl.giic_SourceField__r.QualifiedApiName);
                    mpNewSObjectId.get(parentId).totalChilds = mpNewSObjectId.get(parentId).totalChilds + 1;
                    objRslt.parentFieldApi = objIDtl.giic_SourceField__r.QualifiedApiName;
                }
            }  
            
            Boolean flagValidate = false; 
            if(readyToCreate == true && objCMD.mapValidation.containsKey(mpSource.get(sourceObj.Id))){
                Map<String, Map<String, giic_IntegrationValidation__mdt>> mpSobjectValidation = objCMD.mapValidation.get(mpSource.get(sourceObj.Id));
                if(mpSobjectValidation.containsKey(sObjectApiName)){
                    Map<String, giic_IntegrationValidation__mdt> mpFieldValidation = mpSobjectValidation.get(sObjectApiName);
                    for(String fieldapi :mpFieldValidation.keySet()){
                        giic_IntegrationValidation__mdt objValidation = mpFieldValidation.get(fieldapi);
                        flagValidate = validateValue(sourceObj.get(fieldapi), objValidation);
                        if(flagValidate)collectErrors(lstErrors, setErrorIds, objCMD.userStory, objCMD.fieldApiForErrorLog, sourceObj, objValidation.giic_ErrorCode__c, objValidation.giic_ErrorMessage__c);
                    }
                }
            }
            
            if(readyToCreate == true && flagValidate == false){
                if(objCMD.mapDefaultValue.containsKey(mpSource.get(sourceObj.Id))){
                    Map<String, Map<String, giic_IntegrationDefaultData__mdt>> mpSobjectDefaultValue = objCMD.mapDefaultValue.get(mpSource.get(sourceObj.Id));
                    if(mpSobjectDefaultValue.containsKey(sObjectApiName)){
                        Map<String, giic_IntegrationDefaultData__mdt> mpFieldDefaultValue = mpSobjectDefaultValue.get(sObjectApiName);
                        for(String fieldapi :mpFieldDefaultValue.keySet()){
                            giic_IntegrationDefaultData__mdt objDefaultValue = mpFieldDefaultValue.get(fieldapi);
                            //Step-3-c Method : check data type and convert value
                            convertDataTypeValue(targetObj, fieldapi, objDefaultValue.giic_DefaultValue__c, objDefaultValue.giic_DataType__c);
                        }
                    }
                }
            }
            if(flagValidate == false && readyToCreate ==true){                
                objRslt.targetObj = targetObj;
                mpNewSObjectId.put(sourceObj.Id, objRslt);
                //System.debug('::targetObj=' +targetObj ); imp do not remove
                lstNewTargetSobject.add(targetObj);
                lstSourceFiltered.add(sourceObj);
            }
        }
        
        //System.debug(':::lstNewTargetSobject='+ lstNewTargetSobject); // imp
        if(!lstNewTargetSobject.isEmpty()){
            
            Database.SaveResult[] srList = Database.insert(lstNewTargetSobject, false);
            
            // Iterate through each returned result
            Integer index = 0;
            for (Database.SaveResult sr : srList) {
                if(sr.isSuccess()){
                    Sobject objS =  lstSourceFiltered[index];
                    objS.put(objCMD.convertedFieldApi, sr.getId());
                    String parentFieldApi = ''+ mpNewSObjectId.get(objS.Id).parentFieldApi;
                    if(parentFieldApi != ''){
                        String parentId = String.valueOf(objS.get(parentFieldApi));
                        mpNewSObjectId.get(parentId).totalChilds = mpNewSObjectId.get(parentId).totalChilds - 1;
                    }     
                } else if (!sr.isSuccess()) {
                    for(Database.Error err : sr.getErrors()) {
                        collectErrors(lstErrors, setErrorIds, objCMD.userStory, objCMD.fieldApiForErrorLog, lstSourceFiltered[index], String.valueOf(err.getStatusCode()), err.getMessage());
                    }
                }
                index++;
            }
            
            update lstSourceFiltered;            
        }
        
        return mpNewSObjectId;
    }
    
    //Step-3-c Method : check data type and convert value
    public static void convertDataTypeValue(SObject targetObj, String fieldapi, String value, String dataType){
        
        switch on dataType {
            when 'Text' {   
                targetObj.put(fieldapi, value);
            }  
            when 'Boolean' {   
                targetObj.put(fieldapi, Boolean.valueOf(value));
            } 
            when 'Date' {   
                targetObj.put(fieldapi, Date.valueOf(value));
            } 
            when 'DateTime' {   
                targetObj.put(fieldapi, Date.valueOf(value));
            } 
        }
    }
    
    
    
    //Strpe-3-C Method: Collect Errors
    public static void collectErrors(List<Integration_Error_Log__c> lstErrors, Set<String> stErrorIds, String userStory, Map<String, String> relatedToFieldAPi, Sobject sourceRecord, String errorCode, String errorMessage){
        stErrorIds.add(String.valueOf(sourceRecord.get('Id'))); 
        Integration_Error_Log__c objErr= new Integration_Error_Log__c(
            Name = userStory,
            giic_IsActive__c = true,
            Error_Code__c   = errorCode,
            Error_Message__c = errorMessage,
            giic_Error_Type__c = 'Error'
        );
        for(String errFieldApi :relatedToFieldAPi.keySet())
        {
            String sourceFieldAPi = relatedToFieldAPi.get(errFieldApi);  
            if(sourceRecord.get(sourceFieldAPi) != null) 
                objErr.put(errFieldApi, (sourceRecord.get(sourceFieldAPi)));
        }        
        lstErrors.add(objErr);
    }
    
    //Step-3-B Method : check validation Value
    public static Boolean validateValue(Object value, giic_IntegrationValidation__mdt validationMeta){
        if(validationMeta.giic_Operation__c != ''){
            switch on validationMeta.giic_Operation__c {
                when 'IsNullOrEmpty' {   
                    if(value == null || String.valueOf(value) == '') return true;
                    return false;
                }
                when 'IsBoolean' {   
                    if(value == null || String.valueOf(value) == '' ) return true;
                    if(Boolean.valueOf(value) == Boolean.valueOf(validationMeta.giic_Value__c)) return true;
                    return false;
                }                    
                
            }
        }
        return false;
    } 
    
    //Step-2 Method: fetch childs records
    //Note -> this is dynamic configuration. As per requirement, there is need to fetch child records by perent information
    public static Map<String, List<SObject>> readChildRecords(Map<String, giic_IntegrationCustomMetaDataWrapper> mapIntegrationSettings, List<SObject> lstSourceObject){
        Map<String, List<SObject>> mpChildRecords = new Map<String, List<SObject>>();
        for(String objAPI :mapIntegrationSettings.keySet()){
            giic_IntegrationCustomMetaDataWrapper cmdObj = mapIntegrationSettings.get(objAPI);    
            if(cmdObj.strWhereClause != ''){
                String soql = 'Select ' + cmdObj.strSoql.subString(0, cmdObj.strSoql.length()-1) + ' from ' + cmdObj.sourceSobjectApi + ' where ' + cmdObj.strWhereClause + ' in :lstSourceObject ' +  (cmdObj.convertedFieldApi != ''? (' and ' + cmdObj.convertedFieldApi + ' = null') : '');
                mpChildRecords.put(cmdObj.sourceSobjectApi, Database.query(soql));
            }
        }         
        return mpChildRecords;   
    }
    
    //Step-1 Method: prepare setting accourding to custom metadata
    public static Map<String, giic_IntegrationCustomMetaDataWrapper> getIntegrationSettings(String sObjectApiName){
        Map<String, giic_IntegrationCustomMetaDataWrapper> mapIntegrationSettings = new Map<String, giic_IntegrationCustomMetaDataWrapper>();
        
        Map<String,giic_IntegrationSetting__mdt> mapIS = new Map<String,giic_IntegrationSetting__mdt>();
        //Map<String,giic_IntegrationMapping__mdt> mapIDetail = new Map<String,giic_IntegrationMapping__mdt>();
        Map<String,giic_IntegrationValidation__mdt> mapValidation = new Map<String,giic_IntegrationValidation__mdt>();
        
        List<giic_IntegrationMapping__mdt> lstIDetail = new List<giic_IntegrationMapping__mdt>();
        
        List<giic_IntegrationSetting__mdt> lstIS = [Select giic_NoOfRecords__c, giic_UserStory__c,
                                                    (Select giic_IsSourceField__c, giic_IsExternal__c, giic_ExternalSObject__r.QualifiedApiName, 
                                                     giic_ExternalField__r.QualifiedApiName, giic_IntegrationSetting__c, giic_SourceObject__c, 
                                                     giic_SourceObject__r.QualifiedApiName, giic_SourceField__r.QualifiedApiName, giic_TargetObject__c, 
                                                     giic_TargetObject__r.QualifiedApiName, giic_TargetField__r.QualifiedApiName, giic_IsParentField__c,
                                                     giic_IsConvertedField__c, giic_IsErrorMapping__c, 	giic_UseDefault__c
                                                     From IntegrationMappings__r where giic_DeActivated__c = false)
                                                    From giic_IntegrationSetting__mdt 
                                                    where giic_Object__r.QualifiedApiName =: sObjectApiName and giic_IsActive__c = true];
        
        for(giic_IntegrationSetting__mdt objIS : lstIS){                                          
            mapIS.put(sObjectApiName, objIS);
            if(objIS.IntegrationMappings__r.size() > 0){
                Set<String> setFields = new Set<String>();
                for(giic_IntegrationMapping__mdt objIDetail : objIS.IntegrationMappings__r){ 
                    lstIDetail.add(objIDetail);
                    
                    if(!mapIntegrationSettings.containsKey(objIDetail.giic_SourceObject__r.QualifiedApiName)) mapIntegrationSettings.put(objIDetail.giic_SourceObject__r.QualifiedApiName, new giic_IntegrationCustomMetaDataWrapper());
                    giic_IntegrationCustomMetaDataWrapper objCMD = mapIntegrationSettings.get(objIDetail.giic_SourceObject__r.QualifiedApiName);
                    objCMD.userStory = objIS.giic_UserStory__c;
                    //key => SObject API + '~' + Source's Field API
                    if(objIDetail.giic_IsErrorMapping__c ==false){ objCMD.mapIDetail.put(objIDetail.giic_SourceObject__r.QualifiedApiName +'~'+ objIDetail.giic_SourceField__r.QualifiedApiName, objIDetail); }
                    if(!setFields.contains(objIDetail.giic_SourceObject__r.QualifiedApiName + '-' + objIDetail.giic_SourceField__r.QualifiedApiName)){ objCMD.strSoql += objIDetail.giic_SourceField__r.QualifiedApiName + ','; setFields.add(objIDetail.giic_SourceObject__r.QualifiedApiName + '-' + objIDetail.giic_SourceField__r.QualifiedApiName);}
                    if(objIDetail.giic_IsParentField__c) objCMD.strWhereClause = objIDetail.giic_SourceField__r.QualifiedApiName;
                    objCMD.sourceSobjectApi = objIDetail.giic_SourceObject__r.QualifiedApiName;
                    
                    if(objIDetail.giic_IsConvertedField__c) {objCMD.convertedFieldApi = objIDetail.giic_TargetField__r.QualifiedApiName; objCMD.strSoql += objCMD.convertedFieldApi + ',';}
                    else if(objIDetail.giic_IsErrorMapping__c){objCMD.fieldApiForErrorLog.put(objIDetail.giic_TargetField__r.QualifiedApiName, objIDetail.giic_SourceField__r.QualifiedApiName); }
                    else objCMD.targetSobjectApi = objIDetail.giic_TargetObject__r.QualifiedApiName;
                }
            }
        }
        
        //setup of Validations
        for(giic_IntegrationValidation__mdt objValidation : [Select giic_IntegrationMapping__c, giic_Object__r.QualifiedApiName, giic_Field__r.QualifiedApiName, giic_Operation__c, giic_Value__c, giic_ErrorMessage__c, giic_ErrorCode__c, giic_ValidationFor__c From giic_IntegrationValidation__mdt where giic_DeActived__c = false and giic_IntegrationMapping__c IN : lstIDetail]){
            giic_IntegrationCustomMetaDataWrapper objCMD = mapIntegrationSettings.get(objValidation.giic_Object__r.QualifiedApiName);
            //Key => Source
            if(!objCMD.mapValidation.containsKey(objValidation.giic_ValidationFor__c))objCMD.mapValidation.put(objValidation.giic_ValidationFor__c, new Map<String, Map<String, giic_IntegrationValidation__mdt>>());
            //Key => Sobject API
            Map<String, Map<String, giic_IntegrationValidation__mdt>> mpSobjValidation = objCMD.mapValidation.get(objValidation.giic_ValidationFor__c);
            if(!mpSobjValidation.containsKey(objValidation.giic_Object__r.QualifiedApiName))mpSobjValidation.put(objValidation.giic_Object__r.QualifiedApiName, new Map<String, giic_IntegrationValidation__mdt>());
            //key => SObject's Field API
            mpSobjValidation.get(objValidation.giic_Object__r.QualifiedApiName).put(objValidation.giic_Field__r.QualifiedApiName, objValidation);
        }
        
        //setup of Default Values
        for(giic_IntegrationDefaultData__mdt objDefaultValue : [Select giic_IntegrationMapping__c, giic_DataType__c, giic_SourceObject__r.QualifiedApiName, giic_Object__r.QualifiedApiName, giic_Field__r.QualifiedApiName, giic_DefaultValue__c, giic_Source__c From giic_IntegrationDefaultData__mdt where giic_DeActived__c = false and giic_IntegrationMapping__c IN : lstIDetail]){
            giic_IntegrationCustomMetaDataWrapper objCMD = mapIntegrationSettings.get(objDefaultValue.giic_SourceObject__r.QualifiedApiName);
            //Key => Source
            if(!objCMD.mapDefaultValue.containsKey(objDefaultValue.giic_Source__c))objCMD.mapDefaultValue.put(objDefaultValue.giic_Source__c, new Map<String, Map<String, giic_IntegrationDefaultData__mdt>>());
            //Key => Sobject API
            Map<String, Map<String, giic_IntegrationDefaultData__mdt>> mpSobjDefaultValue = objCMD.mapDefaultValue.get(objDefaultValue.giic_Source__c);
            if(!mpSobjDefaultValue.containsKey(objDefaultValue.giic_SourceObject__r.QualifiedApiName))mpSobjDefaultValue.put(objDefaultValue.giic_SourceObject__r.QualifiedApiName, new Map<String, giic_IntegrationDefaultData__mdt>());
            //key => SObject's Field API
            mpSobjDefaultValue.get(objDefaultValue.giic_SourceObject__r.QualifiedApiName).put(objDefaultValue.giic_Field__r.QualifiedApiName, objDefaultValue);
        }
        
        return mapIntegrationSettings;
    }
    
    public static void deActivateCurrentError(Set<Id> sosIds){
        List<Integration_Error_Log__c> errList = new List<Integration_Error_Log__c>();
        
        if(sosIds.isEmpty())
            errList = [select giic_IsActive__c from Integration_Error_Log__c where
                       giic_IsActive__c = true 
                       and giic_SalesOrderStaging__c != null and giic_SalesOrderStaging__r.giic_Status__c !=  :System.Label.giic_Converted
                      ];
        else errList = [select giic_IsActive__c from Integration_Error_Log__c where
                        giic_IsActive__c = true and giic_SalesOrderStaging__c in :sosIds
                        and giic_SalesOrderStaging__c != null and giic_SalesOrderStaging__r.giic_Status__c !=  :System.Label.giic_Converted
                       ];
        
        for(Integration_Error_Log__c e :errList){ e.giic_IsActive__c =false;}
        if(!errList.isEmpty()) update errList;
    }
    public static void deActivateCurrentErrorSO(String soId, String errorLogName){
        List<Integration_Error_Log__c> errList = new List<Integration_Error_Log__c>();
        
        if(soId!=null)
            errList = [select giic_IsActive__c from Integration_Error_Log__c where
                       giic_IsActive__c = true and Name =: errorLogName and giic_SalesOrder__c = :soId];
        
        
        for(Integration_Error_Log__c e :errList){ e.giic_IsActive__c =false;}
        if(!errList.isEmpty()) update errList;
    }
    
    public static list<Integration_Error_Log__c> deActivateCurrentError(set<id> objIds,String errorLogName, String objectAPIName)
    {
        list<Integration_Error_Log__c> errList = new list<Integration_Error_Log__c>();
        list<id> listIds = new list<id>();
        listIds.addAll(objIds);
        try
        {
           
            if(objIds != null && !objIds.isEmpty())
            {
                String sQuery = 'select giic_IsActive__c from Integration_Error_Log__c where giic_IsActive__c = true and Name = ' + '\'' +  errorLogName + '\'' +  ' and  ' + objectAPIName + '  IN : listIds';
                errList = database.Query(sQuery);
                for(Integration_Error_Log__c e :errList){ e.giic_IsActive__c =false;}
               
            }
        }catch(Exception ex)
        {
            throw ex;
        }
        
        return errList;
    }
    
}