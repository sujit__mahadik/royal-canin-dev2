/************************************************************************************
Version : 1.0
Created Date : 14 Sep 2018
Function : all promotion related wrapper
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/
global class giic_PromotionWrapper {
    
    /******************* Promotion Request from Insight *******************************************/
    
	global class PromoRequestHeader{
	    /********************************* request from Insite system *****************************/
	    
	    public string orderNumber; // Order number will be send from Insite system
	    public string userRole; // User Role to check the Promotion Eligibility
	    public string customer; // Customer Number will be send from Insite system and it will be check on Insite_Customer_Type__c field of account
	    public decimal totalWeight; // Total Weight of the order 
	    public string paymentMethod; // Payment method will be used to check the promotion, in specific promotion it will  be used
	    public Double totalOrderAmount; // Total Order Amount for the Order
	    public boolean isDropShip; // Flag to check the Drop ship of the order, for drop ship case, addtional 25% charges will be applied on order
	    public list<AppliedPromotion> AppliedPromotion; // Applied Promotion will be send from the Insight for which GLovia API will calcualte the amount
	    
	    /******************************** Variables defined for OMS Promotion Calculaton *********************/
	    
        public string carrier,carrierCode,country,orderId,pricebookName,customerType,accountId,accountRefId;
        public set<string> linePromoIds,headerPromoIds,shippingPromoIds;
        public string headerPromo1,headerPromo2,freeShipPromo,promotionCode;
        public decimal headerPromoVal1,headerPromoVal2;
        public set<string> allPromoApplied;
        public boolean isAdditonalCharge;
        public decimal totalQty;
        public map<string,decimal> maSOLDiscountPercent;
        /***************** Amount field used to store the calculation ****************************************/
        
        public Double totalNetAmount;
        public Decimal orderDiscountPercent,lineDiscountPercent,orderDiscountAmount,lineDiscountAmount;
        
        /********************************************** Promotion Lines ***********************************/
        public string errorMessage;
        public list<PromoRequestLines> SalesOrderLine;
        public list<AddtionalCharge> AppliedCharges;
        Public PromoRequestHeader(){
            this.isAdditonalCharge=false;
            this.headerPromo1='';
            this.headerPromo2='';
            this.freeShipPromo='';
            this.headerPromoVal1=0;
            this.headerPromoVal2=0;
            this.maSOLDiscountPercent=new map<string,decimal>();
            this.AppliedPromotion = new list<giic_PromotionWrapper.AppliedPromotion>();
            this.AppliedCharges = new list<AddtionalCharge>();
            this.totalOrderAmount=0;
            this.totalNetAmount=0;
            this.orderDiscountPercent=0;
            this.lineDiscountPercent=0;
            this.orderDiscountAmount=0;
            this.lineDiscountAmount=0;
            this.linePromoIds=new set<string>();
            this.headerPromoIds=new set<string>();
            this.shippingPromoIds=new set<string>();
            this.allPromoApplied=new set<string>();
        }
    }
    
    /******************************** Promotion Request Lines ************************************/
    global class PromoRequestLines{  
        /**************************************** Insite request ********************************/
        public string solNumber; // Sales Order line number 
        public string productCode; // Product Code 
        public string productCategory; //Product Category
        public Double orderQuantity; //Order quantity
        public Double unitPrice; // Unit Price of the Product
        public Double lineTotalAmount; //Line Total Amount
        public Double productTotalWeight; //Product Total Weight
        public Double cancelledQuantity; // Cancelled Quantity
        
        /************************************ Variables defined for internal calcualtion *************************/
        
        public string orderNumber,orderId,productId,productStyleId;
        public PromoRequestLines(){
            this.lineTotalAmount=0;
            this.productTotalWeight=0;
            this.cancelledQuantity=0;
            this.orderQuantity=0;
            this.unitPrice=0;
        }

    }
    
    /******************* Response for OMS Component *******************************************/
    
    global class PromoResponseWrapper{
        
        @auraEnabled public string orderId;
        @auraEnabled public string orderNumber;
        @auraEnabled public string customerType;
        @auraEnabled public string customer;
        @auraEnabled public Double totalOrderAmount; 
        @auraEnabled public Double netOrderAmount;
        @auraEnabled public double subtotalAmount;
        @auraEnabled public Decimal shippingCharge;
        @auraEnabled public Decimal dropshipCharge;
        @auraEnabled public string dropshipChargeName;
        @auraEnabled public Decimal additionalChargeAmount;
        @auraEnabled public Decimal taxAmount;
        @auraEnabled public boolean isOrderAllocated;
        @auraEnabled public string orderAdditionalDesc;
        @auraEnabled public list<PromotionLines> lstPromoLines;
        @auraEnabled public list<PromotionLines> Promotion;
        @auraEnabled public list<ErrorInformation> lstErrorInformation;
        public PromoResponseWrapper(){isOrderAllocated=false;dropshipCharge=0;}
    }
    global class ErrorInformation{
        @auraEnabled
        public string errorMessage;
        @auraEnabled
        public string productCode;
    }
    global class PromotionLines{
        @auraEnabled
        public boolean isAuto; //IsAutoPromo
        @auraEnabled
        public boolean IsvalidPromo; 
        @auraEnabled
        public boolean isShippingPromo;
        @auraEnabled
        public boolean isHeaderPromo; //IsHeadervalue - in api 
        @auraEnabled
        public Decimal discountPercent;
        @auraEnabled
        public Double discountAmount; //total discount calculated on a order 
        @auraEnabled
        public string promotionId;
        @auraEnabled
        public string promotionName;
        @auraEnabled
        public string promotionCode;
        @auraEnabled
        public string promotionType;
        @auraEnabled
        public string promotionPercent;
        @auraEnabled
        public string promotionvalue;
        @auraEnabled
        public string promoMessage;
        
        
    }
    /************************************* Insite Response for the API ********************************************/
    global Class SalesOrder{
        /*************************************** Header Information *******************************************/
        Public String orderNumber;
        Public String customer;
        Public String userRole;
        Public double totalWeight;
        Public String paymentMethod;
        Public double totalOrderAmount;
        Public double netAmount;
        Public boolean isDropShip;
        
        /******************* Promotion Details *************************/
        public double totalHeaderPromotion;
        public double totalLinePromotion;
        public double totalHeaderDiscount;
        public double totalLineDiscount;
        public string promotionCode;
        
        public string headerPromo1;
        public string headerPromo2;
        public string freeShipPromo;
        public decimal headerPromoVal1;
        public decimal headerPromoVal2;
        /******************************** Lines information **************************/
        /******************* Loyalty Details *************************/
        public decimal availableLoyalty;
        
        public list<SalesOrderLine> SalesOrderLine;
        public list<AppliedPromotion> AppliedPromotion; 
        public list<AddtionalCharge> AppliedCharges; 
        
        public string errorMessage;
        public SalesOrder(){
            SalesOrderLine = new list<SalesOrderLine>();
            this.AppliedPromotion=new list<AppliedPromotion>();
            this.appliedCharges=new list<AddtionalCharge>();
            this.totalHeaderPromotion=0;
            this.totalHeaderDiscount=0;
            this.totalHeaderPromotion=0;
            this.totalLineDiscount=0;
            this.totalOrderAmount=0;
            this.availableLoyalty=0;
            this.totalWeight=0;
            this.promotionCode='';
            this.headerPromo1='';
            this.headerPromo2='';
            this.headerPromoVal1=0;
            this.headerPromoVal2=0;
            this.freeShipPromo='';
        }
    }
    /************************************ Addtional Charge Response Information***********************************/
    global Class AddtionalCharge{
        Public String chargeCode;
        Public double qty;
        Public decimal shippingCharge;
        Public decimal shippingChargeAfterPromo;
        Public string promoCode;
        public AddtionalCharge(string chargeCode,decimal shippingCharge,String promoCode,decimal shippingChargeAfterPromo){
            qty=1;
            this.chargeCode=chargeCode;
            this.shippingCharge=shippingCharge;
            this.promoCode=promoCode;
            this.shippingChargeAfterPromo=shippingChargeAfterPromo;
        }
    }
    /************************************ Sales order Lines Response Information***********************************/
    global Class SalesOrderLine{
        Public String solNumber;
        Public String productCode;
        Public String productCategory;
        Public double orderQuantity;
        Public double unitPrice;
        Public double cancelledQuantity;
        Public double lineTotalAmount;
        Public decimal promotionPercent;
        Public double promotionAmount;
        Public double productTotalWeight;
        public SalesOrderLine(){
            promotionPercent=0;
            cancelledQuantity=0;
            promotionAmount=0;
        }
    }
    /************************************ Applied promotion Response Information***********************************/
    global Class AppliedPromotion {
        public string promoCode;
        public string promotionType;
        public double promotionPercent;
        public decimal promotionValue;
        public string promoMessage;
        public boolean isHeaderPromo;
        public boolean isAutoPromo;        
        public boolean IsValidPromo;
        public AppliedPromotion(){
            this.promoMessage=label.giic_PromoCodeInvalidMessage;
            this.promoCode='';
            this.promotionType='';
            this.promotionPercent=0;
            this.promotionValue=0;
            this.isHeaderPromo=false;
            this.IsValidPromo=false;
            this.isAutoPromo=false;
        }
    }
    global class SpecialPromo{
        public boolean isRequired;
        public boolean isOptional;  
        public integer reqQty;
        public set<string> reqPrdCode;
        public SpecialPromo(){
            this.isRequired=false;
            this.isOptional=false;
            this.reqQty=0;
            reqPrdCode= new set<string>();
        }
    }
}