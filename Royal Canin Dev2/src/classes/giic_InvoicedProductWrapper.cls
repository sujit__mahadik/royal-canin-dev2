/************************************************************************************
Version : 1.0
Name : giic_InvoicedProductWrapper
Created Date : 27 Sep 2018
Function : wrapper class for contain information about Invoiced products
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/

public class giic_InvoicedProductWrapper {
    @AuraEnabled public Boolean isSelected;
    @AuraEnabled public String productId;
    @AuraEnabled public String productSKU;
    @AuraEnabled public String productDescription;
    @AuraEnabled public String invoiceId;
    @AuraEnabled public String invoiceNumber;
    @AuraEnabled public String soId;
    @AuraEnabled public String soPaymentMethod;
    @AuraEnabled public String soNumber;
    @AuraEnabled public String invoiceDetailId;
    @AuraEnabled public String orderDate;
    @AuraEnabled public Decimal orderQuantity;
    @AuraEnabled public Decimal qtyAvailableForReturn;
    @AuraEnabled public String qtyAvailableForReturnEA;
    @AuraEnabled public Decimal invoiceAmount;
    @AuraEnabled public Decimal invoiceAmountStocking;
    @AuraEnabled public String UOM;
    @AuraEnabled public String sellingUOM;
    @AuraEnabled public String sellingUOMId;
    @AuraEnabled public String stockingUOM;
    @AuraEnabled public String stockingUOMId;
    @AuraEnabled public Decimal returnQty;
    @AuraEnabled public Decimal returnQtyOld;
    @AuraEnabled public String returnReason;
    @AuraEnabled public Decimal returnAmount;
    @AuraEnabled public Decimal returnRewardAmount;
    
    @AuraEnabled public String additionalChargeInvNo;
    @AuraEnabled public String additionalChargeInvId;
    @AuraEnabled public String additionalChargeId;
    @AuraEnabled public String additionalChargeName;
    @AuraEnabled public Decimal additionalChargeUnitPrice;
    @AuraEnabled public Decimal additionalChargeUnitCost;
    @AuraEnabled public Decimal additionalChargeQuantity;
    @AuraEnabled public Decimal additionalChargeAmount;
    @AuraEnabled public Decimal additionalChargeTaxAmount;
    @AuraEnabled public Decimal additionalChargeTotalAmount;
    
    public giic_InvoicedProductWrapper(){
        additionalChargeInvNo = '';
        additionalChargeInvId = '';
        additionalChargeId= '';
        additionalChargeName= '';
        additionalChargeUnitPrice = 0;
        additionalChargeUnitCost = 0;
        additionalChargeQuantity = 0;
        additionalChargeAmount = 0;
        additionalChargeTaxAmount = 0;
        additionalChargeTotalAmount = 0;
        isSelected = false;
        productId = '';
        productSKU = '';
        productDescription = '';
        invoiceId = '';
        invoiceNumber = '';
        soId = '';
        soNumber = '';
        invoiceDetailId = '';
        orderDate = '';
        orderQuantity = 0;
        qtyAvailableForReturnEA = 'NA';
        qtyAvailableForReturn = 0;
        invoiceAmount = 0;
        invoiceAmountStocking = 0;
        UOM = '';
        sellingUOM = '';
        sellingUOMId = '';
        stockingUOM = '';
        stockingUOMId = '';
        returnQty = 0;
        returnQtyOld = 0;
        returnReason = '--None--';
        returnAmount = 0;
        returnRewardAmount = 0;
    }
}