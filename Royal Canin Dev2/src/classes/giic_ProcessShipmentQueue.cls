/************************************************************************************
Version : 1.0
Name : giic_ProcessShipmentQueue
Created Date : 09 Oct 2018
Function : queue to process records one by one
Modification Log :
Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/
public class giic_ProcessShipmentQueue implements Queueable,Database.AllowsCallouts 
{
     private List<id> lstWHSIds;
    
    public giic_ProcessShipmentQueue (List<id> lstWHSIds) { 
        this.lstWHSIds = lstWHSIds;
    }
    
    public void execute(QueueableContext queCont) {
        if(lstWHSIds.isEmpty()) return;
        Id whasId = lstWHSIds[0];
        Map<String, Object> mapResult = new Map<String, Object>();
        Map<String, Object> mapUpdateResult = new Map<String, Object>();
        
            try{
                List<gii__WarehouseShipmentAdviceStaging__c> lstWHSAStaging = giic_SOShipmentUtility.getWHSAStaging(whasId);
                if(!lstWHSAStaging.isEmpty()){
                    Date shipDate = null;
                    if(lstWHSAStaging[0].giic_ShipDate__c != null ) shipDate = lstWHSAStaging[0].giic_ShipDate__c;
                    //else shipDate = System.Today(); 
                    String soNumber = '';
                    if(lstWHSAStaging[0].giic_WHSShipNo__r.giic_SalesOrder__r.Name != null && lstWHSAStaging[0].giic_WHSShipNo__r.giic_SalesOrder__r.Name != '') soNumber = lstWHSAStaging[0].giic_WHSShipNo__r.giic_SalesOrder__r.Name;
                    mapResult.put('shipDate', String.valueOf(shipDate));
                    mapResult.put('soNumber', soNumber);
                     mapResult.put('recId', whasId);
                    mapResult.put('fulfilmentNumber', lstWHSAStaging[0].giic_WHSShipNo__c);
                    //call updateSOLandIR method
                        if(!lstWHSAStaging[0].Warehouse_Shipment_Advice_Lines_Staging__r.isEmpty()){
                            if(giic_SOShipmentUtility.checkFulfilmentLines(lstWHSAStaging[0].Id, lstWHSAStaging[0].Warehouse_Shipment_Advice_Lines_Staging__r.size(), lstWHSAStaging[0].giic_WHSShipNo__c)){
                                 Map<String, String> mapFulfilmentLines = new Map<String, String>();
                                for(gii__WarehouseShipmentAdviceLinesStaging__c objLine : lstWHSAStaging[0].Warehouse_Shipment_Advice_Lines_Staging__r){
                                    mapFulfilmentLines.put(objLine.giic_WHSShipLineNo__r.giic_SalesOrderLine__r.Name, objLine.giic_WHSShipLineNo__r.Name);
                                }
                                mapResult.put('mapFulfilmentLines', mapFulfilmentLines);
                                mapUpdateResult = giic_SOShipmentUtility.updateSOLandIR(new Map<String, Object>{'lstWHSAStaging'=>lstWHSAStaging});
                                System.debug('mapUpdateResult::::' + mapUpdateResult); // do not remove this log
                            }else{ update new gii__WarehouseShipmentAdviceStaging__c(id = whasId, giic_ProcessStatus__c = giic_Constants.PROCESSEDWITHERROR); return;}
                        }else{
                            update new gii__WarehouseShipmentAdviceStaging__c(id = whasId, giic_ProcessStatus__c = giic_Constants.PROCESSEDWITHERROR);
                            giic_CyberSourceRCCCUtility.handleErrorFuture(whasId, giic_Constants.USER_STORY_SHIPMENTCREATION, giic_Constants.UPDATE_SOL_ERROR_CODE, 'Lines not found');
                            return;
                        }
                    
                    List<Integration_Error_Log__c> lstError = new List<Integration_Error_Log__c>();
                    if(mapUpdateResult.containsKey('lstError') && mapUpdateResult.get('lstError') != null){
                        lstError = (List<Integration_Error_Log__c>)mapUpdateResult.get('lstError');
                    }
                    if(!lstError.isEmpty()){
                       insert lstError; 
                       lstWHSAStaging[0].giic_ProcessStatus__c = giic_Constants.PROCESSEDWITHERROR;
                       update lstWHSAStaging[0];
                    }else{ 
                        
                        Boolean isTaxExempt = false;
                        
                        List<gii__SalesOrder__c> lstobjOrder = [Select Id, giic_Source__c,giic_TaxExempt__c,gii__Account__r.Check_if_you_are_Tax_Exempt__c, gii__Account__r.Resale_Sales_Tax_Cert_Expiration_Date__c from gii__SalesOrder__c where Name = : soNumber];
                        if(!lstobjOrder.isEmpty())
                        {
                            isTaxExempt = lstobjOrder[0].giic_TaxExempt__c;
                            if(lstobjOrder[0].giic_Source__c == giic_Constants.ORDER_TYPE_OLP) // tax will not be calculated for OLP Order
                                isTaxExempt = true;
                        }
                        
                        
                        if(!Test.isRunningTest()) ID jobID = System.enqueueJob(new giic_CalculateTaxforSOLQueue(mapUpdateResult, mapResult, whasId, lstWHSAStaging[0].Warehouse_Shipment_Advice_Lines_Staging__r, isTaxExempt ));
                    }
                
                }else{
                    return;
                }
            }catch(exception e){
                System.debug('Exception::' + e.getMessage() + 'Line::' + e.getLineNumber());
                update new gii__WarehouseShipmentAdviceStaging__c(id = whasId, giic_ProcessStatus__c = giic_Constants.PROCESSEDWITHERROR);
                giic_CyberSourceRCCCUtility.handleErrorFuture(whasId, giic_Constants.USER_STORY_INVOICECREATION, giic_Constants.DML_EX_CODE, e.getMessage() + 'Line::' + e.getLineNumber());
            }
            
        lstWHSIds.remove(0);
        if(!lstWHSIds.isEmpty() && !Test.isRunningTest()) ID jobID = System.enqueueJob(new giic_ProcessShipmentQueue(lstWHSIds));
        
    }
}