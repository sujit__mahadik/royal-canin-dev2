global class AccountRankingBatch  implements Database.batchable<sObject>{
    /*
    Execute the appropriate commands below in 'Execute Anonymous' window

AccountRankingBatch a = new AccountRankingBatch();
Database.executeBatch(a,1);

    */
    
    global String query;
    global Integer mode;
    global String pillar;
    
    global AccountRankingBatch() {
        this.query = 'Select id, name, pillar__c from Region__c';  //region ranks
        if (pillar =='VET') this.query+= ' where pillar__c =\'VET\'';
        if (pillar =='RETAIL') this.query+= ' where pillar__c =\'RETAIL\'';
        if (pillar =='INFLUENCER') this.query+= ' where pillar__c =\'INFLUENCER\'';
    }
    
    //get Querylocator with the specitied query
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext bc, List<sObject> regions) {
        List<Account> updatedAccounts = new List<Account>();
        Set <Id> territoryassignmentIds = new Set <Id>();
        Map <Id, integer> territoryRanking = new Map <Id,Integer>();
        Map <Id, integer> regionRanking = new Map <Id,Integer>();
        Set <Id> regionIds = new Set<Id>();

        for(sObject s : regions){
            Region__c r = (Region__c)s;
            regionIds.add(r.id);
            regionRanking.put(r.id, 1);
        }

        for (TerritoryAssignment__c ta: [select id, TerritoryID__c from TerritoryAssignment__c where TerritoryID__r.region__c in :regionIds]){
            territoryassignmentIds.add(ta.id);
            if (!territoryRanking.keySet().contains(ta.TerritoryID__c)){
                territoryRanking.put(ta.TerritoryID__c, 1);
            }   
        }

        Map <Id, Account> accountUpdateMap = new Map <Id, Account>();
        
        for (Account a: [select id, In_Clinic_OLP_Current_Fiscal_YTD__c, Current_Fiscal_YTD__c, 
                            Current_Year_Territory_Assignment__r.TerritoryID__r.region__c, 
                            Current_Year_Territory_Assignment__r.TerritoryID__c
                         from Account 
                         where Current_Year_Territory_Assignment__c in :territoryassignmentIds
                         and Account_Status__c = 'Customer'
                         and In_Clinic_OLP_Current_Fiscal_YTD__c > 0.0
                         order by In_Clinic_OLP_Current_Fiscal_YTD__c
                         //and Current_Fiscal_YTD__c > 0.0
                         //order by  Current_Fiscal_YTD__c 
                         desc ]){
            //loop through all accounts in the current territory assignments
            integer rRank = regionRanking.get(a.Current_Year_Territory_Assignment__r.TerritoryID__r.region__c);
            integer tRank = territoryRanking.get(a.Current_Year_Territory_Assignment__r.TerritoryID__c);
            
            /*
            if (a.Current_Fiscal_YTD__c > 0.0) {
                updatedAccounts.add(new Account(id=a.id, Region_Ranking__c=rRank, Territory_Ranking__c=tRank ));
                rRank+=1;
                tRank+=1;
            } else {
                updatedAccounts.add(new Account(id=a.id, Region_Ranking__c=99999, Territory_Ranking__c=99999 ));
            }
            */
            updatedAccounts.add(new Account(id=a.id, Region_Ranking__c=rRank, Territory_Ranking__c=tRank ));
            rRank+=1;
            tRank+=1;
            
            regionRanking.put(a.Current_Year_Territory_Assignment__r.TerritoryID__r.region__c, rRank);
            territoryRanking.put(a.Current_Year_Territory_Assignment__r.TerritoryID__c, tRank);
        }

        system.debug(updatedAccounts);
        update updatedAccounts;
    }
    
    global void finish(Database.BatchableContext bc) {
        
        AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,TotalJobItems, CreatedBy.Email FROM AsyncApexJob WHERE Id = :BC.getJobId()];
          
        if(a.status == 'Failed') {
            Job_Failure_Notification_Email__c myemail = Job_Failure_Notification_Email__c.getValues('Email Id');
            String myEmails = myemail.Notification_Email__c;
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {myemails};
            mail.setToAddresses(toAddresses);
            mail.setSubject('Apex Sharing Recalculation ' + a.Status);
            mail.setPlainTextBody('The AccountSalesSummaryBatch job has Failed');
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
        
        if(a.status == 'Completed' || a.TotalJobItems == 0 || a.NumberOfErrors > 0) {
            Job_Failure_Notification_Email__c myemail = Job_Failure_Notification_Email__c.getValues('Email Id');
            String myEmails = myemail.Notification_Email__c;
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {myemails};
            mail.setToAddresses(toAddresses);
            
            if(test.isrunningtest()) {
                mail.setSubject('Test execution mail Apex Sharing Recalculation ' + a.Status);
            }else {
                mail.setSubject('Apex Sharing Recalculation ' + a.Status);
            }
            
            mail.setPlainTextBody('The AccountRankingBatch job processed ' + a.TotalJobItems + ' batches with '+ a.NumberOfErrors + ' failures. The sales summary batch jobs got completed.');
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }   
    }
    
    public class cRank {
        
        public integer ranking;
        public Id       accountId;
        
        public cRank(id acctId, integer rank){
            this.ranking = rank+1;
            this.accountId = acctId;
        }
    }
}