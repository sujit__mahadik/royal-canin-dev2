/************************************************************************************
Version : 1.0
Name : giic_SOStagingToSOConversionScheduler
Created Date : 17 Aug 2018
Function : Schedule giic_SOStagingToSOConversionBatch class
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/
global class giic_SOStagingToSOConversionScheduler implements Schedulable {

   global void execute(SchedulableContext ctx) {
       String sObjectApiName = 'gii__SalesOrderStaging__c';
        Map<String, giic_IntegrationCustomMetaDataWrapper> mapIntegrationSettings = giic_IntegrationCustomMetaDataUtil.getIntegrationSettings(sObjectApiName);
        
        giic_SOStagingToSOConversionBatch batch = new giic_SOStagingToSOConversionBatch(mapIntegrationSettings, sObjectApiName);
        
        Database.executeBatch(batch);
   }   
}