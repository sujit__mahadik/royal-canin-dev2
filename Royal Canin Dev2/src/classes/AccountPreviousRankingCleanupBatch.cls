global class AccountPreviousRankingCleanupBatch implements Database.Batchable<sObject> {
    /*
    Execute the appropriate commands below in 'Execute Anonymous' window

AccountPreviousRankingCleanupBatch a = new AccountPreviousRankingCleanupBatch();
Database.executeBatch(a);

    */
    
    global final String query;

    global AccountPreviousRankingCleanupBatch(){
        query='SELECT Id, Account_Status__c, In_Clinic_OLP_Current_Fiscal_YTD__c, Current_Fiscal_YTD__c, Region_Ranking__c, Territory_Ranking__c FROM Account';         
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        List<Account> updatedAccounts = new List<Account>();
        
        for(sObject s : scope){
            Account a = (Account)s;
            if (a.Account_Status__c == 'Customer') {
                //if ((a.Current_Fiscal_YTD__c == 0.0) || (a.Current_Fiscal_YTD__c == NULL)) {
                if (    (a.In_Clinic_OLP_Current_Fiscal_YTD__c == 0.0) || 
                        (a.In_Clinic_OLP_Current_Fiscal_YTD__c == NULL)) {
                    updatedAccounts.add(new Account(id=a.id, Region_Ranking__c=99999, Territory_Ranking__c=99999 ));
                }
            } else {
                updatedAccounts.add(new Account(id=a.id, Region_Ranking__c=NULL, Territory_Ranking__c=NULL ));
            }
        }
        update updatedAccounts;
    }
    
    global void finish(Database.BatchableContext BC){
        
        AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,TotalJobItems, CreatedBy.Email FROM AsyncApexJob WHERE Id = :BC.getJobId()];
          
        if(a.status == 'Failed') {
            Job_Failure_Notification_Email__c myemail = Job_Failure_Notification_Email__c.getValues('Email Id');
            String myEmails = myemail.Notification_Email__c;
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {myemails};
            mail.setToAddresses(toAddresses);
            mail.setSubject('Apex Sharing Recalculation ' + a.Status);
            mail.setPlainTextBody('The AccountSalesSummaryBatch job has Failed');
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
        
        if(a.status == 'Completed' || a.TotalJobItems == 0 || a.NumberOfErrors > 0) {
            Job_Failure_Notification_Email__c myemail = Job_Failure_Notification_Email__c.getValues('Email Id');
            String myEmails = myemail.Notification_Email__c;
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {myemails};
            mail.setToAddresses(toAddresses);
            if(test.isrunningtest()) {
                mail.setSubject('Test execution mail Apex Sharing Recalculation ' + a.Status);
            }else {
                mail.setSubject('Apex Sharing Recalculation ' + a.Status);
            }
            mail.setPlainTextBody('The AccountPreviousRankingCleanupBatch job processed ' + a.TotalJobItems + ' batches with '+ a.NumberOfErrors + ' failures.');
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }  
         
        if(a.status !='Failed' && a.TotalJobItems != 0 && a.TotalJobItems != a.NumberOfErrors && !test.isrunningtest()) {            
            AccountRankingBatch obj = new AccountRankingBatch();
            Database.executeBatch(obj,1);
        }           
    }
}