global class IsParentUpdateBatch implements Database.Batchable<SObject> {
    
    
    global Database.queryLocator start(Database.batchableContext dbc) {
        return Database.getQueryLocator('Select id,name,Is_Parent__c,parentId,(select id,name, is_parent__c from ChildAccounts) from Account');
    }

    global void execute(Database.BatchableContext bc, LIST<sobject> sObjLst) {
        System.debug('---Start--Executing Batch---');
        List<String> parentAccountIds = new List<String>();
        List<Account> parentAccounts = new List<Account>();
        List<Account> isParentFlagUpdatedAccounts = new List<Account>();
        if(!sObjLst.isEmpty()) {
            for(Sobject sobj : sobjLst) {
                 Account accounttt = (Account)sobj;
                 List<Account> childAccounts = accounttt.ChildAccounts;                
                 if(childAccounts != null && childAccounts.size() > 0 && accounttt.is_parent__c != 'Yes') {
                     accounttt.is_parent__c= 'Yes';
                     isParentFlagUpdatedAccounts.add(accounttt);
                 } else if(childAccounts != null && childAccounts.size() <= 0 && accounttt.is_parent__c != 'No') {
                     accounttt.is_parent__c= 'No';
                     isParentFlagUpdatedAccounts.add(accounttt);                    
                   } else if(childAccounts == null && accounttt.is_parent__c != 'No' ) {
                       accounttt.is_parent__c= 'No';
                       isParentFlagUpdatedAccounts.add(accounttt);
                     }
                
                //parentAccountIds.add(((Account)sobj).parentId);
                 }
            
                 if(!isParentFlagUpdatedAccounts.isEmpty()) {
                     Database.update(isParentFlagUpdatedAccounts,false);
                 }
            }
        }    
    global void finish(Database.BatchableContext bc) {   
    }
}