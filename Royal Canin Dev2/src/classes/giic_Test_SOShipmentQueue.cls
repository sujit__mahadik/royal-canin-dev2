/************************************************************************************
Version : 1.0
Name : giic_Test_SOShipmentQueue
Created Date : 09 October 2018
Function : test class for giic_SOShipmentQueue 
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/
@isTest(SeeAllData = false)
private class giic_Test_SOShipmentQueue {

 @testSetup
    static void setup()
    {
        giic_Test_DataCreationUtility.insertWarehouse();
        
        giic_Test_DataCreationUtility.testSystemPolicyCreation();
        giic_Test_DataCreationUtility.insertGlobalParametersCustomSetting();
        giic_Test_DataCreationUtility.insertConsumerAccount();
        giic_Test_DataCreationUtility.createCarrier();
        giic_Test_DataCreationUtility.insertUnitofMeasure();
        //giic_Test_DataCreationUtility.insertConsumerAccount_N(10);
        List<gii__Product2Add__c> lstProd = giic_Test_DataCreationUtility.insertProduct();
        System.debug('lstProd::'+lstProd);
        giic_Test_DataCreationUtility.insertLocations();
        giic_Test_DataCreationUtility.insertProductInventory(1);
        giic_Test_DataCreationUtility.insertProductInventoryByLoc();
        giic_Test_DataCreationUtility.insertProductInventoryQTYDetails();
        giic_Test_DataCreationUtility.insertSalesOrder(); // 0 - open sales order , 1 - release sales order
        
        
        giic_Test_DataCreationUtility.lstSalesOrder[1].giic_TaxCal__c = false;
        giic_Test_DataCreationUtility.lstSalesOrder[1].giic_SOStatus__c = giic_Constants.SO_PICKED;
        update giic_Test_DataCreationUtility.lstSalesOrder[1];
        giic_Test_DataCreationUtility.insertSOLine(new list<gii__SalesOrder__c>{giic_Test_DataCreationUtility.lstSalesOrder[1]});
        //retriee sales order lines whre order is released 
        List<gii__SalesOrderLine__c>  lstSalesOrderLines = [select id,name,gii__UnitPrice__c,gii__SalesOrder__c,gii__Warehouse__c,gii__OrderQuantity__c,gii__Status__c,gii__Product__c, gii__Product__r.giic_ProductSKU__c from gii__SalesOrderLine__c where gii__SalesOrder__c = : giic_Test_DataCreationUtility.lstSalesOrder[1].id];
        for(gii__SalesOrderLine__c soline  : lstSalesOrderLines)
        {
            soline.giic_LineStatus__c = giic_Constants.SHIPPED;
        }
        update lstSalesOrderLines;

        list<gii__SalesOrder__c>  lstSalesOrder = [select id,gii__Released__c, gii__Carrier__r.giic_UniqueCarrier__c, gii__Warehouse__r.giic_WarehouseCode__c, gii__Account__c,name from gii__SalesOrder__c where id = : giic_Test_DataCreationUtility.lstSalesOrder[1].id ];
       
        giic_Test_DataCreationUtility.insertSalesOrderPayment(giic_Test_DataCreationUtility.lstSalesOrder[1].id);
        giic_Test_DataCreationUtility.insertWarehouseShipmentAdviceStaging(lstSalesOrder);
        giic_Test_DataCreationUtility.insertWarehouseShipmentAdviceLinesStaging(lstSalesOrderLines);
        giic_Test_DataCreationUtility.CreateAdminUser();   
    }
    
    @isTest
     private static void testpositiveSOShipmentQueue() 
     {
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        
        System.Runas(u)        
        {
               Test.startTest();
             gii__SalesOrder__c so;
            
            list<gii__SalesOrder__c> solist = [select id,name,gii__PaymentMethod__c,gii__Account__c,gii__Released__c from gii__SalesOrder__c];
            if(solist.size() > 1)
                so = solist[1];
            
            list<gii__WarehouseShipmentAdviceStaging__c> lstWarehouseshpmentStaging = [Select Id, Name,giic_ProcessStatus__c, giic_Carrier__c, giic_WHSShipNo__r.Name, giic_isTaxCommitted__c, giic_ShipDate__c, giic_WHSShipNo__c, giic_WHSShipNo__r.giic_SalesOrder__r.Name, giic_Warehouse__c, 
                             (Select Id, Name, giic_WHSShipLineNo__r.giic_WHShippingOrderStaging__r.giic_SalesOrder__c, giic_WHSShipLineNo__c, giic_WHSShipLineNo__r.Name,giic_WHSShipAdviceStaging__r.giic_WHSShipNo__c,giic_WHSShipAdviceStaging__r.giic_WHSShipNo__r.giic_Carrier__c,
                              giic_WHSShipLineNo__r.giic_SalesOrderLine__r.Name, giic_WHSShipLineNo__r.giic_SalesOrderLine__r.gii__OrderQuantity__c,giic_WHSShipLineNo__r.giic_SalesOrderLine__r.giic_FulfillmentAttempts__c,
                              giic_WHSShipAdviceStaging__r.giic_isTaxCommitted__c, giic_WHSShipLineNo__r.giic_SalesOrderLine__c, giic_OrderQty__c, giic_ShipQty__c, giic_ProductSKU__c, 
                              giic_UOM__c, giic_WHSShipAdviceStaging__c, giic_ProductLot__c, giic_WHSShipLineNo__r.giic_Product__r.giic_ProductSKU__c, giic_TrackingNumber__c, giic_Carrier__c,giic_TrackingNumber2__c,giic_TrackingNumber3__c,giic_TrackingNumber4__c,giic_TrackingNumber5__c,
                              giic_WHSShipAdviceStaging__r.giic_ShipDate__c from Warehouse_Shipment_Advice_Lines_Staging__r )  
                              From gii__WarehouseShipmentAdviceStaging__c];

            Map<String, Object> mapResult = new Map<String, Object>();
            if(lstWarehouseshpmentStaging != null && !lstWarehouseshpmentStaging.isEmpty())
            {
                Date shipDate = system.today(); 
                String soNumber = lstWarehouseshpmentStaging[0].giic_WHSShipNo__r.giic_SalesOrder__r.Name;
                mapResult.put('shipDate', String.valueOf(shipDate));
                mapResult.put('soNumber', soNumber);
                mapResult.put('recId', lstWarehouseshpmentStaging[0].Id);
                mapResult.put('fulfilmentNumber', lstWarehouseshpmentStaging[0].giic_WHSShipNo__c);
               
                List<gii__SalesOrderLine__c>  lstSalesOrderLines = [select id,name,gii__Warehouse__c,gii__OrderQuantity__c,gii__Status__c,gii__SalesOrder__c,gii__SalesOrder__r.gii__Account__c,gii__Product__c from gii__SalesOrderLine__c where gii__SalesOrder__c = : so.id];
                giic_Test_DataCreationUtility.createInventoryReservation(lstSalesOrderLines);
                list<gii__InventoryReserve__c> lstInvRes = [select id,gii__ProductInventorySequence__c,gii__Status__c,gii__ProductInventorybyLocation__c,gii__Location__c,gii__ProductInventoryQuantityDetail__c,gii__SalesOrder__c,gii__SalesOrderLine__r.gii__Product__c,gii__SalesOrderLine__r.gii__OrderQuantity__c,gii__SalesOrderLine__r.giic_LineStatus__c ,gii__ReserveQuantity__c from gii__InventoryReserve__c ];
                //system.debug('before update inv' + lstInvRes);
                system.assertEquals(lstInvRes.size()>0, true);
                
                for(gii__InventoryReserve__c invRes : lstInvRes)
                {
                   // system.debug('inv res:' + invRes.gii__ProductInventorySequence__c);
                    
                    invRes.gii__ReserveQuantity__c = invRes.gii__SalesOrderLine__r.gii__OrderQuantity__c;
                    invRes.gii__Status__c = 'Reserved';
                }
                update lstInvRes;
               // system.debug('after update inv:::'+lstInvRes);

                 Map<String, Object> mapUpdateResult = giic_SOShipmentUtility.updateSOLandIR(new Map<String, Object>{'lstWHSAStaging'=>lstWarehouseshpmentStaging});
                System.debug('mapUpdateResult='+mapUpdateResult);
             
                    ID jobID = System.enqueueJob(new giic_SOShipmentQueue(mapUpdateResult, mapResult, lstWarehouseshpmentStaging[0].Id, lstWarehouseshpmentStaging[0].Warehouse_Shipment_Advice_Lines_Staging__r));
                    System.assertNotEquals(jobID, null);
                Test.stopTest();
            }               
        }
    }
    
    @isTest
     private static void testnegativeSOShipmentQueue() 
     {
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        
        System.Runas(u)        
        {
             gii__SalesOrder__c so;
            
            list<gii__SalesOrder__c> solist = [select id,name,gii__PaymentMethod__c,gii__Account__c,gii__Released__c from gii__SalesOrder__c];
            if(solist.size() > 1)
                so = solist[1];
            
            list<gii__WarehouseShipmentAdviceStaging__c> lstWarehouseshpmentStaging = [Select Id, Name,giic_ProcessStatus__c, giic_Carrier__c, giic_WHSShipNo__r.Name, giic_isTaxCommitted__c, giic_ShipDate__c, giic_WHSShipNo__c, giic_WHSShipNo__r.giic_SalesOrder__r.Name, giic_Warehouse__c, 
                             (Select Id, Name, giic_WHSShipLineNo__r.giic_WHShippingOrderStaging__r.giic_SalesOrder__c, giic_WHSShipLineNo__c, giic_WHSShipLineNo__r.Name,giic_WHSShipAdviceStaging__r.giic_WHSShipNo__c,giic_WHSShipAdviceStaging__r.giic_WHSShipNo__r.giic_Carrier__c,
                              giic_WHSShipLineNo__r.giic_SalesOrderLine__r.Name, giic_WHSShipLineNo__r.giic_SalesOrderLine__r.gii__OrderQuantity__c,giic_WHSShipLineNo__r.giic_SalesOrderLine__r.giic_FulfillmentAttempts__c,
                              giic_WHSShipAdviceStaging__r.giic_isTaxCommitted__c, giic_WHSShipLineNo__r.giic_SalesOrderLine__c, giic_OrderQty__c, giic_ShipQty__c, giic_ProductSKU__c, 
                              giic_UOM__c, giic_WHSShipAdviceStaging__c, giic_ProductLot__c, giic_WHSShipLineNo__r.giic_Product__r.giic_ProductSKU__c, giic_TrackingNumber__c, giic_Carrier__c,giic_TrackingNumber2__c,giic_TrackingNumber3__c,giic_TrackingNumber4__c,giic_TrackingNumber5__c,
                              giic_WHSShipAdviceStaging__r.giic_ShipDate__c from Warehouse_Shipment_Advice_Lines_Staging__r )  
                              From gii__WarehouseShipmentAdviceStaging__c];

            Map<String, Object> mapResult = new Map<String, Object>();
            if(lstWarehouseshpmentStaging != null && !lstWarehouseshpmentStaging.isEmpty())
            {
                Date shipDate = system.today(); 
                String soNumber = lstWarehouseshpmentStaging[0].giic_WHSShipNo__r.giic_SalesOrder__r.Name;
                mapResult.put('shipDate', String.valueOf(shipDate));
                mapResult.put('soNumber', soNumber);
                mapResult.put('recId', lstWarehouseshpmentStaging[0].Id);
                mapResult.put('fulfilmentNumber', lstWarehouseshpmentStaging[0].giic_WHSShipNo__c);
               
                List<gii__SalesOrderLine__c>  lstSalesOrderLines = [select id,name,gii__Warehouse__c,gii__OrderQuantity__c,gii__Status__c,gii__SalesOrder__c,gii__SalesOrder__r.gii__Account__c,gii__Product__c from gii__SalesOrderLine__c where gii__SalesOrder__c = : so.id];
                giic_Test_DataCreationUtility.createInventoryReservation(lstSalesOrderLines);
                list<gii__InventoryReserve__c> lstInvRes = [select id,gii__ProductInventorySequence__c,gii__Status__c,gii__ProductInventorybyLocation__c,gii__Location__c,gii__ProductInventoryQuantityDetail__c,gii__SalesOrder__c,gii__SalesOrderLine__r.gii__Product__c,gii__SalesOrderLine__r.gii__OrderQuantity__c,gii__SalesOrderLine__r.giic_LineStatus__c ,gii__ReserveQuantity__c from gii__InventoryReserve__c ];
                system.debug('before update inv' + lstInvRes);
                for(gii__InventoryReserve__c invRes : lstInvRes)
                {
                    //system.debug('inv res:' + invRes.gii__ProductInventorySequence__c);
                    invRes.gii__ReserveQuantity__c = invRes.gii__SalesOrderLine__r.gii__OrderQuantity__c; invRes.gii__Status__c = 'Reserved';
                }
                update lstInvRes;
               // system.debug('after update inv:::'+lstInvRes);

                 Map<String, Object> mapUpdateResult;// = giic_SOShipmentUtility.updateSOLandIR(new Map<String, Object>{'lstWHSAStaging'=>lstWarehouseshpmentStaging});
                System.debug('mapUpdateResult='+mapUpdateResult);
                Test.startTest();
                     try{
                         ID jobIDError = System.enqueueJob(new giic_SOShipmentQueue(mapUpdateResult, mapResult, lstWarehouseshpmentStaging[0].Id, null));
                         System.assertNotEquals(jobIDError, null);
                    }catch(exception e){
                        System.debug(e.getMessage());
                    }
                Test.stopTest();
            }               
        }
    }
}