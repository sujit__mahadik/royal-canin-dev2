/************************************************************************************
Version : 1.0
Created Date : 03 Dec 2018
Function : all promotion related method
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/
public class giic_PromotionUtility {
    /*
    * Method name : validateSpecialPromo
    * Description : validate Special Promotion for required and optional products
    * Return Type : it will not return anything but it will set the values in passes parameter 
    * Parameter : setProductCodesToMatch => all product codes which needs to be checked for the sales order, objPromo=> Promotion records to be validated
                                            , mapPromotionLines=> all lines records for the promotion,isRequired=> check for the product is required, isOptional=> check for the promotion is optional
    */
    public static giic_PromotionWrapper.SpecialPromo validateSpecialPromo(set<string> setProductCodesToMatch,gii__Promotion__c objPromo,
                                            map<Id,gii__PromotionLine__c> mapPromotionLines){
        system.debug('**objPromo.gii__Promotion_Details__r**'+objPromo.gii__Promotion_Details__r);
        system.debug('**setProductCodesToMatch**'+setProductCodesToMatch);
        integer reqQty=0;
        giic_PromotionWrapper.SpecialPromo objSpecialPromo=new giic_PromotionWrapper.SpecialPromo();
        for(gii__PromotionLine__c objPromoLine :  objPromo.gii__Promotion_Details__r){
            if(mapPromotionLines.containsKey(objPromoLine.id)){
                system.debug('**mapPromotionLines**'+mapPromotionLines.get(objPromoLine.id).gii__PromotionLineProducts__r);
                for(gii__PromotionLineProduct__c objPromoLineProduct :  mapPromotionLines.get(objPromoLine.id).gii__PromotionLineProducts__r){
                    system.debug('**objPromoLineProduct.gii__Product__r.gii__ProductReference__r.SKU__c**'+setProductCodesToMatch.contains(objPromoLineProduct.gii__Product__r.gii__ProductReference__r.SKU__c));
                    if(objPromoLineProduct.giic_IsRequired__c && setProductCodesToMatch.contains(objPromoLineProduct.gii__Product__r.gii__ProductReference__r.SKU__c)){
                        objSpecialPromo.isRequired=true;
                        reqQty = reqQty+1;
                        objSpecialPromo.reqPrdCode.add(objPromoLineProduct.gii__Product__r.gii__ProductReference__r.SKU__c);
                    }
                    else if(!objPromoLineProduct.giic_IsRequired__c && setProductCodesToMatch.contains(objPromoLineProduct.gii__Product__r.gii__ProductReference__r.SKU__c)){
                        objSpecialPromo.isOptional=true;
                    }
                }
                
            }
        }
        objSpecialPromo.reqQty=reqQty;
       return objSpecialPromo;
    }
    /*
    * Method name : validatePromo
    * Description : validate all promo for weight, quantity etc 
    * Return Type :   map<string,object> => error message and error variables
    * Parameter : objPromo=> promotion, objPRHeader => giic_PromotionWrapper.PromoRequestHeader wrappper
    */
    public static map<string,object> validatePromo(gii__Promotion__c objPromo,giic_PromotionWrapper.PromoRequestHeader objPRHeader,Boolean isAPI,boolean linePromoApplied){
        Boolean isError=false;
        string errorMessage='';
        //system.debug('***objPRHeader**'+objPRHeader);
        /********************************* check if Sales order already has Promotion applied **********************************************/
        if(objPromo.giic_PromotionType__c!=giic_Constants.DROPSHIPPROMO && objPRHeader.allPromoApplied != null && objPRHeader.allPromoApplied.contains(objPromo.id)){isError=true;errorMessage=label.giic_PromoCodeAlreadyAppliedMessage;}
        
        /********************************* check if Sales order already has Promotion applied **********************************************/
        else if(objPRHeader.headerPromoIds != null && objPromo.giic_PromotionType__c!=giic_Constants.DROPSHIPPROMO && objPRHeader.headerPromoIds.size() > 1 && objPromo.giic_PromotionType__c !=giic_Constants.SHIPPINGPROMO && objPromo.giic_IsHeaderPromo__c){isError=true;errorMessage=label.giic_PromoHeaderLevelLimitMessage;}
    
         
        /********************************* check if Sales order already has line item promotion applied **********************************************/
        else if(!objPromo.giic_IsHeaderPromo__c &&  objPromo.giic_PromotionType__c!=giic_Constants.DROPSHIPPROMO && objPromo.giic_PromotionType__c !=giic_Constants.SHIPPINGPROMO && objPRHeader.linePromoIds !=null && objPRHeader.linePromoIds.size() > 0){isError=true;errorMessage=label.giic_LinePromoCodeAlreadyAppliedMessage;}
    
        /********************************* check if Promotion type is Quantity based and Total qty is greater than max limit **********************************************/
        else if(objPromo.giic_PromotionType__c == label.giic_PromotionTypeQuantityBased && objPromo.giic_MaxLimit__c < objPRHeader.totalQty){
            isError=true;errorMessage=label.giic_PromoQtyGreaterMaxLimitMessage;
        }
        
        /********************************* check if min order amount on order **********************************************/
        else if(objPromo.giic_MinimumAmount__c != null && objPRHeader.totalNetAmount != null && objPRHeader.totalNetAmount < objPromo.giic_MinimumAmount__c ){
            isError=true;errorMessage=label.giic_PromoOrderAmountNotSufficientMessage;
        }
    
        /********************************* check if Promotion type is Weight Based and Total weight is greater than max limit **********************************************/
        else if(objPromo.giic_PromotionType__c == label.giic_PromotionTypeWeightBased && objPromo.giic_MaxLimit__c < objPRHeader.totalWeight){
            isError=true;errorMessage=label.giic_PromoOrderWeightGreaterMaxLimitMessage;
        }
        
        /********************************* check if Promotion type is Drop ship and order is not dropship **********************************************/
        else if(objPromo.giic_PromotionType__c==giic_Constants.DROPSHIPPROMO && !objPRHeader.isDropShip){
            isError=true;errorMessage=label.giic_DropShipPromoforNonDOrderMessage;
        }
        
        /********************************* check if Promotion type is Shipping and Order is drp ship **********************************************/
        else if(objPromo.giic_PromotionType__c==giic_Constants.SHIPPINGPROMO  && objPRHeader.isDropShip){
            isError=true;errorMessage=label.giic_ShippingPromotionForDropshipOrderMessage;
        }
        
        /********************************* Line level promo already applied **********************************************/
        else if(!objPromo.giic_IsHeaderPromo__c && objPromo.giic_PromotionType__c !=giic_Constants.SHIPPINGPROMO &&  linePromoApplied){
            isError=true;errorMessage=label.giic_LineLevelPromoAppliedMessage;
        }
        
        /********************************* check if Promotion type is Shipping and no addtional charge on sales order **********************************************/
        else if(!isAPI && objPromo.giic_PromotionType__c==giic_Constants.SHIPPINGPROMO && !objPRHeader.isAdditonalCharge){
            isError=true;errorMessage=label.giic_PromotionNoShippingChargesOrder;
        }
        
        /********************************* check if Promotion type is AC Promo and payment method id sdifferent on sales order **********************************************/
        else if(objPromo.giic_PromotionType__c==giic_Constants.ACHPROMOTYPE && objPRHeader.paymentMethod !=objPromo.giic_PromotionType__c){
            isError=true;errorMessage=Label.giic_PromoACHErrorMessage;
        }
        return new map<string,object>{'ERROR'=>isError,'ERRORMESSAGE'=>errorMessage};
    }
     /*
    * Method name : getPromotionDetails
    * Description : get all the promation based on Customer Type, Roles, Promo code /optional , Auto 
    * Return Type :  list of promotion 
    * Parameter : set if customer type, roles, promo code
    */
     public static map<string,object> getPromotionDetails(map<string,object> mapRequest){
        map<string,object> mapResponse=new map<string,object>();
        set<string> setCustomerType=mapRequest.containsKey('CUSTTYPE') ?(set<String>) mapRequest.get('CUSTTYPE') : new set<string>();
        set<string> setAllowRoles=mapRequest.containsKey('ROLES') ?(set<String>) mapRequest.get('ROLES') : new set<string>();
        boolean isPromoTypeAuto=mapRequest.containsKey('PROMOTYPEAUTO') && mapRequest.get('PROMOTYPEAUTO') != null ?(boolean) mapRequest.get('PROMOTYPEAUTO') : false;
        boolean isCustomPromo=mapRequest.containsKey('ISCUSTOMPROMO') && mapRequest.get('ISCUSTOMPROMO') != null ?(boolean) mapRequest.get('ISCUSTOMPROMO') : false;
        boolean isUpdateSO=mapRequest.containsKey('ISUPDATESO') && mapRequest.get('ISUPDATESO') != null ?(boolean) mapRequest.get('ISUPDATESO') : false;
        boolean isDropShip=mapRequest.containsKey('ISDROPSHIP') && mapRequest.get('ISDROPSHIP') != null ?(boolean) mapRequest.get('ISDROPSHIP') : false;
        set<string> setPromoCode=mapRequest.containsKey('PROMOCODE') ?(set<String>) mapRequest.get('PROMOCODE') : new set<string>();
        set<string> setAllPromoIds=mapRequest.containsKey('ALLPROMOAPPLIED') ? (set<string>)mapRequest.get('ALLPROMOAPPLIED') : new set<string>();
        list<string> lstCustomerType=new list<string>();
        lstCustomerType.addall(setCustomerType);
        list<string> lstAllowRoles=new list<string>();
        lstAllowRoles.addall(setAllowRoles);
        string sAllowRoles='(\''+string.join(lstAllowRoles,'\',\'')+'\')';
        string sCustomerType='(\''+string.join(lstCustomerType,'\',\'')+'\')';
        //system.debug(sAllowRoles+sCustomerType+setPromoCode);
        String todayDate = String.valueOf(system.today()); 
        //system.debug('***isPromoTypeAuto***'+isPromoTypeAuto + 'isCustomPromo:' + isCustomPromo);
        map<Id,gii__Promotion__c> mapPromotion =new  map<Id,gii__Promotion__c>();
        String queryString =  'select id,giic_IsHeaderPromo__c,giic_PromoFrequency__c,giic_IsCustomerBased__c,giic_IsAutoPromo__c, giic_PromotionCode__c,Name,giic_CustomerType__c,giic_MaxUsedLimit__c,giic_MaxLimit__c,giic_PromotionType__c,giic_MinimumAmount__c,(select id,gii__Tier1DiscountPercent__c,gii__DiscountType__c,gii__MinimumAmount__c from gii__Promotion_Details__r)'+
                         'from gii__Promotion__c where ';
        if(setAllPromoIds.size()== 0){
            string promoCondition='';
            if(isUpdateSO)
                promoCondition = setPromoCode.size() > 0 ? ' ( giic_PromotionCode__c in :setPromoCode or giic_IsAutoPromo__c=true)  ' : ' giic_IsAutoPromo__c=true ' ;
            else
                promoCondition = ((!isCustomPromo)?  (setPromoCode.size() > 0 ? ' giic_PromotionCode__c in :setPromoCode and giic_IsAutoPromo__c=:isPromoTypeAuto  ' : ' giic_IsAutoPromo__c=:isPromoTypeAuto  ' ) : '( giic_PromotionCode__c in :setPromoCode or giic_IsAutoPromo__c= true)  ');
            
            if(isDropShip){
                queryString += '((' +promoCondition + ') or giic_PromotionType__c =\'' + giic_Constants.DROPSHIPPROMO +'\') and  ' ;
            }
            else{
                queryString +=promoCondition + ' and ';
            }
            system.debug('***'+queryString);
            queryString+= ' giic_AllowUsrRole__c includes  '+sAllowRoles+ ' and  giic_CustomerType__c includes '+sCustomerType + ' and gii__EffectiveFromDate__c <='+todayDate+' and gii__EffectiveToDate__c >= '+todayDate;
        }
        else{
            queryString += ' id in :setAllPromoIds ';
        }
        queryString += ' order by giic_IsHeaderPromo__c';                               
        //system.debug('queryString:' + queryString);
        mapPromotion = new map<Id,gii__Promotion__c>((list<gii__Promotion__c>)database.query(queryString));
        //system.debug('mapPromotion:' + mapPromotion ); 
        set<string> setSpecialPromo=new set<string>();
        setSpecialPromo.addall(label.giic_SpecialPromoCodes.split(','));
        map<Id,gii__PromotionLine__c> mapPromotionLines=new map<Id,gii__PromotionLine__c>([select id,(select id,giic_IsRequired__c,gii__Product__c,gii__Product__r.gii__ProductReference__r.SKU__c from gii__PromotionLineProducts__r) from gii__PromotionLine__c where gii__Promotion__c=:mapPromotion.keyset() and gii__Promotion__r.giic_PromotionType__c in :setSpecialPromo ]);
        
        mapResponse.put('PROMOTION',mapPromotion);
        if(mapPromotionLines.size() > 0)
            mapResponse.put('PROMOTIONLINES',mapPromotionLines);
        return mapResponse;
    }
    /*
    * Method name : applyShippingChargeForSO
    * Description : apply shipping charges on sales order 
    * Return Type :  map<string,gii.ShippingChargeCalculation.shippingCharge>
    * Parameter : set of sales order ids
    */
    public static map<string,gii.ShippingChargeCalculation.shippingCharge> applyShippingChargeForSO(set<Id> setSOIds){
        //Shipping Charge for an existing Sales Order
        return  gii.ShippingChargeCalculation.applyShippingChargeForSO(setSOIds); 
    }
     /*
    * Method name : applyDiscountForSO
    * Description : apply discount on sales order 
    * Return Type :  list<gii__salesorderline__c>
    * Parameter : set of sales order ids
    */
    public static list<gii__salesorderline__c> applyDiscountForSO(set<Id> setSOIds){
        list<gii__SalesOrderLine__c> lstSOLToUpdate=new list<gii__SalesOrderLine__c>();
        for(gii__SalesOrderLine__c objSOL  : gii.OrderPromotion.ApplyDiscountForSO(setSOIds)){
            if(objSOL.gii__CancelledQuantity__c == 0)
                lstSOLToUpdate.add(objSOL);
        }
        //system.debug('****lstSOLToUpdate***'+lstSOLToUpdate);
        // call OrderPromotion.ApplyDiscountForSO method 
        return  lstSOLToUpdate;
    }
}