/*****************************************************************************************************************
* Purpose    : The purpose of this class to handle New Sales Order Creation from Account.  
*              
* *******************************************************
* Author                        Date            Remarks
* Devanshu Sharma              01/10/2018     
*******************************************************************************************************************/
public class giic_CreateSOCtrl 
{    
    /***********************************************************
    * Method name : getAccountData
    * Description : to get details from the account
    * Return Type : Map<String,Object>
    * Parameter : Id of Account
    ***********************************************************/
    @AuraEnabled
    public static Map<String,Object> getAccountData(String accid)
    {
        Map<String,Object> mapresult = new Map<String,Object>();
        mapresult.put('IsSuccess',false);
        if(accid != '' && accid != null)
        {       
            List<gii__AccountAdd__c> lstAcc = new List<gii__AccountAdd__c>();
            lstAcc= [select id,gii__Account__c,gii__Account__r.Name,gii__Account__r.Bill_To_Customer_ID__c,
                     gii__Account__r.Business_Email__c,
                     gii__Account__r.ShipToCustomerNo__c,gii__PriceBookName__c,
                     gii__Account__r.giic_Carrier__c,gii__Account__r.giic_CustomerPriority__c,
                     gii__Account__r.blocked__c,gii__Account__r.Check_if_you_are_Tax_Exempt__c,
                     gii__Account__r.Resale_Sales_Tax_Cert_Expiration_Date__c,gii__Account__r.Payment_Method__c,gii__Account__r.Validated__c,
                     gii__Account__r.ShippingStreet,gii__Account__r.ShippingCity,gii__Account__r.ShippingState,gii__Account__r.ShippingPostalCode,
                     gii__Account__r.ShippingCountry from gii__AccountAdd__c where gii__Account__c =:accid limit 1];            
            if(lstAcc.size()>0)
            {
                if(lstAcc[0].gii__Account__r.blocked__c != true)
                {
                    mapresult.put('IsSuccess',true);
                    mapresult.put('lstAcc',lstAcc);
                }
                else
                {
                    mapresult.put('Error',giic_Constants.Acc_Blocked);
                }
                
            }
        }
        return mapresult;
    }
    /***********************************************************
    * Method name : getSOData
    * Description : to get details from the SO
    * Return Type : Map<String,Object>
    * Parameter : Id of Sales Order
    ***********************************************************/
    @AuraEnabled
    public static Map<String,Object> getSOData(String soid)
    {
        Map<String,Object> mapresult = new Map<String,Object>();
        mapresult.put('IsSuccess',false);
        if(soid != '' && soid != null)
        {            
            List<gii__SalesOrder__c> lstSO = new List<gii__SalesOrder__c>();
            lstSO = [select id,name,gii__Released__c,gii__PriceBookName__c,gii__Account__r.Name,gii__Account__r.Bill_To_Customer_ID__c,
                     gii__Account__r.ShipToCustomerNo__c,gii__Account__r.blocked__c,giic_SOStatus__c,
                     gii__Account__r.ShippingStreet,gii__Account__r.ShippingCity,gii__Account__r.ShippingState,gii__Account__r.ShippingPostalCode,
                     gii__Account__r.ShippingCountry from gii__SalesOrder__c where id=:soid   limit 1];           
            if(lstSO.size()>0)
            {               
                
                if(lstSO[0].gii__Account__r.blocked__c == true)
                {
                    mapresult.put('Error',giic_Constants.SO_Acc_Blocked);
                }           
                else if(lstSO[0].giic_SOStatus__c == System.Label.giic_SalesOrderCancelledStatus)
                {
                    mapresult.put('Error',giic_Constants.SO_Cancelled);
                    return mapresult;
                }
                else if(lstSO[0].gii__Released__c)
                {
                    mapresult.put('Error',giic_Constants.SO_Released);
                    return mapresult;
                }
                else if(!lstSO[0].gii__Released__c)
                {
                    list<SalesOrderLineWrapper> lstSOLW = new list<SalesOrderLineWrapper>();
                    List<gii__SalesOrderLine__c> lstSOL = new List<gii__SalesOrderLine__c>();
                    
                    lstSOL = [select id,gii__SalesOrder__c,gii__Product__c,gii__OrderQuantity__c,gii__UnitPrice__c,giic_LineStatus__c ,
                              gii__Product__r.giic_ProductSKU__c,gii__Product__r.Name,
                              gii__Product__r.gii__UnitWeight__c from  gii__SalesOrderLine__c where gii__SalesOrder__c = : lstSO[0].id and giic_LineStatus__c !=: giic_Constants.SOLI_CANCEL];
                    
                    mapresult.put('IsSuccess',true);
                    mapresult.put('lstSO',lstSO);
                    if(lstSOL.size()>0)
                    {                      
                        for(gii__SalesOrderLine__c obj : lstSOL)
                        {
                            SalesOrderLineWrapper objSOLW = new SalesOrderLineWrapper();
                            objSOLW.solineId = obj.id;
                            objSOLW.prodId = obj.gii__Product__r.Id;
                            objSOLW.prodCode = obj.gii__Product__r.giic_ProductSKU__c;
                            objSOLW.prodDesc = obj.gii__Product__r.Name;
                            objSOLW.prodQty = obj.gii__OrderQuantity__c;
                            objSOLW.prodUnitPrice = obj.gii__UnitPrice__c;
                            objSOLW.prodLinePrice = obj.gii__OrderQuantity__c *  obj.gii__UnitPrice__c;
                            if(obj.gii__Product__r.gii__UnitWeight__c != null)
                            {
                                objSOLW.prodWeight = obj.gii__Product__r.gii__UnitWeight__c;
                            }
                            else
                            {
                                objSOLW.prodWeight = 0.00;
                            }   
                            if(objSOLW.prodQty !=null && objSOLW.prodWeight !=null)
                            {
                                objSOLW.prodLineWeight = (objSOLW.prodQty * objSOLW.prodWeight);
                            }
                            lstSOLW.add(objSOLW);
                            mapresult.put('lstSOLW',lstSOLW);                            
                        }                        
                    }
                    else
                    {
                        mapresult.put('lstSOLW',lstSOLW);
                    }                        
                }                    
            }                
        }        
        return mapresult;
    }
    
    /***********************************************************
    * Method name : getProductData
    * Description : to get product details based on serach string
    * Return Type : Map<String,Object>
    * Parameter : Product SKU for search, PriceBook Name from Account, Product Quantity
    ***********************************************************/
    @AuraEnabled
    public static Map<String,Object> getProductData(String searchString, string pbname,Decimal prdqty)
    {
        Map<String,Object> mapresult = new Map<String,Object>();
        mapresult.put('IsSuccess',false);
        
        try
        {
            if(searchString !='' && searchString !=null && searchString.trim() != '')
            {
                list<gii__Product2Add__c> lstProdRefs = new list<gii__Product2Add__c>();
                map<id,gii__Product2Add__c> mapProdId = new map<id,gii__Product2Add__c>();     
                
                lstProdRefs =  [select id,gii__UnitWeight__c,Name,giic_ProductSKU__c, 
                                       gii__ProductReference__c from gii__Product2Add__c where giic_ProductSKU__c = : searchString.trim()
                                       and gii__IsActive__c = true];
                
                if(lstProdRefs.size()>0)
                {
                    mapProdId =new map<id,gii__Product2Add__c>(lstProdRefs);
                    /*for(gii__Product2Add__c obj :  lstProdRefs)
                    {
                        if(mapProdId.get(obj.id) == null)
                        {
                            mapProdId.put(obj.id,null);
                        }
                        mapProdId.put(obj.id,obj);
                    } */                   
                    if(pbname != '' && pbname != null)
                    {
                        list<gii__PriceBookEntry__c> lstPriceBookEntry = new list<gii__PriceBookEntry__c>();
                        lstPriceBookEntry = [select id,gii__Product__c,gii__PriceBook__c,gii__PriceBook__r.Name,gii__UnitPrice__c
                                             from gii__PriceBookEntry__c where gii__PriceBook__r.Name =: pbname and  gii__Product__c IN : mapProdId.keyset()];
                        
                        if(lstPriceBookEntry.size()>0)
                        {
                            list<SalesOrderLineWrapper> lstSOLW = new list<SalesOrderLineWrapper>();
                            for(gii__PriceBookEntry__c obj : lstPriceBookEntry)                            
                            {
                                if(mapProdId.get(obj.gii__Product__c) != null)
                                {
                                    SalesOrderLineWrapper objSOLW = new SalesOrderLineWrapper();
                                    objSOLW.prodId = mapProdId.get(obj.gii__Product__c).id;
                                    objSOLW.prodCode = mapProdId.get(obj.gii__Product__c).giic_ProductSKU__c;
                                    objSOLW.prodDesc = mapProdId.get(obj.gii__Product__c).Name;
                                    if(prdqty != null)
                                    {
                                        objSOLW.prodQty = prdqty;
                                    }
                                    else
                                    {
                                        objSOLW.prodQty = 1;
                                    }                                    
                                    objSOLW.prodUnitPrice = obj.gii__UnitPrice__c;
                                    objSOLW.prodLinePrice = (objSOLW.prodQty *  objSOLW.prodUnitPrice);
                                    if(mapProdId.get(obj.gii__Product__c).gii__UnitWeight__c != null)
                                    {
                                        objSOLW.prodWeight = mapProdId.get(obj.gii__Product__c).gii__UnitWeight__c;
                                    }
                                    else
                                    {
                                        objSOLW.prodWeight = 0.00;
                                    } 
                                    if(objSOLW.prodQty !=null && objSOLW.prodWeight !=null)
                                    {
                                        objSOLW.prodLineWeight = (objSOLW.prodQty * objSOLW.prodWeight);
                                    }
                                    
                                    lstSOLW.add(objSOLW);
                                }
                                
                            }                            
                            if(lstSOLW.size()>0)
                            {
                                mapresult.put('IsSuccess',true);
                                mapresult.put('lstSOLW',lstSOLW);
                            }
                        }
                    }
                }                
            }
        }
        catch(Exception ex)
        {         
            mapresult.put('IsSuccess',false); 
            mapresult.put('Error',ex.getMessage()+ 'at:' +ex.getLineNumber());                      
        }        
        return mapresult;        
    }
    
    /***********************************************************
    * Method name : saveSalesOrder
    * Description : to save SO and SOL 
    * Return Type : Map<String,Object>
    * Parameter : allinputParam as a Map<String,Object>
    ***********************************************************/
    @AuraEnabled
    public static Map<String,Object> saveSalesOrder(string data)
    {      
        
        Map<String,Object> allinputParam = new Map<String,Object>();
        allinputParam = (Map<String, Object>)JSON.deserializeUntyped(data);
        
        Savepoint sp = Database.setSavepoint();
        Map<String,Object> mapresult = new Map<String,Object>();
        mapresult.put('IsSuccess',false);        
        List<gii__AccountAdd__c> lstAcc = new List<gii__AccountAdd__c>();    
        List<SalesOrderLineWrapper> lstSOLW = new List<SalesOrderLineWrapper>();
        if(allinputParam.containsKey('lstAcc')){            
            lstAcc = (List<gii__AccountAdd__c>)json.deserialize((String) (Object) allinputParam.get('lstAcc'), List<gii__AccountAdd__c>.class);            
        }
        
        if(allinputParam.containsKey('lstSalesOrderLines')){         
            lstSOLW = (list<SalesOrderLineWrapper>)System.JSON.deserialize((String) (Object) allinputParam.get('lstSalesOrderLines'), list<SalesOrderLineWrapper>.class);                       
        }
        if(!lstAcc.isEmpty() && lstAcc[0].gii__Account__r.Blocked__c != true)
        {
            Id soRecordTypeId = Schema.SObjectType.gii__SalesOrder__c.getRecordTypeInfosByDeveloperName().get(System.Label.giic_RTUSA).getRecordTypeId();
            gii__SalesOrder__c objSalesOrder = new gii__SalesOrder__c();
            objSalesOrder.giic_CustEmail__c = lstAcc[0].gii__Account__r.Business_Email__c;
            objSalesOrder.gii__Account__c =  lstAcc[0].gii__Account__c;
            objSalesOrder.gii__Carrier__c=  lstAcc[0].gii__Account__r.giic_Carrier__c;
            objSalesOrder.giic_CustomerPriority__c=lstAcc[0].gii__Account__r.giic_CustomerPriority__c;
            if(lstAcc[0].gii__Account__r.Payment_Method__c != null  && !lstAcc[0].gii__Account__r.Payment_Method__c.contains('Credit'))
            {
                objSalesOrder.giic_CCAuthorized__c = true;
            }
            else
            {
                objSalesOrder.giic_CCAuthorized__c = false;
            }            
            objSalesOrder.giic_AddressValidated__c = true;            
            objSalesOrder.RecordTypeId=soRecordTypeId;          
            objSalesOrder.giic_TaxCal__c = true;                     
            objSalesOrder.gii__ShippingbyWeight__c=true;
            if(lstAcc[0].gii__Account__r.Check_if_you_are_Tax_Exempt__c != true || 
               (lstAcc[0].gii__Account__r.Check_if_you_are_Tax_Exempt__c == true && 
                lstAcc[0].gii__Account__r.Resale_Sales_Tax_Cert_Expiration_Date__c < System.Today()))
            {
               objSalesOrder.giic_TaxExempt__c = false;
            }
            else
            {
                objSalesOrder.giic_TaxExempt__c = true;
            }
                        
            try
            { 
                insert objSalesOrder;                  
                if(lstSOLW.size()>0) 
                {
                    List<gii__SalesOrderLine__c> lstSOL = new List<gii__SalesOrderLine__c>();
                    for(SalesOrderLineWrapper obj : lstSOLW)
                    {
                        gii__SalesOrderLine__c objSOL = new gii__SalesOrderLine__c();
                        objSOL.gii__SalesOrder__c = objSalesOrder.id;                                                
                        objSOL.gii__Product__c = obj.prodId;
                        objSOL.gii__OrderQuantity__c = obj.prodQty;
                        objSOL.gii__UnitPrice__c =  obj.prodUnitPrice;
                        lstSOL.add(objSOL);
                    }
                    if(lstSOL.size()>0)
                    {
                        insert lstSOL;
                        map<string,object> mapRequest = new map<string,object>();
                        mapRequest.put('soId',objSalesOrder.id);
                        mapRequest.put('ISNEWORDER',true);
                        mapRequest.put('ISNEWORDER',true);
                        map<string,object> mapResponse=new map<string,object>();
                        mapResponse = giic_PromotionController.applyAutoPromotion(mapRequest);                        
                        mapresult.put('objSalesOrder',objSalesOrder);
                        mapresult.put('lstSOL',lstSOL);
                        mapresult.put('promotionresponse',mapResponse);
                        mapresult.put('IsSuccess',true);                       
                    }                 
                }                
            }
            catch(Exception ex)
            {
                mapresult.put('IsSuccess',false);                
                Database.rollback(sp);
                mapresult.put('Error',ex.getMessage()+ 'at:' +ex.getLineNumber());                  
            }
        }
        else
        {
            mapresult.put('Error',giic_Constants.Acc_Blocked);
        }        
        return mapresult; 
    }
    
    /***********************************************************
    * Method name : updateSalesOrder
    * Description : to save SO and SOL 
    * Return Type : Map<String,Object>
    * Parameter : allinputParam as a Map<String,Object>
    ***********************************************************/
    @AuraEnabled
    public static Map<String,Object> updateSalesOrder(string data)
    {
        Map<String,Object> allinputParam = new Map<String,Object>();
        allinputParam = (Map<String, Object>)JSON.deserializeUntyped(data);
        
        Savepoint sp = Database.setSavepoint();
        Map<String,Object> mapresult = new Map<String,Object>();
        mapresult.put('IsSuccess',false);   
        String SOID ;
        List<SalesOrderLineWrapper> lstSOLW = new List<SalesOrderLineWrapper>();
        if(allinputParam.containsKey('lstSalesOrderLines')){         
            lstSOLW = (list<SalesOrderLineWrapper>)System.JSON.deserialize((String) (Object) allinputParam.get('lstSalesOrderLines'), list<SalesOrderLineWrapper>.class);                       
        }
        if(allinputParam.containsKey('soId')){         
            soId = (String) allinputParam.get('soId');                       
        }            
        if(lstSOLW.size()>0)
        {
            List<gii__SalesOrderLine__c> lstSOL = new List<gii__SalesOrderLine__c>();
            for(SalesOrderLineWrapper obj : lstSOLW)
            {
                gii__SalesOrderLine__c objSOL = new gii__SalesOrderLine__c();
                if(obj.solineId != null && obj.solineId != '')
                {
                    objSOL.id = obj.solineId;
                }  
                else
                {
                    objSOL.gii__SalesOrder__c = soId;  
                }                                                             
                objSOL.gii__Product__c = obj.prodId;
                objSOL.gii__OrderQuantity__c = obj.prodQty;
                objSOL.gii__UnitPrice__c =  obj.prodUnitPrice;
                lstSOL.add(objSOL);
            }            
            if(lstSOL.size()>0)
            {
                try
                {
                    upsert lstSOL;                  
                    map<string,object> mapRequest = new map<string,object>();
                    mapRequest.put('SOIDS',new set<string>{soId});
                    mapRequest.put('ISNEWORDER',false);
                    map<string,object> mapResponse=new map<string,object>();
                    mapResponse = giic_PromotionHelper.applyPromotion(mapRequest);
                    mapresult.put('promotionresponse',mapResponse);
                    mapresult.put('IsSuccess',true); 
                }
                catch(Exception ex)
                {
                    mapresult.put('IsSuccess',false);      
                    Database.rollback(sp);                    
                    mapresult.put('Error',ex.getMessage() + 'at:' + ex.getLineNumber());                         
                }                
            }
        }
        return mapresult; 
    }
    
    /***********************************************************
    * Method name : CalculateTax
    * Description : to calucalte tax by AValara for SOL
    * Return Type : Map<String,Object>
    * Parameter :  Id of Sales Order
    ***********************************************************/
    @AuraEnabled
    public static Map<String,Object>CalculateTax(String SOId)
    {
        Map<String,Object> maptaxresult = new Map<String,Object>();
        maptaxresult.put('IsSuccess',false);   
        List<gii__SalesOrder__c>  lstSalesOrder = new List<gii__SalesOrder__c>();
        lstSalesOrder = [select id,gii__Warehouse__r.giic_WarehouseCode__c,
                              gii__Account__r.Check_if_you_are_Tax_Exempt__c,
                              gii__Account__r.Resale_Sales_Tax_Cert_Expiration_Date__c,giic_TaxExempt__c,
                              giic_TaxCal__c
                              from gii__SalesOrder__c where id =: soId ];       
        if(lstSalesOrder.size()>0)
        {
            //Check for Account Tax Exemption before Calculating Tax
            if(!lstSalesOrder[0].giic_TaxExempt__c)
            {            
                try
                {
                    //Call utility method for tax calculation
                    maptaxresult = giic_AvalaraServiceProviderClass.CalculateTaxforSO(lstSalesOrder[0].id,null,lstSalesOrder[0].gii__Warehouse__r.giic_WarehouseCode__c,giic_Constants.CREATE_T,True);
                    if(maptaxresult.containsKey('ISSUCCESS') && maptaxresult.get('ISSUCCESS') == false)
                    {
                        if(maptaxresult.containsKey('RESPONSE'))
                        {                       
                            List<Integration_Error_Log__c> lstErrors = new List<Integration_Error_Log__c>();
                            lstErrors = (List<Integration_Error_Log__c>)maptaxresult.get('RESPONSE');   
                            if(!lstErrors.isEmpty()){
                                insert lstErrors;                           
                            }
                        }
                        if(maptaxresult.containsKey('ERROR'))
                        {
                            giic_AvalaraServiceProviderClass.CollectErrors(soId, (String)maptaxresult.get('ERROR'));  
                        }
                    }
                }
                catch(Exception ex)
                {
                    maptaxresult.put('IsSuccess',false);            
                    maptaxresult.put('Error',ex.getMessage()+ 'at:' +ex.getLineNumber());                             
                }  
            }            
        }       
        return maptaxresult;
    }    
 
    /*Wrapper class to handle Sales Order Line*/
    public Class SalesOrderLineWrapper
    {        
        @auraEnabled  public String solineId;
        @auraEnabled  public String prodId;
        @auraEnabled  public String prodCode;
        @auraEnabled  public String prodDesc;
        @auraEnabled  public Decimal prodQty;
        @auraEnabled  public Decimal prodUnitPrice;
        @auraEnabled  public Decimal prodLinePrice;
        @auraEnabled  public Decimal prodWeight;  
        @auraEnabled  public Decimal prodLineWeight;
        
        public SalesOrderLineWrapper()
        {
            solineId = '';
            prodId = '';
            prodCode = '';
            prodDesc = '';
            prodQty = 0.00;
            prodUnitPrice = 0.00;
            prodLinePrice = 0.00;
            prodWeight = 0.00;     
            prodLineWeight = 0.00;
        
        }  
    }
}