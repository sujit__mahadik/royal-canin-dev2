global class AccountSalesSummarySchedulable  implements Schedulable {
	
	
	global void execute(SchedulableContext SC) {
		
		// initiate a batch to geocode addresses
		// scope parameter is 1 in order to ensure that the batch attempts only 1 callout
		Id batchProcessId = Database.executebatch(new AccountSalesSummaryBatch(), 20);
		
		Id batchProcessId2 = Database.executebatch(new AccountRankingBatch(), 1);
	}
	
		

}