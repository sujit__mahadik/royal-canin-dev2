/************************************************************************************
Version : 1.0
Created Date : 16 Oct 2018
Function : to create account program from api call
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/

@RestResource(urlMapping='/giic_CreateAccountPrograms/*')
global with sharing class giic_CreateAccountPrograms{  

   @HttpPost  
   /*
    * Method name : InsertAccountPrograms
    * Description : To Insert Account Programs from Librix
    * Return Type : Map
    * Parameter : None
    */
   global static Map<String,List<AccountProgramWrapper>> InsertAccountPrograms() 
   {   
     
     Map<String,List<AccountProgramWrapper>> mapResponse = new Map<String,List<AccountProgramWrapper>>();
     Map<String,String> map_CNO_AccRef = new Map<String,String>(); //map to store id and name of matching account refrences based on librix data     
     Map<string,gii__Promotion__C> map_ELProgram_Promo= new  Map<string,gii__promotion__c>(); //map to promocode and promotions for matching promotions based on librix data
     Set<String> setCNO = new  Set<String>();// set of customer no (Account Bill_To_Customer_ID__c) recieved from librix
     Set<String> setPromo = new  Set<String>();//set of promo code recieved from librix 
     Set<String> setNewPromo = new  Set<String>(); //set of id of new promo code inserted
     List<Integration_Error_Log__c> lstErrorLogs = new List<Integration_Error_Log__c>();
     List<gii__AccountProgram__c> lstAPtoInsert = new List<gii__AccountProgram__c>();
     RestRequest request = RestContext.request;       
     RestResponse response = RestContext.response;       
     String jSONRequestBody=request.requestBody.toString().trim();      
     List<AccountProgramWrapper> lstAPWrapper = (List<AccountProgramWrapper>)JSON.deserialize(jSONRequestBody,List<AccountProgramWrapper>.class);
     List<AccountProgramWrapper> lstSucceses = new List<AccountProgramWrapper>();
     List<AccountProgramWrapper> lstErrors = new List<AccountProgramWrapper>();
     List<gii__Promotion__c> lstPromotoInsert = new List<gii__Promotion__c>();
     
     try
     {
         if(lstAPWrapper.size()>0)
         {
             for(AccountProgramWrapper obj : lstAPWrapper )
             {
                 if(obj.CustomerNo != '' && obj.CustomerNo !=null)
                 {
                    setCNO.add(obj.CustomerNo);
                 }
                 if(obj.ELearnProgram != '' && obj.ELearnProgram !=null)
                 {
                    setPromo.add(obj.ELearnProgram); 
                 }                         
             }   
             
             if(setCNO.size()>0)
             {   
                 for(gii__AccountAdd__c obj : [select id,gii__Account__r.Bill_To_Customer_ID__c from gii__AccountAdd__c  where gii__Account__r.Bill_To_Customer_ID__c in :  setCNO])
                 {
                     if(map_CNO_AccRef.get(obj.gii__Account__r.Bill_To_Customer_ID__c ) == null)
                     {
                         map_CNO_AccRef.put(obj.gii__Account__r.Bill_To_Customer_ID__c ,null);
                     }
                     map_CNO_AccRef.put(obj.gii__Account__r.Bill_To_Customer_ID__c ,obj.id);
                 }
             }
             if(setPromo.size()>0)
             {                 
                 for(gii__Promotion__c objPromo : [select id,giic_PromotionCode__c from gii__Promotion__c where giic_PromotionCode__c in : setPromo])
                 {
                     if(map_ELProgram_Promo.get(objPromo.giic_PromotionCode__c ) == null)
                     {
                         map_ELProgram_Promo.put(objPromo.giic_PromotionCode__c,null);
                     }
                     map_ELProgram_Promo.put(objPromo.giic_PromotionCode__c,objPromo);
                 }
                 for(String str : setPromo)
                 {
                     if(!map_ELProgram_Promo.containsKey(str))
                     {
                         lstPromotoInsert.add(new gii__Promotion__c(Name = str,giic_PromotionCode__c = str));
                     }
                 }                        
             }
             
             for(AccountProgramWrapper obj : lstAPWrapper )
             {             
                 if(map_CNO_AccRef.get(obj.CustomerNo) != null)
                 {
                     gii__AccountProgram__c objAccPrg = new gii__AccountProgram__c(); 
                     objAccPrg.gii__AccountReference__c = map_CNO_AccRef.get(obj.CustomerNo);
                     if(map_ELProgram_Promo.size()>0 &&map_ELProgram_Promo.get(obj.ELearnProgram) != null )
                     {
                         objAccPrg.giic_Promotion__c = map_ELProgram_Promo.get(obj.ELearnProgram).id;
                     }                     
                     objAccPrg.giic_QualifiedDate__c = Date.Parse(obj.DateQualified);
                     objAccPrg.giic_PromoEndDate__c =  Date.Parse(obj.ExpirationDate);                     
                     lstAPtoInsert.add(objAccPrg );  
                     obj.NonMatchingValueField = giic_Constants.LIBRIX_NonMatchingValueField;
                     lstSucceses.add(obj);                   
                 }   
                 else if (map_CNO_AccRef.get(obj.CustomerNo) == null )          
                 {               
                     obj.NonMatchingValueField = giic_Constants.LIBRIX_NonMatchingField;
                     lstErrors.add(obj);                 
                 }                                     
             } 
             
             if(lstSucceses.size()>0)
             {
                 mapResponse.put('SUCCESS',lstSucceses);
             }
             if(lstErrors.size()>0)
             {
                 mapResponse.put('ERROR',lstErrors);
             }
             
             if(lstAPtoInsert.size()>0)
             {                       
                 Database.SaveResult[] srList = Database.insert(lstAPtoInsert, false);          
             } 
             
             if(lstPromotoInsert.size()>0)
             {
                Database.SaveResult[] srList = Database.insert(lstPromotoInsert, false);                 
                for (Database.SaveResult sr : srList) 
                {
                    if(sr.isSuccess())
                    {                        
                        Integration_Error_Log__c objErr= new Integration_Error_Log__c(
                        Name = giic_Constants.USER_STORY_LIBRIX,
                        giic_Promotion__c = sr.getId(),
                        giic_IsActive__c = true,
                        Error_Code__c   =  giic_Constants.LIBRIX_ERROR_CODE,
                        Error_Message__c = giic_Constants.LIBRIX_ERROR_MESSAGE
                        );                      
                        lstErrorLogs.add(objErr);                                               
                    }                   
                }
                
                if(lstErrorLogs.size()>0)
                {
                    insert lstErrorLogs;
                }
             }
         }  
     }
     catch(Exception ex)
     {
         mapResponse.put('EXCEPTION - '+ex.getMessage(),lstErrors);
     }
     
     return mapResponse;     
   }  
   
   public class AccountProgramWrapper
   {
       public string CustomerNo ;
       public string ELearnProgram;
       public string ExpirationDate;
       public string DateQualified;
       public string NonMatchingValueField;
   
   }   
   
}