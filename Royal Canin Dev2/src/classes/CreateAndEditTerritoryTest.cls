/*
 * Date:       		 - Developer, Company					       - Description
 * 2015-08-05		 - Stevie Yakkel, Acumen Solutions			   - Edited
 */

@isTest
private class CreateAndEditTerritoryTest {
	
	static testMethod void testWithoutSalesRepSelection() {
		Test.startTest();
        User adminUser = TestHelperClass.createAdminUser();
        User ManagerUser;
        System.runAs(adminUser){
        
            User salesRep = TestHelperClass.createSalesRepUserWithManager();
			UserRole salesRepRole = [Select id, ParentRoleId from UserRole where id = : salesRep.UserRoleId Limit 1];
			ManagerUser = [Select Id, Name from User where UserRoleId = : salesRepRole.ParentRoleId Limit 1];
			TestHelperClass.createFiscalYears();
			List<PostalCode__c> pcs = TestHelperClass.createPostalCodes();
			ApexPages.PageReference ref = new PageReference('/apex/CreateAndEditTerritory');
	    	Test.setCurrentPageReference(ref);			
        }
        ApexPages.StandardController sc = new ApexPages.StandardController(new Territory__c(Name='Test'));
		System.runAs(ManagerUser) {

			CreateAndEditTerritory caet = new CreateAndEditTerritory(sc);
			caet.Validate();
			caet.getAllSalesReps();
			caet.SaveTerritory();
			//caet.setSelectedSalesRep(salesRep.Id);
			//caet.SaveTerritory();
			System.assertEquals(ApexPages.getMessages().size(), 2, ApexPages.getMessages());
		}
		Test.stopTest();
	}
	
	static testMethod void testWithSalesRepAndZipCodeSelection() {
		Test.startTest();
        User adminUser = TestHelperClass.createAdminUser();
        User ManagerUser;
        User salesRep;
        List<PostalCode__c> pcs;
        System.runAs(adminUser){
			salesRep = TestHelperClass.createSalesRepUserWithManager();
			UserRole salesRepRole = [Select id, ParentRoleId from UserRole where id = : salesRep.UserRoleId Limit 1];
			ManagerUser = [Select Id, Name from User where UserRoleId = : salesRepRole.ParentRoleId Limit 1];
			salesRep.ManagerId = ManagerUser.id;
			update salesRep;
			TestHelperClass.createRegion(managerUser);
		//User adminUser = TestHelperClass.createAdminUser();
			TestHelperClass.createFiscalYears();
			
        }
        System.runAs(adminUser){
            pcs = TestHelperClass.createPostalCodes();
		}
		ApexPages.PageReference ref = new PageReference('/apex/CreateAndEditTerritory');
	    Test.setCurrentPageReference(ref);

		ApexPages.StandardController sc = new ApexPages.StandardController(new Territory__c());
		
		System.runAs(ManagerUser) {
			CreateAndEditTerritory caet = new CreateAndEditTerritory(sc);
			caet.SelectedTerritory.Name = 'Resi Test Terr';		
			caet.Validate();
			caet.getAllSalesReps();
			caet.selectedSalesRep = (salesRep.Id);
			caet.SelectedZips = pcs[0].Id + ';' + pcs[0].Name + ';' + pcs[0].BusinessID__c + ' ';
			caet.SelectedPillar = 'VET';
			caet.populateZipsAndRegManager();
			caet.getRegionalManager();
			caet.SaveTerritory();
			TerritoryAssignment__c terrAssign = [select id, PostalCodeID__c, OwnerId from TerritoryAssignment__c where TerritoryId__c = : caet.SelectedTerritory.Id];
			System.assertEquals(terrAssign.PostalCodeID__c, pcs[0].Id, 'Territory assignment created with postal code');
			System.assertEquals(terrAssign.OwnerId, caet.SelectedSalesRep, 'Territory assignment owner is sales rep');
		}
		Test.stopTest();
	}	
	
	static testMethod void testWithSalesRepAndZipCodeSelectionForExistingTerr() {
		Territory__c terr;
        User salesRep;
        User ManagerUser;
        User adminUser = TestHelperClass.createAdminUser();
        List<PostalCode__c> pcs;
        System.runAs(adminUser){
        	Test.startTest();
			salesRep = TestHelperClass.createSalesRepUserWithManager();
			UserRole salesRepRole = [Select id, ParentRoleId from UserRole where id = : salesRep.UserRoleId Limit 1];
			ManagerUser = [Select Id, Name from User where UserRoleId = : salesRepRole.ParentRoleId Limit 1];
			salesRep.ManagerId = ManagerUser.id;
			update salesRep;
		
			Region__c rg = TestHelperClass.createRegion(managerUser);
			
			TestHelperClass.createFiscalYears();
			
			terr = new Territory__c (Name='Resi Test Terr', OwnerId = salesRep.Id, Region__c = rg.id);
			insert terr;
			pcs = TestHelperClass.createPostalCodes();
		}
		ApexPages.PageReference ref = new PageReference('/apex/CreateAndEditTerritory');
	    Test.setCurrentPageReference(ref);

		ApexPages.StandardController sc = new ApexPages.StandardController(terr);
		
		System.runAs(ManagerUser) {
			CreateAndEditTerritory caet = new CreateAndEditTerritory(sc);
			caet.SelectedTerritory.Name = 'Resi Test Terr';		
			caet.Validate();
			caet.getAllSalesReps();
			caet.SelectedSalesRep = (salesRep.Id);
			
			caet.SelectedZips = pcs[0].Id + ';' + pcs[0].Name + ';' + pcs[0].BusinessID__c + ' ';
			caet.SelectedPillar = 'VET';
			caet.SaveTerritory();
			TerritoryAssignment__c terrAssign = [select id, PostalCodeID__c, OwnerId from TerritoryAssignment__c where TerritoryId__c = : caet.SelectedTerritory.Id];
			System.assertEquals(terrAssign.PostalCodeID__c, pcs[0].Id, 'Territory assignment created with postal code');
			System.assertEquals(terrAssign.OwnerId, caet.SelectedSalesRep, 'Territory assignment owner is sales rep');			
		}
		Test.stopTest();
	}
	static testMethod void testGetAllPillars(){
		ApexPages.PageReference ref = new PageReference('/apex/CreateAndEditTerritory');
	    Test.setCurrentPageReference(ref);
	    TestHelperClass.createFiscalYears();	
		ApexPages.StandardController sc = new ApexPages.StandardController(new Territory__c());
		CreateAndEditTerritory caet = new CreateAndEditTerritory(sc);
		Test.startTest();
		caet.getAllPillars();
		Test.stopTest();
	}

	static testMethod void testGetCurrentFiscalYear(){
		ApexPages.PageReference ref = new PageReference('/apex/CreateAndEditTerritory');
	    Test.setCurrentPageReference(ref);
	    TestHelperClass.createFiscalYears();
		ApexPages.StandardController sc = new ApexPages.StandardController(new Territory__c());
		CreateAndEditTerritory caet = new CreateAndEditTerritory(sc);
		Test.startTest();
		caet.getCurrentFiscalYear();
		Test.stopTest();
	}

	static testMethod void testGetRegionManagerAndArea(){
		ApexPages.PageReference ref = new PageReference('/apex/CreateAndEditTerritory');
	    Test.setCurrentPageReference(ref);
	    TestHelperClass.createFiscalYears();
		ApexPages.StandardController sc = new ApexPages.StandardController(new Territory__c());
		CreateAndEditTerritory caet = new CreateAndEditTerritory(sc);
		Test.startTest();
		caet.getRegionalManagerAndArea();
		Test.stopTest();
	}

	static testMethod void testGetallSalesReps() {
		Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
		User user = new User(Alias = 'syakkel', Email='syakkel@test.com', 
            EmailEncodingKey='UTF-8', LastName='Tester', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Chicago', UserName='syakkel@test.com', channel__c = 'Vet');
		User managerUser = new User(Alias = 'syakk', Email='syakk@test.com', 
            EmailEncodingKey='UTF-8', LastName='Test', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Chicago', UserName='syakk@test.com');
		insert user;
		insert managerUser;
		Region__c reg = new Region__c(User__c = managerUser.Id, Name = 'Test', Pillar__c = 'Vet');
		insert reg;
		user.ManagerId = managerUser.Id;
		update user;
		ApexPages.PageReference ref = new PageReference('/apex/CreateAndEditTerritory');
	    Test.setCurrentPageReference(ref);
	    TestHelperClass.createFiscalYears();
		ApexPages.StandardController sc = new ApexPages.StandardController(new Territory__c());
		CreateAndEditTerritory caet = new CreateAndEditTerritory(sc);
		caet.SelectedRegion = reg.Id;
		caet.SelectedPillar = reg.Pillar__c;
		caet.getRegionalManager();
		Test.startTest();
		List<SelectOption> options = caet.getallSalesReps();
		System.assertEquals('Tester', options.get(1).getLabel());
		Test.stopTest();
	}

	static testMethod void testGetSelectedArea() {
		ApexPages.PageReference ref = new PageReference('/apex/CreateAndEditTerritory');
	    Test.setCurrentPageReference(ref);
	    TestHelperClass.createFiscalYears();
		ApexPages.StandardController sc = new ApexPages.StandardController(new Territory__c());
		CreateAndEditTerritory caet = new CreateAndEditTerritory(sc);
		Test.startTest();
		caet.getSelectedArea();
		Test.stopTest();
	}

	static testMethod void testPreviousPage() {
		ApexPages.PageReference ref = new PageReference('/apex/CreateAndEditTerritory');
	    Test.setCurrentPageReference(ref);
	    TestHelperClass.createFiscalYears();
		ApexPages.StandardController sc = new ApexPages.StandardController(new Territory__c());
		CreateAndEditTerritory caet = new CreateAndEditTerritory(sc);
		Test.startTest();
		caet.previousPage();
		Test.stopTest();
	}

	static testMethod void testnextPage() {
		ApexPages.PageReference ref = new PageReference('/apex/CreateAndEditTerritory');
	    Test.setCurrentPageReference(ref);
	    TestHelperClass.createFiscalYears();
		ApexPages.StandardController sc = new ApexPages.StandardController(new Territory__c());
		CreateAndEditTerritory caet = new CreateAndEditTerritory(sc);
		Test.startTest();
		caet.nextPage();
		Test.stopTest();
	}

	static testMethod void testchangeData() {
		ApexPages.PageReference ref = new PageReference('/apex/CreateAndEditTerritory');
	    Test.setCurrentPageReference(ref);
	    TestHelperClass.createFiscalYears();
		ApexPages.StandardController sc = new ApexPages.StandardController(new Territory__c());
		CreateAndEditTerritory caet = new CreateAndEditTerritory(sc);
		Test.startTest();
		caet.changeData();
		Test.stopTest();
	}

	static testMethod void testSaveTerritory() {
		ApexPages.PageReference ref = new PageReference('/apex/CreateAndEditTerritory');
	    Test.setCurrentPageReference(ref);
	    TestHelperClass.createFiscalYears();
		ApexPages.StandardController sc = new ApexPages.StandardController(new Territory__c());
		CreateAndEditTerritory caet = new CreateAndEditTerritory(sc);
		Test.startTest();
		caet.SaveTerritory();
		Test.stopTest();
	}
}