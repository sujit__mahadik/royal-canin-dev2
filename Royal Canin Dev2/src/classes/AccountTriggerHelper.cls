/*
 * Date:                - Developer, Company                          - Description
 * 2015-04-28        - Mayank Srivastava, Acumen Solutions         - Created
 * 2015-06-17        - Joe Wrabel, Acumen Solutions                - Edited
 * 2015-08-05        - Stevie Yakkel, Acumen Solutions             - Edited
 * 2015-10-02        - Esteban Woodring, Acumen Solutions          - Edited priceBookCodeLink() to so that the PricebookList matches PricebookCode
 * 2015-10-13        - Maruthi(Siva) Gotety, Royal Canin           - Edited - Added the method isParentStatusUpdate() to let users know if the account is a parent
 * 2015-12-07        - Maruthi(Siva) Gotety, Royal Canin           - Commented - Commented the method isParentStatusUpdate() as we are using a different approach for this by building a scheduled job 
 * 2016-01-08        - Maruthi(Siva) Gotety, Royal Canin           - Edited - Added the method populateAddressChanges() to store the previous address after an address change. In case the new address gets rejected, the old address is replaced in the Account address fields
 * 2018-09-12        - Bethi Reddy, Royal Canin                    - Edited - Added the method getDistanceOfWarehouses() to calculate the distance between Account shipping address and group of warehouses whenever account shipping address is updated
 * 2018-10-02        - Bethi Reddy, Royal Canin                    - Edited - Added the method UpdateAccountReference() to copy Account record fields data to its respective Account Reference record fields when an Account is updated 
 * 2018-10-23        - Pradeep Kumar, Fujitsu                    - Edited - update the method getDistanceOfWarehouses() to use the list<account> instead of set<Id> of account
 * 2018-11-12        -Bethi Reddy, Royal canin                    - Edited - Included payment term field mapping in UpdateAccountReference() method
 */

public with sharing class AccountTriggerHelper {

    static Set<String> accountFields = new Set<String>();

    public static void setUpTerritoryData(List<Account> newAccts, Map<Id,Account> oldIdToAccts){
        /*List<Account> acctsToProcess = new List<Account>();
        if(oldIdToAccts != null){
            for(Account acc: newAccts){
                if((oldIdToAccts.get(acc.id)).ownerid != acc.ownerid 
                    || (oldIdToAccts.get(acc.id)).ShippingPostalCode != acc.ShippingPostalCode){
                    acctsToProcess.add(acc);
                }
            }
        }
        else{
            acctsToProcess.addAll(newAccts);
        }*/
        AccountTriggerUtilities.setPostalCodeLookup(newAccts);
        for(Account acn : newAccts) {
            System.debug('acn=======>>>>'+acn.Customer_Registration__c);
        }
        ChangeAccountOwnership.changeAccountOwnershipWithListOfAccounts(newAccts);
    }
    
    // This code looks for address changes on accounts made by Integration User and puts back the old address
    
 /*  public static void checkIntegrationUserAddressEdits(List<Account> accounts, Map<Id,Account> oldAccountMap) {
        User iuser = [select Id from User where alias = 'iuser' LIMIT 1];

        if(iuser.Id == UserInfo.getUserId()) {
            Set<String> addressFields = new Set<String> {'BillingCity' , 'BillingCountry', 'BillingPostalCode', 'BillingState', 'BillingStreet',
                                                         'ShippingCity' , 'ShippingCountry', 'ShippingPostalCode', 'ShippingState', 'ShippingStreet'};
            for(Account account : accounts) {
                for(String addressField : addressFields) {
                    // if(account.get(addressField) != oldAccountMap.get(account.Id).get(addressField)) {
                     //   account.addError('Integration User cannot update address fields on Account.');
                   // }
                     account.put(addressField,(oldAccountMap.get(account.Id).get(addressField)));   
                }
            }
        }
    } */
    
    
     

    public static void checkForAddressChanges(List<Account> accounts, Map<Id,Account> oldAccountMap){
        Set<Id> accountIds = new Set<Id>();
        for(Account account : accounts){
            if(account.BillingStreet != oldAccountMap.get(account.Id).BillingStreet || account.BillingCity != oldAccountMap.get(account.Id).BillingCity || account.BillingState != oldAccountMap.get(account.Id).BillingState || account.BillingPostalCode != oldAccountMap.get(account.Id).BillingPostalCode || account.ShippingStreet != oldAccountMap.get(account.Id).ShippingStreet || account.ShippingCity != oldAccountMap.get(account.Id).ShippingCity || account.ShippingState != oldAccountMap.get(account.Id).ShippingState || account.ShippingPostalCode != oldAccountMap.get(account.Id).ShippingPostalCode){
                accountIds.add(account.Id);
            }
        }
        if(!accountIds.isEmpty()){
            callAddressValidate(accountIds, true);
        }
    }

    public static void dataDotComReFormat(List<Account> accounts) {
        for(Account act : accounts) {
            System.debug('act=======>'+act.Customer_Registration__c);
        }
        DataDotComReformatUtility.dataDotComPhoneAndFaxReformat(accounts);
        DataDotComReformatUtility.dataDotComAccountPostalCodeReformat(accounts);
        for(Account act : accounts) {
            System.debug('act=======>'+act.Customer_Registration__c);
        }
    }

    public static void validateAddressOnInsert(List<Account> accounts){
        Set<Id> accountIds = new Set<Id>();
        for(Account account : accounts){
            System.debug('account_CustomerRegistration----->'+account.Customer_Registration__c);
                accountIds.add(account.Id);
        }
        if(!accountIds.isEmpty()){
            callAddressValidate(accountIds, true);
        }
    }
    
    public static void callAddressValidate(Set<Id> accountIds, boolean asynCallOut){
        if(accountIds.isEmpty()) {
            return;
        } else if(!AddressValidator.Pending_Avalara_Response) {
            //Address Validator WS 'avalara'
            if(asynCallOut){
                //Asynchronous call
                AddressValidator.accountAddressToValidate_Future(accountIds);
            } else {
                //Synchronous call
                AddressValidator.accountAddressToValidate(accountIds);
            }
        }
    }
    public static void isSyncWithNavNeeded(List<Account> newAccts, Map<Id,Account> oldAccountMap){
        for(Account acc: newAccts){
            if(checkAddressChange(acc,oldAccountMap.get(acc.id)) 
                || checkNameChange(acc,oldAccountMap.get(acc.id))){
                acc.Needs_Sync__c = true;
            }
        }
    }
    
 /*   public static void isSyncWithNavNeeded(List<Account> newAccts, Map<Id,Account> oldAccountMap){
        for(Account acc: newAccts){
            if(checkAddressApprovalProcessChange(acc,oldAccountMap.get(acc.id)) 
                || checkNameChange(acc,oldAccountMap.get(acc.id))){
                acc.Needs_Sync__c = true;
            }
        }
    }
    
    //This code syncs the address changes with navision only after the address is approved after change
    
    public static boolean checkAddressApprovalProcessChange(Account newAcct, Account oldAcct) {
        if(newAcct.Address_Approval_Process__c == 'Approved' && newAcct.Address_Approval_Process__c != oldAcct.Address_Approval_Process__c) {
            return true;
        }
        return false;
    } */

    public static boolean checkAddressChange(Account newAcct, Account oldAcct){
        if( (newAcct.BillingStreet != null && newAcct.BillingStreet != oldAcct.BillingStreet) 
            || (newAcct.BillingCity != null  && newAcct.BillingCity != oldAcct.BillingCity) 
            || (newAcct.BillingState != null && newAcct.BillingState != oldAcct.BillingState) 
            || (newAcct.BillingPostalCode != null && newAcct.BillingPostalCode != oldAcct.BillingPostalCode) 
            || (newAcct.ShippingStreet !=null && newAcct.ShippingStreet != oldAcct.ShippingStreet) 
            || (newAcct.ShippingCity  != null && newAcct.ShippingCity != oldAcct.ShippingCity )
            || (newAcct.ShippingState != null && newAcct.ShippingState != oldAcct.ShippingState) 
            || (newAcct.ShippingPostalCode != null && newAcct.ShippingPostalCode != oldAcct.ShippingPostalCode)){
            return true;
        }
        return false;           
    }

    public static boolean checkNameChange(Account newAcct, Account oldAcct){
        if(newAcct.Name != oldAcct.Name){
            return true;
        }
        return false;
    }

    public static void updateCustomerStatus(List<Account> accounts, Map<Id,Account> oldAccountMap) {
        for(Account acc : accounts) {
            if(acc.Order_Placed__c == true && acc.Order_Placed__c != oldAccountMap.get(acc.Id).Order_Placed__c && acc.Account_Status__c != 'Disqualified') {
                acc.Account_Status__c = 'Customer';
            }
        }
    }

    public static void priceBookCodeLink(List<Account> accounts) {
        Map<Id, PriceBook2> acctToPriceBookMap = new Map<Id, Pricebook2>();
        Map<String, List<Id>> pbcToAcctId = new Map<String, List<Id>>();
        Set<String> pbcSet = new Set<String>();

        for(Account acc : accounts) {

                        pbcSet.add(acc.PriceBookCode__c);
                        if(pbcToAcctId.containsKey(acc.PriceBookCode__c)) {
                            pbcToAcctId.get(acc.PriceBookCode__c).add(acc.Id);
                        } else {
                            pbcToAcctId.put(acc.PriceBookCode__c, new List<Id>{acc.Id});
                        }
               System.debug('============>'+acc.Customer_Registration__c);
        }

        for(Pricebook2 pb : [SELECT Id, PriceBookCode__c FROM Pricebook2 WHERE PriceBookCode__c IN :pbcSet]) {
            if(pbcToAcctId.containsKey(pb.PriceBookCode__c)){
                for(Id accId : pbcToAcctId.get(pb.PriceBookCode__c)){
                    acctToPriceBookMap.put(accId, pb);
                }
            }
        }

        for(Account acc : accounts) {
            if(acctToPriceBookMap.get(acc.Id) != null) {
                acc.Price_Book_List__c = acctToPriceBookMap.get(acc.Id).Id;
            }

            if(acc.PricebookCode__c == 'UNKNOWN'){
                acc.Price_Book_List__c = null;
            }
            System.debug('=====2=======>'+acc.Customer_Registration__c);
        }
    }

    public static void passDownKeyMessage(List<Account> accounts, Map<Id, Account> oldAccountMap) {

        Set<Id> parentAccountIds = new Set<Id>();
        List<Account> accsToUpdate = new List<Account>();
        Map<Id, String> parentAccToMessage = new Map<Id, String>();

        for(Account acc : accounts) {
            System.debug('==================--======>'+acc.Customer_Registration__c);
            if(oldAccountMap != null){
                if(acc.Key_Account_Message__c != null && oldAccountMap.get(acc.Id).Key_Account_Message__c != acc.Key_Account_Message__c) {
                    parentAccountIds.add(acc.Id);
                    parentAccToMessage.put(acc.Id, acc.Key_Account_Message__c);
                }
            } else if(acc.Key_Account_Message__c != null) {
                parentAccountIds.add(acc.Id);
                parentAccToMessage.put(acc.Id, acc.Key_Account_Message__c);
            }
        }

        for(Account acc : [SELECT ParentId, Key_Account_Message__c, Parent.Key_Account_Message__c FROM Account WHERE ParentId IN :parentAccountIds]) {
            acc.Key_Account_Message__c = parentAccToMessage.get(acc.ParentId);
            accsToUpdate.add(acc);
        }
        if(!accsToUpdate.isEmpty())
            Database.update(accsToUpdate, false);
    }

    public static void formatPhoneFax (List<Account> accounts, Map<Id, Account> oldAccountMap){

        Pattern MyPattern = Pattern.compile('\\d{10}');
        Set<String> fields = new Set<String> {'Phone', 'Fax'};

        for(Account acc : accounts) {
            System.debug('acccccc==========>'+acc.Customer_Registration__c);
            for(String field : fields) {
                if(oldAccountMap != null) {
                    if(acc.get(field) != null && acc.get(field) != oldAccountMap.get(acc.Id).get(field)){
                        String value = (String)acc.get(field);
                        Matcher MyMatcher = MyPattern.matcher(value);
                        if(MyMatcher.matches()) {
                            value = '(' + value.substring(0, 3) + ') ' + value.substring(3, 6) + '-' + value.substring(6, 10);
                            acc.put(field, value);
                        }
                    }
                } else if(acc.get(field) != null) {
                    String value = (String)acc.get(field);
                    Matcher MyMatcher = MyPattern.matcher(value);
                    if(MyMatcher.matches()) {
                        value = '(' + value.substring(0, 3) + ') ' + value.substring(3, 6) + '-' + value.substring(6, 10);
                        acc.put(field, value);
                    }
                }
            }
        }
    }
    
     public static void getDistanceOfWarehouses(Map<Id, Account> newAccMap, Map<Id, Account> oldAccMap){
        //Set<Id> setAccountIds; Commented to use the List<Account> to send into giic_CommonUtility.getDistanceOfWarehouses method
        list<Account> lstAccountToUpdate=new list<Account>();
         if(oldAccMap != null){
            //setAccountIds = new Set<Id>(); 
            lstAccountToUpdate=new list<Account>();
            for(Account newAcc : newAccMap.values()){
            system.debug('newAcc'+newAcc);
                Account oldAcc = oldAccMap.get(newAcc.Id);
                String newAddress = newAcc.Shippingstreet + newAcc.Shippingcity + newAcc.Shippingstate + newAcc.Shippingcountry + 
                   newAcc.ShippingpostalCode + newAcc.Shippinglatitude + newAcc.Shippinglongitude,
                    oldAddress = oldAcc.Shippingstreet + oldAcc.Shippingcity + oldAcc.Shippingstate + oldAcc.Shippingcountry + 
                    oldAcc.ShippingpostalCode + oldAcc.Shippinglatitude + oldAcc.Shippinglongitude;
                     system.debug('newAddress =='+ newAddress );
                     system.debug('oldAddress =='+ oldAddress );
                if(!newAddress.equalsIgnoreCase(oldAddress)){
                    //setAccountIds.add(newAcc.Id); commented to use the list<account>
                    lstAccountToUpdate.add(newAcc);
                    system.debug('lstAccountToUpdate'+lstAccountToUpdate);
                }
            }
        }
        
        //if(setAccountIds.size() > 0){
         if(lstAccountToUpdate.size() > 0){
            map<String,Object> mapResponse = giic_CommonUtility.getDistanceOfWarehouses(lstAccountToUpdate);
            system.debug('mapResponse'+mapResponse);
            String error;
            If(mapResponse.containsKey('ERROR')){
                error= (String)mapResponse.get('ERROR');
            }else{
                system.debug('success');
            }
            system.debug('error == '+error);
        }
     } 
    
    public static void UpdateAccountReference(Map<Id, Account> newAccMap){
        Map<String, Id> locationTowarehouseId = new Map<String, Id>();
        List<String> locationCodes = new List<String>();
        for(Account acc : [Select Id, Location_code__c from account where id in :newAccMap.keySet()]){
            locationCodes.add(acc.Location_Code__c);
        }
        for(gii__Warehouse__c w  : [select Id, name, giic_WarehouseCode__c from gii__warehouse__c where giic_WarehouseCode__c in :locationCodes]){
            locationTowarehouseId.put(w.giic_WarehouseCode__c, w.Id);
        }
        List<gii__AccountAdd__c> accRefs = [Select Id, Name, gii__Account__c, gii__Carrier__c, giic_WarehouseGroup__c, gii__DefaultWarehouse__c, gii__PaymentTerms__c, gii__PaymentMethod__c, gii__PriceBookName__c from gii__AccountAdd__c where gii__Account__c in : newAccMap.keySet() order by gii__Account__c];
        for(gii__AccountAdd__c accRef : accRefs){
            Account acc = newAccMap.get(accRef.gii__Account__c); 
           Boolean isLocValid = acc.Location_Code__c != null && locationTowarehouseId.containsKey(acc.Location_Code__c);
           accRef.gii__Carrier__c = acc.giic_Carrier__c != null ? acc.giic_Carrier__c : accRef.gii__Carrier__c;
           accRef.gii__DefaultWarehouse__c = (isLocValid ? locationTowarehouseId.get(acc.Location_Code__c) : accRef.gii__DefaultWarehouse__c);
           accRef.gii__PaymentMethod__c = acc.Payment_Method__c != null ? acc.Payment_Method__c : accRef.gii__PaymentMethod__c;
           accRef.gii__PaymentTerms__c = acc.giic_Payment_Terms__c != null ? acc.giic_Payment_Terms__c : accRef.gii__PaymentTerms__c;
           accRef.gii__PriceBookName__c = acc.PriceBookCode__c != null ? acc.PriceBookCode__c : accRef.gii__PriceBookName__c;
           accRef.giic_WarehouseGroup__c = acc.giic_WarehouseGroup__c != null ? acc.giic_WarehouseGroup__c : accRef.giic_WarehouseGroup__c;
   
      }
        update accRefs;
    } 
          
   /* public static void populateAddressChanges(List<Account> newAccountList,List<Account> oldAccountList, Map<Id,Account> oldAccountMap) {
       for(Account newAcc : newAccountList) { 
         if(newAcc.Address_Approval_Process__c != oldAccountMap.get(newAcc.Id).Address_Approval_Process__c  
                && newAcc.Address_Approval_Process__c  == 'Approved') {
                newAcc.Is_Billing_Changed__c = false;
                newAcc.Is_Shipping_Changed__c = false;
         }
         if(newAcc.Address_Approval_Process__c != oldAccountMap.get(newAcc.Id).Address_Approval_Process__c  
                && newAcc.Address_Approval_Process__c  == 'Rejected') {
                if(newAcc.Is_Billing_Changed__c && (newAcc.BillingCity != newAcc.Old_Billing_City__c || newAcc.BillingCountry != newAcc.Old_Billing_Country__c || newAcc.BillingState != newAcc.Old_Billing_State_Province__c ||
                    newAcc.BillingStreet != newAcc.Old_Billing_Street__c || newAcc.BillingPostalCode != newAcc.Old_Billing_Zip_Postal_Code__c)) {
                    newAcc.BillingCity = newAcc.Old_Billing_City__c;
                    newAcc.BillingCountry = newAcc.Old_Billing_Country__c;
                    newAcc.BillingState = newAcc.Old_Billing_State_Province__c;
                    newAcc.BillingStreet = newAcc.Old_Billing_Street__c;
                    newAcc.BillingPostalCode = newAcc.Old_Billing_Zip_Postal_Code__c;
                    newAcc.Is_Billing_Changed__c = false;
                }
                if(newAcc.Is_Shipping_Changed__c  && (newAcc.ShippingCity!= newAcc.Old_Shipping_City__c || newAcc.ShippingCountry != newAcc.Old_Shipping_Country__c || newAcc.ShippingState!= newAcc.Old_Shipping_State_Province__c ||
                    newAcc.ShippingStreet != newAcc.Old_Shipping_Street__c || newAcc.ShippingPostalCode != newAcc.Old_Shipping_Zip_Postal_Code__c)) {
                    newAcc.ShippingCity = newAcc.Old_Shipping_City__c;
                    newAcc.ShippingCountry = newAcc.Old_Shipping_Country__c;
                    newAcc.ShippingState = newAcc.Old_Shipping_State_Province__c;
                    newAcc.ShippingStreet = newAcc.Old_Shipping_Street__c;
                    newAcc.ShippingPostalCode = newAcc.Old_Shipping_Zip_Postal_Code__c;
                    newAcc.Is_Shipping_Changed__c = false;
                }
                //addressChangeAccounts.add(newAcc);
         } else {       
                if(newAcc.BillingCity != oldAccountMap.get(newAcc.Id).BillingCity || newAcc.BillingCountry != oldAccountMap.get(newAcc.Id).BillingCountry || newAcc.BillingState != oldAccountMap.get(newAcc.Id).BillingState ||
                    newAcc.BillingStreet != oldAccountMap.get(newAcc.Id).BillingStreet || newAcc.BillingPostalCode != oldAccountMap.get(newAcc.Id).BillingPostalCode ) {
                    System.debug('---Enter into if stement-----');
                    newAcc.Old_Billing_City__c = oldAccountMap.get(newAcc.Id).BillingCity;
                    newAcc.Old_Billing_Country__c= oldAccountMap.get(newAcc.Id).BillingCountry;
                    newAcc.Old_Billing_State_Province__c= oldAccountMap.get(newAcc.Id).BillingState;
                    newAcc.Old_Billing_Street__c= oldAccountMap.get(newAcc.Id).BillingStreet;
                    newAcc.Old_Billing_Zip_Postal_Code__c= oldAccountMap.get(newAcc.Id).Billingpostalcode;
                    newAcc.Is_Billing_Changed__c = true;
                                        
                }
                if(newAcc.ShippingCity!= oldAccountMap.get(newAcc.Id).ShippingCity || newAcc.ShippingCountry != oldAccountMap.get(newAcc.Id).ShippingCountry || newAcc.ShippingState!= oldAccountMap.get(newAcc.Id).ShippingState||
                    newAcc.ShippingStreet != oldAccountMap.get(newAcc.Id).ShippingStreet || newAcc.ShippingPostalCode != oldAccountMap.get(newAcc.Id).ShippingPostalCode ) {
                    
                    newAcc.Old_Shipping_City__c= oldAccountMap.get(newAcc.Id).shippingcity;
                    newAcc.Old_Shipping_Country__c = oldAccountMap.get(newAcc.Id).ShippingCountry;
                    newAcc.Old_Shipping_State_Province__c = oldAccountMap.get(newAcc.Id).ShippingState;
                    newAcc.Old_Shipping_Street__c = oldAccountMap.get(newAcc.Id).ShippingStreet;
                    newAcc.Old_Shipping_Zip_Postal_Code__c = oldAccountMap.get(newAcc.Id).ShippingPostalCode;
                    newAcc.Is_Shipping_Changed__c = true;
                    //addressChangeAccounts.add(newAcc);
                }                              
           }
       }
    } */
        
   /* public static void addressChangeSendForApprovalRejected(List<Account> newAccountList,List<Account> oldAccountList, Map<Id,Account> oldAccountMap) {
      
        List<Account> addressChangeAccounts = new List<Account>();
        List<Approval.ProcessSubmitRequest> approvalReqList=new List<Approval.ProcessSubmitRequest>();
        for(Account newAcc : newAccountList) {            
            if(newAcc.Address_Approval_Process__c != oldAccountMap.get(newAcc.Id).Address_Approval_Process__c  
                && newAcc.Address_Approval_Process__c  == 'In Process' && newAcc.Address_Approval_Process__c != 'Pending' && oldAccountMap.get(newAcc.Id).Address_Approval_Process__c != 'Pending') {
                Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
                //req.setComments('Submitted for approval. Please approve.');
                req.setObjectId(newAcc.Id);
                // submit the approval request for processing
                approvalReqList.add(req); 
                //Approval.ProcessResult result = Approval.process(req);                
             } 
            
             if(!approvalReqList.isEmpty() && approvalReqList.size() > 0) {
             //submit the approval request for processing        
                 List<Approval.ProcessResult> resultList = Approval.process(approvalReqList);        
             //display if the reqeust was successful
                 for(Approval.ProcessResult result: resultList ) {        
                      System.debug('Submitted for approval successfully: '+result.isSuccess());      
                 }  
             }   
        } 
    } */
}