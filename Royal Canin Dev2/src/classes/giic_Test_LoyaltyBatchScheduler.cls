/************************************************************************************
Version : 1.0
Created Date : 26 Nov 2018
Function : Test class for class giic_LoyaltyBatchScheduler 
Modification Log : NA
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/
@isTest(SeeAllData = false)
private  class giic_Test_LoyaltyBatchScheduler {
    
    @testSetup
    static void setup(){
        List<gii__Warehouse__c> defaultWHList = giic_Test_DataCreationUtility.insertWarehouse();
        giic_Test_DataCreationUtility.testSystemPolicyCreation();
        giic_Test_DataCreationUtility.CreateAdminUser();   
    }
    public static testMethod void testExecute(){
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        System.Runas(u)        
        { 
            List<gii__SystemPolicy__c> lstSystemPolicy = [select Name from gii__SystemPolicy__c ];         
            apexpages.standardcontroller stdController = new apexpages.standardcontroller(lstSystemPolicy[0]);
            giic_LoyaltyBatchScheduler obj = new giic_LoyaltyBatchScheduler(stdController);
            giic_LoyaltyBatchScheduler obj1 = new giic_LoyaltyBatchScheduler(stdController);
            Test.startTest(); 
                obj.scheduleBatch();
                List<ApexPages.Message> msgList =  ApexPages.getMessages() ;
                
                if(msgList.size()>0)
                {
                    System.assertEquals(msgList[0].getSummary(),giic_Constants.Batch_MSG_SCHEDULED);
                }
            	obj1.scheduleBatch();
            Test.stopTest();       
        }
    } 
}