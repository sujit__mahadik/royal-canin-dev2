/************************************************************************************
Version : 1.0
Name : giic_SOStagingToSOConversionBatch
Created Date : 14 Aug 2018
Function : Convert the Sales Order Staging to Sales Orders
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/
global class giic_SOStagingToSOConversionBatch implements Database.Batchable<sObject>, Database.Stateful{
    
    global String sObjectApiName = '';
    global Map<String, giic_IntegrationCustomMetaDataWrapper> mapIntegrationSettings = new Map<String, giic_IntegrationCustomMetaDataWrapper>();
    
    public giic_SOStagingToSOConversionBatch(Map<String, giic_IntegrationCustomMetaDataWrapper> tempMapIntegrationSettings, String strSObjectApiName){
        mapIntegrationSettings = tempMapIntegrationSettings;
        sObjectApiName = strSObjectApiName;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){      
        String soql = '';
        if(mapIntegrationSettings.containsKey(sObjectApiName)){            
            giic_IntegrationCustomMetaDataWrapper cmdObj = mapIntegrationSettings.get(sObjectApiName); 
            soql = 'Select ' + cmdObj.strSoql.subString(0, cmdObj.strSoql.length()-1) + ' from ' + cmdObj.sourceSobjectApi + ' where giic_Status__c =  \''+ System.Label.giic_Draft + '\'' ;
            soql+= ' and giic_ExecuteTrigger__c = true';            
        }

        return Database.getQueryLocator(soql);
    }
    
    global void execute(Database.BatchableContext BC, List<gii__SalesOrderStaging__c> lstSOStaging){
        try
        {
            if(!lstSOStaging.isEmpty()){
                
                Set<Id> setSOSIds = new Set<Id>();
                for(gii__SalesOrderStaging__c obj : lstSOStaging)
                {
                    setSOSIds.add(obj.id);              
                }
                if(setSOSIds.size()>0)
                {
                    //DeActivate Errors
                    giic_IntegrationCustomMetaDataUtil.deActivateCurrentError(setSOSIds);                    
                }          
                Set<String> setErrorIds = new Set<String>();
                Map<String, giic_IntegrationCMDResultWrapper> mpNewSObjectId=giic_IntegrationCustomMetaDataUtil.doConversion(sObjectApiName, mapIntegrationSettings, lstSOStaging, setErrorIds);
                
                // peace of code update SOS and SO status // Start here 
                List<gii__SalesOrderStaging__c> lstSourceObject = new List<gii__SalesOrderStaging__c>();
                lstSourceObject = lstSOStaging;
                List<gii__SalesOrder__c> soList = new List<gii__SalesOrder__c>();
                List<gii__SalesOrder__c> soListToUpdate = new List<gii__SalesOrder__c>();
                for(gii__SalesOrderStaging__c objSOS : lstSourceObject){               
                    if(setErrorIds.contains(''+objSOS.id)) objSOS.giic_Status__c = System.Label.giic_Error;
                    else if(mpNewSObjectId.get(objSOS.Id).totalChilds != 0){
                        
                        objSOS.giic_Status__c = System.Label.giic_InProgress; 
                        gii__SalesOrder__c so = new gii__SalesOrder__c(Id=mpNewSObjectId.get(objSOS.Id).targetObj.Id, giic_ProcessingStatus__c=System.Label.giic_ProcessedwithError,giic_SOStatus__c=System.Label.giic_Draft); 
                        soList.add(so); 
                        
                        }
                    else { 
                        objSOS.giic_Status__c = System.Label.giic_Converted; 
                        gii__SalesOrder__c so = new gii__SalesOrder__c(Id=mpNewSObjectId.get(objSOS.Id).targetObj.Id, giic_ProcessingStatus__c=System.Label.giic_Processed); 
                        soList.add(so); }
                }
                // update soList; // this list of sales Order will be updated below in code
                update lstSourceObject;
                
                // piece of code update SOS and SO status // Ends here 
                
                set<string> SOIds = new set<string>();
                list<gii__SalesOrder__c> lstSalesOrder = new list<gii__SalesOrder__c>();
                 set<string> setAccountId = new set<string>();
                 
                if(mpNewSObjectId.values() != null && mpNewSObjectId.values().size() > 0){
                    for(giic_IntegrationCMDResultWrapper oICMDRW : mpNewSObjectId.values()){
                        if(oICMDRW.targetObj != null && oICMDRW.targetObj.getSObjectType() == Schema.gii__SalesOrder__c.getSObjectType() ){
                            if(string.isNotEmpty(string.valueOf( oICMDRW.targetObj.get('Id')))){
                                SOIds.add(string.valueOf( oICMDRW.targetObj.get('Id')));
                                if(oICMDRW.targetObj.get('gii__AccountReference__c') != null && oICMDRW.targetObj.get('gii__Account__c') != null) lstSalesOrder.add((gii__SalesOrder__c) oICMDRW.targetObj);
                                if(oICMDRW.targetObj.get('gii__Account__c') != null) setAccountId.add(string.valueOf(oICMDRW.targetObj.get('gii__Account__c')));
                            }
                        }
                    }
                }

                //insert update promotions on sales order
                try{
                   map<string,object> mapPromoResponce = giic_PromotionHelper.getAccProgramForSOStoSO(lstSalesOrder, setAccountId);
                   list<gii__AccountProgram__c> lstAccountProgramToInsert = (list<gii__AccountProgram__c>) mapPromoResponce.get('ACCPROINSERT');
                   list<gii__AccountProgram__c> lstAccountProgramToUpdate = (list<gii__AccountProgram__c>) mapPromoResponce.get('ACCPROUPDATE');
                   list<giic_PromotionWrapper.PromoResponseWrapper> lstPromoResponseWrapper = (list<giic_PromotionWrapper.PromoResponseWrapper>) mapPromoResponce.get('ERRORINFO');
                   
                   if(lstAccountProgramToInsert != null && !lstAccountProgramToInsert.isEmpty()) insert lstAccountProgramToInsert;
                   if(lstAccountProgramToUpdate != null && !lstAccountProgramToUpdate.isEmpty()) update lstAccountProgramToUpdate;
                   if(lstPromoResponseWrapper != null && !lstPromoResponseWrapper.isEmpty()){
                       List<Integration_Error_Log__c> lstUOMErrors = new List<Integration_Error_Log__c>();
                        Set<String> setErrorPromoIds = new Set<String>();
                        String userStory = 'Promotion Calculation';
                        Map<String, String> fieldApiForErrorLogMaster = new Map<String, String>{'giic_SalesOrder__c'=>'Id'};
                       for(giic_PromotionWrapper.PromoResponseWrapper objWrapper : lstPromoResponseWrapper){
                            giic_IntegrationCustomMetaDataUtil.collectErrors(lstUOMErrors, setErrorPromoIds, userStory, fieldApiForErrorLogMaster, new gii__SalesOrder__c(Id = objWrapper.orderId), 'Promotion Calculation Error', objWrapper.lstErrorInformation[0].errorMessage); 
                
                       }
                       
                       if(!lstUOMErrors.isEmpty()) insert lstUOMErrors;
                   }
                   
                }catch(exception e){
                   System.debug('Exp:' + e.getLineNumber() + e.getMessage());
                }
                   
                
                //Check for exception state of the converted orders
                giic_IntegrationCommonUtil.checkForExceptionState(SOIds);
                
                set<string> ZIPCODES = new set<string>();
                set<string> WHGIDS = new set<string>();
                set<string> ZIPWHKEY = new set<string>();
                map<string,object> mapRequest = new map<string,object>();
                map<string, List<gii__SalesOrder__c>> mapZipCodeWHGroupIdVsLstSO = new map<string, List<gii__SalesOrder__c>>();
                List<gii__SalesOrder__c> lstSOToUpdate = new List<gii__SalesOrder__c>();
                map<Id, gii__SalesOrder__c> mapSalesOrder = new map<Id, gii__SalesOrder__c>();
                List<gii__SalesOrder__c> lstSO =  [select id, name, gii__DropShip__c, giic_DropShipWHMapping__c, gii__Warehouse__c, gii__ShipToZipPostalCode__c, 
                                                   gii__AccountReference__r.giic_WarehouseGroup__c from gii__SalesOrder__c where gii__ShipToZipPostalCode__c != null AND gii__AccountReference__c != null 
                                                   AND gii__AccountReference__r.giic_WarehouseGroup__c != null AND id in : SOIds AND ((giic_Source__c = 'INSITE' AND gii__DropShip__c = true) OR giic_Source__c = 'OLP')];

                for(gii__SalesOrder__c objSO :  lstSO ){
                    ZIPCODES.add(objSO.gii__ShipToZipPostalCode__c);
                    WHGIDS.add(objSO.gii__AccountReference__r.giic_WarehouseGroup__c);
                    ZIPWHKEY.add(objSO.gii__ShipToZipPostalCode__c+'~'+objSO.gii__AccountReference__r.giic_WarehouseGroup__c);
                    if(!mapZipCodeWHGroupIdVsLstSO.containsKey(objSO.gii__ShipToZipPostalCode__c+'~'+objSO.gii__AccountReference__r.giic_WarehouseGroup__c)){
                        mapZipCodeWHGroupIdVsLstSO.put(objSO.gii__ShipToZipPostalCode__c+'~'+objSO.gii__AccountReference__r.giic_WarehouseGroup__c, new List<gii__SalesOrder__c>{});
                    }
                    mapZipCodeWHGroupIdVsLstSO.get(objSO.gii__ShipToZipPostalCode__c+'~'+objSO.gii__AccountReference__r.giic_WarehouseGroup__c).add(objSO);
                }
                mapRequest.put('ZIPCODES', ZIPCODES);
                mapRequest.put('WHGIDS', WHGIDS);
                mapRequest.put('ZIPWHKEY', ZIPWHKEY);

                map<string,Object> mapZipCodesResponse = giic_CommonUtility.getDistanceOfWarehousesFromZipCode(mapRequest);
                
                map<string,string> mapZipCode = new map<string,string>();
                if(mapZipCodesResponse.containsKey('ZIPCODESMAP')){
                    // mapZipCode contains dropship warehouse distance based on ZipCode+WarehouseGroupId
                    mapZipCode=(map<string,string>)mapZipCodesResponse.get('ZIPCODESMAP');
                }
                if(mapZipCodeWHGroupIdVsLstSO !=null && mapZipCodeWHGroupIdVsLstSO.keySet()!=null && mapZipCodeWHGroupIdVsLstSO.keySet().size()>0 && mapZipCode !=null && mapZipCode.size()>0){
                    for(string sZipCodeWHGId: mapZipCodeWHGroupIdVsLstSO.keySet() ){
                        if(mapZipCode.containsKey(sZipCodeWHGId)){
                            for(gii__SalesOrder__c objSO : mapZipCodeWHGroupIdVsLstSO.get(sZipCodeWHGId)){
                                objSO.giic_DropShipWHMapping__c = mapZipCode.get(sZipCodeWHGId);
                                lstSOToUpdate.add(objSO);
                            }
                        }
                    }
                }
                
                if(lstSOToUpdate != null && lstSOToUpdate.size() > 0){
                    for(gii__SalesOrder__c oSO : lstSOToUpdate){
                        mapSalesOrder.put(oSO.Id, oSO);
                    }
                    // update lstSOToUpdate;
                }
                gii__SalesOrder__c objSO;
                for(gii__SalesOrder__c oSO : soList){
                    if(mapSalesOrder.containsKey(oSO.Id)){
                        objSO = mapSalesOrder.get(oSO.Id);
                        objSO.giic_ProcessingStatus__c = oSO.giic_ProcessingStatus__c;
                        soListToUpdate.add(objSO);
                    }else{
                        soListToUpdate.add(oSO);
                    }
                }
                
                if(soListToUpdate != null && soListToUpdate.size() > 0){
                    update soListToUpdate;
                }
            }
        }
        catch(Exception ex)
        {
            System.debug('Exception==='+ex.getMessage()+'~'+ex.getLineNumber());
        }
        
    }
    
    global void finish(Database.BatchableContext BC){
        
    }
}