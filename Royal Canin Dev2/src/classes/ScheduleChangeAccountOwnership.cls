/*
 * Date:       		 - Developer, Company					       - Description
 * 2015-05-04        - Mayank Srivatava, Acumen Solutions             - Created
 */
global class ScheduleChangeAccountOwnership implements Schedulable {
	
	public static string cron = '0 0 1 * * ?';
	
	global void execute(SchedulableContext sc) {
      ChangeAccountOwnershipBatch cab = new ChangeAccountOwnershipBatch(); 
      database.executebatch(cab);
    }

    //call this from exec anon after code deploy
	global static String schedule() {
		ScheduleChangeAccountOwnership schedule = new ScheduleChangeAccountOwnership();
		return System.Schedule('ChangeAccountOwnershipBatch', cron, schedule);
	}
}