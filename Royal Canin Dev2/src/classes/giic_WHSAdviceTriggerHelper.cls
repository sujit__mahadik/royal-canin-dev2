/**********************************************************
* Purpose    : The purpose of this class to handle giic_WHSAdviceTrigger event by extending Triggerhandler.  
*              
* *******************************************************
* Author                        Date            Remarks
*                               24/09/2018     
*********************************************************/
public with sharing class giic_WHSAdviceTriggerHelper {
    
    
    
      /***********************************************************
    * Method name : OnBeforeInsert
    * Description : to handle After Insert events of trigger
    * Return Type : void
    * Parameter : List of gii__WarehouseShipmentAdviceLinesStaging__c records
    ***********************************************************/
    public void OnBeforeInsert(List<gii__WarehouseShipmentAdviceStaging__c > lstTriggerNew)
    { 
        //system.debug('lstTriggerNew--->>>>>>>>> ' + lstTriggerNew); // do not remove log
       // checkDuplicateShipment(lstTriggerNew); // check for duplicate records for fulfilment with processed status if any duplicate found throw exception 
    }
    
    
    
    
     /***********************************************************
    * Method name : OnAfterInsert
    * Description : to handle After Update events of trigger
    * Return Type : void
    * Parameter : List of gii__WarehouseShipmentAdviceLinesStaging__c records
    ***********************************************************/
    public void OnAfterUpdate(List<gii__WarehouseShipmentAdviceStaging__c > lstTriggerNew)
    {  
        //system.debug('lstTriggerNew--->>>>>>>>> ' + lstTriggerNew); // do not remove log
        List<Integration_Error_Log__c> lstErrors = new List<Integration_Error_Log__c>();
        Set<String> setErrorRecords = new Set<String>();
        
        Map<String,gii__WarehouseShipmentAdviceStaging__c> mapWSAS = new Map<String,gii__WarehouseShipmentAdviceStaging__c>();
        for(gii__WarehouseShipmentAdviceStaging__c objWSAS : lstTriggerNew){
            if(objWSAS.giic_ProcessStatus__c == giic_Constants.INPROGRESS) mapWSAS.put(objWSAS.Id, objWSAS); 
        }
        if(!mapWSAS.isEmpty()){
            Map<String,Object> mapDuplicateSHipment = checkDuplicateShipment(mapWSAS.values()); // check for duplicate shipment is already processed
            
            Map<String,Object> mapReturnHeader = validateWSAdviceStaging(mapWSAS.values()); // validate header records
            system.debug('mapReturnHeader--->>>>>>>>> ' + mapReturnHeader); // do not remove log
            if(mapReturnHeader.containsKey('lstErrors') && mapReturnHeader.get('lstErrors') != null) lstErrors.addAll((List<Integration_Error_Log__c>) mapReturnHeader.get('lstErrors'));
            if(mapReturnHeader.containsKey('setErrorRecords') && mapReturnHeader.get('setErrorRecords') != null) setErrorRecords.addAll((Set<String>) mapReturnHeader.get('setErrorRecords'));
            
            List<gii__WarehouseShipmentAdviceLinesStaging__c> lstWSASLines = getWSLineStaging(mapWSAS.keySet()); // get child records
            Map<String,Object> mapReturnChild = validateWSALineStaging(lstWSASLines); // validate child records
            system.debug('mapReturnChild--->>>>>>>>> ' + mapReturnChild); // do not remove log
            if(mapReturnChild.containsKey('lstErrors') && mapReturnChild.get('lstErrors') != null) lstErrors.addAll((List<Integration_Error_Log__c>) mapReturnChild.get('lstErrors'));
            if(mapReturnChild.containsKey('setErrorRecords') && mapReturnChild.get('setErrorRecords') != null) setErrorRecords.addAll((Set<String>) mapReturnChild.get('setErrorRecords'));
            
            List<id> lstWHSIds = new List<id>();
            List<gii__WarehouseShipmentAdviceStaging__c> lstErrorRecords = new List<gii__WarehouseShipmentAdviceStaging__c>();
            for(gii__WarehouseShipmentAdviceStaging__c objWSAS : mapWSAS.values()){
                if(!mapDuplicateSHipment.containsKey(objWSAS.Id)){
                    if(!setErrorRecords.contains(objWSAS.Id)) lstWHSIds.add(objWSAS.Id);
                    else{
                        lstErrorRecords.add(new gii__WarehouseShipmentAdviceStaging__c(Id = objWSAS.Id, giic_ProcessStatus__c = giic_Constants.PROCESSEDWITHERROR));
                    }
                }else{
                    lstErrorRecords.add(new gii__WarehouseShipmentAdviceStaging__c(Id = objWSAS.Id, giic_ProcessStatus__c = giic_Constants.PROCESSEDWITHERROR));
                    lstErrors.addAll(giic_CyberSourceRCCCUtility.handleError(objWSAS.id, giic_Constants.USER_STORY_SHIPMENTCREATION, giic_Constants.ERROR_INVALIDRECORD,String.valueOf(mapDuplicateSHipment.get(objWSAS.Id))));
                }
            }
            
            if(!lstErrors.isEmpty()) insert lstErrors;
            if(!lstErrorRecords.isEmpty()) update lstErrorRecords;
            system.debug('lstWHSIds--->>>>>>>>> ' + lstWHSIds); // do not remove log
            if(!lstWHSIds.isEmpty() && lstWHSIds.size() < 40){
                if(!Test.isRunningTest()) ID jobID = System.enqueueJob(new giic_ProcessShipmentQueue(lstWHSIds)); // call queue for records to process one by one if not test in test its maximum stack limit reach
            }else{
                return;
            }
        }
    }
    
     /*********************************************************** 
    * Method name : checkDuplicateShipment
    * Description : to check is any shipment present for fulfilment
    * Return Type : Map<String,Object>
    * Parameter : List of gii__WarehouseShipmentAdviceStaging__c records
    ***********************************************************/
    public static Map<String,Object> checkDuplicateShipment(List<gii__WarehouseShipmentAdviceStaging__c> lstWSAS)
    {
        Map<String,Object> mapReturn = new Map<String,Object>(); // lstErrors , setErrorRecords
         Set<String> setFulfilmentNumber = new Set<String>();
         
         for(gii__WarehouseShipmentAdviceStaging__c objWSAS : lstWSAS)
        {
            if(objWSAS.giic_WHSShipNo__c != null)
            {
                 setFulfilmentNumber.add(objWSAS.giic_WHSShipNo__c);
            }
        }
        map<String,String> mapWSASVsFulfilment = new map<String,String>();
        if(!setFulfilmentNumber.isEmpty()){
            List<gii__WarehouseShipmentAdviceStaging__c> lstWSASDuplicate =  [select id, Name, giic_WHSShipNo__c, giic_WHSShipNo__r.Name from gii__WarehouseShipmentAdviceStaging__c 
                                                                                where giic_WHSShipNo__c IN : setFulfilmentNumber and giic_ProcessStatus__c =: giic_Constants.PROCESSED];
           
            if(!lstWSAS.isEmpty()){
                for(gii__WarehouseShipmentAdviceStaging__c wsas : lstWSASDuplicate){
                    mapWSASVsFulfilment.put(wsas.giic_WHSShipNo__c,wsas.id); 
                } 
            }
        }
        for(gii__WarehouseShipmentAdviceStaging__c objWSAS : lstWSAS)
        {
            if(objWSAS.giic_WHSShipNo__c != null && mapWSASVsFulfilment != null && mapWSASVsFulfilment.containsKey(objWSAS.giic_WHSShipNo__c))
            {
                //objWSAS.addError(system.label.giic_ShipmentAlreadyPresent + ' ' +  mapWSASVsFulfilment.get(objWSAS.giic_WHSShipNo__c));
                mapReturn.put(objWSAS.Id, system.label.giic_ShipmentAlreadyPresent + ' ' +  mapWSASVsFulfilment.get(objWSAS.giic_WHSShipNo__c));
            }
        }
        
        return mapReturn;
    }
    
    
    
    /***********************************************************
    * Method name : getWSLineStaging
    * Description : get child records
    * Return Type : List<gii__WarehouseShipmentAdviceLinesStaging__c>
    * Parameter : Set<String> setWSASIds
    ***********************************************************/
    public static List<gii__WarehouseShipmentAdviceLinesStaging__c> getWSLineStaging(Set<String> setWSASIds){
        List<gii__WarehouseShipmentAdviceLinesStaging__c> lstWSASLines = new List<gii__WarehouseShipmentAdviceLinesStaging__c>(); 
        if(!setWSASIds.isEmpty()){
            lstWSASLines = [Select Id, Name, giic_WHSShipLineNo__c, giic_WHSShipLineNo__r.Name, giic_WHSShipLineNo__r.giic_SalesOrderLine__r.Name,giic_WHSShipLineNo__r.giic_SalesOrderLine__c, giic_OrderQty__c, giic_ShipQty__c, 
                            giic_ProductSKU__c,giic_UOM__c, giic_WHSShipAdviceStaging__c, giic_ProductLot__c, giic_TrackingNumber__c, 
                            giic_Carrier__c, giic_WHSShipAdviceStaging__r.giic_ShipDate__c 
                            from gii__WarehouseShipmentAdviceLinesStaging__c 
                            where giic_WHSShipAdviceStaging__c IN : setWSASIds];
        }
        return lstWSASLines;
    }
    
    /***********************************************************
    * Method name : validateWSAdviceStaging
    * Description : to validate if header records are valid or not
    * Return Type : Map<String,Object>
    * Parameter : List of gii__WarehouseShipmentAdviceStaging__c records
    ***********************************************************/
    public static Map<String,Object> validateWSAdviceStaging(List<gii__WarehouseShipmentAdviceStaging__c> lstWSAS)
    {
        Map<String,Object> mapReturn = new Map<String,Object>(); // lstErrors , setErrorRecords
        if(!lstWSAS.isEmpty()){
            Set<String> setOrderNumbers = new Set<String>();
            Set<String> setWarehouseCode = new Set<String>();
            Set<String> setCarrier = new Set<String>();
            List<Integration_Error_Log__c> lstErrors = new List<Integration_Error_Log__c>();
            Set<String> setErrorRecords = new Set<String>();
           
           Set<id> setRejectedFulfilmentRecords = checkForRejectedFulfilments(lstWSAS);
           
           
            for(gii__WarehouseShipmentAdviceStaging__c objWSAS : lstWSAS)
            {
                if(objWSAS.giic_WHSShipNo__c != null && setRejectedFulfilmentRecords.contains(objWSAS.giic_WHSShipNo__c)) //giic_Status__c == giic_Constants.REJECTED_STATUS
                {
                    setErrorRecords.add(objWSAS.Id);
                    lstErrors.addAll(collectErrors(objWSAS.id,system.label.giic_FulfillmentRejectedByDC)); //Fulfillment is rejected by DC
                    
                }else{
                    if(objWSAS.giic_WHSShipNo__c == null)
                        {
                            setErrorRecords.add(objWSAS.Id);
                            lstErrors.addAll(collectErrors(objWSAS.id,system.label.giic_ShipmentNumberMissing));
                            
                        }
                        if(objWSAS.giic_Warehouse__c == null || objWSAS.giic_Warehouse__c == '')
                        {
                            setErrorRecords.add(objWSAS.id);
                            lstErrors.addAll(collectErrors(objWSAS.id,system.label.giic_WarehouseCodeMissing));
                        }else setWarehouseCode.add(objWSAS.giic_Warehouse__c);
                        if(objWSAS.giic_Carrier__c == null || objWSAS.giic_Carrier__c == '' )
                        {
                            setErrorRecords.add(objWSAS.id);
                            lstErrors.addAll(collectErrors(objWSAS.id,system.label.giic_CarrierisMissing));
                        }else  setCarrier.add(objWSAS.giic_Carrier__c);
                        if(objWSAS.giic_ShipDate__c == null){
                            setErrorRecords.add(objWSAS.id);
                            lstErrors.addAll(collectErrors(objWSAS.id,system.label.giic_ShipDateMissing));
                        }
                    map<String,id> mapWarehouseCodeDB = new map<String,id>(); 
                    if(!setWarehouseCode.isEmpty()){
                        for(gii__Warehouse__c w :  [select id, giic_WarehouseCode__c from gii__Warehouse__c where giic_WarehouseCode__c IN : setWarehouseCode])
                        {
                           mapWarehouseCodeDB.put(w.giic_WarehouseCode__c,w.id);
                        }
                    }
                   
                   Set<String> validCarriers = new Set<String>();
                   if(!setCarrier.isEmpty()) validCarriers = getCarrier(setCarrier);
                    for(gii__WarehouseShipmentAdviceStaging__c objWS : lstWSAS)
                    {
                        if(!mapWarehouseCodeDB.containsKey(objWS.giic_Warehouse__c))
                        {
                            setErrorRecords.add(objWS.id);
                            lstErrors.addAll(collectErrors(objWS.id,system.label.giic_WHCodeNotAvailable));
                        }
                        if(!validCarriers.contains(objWS.giic_Carrier__c))
                        {
                            setErrorRecords.add(objWS.id);
                            lstErrors.addAll(collectErrors(objWS.id,system.label.giic_CarrierNotPresent));
                        }
                            
                    }
                }
            }  
            mapReturn.put('lstErrors',lstErrors);
            mapReturn.put('setErrorRecords',setErrorRecords);
        }
        return mapReturn;
    }
    
    /***********************************************************
    * Method name : validateWSALineStaging
    * Description : to validate if child records are valid or not
    * Return Type : Map<String,Object>
    * Parameter : List of gii__WarehouseShipmentAdviceLinesStaging__c records
    ***********************************************************/
    Public static Map<String,Object> validateWSALineStaging(List<gii__WarehouseShipmentAdviceLinesStaging__c > lstWSALineStaging)
    {
        Map<String,Object> mapReturn = new Map<String,Object>(); //map - whas id , isvalid - false
        if(!lstWSALineStaging.isEmpty()){
            Set<String> setErrorRecords = new Set<String>();
            List<Integration_Error_Log__c> lstErrors = new List<Integration_Error_Log__c>();
            set<String> setUOM = new set<String>();
            set<String> setMissingUOM = new set<String>();
            
            for(gii__WarehouseShipmentAdviceLinesStaging__c objWSALine : lstWSALineStaging)
            {
                if(objWSALine.giic_ProductSKU__c == null || objWSALine.giic_ProductSKU__c == '')  //validate lines 
              {
                    setErrorRecords.add(objWSALine.giic_WHSShipAdviceStaging__c); 
                    lstErrors.addAll(collectErrors(objWSALine.giic_WHSShipAdviceStaging__c,system.label.giic_ProductSKUMissingLine + objWSALine.Name));  //Product SKU Missing on Line = 
              }
              if(objWSALine.giic_WHSShipLineNo__c == null){
                    setErrorRecords.add(objWSALine.giic_WHSShipAdviceStaging__c); 
                    lstErrors.addAll(collectErrors(objWSALine.giic_WHSShipAdviceStaging__c,system.label.giic_ShipmentLineNumberMissing + objWSALine.Name)); // Shipment Line Number Missing on Line =
              }/*else{
                  
              }*/
              if(objWSALine.giic_UOM__c == null || objWSALine.giic_UOM__c == ''){
                    setMissingUOM.add(objWSALine.Id);
                    setErrorRecords.add(objWSALine.giic_WHSShipAdviceStaging__c); 
                    lstErrors.addAll(collectErrors(objWSALine.giic_WHSShipAdviceStaging__c, system.label.giic_UMOMissing + objWSALine.Name)); //Unit Of Measure Missing on Line =
              }else{
                  setUOM.add(objWSALine.giic_UOM__c);
              }
            }
            if(!setUOM.isEmpty()){
                map<String,id> mapUOM = getUOM(setUOM);
        
                for(gii__WarehouseShipmentAdviceLinesStaging__c objWSALine : lstWSALineStaging)
                {
                    if(!setMissingUOM.contains(objWSALine.Id) && !mapUOM.containsKey(objWSALine.giic_UOM__c))
                    {
                        setErrorRecords.add(objWSALine.giic_WHSShipAdviceStaging__c); 
                        lstErrors.addAll(collectErrors(objWSALine.giic_WHSShipAdviceStaging__c, system.label.giic_WrongUOM + ' = ' + objWSALine.Name)); //Wrong Unit Of Measure on Line 
                    }
                }
            }
            
            mapReturn.put('lstErrors',lstErrors);
            mapReturn.put('setErrorRecords',setErrorRecords);
        }
        return mapReturn;
    }
    
    public static map<String,id> getUOM(set<String> setUOM)
    {
        map<String,id> mapUOM = new map<String,id>();
        if(!setUOM.isEmpty()){
           for(gii__UnitofMeasure__c um : [select id, gii__UniqueId__c  from gii__UnitofMeasure__c where gii__UniqueId__c  IN : setUOM])
           {
               mapUOM.put(um.gii__UniqueId__c,um.id);
           }
        }
       return mapUOM;
    }
    
    public static set<String> getCarrier(set<String> setCarrier)
    {
        list<gii__Carrier__c> carriers = new list<gii__Carrier__c>();
        set<String> validCarriers = new set<String>();
        if(!setCarrier.isEmpty()){
            for(gii__Carrier__c carr : [select id,  giic_UniqueCarrier__c from gii__Carrier__c where  giic_UniqueCarrier__c In : setCarrier])
            {
                validCarriers.add(carr.giic_UniqueCarrier__c);
            }
        }
        return validCarriers;
    }
    
     public static set<id> checkForRejectedFulfilments(List<gii__WarehouseShipmentAdviceStaging__c> lstWSAS)
    {
        set<id> setRejectedFulfilmentRecords = new set<id>();
        set<String> setFulfilmentIds = new set<String>();
        for(gii__WarehouseShipmentAdviceStaging__c objWSAS : lstWSAS){
           if(objWSAS.giic_WHSShipNo__c != null) setFulfilmentIds.add(objWSAS.giic_WHSShipNo__c);
        }
        
        if(!setFulfilmentIds.isEmpty()){
            Map<Id, gii__WarehouseShippingOrderStaging__c> mapFulfilment = new Map<Id, gii__WarehouseShippingOrderStaging__c> ([Select Id, Name from gii__WarehouseShippingOrderStaging__c 
                                                                        where id IN : setFulfilmentIds and giic_Status__c = : giic_Constants.REJECTED_STATUS ]);
            if(!mapFulfilment.isEmpty()) setRejectedFulfilmentRecords.addAll(mapFulfilment.keySet());
        }
        return setRejectedFulfilmentRecords;
    } 
    
     /***********************************************************
    * Method name : collectErrors
    * Description : collect error log
    * Return Type : List<Integration_Error_Log__c>
    * Parameter : String objId, String error
    ***********************************************************/
    public static List<Integration_Error_Log__c> collectErrors(String objId, String error)
    { 
        List<Integration_Error_Log__c> lstErrors = new List<Integration_Error_Log__c>();
        Set<String> setErrorIds = new Set<String>();
        String userStory = giic_Constants.USER_STORY_REQ_FIELD_MISSING;
        Map<String, String> fieldApiForErrorLog = new Map<String, String>{'giic_WarehouseShipmentAdviceStaging__c'=>'Id'};
            gii__WarehouseShipmentAdviceStaging__c objSO =  new gii__WarehouseShipmentAdviceStaging__c(Id=objId);          
        giic_IntegrationCustomMetaDataUtil.collectErrors(lstErrors, setErrorIds, userStory, fieldApiForErrorLog, objSO, giic_Constants.ERROR_INVALIDRECORD, error);        
        return lstErrors;
        
    }
    
}