/************************************************************************************
Version : 1.0
Created Date : 17 Jan 2019
Function : Authorize Amount to Rc cc
Modification Log : 

* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/

public with sharing class giic_AuthAmountHelper {
    /*
* Method name : authAmountToRccc
* Description :Authorize Amount to Rc cc
* Return Type : Map<String, Object>
* Parameter : fulfillmentId (Warehouse Shipping Staging id)
*/
    @auraEnabled
    public static Map<String, Object> authAmountToRccc(String fulfillmentId){
        Map<String, Object> mapResult = new Map<String, Object>();
        if(fulfillmentId != null && fulfillmentId != ''){
            try
            { 
                List<gii__WarehouseShippingOrderStaging__c> listFulfillment = [select Id, Name, giic_Status__c, giic_SalesOrder__r.giic_CurrencyIsoCode__c, giic_SalesOrder__r.Name, 
                                                                               (select id, gii__SettlementAmount__c	 from Sales_Order_Payment_Settlements__r where  gii__SalesOrderPayment__r.gii__PaymentMethod__c =: giic_Constants.CREDIT_CARD),
                                                                               (select id, giic_SalesOrderLine__c from Warehouse_Shipping_Order_Lines_Staging__r )                                                               
                                                                               from  gii__WarehouseShippingOrderStaging__c where id=:fulfillmentId and giic_IntegrationStatus__c =: giic_Constants.AUTHORIZE_AMOUNT];
                if(!listFulfillment.isEmpty()){
                    if(listFulfillment[0].giic_Status__c == giic_Constants.INITIAL || listFulfillment[0].giic_Status__c == giic_Constants.FULLFILLMWNT_SUBMITTEDWITHERROR){
                        Decimal totalAmount = 0;
                        List<gii__SalesOrderPaymentSettlement__c> listSOPSettlements = new List<gii__SalesOrderPaymentSettlement__c>();
                        if(!listFulfillment[0].Sales_Order_Payment_Settlements__r.isEmpty()){
                            listSOPSettlements = listFulfillment[0].Sales_Order_Payment_Settlements__r;
                            for(gii__SalesOrderPaymentSettlement__c sopObj: listSOPSettlements){
                                totalAmount = totalAmount + sopObj.gii__SettlementAmount__c;
                            }
                            
                            if(totalAmount > 0){
                                Map<String, Object> mapParams = new Map<String, Object>();
                                giic_BillToWrapper billTo = new giic_BillToWrapper();
                                billTo.OrderNumber = listFulfillment[0].giic_SalesOrder__r.Name;
                                billTo.FulfillmentNumber = listFulfillment[0].Name;
                                billTo.PaymentCurrency = String.isBlank(listFulfillment[0].giic_SalesOrder__r.giic_CurrencyIsoCode__c) ? giic_Constants.DEFAULT_CURRENCY : listFulfillment[0].giic_SalesOrder__r.giic_CurrencyIsoCode__c; 
                                mapParams.put('billTo', billTo);
                                mapParams.put('totalAmount', totalAmount);
                                mapParams.put('recordId', listFulfillment[0].Id);
                                
                                Map<String, Object> mapAuthResult = giic_CyberSourceRCCCUtility.authorizationRCCC(mapParams);
                                // Checking response from server
                                if(!mapAuthResult.containsKey('error')){
                                    listFulfillment[0].giic_IntegrationStatus__c = giic_Constants.SENT_TO_WMS;
                                    listFulfillment[0].giic_Status__c = giic_Constants.FF_STATUS_IN_PROGRESS;
                                    update listFulfillment[0];
                                }else{
                                    mapResult.put('errorMsg', Label.giic_AmountAuthError);  //	Error occur in Amount Authorization. Please see error log
                                }
                            }else{
                                mapResult.put('errorMsg', Label.giic_AmountZeroError); //Settlement amount must be greater than zero.
                                giic_CyberSourceRCCCUtility.handleErrorFuture(fulfillmentId, giic_Constants.USER_STORY_PAYMENTAUTHORIZATION, giic_Constants.PAYMENTAUTHORIZATION_ERROR_CODE, Label.giic_AmountZeroError);
                            }
                        }else{
                            mapResult.put('errorMsg', Label.giic_CheckPaymentMethod);  //You can not authorize amount since SOP setllment is not there with Credit Card type.
                            giic_CyberSourceRCCCUtility.handleErrorFuture(fulfillmentId, giic_Constants.USER_STORY_PAYMENTAUTHORIZATION, giic_Constants.PAYMENTAUTHORIZATION_ERROR_CODE, Label.giic_CheckPaymentMethod);
                        }
                    }else{
                        mapResult.put('errorMsg', Label.giic_AmountAuthStatus);  // Amount can only be Authorized on Initial or Submitted with Error status.
                    }
                }else{
                    mapResult.put('errorMsg', Label.giic_TaxNotCalculated);  // Amount can only be Authorized on Authorize Amount Integration status.
                }
            }catch(Exception ex) {
                system.debug('exp: authAmountToRccc:' +  ex.getMessage()+ ' line number::'+ex.getLineNumber()) ; 
                mapResult.put('errorMsg', Label.giic_AmountAuthError); 
                giic_CyberSourceRCCCUtility.handleErrorFuture(fulfillmentId, giic_Constants.USER_STORY_PAYMENTAUTHORIZATION, giic_Constants.PAYMENTAUTHORIZATION_ERROR_CODE, 'authAmountToRccc:' +  ex.getMessage()+ ' line number::'+ex.getLineNumber());
            }
        }
        return mapResult;
    }
    /*
* Method name : dropAmountAuth
* Description : Drop authorization for the amount authorized 
* Return Type : Map<String, Object>
* Parameter : fulfillmentId (Warehouse Shipping Staging id)
*/
    @auraEnabled
    public static Map<String, Object> dropAmountAuth(String fulfillmentId){
        Map<String, Object> mapResult = new Map<String, Object>();
        if(fulfillmentId != null && fulfillmentId != ''){
            try
            { 
                List<gii__WarehouseShippingOrderStaging__c> listFulfillment = [select Id, Name, giic_Status__c, giic_SalesOrder__r.giic_CurrencyIsoCode__c, giic_SalesOrder__r.Name, 
                                                                               (select id, gii__SettlementAmount__c	 from Sales_Order_Payment_Settlements__r where  gii__SalesOrderPayment__r.gii__PaymentMethod__c =: giic_Constants.CREDIT_CARD),
                                                                               (select id, giic_SalesOrderLine__c from Warehouse_Shipping_Order_Lines_Staging__r )                                                               
                                                                               from  gii__WarehouseShippingOrderStaging__c 
                                                                               where id=:fulfillmentId and giic_IntegrationStatus__c =: giic_Constants.SENT_TO_WMS];
                if(!listFulfillment.isEmpty()){
                    if(listFulfillment[0].giic_Status__c == giic_Constants.INITIAL || listFulfillment[0].giic_Status__c == giic_Constants.FULLFILLMWNT_SUBMITTEDWITHERROR){
                        Decimal totalAmount = 0;
                        List<gii__SalesOrderPaymentSettlement__c> listSOPSettlements = new List<gii__SalesOrderPaymentSettlement__c>();
                        if(!listFulfillment[0].Sales_Order_Payment_Settlements__r.isEmpty()){
                            listSOPSettlements = listFulfillment[0].Sales_Order_Payment_Settlements__r;
                            for(gii__SalesOrderPaymentSettlement__c sopObj: listSOPSettlements){
                                totalAmount = totalAmount + sopObj.gii__SettlementAmount__c;
                            }
                            
                            if(totalAmount > 0){
                                Map<String, Object> mapParams = new Map<String, Object>();
                                giic_BillToWrapper billTo = new giic_BillToWrapper();
                                billTo.OrderNumber = listFulfillment[0].giic_SalesOrder__r.Name;
                                billTo.FulfillmentNumber = listFulfillment[0].Name;
                                billTo.PaymentCurrency = String.isBlank(listFulfillment[0].giic_SalesOrder__r.giic_CurrencyIsoCode__c) ? giic_Constants.DEFAULT_CURRENCY : listFulfillment[0].giic_SalesOrder__r.giic_CurrencyIsoCode__c; 
                                mapParams.put('billTo', billTo);
                                mapParams.put('totalAmount', totalAmount);
                                mapParams.put('recordId', listFulfillment[0].Id);
                                
                                Map<String, Object> mapAuthResult = giic_CyberSourceRCCCUtility.reverseAuthorizationRCCC(mapParams);
                                // Checking response from server
                                if(!mapAuthResult.containsKey('error')){
                                    listFulfillment[0].giic_IntegrationStatus__c = giic_Constants.DROP_AUTHORIZATION;
                                    update listFulfillment[0];
                                }else{
                                    mapResult.put('errorMsg', Label.giic_GenericAuthRevError);  //	Error occur in Drop Authorization. Please see error log
                                }
                            }else{
                                mapResult.put('errorMsg', Label.giic_AmountZeroError); //Settlement amount must be greater than zero.
                                giic_CyberSourceRCCCUtility.handleErrorFuture(fulfillmentId, giic_Constants.RCCC_USER_STORY, giic_Constants.ProcessCCOMS_ReverseAuth, Label.giic_AmountZeroError);
                            }
                        }else{
                            mapResult.put('errorMsg', Label.giic_SPOSWithCCNotFound);  //You can not authorize amount since SOP setllment is not there with Credit Card type.
                            giic_CyberSourceRCCCUtility.handleErrorFuture(fulfillmentId, giic_Constants.RCCC_USER_STORY, giic_Constants.ProcessCCOMS_ReverseAuth, Label.giic_SPOSWithCCNotFound);
                        }
                    }/*else{
                        mapResult.put('errorMsg', Label.giic_AmountAuthStatus);  // Amount can only be Authorized on Initial or Submitted with Error status.
                    }*/
                }/*else{
                    mapResult.put('errorMsg', Label.giic_TaxNotCalculated);  // Amount can only be Authorized on Authorize Amount Integration status.
                }*/
            }catch(Exception e) {
                System.debug('Exception type caught: ' + e.getTypeName());    
                System.debug('Message: ' + e.getMessage());    
                System.debug('Cause: ' + e.getCause());    // returns null
                System.debug('Line number: ' + e.getLineNumber());    
                System.debug('Stack trace: ' + e.getStackTraceString()); 
                mapResult.put('errorMsg','Message: '+ e.getMessage()+ ' Line number: ' +e.getLineNumber() + ' Stack trace: ' + e.getStackTraceString()); 
                giic_CyberSourceRCCCUtility.handleErrorFuture(fulfillmentId, giic_Constants.RCCC_USER_STORY, giic_Constants.ProcessCCOMS_ReverseAuth, (String)mapResult.get('errorMsg'));
            }
        }
        return mapResult;
    }
}