/************************************************************************************
Version : 1.0
Created Date : 27 Aug 2018
Function : Test class for giic_InvoiceUtility
Modification Log : NA
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/
@isTest
public class giic_Test_InvoiceUtility {
    @testSetup
    static void setup()
    {        
        giic_Test_DataCreationUtility.CreateAdminUser();
        List<Account> testConsumerAccountList = giic_Test_DataCreationUtility.insertConsumerAccount();
        List<gii__Warehouse__c> testWareList = giic_Test_DataCreationUtility.insertWarehouse();
        SetupTestData.createCustomSettings();
        giic_Test_DataCreationUtility.testSystemPolicyCreation();       
        List<gii__AccountAdd__c> testAccRefList = giic_Test_DataCreationUtility.getAccountReference(testConsumerAccountList);
        Database.insert(testAccRefList);
        List<gii__Product2Add__c> testProductList = giic_Test_DataCreationUtility.insertProduct();
        List<gii__ProductInventory__c> lstPI = giic_Test_DataCreationUtility.insertProductInventory(1);
        System.assertEquals(lstPI.size()>0, true);
    //    System.debug('lstPI>>>>'+lstPI);        
        List<gii__SalesOrder__c> testSOList = giic_Test_DataCreationUtility.insertSalesOrder(testConsumerAccountList[0].Id, testAccRefList[0].Id);
        List<gii__SalesOrderLine__c> testSOLine = giic_Test_DataCreationUtility.insertSOLine(testSOList[0].Id, testProductList[0].Id);
        // map for sales order line ids and quantity to be reserved
        Map<id,Double> SOLineQuantityMap = new Map<id,Double>();
        Boolean  NewSOLine = false; 
        String Action = 'Partial Reservation';
        for(gii__SalesOrderLine__c obj : testSOLine )
        {
            Decimal i =0.0;
            //Set the Sales Order Line's record id by String instead of id
            id SOLine = obj.id;
            Double Quantity = 1.0+i; // (Selling UM)
            // build map for sales order line ids and quantity to be reserved
            SOLineQuantityMap.put(SOLine, Quantity);
            i++;
        }       
        // execute method to create inventory reservation records 
        try {
            gii.Reserve2.Reservations(SOLineQuantityMap, NewSOLine, Action);
        }catch(exception e){
            system.debug('Error : '+e.getMessage());
        } 
        System.debug('query>>>'+[select id,gii__SalesOrderLine__c,gii__ReserveQuantity__c from gii__InventoryReserve__c]);
	        
        List<Id> selectedRecordIds = new List<Id>();
		for(gii__InventoryReserve__c obj : [select id,gii__SalesOrderLine__c,gii__ReserveQuantity__c from gii__InventoryReserve__c])
        {
            selectedRecordIds.add(obj.id);
        }
        // get ship date string value in yyyy-MM-DD format and 
        // pass it to packlist ship confirm class along with 
        // groupBy with semi-colon ';' as  delimeter
        String groupBy = 'SalesOrder';
        String shippedDateString = system.today().year() + '-' + system.today().month() + '-' + system.today().day() ;   
        String groupByAndShippedDate = groupBy + ';' + shippedDateString;       
        
        
        // execute method to create pick lists
        try {
            gii.OrderShipment.quickShip(selectedRecordIds, groupByAndShippedDate);
        }
        catch(exception e){
              //  system.debug('Error1 : '+e.getMessage());
            Boolean Error1 =  e.getMessage().contains('Error Message') ? true : false;
            System.AssertEquals(Error1, true);
        
        } 
        
       //System.debug('query1>>>'+[select id from gii__Shipment__c]);
    
    }
    public static testmethod void TestCreateInvoices()
    {
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        System.Runas(u)  
        {
        Account accObj = [SELECT Id FROM Account LIMIT 1];
        gii__SalesOrder__c testSO = [SELECT Id, giic_SOStatus__c, gii__Account__c FROM gii__SalesOrder__c WHERE gii__Account__c =: accObj.Id LIMIT 1];
        Set<Id> setSOID = new Set<Id>();
        setSOID.add(testSO.id);
        giic_InvoiceUtility.CreateInvoices(setSOID);
        
    }
    }
    public static testmethod void TestcollectErrors()
    {
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
       System.Runas(u)  
       {
        List<Integration_Error_Log__c> lstErrors = new List<Integration_Error_Log__c>();
        Set<String> stErrorIds = new Set<String>();
        String userStory;
        Map<String, String> relatedToFieldAPi = new Map<String, String>();
        List<Sobject> sourceRecord = new List<Sobject>();
        String errorCode;
        String errorMessage;
        giic_InvoiceUtility.collectErrors(lstErrors,stErrorIds,userStory,relatedToFieldAPi,sourceRecord,errorCode,errorMessage);
    }
    }
}