/************************************************************************************
Version : 1.0
Name : giic_Test_CalculateTaxforSOLQueue
Created Date : 09 October 2018
Function : test class for giic_CalculateTaxforSOLQueue 
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/

@isTest(SeeAllData = false)
private class giic_Test_CalculateTaxforSOLQueue
{

    @testSetup
    static void setup()
    {
         giic_Test_DataCreationUtility.insertWarehouse();
        
        giic_Test_DataCreationUtility.testSystemPolicyCreation();
        giic_Test_DataCreationUtility.insertGlobalParametersCustomSetting();
        giic_Test_DataCreationUtility.insertConsumerAccount();
        giic_Test_DataCreationUtility.createCarrier();
        giic_Test_DataCreationUtility.insertUnitofMeasure();
        //giic_Test_DataCreationUtility.insertConsumerAccount_N(10);
        giic_Test_DataCreationUtility.insertProduct();
        giic_Test_DataCreationUtility.insertLocations();
        giic_Test_DataCreationUtility.insertProductInventory(1);
        giic_Test_DataCreationUtility.insertProductInventoryByLoc();
        giic_Test_DataCreationUtility.insertProductInventoryQTYDetails();
        giic_Test_DataCreationUtility.insertSalesOrder(); // 0 - open sales order , 1 - release sales order
        
        
        giic_Test_DataCreationUtility.lstSalesOrder[1].giic_TaxCal__c = false;
        giic_Test_DataCreationUtility.lstSalesOrder[1].giic_SOStatus__c = giic_Constants.SO_PICKED;
        update giic_Test_DataCreationUtility.lstSalesOrder[1];
        giic_Test_DataCreationUtility.insertSOLine(new list<gii__SalesOrder__c>{giic_Test_DataCreationUtility.lstSalesOrder[1]});
        //retriee sales order lines whre order is released 
        List<gii__SalesOrderLine__c>  lstSalesOrderLines = [select id,name,gii__UnitPrice__c,gii__SalesOrder__c,gii__Warehouse__c,gii__OrderQuantity__c,gii__Status__c,gii__Product__c, gii__Product__r.giic_ProductSKU__c from gii__SalesOrderLine__c where gii__SalesOrder__c = : giic_Test_DataCreationUtility.lstSalesOrder[1].id];
        for(gii__SalesOrderLine__c soline  : lstSalesOrderLines)
        {
            soline.giic_LineStatus__c = giic_Constants.SHIPPED;
        }
        update lstSalesOrderLines;

        list<gii__SalesOrder__c>  lstSalesOrder = [select id,gii__Released__c, gii__Carrier__r.giic_UniqueCarrier__c, gii__Warehouse__r.giic_WarehouseCode__c, gii__Account__c,name from gii__SalesOrder__c where id = : giic_Test_DataCreationUtility.lstSalesOrder[1].id ];
       
         giic_Test_DataCreationUtility.insertSalesOrderPayment(giic_Test_DataCreationUtility.lstSalesOrder[1].id);
        giic_Test_DataCreationUtility.insertWarehouseShipmentAdviceStaging(lstSalesOrder);
        giic_Test_DataCreationUtility.insertWarehouseShipmentAdviceLinesStaging(lstSalesOrderLines);
        giic_Test_DataCreationUtility.CreateAdminUser();   
    }
    private static testMethod void positivetest() 
    {
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        
        System.Runas(u)        
        {
            List<gii__Product2Add__c> lstProd = [ SELECT Id from gii__Product2Add__c limit 1];
            lstProd[0].gii__NonStock__c = false;
            update lstProd ;
            List<gii__SalesOrder__c> lstSO = [Select Id,giic_SOStatus__c,gii__Released__c,gii__Account__r.Blocked__c from gii__SalesOrder__c LIMIT 1];
            List<gii__SalesOrderLine__c> lstSOL = [Select Id,gii__LineStatus__c,giic_LineStatus__c,gii__ReservedQuantity__c,gii__Product__r.gii__NonStock__c from gii__SalesOrderLine__c LIMIT 1];
            List<gii__WarehouseShippingOrderStaging__c> lstWRS = [Select Id from gii__WarehouseShippingOrderStaging__c];
          //  System.debug('lstSO+++'+lstSO);
            System.assertEquals(lstSO.size()>0, true);
             System.assertEquals(lstSOL.size()>0, true);
              System.assertEquals(lstWRS.size()>0, true);
         //   System.debug('lstSOL+++'+lstSOL);
          //  System.debug('lstWRS+++'+lstWRS);      
            for(gii__SalesOrderLine__c obj : lstSOL)
            {
             //   System.debug('Product Type+++'+obj.gii__Product__r.gii__NonStock__c);
                 System.assert(obj.gii__Product__r.gii__NonStock__c != null);
            }
            
         //   System.debug('lstSO+++'+([select id,(select id,gii__Warehouse__c,gii__Product__c,Name,gii__Product__r.gii__DefaultWarehouse__c,gii__OrderQuantity__c  from gii__SalesOrder__r where (((giic_LineStatus__c ='Initial' or giic_LineStatus__c='Back Order') and gii__NonStockQuantity__c =0) or giic_LineStatus__c='Rejected by DC' )  and gii__LineStatus__c='Open' and gii__Product__r.gii__NonStock__c =true)  from gii__SalesOrder__c LIMIT 1]).size());
            System.assertEquals(([select id,(select id,gii__Warehouse__c,gii__Product__c,Name,gii__Product__r.gii__DefaultWarehouse__c,gii__OrderQuantity__c  from gii__SalesOrder__r where (((giic_LineStatus__c ='Initial' or giic_LineStatus__c='Back Order') and gii__NonStockQuantity__c =0) or giic_LineStatus__c='Rejected by DC' )  and gii__LineStatus__c='Open' and gii__Product__r.gii__NonStock__c =true)  from gii__SalesOrder__c LIMIT 1]).size()>0,true);
            
            list<gii__WarehouseShipmentAdviceStaging__c> lstWarehouseshpmentStaging = [select id,giic_isTaxCommitted__c from gii__WarehouseShipmentAdviceStaging__c];
            list<gii__WarehouseShipmentAdviceLinesStaging__c> lstWarehouseshpmentStagingline = [select id from gii__WarehouseShipmentAdviceLinesStaging__c];
            if(lstWarehouseshpmentStaging != null && !lstWarehouseshpmentStaging.isEmpty())
            {         
                Map<String, Object> mapUpdateResult = new Map<String, Object>();
                Map<String, String> mapResult = new  Map<String, String>();
                Boolean isTaxExempt = false;
                Id whasId = lstWarehouseshpmentStaging[0].id;
                Test.startTest();
                
                    giic_AllocationBatchForNonStockSO allocation = new giic_AllocationBatchForNonStockSO();
                    // execute batch process
                    Database.executeBatch(allocation);
                    System.debug('SO+++'+[Select Id,giic_SOStatus__c,gii__Released__c,gii__Account__r.Blocked__c from gii__SalesOrder__c LIMIT 1]);
                    System.debug('SOL+++'+[Select Id,gii__LineStatus__c,gii__ReservedQuantity__c,gii__Product__r.gii__NonStock__c from gii__SalesOrderLine__c LIMIT 1]);
                    System.debug('WRS+++'+[Select Id from gii__WarehouseShippingOrderStaging__c LIMIT 1]);                    
                    giic_CalculateTaxforSOLQueue obj = new giic_CalculateTaxforSOLQueue (mapUpdateResult,mapResult,whasId,lstWarehouseshpmentStagingline ,isTaxExempt );        
                    System.enqueueJob(obj );     
                Test.stopTest();             
            } 
        }  
    }  
    private static testMethod void negativetest() 
    {
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        
        System.Runas(u)        
        {
            list<gii__WarehouseShipmentAdviceStaging__c> lstWarehouseshpmentStaging = [select id,giic_isTaxCommitted__c from gii__WarehouseShipmentAdviceStaging__c];
            list<gii__WarehouseShipmentAdviceLinesStaging__c> lstWarehouseshpmentStagingline = [select id from gii__WarehouseShipmentAdviceLinesStaging__c];
            if(lstWarehouseshpmentStaging != null && !lstWarehouseshpmentStaging.isEmpty())
            {         
                Map<String, Object> mapUpdateResult = new Map<String, Object>();
                Map<String, String> mapResult = new  Map<String, String>();
                Boolean isTaxExempt = true;
                Id whasId = lstWarehouseshpmentStaging[0].id;
                Test.startTest();
                    giic_CalculateTaxforSOLQueue obj = new giic_CalculateTaxforSOLQueue (mapUpdateResult,mapResult,whasId,lstWarehouseshpmentStagingline ,isTaxExempt );        
                    System.enqueueJob(obj);     
                Test.stopTest();             
            }
        }       
    }   
    private static testMethod void Exceptiontest() 
    {
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        
        System.Runas(u)        
        {
            list<gii__WarehouseShipmentAdviceStaging__c> lstWarehouseshpmentStaging = [select id,giic_isTaxCommitted__c from gii__WarehouseShipmentAdviceStaging__c];
            list<gii__WarehouseShipmentAdviceLinesStaging__c> lstWarehouseshpmentStagingline = [select id from gii__WarehouseShipmentAdviceLinesStaging__c];
            if(lstWarehouseshpmentStaging != null && !lstWarehouseshpmentStaging.isEmpty())
            {         
                Map<String, Object> mapUpdateResult = new Map<String, Object>();
                Map<String, String> mapResult = new  Map<String, String>();
                Boolean isTaxExempt = true;
                Id whasId =  lstWarehouseshpmentStaging[0].id;
                Test.startTest();
                    giic_CalculateTaxforSOLQueue obj = new giic_CalculateTaxforSOLQueue (mapUpdateResult,mapResult,whasId,lstWarehouseshpmentStagingline ,null );        
                    System.enqueueJob(obj);     
                Test.stopTest();             
            }
        }       
    }   
}