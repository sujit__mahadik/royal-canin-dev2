/************************************************************************************
Version : 1.0
Created Date : 28-Dec-2018
Function : Mock Test class for HTTPCallout
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/
global class giic_Test_CalloutMockImpl  implements HttpCalloutMock {

    // Implement this interface method
       global static HTTPResponse respond(HTTPRequest req) { 
         HttpResponse res = new HttpResponse();
      	String body = 'DUMMY RESPONSE';   
        res.setHeader('Content-Type', 'application/json');
        res.setBody(body);
        res.setStatusCode(200);
        return res;
    }
	
}