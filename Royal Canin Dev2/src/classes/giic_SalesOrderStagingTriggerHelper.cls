/**********************************************************
* Purpose    : The purpose of this class to handle giic_SalesOrderStagingTrigger event by extending Triggerhandler.  
*              
* *******************************************************
* Author                        Date            Remarks
* Abhishek Tripathi              21/11/2018     
*********************************************************/
public class giic_SalesOrderStagingTriggerHelper {
    
    /***********************************************************
    * Method name : OnAfterInsert
    * Description : to handle After Insert events of trigger
    * Return Type : void
    * Parameter : List of gii__SalesOrderStaging__c records
    ***********************************************************/
    public void OnAfterInsert(List<gii__SalesOrderStaging__c > lstTriggerNew) // this would be removed as to convert giic_ExecuteTrigger__c flag needs to be updated on SOS Object
    {        
        Set<ID> newsetSOSID = new Set<Id>();
        if(lstTriggerNew.size()>0)
        {
            for(gii__SalesOrderStaging__c obj : [select id, giic_Status__c,giic_AddressValidated__c from gii__SalesOrderStaging__c where id in : lstTriggerNew and giic_ExecuteTrigger__c = true and giic_Status__c != : System.Label.giic_Converted and (giic_AddressType__c != null and giic_AddressType__c != '') ])
            {                
                newsetSOSID.add(obj.id);                
            }           
        }
        if(newsetSOSID.size()>0)
        {
            ProcessSalesOrderStaging(newsetSOSID);    
        }          
    }
    /***********************************************************
    * Method name : OnAfterUpdate
    * Description : to handle After Update events of trigger
    * Return Type : void
    * Parameter : List of gii__SalesOrderStaging__c records
    ***********************************************************/
    public void OnAfterUpdate(List<gii__SalesOrderStaging__c> lstTriggerold, List<gii__SalesOrderStaging__c> lstTriggerNew, Map<Id,gii__SalesOrderStaging__c> mapTriggernew,Map<Id,gii__SalesOrderStaging__c>  mapTriggernold)
    {        
        
        Set<ID> newsetSOSID = new Set<Id>();        
        if(lstTriggerNew.size()>0)
        {
            for(gii__SalesOrderStaging__c obj : [select id, giic_Status__c,giic_AddressValidated__c from gii__SalesOrderStaging__c where id in : lstTriggerNew and giic_ExecuteTrigger__c = true and giic_Status__c != : System.Label.giic_Converted and (giic_AddressType__c != null and giic_AddressType__c != '')])
            {                
                newsetSOSID.add(obj.id);                
            }
        }
        if(newsetSOSID.size()>0)
        {
            ProcessSalesOrderStaging(newsetSOSID);       
        }        
    }    
    /**********************************************************************************************************************
    * Method name : ProcessSalesOrderStaging
    * Description : to convert Sales Order Staging records to Sales Orders if Drop Ship = true and Address Validation = true
    * Return Type : void
    * Parameter : Set of gii__SalesOrderStaging__c ids
    **********************************************************************************************************************/
    @future
    public static void ProcessSalesOrderStaging(Set<ID> setSOSID) 
    {        
        //DeActivate Errors
        giic_IntegrationCustomMetaDataUtil.deActivateCurrentError(setSOSID);        
        List<Integration_Error_Log__c> lstErrorLog = new List<Integration_Error_Log__c>();
        Set<String> setErrorIds = new Set<String>();
        Set<String> setSOSIds = new Set<String>();
        String sObjectApiName = 'gii__SalesOrderStaging__c'; 
        //Getting Intergarion Details for Sobject
        Map<String, giic_IntegrationCustomMetaDataWrapper> mapIntegrationSettings = giic_IntegrationCustomMetaDataUtil.getIntegrationSettings(sObjectApiName);            
        String soql = '';
        //Creating filter clause for SOQL for related Sales Order Staging Id's
        String inClause = String.format( '(\'\'{0}\'\')',new List<String> { String.join( new List<Id>(setSOSID) , '\',\'') });            
        if(mapIntegrationSettings.containsKey(sObjectApiName)){
            giic_IntegrationCustomMetaDataWrapper cmdObj = mapIntegrationSettings.get(sObjectApiName); 
            soql = 'Select ' + cmdObj.strSoql.subString(0, cmdObj.strSoql.length()-1) + ' from ' + cmdObj.sourceSobjectApi + ' where id in '+ inClause;            
        }        
        List<gii__SalesOrderStaging__c> lstSalesOrderStaging = Database.query(soql);        
        if(lstSalesOrderStaging.size()>0)
        {
            
            //Calling utility method do conversion for conversting SOS to SO and storing result in a map
            Map<String, giic_IntegrationCMDResultWrapper> mpNewSObjectId = giic_IntegrationCustomMetaDataUtil.doConversion(sObjectApiName, mapIntegrationSettings, lstSalesOrderStaging, setErrorIds);
            List<gii__SalesOrder__c> soList = new List<gii__SalesOrder__c>();
            for(gii__SalesOrderStaging__c objSOS :(List<gii__SalesOrderStaging__c>) lstSalesOrderStaging)
            {                
                if(setErrorIds.contains(''+objSOS.id)) 
                {   setSOSIds.add(objSOS.id);
                    objSOS.giic_Status__c = System.Label.giic_Error;
                }                       
                else if(mpNewSObjectId.get(objSOS.Id).totalChilds != 0)
                { 
                    setSOSIds.add(objSOS.id);
                    //Updating SOS Status
                    objSOS.giic_Status__c = System.Label.giic_InProgress; 
                    //Updating SO Status
                    gii__SalesOrder__c so = new gii__SalesOrder__c(Id=mpNewSObjectId.get(objSOS.Id).targetObj.Id,giic_ProcessingStatus__c=System.Label.giic_ProcessedwithError,giic_SOStatus__c=System.Label.giic_Draft); 
                    soList.add(so); 
                }
                else if(mpNewSObjectId.get(objSOS.Id).totalChilds == null || mpNewSObjectId.get(objSOS.Id).totalChilds == 0)
                {   
                    objSOS.giic_Status__c = System.Label.giic_Error;
                    
                    gii__SalesOrder__c so = new gii__SalesOrder__c(Id=mpNewSObjectId.get(objSOS.Id).targetObj.Id); 
                    so.giic_SOStatus__c=System.Label.giic_Draft;
                    so.giic_ProcessingStatus__c=System.Label.giic_ProcessedwithError;
                    soList.add(so); 
                    
                    Integration_Error_Log__c objEL = new Integration_Error_Log__c();
                    objEL.giic_SalesOrder__c = so.Id;
                    objEL.giic_SalesOrderStaging__c = objSOS.Id;
                    setSOSId.add(objSOS.Id);
                    objEL.Error_Code__c = giic_Constants.NO_LINE_ERROR;
                    objEL.Name = giic_Constants.NO_LINE_ERROR;
                    objEL.giic_IsActive__c = true;
                    objEL.Error_Message__c = giic_Constants.NO_LINE_AVAILABLE;
                    lstErrorLog.add(objEL);
                    
                }
                else 
                { 
                    //Updating SOS Status
                    objSOS.giic_Status__c = System.Label.giic_Converted; 
                    //Updating SOS Status
                    gii__SalesOrder__c so = new gii__SalesOrder__c(Id=mpNewSObjectId.get(objSOS.Id).targetObj.Id,giic_ProcessingStatus__c=System.Label.giic_Processed); 
                    soList.add(so); 
                }
            }
            if(soList.size()>0)
            {
                //Updating list of SO
                update soList;
            }    
            //Updating list of SOS
            update lstSalesOrderStaging;
            
            giic_CommonUtility.updateSOSErrorOnSalesOrder(setSOSIds); // Update error logs from sales order staging to sales orders
            if(lstErrorLog != null && lstErrorLog.size() > 0 ){
                insert lstErrorLog;
            }
            //Check for exception state in the converted SO
            set<string> SOIds = new set<string>();
            if(mpNewSObjectId.values() != null && mpNewSObjectId.values().size() > 0)
            {
               for(giic_IntegrationCMDResultWrapper oICMDRW : mpNewSObjectId.values()){
                   // 
                   if(oICMDRW.targetObj != null && oICMDRW.targetObj.getSObjectType() == Schema.gii__SalesOrder__c.getSObjectType() ){
                       if(string.isNotEmpty(string.valueOf( oICMDRW.targetObj.get('Id')))){
                           SOIds.add(string.valueOf( oICMDRW.targetObj.get('Id')));
                       }
                   }
               }
            }          
           //Check for exception state of the converted orders
           giic_IntegrationCommonUtil.checkForExceptionState(SOIds);           
        
        }       
    }  
}