/************************************************************************************
Version : 1.0
Created Date : 19 Sep 2018
Function : Controller for giic_ReleaseSalesOrder page.
Author : Abhishek Tripathi
Modification Log : 
* 25 Sept 2018 - Updated the query
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/

global class giic_SOReleaseBatch implements Database.Batchable<sObject>, Schedulable{
    
    /*
* Method name : start
* Description : Query all the Open Sales order based on the condition that gii__Released__c is set to false and giic_CCAuthorized__c, giic_AddressValidated__c, giic_TaxCal__c, giic_ShipFeeCal__c, giic_PromoCal__c are set to true.  
* Return Type : Database.QueryLocator
* Parameter : Database.BatchableContext BC
*/
    global Database.QueryLocator start(Database.BatchableContext BC){
        string query='Select Id, giic_SOStatus__c, gii__Released__c, gii__ShipToStateProvince__c, gii__PaymentMethod__c, giic_Source__c, gii__NetAmount__c, ' +
            '(SELECT Id, gii__LineStatus__c, gii__Product__r.Name, gii__Product__r.giic_ExceptionState__c FROM gii__SalesOrder__r),'+
            '(Select Id, gii__PaymentMethod__c, gii__PaidAmount__c from gii__SalesOrderPayments__r)' +
            'from gii__SalesOrder__c where CreatedDate < ' + Datetime.now().addMinutes(-30).formatGMT('yyyy-MM-dd\'T\'HH:mm:ss.SSSZ') +
            ' AND gii__Released__c = false AND giic_SOStatus__c = \'Open\' AND giic_ProcessingStatus__c != \'Processed with Error\' AND giic_CCAuthorized__c = true AND giic_AddressValidated__c = true AND giic_TaxCal__c = true AND giic_ShipFeeCal__c = true AND giic_PromoCal__c = true';
        return Database.getQueryLocator(query);
    } 
    /*
* Method name : execute
* Description : Process the Open Sales Order for allocation
* Return Type : nill
* Parameter : Database.BatchableContext BC, List<sObject> scope
*/
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        if(!scope.isEmpty()){
            Map<String , List<sObject>> returnMap = giic_SchedulerUtility.processUnPaidSO(scope);
           if(returnMap.containsKey('sopToUpdate') && !returnMap.get('sopToUpdate').isEmpty()) Database.update(returnMap.get('sopToUpdate'));
           if(returnMap.containsKey('lstErrors') && !returnMap.get('lstErrors').isEmpty()) Database.insert(returnMap.get('lstErrors'));
           if(returnMap.containsKey('validSO') && !returnMap.get('validSO').isEmpty()) giic_SalesOrderHelper.bulkReleaseSO(returnMap.get('validSO'));
        }
        
    }
    /*
* Method name : finish
* Description : Process any pending work after the batch complete
* Return Type : nill
* Parameter : Database.BatchableContext BC,
*/
    global void finish(Database.BatchableContext BC){
        
    }
    
    /*
* Method name : execute
* Description : Execute in schedulable context
* Return Type : null
* Parameter : SchedulableContext,
*/
    global void execute(SchedulableContext scx){
        Database.executeBatch(new giic_SOReleaseBatch());
        
    }
    
}