@isTest
private class AccountSalesSummaryCleanupBatchTest {

    static testMethod void testSalesSummaryCleanup(){
        integer numAccounts = 100;
        
        SetupTestData.createCustomSettings();
        SetupTestData.createAccounts(numAccounts);

        integer index = 0;
        for (Account a: SetupTestData.testAccounts){
            if (Math.mod(index,2) == 0) {
                // Even numbered index: Set Account Sales_Summarization_Completed flag to true
                a.Sales_Summarization_Completed__c = true;
            } else {
                // Odd numbered index: Set Account Sales_Summarization_Completed flag to false
                a.Sales_Summarization_Completed__c = false;
            }
            index ++;
        }
        
        update SetupTestData.testAccounts;

        test.startTest();
        Job_Failure_Notification_Email__c jfn = new Job_Failure_Notification_Email__c();
        jfn.Name = 'Email Id';
        jfn.Notification_Email__c = UserInfo.getUserEmail();
        insert jfn;
        AccountSalesSummaryCleanupBatch a = new AccountSalesSummaryCleanupBatch();
        Database.executeBatch(a, 2000);
        test.stopTest();
        
        system.debug([SELECT Id, Sales_Summarization_Completed__c FROM Account]);
        system.assertEquals([select count() from Account where Sales_Summarization_Completed__c = false], numAccounts);
        system.assertEquals([select count() from Account where Sales_Summarization_Completed__c = true], 0);
    }
}