/************************************************************************************
Version : 1.0
Created Date : 19 Oct 2018
Function : Process InventoryAdjustmentStaging
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/
global class giic_InventoryAdjustmentStagingBatch implements Database.Batchable<sObject>, Database.AllowsCallouts {
    
    
    /*
    * Method name : start
    * Description : Query InventoryAdjustmentStaging records
    * Return Type : Database.QueryLocator
    * Parameter : Database.BatchableContext BC
    */
    global Database.QueryLocator start(Database.BatchableContext BC){
        string query= 'select Id, giic_AdjustmentDate__c, giic_AdjustmentQuantity__c, giic_InvOverride__c, giic_Product__c, giic_Reason__c, giic_Status__c, giic_UnitofMeasure__c, giic_Warehouse__c from gii__InventoryAdjustmentStaging__c where giic_Status__c = \'' + system.label.giic_IAS_InitialStatus + '\'' ; 
        //System.debug('query for allocation is ::::' + query);
        return Database.getQueryLocator(query);
    } 
    /*
    * Method name : execute
    * Description : Process the Open Sales Order for allocation
    * Return Type : nill
    * Parameter : Database.BatchableContext BC, List<sObject> scope
    */
    global void execute(Database.BatchableContext BC, List<gii__InventoryAdjustmentStaging__c> scope){
        giic_InventoryAdjustmentStagingTrgHelper.OnAfterInsert(scope);
    }
    /*
    * Method name : finish
    * Description : Process any pending work after the batch complete
    * Return Type : nill
    * Parameter : Database.BatchableContext BC,
    */
    global void finish(Database.BatchableContext BC){
        
    }
}