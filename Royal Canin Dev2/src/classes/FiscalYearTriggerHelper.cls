/*
 * Date:       		 - Developer, Company					       - Description
 * 2015-05-04        - Mayank Srivastava, Acumen Solutions         - Created
 * 2015-08-05		 - Joseph Wortman, Acumen Solutions			   - Edited
 */

public with sharing class FiscalYearTriggerHelper {

	public static void checkActiveFYs(List<Fiscal_Year__c> newValues, Map<Id,Fiscal_Year__c> newMap) {
		Map<Id,Fiscal_Year__c> existingFiscalYears = new Map<Id,Fiscal_Year__c>([Select id, isActive__c from Fiscal_Year__c]);
		Fiscal_Year__c activeFY = null;
		for(Fiscal_Year__c fy : existingFiscalYears.values()){
			if(fy.isActive__c){
				activeFY = fy;
			}
		}
		System.debug('activeFy '+activeFy);
		for(Fiscal_Year__c fy: newValues){
			if(fy.isActive__c && activeFy == null){
				activeFY = fy;
			}
			else if(fy.isActive__c && activeFy != null && fy.id != activeFy.id) {
				fy.addError('You cannot have more than one active Fical Year. Please uncheck isActive Flag for an existing Fiscal Year');		
			}
		}
	}

	public static void setActivationDefaultDate(List<Fiscal_Year__c> newList){
		for(Fiscal_Year__c fy : newList){
			fy.Activation_Date__c = fy.Start_Date__c;
		}
	}
}