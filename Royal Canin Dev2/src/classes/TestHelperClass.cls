/*
 * Date:             - Developer, Company                          - Description
 * 2015-05-04        - Solution Accelerator, Acumen Solutions      - Acumen TM Accelerator
 * 2015-05-04        - Mayank Srivastava, Acumen Solutions         - Added methods for RC specific scenarios
 */
public class TestHelperClass 
{
    public static final String ACCOUNT_NAME = 'Unit Test Acct';
    public static final String OTHER_DISPOSITION = 'X - Other';
    public static final String KEEP_CURRENT_DISPOSITION = 'KP - Keep Current Disposition';
    
    
    //------------------------------------------------------------------------------------------------------------------------------
    //createUser()
    //------------------------------------------------------------------------------------------------------------------------------
    public static User createUser(Integer i) 
    { 
        //Profile p = [select id from profile where name='ADT NA Resale Sales Manager'];
        Profile p = [select id from profile where name='System Administrator'];
        
        User u = new User(alias = 'stand', email='standarduser@testorg.com',
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
            localesidkey='en_US', profileid = p.Id,
            timezonesidkey='America/Los_Angeles', username='testing'+i+'@adt.com');
        
        insert u;
        return u;
    }
    
    //------------------------------------------------------------------------------------------------------------------------------
    //createSalesRepUser()
    //------------------------------------------------------------------------------------------------------------------------------
    //public static User createSalesRepUser() {
    //    Profile p1 = [select id from profile where name='ADT NA Resale Sales Representative'];
    //    UserRole urExisting = [select id, OpportunityAccessForAccountOwner from UserRole limit 1];
    //    UserRole urMgr = new UserRole(name = 'Test Sales Mgr', OpportunityAccessForAccountOwner = urExisting.OpportunityAccessForAccountOwner);
    //    insert urMgr;
    //    UserRole urNew = new UserRole(name = 'Test Sales Rep', OpportunityAccessForAccountOwner = urExisting.OpportunityAccessForAccountOwner, parentRoleId = urMgr.Id);
    //    insert urNew; 
         
    //    User salesRep = new User(alias = 'utsr1', email='unitTestSalesRep1@testorg.com', 
    //            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
    //            localesidkey='en_US', profileid = p1.Id, userroleid = urNew.Id,
    //            timezonesidkey='America/Los_Angeles', username='unitTestSalesRep1@testorg.com');
    //    insert salesRep;
    //    return salesRep;
    //}

    //------------------------------------------------------------------------------------------------------------------------------
    //createAdminUser()
    //------------------------------------------------------------------------------------------------------------------------------
    public static User createAdminUser() {
        Profile p1 = [select id from profile where name='System Administrator'];
        UserRole urExisting = [select id, OpportunityAccessForAccountOwner from UserRole where name like '%SVP_Customer_Experience%' limit 1];
        User admin = new User(alias = 'utsr1', email='unitTestSalesRep1@testorg.com', 
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                localesidkey='en_US', profileid = p1.Id, userroleid = urExisting.Id,
                timezonesidkey='America/Los_Angeles', username='unitTestSalesRep1@testorg.com');    
        insert admin;
        return admin;
    }
    
    
     //------------------------------------------------------------------------------------------------------------------------------
    //createManagerUser()
    //------------------------------------------------------------------------------------------------------------------------------
    //public static User createManagerUser() {
    //    Profile p1 = [select id from profile where name='ADT NA Resale Sales Manager']; 
    //    User salesMgr = new User(alias = 'utsm1', email='unitTestSalesMgr1@testorg.com', 
    //        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
    //        localesidkey='en_US', profileid = p1.Id, 
    //        timezonesidkey='America/Los_Angeles', username='unitTestSalesMgr1@testorg.com');
    //    insert salesMgr;
    //    return salesMgr;
    //}
    
     //------------------------------------------------------------------------------------------------------------------------------
    //createSalesRepUserWithManager()
    //------------------------------------------------------------------------------------------------------------------------------
    public static User createSalesRepUserWithManager() {
        Profile p1 = [select id from profile where name='System Administrator'];
        UserRole urExisting = [select id, OpportunityAccessForAccountOwner from UserRole  limit 1];
        UserRole urMgr = new UserRole(name = 'Test Sales Mgr', OpportunityAccessForAccountOwner = urExisting.OpportunityAccessForAccountOwner);
        insert urMgr;
        UserRole urNew = new UserRole(name = 'Test Sales Rep', OpportunityAccessForAccountOwner = urExisting.OpportunityAccessForAccountOwner, parentRoleId = urMgr.Id);
        insert urNew; 
         
        User salesRep = new User(alias = 'utsr2', email='unitTestSalesRep2@testorg.com', 
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                localesidkey='en_US', profileid = p1.Id, userroleid = urNew.Id,
                timezonesidkey='America/Los_Angeles', username='unitTestSalesRep2@testorg.com');
        insert salesRep;
        
        Profile p2 = [select id from profile where name='System Administrator']; 
        User salesMgr = new User(alias = 'utsm2', email='unitTestSalesMgr2@testorg.com', 
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
            localesidkey='en_US', profileid = p2.Id, userroleid = urMgr.Id, 
            timezonesidkey='America/Los_Angeles', username='unitTestSalesMgr2@testorg.com');
        insert salesMgr;
        
        return salesRep;
    }
    
    //------------------------------------------------------------------------------------------------------------------------------
    //createManagerWithTeam()
    //------------------------------------------------------------------------------------------------------------------------------
    //public static User createManagerWithTeam() {
    //    Profile p1 = [select id from profile where name='System Administrator'];
    //    UserRole urExisting = [select id, OpportunityAccessForAccountOwner from UserRole limit 1];
    //    UserRole urMgr = new UserRole(name = 'Test Sales Mgr', OpportunityAccessForAccountOwner = urExisting.OpportunityAccessForAccountOwner);
    //    insert urMgr;
    //    UserRole urNew = new UserRole(name = 'Test Sales Rep', OpportunityAccessForAccountOwner = urExisting.OpportunityAccessForAccountOwner, parentRoleId = urMgr.Id);
    //    insert urNew; 
         
    //    User salesRep = new User(alias = 'utsr3', email='unitTestSalesRep3@testorg.com', 
    //            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
    //            localesidkey='en_US', profileid = p1.Id, userroleid = urNew.Id,
    //            timezonesidkey='America/Los_Angeles', username='unitTestSalesRep3@testorg.com');
    //    insert salesRep;
        
    //    Profile p2 = [select id from profile where name='System Administrator']; 
    //    User salesMgr = new User(alias = 'utsm3', email='unitTestSalesMgr3@testorg.com', 
    //        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
    //        localesidkey='en_US', profileid = p2.Id, userroleid = urMgr.Id, 
    //        timezonesidkey='America/Los_Angeles', username='unitTestSalesMgr3@testorg.com');
    //    insert salesMgr;
        
    //    return salesMgr;
    //}
    
    //------------------------------------------------------------------------------------------------------------------------------
    // CreatePostalCode()
    //------------------------------------------------------------------------------------------------------------------------------
    public static List<PostalCode__c> createPostalCodes() {
        PostalCode__c pc = new PostalCode__c (name='12345', Pillar__c = 'VET',ZipCode__c = '12345', Channel__c = 'VET');
        List<PostalCode__c> pcs = new List<PostalCode__c>();
        pcs.add(pc);
        pc = new PostalCode__c (name='12346', TownId__c='Test123', Town__c='Test Town 123', Area__c='Test Area 123', BusinessId__c='1100');
        pcs.add(pc);
        insert pcs;
        return pcs;
    }
    
    
    //------------------------------------------------------------------------------------------------------------------------------
    // inferPostalCodeID()
    //------------------------------------------------------------------------------------------------------------------------------
    
    //public static Id inferPostalCodeID(String postalCode, String businessID) {
        
    //    Id returnValue = null;
    //    try {
    //        PostalCode__c pc = [select id, name, businessId__c from PostalCode__c where name = :postalCode and businessId__c = :businessID];
        
    //        if (pc != null) {
    //            returnValue = pc.Id;
    //        }
    //    }
    //    catch (Exception e) {
    //        PostalCode__c newPC = new PostalCode__c ( name = postalCode, businessId__c = businessID);
            
    //        insert newPC;
            
    //        returnValue = newPC.Id;
                
    //    }
        
    //    return returnValue;
    //}

    public static Fiscal_Year__c createFiscalYears(){
        Fiscal_Year__c fy = new Fiscal_Year__c(isActive__c = TRUE, Start_Date__c= Date.today(), Name='2015');
        insert fy;
        return fy;
    }

    public static Region__c createRegion(User managerUser){
        Region__c rg = new Region__c(Pillar__c = 'VET',Ownerid = managerUser.id, User__c = managerUser.id, Name = 'RegName');
        insert rg;
        return rg;
    }

    public static Territory__c createTerritory(Id reg){
        Territory__c terr = new Territory__c(Name='Midwest' , Region__c=reg);
        insert terr;
        return terr;
    }

    public static TerritoryAssignment__c createTerrirtoryAssignment(Id postalId, Id terr, Id fyid){
        TerritoryAssignment__c ta = new TerritoryAssignment__c(PostalCodeID__c = postalId,TerritoryID__c  = terr, sync_needed__c = true, Fiscal_Year__c = fyid);
        insert ta;
        return ta;
    }

    public static Account createAccountData(Id postalid){
    	setupTestData.createAccounts(2);
        setupTestData.testAccounts[0].PostalCodeID__c =  postalid;
        update setupTestData.testAccounts[0];
        return setupTestData.testAccounts[0];
    }
}