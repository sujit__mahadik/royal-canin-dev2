/**********************************************************
* Purpose    : This is handler class to handle giic_ProductUnitofMeasureConversionTrigger trigger.  
*              
* *******************************************************
* Author                        Date            Remarks
*                               18/10/2018     
*********************************************************/
public class giic_ProductUnitofMeasureConversionHlpr {

    map<id,gii__Product2Add__c> mapproduct = new map<id,gii__Product2Add__c>();
    List<giic_UomCommonUtility.UoMConversion> lstUpdateUom = new List<giic_UomCommonUtility.UoMConversion>();
    
    public void processProductUnitOfMeasureRecords(set<id> productids){
        
        mapproduct = new map<id,gii__Product2Add__c>([select id,gii__BuyingUnitofMeasure__c,gii__SellingUnitofMeasure__c,gii__StockingUnitofMeasure__c,giic_ConvFactor__c,giic_RConvFactor__c from gii__Product2Add__c where id IN :productids]);
        // code to generate wrapper list which will be passed to giic_UomCommonUtility  
        lstUpdateUom = returnUoMConversionList(mapproduct);
        // Convert Uom Method MAP<ProductId UoMSourceId UoMTargetId,Conversion Factor>
        map<string,decimal> mapReturn = giic_UomCommonUtility.ConvertUoMBulk(lstUpdateUoM);
    
        // code to update Product fields
        for(Id ProductId : mapproduct.keyset())
        {
            // code to get product record
            gii__Product2Add__c temprec = mapproduct.get(ProductId);
            
            // code to get product fields needs to be update
            if(mapReturn.containskey(giic_UomCommonUtility.createProductUoMUniqueKey(temprec.id, temprec.gii__SellingUnitofMeasure__c, temprec.gii__StockingUnitofMeasure__c)))
            {
                decimal ConversionFactor = mapReturn.get(temprec.id+' '+temprec.gii__SellingUnitofMeasure__c+' '+temprec.gii__StockingUnitofMeasure__c);
                temprec.giic_ConvFactor__c = ConversionFactor;
                //temprec.giic_RConvFactor__c = 1/ConversionFactor;
            }
            if(mapReturn.containskey(giic_UomCommonUtility.createProductUoMUniqueKey(temprec.id, temprec.gii__StockingUnitofMeasure__c, temprec.gii__SellingUnitofMeasure__c)))
            {
                decimal ConversionFactor = mapReturn.get(temprec.id+' '+temprec.gii__StockingUnitofMeasure__c+' '+temprec.gii__SellingUnitofMeasure__c);
                temprec.giic_RConvFactor__c = ConversionFactor;
            }
        }
        
        database.update(mapproduct.values());
    }
    
    public List<giic_UomCommonUtility.UoMConversion> returnUoMConversionList(map<id,gii__Product2Add__c> mapproduct){
        List<giic_UomCommonUtility.UoMConversion> lstToUpdateUom = new List<giic_UomCommonUtility.UoMConversion>();
        // code to generate wrapper list which will be passed to giic_UomCommonUtility  
        for(gii__Product2Add__c Prod : mapproduct.values())
        {
            giic_UomCommonUtility.UoMConversion wrapperobj = new giic_UomCommonUtility.UoMConversion();
            if(((!string.isEmpty(Prod.gii__SellingUnitofMeasure__c)) && (!string.isEmpty(Prod.gii__StockingUnitofMeasure__c))))
            {
                wrapperobj = new giic_UomCommonUtility.UoMConversion();        
                wrapperobj.SourceUoMId = Prod.gii__SellingUnitofMeasure__c;
                wrapperobj.TargetUoMId = Prod.gii__StockingUnitofMeasure__c;
                wrapperobj.ProductId = Prod.Id;
                wrapperobj.qtyNeedToBeConvert = 1;      
                lstToUpdateUom.add(wrapperobj);
            }
        }
        return lstToUpdateUom;
    }
}