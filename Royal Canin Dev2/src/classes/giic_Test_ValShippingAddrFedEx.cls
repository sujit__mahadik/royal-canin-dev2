/************************************************************************************
Version : 1.0
Created Date : 10-Sept-2018
Function : Server side controller for giic_SalesOrderPathStatusController.cmp
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/

@isTest 
private class giic_Test_ValShippingAddrFedEx {
    
   //public static List<giic_FedExAddressValidation.AddressToValidate> lstAddressToValidate = new List<giic_FedExAddressValidation.AddressToValidate>();
   //public static giic_FedExAddressValidation.AddressValidationReply response = new giic_FedExAddressValidation.AddressValidationReply();
    
    /*@Param: NA
    * @Return: NA
    * @Description: @testSetup method to create all test data which are going to be used in test methods.
    *               Use giic_Test_DataCreationUtility for data creation.
    */
    
    @testSetup static void setup(){
        giic_Test_DataCreationUtility.CreateAdminUser(); 
        List<Account> testConsumerAccountList = giic_Test_DataCreationUtility.insertConsumerAccount();
        List<gii__Warehouse__c> testWareList = giic_Test_DataCreationUtility.insertWarehouse();
        SetupTestData.createCustomSettings();
        giic_Test_DataCreationUtility.testSystemPolicyCreation2();
        List<gii__AccountAdd__c> testAccRefList = giic_Test_DataCreationUtility.getAccountReference(testConsumerAccountList);
        Database.insert(testAccRefList);
        List<gii__Product2Add__c> testProductList = giic_Test_DataCreationUtility.insertProduct();
        List<gii__SalesOrder__c> testSOList = giic_Test_DataCreationUtility.insertSalesOrderUnreleased(testConsumerAccountList[0].Id, testAccRefList[0].Id);
        List<gii__SalesOrderStaging__c> testSOStaging = giic_Test_DataCreationUtility.insertSalesOrderStaging();
        List<gii__SalesOrderLineStaging__c> testSOLStaging = giic_Test_DataCreationUtility.insertSalesOrderLineStaging();
        /*testSOList.get(0).gii__ShipToStreet__c = '9 CLEMENT DR';
        testSOList.get(0).gii__ShipToCity__c = 'SOMERDALE';
        testSOList.get(0).gii__ShipToStateProvince__c = 'NJ'; 
        testSOList.get(0).gii__ShipToZipPostalCode__c = '08083';
        testSOList.get(0).gii__ShipToCountry__c = 'United States';
        Database.update(testSOList[0]);*/
    }
    
    /*@Param: NA
    * @Return: NA
    * @Description: Cover all the wrapper classes used in the main class.
    */
    @isTest private static void coverTypes(){
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        System.Runas(u)  
        {

        new giic_ValShippingAddrFedExController.AddressToValidateWrapper();
        new giic_ValShippingAddrFedExController.Address();
        new giic_ValShippingAddrFedExController.AddressAttribute();
        new giic_ValShippingAddrFedExController.AddressValidationResult();
        new giic_ValShippingAddrFedExController.AddressValidation();
        new giic_ValShippingAddrFedExController.sObjFieldLabelWrapper();
        new giic_ValShippingAddrFedExController.sObjFieldValueWrapper();
        //Cover stub wrappers
        new giic_FedExAddressValidation.NotificationParameter();
        new giic_FedExAddressValidation.Notification();
        new giic_FedExAddressValidation.Contact();
        new giic_FedExAddressValidation.ParsedAddressPartsDetail();
        new giic_FedExAddressValidation.ParsedStreetLineDetail();
        new giic_FedExAddressValidation.TransactionDetail();
        new giic_FedExAddressValidation.Localization();
        new giic_FedExAddressValidation.ParsedPostalCodeDetail();
    }
    }
    /*@Param: NA
    * @Return: NA
    * @Description: test method for getFieldLabels and getFieldValues
    */
    @isTest private static void fieldAndLabelTest(){
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        System.Runas(u)  
        {
        Test.startTest();
        
        Account accObj = [SELECT Id, ShippingStreet, ShippingCountry,
                          ShippingPostalCode,ShippingState,ShippingCity
                          FROM Account LIMIT 1];
        gii__SalesOrder__c soObj = [SELECT Id, gii__ShipToStreet__c, gii__ShipToCity__c, 
                                    gii__ShipToStateProvince__c, gii__ShipToZipPostalCode__c,
                                    gii__ShipToCountry__c,
                                    gii__Account__c 
                                    FROM gii__SalesOrder__c 
                                    WHERE gii__Account__c =: accObj.Id 
                                    LIMIT 1];
        gii__SalesOrderStaging__c testSOS = [SELECT Id, giic_ShipToCity__c, 
                              giic_ShipToStateProvince__c, giic_ShipToStreet__c, 
                              giic_ShipToZipPostalCode__c, giic_ShipToCountry__c,
                              giic_AddressType__c, giic_AddressValidated__c
                              FROM gii__SalesOrderStaging__c
                              LIMIT 1];
        
        giic_ValShippingAddrFedExController.getFieldValues(accObj.Id); //For Account
        giic_ValShippingAddrFedExController.getFieldValues(soObj.Id);  //For Sales Order
        giic_ValShippingAddrFedExController.getFieldValues(testSOS.Id);//For Sales Order Staging
        
        giic_ValShippingAddrFedExController.getFieldLabels(accObj.Id); //For Account
        giic_ValShippingAddrFedExController.getFieldLabels(soObj.Id);  //For Sales Order
        giic_ValShippingAddrFedExController.getFieldLabels(testSOS.Id); //For Sales Order Staging
        
        Test.stopTest();
    }
    }
    
    /*@Param: NA
    * @Return: NA
    * @Description: test method for getFieldLabels and getFieldValues
    */
    @isTest private static void testWebAuthentication(){
         User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        System.Runas(u)  
        {
        Test.startTest();
        giic_FedExAddressValidation.WebAuthenticationDetail authentication = new giic_FedExAddressValidation.WebAuthenticationDetail();
        authentication = giic_ValShippingAddrFedExController.webAuthentication();
        Test.stopTest();
        System.assertNotEquals(null, authentication);
    }
    }
    @isTest private static void testProcessAddressToValidate(){
         User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
         System.debug('Object msg first debug::::::::::');
        System.Runas(u)  
        {
        List<giic_ValShippingAddrFedExController.AddressToValidateWrapper> lstAddress = new List<giic_ValShippingAddrFedExController.AddressToValidateWrapper>();
        giic_ValShippingAddrFedExController.AddressToValidateWrapper address = new giic_ValShippingAddrFedExController.AddressToValidateWrapper();
        Test.startTest();
        Account accObj = [SELECT Id, ShippingStreet, ShippingCountry,
                          ShippingPostalCode,ShippingState,ShippingCity
                          FROM Account LIMIT 1];
        gii__SalesOrder__c soObj = [SELECT Id, gii__ShipToStreet__c, gii__ShipToCity__c, 
                                    gii__ShipToStateProvince__c, gii__ShipToZipPostalCode__c,
                                    gii__ShipToCountry__c,
                                    gii__Account__c 
                                    FROM gii__SalesOrder__c 
                                    WHERE gii__Account__c =: accObj.Id 
                                    LIMIT 1];
        System.debug('Object msg second debug::::::::::');
        address.recordId = soObj.Id;
        address.ClientReferenceId = '';
        address.StreetLines = soObj.gii__ShipToStreet__c;
        address.City= soObj.gii__ShipToCity__c;
        address.PostalCode=soObj.gii__ShipToZipPostalCode__c;
        address.CountryName=soObj.gii__ShipToCountry__c;
        lstAddress.add(address);
        Test.setMock(WebServiceMock.class, new giic_Test_FedExCalloutNegativeMock());
        giic_ValShippingAddrFedExController.initAddressToValidate(JSON.serialize(lstAddress), soObj.Id);
        giic_FedExAddressValidation.AddressValidationReply response = giic_ValShippingAddrFedExController.processAddressToValidate(lstAddress);
        Map<Id, String> mpAddresssType = new Map<Id, String>();
        giic_ValShippingAddrFedExController.validateAddress(response, lstAddress, mpAddresssType);
        Test.stopTest();
    }
    }
    @isTest private static void testInitAddressToValidate(){
         User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        System.Runas(u)  
        {
        Test.startTest();
        Account accObj = [SELECT Id, ShippingStreet, ShippingCountry,
                          ShippingPostalCode,ShippingState,ShippingCity
                          FROM Account LIMIT 1];
        gii__SalesOrder__c soObj = [SELECT Id, gii__ShipToStreet__c, gii__ShipToCity__c, 
                                    gii__ShipToStateProvince__c, gii__ShipToZipPostalCode__c,
                                    gii__ShipToCountry__c,
                                    gii__Account__c 
                                    FROM gii__SalesOrder__c 
                                    WHERE gii__Account__c =: accObj.Id 
                                    LIMIT 1];
        
        List<giic_ValShippingAddrFedExController.AddressToValidateWrapper> addrValWrapList = new List<giic_ValShippingAddrFedExController.AddressToValidateWrapper>();
        giic_ValShippingAddrFedExController.AddressToValidateWrapper addrToVal = new giic_ValShippingAddrFedExController.AddressToValidateWrapper();
        addrToVal.recordId = soObj.Id;
        addrToVal.ClientReferenceId = '';
        addrToVal.StreetLines = soObj.gii__ShipToStreet__c;
        addrToVal.City = soObj.gii__ShipToCity__c;
        addrToVal.PostalCode = soObj.gii__ShipToZipPostalCode__c;
        addrToVal.CountryName = soObj.gii__ShipToCountry__c;
        addrValWrapList.add(addrToVal);
        //String str = ;
        Test.setMock(WebServiceMock.class, new giic_Test_FedExCalloutMock());
        giic_ValShippingAddrFedExController.initAddressToValidate(JSON.serialize(addrValWrapList), soObj.Id);
        giic_FedExAddressValidation.AddressValidationReply response = giic_ValShippingAddrFedExController.processAddressToValidate(addrValWrapList);
        Map<Id, String> mpAddresssType = new Map<Id, String>();
        giic_ValShippingAddrFedExController.validateAddress(response, addrValWrapList, mpAddresssType);
        Test.stopTest();
    }
    }
    @isTest private static void  testUpdateRecordSO(){
         User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        System.Runas(u)  
        {
        Test.startTest();
        
        Account accObj = [SELECT Id, ShippingStreet, ShippingCountry,
                          ShippingPostalCode,ShippingState,ShippingCity,
                          giic_AddressType__c
                          FROM Account LIMIT 1];
        gii__SalesOrder__c soObj = [SELECT Id, gii__ShipToStreet__c, gii__ShipToCity__c, 
                                    gii__ShipToStateProvince__c, gii__ShipToZipPostalCode__c,
                                    gii__ShipToCountry__c,giic_AddressValidated__c,
                                    gii__Account__c, giic_AddressType__c 
                                    FROM gii__SalesOrder__c 
                                    WHERE gii__Account__c =: accObj.Id 
                                    LIMIT 1];
        list<giic_ValShippingAddrFedExController.Address> listAddressToUpdate = new  list<giic_ValShippingAddrFedExController.Address>();
        giic_ValShippingAddrFedExController.Address address = new giic_ValShippingAddrFedExController.Address();
        address.StreetLines = soObj.gii__ShipToStreet__c;
        address.City = soObj.gii__ShipToCity__c;
        address.StateOrProvinceCode = soObj.gii__ShipToStateProvince__c;
        address.PostalCode = soObj.gii__ShipToZipPostalCode__c;
        address.CountryCode = soObj.gii__ShipToCountry__c;
        address.Classification = 'BUSINESS';
        listAddressToUpdate.add(address);
        
        giic_ValShippingAddrFedExController.updateRecord(JSON.serialize(listAddressToUpdate),soObj.Id);
        
        giic_ValShippingAddrFedExController.updateRecord(JSON.serialize(listAddressToUpdate),null);
        
        Test.stopTest();      
    }
    }
    
    @isTest private static void  testUpdateRecordAccount(){
         User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        System.Runas(u)  
        {
        Test.startTest();
        
        Account accObj = [SELECT Id, ShippingStreet, ShippingCountry,
                          ShippingPostalCode,ShippingState,ShippingCity,
                          giic_AddressType__c
                          FROM Account LIMIT 1];
        
        list<giic_ValShippingAddrFedExController.Address> listAddressToUpdate = new  list<giic_ValShippingAddrFedExController.Address>();
        giic_ValShippingAddrFedExController.Address address = new giic_ValShippingAddrFedExController.Address();
        address.StreetLines = accObj.ShippingStreet;
        address.City = accObj.ShippingCity;
        address.StateOrProvinceCode = accObj.ShippingState;
        address.PostalCode = accObj.ShippingPostalCode;
        address.CountryCode = accObj.ShippingCountry;
        address.Classification = 'BUSINESS';
        listAddressToUpdate.add(address);
        
        giic_ValShippingAddrFedExController.updateRecord(JSON.serialize(listAddressToUpdate),accObj.Id);
               
        //giic_ValShippingAddrFedExController.updateRecord(JSON.serialize(listAddressToUpdate),null);
        
        Test.stopTest();      
    }
    }
    @isTest private static void testUpdateRecordSOS(){
         User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        System.Runas(u)  
        {
        test.startTest();
        gii__SalesOrderStaging__c testSOS = [SELECT Id, giic_ShipToCity__c, 
                              giic_ShipToStateProvince__c, giic_ShipToStreet__c, 
                              giic_ShipToZipPostalCode__c, giic_ShipToCountry__c,
                              giic_AddressType__c, giic_AddressValidated__c
                              FROM gii__SalesOrderStaging__c
                              LIMIT 1];
        list<giic_ValShippingAddrFedExController.Address> listAddressToUpdate = new  list<giic_ValShippingAddrFedExController.Address>();
        giic_ValShippingAddrFedExController.Address address = new giic_ValShippingAddrFedExController.Address();
        address.StreetLines = testSOS.giic_ShipToStreet__c;
        address.City = testSOS.giic_ShipToCity__c;
        address.StateOrProvinceCode = testSOS.giic_ShipToStateProvince__c;
        address.PostalCode = testSOS.giic_ShipToZipPostalCode__c;
        address.CountryCode = testSOS.giic_ShipToCountry__c;
        address.Classification = 'BUSINESS';
        listAddressToUpdate.add(address);
        
        giic_ValShippingAddrFedExController.updateRecord(JSON.serialize(listAddressToUpdate),testSOS.Id);
        test.stopTest();
    }
    }
    
    @isTest private static void testCalTax(){
         User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        System.Runas(u)  
        {
        test.startTest();
        Account accObj = [SELECT Id, Check_if_you_are_Tax_Exempt__c, Resale_Sales_Tax_Cert_Expiration_Date__c
                          FROM Account LIMIT 1];
        accObj.Check_if_you_are_Tax_Exempt__c = false;
        accObj.Resale_Sales_Tax_Cert_Expiration_Date__c = date.newInstance(2018, 3, 21);
        
        update accObj;
        
        gii__SalesOrder__c soObj = [SELECT Id, gii__ShipToStreet__c, gii__ShipToCity__c, 
                                    gii__ShipToStateProvince__c, gii__ShipToZipPostalCode__c,
                                    gii__ShipToCountry__c,
                                    gii__Account__c 
                                    FROM gii__SalesOrder__c 
                                    WHERE gii__Account__c =: accObj.Id 
                                    LIMIT 1];
                                    
        giic_ValShippingAddrFedExController.CalculateTax(soObj.Id);
        
        test.stopTest();
    }
    }

}