/************************************************************************************
Version : 1.0
Created Date : 27-Aug-2018
Function : Test class for giic_SOLAllocationController
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/
@isTest
private class giic_Test_SOLAllocationController {
	@testSetup
    static void setup(){
		List<Account> testConsumerAccountList = giic_Test_DataCreationUtility.insertConsumerAccount();
        List<gii__Warehouse__c> testWareList = giic_Test_DataCreationUtility.insertWarehouse();
        SetupTestData.createCustomSettings();
        giic_Test_DataCreationUtility.testSystemPolicyCreation();
        List<gii__AccountAdd__c> testAccRefList = giic_Test_DataCreationUtility.getAccountReference(testConsumerAccountList);
        Database.insert(testAccRefList);
        List<gii__Product2Add__c> testProductList = giic_Test_DataCreationUtility.insertProduct();
        List<gii__SalesOrder__c> testSOList = giic_Test_DataCreationUtility.insertSalesOrder(testConsumerAccountList[0].Id, testAccRefList[0].Id);
        List<gii__SalesOrderLine__c> testSOLine = giic_Test_DataCreationUtility.insertSOLine(testSOList[0].Id, testProductList[0].Id);
        giic_Test_DataCreationUtility.CreateAdminUser();
        
    }
	private static testMethod void positiveTest() {
	    User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
	    System.runAs(u){
            Test.startTest();
            List<gii__SalesOrderLine__c> testSOLine=[select id from gii__SalesOrderLine__c];
            ApexPages.StandardController stdController = new ApexPages.StandardController(testSOLine[0]);
            giic_SOLAllocationController objSolcontroller = new giic_SOLAllocationController(stdController);
       	    objSolcontroller.updateAllocation();
            Test.stopTest();
	    }
	}
	private static testMethod void negativeTest() {
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
	    System.runAs(u){
        Test.startTest();
        list<account> testConsumerAccountList=[select id from account];
        testConsumerAccountList[0].Blocked__c = true;
        update testConsumerAccountList[0];
        List<gii__SalesOrderLine__c> testSOLine=[select id from gii__SalesOrderLine__c];
        ApexPages.StandardController stdController = new ApexPages.StandardController(testSOLine[0]);
        giic_SOLAllocationController objSolcontroller = new giic_SOLAllocationController(stdController);
        objSolcontroller.updateAllocation();
        objSolcontroller.objSOL=null;
        objSolcontroller.updateAllocation();
        Test.stopTest();
	    }
	}

}