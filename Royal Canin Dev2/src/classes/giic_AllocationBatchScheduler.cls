/************************************************************************************
Version : 1.0
Created Date : 13 Aug 2018
Function : Used to schedule the allocation batch from system policy
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/
public class giic_AllocationBatchScheduler{
    
    public gii__SystemPolicy__c objSystemPolicy;
    
    public giic_AllocationBatchScheduler(ApexPages.StandardController stdController){
        objSystemPolicy=(gii__SystemPolicy__c)stdController.getRecord();
    }
    
    
    /*
    * Method name : scheduleBatch
    * Description : Schedule batches after checking for duplicates.
    * Return Type : PageReference
    * Parameter : 
    */
    public void scheduleBatch(){
        
        try{
            List<gii__SystemPolicy__c> Sp = new List<gii__SystemPolicy__c>([Select Id, giic_JobTime__c from gii__SystemPolicy__c where Id= :objSystemPolicy.Id]);
            if(!Sp.isEmpty() &&  Sp[0].giic_JobTime__c!=null){ // There can only be single record in system policy.
                giic_SchedulerUtility.scheduleBatchJobs(Integer.valueOf(Sp[0].giic_JobTime__c), new giic_AllocationBatch(), System.Label.giic_AllocationBatchName, true);
            }else{
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, Label.giic_EnterBatchJobTimeErrror));
            }
        }catch(Exception e){
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR,e.getMessage()));
        }
    }
}