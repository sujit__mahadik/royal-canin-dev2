/************************************************************************************
Version : 1.0
Name : giic_SalesOrderListViewController
Created Date : 16 Aug 2018
Function : Convert the selected Sales Order Staging Records to Sales Orders 
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/
public class giic_SalesOrderListViewController {
    public List<gii__SalesOrderStaging__c> lstSOS{get; set;}
    public List<gii__SalesOrderStaging__c> lstSOSErrors{get; set;}
    public String sObjectApiName{get; set;}
    public Boolean isError{get; set;}
    public String sosId{get; set;}
    
    public giic_SalesOrderListViewController(ApexPages.StandardSetController controller) {
        lstSOS = (List<gii__SalesOrderStaging__c>) controller.getSelected();
        lstSOSErrors = new List<gii__SalesOrderStaging__c>();
        sObjectApiName = 'gii__SalesOrderStaging__c';
        isError = false;
    }
    
    public void onLoad(){
        set<string> setOfSOSId = new set<string>();
        List<Integration_Error_Log__c> lstErrorLog = new List<Integration_Error_Log__c>();
        set<Id> setSOSId = new set<Id>();
        try{
            if(!lstSOS.isEmpty()){
                giic_RecursiveTriggerHandler.isFirstTime = true; // dont run trigger on update call
                //prepare setting accourding to custom metadata
                Map<String, giic_IntegrationCustomMetaDataWrapper> mapIntegrationSettings = giic_IntegrationCustomMetaDataUtil.getIntegrationSettings(sObjectApiName);
    
                //Get Paraent Records
                giic_IntegrationCustomMetaDataWrapper cmdObj = mapIntegrationSettings.get(sObjectApiName); 
                String soql = 'Select Name, ' + cmdObj.strSoql.subString(0, cmdObj.strSoql.length()-1) + ' from ' + cmdObj.sourceSobjectApi + ' where giic_Status__c !=  \''+ System.Label.giic_Converted + '\' and id IN :lstSOS';
                List<SObject> lstSourceObject = Database.query(soql);
                System.debug(':::lstSourceObject=' + lstSourceObject); // imp
                if(lstSourceObject.isEmpty()){ ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, System.Label.giic_ErrorMsgConverted)); return;}
                
                //DeActivate Errors
                Set<Id> sosIds = new Set<Id>();
                for(gii__SalesOrderStaging__c sos :lstSOS){sosIds.add(sos.Id);}
                giic_IntegrationCustomMetaDataUtil.deActivateCurrentError(sosIds);
                
                // address validation removed
                //Validate Address
             /*   Map<Id,giic_ValShippingAddrFedExController.AddressValidation> mapResult = giic_IntegrationCommonUtil.validateAddress((List<gii__SalesOrderStaging__c>) lstSource);
                
                // Remove error recordes from conversion process
                List<SObject> lstSourceObject = new List<SObject>();
                Boolean isValidAddress = true;
                for(SObject obj : lstSource){
                    if(!mapResult.get(obj.id).isValidAddress == true){
                        isValidAddress = false;
                        lstSOSErrors.add((gii__SalesOrderStaging__c) obj);
                    }else lstSourceObject.add(obj);
                }
                System.debug(':::isValidAddress=' + isValidAddress);  
                System.debug(':::lstSourceObject.size()=' + lstSourceObject.size());
                if(lstSourceObject.isEmpty()){ ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, System.Label.giic_ErrorMsgConverted)); return;}*/
                
                //Convert selected sales order staging records to sales orders
                Set<String> setErrorIds = new Set<String>();
                Set<String> setSOSIds = new Set<String>();
                Map<String, giic_IntegrationCMDResultWrapper> mpNewSObjectId = giic_IntegrationCustomMetaDataUtil.doConversion(sObjectApiName, mapIntegrationSettings, lstSourceObject, setErrorIds);
                
                map<string, Integer> mapSOSLineCount = new map<string, Integer>();
                for(gii__SalesOrderStaging__c objSOS :(List<gii__SalesOrderStaging__c>) lstSourceObject){
                    if(string.isNotEmpty((''+objSOS.id))){
                        setOfSOSId.add(''+objSOS.id);
                    }
                }
                if(setOfSOSId != null && setOfSOSId.size() > 0 ){
                    List<gii__SalesOrderStaging__c> listOfSOS = [select id, (select id from SalesOrderLineStagings__r) from gii__SalesOrderStaging__c where id in : setOfSOSId];
                    if(listOfSOS != null && listOfSOS.size() > 0){
                        for( gii__SalesOrderStaging__c obSOS : listOfSOS ){
                            if(obSOS.SalesOrderLineStagings__r == null || obSOS.SalesOrderLineStagings__r.size() == 0){
                                mapSOSLineCount.put(obSOS.Id, 0);
                            }else{
                                mapSOSLineCount.put(obSOS.Id, obSOS.SalesOrderLineStagings__r.size() );
                            }
                        }
                    }
                }
                List<gii__SalesOrder__c> soList = new List<gii__SalesOrder__c>();
                for(gii__SalesOrderStaging__c objSOS :(List<gii__SalesOrderStaging__c>) lstSourceObject){
                    gii__SalesOrder__c so;              
                    if(setErrorIds.contains(''+objSOS.id)){
                        objSOS.giic_Status__c = System.Label.giic_Error; 
                        setSOSIds.add(objSOS.id);
                    }
                    else if(mpNewSObjectId.get(objSOS.Id).totalChilds != 0){
                        setSOSIds.add(objSOS.id);
                        objSOS.giic_Status__c = System.Label.giic_InProgress; 
                        so = new gii__SalesOrder__c(Id=mpNewSObjectId.get(objSOS.Id).targetObj.Id, giic_ProcessingStatus__c=System.Label.giic_ProcessedwithError); soList.add(so); 
                        
                        }
                    else {
                        objSOS.giic_Status__c = System.Label.giic_Converted; 
                        so=new gii__SalesOrder__c(Id=mpNewSObjectId.get(objSOS.Id).targetObj.Id,giic_ProcessingStatus__c=System.Label.giic_Processed); 
                        soList.add(so); 
                    }
                    // start of below peace of code update status of SO and SOS if there is no line
                    system.debug('mapSOSLineCount--->>>>>> ' + mapSOSLineCount + ' so--->>>> ' + so);
                    if(so != null && mapSOSLineCount != null && mapSOSLineCount.containsKey(''+objSOS.id) && mapSOSLineCount.get(''+objSOS.id) == 0){
                        objSOS.giic_Status__c = System.Label.giic_Error;
                        // gii__SalesOrder__c so = new gii__SalesOrder__c(Id=mpNewSObjectId.get(objSOS.Id).targetObj.Id); 
                        so.giic_SOStatus__c=System.Label.giic_Draft;
                        so.giic_ProcessingStatus__c=System.Label.giic_ProcessedwithError;
                        // soList.add(so); 
                        
                        Integration_Error_Log__c objEL = new Integration_Error_Log__c();
                        objEL.giic_SalesOrder__c = so.Id;
                        objEL.giic_SalesOrderStaging__c = objSOS.Id;
                        setSOSId.add(objSOS.Id);
                        objEL.Error_Code__c = giic_Constants.NO_LINE_ERROR;
                        objEL.Name = giic_Constants.NO_LINE_ERROR;
                        objEL.giic_IsActive__c = true;
                        objEL.Error_Message__c = giic_Constants.NO_LINE_AVAILABLE;
                        lstErrorLog.add(objEL);
                    }
                    // End of peace of code update status of SO and SOS if there is no line
                }
                update soList;
                update lstSourceObject;
                giic_CommonUtility.updateSOSErrorOnSalesOrder(setSOSIds); // Update error logs from sales order staging to sales orders
                
                // deactivate old error logs
                // giic_IntegrationCustomMetaDataUtil.deActivateCurrentError(setSOSId);
                
                if(lstErrorLog != null && lstErrorLog.size() > 0 ){
                    insert lstErrorLog;
                }
                
                /******************************************/
                
                set<string> SOIds = new set<string>();
                list<gii__SalesOrder__c> lstSalesOrder = new list<gii__SalesOrder__c>();
                 set<string> setAccountId = new set<string>();
                 
                if(mpNewSObjectId.values() != null && mpNewSObjectId.values().size() > 0){
                   for(giic_IntegrationCMDResultWrapper oICMDRW : mpNewSObjectId.values()){
                       
                       //system.debug('oICMDRW.targetObj.getSObjectType()---->>>>>1111 ' + oICMDRW.targetObj.getSObjectType());
                       if(oICMDRW.targetObj != null && oICMDRW.targetObj.getSObjectType() == Schema.gii__SalesOrder__c.getSObjectType() ){
                          // system.debug('oICMDRW.targetObj.getSObjectType()---->>>>>2222' + oICMDRW.targetObj.getSObjectType()+ ' <<<<--->>>> ' + Schema.gii__SalesOrder__c.getSObjectType());
                           if(string.isNotEmpty(string.valueOf( oICMDRW.targetObj.get('Id')))){
                               SOIds.add(string.valueOf( oICMDRW.targetObj.get('Id')));
                               if(oICMDRW.targetObj.get('gii__AccountReference__c') != null && oICMDRW.targetObj.get('gii__Account__c') != null) lstSalesOrder.add((gii__SalesOrder__c) oICMDRW.targetObj);
                                if(oICMDRW.targetObj.get('gii__Account__c') != null) setAccountId.add(string.valueOf(oICMDRW.targetObj.get('gii__Account__c')));
                           }
                       }
                   }
                }

                /*system.debug('SOIds------>>>>>> ' + SOIds);
                system.debug('lstSalesOrder------>>>>>> ' + lstSalesOrder);
                system.debug('setAccountId------>>>>>> ' + setAccountId);*/
                //insert update promotions on sales order
                try{
                   map<string,object> mapPromoResponce = giic_PromotionHelper.getAccProgramForSOStoSO(lstSalesOrder, setAccountId);
                   list<gii__AccountProgram__c> lstAccountProgramToInsert = (list<gii__AccountProgram__c>) mapPromoResponce.get('ACCPROINSERT');
                   list<gii__AccountProgram__c> lstAccountProgramToUpdate = (list<gii__AccountProgram__c>) mapPromoResponce.get('ACCPROUPDATE');
                   list<giic_PromotionWrapper.PromoResponseWrapper> lstPromoResponseWrapper = (list<giic_PromotionWrapper.PromoResponseWrapper>) mapPromoResponce.get('ERRORINFO');
                   
                   if(lstAccountProgramToInsert != null && !lstAccountProgramToInsert.isEmpty()) insert lstAccountProgramToInsert;
                   if(lstAccountProgramToUpdate != null && !lstAccountProgramToUpdate.isEmpty()) update lstAccountProgramToUpdate;
                   if(lstPromoResponseWrapper != null && !lstPromoResponseWrapper.isEmpty()){
                       List<Integration_Error_Log__c> lstUOMErrors = new List<Integration_Error_Log__c>();
                        Set<String> setErrorPromoIds = new Set<String>();
                        String userStory = giic_Constants.USER_STORY_PROMOTIONCALCULATION; //'Promotion Calculation';
                        Map<String, String> fieldApiForErrorLogMaster = new Map<String, String>{'giic_SalesOrder__c'=>'Id'};
                       for(giic_PromotionWrapper.PromoResponseWrapper objWrapper : lstPromoResponseWrapper){
                            giic_IntegrationCustomMetaDataUtil.collectErrors(lstUOMErrors, setErrorPromoIds, userStory, fieldApiForErrorLogMaster, new gii__SalesOrder__c(Id = objWrapper.orderId), 'Promotion Calculation Error', objWrapper.lstErrorInformation[0].errorMessage); 
                
                       }
                       
                       if(!lstUOMErrors.isEmpty()) insert lstUOMErrors;
                   }
                   
                }catch(exception e){
                   System.debug('Exp:' + e.getLineNumber() + e.getMessage());
                }
                
                //Check for exception state of the converted orders
                giic_IntegrationCommonUtil.checkForExceptionState(SOIds);

                set<string> ZIPCODES = new set<string>();
                set<string> WHGIDS = new set<string>();
                set<string> ZIPWHKEY = new set<string>();
                map<string,object> mapRequest = new map<string,object>();
                map<string, List<gii__SalesOrder__c>> mapZipCodeWHGroupIdVsLstSO = new map<string, List<gii__SalesOrder__c>>();
                List<gii__SalesOrder__c> lstSOToUpdate = new List<gii__SalesOrder__c>();
                List<gii__SalesOrder__c> lstSO =  [select id, name, gii__DropShip__c, giic_DropShipWHMapping__c, gii__Warehouse__c, gii__ShipToZipPostalCode__c, gii__AccountReference__r.giic_WarehouseGroup__c from gii__SalesOrder__c where gii__ShipToZipPostalCode__c != null AND gii__AccountReference__c != null AND gii__AccountReference__r.giic_WarehouseGroup__c != null AND id in : SOIds AND ((giic_Source__c = 'INSITE' AND gii__DropShip__c = true) OR giic_Source__c = 'OLP')];
                // condition giic_Source__c = 'INSITE' AND gii__DropShip__c = true means it is insiteOrder and dropship is true so need to calculate "dropship warehouse distance"
                // condition giic_Source__c = 'OLP' means it is OLP Order and  "dropship warehouse distance" need to be calculated for all OLP Order
                //system.debug('lstSO Size = ' + lstSO.size() + ' --->>>> ' + lstSO);
                for(gii__SalesOrder__c objSO :  lstSO ){
                   ZIPCODES.add(objSO.gii__ShipToZipPostalCode__c);
                   WHGIDS.add(objSO.gii__AccountReference__r.giic_WarehouseGroup__c);
                   ZIPWHKEY.add(objSO.gii__ShipToZipPostalCode__c+'~'+objSO.gii__AccountReference__r.giic_WarehouseGroup__c);
                   if(!mapZipCodeWHGroupIdVsLstSO.containsKey(objSO.gii__ShipToZipPostalCode__c+'~'+objSO.gii__AccountReference__r.giic_WarehouseGroup__c)){
                       mapZipCodeWHGroupIdVsLstSO.put(objSO.gii__ShipToZipPostalCode__c+'~'+objSO.gii__AccountReference__r.giic_WarehouseGroup__c, new List<gii__SalesOrder__c>{});
                   }
                   mapZipCodeWHGroupIdVsLstSO.get(objSO.gii__ShipToZipPostalCode__c+'~'+objSO.gii__AccountReference__r.giic_WarehouseGroup__c).add(objSO);
                }
                mapRequest.put('ZIPCODES', ZIPCODES);
                mapRequest.put('WHGIDS', WHGIDS);
                mapRequest.put('ZIPWHKEY', ZIPWHKEY);

                /*system.debug('ZIPCODES--->>>> ' + ZIPCODES);
                system.debug('WHGIDS--->>>> ' + WHGIDS);
                system.debug('ZIPWHKEY--->>>> ' + ZIPWHKEY);*/

                map<string,Object> mapZipCodesResponse = giic_CommonUtility.getDistanceOfWarehousesFromZipCode(mapRequest);

                //system.debug('mapZipCodesResponse--->>>> ' + mapZipCodesResponse);
                map<string,string> mapZipCode = new map<string,string>();
                if(mapZipCodesResponse.containsKey('ZIPCODESMAP')){
                   // mapZipCode contains dropship warehouse distance based on ZipCode+WarehouseGroupId
                    mapZipCode=(map<string,string>)mapZipCodesResponse.get('ZIPCODESMAP');
                }
                if(mapZipCodeWHGroupIdVsLstSO !=null && mapZipCodeWHGroupIdVsLstSO.keySet()!=null && mapZipCodeWHGroupIdVsLstSO.keySet().size()>0 && mapZipCode !=null && mapZipCode.size()>0){
                   for(string sZipCodeWHGId: mapZipCodeWHGroupIdVsLstSO.keySet() ){
                       if(mapZipCode.containsKey(sZipCodeWHGId)){
                          for(gii__SalesOrder__c objSO : mapZipCodeWHGroupIdVsLstSO.get(sZipCodeWHGId)){
                              objSO.giic_DropShipWHMapping__c = mapZipCode.get(sZipCodeWHGId);
                              lstSOToUpdate.add(objSO);
                          }
                       }
                   }
                }

                //system.debug('lstSOToUpdate Size = ' + lstSOToUpdate.size() + ' --->>>> ' + lstSOToUpdate);
                if(lstSOToUpdate != null && lstSOToUpdate.size() > 0){
                   update lstSOToUpdate;
                }
                
                /******************************************/
                
                if(setErrorIds.isEmpty()){ ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM, System.Label.giic_SuccessMsg)); return;} //lstSourceObject
                else{
                    isError = true; 
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, System.Label.giic_ErrorMsgCheckErrorLog));
                    for(SObject obj : lstSourceObject){
                       // if(!mapResult.containsKey(obj.id)) lstSOSErrors.add((gii__SalesOrderStaging__c) obj); // address validation removed
                        if(setErrorIds.contains(obj.id)) lstSOSErrors.add((gii__SalesOrderStaging__c) obj);
                    }
                    return;
                }
            }else{
                isError = true;
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, System.Label.giic_ErrorMsgSelectRecord));
                return;
            }
        }catch(Exception ex){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getMessage()));
        }
    }
    
    public PageReference goBack(){
        PageReference pObj ;
        try{          
            Schema.DescribeSObjectResult result = gii__SalesOrderStaging__c.SObjectType.getDescribe();               
            pObj = new PageReference('/' + result.getKeyPrefix() + '/o'); // redirect to sales order Staging list view
            pObj.setRedirect(true);            
        }catch(Exception ex){ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getMessage())); return null; }
        return pObj;
    }
    
     public PageReference goToSOS(){
        PageReference pObj ;
        try{          
            pObj = new PageReference('/' + sosId); // redirect to sales order Staging record
            pObj.setRedirect(true);            
        }catch(Exception ex){ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getMessage())); return null; }
        return pObj;
     }
}