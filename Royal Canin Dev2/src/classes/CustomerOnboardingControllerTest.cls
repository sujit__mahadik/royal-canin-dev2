/*
 * Date:             - Developer, Company                          - description
 * 2015-08-05        - Stevie Yakkel, Acumen Solutions             - Edited
 */

@isTest
private class CustomerOnboardingControllerTest
{
    @isTest (SeeAllData=true)
    static void verifyACHFormPresent() {
        Document myDocument = new Document();
        myDocument = [Select Id, DeveloperName From Document Where DeveloperName = 'ACH_FORM' limit 1];
        //Move Royal_Canin_US_ACH_Form to Custom Setting
        System.assertNotEquals(null, myDocument);
    }

    @isTest
    static void constr() {
        SetupTestData.createCustomSettings();
        
        Customer_Registration__c reg = SetupTestData.createCustomerRegistration();
        insert reg;

        test.StartTest();
            ApexPages.StandardController stdController = new ApexPages.StandardController(reg);
            CustomerOnboardingController controller = new CustomerOnboardingController(stdController);
        test.StopTest();
    }

    @isTest
    static void testgetItems() {
        CustomerOnboardingController controller = createCustomerOnboardingController();

        test.StartTest();
            List<SelectOption> options = controller.getItems();
        test.StopTest();

        System.assertNotEquals(options, null);
    }

    @isTest
    static void testinitPaging() {
        CustomerOnboardingController controller = createCustomerOnboardingController();
        controller.state.type = 'Vet';
        test.StartTest();
            controller.initPaging();
        test.StopTest();

        System.assertEquals(true, controller.state.renderMain);
    }

    @isTest
    static void testStart() {
        CustomerOnboardingController controller = createCustomerOnboardingController();
        controller.state.email = 'tester@tester.com';
        controller.state.type = 'Retail';

        test.StartTest();
            controller.start();
        test.StopTest();

        List<Customer_Registration__c> reg = [SELECT Id, Application_Status__c, Email_Address__c FROM Customer_Registration__c WHERE Application_Id__c =: controller.applicationId];
        System.debug('REGGGGG ' + reg);
        System.assertEquals(reg[0].Application_Status__c, 'Open');
        System.assertEquals(reg[0].Email_Address__c, controller.state.email);
    }

    @isTest
    static void testStartChangingState(){
        CustomerOnboardingController controller = createCustomerOnboardingController();
        controller.state.email = null;
        Test.startTest();
            controller.start();
        Test.stopTest();
    }

    @isTest
    static void testStartEmailNotNull() {
        CustomerOnboardingController controller = createCustomerOnboardingController();
        controller.state.email = 'test@test.com';

        test.StartTest();
            controller.start();
        test.StopTest();
    }

    @isTest
    static void testNext() {
        CustomerOnboardingController controller = createCustomerOnboardingController();
        controller.state.isReadOnly = false;
        controller.state.email = 'test@test.com';
        test.StartTest();
            controller.next_pr();
        test.StopTest();

        System.assertEquals(1, controller.state.curStep);

        controller.prev_pr();

        System.assertEquals(0, controller.state.curStep);
    }
    
    @isTest
    static void testTermUrl() {
        CustomerOnboardingController controller = createCustomerOnboardingController();
        test.StartTest();
        Global_Parameters__c gparams = new Global_Parameters__c();
        gparams.name='ApplicationURLStub';
        gparams.Value__c = 'http://full-royalcanin-usa.cs20.force.com.test/';
        insert gparams;
        
        controller.getPageCont();
        String url = null;
        
        controller.state.type = 'Retailer';
        url = controller.termsURL;
        controller.state.type = 'Cat Breeder';
        url = controller.termsURL;
        controller.state.type = 'Working Dog';
        url = controller.termsURL;
        controller.state.type = 'Shelter';
        url = controller.termsURL;
        controller.state.type = 'Veterinary';
        url = controller.termsURL;
        controller.state.type = 'Banfield Associate';
        url = controller.termsURL;
        controller.state.type = 'RC Associate';
        url = controller.termsURL;
        test.StopTest();

        
    }
    
    @isTest
    static void testNextSellToAcount() {
        CustomerOnboardingController controller = createCustomerOnboardingController();
        Customer_Registration__c reg = SetupTestData.createCustomerRegistration();
        controller.state.isReadOnly = false;
        controller.state.email = 'test@test.com';
        test.StartTest();
            Boolean v3 = controller.isVet;
            Boolean v2 = controller.isRetail;
            controller.next_pr();
            controller.state.type = 'RET-Sell To';
            Boolean v1 = controller.isVetRetailSellTo;
            controller.next_pr();
            reg.ClinicCustomerNo__c = '102345687';
            insert reg;
            controller.state.type = 'Clinic Feeding Participant';
            controller.next_pr();
            //controller.splitAddress('Test 12373 maverick drive \n St.Louis');
        test.StopTest();

        System.assertEquals(1, controller.state.curStep);

        controller.prev_pr();

        System.assertEquals(0, controller.state.curStep);
    }

    @isTest
    static void testMatchingCustomerTypeAndAccountStatusProspectValidation(){
        System.debug('MERGE TEST');
        SetupTestData.createAccounts(1);
        Account acc = SetupTestData.testAccounts[0];
        acc.Customer_Type__c = 'Feeding Participant';
        //acc.Account_Status__c = 'Customer';
        acc.Name = 'Test Account';
        acc.Phone = '(123) 456-1234';
        acc.Do_Not_Use__c = false;
        acc.Bill_To_Customer_ID__c = acc.Id;
        update acc;



        CustomerOnboardingController controller = createCustomerOnboardingController();
        controller.state.isReadOnly = false;
        controller.state.email = 'test@test.com';
        controller.registration.Name_of_Business_Organization__c = 'Test';
        controller.registration.Phone_Number__c = '(123) 456-1234';
        controller.registration.Customer_Account_ID__c = acc.Id;
        controller.registration.Customer_Type__c = 'Feeding Participant';
        controller.registration.First_Name__c = 'Test';
        controller.registration.Last_Name__c = 'Account';
        controller.state.type = 'Feeding Participant';

        update controller.registration;

        test.StartTest();
            controller.next_pr();


        boolean expectedException = true;
        List<Apexpages.Message> msgs = ApexPages.getMessages();
        for(Apexpages.Message msg : msgs) {
            if(msg.getDetail().contains('You already have an account'))
                expectedException = false;
        }

        test.StopTest();

        System.assert(expectedException, 'Exception should not be thrown');

        List<Customer_Registration__c> registrationList = new List<Customer_Registration__c> ([SELECT Id FROM Customer_Registration__c]);

        System.assertEquals(1,registrationList.size());
    }

    @isTest
    static void testMatchingCustomerTypeAndAccountStatusCustomerValidation(){
        RecordType rt = [select id from RecordType where SobjectType = 'Account' and developerName='Retailer'];

        Account acc = new Account( );
        acc.Name = 'Test Account';
        acc.RecordTypeId = rt.id;
        acc.Bill_To_Customer_ID__c = 'RC-TEST-00';
        acc.ShipToCustomerNo__c = 'RC-TEST-001-00';
        acc.BillingStreet = '123 Main Street';
        acc.BillingCity = 'Apple';
        acc.BillingState = 'MN';
        acc.BillingPostalCode ='12345';
        acc.ShippingStreet = '123 Main Street';
        acc.ShippingCity = 'Apple';
        acc.ShippingState = 'MN';
        acc.ShippingPostalCode ='12345';
        acc.Current_Fiscal_YTD__c = 5000;
        acc.Sales_Summarization_Completed__c = false;
        acc.Customer_Type__c = 'Feeding Participant';
        acc.Account_Status__c = 'Customer';
        acc.Phone = '(123) 456-1234';
        acc.Do_Not_Use__c = false;
        acc.Order_Placed__c = true;

        insert acc;

        CustomerOnboardingController controller = createCustomerOnboardingController();
        controller.state.isReadOnly = false;
        controller.state.email = 'test@test.com';
        controller.registration.Name_of_Business_Organization__c = 'Test';
        controller.registration.Phone_Number__c = '(123) 456-1234';
        controller.registration.Customer_Account_ID__c = acc.Bill_To_Customer_ID__c;
        controller.registration.Customer_Type__c = 'Feeding Participant';
        controller.registration.First_Name__c = 'Test';
        controller.registration.Last_Name__c = 'Account';
        controller.state.type = 'Feeding Participant';

        update controller.registration;

        test.StartTest();
            controller.next_pr();


        boolean expectedException = false;
        List<Apexpages.Message> msgs = ApexPages.getMessages();
        for(Apexpages.Message msg : msgs) {
            if(msg.getDetail().contains('You already have an account'))
                expectedException = true;
        }

        test.StopTest();

        System.assert(expectedException);

        //List<Customer_Registration__c> registrationList = new List<Customer_Registration__c> ([SELECT Id FROM Customer_Registration__c]);

        //System.assertEquals(2,registrationList.size());
    }

    @isTest
    static void testBanfieldCustomerType(){
        System.debug('DIFFERENT TEST');
        SetupTestData.createAccounts(1);
        Account acc = SetupTestData.testAccounts[0];
        acc.Customer_Type__c = 'Banfield Associate';
        //acc.Account_Status__c = 'Customer';
        //acc.Id = 'BF_64536';
        acc.Name = 'Test Account';
        acc.Phone = '(123) 456-1234';
        acc.Do_Not_Use__c = false;
        acc.Bill_To_Customer_ID__c = acc.Id;
        acc.Account_Status__c = 'Prospect';     
        update acc;

        Employee__c regEmployee = new Employee__c();
        regEmployee.UserLogon__c = acc.Id;
        regEmployee.Location_Code__c = 'abc1234';
        regEmployee.FirstName__c = 'Test';
        regEmployee.LastName__c = 'Account';
        regEmployee.Zip_Code__c = '44133';  
        insert regEmployee;

        CustomerOnboardingController controller = createCustomerOnboardingController();
        controller.state.isReadOnly = false;
        controller.state.email = 'test@test.com';
        controller.registration.Name_of_Business_Organization__c = 'Test';
        controller.registration.User_Logon__c = acc.Id;
        controller.registration.Phone_Number__c = '(123) 456-1234';
        controller.registration.Customer_Account_ID__c = acc.Id;
        controller.registration.Customer_Type__c = 'Banfield Associate';
        controller.registration.First_Name__c = 'Test';
        controller.registration.Last_Name__c = 'Account';
        controller.registration.Billing_Zip_Code__c = '44133';
        controller.registration.Dept_Store_Number__c = '1234';
        //controller.state.type = 'Feeding Participant';

        try {
        
        update controller.registration;
        
        } catch(exception E) { 
          }


        test.StartTest();
            controller.next_pr();


        boolean expectedException = false;
        List<Apexpages.Message> msgs = ApexPages.getMessages();
        for(Apexpages.Message msg : msgs) {
            if(msg.getDetail().contains('You already have an account'))
                expectedException = true;
        }

        test.StopTest();

        System.assert(expectedException, 'Exception should be thrown');

    }

    @isTest
    static void testNextBillToException() {
        SetupTestData.createAccounts(1);
        Account acc = SetupTestData.testAccounts[0];
        acc.Name = 'Test';
        acc.Order_Placed__c = true;
        acc.Account_Status__c = 'Customer';
        acc.Phone = '(123) 456-1234';
        update acc;

        CustomerOnboardingController controller = createCustomerOnboardingController();
        controller.registration.Name_of_Business_Organization__c = 'Test';
        controller.registration.Phone_Number__c = '(123) 456-1234';
        controller.registration.Customer_Account_ID__c = acc.Id;
        controller.state.isReadOnly = false;
        controller.state.email = 'test@test.com';

        System.debug('here!!!!');
        test.StartTest();
        boolean expectedException = false;
            controller.next_pr();
            List<Apexpages.Message> msgs = ApexPages.getMessages();
            for(Apexpages.Message msg : msgs) {
                if(msg.getDetail().contains('There is no account with that Bill-To Customer Id'))
                    expectedException = true;
            }
            System.assert(expectedException);
        test.StopTest();
    }

    @isTest
    static void testNextClinicCustomerException() {
        SetupTestData.createAccounts(1);
        Account acc = SetupTestData.testAccounts[0];
        acc.Name = 'Test';
        acc.Account_Status__c = 'Customer';
        acc.Phone = '(123) 456-1234';
        acc.Order_Placed__c = true;
        update acc;

        CustomerOnboardingController controller = createCustomerOnboardingController();
        controller.registration.Name_of_Business_Organization__c = 'Test';
        controller.registration.Phone_Number__c = '(123) 456-1234';
        controller.registration.Customer_Account_ID__c = acc.Id;
        controller.registration.ClinicCustomerNo__c = '1234';
        controller.state.isReadOnly = false;
        controller.state.email = 'test@test.com';

        System.debug('here!!!!');
        test.StartTest();
        boolean expectedException = false;
            controller.next_pr();
            List<Apexpages.Message> msgs = ApexPages.getMessages();
            for(Apexpages.Message msg : msgs) {
                if(msg.getDetail().contains('No existing account with that Clinic Customer Number.'))
                    expectedException = true;
            }
            //temporarily removed assert due to last minute request from RC to remove account validation for UFP
            //System.assert(expectedException);
        test.StopTest();
    }

    @isTest
    static void testPrev() {
        CustomerOnboardingController controller = createCustomerOnboardingController();
        controller.state.isReadOnly = false;

        test.StartTest();
            controller.prev();
        test.StopTest();
    }

    @isTest
    static void testPrevBillToException() {
        SetupTestData.createAccounts(1);
        Account acc = SetupTestData.testAccounts[0];
        acc.Name = 'Test';
        acc.Account_Status__c = 'Customer';
        acc.Phone = '(123) 456-1234';
        acc.Order_Placed__c = true;
        update acc;

        CustomerOnboardingController controller = createCustomerOnboardingController();
        controller.registration.Name_of_Business_Organization__c = 'Test';
        controller.registration.Phone_Number__c = '(123) 456-1234';
        controller.registration.Customer_Account_ID__c = acc.Id;
        controller.state.isReadOnly = false;
        controller.state.email = 'test@test.com';

        System.debug('here!!!!');
        test.StartTest();
        boolean expectedException = false;
            controller.prev_pr();
            List<Apexpages.Message> msgs = ApexPages.getMessages();
            for(Apexpages.Message msg : msgs) {
                if(msg.getDetail().contains('There is no account with that Bill-To Customer Id'))
                    expectedException = true;
            }
            System.assert(expectedException);
        test.StopTest();
    }

    @isTest
    static void testPrevClinicCustomerException() {
        SetupTestData.createAccounts(1);
        Account acc = SetupTestData.testAccounts[0];
        acc.Name = 'Test';
        acc.Account_Status__c = 'Customer';
        acc.Phone = '(123) 456-1234';
        acc.Order_Placed__c = true;
        update acc;

        CustomerOnboardingController controller = createCustomerOnboardingController();
        controller.registration.Name_of_Business_Organization__c = 'Test';
        controller.registration.Phone_Number__c = '(123) 456-1234';
        controller.registration.Customer_Account_ID__c = acc.Id;
        controller.registration.ClinicCustomerNo__c = '1234';
        controller.state.isReadOnly = false;
        controller.state.email = 'test@test.com';

        System.debug('here!!!!');
        test.StartTest();
        boolean expectedException = false;
            controller.prev_pr();
            List<Apexpages.Message> msgs = ApexPages.getMessages();
            for(Apexpages.Message msg : msgs) {
                if(msg.getDetail().contains('No existing account with that Clinic Customer Number.'))
                    expectedException = true;
            }
            //temporarily removed assert due to last minute request from RC to remove account validation for UFP
            //System.assert(expectedException);
        test.StopTest();
    }

    @isTest
    static void testSave() {
        CustomerOnboardingController controller = createCustomerOnboardingController();
        controller.state.isReadOnly = false;

        test.StartTest();
            controller.save_pr();
        test.StopTest();
    }

    @isTest
    static void testSaveBillToException() {
        SetupTestData.createAccounts(1);
        Account acc = SetupTestData.testAccounts[0];
        acc.Name = 'Test';
        acc.Account_Status__c = 'Customer';
        acc.Phone = '(123) 456-1234';
        acc.Order_Placed__c = true;
        update acc;

        CustomerOnboardingController controller = createCustomerOnboardingController();
        controller.registration.Name_of_Business_Organization__c = 'Test';
        controller.registration.Phone_Number__c = '(123) 456-1234';
        controller.registration.Customer_Account_ID__c = acc.Id;
        controller.state.isReadOnly = false;
        controller.state.email = 'test@test.com';

        System.debug('here!!!!');
        test.StartTest();
        boolean expectedException = false;
            controller.save_pr();
            List<Apexpages.Message> msgs = ApexPages.getMessages();
            for(Apexpages.Message msg : msgs) {
                if(msg.getDetail().contains('There is no account with that Bill-To Customer Id'))
                    expectedException = true;
            }
            System.assert(expectedException);
        test.StopTest();
    }

    @isTest
    static void testSaveClinicCustomerException() {
        SetupTestData.createAccounts(1);
        Account acc = SetupTestData.testAccounts[0];
        acc.Name = 'Test';
        acc.Account_Status__c = 'Customer';
        acc.Phone = '(123) 456-1234';
        acc.Order_Placed__c = true;
        update acc;

        CustomerOnboardingController controller = createCustomerOnboardingController();
        controller.registration.Name_of_Business_Organization__c = 'Test';
        controller.registration.Phone_Number__c = '(123) 456-1234';
        controller.registration.Customer_Account_ID__c = acc.Id;
        controller.registration.ClinicCustomerNo__c = '1234';

        update controller.registration;
        controller.state.isReadOnly = false;
        controller.state.email = 'test@test.com';

        System.debug('here!!!!');
        test.StartTest();
        boolean expectedException = false;
            controller.save_pr();
            List<Apexpages.Message> msgs = ApexPages.getMessages();
            for(Apexpages.Message msg : msgs) {
                if(msg.getDetail().contains('No existing account with that Clinic Customer Number.'))
                    expectedException = true;
            }
            //temporarily removed assert due to last minute request from RC to remove account validation for UFP
            //System.assert(expectedException);
        test.StopTest();
    }

    @isTest
    static void testSubmit() {
        SetupTestData.createCustomSettings();       
        Customer_Registration__c reg = SetupTestData.createCustomerRegistration();
        reg.Upload_Complete_Resale__c = true;
        reg.Terms_and_Conditions_Accepted__c = true;
        
        insert reg;

        Customer_Registration__c retrievedReg = [SELECT Id, Application_Id__c FROM Customer_Registration__c WHERE Id =: reg.Id];

        PageReference pageRef = Page.CustomerOnboarding;
        pageRef.getParameters().put('applicationid', retrievedReg.Application_Id__c);
        Test.setCurrentPage(pageRef);

        ApexPages.StandardController stdController = new ApexPages.StandardController(reg);
        CustomerOnboardingController controller = new CustomerOnboardingController(stdController);

        controller.state.email = 'test@tester.com';
        controller.state.isReadOnly = false;

        test.StartTest();
            controller.submit_pr();
        test.StopTest();

        //System.assert(controller.state.isReadOnly);
    }

    @isTest
    static void testSubmitBillToException() {
        SetupTestData.createAccounts(1);
        Account acc = SetupTestData.testAccounts[0];
        acc.Name = 'Test';
        acc.Account_Status__c = 'Customer';
        acc.Phone = '(123) 456-1234';
        acc.Order_Placed__c = true;
        update acc;

        CustomerOnboardingController controller = createCustomerOnboardingController();
        controller.registration.Name_of_Business_Organization__c = 'Test';
        controller.registration.Phone_Number__c = '(123) 456-1234';
        controller.registration.Customer_Account_ID__c = acc.Id;
        controller.state.isReadOnly = false;
        controller.state.email = 'test@test.com';

        System.debug('here!!!!');
        test.StartTest();
        boolean expectedException = false;
            controller.submit_pr();
            List<Apexpages.Message> msgs = ApexPages.getMessages();
            for(Apexpages.Message msg : msgs) {
                if(msg.getDetail().contains('There is no account with that Bill-To Customer Id'))
                    expectedException = true;
            }
            System.assert(expectedException);
        test.StopTest();
    }

    @isTest
    static void testSubmitClinicCustomerException() {
        SetupTestData.createAccounts(1);
        Account acc = SetupTestData.testAccounts[0];
        acc.Name = 'Test';
        acc.Account_Status__c = 'Customer';
        acc.Phone = '(123) 456-1234';
        acc.Order_Placed__c = true;
        update acc;

        CustomerOnboardingController controller = createCustomerOnboardingController();
        controller.registration.Name_of_Business_Organization__c = 'Test';
        controller.registration.Phone_Number__c = '(123) 456-1234';
        controller.registration.Customer_Account_ID__c = acc.Id;
        controller.registration.ClinicCustomerNo__c = '1234';
        controller.state.isReadOnly = false;
        controller.state.email = 'test@test.com';

        System.debug('here!!!!');
        test.StartTest();
        boolean expectedException = false;
            controller.submit_pr();
            List<Apexpages.Message> msgs = ApexPages.getMessages();
            for(Apexpages.Message msg : msgs) {
                if(msg.getDetail().contains('No existing account with that Clinic Customer Number.'))
                    expectedException = true;
            }
            //temporarily removed assert due to last minute request from RC to remove account validation for UFP
            //System.assert(expectedException);
        test.StopTest();
    }

    //@isTest
    //static void testEmailDupe() {
    //  CustomerOnboardingController controller = createCustomerOnboardingController();
    //  List<Customer_Registration__c> previousRegistrations = new List<Customer_Registration__c>();
    //  Customer_Registration__c reg = SetupTestData.createCustomerRegistration();
    //  reg.Email_Address__c = 'mytest@test.com';
    //  insert reg;
    //  controller.state.email = 'mytest@test.com';
    //  controller.state.type = 'RetailSellTo';

    //  boolean result;
    //  test.StartTest();
    //      result = controller.isEmailDupe();
    //  test.StopTest();

    //}

    @isTest
    static void testAppIdURLEncoded() {
        CustomerOnboardingController controller = createCustomerOnboardingController();
        Customer_Registration__c reg = SetupTestData.createCustomerRegistration();
        controller.applicationId = '123';

        string s;
        test.StartTest();
            s = controller.applicationIdURLEncoded;
        test.StopTest();
    }

    @isTest
    static void testAppIdURLEncodedIDNull() {
        CustomerOnboardingController controller = createCustomerOnboardingController();
        Customer_Registration__c reg = SetupTestData.createCustomerRegistration();
        controller.applicationId = null;

        string s;
        test.StartTest();
            s = controller.applicationIdURLEncoded;
        test.StopTest();
    }

    @isTest
    static void testIsVetRetail() {
        CustomerOnboardingController controller = createCustomerOnboardingController();
        Customer_Registration__c reg = SetupTestData.createCustomerRegistration();
        controller.state.type = 'Veterinary';

        boolean result;
        test.StartTest();
            result = controller.isVetRetail;
        test.StopTest();
    }

    @isTest
    static void testIsVetRetailNull() {
        CustomerOnboardingController controller = createCustomerOnboardingController();
        Customer_Registration__c reg = SetupTestData.createCustomerRegistration();
        controller.state = null;

        boolean result;
        test.StartTest();
            result = controller.isVetRetail;
        test.StopTest();
    }

    @isTest
    static void testEnableButton() {
        CustomerOnboardingController controller = createCustomerOnboardingController();
        controller.state.email = 'tester@testing.com';

        Test.startTest();
            controller.enableStartBtn();
        Test.stopTest();

        System.assert(controller.state.renderStart);
    }

    @isTest
    static void testEnableButtonError() {
        CustomerOnboardingController controller = createCustomerOnboardingController();

        Test.startTest();
            controller.enableStartBtn();
        Test.stopTest();

        System.assert(controller.state.renderError);
    }
    
    @isTest 
    static void testgetType(){
        CustomerOnboardingController controller = createCustomerOnboardingController();

        Test.startTest();
            String type = controller.getType();
        Test.stopTest();

        System.assertEquals(controller.state.type, type, 'Type is not the correct value');
    }

    @isTest 
    static void testenableStartBtn(){
        CustomerOnboardingController controller = createCustomerOnboardingController();
        controller.state.email = 'test1234@test.com';

        Test.startTest();
            controller.enableStartBtn();
        Test.stopTest();

        System.assert(controller.state.renderStart, 'render start should not be false');
    }

    @isTest
    static void testDeleteAttachment(){
        CustomerOnboardingController controller = createCustomerOnboardingController();
        controller.state.email = 'test1234@test.com';

        System.debug('LOOK HERE');
        System.debug(controller.registration.Id);

        Attachment att = new Attachment();
        att.ParentId = controller.registration.Id;
        att.Name = 'Test';
        att.Description = 'Test';
        att.Body = Blob.valueOf('This is a test');
        insert att;

        System.debug(att);

        Test.startTest();
            controller.attachments = null;
            List<Attachment> attachments = controller.getAttachments();
            System.assert(!attachments.isEmpty());
            controller.selectedAttachmentId = attachments[0].Id;
            controller.deleteAttachment();
        Test.stopTest();

        try{
            Attachment deletedAttachment = [SELECT Id FROM Attachment WHERE Id = :attachments[0].Id];
            System.assert(false, 'Should not be here');
        } catch (Exception e){
            System.assert(true, 'This should not fail');
        }
    }

    @isTest 
    static void testuploadNext(){
        CustomerOnboardingController controller = createCustomerOnboardingController();
        controller.state.email = 'test1234@test.com';

        Test.startTest();
            PageReference pageRef;
            pageRef = controller.uploadNext();
        Test.stopTest();

        System.assertNotEquals(null, pageRef, 'Page ref should not be null');
    }

    @isTest
    static void testvalidateClinicCustomerNumber(){
        SetupTestData.createAccounts(1);
        Account acc = SetupTestData.testAccounts[0];
        acc.Name = 'Test';
        acc.RecordTypeId = [SELECT Id FROM RecordType WHERE sObjectType='Account' AND DeveloperName='Veterinarian'].Id;
        acc.BillingCity = 'Test';
        acc.BillingState = 'Test';
        acc.BillingPostalCode = '44133';
        acc.ShippingPostalCode = '44133';
        acc.ShipToCustomerNo__c = '404040';
        acc.Account_Status__c = 'Customer';
        acc.Phone = '(123) 456-1234';
        acc.Order_Placed__c = true;
        update acc;
        
        CustomerOnboardingController controller = createCustomerOnboardingController();
        controller.registration.Name_of_Business_Organization__c = 'Test';
        controller.registration.Phone_Number__c = '(123) 456-1234';
        controller.state.type  = 'Clinic Feeding Participant';
        controller.registration.ClinicCustomerNo__c = '404040';
        
                 
        Test.startTest();
            controller.validateClinicCustomerNumber();
                
        Test.stopTest();
    }
    
      @isTest
    static void testvalidateClinicCustomerNumberException(){
        SetupTestData.createAccounts(2);
        Account parent = SetupTestData.testAccounts[0];
        parent.Name = 'parent';
        update parent;
        Account acc = SetupTestData.testAccounts[1];
        acc.Name = 'Test';
        acc.RecordTypeId = [SELECT Id FROM RecordType WHERE sObjectType='Account' AND DeveloperName='Veterinarian'].Id;
        acc.Account_Status__c = 'Customer';
        acc.Phone = '(123) 456-1234';
        acc.ShipToCustomerNo__c = '404040';
        acc.ParentId = parent.Id;
        acc.Order_Placed__c = true;
        update acc;
       

        CustomerOnboardingController controller = createCustomerOnboardingController();
        controller.registration.Name_of_Business_Organization__c = 'Test';
        controller.registration.Phone_Number__c = '(123) 456-1234';
        controller.state.type  = 'Clinic Feeding Participant';
        controller.registration.ClinicCustomerNo__c = '404040';
     
        
        Test.startTest();
        try { 
            controller.validateClinicCustomerNumber();
        } catch(Exception e) {
            Boolean expectedException;
            if(e.getMessage().contains('Script-thrown exception'))
                expectedException = true;
            else
                expectedException = false;
            System.assert(expectedException);
        }
        Test.stopTest();
    }

    
    @isTest
    static void testvalidateBillToAccountIds(){
        SetupTestData.createAccounts(1);
        Account acc = SetupTestData.testAccounts[0];
        acc.Name = 'Test';
        acc.Account_Status__c = 'Customer';
        acc.Phone = '(123) 456-1234';
        acc.ClinicCustomerNo__c = '123456';
        acc.Order_Placed__c = true;
        update acc;
        acc.Bill_To_Customer_ID__c = acc.Id;
        update acc;

        CustomerOnboardingController controller = createCustomerOnboardingController();
        controller.registration.Name_of_Business_Organization__c = 'Test';
        controller.registration.Phone_Number__c = '(123) 456-1234';
        controller.registration.ClinicCustomerNo__c = '123456';
        controller.registration.Customer_Account_ID__c = acc.Id;

        Test.startTest();
            controller.validateBillToAccountIds();
        Test.stopTest();
    }

    @isTest
    static void testvalidateBillToAccountIdsNoAccountsException(){
        SetupTestData.createAccounts(2);
        Account parent = SetupTestData.testAccounts[0];
        parent.Name = 'parent';
        update parent;
        Account acc = SetupTestData.testAccounts[1];
        acc.Name = 'Test';
        acc.Account_Status__c = 'Customer';
        acc.Phone = '(123) 456-1234';
        acc.ClinicCustomerNo__c = '123456';
        acc.ParentId = parent.Id;
        acc.Order_Placed__c = true;
        update acc;
        acc.Bill_To_Customer_ID__c = acc.Id;
        update acc;

        CustomerOnboardingController controller = createCustomerOnboardingController();
        controller.registration.Name_of_Business_Organization__c = 'Test';
        controller.registration.Phone_Number__c = '(123) 456-1234';
        controller.registration.ClinicCustomerNo__c = '123456';
        controller.registration.Customer_Account_ID__c = acc.Id;

        Test.startTest();
        try { 
            controller.validateBillToAccountIds();
        } catch(Exception e) {
            Boolean expectedException;
            if(e.getMessage().contains('Script-thrown exception'))
                expectedException = true;
            else
                expectedException = false;
            System.assert(expectedException);
        }
        Test.stopTest();
    }
    
    @isTest
    static void testvalidateSellToAccountAlreadyExists(){
        SetupTestData.createAccounts(1);
        Account acc = SetupTestData.testAccounts[0];
        acc.Name = 'Test';
        acc.Customer_Type__c = 'Test';
        acc.ShippingStreet = 'shipStreet';
        acc.ShippingCity = 'Test';
        acc.ShippingState = 'Test';
        acc.ShippingPostalCode = '44133';
        acc.Bill_To_Customer_ID__c = '123456';
        acc.Account_Status__c = 'Customer';
        acc.Phone = '(123) 456-1234';
        acc.ShipToCustomerNo__c = acc.id;
        acc.Order_Placed__c = true;
        acc.Do_Not_Use__c = false;
        update acc;
        
        CustomerOnboardingController controller = createCustomerOnboardingController();
        controller.registration.Name_of_Business_Organization__c = 'Test';
        controller.registration.Phone_Number__c = '(123) 456-1234';
        controller.registration.Shipping_Street_Address1__c = 'shipStreet';
        controller.registration.Shipping_City__c= 'Test';
        controller.registration.Ship_To_State__c= 'Test';
        controller.registration.ClinicCustomerNo__c = '123456';
        controller.registration.Customer_Type__c = 'Test';
       
         
        Test.startTest();
            controller.validateSellToAccountAlreadyExists();
                
        Test.stopTest();
    }

    @isTest
    static void testNeedsAttachments() {
        CustomerOnboardingController controller = createCustomerOnboardingController();
        Customer_Registration__c reg = SetupTestData.createCustomerRegistration();
        controller.state.type = 'Veterinary';

        boolean result;
        test.StartTest();
        result = controller.needsAttachments;
        Boolean val = controller.state.renderAttachment;
        String val3 = controller.state.getCurrentStepForRerender;
        Boolean val2 = controller.state.renderSubmit;
        Boolean val1 = controller.state.renderButtonsPanel;
        controller.state.getCurrentStep();
        controller.showPopup();
        controller.closePopup();
        controller.setDocumentUploaded();
        test.StopTest();
    }

    //Not Done. Coming back to this later
    @isTest
    static void testAddError() {
        CustomerOnboardingController controller = createCustomerOnboardingController();

        Test.startTest();
        controller.addError(new TestException('FIELD_CUSTOM_VALIDATION_EXCEPTION, Test Exception:'));
        boolean expectedException = false;
        List<Apexpages.Message> msgs = ApexPages.getMessages();
        for(Apexpages.Message msg : msgs) {
            System.debug('dududu:' + msg.getDetail());
            if(msg.getDetail().contains('Test Exception'))
                expectedException = true;
        
        System.assert(expectedException);
        Test.stopTest();
        }
    }

    @isTest
    static void testsetDefaultRedirect() {
        CustomerOnboardingController controller = createCustomerOnboardingController();
        Test.startTest();
        controller.setDefaultRedirect();
        Test.stopTest();
    }

    static CustomerOnboardingController createCustomerOnboardingController() {
        SetupTestData.createCustomSettings();       
        Customer_Registration__c reg = SetupTestData.createCustomerRegistration();
        insert reg;

        Customer_Registration__c retrievedReg = [SELECT Id, Application_Id__c FROM Customer_Registration__c WHERE Id =: reg.Id];

        PageReference pageRef = Page.CustomerOnboarding;
        pageRef.getParameters().put('applicationid', retrievedReg.Application_Id__c);
        Test.setCurrentPage(pageRef);

        ApexPages.StandardController stdController = new ApexPages.StandardController(reg);
        return new CustomerOnboardingController(stdController);
    }

    public class TestException extends Exception {}

}