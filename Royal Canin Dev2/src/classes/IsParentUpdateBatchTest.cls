@istest(seeAllData = true)

public class IsParentUpdateBatchTest {
   static testMethod void IsParent(){
   List<Account> accountssList = new List<Account>();
     Account accc = new Account();
     accc.Name='Test Account';
     accc.Phone = '1234567890';
     accc.Account_Status__c = 'Prospect';
     accc.Customer_Type__c = 'Retailer';    
     accc.billingstreet = 'qwer';
     accc.billingcity = 'St.Louis';
     accc.billingstate = 'MO';    
     accc.billingpostalcode = '12345';
     accc.BillingCountry = 'USA';    
     accc.shippingstreet = 'qwer';
     accc.shippingcity = 'St.Louis';
     accc.shippingstate = 'MO';
     accc.shippingpostalcode = '12345';
     accc.shippingCountry = 'USA';     
     accc.Is_Parent__c = 'No';     
     accc.Validated__c = true;
    
      insert accc;
  
     Account accc2 = new Account();
     accc2.Name='Test Account2';
     accc2.Phone = '1234567840';
     accc2.Account_Status__c = 'Prospect2';
     accc2.Customer_Type__c = 'Retailer2';    
     accc2.billingstreet = 'qwer2';
     accc2.billingcity = 'St.Louis2';
     accc2.billingstate = 'MO2';    
     accc2.billingpostalcode = '12345';
     accc2.BillingCountry = 'USA2';    
     accc2.shippingstreet = 'qwer2';
     accc2.shippingcity = 'St.Louis2';
     accc2.shippingstate = 'MO';
     accc2.shippingpostalcode = '12345';
     accc2.shippingCountry = 'USA';
     accc2.parentId = accc.Id;
     accc2.Is_Parent__c = 'No';
     
     accc2.Validated__c = true;
     insert accc2;
     
     Account accc3 = new Account();
     accc3.Name='Test Account3';
     accc3.Phone = '1234567841';
     accc3.Account_Status__c = 'Prospect3';
     accc3.Customer_Type__c = 'Retailer3';    
     accc3.billingstreet = 'qwer3';
     accc3.billingcity = 'St.Louis3';
     accc3.billingstate = 'MO';    
     accc3.billingpostalcode = '12345';
     accc3.BillingCountry = 'USA';    
     accc3.shippingstreet = 'qwer3';
     accc3.shippingcity = 'St.Louis3';
     accc3.shippingstate = 'MO';
     accc3.shippingpostalcode = '12345';
     accc3.shippingCountry = 'USA';
     //accc2.parentId = accc.Id;
     accc3.Is_Parent__c = 'Yes';
     
     accc3.Validated__c = true;
     insert accc3;
             
     List<String> ids= new List<String>();
     ids.add(accc2.Id);
     ids.add(accc.Id);
     ids.add(accc3.Id);
     //accountssList.add(accc);
     //accountssList.add(accc2);
     //insert accountssList; 
    accountssList = [Select id,name,Is_Parent__c,parentId,(select id,name, is_parent__c from ChildAccounts) from Account where id in :ids];
     //test.startTest();
     Database.BatchableContext bc;
     IsParentUpdateBatch ipub = new IsParentUpdateBatch();
     //Database.QueryLocator st=ipub.start(bc);
     ipub.execute(bc,accountssList);
     ipub.finish(bc);
     
   //  test.stopTest();
   }
}