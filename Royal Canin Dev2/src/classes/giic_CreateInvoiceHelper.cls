/************************************************************************************
Version : 1.0
Created Date : 26 Sep 2018
Function : Invoice creation related functions
Modification Log : 

* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/
public with sharing class giic_CreateInvoiceHelper {
    /*
    * Method name : createShipment
    * Description : create shipment records
    * Return Type : Map<String, Object>
    * Parameter : wHASId (Warehouse Shipment Advice Staging id)
    */
    
    public static Savepoint sp;
    
    @auraEnabled
    public static Map<String, Object> createShipment(String wHASId){
        sp = Database.setSavepoint();
        Map<String, Object> mapResult = new Map<String, Object>();
        try
        {   
            List<gii__WarehouseShipmentAdviceStaging__c> lstWHSAStaging = giic_SOShipmentUtility.getWHSAStaging(wHASId);
            if(!lstWHSAStaging.isEmpty() && lstWHSAStaging[0].giic_ProcessStatus__c != giic_Constants.PROCESSED){
                if(lstWHSAStaging[0].giic_isTaxCommitted__c != true){ mapResult.put('errorMsg', giic_Constants.INVOICE_TAX_ERROR); return mapResult;}
                
                Map<String,Object> mapDuplicateSHipment = giic_WHSAdviceTriggerHelper.checkDuplicateShipment(lstWHSAStaging); // check for duplicate shipment is already processed
                if(!mapDuplicateSHipment.containsKey(wHASId)){
                    Date shipDate = null;
                    if(lstWHSAStaging[0].giic_ShipDate__c != null ) shipDate = lstWHSAStaging[0].giic_ShipDate__c;
                    String soNumber = '';
                    if(lstWHSAStaging[0].giic_WHSShipNo__r.giic_SalesOrder__r.Name != null && lstWHSAStaging[0].giic_WHSShipNo__r.giic_SalesOrder__r.Name != '') soNumber = lstWHSAStaging[0].giic_WHSShipNo__r.giic_SalesOrder__r.Name;

                    mapResult.put('shipDate', String.valueOf(shipDate));
                    mapResult.put('soNumber', soNumber);
                    mapResult.put('recId', wHASId);
                    mapResult.put('fulfilmentNumber', lstWHSAStaging[0].giic_WHSShipNo__c);
                     //call shipment method
                    if(!lstWHSAStaging[0].Warehouse_Shipment_Advice_Lines_Staging__r.isEmpty() && giic_SOShipmentUtility.checkFulfilmentLines(lstWHSAStaging[0].Id, lstWHSAStaging[0].Warehouse_Shipment_Advice_Lines_Staging__r.size(), lstWHSAStaging[0].giic_WHSShipNo__c)){
                        Map<String, String> mapFulfilmentLines = new Map<String, String>();
                        for(gii__WarehouseShipmentAdviceLinesStaging__c objLine : lstWHSAStaging[0].Warehouse_Shipment_Advice_Lines_Staging__r){
                            mapFulfilmentLines.put(objLine.giic_WHSShipLineNo__r.giic_SalesOrderLine__r.Name, objLine.giic_WHSShipLineNo__r.Name);
                        }
                        mapResult.put('mapFulfilmentLines', mapFulfilmentLines);
                        
                        // get inventory reserves for shipment
                        Map<String, List<Id>> mapIReservsQuickShip = giic_SOShipmentUtility.getIRforShipment(soNumber, String.valueOf(shipDate), mapFulfilmentLines);
                        //system.debug('mapIReservsQuickShip:' + mapIReservsQuickShip);
                         //Create Quick ship
                         giic_SOShipmentUtility.createQuickShip(mapIReservsQuickShip);
                         
                         List<gii__Shipment__c> lstShipment = [Select Id, gii__SalesOrder__c,gii__SalesOrder__r.giic_SOStatus__c , gii__SalesOrder__r.giic_CustEmail__c, gii__SalesOrder__r.gii__PaymentMethod__c, ( Select Id, gii__SalesOrderLine__c, gii__SalesOrderLine__r.giic_ProductLot__c from gii__ShipmentDetails__r) From gii__Shipment__c where gii__ForwardToInvoice__c = false and gii__ShippedDate__c = :shipDate  and gii__SalesOrder__r.Name = :soNumber];
                        if(lstShipment.isEmpty()){
                            lstWHSAStaging[0].giic_ProcessStatus__c = giic_Constants.PROCESSEDWITHERROR; mapResult.put('errorMsg', system.label.giic_Recordcannotbeprocessed);
                            giic_CyberSourceRCCCUtility.handleErrorFuture(wHASId, giic_Constants.USER_STORY_UPDATE_SOL, giic_Constants.UPDATE_SOL_ERROR_CODE, system.label.giic_ShipmentNotCreated);
                        }else{
                            if(lstShipment[0].gii__SalesOrder__r.giic_SOStatus__c == giic_Constants.SO_PICKED){
                                update new gii__SalesOrder__c(Id = lstShipment[0].gii__SalesOrder__c, giic_SOStatus__c = giic_Constants.SO_SHIPPED);
                            } 
                            if(!lstShipment[0].gii__ShipmentDetails__r.isEmpty()){
                                List<gii__SalesOrderLine__c> lstSOL = new List<gii__SalesOrderLine__c>();
                                for(gii__ShipmentDetail__c objShipDetail : lstShipment[0].gii__ShipmentDetails__r){
                                    gii__SalesOrderLine__c sol = new gii__SalesOrderLine__c();
                                    sol.id = objShipDetail.gii__SalesOrderLine__c; 
                                    sol.giic_LineStatus__c = giic_Constants.SHIPPED;
                                    lstSOL.add(sol); 
                                }
                                if(!lstSOL.isEmpty()) update lstSOL; 
                            }
                        }
                    }else{
                        lstWHSAStaging[0].giic_ProcessStatus__c = giic_Constants.PROCESSEDWITHERROR;
                        update lstWHSAStaging[0]; mapResult.put('errorMsg', Label.giic_LinesNotFound);
                        giic_CyberSourceRCCCUtility.handleErrorFuture(wHASId, giic_Constants.USER_STORY_SHIPMENTCREATION, giic_Constants.UPDATE_SOL_ERROR_CODE, Label.giic_LinesNotFound);
                    }
                    update lstWHSAStaging[0];
                }else{
                    update new gii__WarehouseShipmentAdviceStaging__c(Id = wHASId, giic_ProcessStatus__c = giic_Constants.PROCESSEDWITHERROR);  mapResult.put('errorMsg', String.valueOf(mapDuplicateSHipment.get(wHASId)));
                    giic_CyberSourceRCCCUtility.handleErrorFuture(wHASId, giic_Constants.USER_STORY_SHIPMENTCREATION, giic_Constants.ERROR_INVALIDRECORD, String.valueOf(mapDuplicateSHipment.get(wHASId)));
                }
            }else{   mapResult.put('errorMsg', Label.giic_RecordAlreadyProcessed); }   
        }catch(Exception ex) {
            Database.rollback(sp); system.debug('exp: createShipment: ' + ex.getMessage() + ex.getLineNumber()) ; 
            update new gii__WarehouseShipmentAdviceStaging__c(id = wHASId, giic_ProcessStatus__c = giic_Constants.PROCESSEDWITHERROR); mapResult.put('errorMsg', system.label.giic_Recordcannotbeprocessed); 
            giic_CyberSourceRCCCUtility.handleErrorFuture(wHASId, giic_Constants.USER_STORY_SHIPMENTCREATION, giic_Constants.UPDATE_SOL_ERROR_CODE, 'createShipment:' +  ex.getMessage()+ ' line number::'+ex.getLineNumber());
        }
        System.debug('mapResult:::' + mapResult); //do not remove this debug
        return mapResult;
    }

     /*
    * Method name : createInvoiceFromShipment
    * Description : create invoice records from shipment
    * Return Type : boolean
    * Parameter : String soNumber, String shipDate, String fulfilmentNumber, String strmapFulfilmentLines
    */
    @auraEnabled
    public static boolean createInvoiceFromShipment(String strmapRequest, String strmapFulfilmentLines){
       if(sp == null) sp = Database.setSavepoint();
       Map<string,object> mapRequest = new Map<string,object>();
       mapRequest = (Map<String, Object>)JSON.deserializeUntyped(strmapRequest);
       String recId = mapRequest.containsKey('recId') ? (String)mapRequest.get('recId') : '';
       String soNumber = mapRequest.containsKey('soNumber') ? (String)mapRequest.get('soNumber') : '';
       String shipDate =  mapRequest.containsKey('shipDate') ? (String)mapRequest.get('shipDate') : '';
       String fulfilmentNumber = mapRequest.containsKey('fulfilmentNumber') ? (String)mapRequest.get('fulfilmentNumber') : '';
       Map<String, String> mapFulfilmentLines = new Map<String, String>();
       if(strmapFulfilmentLines != null)
                mapFulfilmentLines = ( Map<String, String>) JSON.deserialize(strmapFulfilmentLines,  Map<String, String>.class);
                //system.debug('recId='+recId+' soNumber='+soNumber+' shipDate='+shipDate+' fulfilmentNumber='+fulfilmentNumber+' mapFulfilmentLines='+mapFulfilmentLines);
        if(recId != '' && soNumber != '' && shipDate != '' && fulfilmentNumber != '' && !mapFulfilmentLines.isEmpty()){
        try{
            Date shipmentDate = Date.valueOf(shipDate);
            List<gii__Shipment__c> lstShipment = [Select Id, gii__SalesOrder__c,gii__SalesOrder__r.giic_SOStatus__c , gii__SalesOrder__r.giic_CustEmail__c, gii__SalesOrder__r.gii__PaymentMethod__c, ( Select Id, gii__SalesOrderLine__c, gii__SalesOrderLine__r.giic_ProductLot__c from gii__ShipmentDetails__r) From gii__Shipment__c where gii__ForwardToInvoice__c = false and gii__ShippedDate__c = :shipmentDate  and gii__SalesOrder__r.Name = :soNumber];
            if(!lstShipment.isEmpty()){
                gii__Shipment__c objShip = lstShipment[0];
                String shipmentId = objShip.Id;  String soId = objShip.gii__SalesOrder__c; String customerEmail = objShip.gii__SalesOrder__r.giic_CustEmail__c != null ? objShip.gii__SalesOrder__r.giic_CustEmail__c : '';
                //create invoice from shipment
                giic_SOShipmentUtility.createInvoice(shipmentId); 
                if(objShip.gii__SalesOrder__r.giic_SOStatus__c != giic_Constants.SO_SETTLED){
                    gii__SalesOrder__c objSO = new gii__SalesOrder__c(Id = soId , giic_SOStatus__c = giic_Constants.SO_INVOICED);
                    if(lstShipment[0].gii__SalesOrder__r.gii__PaymentMethod__c != giic_Constants.SO_CREDITCARD) objSO.giic_SOStatus__c = giic_Constants.SO_SETTLED;
                    update objSO;
                }
                
                List<gii__SalesOrderLine__c> lstSOL = new List<gii__SalesOrderLine__c>();
                set<id> setSOIL = new set<id>();
                if(objShip.gii__ShipmentDetails__r.size() > 0){
                    for(gii__ShipmentDetail__c objSD : objShip.gii__ShipmentDetails__r){
                            gii__SalesOrderLine__c sol = new gii__SalesOrderLine__c();
                            sol.id = objSD.gii__SalesOrderLine__c; 
                            sol.giic_LineStatus__c = giic_Constants.SO_INVOICED;
                            if(lstShipment[0].gii__SalesOrder__r.gii__PaymentMethod__c != giic_Constants.SO_CREDITCARD) sol.giic_LineStatus__c = giic_Constants.SO_SETTLED;
                            lstSOL.add(sol); setSOIL.add(sol.id);
                   }
                   if(!lstSOL.isEmpty()) update lstSOL;  
                   giic_SOShipmentUtility.updateInvoiceDetails(setSOIL, customerEmail, fulfilmentNumber, mapFulfilmentLines); // update invoice details with product lot and fulfilment line number
                   List<gii__OrderInvoice__c> lstInvoice = [SELECT Id, gii__NetAmount__c,gii__PaymentMethod__c, gii__SalesOrder__r.gii__PaymentMethod__c, giic_Status__c, giic_SettlementStatus__c,gii__SalesOrder__c,gii__SalesOrder__r.Name,giic_WarehouseShippingOrderStaging__c, giic_WarehouseShippingOrderStaging__r.Name FROM gii__OrderInvoice__c 
                                                where gii__SalesOrder__c  = : soId and giic_WarehouseShippingOrderStaging__c =: fulfilmentNumber]; 
                    if(!lstInvoice.isEmpty()){
                       giic_SOShipmentUtility.createInvoiceAdditionalCharge(lstInvoice); // create Invoice AdditionalCharge record if SO have any unprocessed Additional charge
                       giic_SOShipmentUtility.createARInvoicePayments(lstInvoice); //create AR Invoice Payments
                       gii__OrderInvoice__c objInvoice = lstInvoice[0];
                       objInvoice.giic_Status__c = giic_Constants.INPROGRESS;
                       if(objShip.gii__SalesOrder__r.gii__PaymentMethod__c != giic_Constants.SO_CREDITCARD){
                           objInvoice.giic_SettlementStatus__c = giic_Constants.CONFIRMED; // Changed settlement status for invoice as settled
                       }
                       Update objInvoice;
                       if(objShip.gii__SalesOrder__r.gii__PaymentMethod__c == giic_Constants.SO_CREDITCARD){   // call RC Settlement API for settlement
                            callSettlementAPI(objInvoice.gii__SalesOrder__r.Name, objInvoice.giic_WarehouseShippingOrderStaging__r.Name, objInvoice.gii__NetAmount__c, objInvoice.Id, soId, setSOIL);
                       }
                   }
                }
                
                update new gii__WarehouseShipmentAdviceStaging__c(id = recId, giic_ProcessStatus__c = giic_Constants.PROCESSED);
                
                return true;
                
            }else{giic_CyberSourceRCCCUtility.handleErrorFuture(recId, giic_Constants.USER_STORY_UPDATE_SOL, giic_Constants.UPDATE_SOL_ERROR_CODE, system.label.giic_ShipmentNotCreated); return false; } 
        }
        catch(Exception ex) {
            Database.rollback(sp);  system.debug('exp: createInvoiceFromShipment: ' + ex.getMessage() + ex.getLineNumber()) ; 
            update new gii__WarehouseShipmentAdviceStaging__c(id = recId, giic_ProcessStatus__c = giic_Constants.PROCESSEDWITHERROR);
            giic_CyberSourceRCCCUtility.handleErrorFuture(recId, giic_Constants.USER_STORY_INVOICECREATION, giic_Constants.DML_EX_CODE, 'createInvoiceFromShipment:' + ex.getMessage()+ ' line number::'+ex.getLineNumber());
            return false; } // error log
        }
         return false;     
    }

    // call RC Settlement API for settlement
    @Future(callout=true) 
     public static void  callSettlementAPI(String orderNumber, String fulfillmentNumber, Decimal totalAmount, String recordId, String soId, set<id> setSOIL){
         if(orderNumber != null && fulfillmentNumber != null && totalAmount != null && recordId != null){
            try{
                gii__OrderInvoice__c objInvoice = new gii__OrderInvoice__c(Id = recordId);

                Map<String, Object> mapParams = new Map<String, Object>();
                giic_BillToWrapper billToWrapper = new giic_BillToWrapper();
                billToWrapper.OrderNumber = orderNumber;
                billToWrapper.FulfillmentNumber = fulfillmentNumber;
                billToWrapper.PaymentCurrency = 'USD'; //default to USD
                
                mapParams.put('billTo', billToWrapper);
                mapParams.put('totalAmount', totalAmount);
                mapParams.put('recordId', recordId);
                mapParams.put('isFuture', true);
                Map<String, Object> settlementRequestRCCCRes = new Map<String, Object>();
                if(!Test.isRunningTest()) settlementRequestRCCCRes =  giic_CyberSourceRCCCUtility.settlementRequestRCCC(mapParams);
                
                if(settlementRequestRCCCRes.containsKey('error') || settlementRequestRCCCRes.containsKey('exception')){
                    List<Integration_Error_Log__c> lstError = giic_CyberSourceRCCCUtility.handleError(recordId, giic_Constants.RCCC_USER_STORY, giic_Constants.RCCC_USER_STORY, settlementRequestRCCCRes.containsKey('error') ? String.valueOf(settlementRequestRCCCRes.get('error')) : String.valueOf(settlementRequestRCCCRes.get('exception')));
                    if(!lstError.isEmpty()) insert lstError;
                    objInvoice.giic_SettlementStatus__c = giic_Constants.PROCESSWITHERROR; update objInvoice;
                }else{
                    objInvoice.giic_SettlementStatus__c = giic_Constants.CONFIRMED;
                    update objInvoice;
                    if(soId != null && soId != '') update new gii__SalesOrder__c(Id = soId , giic_SOStatus__c = giic_Constants.SO_SETTLED);
                    if(!setSOIL.isEmpty()){
                        List<gii__SalesOrderLine__c> lstSOL = new List<gii__SalesOrderLine__c>();
                        for(Id solId : setSOIL){
                            gii__SalesOrderLine__c sol = new gii__SalesOrderLine__c();
                            sol.id = solId; 
                            sol.giic_LineStatus__c = giic_Constants.SO_SETTLED;
                            lstSOL.add(sol); 
                        }
                        if(!lstSOL.isEmpty()) update lstSOL;
                    }
                }
             }catch(Exception ex) {
                List<Integration_Error_Log__c> lstError = giic_CyberSourceRCCCUtility.handleError(recordId, giic_Constants.RCCC_USER_STORY, giic_Constants.DML_EX_CODE, 'callSettlementAPI:' + ex.getMessage()+ ' line number::'+ex.getLineNumber());
                if(!lstError.isEmpty()) insert lstError;
             }
         }
     }
    
    /* create by - ranu
     *
     */
      /*
    * Method name : updateSOLandIR
    * Description : update sales order lines and inventory reserves based on shipment record
    * Return Type : Map<String,Object>
    * Parameter : String soNumber, String shipDate, String fulfilmentNumber, String strmapFulfilmentLines
    */
    @auraEnabled
    public static Map<String,Object>  updateSOLandIR(String wHASId)
    {
        Map<String,Object> result = new Map<String,Object>();
        if(wHASId != null && wHASId != ''){
            List<gii__WarehouseShipmentAdviceLinesStaging__c> lstWSAL = new List<gii__WarehouseShipmentAdviceLinesStaging__c>();  
            List<Integration_Error_Log__c> lstErrors = new List<Integration_Error_Log__c>();
            Set<String> setErrorRecords = new Set<String>();
            List<gii__WarehouseShipmentAdviceStaging__c> lstWSAS =  giic_SOShipmentUtility.getWHSAStaging(whasId);
            if(lstWSAS[0].giic_isTaxCommitted__c){ throw new AuraHandledException(giic_Constants.TAX_COMMITED_ERROR); }
            try
            {
                Map<String,Object> mapDuplicateSHipment = giic_WHSAdviceTriggerHelper.checkDuplicateShipment(lstWSAS); // check for duplicate shipment is already processed
                if(!mapDuplicateSHipment.containsKey(wHASId)){
                    Map<String,Object> mapReturnHeader = giic_WHSAdviceTriggerHelper.validateWSAdviceStaging(lstWSAS); // validate header records
                    system.debug('mapReturnHeader--->>>>>>>>> ' + mapReturnHeader); //DO NOT REMVE THIS DEBUG LOG 
                    if(mapReturnHeader.containsKey('lstErrors') && mapReturnHeader.get('lstErrors') != null) lstErrors.addAll((List<Integration_Error_Log__c>) mapReturnHeader.get('lstErrors'));
                    if(mapReturnHeader.containsKey('setErrorRecords') && mapReturnHeader.get('setErrorRecords') != null) setErrorRecords.addAll((Set<String>) mapReturnHeader.get('setErrorRecords'));
                    if(!setErrorRecords.contains(wHASId))
                    {
                        lstWSAL = lstWSAS[0].Warehouse_Shipment_Advice_Lines_Staging__r;
                        if(lstWSAL.isEmpty()){
                            update new gii__WarehouseShipmentAdviceStaging__c(id = whasId, giic_ProcessStatus__c = giic_Constants.PROCESSEDWITHERROR);
                            result.put('ERROR',system.label.giic_LinesNotFound); 
                            giic_CyberSourceRCCCUtility.handleErrorFuture(wHASId, giic_Constants.USER_STORY_UPDATE_SOL, giic_Constants.UPDATE_SOL_ERROR_CODE, Label.giic_LinesNotFound);
                        }else if(giic_SOShipmentUtility.checkFulfilmentLines(lstWSAS[0].Id, lstWSAS[0].Warehouse_Shipment_Advice_Lines_Staging__r.size(), lstWSAS[0].giic_WHSShipNo__c)){
                            Map<String,Object> mapReturnChild = giic_WHSAdviceTriggerHelper.validateWSALineStaging(lstWSAL); // validate child records
                            system.debug('mapReturnChild--->>>>>>>>> ' + mapReturnChild); //DO NOT REMVE THIS DEBUG LOG 
                            if(mapReturnChild.containsKey('lstErrors') && mapReturnChild.get('lstErrors') != null) lstErrors.addAll((List<Integration_Error_Log__c>) mapReturnChild.get('lstErrors'));
                            if(mapReturnChild.containsKey('setErrorRecords') && mapReturnChild.get('setErrorRecords') != null) setErrorRecords.addAll((Set<String>) mapReturnChild.get('setErrorRecords'));
            
                            if(lstWSAL != null && lstWSAL.size()>0 && !lstWSAL[0].giic_WHSShipAdviceStaging__r.giic_isTaxCommitted__c && !setErrorRecords.contains(wHASId))
                            {  
                                //result = giic_SOShipmentUtility.updateSOLandIR(lstWSAL,lstWSAL[0].giic_WHSShipAdviceStaging__r.giic_ShipDate__c,lstWSAL[0].giic_WHSShipAdviceStaging__r.giic_WHSShipNo__r.giic_SalesOrder__r.Name,lstWSAL[0].giic_WHSShipAdviceStaging__r.giic_Warehouse__c,true);
                                result = giic_SOShipmentUtility.updateSOLandIR(new Map<String, Object>{'lstWHSAStaging'=>lstWSAS});
                               List<Integration_Error_Log__c> lstUOMErrors = (List<Integration_Error_Log__c>)result.get('lstError');
                               if(!lstUOMErrors.isEmpty()){
                                  lstErrors.addAll(lstUOMErrors);//insert lstUOMErrors; 
                                  result.put('ERROR',system.label.giic_Recordcannotbeprocessed);
                               }  
                                   
                            } else { 
                                result.put('ERROR',system.label.giic_Recordcannotbeprocessed); 
                            }
                        }
                    }else
                    {
                        result.put('ERROR', system.label.giic_Recordcannotbeprocessed); 
                    }
                    if(lstErrors != null && !lstErrors.isEmpty()) insert lstErrors;
                }else{
                    update new gii__WarehouseShipmentAdviceStaging__c(Id = wHASId, giic_ProcessStatus__c = giic_Constants.PROCESSEDWITHERROR);
                    giic_CyberSourceRCCCUtility.handleErrorFuture(wHASId, giic_Constants.USER_STORY_UPDATE_SOL, giic_Constants.ERROR_INVALIDRECORD, String.valueOf(mapDuplicateSHipment.get(wHASId)));
                    result.put('ERROR',String.valueOf(mapDuplicateSHipment.get(wHASId)));
                }
            }
            catch(Exception ex) { 
                system.debug('exp:' + ex.getMessage() + ex.getLineNumber()) ; 
                giic_CyberSourceRCCCUtility.handleErrorFuture(wHASId, giic_Constants.USER_STORY_UPDATE_SOL, giic_Constants.DML_EX_CODE, 'updateSOLandIR:' + ex.getMessage()+ ' line number::'+ex.getLineNumber());
                result.put('ERROR',system.label.giic_Recordcannotbeprocessed);
            }
            system.debug('1:' + result );//DO NOT REMVE THIS DEBUG LOG 
        }
        return result;
    }
    
    
    /*
    * Method name : CalculateTax, created by - ranu
    * Description : Based on Warehouse shipment Record calcuate tax for SOL's.
    * Return Type : Map<String,Object>
    * Parameter : wHASId (Warehouse shipment Record id)
    */
    @auraEnabled
    public static Map<String,Object> CalculateTax(String wHASId) // null check
    {
        List<gii__WarehouseShipmentAdviceLinesStaging__c> lstWSAL = new List<gii__WarehouseShipmentAdviceLinesStaging__c>();        
        List<String> lstSOLName =  new List<String>();        
        Set<String> setOrderNo = new Set<String>();
        list<gii__SalesOrderLine__c> lstSOlineUpdate = new list<gii__SalesOrderLine__c>();
        Id errorRec = wHASId; //object id to store integration error log;
        Map<String,Object> result = new Map<String,Object>();
        if(wHASId != null && wHASId != ''){                      
            try
            {
                List<gii__WarehouseShipmentAdviceStaging__c> lstWSAS =  giic_SOShipmentUtility.getWHSAStaging(whasId);
               lstWSAL = lstWSAS[0].Warehouse_Shipment_Advice_Lines_Staging__r;
               if(lstWSAL != null && lstWSAL.size()>0)
                {     
                    errorRec = lstWSAL[0].giic_WHSShipAdviceStaging__c;
                    for(gii__WarehouseShipmentAdviceLinesStaging__c obj : lstWSAL)
                    {  
                        setOrderNo.add((obj.giic_WHSShipLineNo__r.giic_SalesOrderLine__r.Name).trim()); //giic_OrderLineNo__c - text field
                    }
                    
                    if(setOrderNo.size()>0)
                    {
                        String soid = '';
                        List<gii__SalesOrderLine__c> lstSOL = new List<gii__SalesOrderLine__c>();
                        lstSOL= [select id,gii__Product__c,gii__Product__r.gii__SellingUnitofMeasure__c,gii__Product__r.gii__SellingUnitofMeasure__r.name,Name,gii__OrderQuantity__c,gii__CancelledQuantity__c,gii__LineStatus__c,gii__SalesOrder__c,gii__SalesOrder__r.gii__Warehouse__r.giic_WarehouseCode__c from gii__SalesOrderLine__c 
                                        where Name in : setOrderNo order by name];
                        if(lstSOL.size()>0)
                        {                   
                            soid = lstSOL[0].gii__SalesOrder__c;
                            for(gii__SalesOrderLine__c objSOL : lstSOL)  { lstSOLName.add(objSOL.Name); }
                            
                            if(lstSOLName.size()>0)
                            {
                                List<gii__SalesOrder__c>  lstobjOrder = new List<gii__SalesOrder__c>();
                                lstobjOrder = [select id,gii__Warehouse__r.giic_WarehouseCode__c,giic_TaxExempt__c, // use above query
                                               		  gii__Account__r.Check_if_you_are_Tax_Exempt__c,
                                               		  gii__Account__r.Resale_Sales_Tax_Cert_Expiration_Date__c
                                               		  from gii__SalesOrder__c where id =: soid ];
                                if(lstobjOrder.size()>0)
                                {
                                    //Check for Account Tax Exemption before Calculating Tax
                                    if(!lstobjOrder[0].giic_TaxExempt__c)
                                    {
                                        result = giic_AvalaraServiceProviderClass.CalculateTaxforSO(lstobjOrder[0].id,lstSOLName,lstobjOrder[0].gii__Warehouse__r.giic_WarehouseCode__c,giic_Constants.COMMIT_T_TRUE,true);
                                        if(result.containsKey('ISSUCCESS') && result.containsKey('ISSUCCESS') == true) 
                                        {
                                            result.put('SUCCESS',true);
                                            gii__WarehouseShipmentAdviceStaging__c wsaObj = new gii__WarehouseShipmentAdviceStaging__c(id = errorRec,giic_isTaxCommitted__c = true );
                                            update wsaObj; 
                                        }
                                        else { 
                                            result.put('ERROR', system.label.giic_TaxCalculationFailError);  
                                            giic_CyberSourceRCCCUtility.handleErrorFuture(errorRec, giic_Constants.USER_STORY_TAX_CALC, giic_Constants.TAX_CALC_ERROR_CODE, giic_Constants.TAX_CALCULATIONFAIL_ERROR);
                                        }
                                    }else
                                    {
                                        gii__WarehouseShipmentAdviceStaging__c wsaObj = new gii__WarehouseShipmentAdviceStaging__c(id = errorRec,giic_isTaxCommitted__c = true );
                                        update wsaObj;  result.put('SUCCESS',true);
                                    }
                                        
                                }
                            }
                        }
                    }
                }
        }catch(Exception ex) { 
            system.debug('exp:' + ex.getMessage() + ex.getLineNumber()); result.put('ERROR',ex.getMessage());
            giic_CyberSourceRCCCUtility.handleErrorFuture(errorRec, giic_Constants.USER_STORY_TAX_CALC, giic_Constants.DML_EX_CODE, 'CalculateTax:' + ex.getMessage()+ ' line number::'+ex.getLineNumber());
        }
    }
    return result;
        
    }
   
}