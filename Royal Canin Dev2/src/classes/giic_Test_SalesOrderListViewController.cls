/************************************************************************************
Version : 1.0
Name : giic_Test_SalesOrderListViewController
Created Date : 20 Aug 2018
Function : test class for giic_SalesOrderListViewController
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/

@isTest(SeeAllData = false)
private class giic_Test_SalesOrderListViewController {

     @testSetup   
    static void setup()
    {
        //Insert warehouse
        giic_Test_DataCreationUtility.insertWarehouse();
        //Create System policy record
        giic_Test_DataCreationUtility.testSystemPolicyCreation();
        //Insert Global param
        giic_Test_DataCreationUtility.insertGlobalParametersCustomSetting();
        //Insert Consumer Accounts
        giic_Test_DataCreationUtility.insertConsumerAccount();
        //Insert warehouse
        giic_Test_DataCreationUtility.insertWarehouse_N(2);
        //Insert Account Reference
        giic_Test_DataCreationUtility.getAccountReference_N(giic_Test_DataCreationUtility.lstAccount);
        //Add products
        giic_Test_DataCreationUtility.insertProduct();
        giic_Test_DataCreationUtility.createCarrier();
        //Add Multiple Sales Order and Sales Order Lines
        // giic_Test_DataCreationUtility.insertBulkSalesOrder();
        // giic_Test_DataCreationUtility.insertBulkSOLine();
        giic_Test_DataCreationUtility.insertSalesOrder();
        //Create promotions
        giic_Test_DataCreationUtility.createPromotions();
        giic_Test_DataCreationUtility.createPromotionLines();
        giic_Test_DataCreationUtility.createPromotionLineStyle();
        giic_Test_DataCreationUtility.createAdditionalCharge();
        //Create user record
        giic_Test_DataCreationUtility.CreateAdminUser();
        giic_Test_DataCreationUtility.insertSalesOrderStaging();
        giic_Test_DataCreationUtility.insertSalesOrderLineStaging();
        //Create Account Program
        //giic_Test_DataCreationUtility.createAccProg();
        giic_Test_DataCreationUtility.createRateTable();
        

    }

    private static testMethod void positivetest() {
         User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
         System.runAs(u){
        
          List<gii__SalesOrder__c> lstSalesOrder1 = [select id, name, gii__DropShip__c, giic_DropShipWHMapping__c, gii__Warehouse__c, gii__ShipToZipPostalCode__c, gii__AccountReference__r.giic_WarehouseGroup__c, giic_Source__c from gii__SalesOrder__c];
        for(gii__SalesOrder__c so : lstSalesOrder1){
           // so.giic_Source__c = 'OLP';
        }
       // update lstSalesOrder1;
        system.debug('lstSalesOrder1--->>>> ' + lstSalesOrder1);
        List<gii__SalesOrder__c> lstSalesOrder2 = [select id, name, gii__DropShip__c, giic_DropShipWHMapping__c, gii__Warehouse__c, gii__ShipToZipPostalCode__c, gii__AccountReference__r.giic_WarehouseGroup__c, giic_Source__c from gii__SalesOrder__c where gii__ShipToZipPostalCode__c != null AND gii__AccountReference__c != null AND gii__AccountReference__r.giic_WarehouseGroup__c != null AND ((giic_Source__c = 'INSITE' AND gii__DropShip__c = true) OR giic_Source__c = 'OLP')];
        
       system.debug('lstSalesOrder2--->>>> ' + lstSalesOrder2);  
             
        List<gii__SalesOrderStaging__c> lstSOStaging = [Select id,giic_AddressValidated__c from gii__SalesOrderStaging__c];
        lstSOStaging[1].giic_AddressValidated__c = true;
        update lstSOStaging[1];
        
        Test.startTest();
        Test.setMock(WebServiceMock.class, new giic_Test_FedExCalloutMock());
        
        ApexPages.StandardSetController sc = new ApexPages.StandardSetController(lstSOStaging);
        sc.setSelected(lstSOStaging);
        giic_SalesOrderListViewController objSOList = new giic_SalesOrderListViewController(sc);
        objSOList.onLoad();
        objSOList.goBack();
        objSOList.goToSOS();
        Test.stopTest();
         }
    }
    
    private static testMethod void positivetestwithErrors() {
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
         System.runAs(u){
        List<gii__SalesOrderStaging__c> lstSOStaging = [Select id,giic_AddressValidated__c from gii__SalesOrderStaging__c];
        lstSOStaging[1].giic_AddressValidated__c = true;
        lstSOStaging[1].giic_Status__c = 'Draft';
        lstSOStaging[1].giic_Source__c = 'OLP';
        lstSOStaging[1].giic_ShipToName__c = 'Test';
        update lstSOStaging[1];
        
        Test.startTest();
        
        List<gii__SalesOrderStaging__c> lstSOS = new List<gii__SalesOrderStaging__c>{lstSOStaging[1]};
        ApexPages.StandardSetController sc = new ApexPages.StandardSetController(lstSOStaging);
        sc.setSelected(lstSOS);
        giic_SalesOrderListViewController objSOList = new giic_SalesOrderListViewController(sc);
        objSOList.onLoad();
        Test.stopTest();
         }
    }
    
    private static testMethod void negativetest() {
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
         System.runAs(u){
        List<gii__SalesOrderStaging__c> lstSOS = new List<gii__SalesOrderStaging__c>{new gii__SalesOrderStaging__c()};
        ApexPages.StandardSetController sc = new ApexPages.StandardSetController(lstSOS);
        giic_SalesOrderListViewController objSOList = new giic_SalesOrderListViewController(sc);
        objSOList.onLoad();
         }
    }
    
    private static testMethod void negativetestCatch() {
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
         System.runAs(u){
        Test.startTest();
        List<gii__SalesOrderStaging__c> lstSOStaging = [Select id,giic_AddressValidated__c from gii__SalesOrderStaging__c];
        lstSOStaging[1].giic_AddressValidated__c = true;
        lstSOStaging[1].giic_Status__c = 'Draft';
        lstSOStaging[1].giic_Source__c = 'INSITE';
       // lstSOStaging[1].giic_Account__c = giic_Test_DataCreationUtility.lstAccount[0].ShipToCustomerNo__c;
        lstSOStaging[1].giic_ShipToName__c = 'Test';
        update lstSOStaging[1];
        
        List<gii__SalesOrderStaging__c> lstSOS = new List<gii__SalesOrderStaging__c>{lstSOStaging[1]};
        ApexPages.StandardSetController sc = new ApexPages.StandardSetController(lstSOStaging);
        sc.setSelected(lstSOS);
        giic_SalesOrderListViewController objSOList = new giic_SalesOrderListViewController(sc);
        objSOList.sObjectApiName = 'gii__SalesOrderStagingLine__c';
        objSOList.onLoad();
        
        Test.stopTest();
         }
    }
}