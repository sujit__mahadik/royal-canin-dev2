/************************************************************************************
Version : 1.0
Created Date : 27-Aug-2018
Function : Test class for Apex controller that incorporats the server side functionality
		   for giic_SOWarehouseDistance component
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/
@isTest
private class giic_Test_SOWHDistanceController {    
    @testSetup
    static void setup(){
        List<Account> testConsumerAccountList = giic_Test_DataCreationUtility.insertConsumerAccount();
        testConsumerAccountList[0].ShippingLatitude = 10.00;
        testConsumerAccountList[0].ShippingLongitude = 10.00;
        Database.update(testConsumerAccountList[0]);
        List<gii__Warehouse__c> testWareList = giic_Test_DataCreationUtility.insertWarehouse();
        testWareList[0].gii__WareHouseStreet__c = '123 Test Street';
        testWareList[0].gii__WareHouseCity__c = 'SUWANEE';
        testWareList[0].gii__WareHouseStateProvince__c = 'GA';
        testWareList[0].gii__WareHouseZipPostalCode__c = '30024';
        testWareList[0].gii__WareHouseCountry__c = 'United States';
        testWareList[0].gii__Geolocation__Longitude__s = 5.0;
        testWareList[0].gii__Geolocation__Latitude__s = 5.0;
        Database.update( testWareList[0]);
        SetupTestData.createCustomSettings();
        giic_Test_DataCreationUtility.testSystemPolicyCreation();
        List<gii__AccountAdd__c> testAccRefList = giic_Test_DataCreationUtility.getAccountReference(testConsumerAccountList);
        Database.insert(testAccRefList);
        List<gii__Product2Add__c> testProductList = giic_Test_DataCreationUtility.insertProduct();
        List<gii__SalesOrder__c> testSOList = giic_Test_DataCreationUtility.insertSalesOrder(testConsumerAccountList[0].Id, testAccRefList[0].Id);
        List<gii__SalesOrderLine__c> testSOLine = giic_Test_DataCreationUtility.insertSOLine(testSOList[0].Id, testProductList[0].Id);
        List<giic_SOWarehouseDistanceController.WarehouseWrapper> wareHouseWrapper = new List<giic_SOWarehouseDistanceController.WarehouseWrapper>();
        giic_SOWarehouseDistanceController.AccountWarehouseWrapper accWareWrapper = new giic_SOWarehouseDistanceController.AccountWarehouseWrapper();
        giic_Test_DataCreationUtility.CreateAdminUser();
    }
    
    public static testMethod void getDataTest(){
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        System.runAs(u){
        Test.startTest();
        Account TestAcc = [SELECT Id, Name FROM Account WHERE Name LIKE 'Test%' LIMIT 1];
        gii__SalesOrder__c testSO = [SELECT Id, gii__Account__c FROM gii__SalesOrder__c WHERE gii__Account__c =: TestAcc.Id LIMIT 1];
        giic_SOWarehouseDistanceController.AccountWarehouseWrapper testReturnedValue =  giic_SOWarehouseDistanceController.getData(testSO.Id);
        Test.stopTest();
        System.assertNotEquals(null, testReturnedValue);
        }
    }


}