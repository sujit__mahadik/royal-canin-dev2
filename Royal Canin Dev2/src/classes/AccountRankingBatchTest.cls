/*
 * Date:             - Developer, Company                          - Description
 * 2015-08-12        - Stevie Yakkel, Acumen Solutions             - Edited
 */
@isTest
private class AccountRankingBatchTest {

    static testMethod void testRankings(){
        integer numAccounts = 10;
        
        SetupTestData.createCustomSettings();
        SetupTestData.createSalesSummaryData(numAccounts);
        
        //create the territory assignment data
        Region__c r = new Region__c (Pillar__c ='VET',Name='test vet region1');
        insert r;
        
        List <Territory__c> terrList = new List <Territory__c>();
        for (integer i=0; i<5; i++){
            Territory__c terr = new Territory__c(Region__c=r.id, Name='VETTerritory'+i);
            terrList.add(terr);
        }
        insert terrList;
        
        List <TerritoryAssignment__c> taList = new List <TerritoryAssignment__c>();
        for (Territory__c t: terrList){
            TerritoryAssignment__c ta = new TerritoryAssignment__c(TerritoryID__c=t.id);
            taList.add(ta);
        }
        insert taList;
        
        integer j = 0;
        for (Account a: SetupTestData.testAccounts){
            //integer tanum = integer.valueOf(j/terrList.size());
            integer tanum = Math.mod(j, terrList.size());
            system.debug('tanum~~~'+tanum);
            a.Current_Year_Territory_Assignment__c = taList[tanum].id;

            a.Current_Fiscal_YTD__c = j * 100000.00;
            a.OLP_Current_Fiscal_YTD__c = j * 10.00; 

            j++;
        }
        update SetupTestData.testAccounts;

        test.startTest();
        Job_Failure_Notification_Email__c jfn = new Job_Failure_Notification_Email__c();
        jfn.Name = 'Email Id';
        jfn.Notification_Email__c = UserInfo.getUserEmail();
        insert jfn;
        AccountRankingBatch arb = new AccountRankingBatch();
        database.executeBatch(arb,1);
        test.stopTest();
        
        //system.debug([select Current_Year_Territory_Assignment__r.TerritoryID__r.Name,Current_Fiscal_YTD__c, Territory_Ranking__c,Region_Ranking__c from Account]);
        system.debug([select Id, Territory_Ranking__c, Region_Ranking__c from Account]);
        //system.assertEquals([select count() from Account where Region_Ranking__c=5],1); //should only be 1 number 5
        //system.assertEquals([select Current_Fiscal_YTD__c from Account where Territory_Ranking__c=1 and Current_Year_Territory_Assignment__r.TerritoryID__r.Name='VETTerritory1'].Current_Fiscal_YTD__c,45000.00);
    }
}