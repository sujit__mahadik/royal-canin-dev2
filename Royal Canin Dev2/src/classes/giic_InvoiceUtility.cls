/************************************************************************************
Version : 1.0
Created Date : 17 Sep 2018
Function : all common method which is used for genrating invoices
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/
public class giic_InvoiceUtility {
    
    /* Method name : CreateInvoices
    * Description : Create Invoices for Sales Orders
    * Return Type : void
    * Parameter : Set of Sales Order Id's
    */
    public static void CreateInvoices(Set<Id> setSOID)
    {
        List<Id> lstInvoiceIds = new List<Id>();
        List<Id> selectedRecordIds = new List<Id>();
        List<Integration_Error_Log__c> lstErrors = new List<Integration_Error_Log__c>();
        Set<String> stErrorIds = new Set<String>();
        string userStory = 'CreateInvoices';
        Map<String, String> relatedToFieldAPi = new Map<String, String> ();
        List<SObject> sourceRecord = new List<SObject>();
        Schema.sObjectType targetType = Schema.getGlobalDescribe().get('gii__ForwardToInvoiceQueue__c');       
        for(gii__ForwardToInvoiceQueue__c obj : [Select Name, Id, gii__SalesOrder__c, 
                                                 gii__SalesOrderLine__c,gii__ServiceOrderLine__c, 
                                                 gii__SalesOrderAdditionalCharge__c 
                                                 from gii__ForwardToInvoiceQueue__c 
                                                 where gii__SalesOrder__c in : setSOID ])
        {
            selectedRecordIds.add(obj.id);
        }
        System.debug('selectedRecordIds==='+selectedRecordIds);
        if(selectedRecordIds.size()>0)
        {                
            Date InvoiceDate = system.today(); 
            String invoiceDateString = InvoiceDate.year() + '-' + InvoiceDate.month() + '-' + InvoiceDate.day() ;         
            String groupBy    = 'SalesOrder';            
            String groupByandInvoiceDate = groupBy + ';' + invoiceDateString ;                 
            try 
            {
                lstInvoiceIds =  gii.OrderInvoicing.createInvoice(selectedRecordIds,groupByandInvoiceDate);    
                System.debug('lstInvoiceIds==='+lstInvoiceIds);
            }
            catch(exception ex)
            {    
                for(gii__ForwardToInvoiceQueue__c obj: [select id from gii__ForwardToInvoiceQueue__c where id in: selectedRecordIds])
                {
                    stErrorIds.add(obj.id);
                    if(relatedToFieldAPi.get('giic_ForwardToInvoiceQueue__c') == null)
                    {
                        relatedToFieldAPi.put('giic_ForwardToInvoiceQueue__c','');
                    }
                    relatedToFieldAPi.put('giic_ForwardToInvoiceQueue__c',obj.id);
                    SObject targetObj = targetType.newSObject();
                    targetObj.id = obj.id;               
                    sourceRecord.add(targetObj); 
                }
                System.debug('stErrorIds==='+stErrorIds);
                if(stErrorIds.size()>0)
                {
                    giic_InvoiceUtility.collectErrors(lstErrors,stErrorIds, userStory, relatedToFieldAPi, sourceRecord, 'Invoice Creation Failed', ex.getMessage());                 
                }                
            }  
            System.debug('lstErrors==='+lstErrors);
            if(!lstErrors.isEmpty())
            {
                insert lstErrors;                   
            }
        } 
    } 
    /* Method name : collectErrors
    * Description : log errors while creating invoices
    * Return Type : void    
    */
    public static void collectErrors(List<Integration_Error_Log__c> lstErrors, Set<String> stErrorIds, String userStory, Map<String, String> relatedToFieldAPi, List<Sobject> sourceRecord, String errorCode, String errorMessage)
    {       
        for(String errFieldApi :relatedToFieldAPi.keySet())
        {
            for(Sobject obj : sourceRecord)
            {
                Integration_Error_Log__c objErr= new Integration_Error_Log__c(
                    Name = userStory,
                    giic_IsActive__c = true,
                    Error_Code__c   = errorCode,
                    Error_Message__c = errorMessage
                );
                String sourceFieldAPi = relatedToFieldAPi.get(errFieldApi);                
                objErr.put(errFieldApi, obj.id);  
                lstErrors.add(objErr);
            }           
        }    
    }
}