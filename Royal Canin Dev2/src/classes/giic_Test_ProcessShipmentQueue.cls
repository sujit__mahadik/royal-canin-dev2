/************************************************************************************
Version : 1.0
Name : giic_Test_ProcessShipmentQueue
Created Date : 10 Oct 2018
Function : test class for giic_ProcessShipmentQueue
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/

@isTest(SeeAllData = false)
private class giic_Test_ProcessShipmentQueue {
    @testSetup
    private static void test() {
          giic_Test_DataCreationUtility.insertWarehouse();
        
        giic_Test_DataCreationUtility.testSystemPolicyCreation();
        giic_Test_DataCreationUtility.insertGlobalParametersCustomSetting();
        giic_Test_DataCreationUtility.insertConsumerAccount();
        giic_Test_DataCreationUtility.createCarrier();
        giic_Test_DataCreationUtility.insertUnitofMeasure();
        //giic_Test_DataCreationUtility.insertConsumerAccount_N(10);
        giic_Test_DataCreationUtility.insertProduct();
        giic_Test_DataCreationUtility.insertLocations();
        giic_Test_DataCreationUtility.insertProductInventory(1);
        giic_Test_DataCreationUtility.insertProductInventoryByLoc();
        giic_Test_DataCreationUtility.insertProductInventoryQTYDetails();
        giic_Test_DataCreationUtility.insertSalesOrder(); // 0 - open sales order , 1 - release sales order
        
        
        giic_Test_DataCreationUtility.lstSalesOrder[1].giic_TaxCal__c = false;
        giic_Test_DataCreationUtility.lstSalesOrder[1].giic_SOStatus__c = giic_Constants.SO_PICKED;
        update giic_Test_DataCreationUtility.lstSalesOrder[1];
        giic_Test_DataCreationUtility.insertSOLine(new list<gii__SalesOrder__c>{giic_Test_DataCreationUtility.lstSalesOrder[1]});
        //retriee sales order lines whre order is released 
        List<gii__SalesOrderLine__c>  lstSalesOrderLines = [select id,name,gii__UnitPrice__c,gii__SalesOrder__c,gii__Warehouse__c,gii__OrderQuantity__c,gii__Status__c,gii__Product__c, gii__Product__r.giic_ProductSKU__c from gii__SalesOrderLine__c where gii__SalesOrder__c = : giic_Test_DataCreationUtility.lstSalesOrder[1].id];
        for(gii__SalesOrderLine__c soline  : lstSalesOrderLines)
        {
            soline.giic_LineStatus__c = giic_Constants.SHIPPED;
        }
        update lstSalesOrderLines;

        list<gii__SalesOrder__c>  lstSalesOrder = [select id,gii__Released__c, gii__Carrier__r.giic_UniqueCarrier__c, gii__Warehouse__r.giic_WarehouseCode__c, gii__Account__c,name from gii__SalesOrder__c where id = : giic_Test_DataCreationUtility.lstSalesOrder[1].id ];
       
        giic_Test_DataCreationUtility.insertSalesOrderPayment(giic_Test_DataCreationUtility.lstSalesOrder[1].id);
        giic_Test_DataCreationUtility.insertWarehouseShipmentAdviceStaging(lstSalesOrder);
        giic_Test_DataCreationUtility.insertWarehouseShipmentAdviceLinesStaging(lstSalesOrderLines);
        giic_Test_DataCreationUtility.CreateAdminUser();   
    }
    
    private static testMethod void test_ProcessShipmentQueue() {
      User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        
        System.Runas(u)   
        {
            List<id> lstWHSIds = new List<id>();
            List<gii__WarehouseShipmentAdviceStaging__c> lstWSAS = [Select Id, Name,giic_ProcessStatus__c, giic_ShipDate__c, giic_Warehouse__c from gii__WarehouseShipmentAdviceStaging__c];
            system.debug('lstWSAS--->>>>>>>>> ' + lstWSAS);
            lstWSAS[0].giic_ProcessStatus__c= 'Initial';
            update lstWSAS[0];
            if(lstWSAS != null && lstWSAS.size() > 0 ){
                for(gii__WarehouseShipmentAdviceStaging__c objWSAS : lstWSAS){
                   if(objWSAS.giic_ProcessStatus__c != 'Processed'){
                        lstWHSIds.add(objWSAS.Id);
                    } 
                }
            }
                
            Test.startTest();
                ID jobID = System.enqueueJob(new giic_ProcessShipmentQueue(lstWHSIds));
            Test.stopTest();
        }
    }
    
    private static testMethod void test_NegativeProcessShipmentQueue() {
      User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        
        System.Runas(u)   
        {
            List<id> lstWHSIds = new List<id>{null};
          /*  List<gii__WarehouseShipmentAdviceStaging__c> lstWSAS = [Select Id, Name,giic_ProcessStatus__c, giic_ShipDate__c, giic_Warehouse__c from gii__WarehouseShipmentAdviceStaging__c];
            system.debug('lstWSAS--->>>>>>>>> ' + lstWSAS);
            lstWSAS[0].giic_ProcessStatus__c= 'Initial';
            update lstWSAS[0];
            if(lstWSAS != null && lstWSAS.size() > 0 ){
                for(gii__WarehouseShipmentAdviceStaging__c objWSAS : lstWSAS){
                   if(objWSAS.giic_ProcessStatus__c != 'Processed'){
                        lstWHSIds.add(objWSAS.Id);
                    } 
                }
            }*/
                
            Test.startTest();
                ID jobID = System.enqueueJob(new giic_ProcessShipmentQueue(lstWHSIds));
            Test.stopTest();
        }
    }
}