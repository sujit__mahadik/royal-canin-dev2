/************************************************************************************
Version : 1.0
Name : giic_Test_CreateInvoiceHelper
Created Date : 10 Oct 2018
Function : test class for giic_CreateInvoiceHelper
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/
@isTest(SeeAllData = false)
public class giic_Test_CreateInvoiceHelper {

    @testSetup
    static void setup()
    {
        giic_Test_DataCreationUtility.insertWarehouse();
        
        giic_Test_DataCreationUtility.testSystemPolicyCreation();
        giic_Test_DataCreationUtility.insertGlobalParametersCustomSetting();
        giic_Test_DataCreationUtility.insertConsumerAccount();
        giic_Test_DataCreationUtility.createCarrier();
        giic_Test_DataCreationUtility.insertUnitofMeasure();
        //giic_Test_DataCreationUtility.insertConsumerAccount_N(10);
        giic_Test_DataCreationUtility.insertProduct();
        
        giic_Test_DataCreationUtility.insertLocations();
        giic_Test_DataCreationUtility.insertProductInventory(1);
        giic_Test_DataCreationUtility.insertProductInventoryByLoc();
        giic_Test_DataCreationUtility.insertProductInventoryQTYDetails();
        giic_Test_DataCreationUtility.insertSalesOrder(); // 0 - open sales order , 1 - release sales order
        
        
        giic_Test_DataCreationUtility.lstSalesOrder[1].giic_TaxCal__c = false;
        giic_Test_DataCreationUtility.lstSalesOrder[1].giic_SOStatus__c = giic_Constants.SO_PICKED;
        update giic_Test_DataCreationUtility.lstSalesOrder[1];
        giic_Test_DataCreationUtility.insertSOLine(new list<gii__SalesOrder__c>{giic_Test_DataCreationUtility.lstSalesOrder[1]});
        //retriee sales order lines whre order is released 
        List<gii__SalesOrderLine__c>  lstSalesOrderLines = [select id,name,gii__UnitPrice__c,gii__SalesOrder__c,gii__Warehouse__c,gii__OrderQuantity__c,gii__Status__c,gii__Product__c, gii__Product__r.giic_ProductSKU__c from gii__SalesOrderLine__c where gii__SalesOrder__c = : giic_Test_DataCreationUtility.lstSalesOrder[1].id];
        for(gii__SalesOrderLine__c soline  : lstSalesOrderLines)
        {
            soline.giic_LineStatus__c = giic_Constants.SHIPPED;
        }
        update lstSalesOrderLines;

        list<gii__SalesOrder__c>  lstSalesOrder = [select id,gii__Released__c, gii__Carrier__r.giic_UniqueCarrier__c, gii__Warehouse__r.giic_WarehouseCode__c, gii__Account__c,name from gii__SalesOrder__c where id = : giic_Test_DataCreationUtility.lstSalesOrder[1].id ];
       system.debug('lstSalesOrder:::'+lstSalesOrder + '  ' + lstSalesOrder[0].gii__Warehouse__r.giic_WarehouseCode__c);
        giic_Test_DataCreationUtility.insertSalesOrderPayment(giic_Test_DataCreationUtility.lstSalesOrder[1].id);
        giic_Test_DataCreationUtility.insertWarehouseShipmentAdviceStaging(new list<gii__SalesOrder__c>{lstSalesOrder[0]});
        giic_Test_DataCreationUtility.insertWarehouseShipmentAdviceLinesStaging(lstSalesOrderLines);
        giic_Test_DataCreationUtility.CreateAdminUser(); 
    }
    
   private static testMethod void createShipmentNegativeTest() {
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        
        System.Runas(u)        
        { 
            gii__SalesOrder__c so;
            
            list<gii__SalesOrder__c> solist = [select id,name,gii__PaymentMethod__c,gii__Account__c,gii__Released__c from gii__SalesOrder__c];
            if(solist.size() > 1)
                so = solist[1];
            
            list<gii__WarehouseShipmentAdviceStaging__c> lstWarehouseshpmentStaging = [select id,Name,giic_isTaxCommitted__c from gii__WarehouseShipmentAdviceStaging__c];
            Test.startTest();
            
            Map<String, Object> mapResult;
            if(lstWarehouseshpmentStaging != null && !lstWarehouseshpmentStaging.isEmpty())
            {
                try{
                    mapResult = giic_CreateInvoiceHelper.createShipment(lstWarehouseshpmentStaging[0].id); // create shipment
                }catch(exception e){System.debug('Exception is handled');} 
            }
        }
            
    }
    
     private static testMethod void CalculateTaxNegativeTest() {
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        
        System.Runas(u)        
        { 
            gii__SalesOrder__c so;
            
            list<gii__SalesOrder__c> solist = [select id,name,gii__PaymentMethod__c,gii__Account__c,gii__Released__c from gii__SalesOrder__c];
            if(solist.size() > 1)
                so = solist[1];
            
            list<gii__WarehouseShipmentAdviceStaging__c> lstWarehouseshpmentStaging = [select id,Name,giic_isTaxCommitted__c from gii__WarehouseShipmentAdviceStaging__c];
            Test.startTest();
            
            Map<String, Object> mapResult;
            if(lstWarehouseshpmentStaging != null && !lstWarehouseshpmentStaging.isEmpty())
            {
                 try
                {
                    Map<String,Object> res = giic_CreateInvoiceHelper.CalculateTax(lstWarehouseshpmentStaging[0].id); // calculate tax
                }catch(exception ex)
                {
                    system.debug('exception is handled');
                } 
            }
        }
            
    }
    
    private static testMethod void updateSOLandIRTest() {
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        
        System.Runas(u)        
        { 
            gii__SalesOrder__c so;
            
            list<gii__SalesOrder__c> solist = [select id,name,gii__PaymentMethod__c,gii__Account__c,gii__Released__c from gii__SalesOrder__c];
            if(solist.size() > 1)
                so = solist[1];
            
            list<gii__WarehouseShipmentAdviceStaging__c> lstWarehouseshpmentStaging = [select id,Name,giic_isTaxCommitted__c,giic_Carrier__c,giic_Warehouse__c from gii__WarehouseShipmentAdviceStaging__c];
             system.debug('lstWarehouseshpmentStaging:::'+lstWarehouseshpmentStaging);
            Test.startTest();
            
            Map<String, Object> mapResult;
            if(lstWarehouseshpmentStaging != null && !lstWarehouseshpmentStaging.isEmpty())
            {
                 List<gii__SalesOrderLine__c>  lstSalesOrderLines = [select id,name,gii__Warehouse__c,gii__OrderQuantity__c,gii__Status__c,gii__SalesOrder__c,gii__SalesOrder__r.gii__Account__c,gii__Product__c from gii__SalesOrderLine__c where gii__SalesOrder__c = : so.id];
                giic_Test_DataCreationUtility.createInventoryReservation(lstSalesOrderLines);
                list<gii__InventoryReserve__c> lstInvRes = [select id,gii__ProductInventorySequence__c,gii__Status__c,gii__ProductInventorybyLocation__c,gii__Location__c,gii__ProductInventoryQuantityDetail__c,gii__SalesOrder__c,gii__SalesOrderLine__r.gii__Product__c,gii__SalesOrderLine__r.giic_LineStatus__c ,gii__ReserveQuantity__c from gii__InventoryReserve__c ];
                system.debug('before update inv' + lstInvRes);
                for(gii__InventoryReserve__c invRes : lstInvRes)
                {
                    system.debug('inv res:' + invRes.gii__ProductInventorySequence__c);
                    invRes.gii__ReserveQuantity__c = 2; invRes.gii__Status__c = giic_Constants.Reserved;
                }
                update lstInvRes;
                system.debug('after update inv:::'+lstInvRes);
                Map<String, Object> mapUpdateResult = giic_CreateInvoiceHelper.updateSOLandIR(lstWarehouseshpmentStaging[0].id); //  first update SoL and IR
                system.debug('mapUpdateResult::'+mapUpdateResult);
            }
        }
            
    }
    
   private static testMethod void positivetest() {
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        
        System.Runas(u)        
        { 
            gii__SalesOrder__c so;
            
            list<gii__SalesOrder__c> solist = [select id,name,gii__PaymentMethod__c,gii__Account__c,gii__Released__c from gii__SalesOrder__c];
            if(solist.size() > 1)
                so = solist[1];
            
            list<gii__WarehouseShipmentAdviceStaging__c> lstWarehouseshpmentStaging = [select id,Name,giic_isTaxCommitted__c from gii__WarehouseShipmentAdviceStaging__c];
            Test.startTest();
            
            Map<String, Object> mapResult;
            if(lstWarehouseshpmentStaging != null && !lstWarehouseshpmentStaging.isEmpty())
            {
               
                List<gii__SalesOrderLine__c>  lstSalesOrderLines = [select id,name,gii__Warehouse__c,gii__OrderQuantity__c,gii__Status__c,gii__SalesOrder__c,gii__SalesOrder__r.gii__Account__c,gii__Product__c from gii__SalesOrderLine__c where gii__SalesOrder__c = : so.id];
                giic_Test_DataCreationUtility.createInventoryReservation(lstSalesOrderLines);
                list<gii__InventoryReserve__c> lstInvRes = [select id,gii__ProductInventorySequence__c,gii__Status__c,gii__ProductInventorybyLocation__c,gii__Location__c,gii__ProductInventoryQuantityDetail__c,gii__SalesOrder__c,gii__SalesOrderLine__r.gii__Product__c,gii__SalesOrderLine__r.giic_LineStatus__c ,gii__ReserveQuantity__c from gii__InventoryReserve__c ];
                system.debug('before update inv' + lstInvRes);
                for(gii__InventoryReserve__c invRes : lstInvRes)
                {
                    system.debug('inv res:' + invRes.gii__ProductInventorySequence__c);
                    invRes.gii__ReserveQuantity__c = 2; invRes.gii__Status__c = giic_Constants.Reserved;
                }
                update lstInvRes;
                system.debug('after update inv:::'+lstInvRes);

                gii__WarehouseShipmentAdviceStaging__c ws = new gii__WarehouseShipmentAdviceStaging__c(id = lstWarehouseshpmentStaging[0].id, giic_isTaxCommitted__c = true );
                update ws;
                
                mapResult = giic_CreateInvoiceHelper.createShipment(lstWarehouseshpmentStaging[0].id); // create shipment
                system.debug('mapResult:::'+mapResult);
                list<gii__Shipment__c> lstShipment =  giic_Test_DataCreationUtility.insertShipment(new list<gii__SalesOrder__c>{so});
                system.debug('shipment:' + lstShipment);
                
                giic_CreateInvoiceHelper.createInvoiceFromShipment( JSON.serialize(mapResult) ,mapResult.containsKey('mapFulfilmentLines') ? JSON.serialize(mapResult.get('mapFulfilmentLines')) : null); // create invoice from shipment
                List<gii__OrderInvoice__c> lstInvoice = [SELECT Id, Name FROM gii__OrderInvoice__c]; 
               System.assertEquals(lstInvoice.isEmpty(), false);
               
            }
            Test.stopTest();
        }
    }
    
    private static testMethod void testRemaningUtilMethods() {
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        
        System.Runas(u)        
        {
             gii__SalesOrder__c so = new gii__SalesOrder__c();
            Set<String> carrierIds = new Set<String>();
            for(gii__Carrier__c carrier : [select id,gii__TrackingURL__c , giic_UniqueCarrier__c  from gii__Carrier__c]){
                carrierIds.add(carrier.Id);
            }
            list<gii__SalesOrder__c> solist = [select id,name,gii__PaymentMethod__c,gii__Account__c,gii__Released__c from gii__SalesOrder__c];
            if(solist.size() > 1)
                so = solist[1];
            
            Test.startTest();
            List<gii__SalesOrderLine__c>  lstSalesOrderLines = [select id,name,gii__Warehouse__c,gii__OrderQuantity__c,gii__Status__c,gii__SalesOrder__c,gii__SalesOrder__r.gii__Account__c,gii__Product__c from gii__SalesOrderLine__c where gii__SalesOrder__c = : so.id];
            giic_SOShipmentUtility.cancelSOL(lstSalesOrderLines[0], 1, so.id);
            giic_SOShipmentUtility.updateOrderLineCancelled(so.Id);
            giic_SOShipmentUtility.getCarriers(carrierIds);
            Test.stopTest();
        }
    }
    
   
   @isTest private static void testGetInventoryReservation(){
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        System.RunAs(u){
            List<String> soIds = new List<String>();
            for (gii__SalesOrder__c soObj : [SELECT Id FROM gii__SalesOrder__c]){
                soIds.add(soObj.Id);
            }
            Test.startTest();
            giic_SOShipmentUtility.getInventoryReservation(soIds);
            Test.stopTest();
        }
   }
     
}