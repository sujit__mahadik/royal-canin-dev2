@isTest(SeeAllData=true)
public class AccountRejectedEmailToOwnerTest{

     static testMethod void sendEmail() {
     
      RecordType rt = [SELECT Id,name FROM RecordType WHERE SObjectType='Customer_Registration__c' and name='Retailer' Limit 1];
      User ussss = [SELECT Id,name FROM user WHERE email='maruthi.gotety.external@royalcanin.com' Limit 1];      
      Customer_Registration__c custobj = new Customer_Registration__c();
      custobj.First_Name__c = 'Brain';
      custobj.Last_Name__c = 'Hopkins';
      custobj.Name_of_Business_Organization__c = 'Test Account';
      custobj.recordTypeId = rt.Id;
      //custobj.Application_Status__c= 'Processed';
      custobj.ownerId = ussss.Id;
      custobj.Shipping_Street_Address1__c = 'Test address 1';
      custobj.Shipping_Street_Address2__c = 'Test address 2';
      custobj.Shipping_Zip_Code__c = '12345';
      custobj.Shipping_City__c = 'Test City';
      custobj.Ship_To_State__c = 'Test State';
      custobj.Email_Address__c = 'testaccapprove@tesst.com';
      insert custobj;
      
      
        Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
        req1.setComments('Submitting request for approval.');
        req1.setObjectId(custobj.id);
        req1.setSubmitterId(ussss.Id);
        req1.setProcessDefinitionNameOrId('Manual_Approval_Steps');
        req1.setSkipEntryCriteria(true);
        Approval.ProcessResult result = Approval.process(req1);
        
        System.assert(result.isSuccess());
        
        System.assertEquals('Pending', result.getInstanceStatus(),'Instance Status'+result.getInstanceStatus());
        List<Id> newWorkItemIds = result.getNewWorkitemIds();
        Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();
        req2.setComments('Rejectng registration');
        req2.setAction('Reject');
        req2.setNextApproverIds(new Id[] {ussss.Id});
        req2.setWorkitemId(newWorkItemIds.get(0));
        Approval.ProcessResult result2 =  Approval.process(req2);

     }
 }