/************************************************************************************
Version : 1.0
Created Date : 20 Dec 2018
Description : Update Fullfillment based on response from DC
Author : Abhishek Tripathi
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------*/
@RestResource(urlMapping='/updateFulfillmentByDC/v1.0/*')


global class giic_UpdateFulfillmentByDC {
    
    /******************************************************
POST : MW hits endpoint to with post request to update fulfillment  
*******************************************************/
    @HttpPost 
    global static void processPostRequest(Id fulfillmentId, String Status){
        RestRequest req = RestContext.request;
        RestResponse res;
        if(Test.isRunningTest()){
            res = new RestResponse();
        }else{
            res = Restcontext.response;
        }
        
        Map<String, String> responseBody = new Map<String, String>();
        List<String> statusList = new List<String>{giic_Constants.ACCEPTED, giic_Constants.REJECTED, giic_Constants.SUBMITTED, giic_Constants.SUBMITTED_WITH_ERROR};
            
            if(String.isBlank(fulfillmentId)){
                res.statusCode = 400;
                responseBody.put('message', Label.giic_fulfillmentidrequired);
                res.responseBody = Blob.valueOf(JSON.serialize(responseBody));
                return;
            }
        
        if(!statusList.contains(Status)){
            res.statusCode = 400;
            responseBody.put('message', Label.giic_AcceptedValueForStatus + String.join(statusList, ', '));
            res.responseBody = Blob.valueOf(JSON.serialize(responseBody));
            return;
        }
        // Get all related records using Fullfillment Id 
        List<gii__WarehouseShippingOrderStaging__c> listFulfillment = [select id, name,giic_Status__c, giic_SalesOrder__r.giic_CurrencyIsoCode__c, giic_SalesOrder__r.Name, 
                                                                       (select id, gii__SettlementAmount__c	 from Sales_Order_Payment_Settlements__r where  gii__SalesOrderPayment__r.gii__PaymentMethod__c =: giic_Constants.CREDIT_CARD	),
                                                                       (select id, giic_SalesOrderLine__c from Warehouse_Shipping_Order_Lines_Staging__r )                                                               
                                                                       from  gii__WarehouseShippingOrderStaging__c where id=:fulfillmentId];
        if(!listFulfillment.isEmpty()){
            List<gii__WarehouseShippingOrderLinesStaging__c> whsoLinesList = listFulfillment[0].Warehouse_Shipping_Order_Lines_Staging__r;
            Set<Id> solIDs = new Set<Id>();
            if(whsoLinesList != null && !whsoLinesList.isEmpty()){
                for(gii__WarehouseShippingOrderLinesStaging__c whsolObj : whsoLinesList){
                    solIDs.add(whsolObj.giic_SalesOrderLine__c);
                }
            }
            
            if(Status == giic_Constants.REJECTED){
                Decimal totalAmount = 0;
                List<gii__SalesOrderPaymentSettlement__c> listSOPSettlements = new List<gii__SalesOrderPaymentSettlement__c>();
                if(!listFulfillment[0].Sales_Order_Payment_Settlements__r.isEmpty()){
                    listSOPSettlements = listFulfillment[0].Sales_Order_Payment_Settlements__r;
                    for(gii__SalesOrderPaymentSettlement__c sopObj: listSOPSettlements){
                        totalAmount = totalAmount + sopObj.gii__SettlementAmount__c;
                    }
                }
                
                Map<String, Object> mapParams = new Map<String, Object>();
                giic_BillToWrapper billTo = new giic_BillToWrapper();
                billTo.OrderNumber = listFulfillment[0].giic_SalesOrder__r.Name;
                billTo.FulfillmentNumber = listFulfillment[0].Name;
                mapParams.put('billTo', billTo);
                mapParams.put('totalAmount', totalAmount);
                mapParams.put('recordId', listFulfillment[0].Id);
                
                Map<String, Object> mapResult = giic_CyberSourceRCCCUtility.reverseAuthorizationRCCC(mapParams);
                // Checking response from server
                if(!mapResult.containsKey('error') && !mapResult.containsKey('exception')){
                    listFulfillment[0].giic_IntegrationStatus__c = giic_Constants.DROP_AUTHORIZATION;
                    res.statusCode = 200;
                    responseBody.put('message', Label.giic_ProcessedSuccessfully);
                    
                }else{
                    res.statusCode = 200;
                    responseBody.put('message', Label.giic_DropAuthorizationFailed);
                }
                
                try{
                    if(!solIDs.isEmpty()){
                        List<gii__SalesOrderLine__c> solList = [Select Id, Name, gii__SalesOrder__c, giic_FulfillmentAttempts__c from gii__SalesOrderLine__c where id in : solIDs];
                        if(!solList.isEmpty()){
                            for(gii__SalesOrderLine__c solObj : solList){
                                //  solObj.giic_FulfillmentAttempts__c = solObj.giic_FulfillmentAttempts__c + 1; // Increasing fulfillment attempt by one
                                giic_SOShipmentUtility.cancelSOL(solObj, solObj.giic_FulfillmentAttempts__c, solObj.gii__SalesOrder__c);
                            } 
                            update solList;
                        }
                    } 
                    listFulfillment[0].giic_Status__c = giic_Constants.REJECTED_STATUS;
                    update listFulfillment;
                    List<gii__InventoryReserve__c> inventoryResList = [Select id from gii__InventoryReserve__c where gii__SalesOrderLine__c in : solIDs];
                    if(!listSOPSettlements.isempty()) delete listSOPSettlements;
                    if(!inventoryResList.isempty()) delete inventoryResList;
                    
                    
                }catch(Exception e){
                    System.debug(e);
                    res.statusCode = 417;
                    responseBody.put('message', e.getMessage());
                }
            }else if(Status == giic_Constants.ACCEPTED){
                try{
                    update new gii__WarehouseShippingOrderStaging__c(id = fulfillmentId, giic_Status__c = giic_Constants.ACCEPTED_STATUS);
                    List<gii__SalesOrderLine__c> lstSOL = new List<gii__SalesOrderLine__c>();
                    for(Id solId : solIDs){
                        gii__SalesOrderLine__c sol = new gii__SalesOrderLine__c();
                        sol.id = solId; 
                        sol.giic_LineStatus__c = giic_Constants.ACCEPTED;
                        lstSOL.add(sol); 
                    }
                    if(!lstSOL.isEmpty()) update lstSOL;
                    res.statusCode = 200;
                    responseBody.put('message', Label.giic_ProcessedSuccessfully);
                }catch(Exception e){
                    System.debug(e);
                    res.statusCode = 417;
                    responseBody.put('message', e.getMessage());
                } 
            }else if(Status == giic_Constants.SUBMITTED){
                try{
                    update new gii__WarehouseShippingOrderStaging__c(id = fulfillmentId, giic_Status__c = giic_Constants.SUBMITTED);
                    res.statusCode = 200;
                    responseBody.put('message', Label.giic_ProcessedSuccessfully);
                }catch(Exception e){
                    System.debug(e);
                    res.statusCode = 417;
                    responseBody.put('message', e.getMessage());
                }
            }
            else if(Status == giic_Constants.SUBMITTED_WITH_ERROR){
                try{
                    update new gii__WarehouseShippingOrderStaging__c(id = fulfillmentId, giic_Status__c = giic_Constants.SUBMITTED_WITH_ERROR);
                    res.statusCode = 200;
                    responseBody.put('message', Label.giic_ProcessedSuccessfully);
                }catch(Exception e){
                    System.debug(e);
                    res.statusCode = 417;
                    responseBody.put('message', e.getMessage());
                }
            }
        }else{
            res.statusCode = 200;
            responseBody.put('message', Label.giic_FulfillmentIdNotexists);
        }
        res.responseBody = Blob.valueOf(JSON.serialize(responseBody)); 
    }
}