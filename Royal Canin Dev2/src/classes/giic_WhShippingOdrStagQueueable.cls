/************************************************************************************
Version : 1.0
Name : giic_WhShippingOdrStagQueueable
Created Date : 06 Dec 2018
Function :  
Modification Log :
Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/

public class giic_WhShippingOdrStagQueueable  implements Queueable{
    private set<Id> setWSOSIds;
    
    public static giic_WhShippingOrderStagingWrapper ObjWHShipOdrStg = new giic_WhShippingOrderStagingWrapper();
    // constructor of class
    public giic_WhShippingOdrStagQueueable(set<Id> setOfWSOSIds ){
        setWSOSIds = setOfWSOSIds;
    }
    // execute method of Queueable class
    public void execute(QueueableContext context) { 
        List<giic_WhShippingOrderStagingWrapper.WarehouseShippingOrderStaging> listWSOS;
        if(setWSOSIds != null && setWSOSIds.size() > 0 ){
            listWSOS = new List<giic_WhShippingOrderStagingWrapper.WarehouseShippingOrderStaging>();
            giic_WhShippingOrderStagingWrapper.WarehouseShippingOrderStaging objWSOS;
            List<gii__WarehouseShippingOrderStaging__c> lstWSOS = [select id, name, giic_SalesOrder__c,giic_SalesOrder__r.giic_OrderNo__c, (select id, giic_SalesOrderLine__r.giic_LineNo__c from Warehouse_Shipping_Order_Lines_Staging__r ) from gii__WarehouseShippingOrderStaging__c where id in :  setWSOSIds];
            if(lstWSOS != null && lstWSOS.size() > 0 ){
                for(gii__WarehouseShippingOrderStaging__c oWSOS : lstWSOS ){
                    objWSOS = new giic_WhShippingOrderStagingWrapper.WarehouseShippingOrderStaging();
                    objWSOS.WSOS_Id = oWSOS.Id;
                    objWSOS.WSOS_SOId = oWSOS.giic_SalesOrder__c;
                    List<giic_WhShippingOrderStagingWrapper.WarehouseShippingOrderLineStaging> WhSOdrStgsLines;
                    giic_WhShippingOrderStagingWrapper.WarehouseShippingOrderLineStaging objWSOLS;
                    
                    if(oWSOS.Warehouse_Shipping_Order_Lines_Staging__r != null && oWSOS.Warehouse_Shipping_Order_Lines_Staging__r.size() > 0 ){
                        WhSOdrStgsLines = new List<giic_WhShippingOrderStagingWrapper.WarehouseShippingOrderLineStaging>();
                        for(gii__WarehouseShippingOrderLinesStaging__c oWSOLS : oWSOS.Warehouse_Shipping_Order_Lines_Staging__r  ){
                            objWSOLS = new giic_WhShippingOrderStagingWrapper.WarehouseShippingOrderLineStaging();
                            objWSOLS.WSOLS_SOLId = oWSOLS.giic_SalesOrderLine__c;
                            WhSOdrStgsLines.add(objWSOLS);
                        }
                        objWSOS.WhSOdrStgsLines = WhSOdrStgsLines;
                    }
                    listWSOS.add(objWSOS);
                }
            }
        }
        ObjWHShipOdrStg.WhSOdrStgs = listWSOS;
        // below methos is being called to create Sales Order payment settlement records 
         giic_UpdateTaxAndCreateSOPSWS.processSOPSettlementRequest(ObjWHShipOdrStg);
    }
    
   
    
}