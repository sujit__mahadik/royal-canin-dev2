/************************************************************************************
Version : 1.0
Created Date : 14 Sep 2018
Function : All Sales order line related functions
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/
public class giic_SalesOrderLineHelper {
     /*
    * Method name : updateSalesOrderLine
    * Description : update Sales order line with updated information
    * Return Type : 
    * Parameter : list<gii__SalesOrderLine__c>
    */
    
    public static map<string,Object> updateSalesOrderLine(list<gii__SalesOrderLine__c> lstSalesOrderLine){
        if(lstSalesOrderLine != null && lstSalesOrderLine.size() > 0){
            //insert lstNewTargetSobject;
            Database.SaveResult[] srList = Database.update(lstSalesOrderLine, false);
            // Iterate through each returned result
            Integer index = 0;
            List<Integration_Error_Log__c> lstErrors = new List<Integration_Error_Log__c>();
            Set<String> setErrorIds = new Set<String>();
            String userStory = system.label.giic_AllocationUserStoryName;
            Map<String, String> fieldApiForErrorLog = new Map<String, String>{'giic_SalesOrderLine__c'=>'Id'};
            for (Database.SaveResult sr : srList) {
                if (!sr.isSuccess()) {
                    for(Database.Error err : sr.getErrors()) {
                        giic_IntegrationCustomMetaDataUtil.collectErrors(lstErrors, setErrorIds, userStory, fieldApiForErrorLog, lstSalesOrderLine[index], String.valueOf(err.getStatusCode()), err.getMessage());
                    }
                }
                index++;
            }
            if(!lstErrors.isEmpty()){
                insert lstErrors;
                return new map<string,Object>{'ERRORLIST'=>lstErrors};
            } 
            
        }
        return new map<string,Object>();
    }
    /*
* Method name : getSOLInfo
* Description : get Sales Order Line info for allocation
* Return Type : map<string,Object>
* Parameter : list<gii__SalesOrderLine__c>
*/
    
    public static map<string,Object> getSOLInfo(list<gii__SalesOrderLine__c> lstSOls,boolean isRoute,string sWarehouseMapping){
        set<id> setProductIds=new set<id>();
        set<Id> setWarehouseIds=new set<Id>();
        map<string,list<gii__SalesOrderLine__c>> mapIRTypeWithSOL=new map<string,list<gii__SalesOrderLine__c>>();
        for(gii__SalesOrderLine__c objSOL: lstSOls){
            setProductIds.add(objSOL.gii__Product__c);
            setWarehouseIds.add(objSOL.gii__Warehouse__c);
            if(!isRoute){
                if(objSOL.gii__ReservedQuantity__c == 0 || objSOL.gii__ReservedQuantity__c == null){
                    if(objSOL.gii__BackOrderQuantity__c != null && objSOL.gii__BackOrderQuantity__c > 0){
                        if(!mapIRTypeWithSOL.containsKey('BACK')){ 
                            mapIRTypeWithSOL.put('BACK',new list<gii__SalesOrderLine__c>());
                        }
                        mapIRTypeWithSOL.get('BACK').add(objSOL);
                    }
                    else{
                        if(!mapIRTypeWithSOL.containsKey('IRSER')){ 
                            mapIRTypeWithSOL.put('IRSER',new list<gii__SalesOrderLine__c>());
                        }
                        mapIRTypeWithSOL.get('IRSER').add(objSOL);
                    }
                    
                }
            }
        }
        if(sWarehouseMapping != null && sWarehouseMapping !='' && !isRoute){
            list<giic_CommonUtility.WarehouseDistance> lstWarehouseDistance=(list<giic_CommonUtility.WarehouseDistance>)JSON.deserialize(sWarehouseMapping,list<giic_CommonUtility.WarehouseDistance>.Class);
            for(giic_CommonUtility.WarehouseDistance objWarehouseDistance : lstWarehouseDistance){
                setWarehouseIds.add(objWarehouseDistance.warehouseId);
            }
        }
        return new map<string,Object>{'PRODIDS' => setProductIds,'WAREIDS' => setWarehouseIds,'SOL'=> mapIRTypeWithSOL };
    }
}