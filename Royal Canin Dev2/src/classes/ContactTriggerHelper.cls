/*
 * Date:             - Developer, Company                          - description
 * 2015-08-05        - Stevie Yakkel, Acumen Solutions             - Edited
 * 2018-06-18        - Bethi Reddy, Royal Canin                    - Edited to support Account contact Relationship functionality (SCH-0065)
 */


public with sharing class ContactTriggerHelper {
    public static Map<String, Id> accountRecTypes;
    public static boolean isIntegrationUser;

    static {
        accountRecTypes = new Map<String,Id>();
        for(recordType rec : [select developerName, Id from recordType where sObjectType = 'Account']) {
            accountRecTypes.put(rec.DeveloperName, rec.Id);
        }
    }
    
    static {
        if (isIntegrationUser==null){
            if ([select name from Profile where id =:UserInfo.getProfileId()].name=='Integration User'){
                isIntegrationUser=true;
            }
            else isIntegrationUser=false;
        }
        
    }
    
    
    public static void setContactToMultipleAccounts(List<Contact> contacts){
        
        system.debug('==== contacts');
        system.debug(contacts);
        Map<id, Id> AccountsIdContact = new Map<id, Id>();
        
        List<id> ContactIds = new List<Id>();
        for (Contact c: contacts){
         if(c.ClinicCustomerNo__c != null){
            AccountsIdContact.put(c.AccountId, c.id);
            ContactIds.add(c.id);
           }
        }
        system.debug('==== AccountsIdContact');
        system.debug(AccountsIdContact);
        
        List<Contact> contactList = [Select id, ClinicCustomerNo__c from Contact where id in:  ContactIds];
        
        system.debug('==== contactList');
        system.debug(contactList);
        
        Map<String, Contact> clinicMap = new Map<String, Contact>();
        for (Contact c: contactList){
            clinicMap.put(c.ClinicCustomerNo__c, c);
        }
        
        system.debug('==== clinicMap');
        system.debug(clinicMap);
        
        
        Map<id, Account> toRelate = new Map<id, Account>([Select id, ShipToCustomerNo__c  from Account where ShipToCustomerNo__c in: clinicMap.keySet() and id not in:AccountsIdContact.keySet()]);
        system.debug('==== toRelate');
        system.debug(toRelate);
        
        List<AccountContactRelation> newRelations = new list<AccountContactRelation>();
        
        if (toRelate.size() > 0){
            
            List <id> accountsToRelate = new List<Id>();
            accountsToRelate.addAll(toRelate.keySet());
            for (Id accIdToRelate: accountsToRelate){
                AccountContactRelation newRelation = new AccountContactRelation();
                newRelation.AccountId = accIdToRelate;
                newRelation.ContactId = clinicMap.get(toRelate.get(accIdToRelate).ShipToCustomerNo__c).id;//AccountIdsContact.get(clinicMap.get(toRelate.get(accIdToRelate).ShipToCustomerNo__c).id);
                newRelation.Roles = 'Other';
                newRelations.add(newRelation);
            
          }
            system.debug('==== newRelations');
            system.debug(newRelations);
            upsert newRelations;
        }
    }
    
   /* 
    public static void processLoadedConsumerRelationship(List<Contact> contacts){
        if (isIntegrationUser){
            List <Partner__c> partnersToInsert = new List <Partner__c>();
            Set <Id> acctIds = new Set <Id>();
            Map <Id, Account> accountMap = new Map <Id, Account>();
            Set <String> relationshipClientTypeSet = new Set <String>{'Clinic Feeding Participant', 'University Feeding Participant'};
            for(Contact con : contacts) {
                acctIds.add(con.accountid);
            }
            for (Account acct: [select id, parentid,customer_Type__c from Account where id in:acctIds and parentId!=null]){
                accountMap.put(acct.id,acct);
            }
            for(Contact con : contacts) {
                if (accountMap.get(con.accountId)!=null){
                    if (relationshipClientTypeSet.contains(accountMap.get(con.accountId).customer_Type__c)){
                        Partner__c pt = new Partner__c();
                        pt.AccountToId__c = accountMap.get(con.accountId).id;
                        pt.AccountFromId__c =  accountMap.get(con.accountId).parentId;
                        pt.Role__c = con.account_relationship__c;
                        partnersToInsert.add(pt); 
                    }
                }
            }
            if (!partnersToInsert.isEmpty()){
                insert partnersToInsert;
            }
        }
        
    }
    
    */
   
    public static void processConsumerAccount(List<Contact> contacts) {
        List<Account> accountsToInsert = new List<Account>();
        List<wrapper> wrappers = new List<wrapper>();
        Set<Id> accountIds = new Set<Id>();

        Set<Id> parentIds = new Set<id>();
        Map<Id, List<Contact>> accIdToContact = new Map<Id, List<Contact>>();
        for(Contact con : contacts) {
            if(con.AccountId != null) {
                if(accIdToContact.containsKey(con.AccountId)) {
                    accIdToContact.get(con.AccountId).add(con);
                } else {
                    accIdToContact.put(con.AccountId, new List<Contact> {con});
                }
            }
            parentIds.add(con.AccountId);
        }

        List<Contact> tempContacts = new List<Contact>();
        for(Account acc : [select id from Account where id in : parentIds and RecordTypeId != : accountRecTypes.get('Consumer')]) {
            for(Contact con : accIdToContact.get(acc.Id)) {
                tempContacts.add(con);
            }
        }

        //for rc/banfield
        for(Contact con : contacts) {
            if(con.AccountId == null) {
                tempContacts.add(con);
            }
        }
       
        contacts = new List<Contact>(tempContacts);
        for(Contact con : contacts) {
        if(Test.isRunningTest() || con.Customer_Type__c == 'Clinic Feeding Participant' || con.Customer_Type__c == 'University Feeding Participant' || con.Customer_Type__c =='RC Associate' || con.Customer_Type__c =='Banfield Associate'){
            
            wrapper w = new wrapper();
            w.accId = con.AccountId;
            accountIds.add(con.AccountId);
            w.acc = createConsumerAccount(con);
            w.con = con;
            wrappers.add(w);
            
            accountsToInsert.add(w.acc);
        }
       }
        
        insert accountsToInsert;
   Map<Id, Account> accountMap = new Map<Id, Account>([select id, name from Account where id in : accountIds]);

        for(wrapper w : wrappers) {
        
         w.con.AccountId = w.acc.Id;
        }
            
  //          List<Contact> contactsToInsert = new List<Contact>();
    //        for(wrapper w : wrappers) {
     //       if(w.con.Customer_Type__c == 'Clinic Feeding Participant' || w.con.Customer_Type__c == 'University Feeding Participant'){
       //     if(w.accId != null) {
          //      Contact contact = new Contact();
       //         contact.AccountId = w.accId;
             /* partner.AccountToId__c = w.acc.Id;

                if(w.con.Contact_source__c == 'Onboarding Application') {
                    partner.Role__c = 'Employee';
                } else {
                    partner.Role__c = w.con.Account_Relationship__c;
                }
                system.debug('&&&&&w.accId' + w.accId);
                //partner.Name = accountMap.get(w.accId).Name + ' - ' + partner.Role__c; */

      //          contactsToInsert.add(contact);
     //       }
      //    }
     
   //       insert contactsToInsert;  
       
   //  } 
       
    }
    
    
   
    public static void dataDotComReFormat(List<Contact> contacts) {
        DataDotComReformatUtility.dataDotComPhoneAndFaxReformat(contacts);
        DataDotComReformatUtility.dataDotComContactPostalCodeReformat(contacts);
    }
    
     
    /*
    
    public static void preventDuplicateContact(List<Contact> contacts){
        Map<String, Contact> contactKeys = new Map<String, Contact>();
        
        //first name+ last name+ phone+ mailing street
        for (Contact c : contacts) {
            String k = c.FirstName + '_'+c.LastName + '_'+ c.Phone + '_'+c.MailingStreet;
            contactKeys.put(k.toLowerCase(), c);
        }
        
        String query = 'SELECT Id, FirstName, LastName, Phone, MailingStreet from Contact WHERE ';
        List<String> keys = new List<String>();
        keys.addAll(contactKeys.keySet());
        
        Integer i = 0;
        for (String a: keys){
            List<String> b = a.split('_');
            query = query + ' (FirstName = \''+b[0]+'\' AND LastName= \''+b[1]+'\' AND Phone= \''+b[2]+'\' AND MailingStreet= \''+b[3]+'\') ' ;
        
            if (i < keys.size() -1){
                query = query + ' OR ';
                i++;
            }
        }
        system.debug('=== query = '+ query);
        List<sObject> sobjList = Database.query(query);
        system.debug('=== sobjList = '+ sobjList);
        //List<AccountContactRelation> newRelations = new list<AccountContactRelation>();
        Map<Id, Id> relationMap = new Map<Id, Id>();
        List<Contact> contactWithErrors = new List<Contact>();
        for (sObject dup: sobjList){
            Contact duplicateContact = (Contact) dup;   
        
            if (contactKeys.containsKey(duplicateContact.FirstName.toLowerCase()+'_'+duplicateContact.LastName.toLowerCase()+'_'+duplicateContact.Phone.toLowerCase()+'_'+duplicateContact.MailingStreet.toLowerCase())){
                Contact c = contactKeys.get(duplicateContact.FirstName.toLowerCase()+'_'+duplicateContact.LastName.toLowerCase()+'_'+duplicateContact.Phone.toLowerCase()+'_'+duplicateContact.MailingStreet.toLowerCase());
                if (c.Id != duplicateContact.Id){
                    contactWithErrors.add(c);
                    relationMap.put(c.AccountId, duplicateContact.Id);
                }
            }
        }
        system.debug('==== contactWithErrors = '+ contactWithErrors);
        FixDuplicateContact(contactWithErrors, relationMap);
        
    }
    
    
    
    public static void FixDuplicateContact(List<Contact> contactWithErrors, Map<id, id> relationMap){
        system.debug('====== relationMap');
        system.debug(relationMap);
        List<AccountContactRelation> newRelations = new list<AccountContactRelation>();
        
        for (Id accId: relationMap.keySet()){
                AccountContactRelation newRelation = new AccountContactRelation();
                newRelation.AccountId = accId;
                newRelation.ContactId = relationMap.get(accId);
                newRelation.Roles = 'Other';
                newRelations.add(newRelation);
        }
        upsert newRelations;
        delete contactWithErrors;
    }
    
    */
    
    public static void preventConsumerAccountLkpUpdate(List<Contact> contacts, Map<Id,Contact> oldContactMap) {
        Set<Id> parentIds = new Set<id>();
        Map<Id, Contact> accIdToContact = new Map<Id, Contact>();
        for(Contact con : contacts) {
            accIdToContact.put(con.AccountId, con);
            parentIds.add(con.AccountId);
        }

        List<Contact> tempContacts = new List<Contact>();
        for(Account acc : [select id from Account where id in : parentIds and RecordTypeId =: accountRecTypes.get('Consumer')]) {
            if(accIdToContact.get(acc.Id).AccountId != oldContactMap.get(accIdToContact.get(acc.Id).Id).AccountId) {
                accIdToContact.get(acc.Id).addError('Cannot change lookup to Consumer Account.');
            }
        }
    }

    public static void setConsumerContactLkp(List<Contact> contacts) {
        Set<Id> parentIds = new Set<id>();
        Map<Id, Contact> accIdToContact = new Map<Id, Contact>();
        for(Contact con : contacts) {
            if(con.AccountId != null) {
                accIdToContact.put(con.AccountId, con);
                parentIds.add(con.AccountId);
            }
        }

        List<Contact> tempContacts = new List<Contact>();
        List<Account> accountsToUpdate = [select Id, Consumer_Contact_Lookup__c from Account where id in : parentIds and RecordTypeId =: accountRecTypes.get('Consumer')];
        for(Account acc : accountsToUpdate) {
            acc.Consumer_Contact_Lookup__c = accIdToContact.get(acc.Id).Id;
        }

        update accountsToUpdate;
    }

    public static void checkForAddressChanges(List<Contact> contacts, Map<Id,Contact> oldContactMap){
        Set<Id> contactIds = new Set<Id>();
        for(Contact contact : contacts){
            if(contact.MailingStreet != oldContactMap.get(contact.Id).MailingStreet || contact.MailingCity != oldContactMap.get(contact.Id).MailingCity || contact.MailingState != oldContactMap.get(contact.Id).MailingState || contact.MailingPostalCode != oldContactMap.get(contact.Id).MailingPostalCode || contact.OtherStreet != oldContactMap.get(contact.Id).OtherStreet || contact.OtherCity != oldContactMap.get(contact.Id).OtherCity || contact.OtherState != oldContactMap.get(contact.Id).OtherState || contact.OtherPostalCode != oldContactMap.get(contact.Id).OtherPostalCode){
                contactIds.add(contact.Id);
            }
        }
        if(!contactIds.isEmpty()){
            callAddressValidate(contactIds, true);
        }
    }

    public static void validateAddressOnInsert(List<Contact> contacts){
        Set<Id> contactIds = new Set<Id>();
        for(Contact contact : contacts){
            contactIds.add(contact.Id);
        }
        if(!contactIds.isEmpty()){
            callAddressValidate(contactIds, true);
        }
    }
    
    public static void callAddressValidate(Set<Id> contactIds, boolean asynCallOut){
        if(contactIds.isEmpty()) {
            return;
        } else if(!AddressValidator.Pending_Avalara_Response) {
            //Address Validator WS 'avalara'
            if(asynCallOut){
                //Asynchronous call
                AddressValidator.contactAddressToValidate_Future(contactIds);
            } else {
                //Synchronous call
                AddressValidator.contactAddressToValidate(contactIds);
            }
        }
    }

     public static Account createConsumerAccount(Contact con) {
         Set<String> customerTypesForBillToShipToUpdates =  new Set<String> {'Clinic Feeding Participant','RC Associate','University Feeding Participant',
        'Banfield Associate'};
        
        Account acc = new Account();
       
        if(con.FirstName != null) {
            acc.Name = con.FirstName;
            if(con.LastName != null) {
                acc.Name += ' ' + con.LastName;
            }
        } else if (con.LastName != null) {
            acc.Name += con.LastName;
        }
        acc.Customer_Type__c = con.Customer_Type__c;
        acc.RecordTypeId = accountRecTypes.get('Consumer');
        //acc.Link_to_Application__c = con.Link_to_Application__c;
        acc.Phone = con.Phone;
        
        if(con.Customer_Type__c == 'Clinic Feeding Participant' || con.Customer_Type__c == 'University Feeding Participant'){
            acc.ClinicCustomerNo__c = con.ClinicCustomerNo__c;
        }

        if(customerTypesForBillToShipToUpdates.contains(con.Customer_Type__c)){
            acc.BillingStreet = con.Billing_Street_Address_1__c + (con.Billing_Street_Address_2__c != null ? '\r\n' + con.Billing_Street_Address_2__c : '');
            acc.BillingCity = con.Billing_City__c;
            acc.BillingState = con.Billing_State__c;
            acc.BillingPostalCode = con.Billing_Zip_Code__c;
            acc.ShippingStreet = con.MailingStreet;
            acc.ShippingCity = con.MailingCity;
            acc.ShippingState = con.MailingState;
            acc.ShippingPostalCode = con.MailingPostalCode;
            acc.Customer_Registration__c = con.Customer_Registration__c;
            acc.ShipToCustomerNo__c =  con.ShipToCustomerNo__c;
            acc.Bill_To_Customer_ID__c = con.Bill_To_Customer_ID__c;
            acc.Insite_Customer_Type__c = con.Insite_Customer_Type__c;
            acc.Payment_Method__c = con.Payment_Method__c;
            acc.Statement_Type_Preference__c = con.Statement_Type_Preference__c;
        }
       
       return acc;
       
      }

    private class wrapper {
        public Id accId;
        public Account acc;
        public Contact con;
    }

    public static void formatPhoneFax (List<Contact> contacts, Map<Id, Contact> oldContactMap){

        Pattern MyPattern = Pattern.compile('\\d{10}');
        Set<String> fields = new Set<String> {'Phone', 'Fax'};

        for(Contact con : contacts) {
            
            for(String field : fields) {
                if(oldContactMap != null) {
                    if(con.get(field) != null && con.get(field) != oldContactMap.get(con.Id).get(field)){
                        String value = (String)con.get(field);
                        Matcher MyMatcher = MyPattern.matcher(value);
                        if(MyMatcher.matches()) {
                            value = '(' + value.substring(0, 3) + ') ' + value.substring(3, 6) + '-' + value.substring(6, 10);
                            con.put(field, value);
                        }
                    }
                } else if(con.get(field) != null) {
                    String value = (String)con.get(field);
                    Matcher MyMatcher = MyPattern.matcher(value);
                    if(MyMatcher.matches()) {
                        value = '(' + value.substring(0, 3) + ') ' + value.substring(3, 6) + '-' + value.substring(6, 10);
                        con.put(field, value);
                    }
                }
            }
        }
    }
}