/*
 * Date:             - Developer, Company                          - Description
 * 2015-08-12        - Stevie Yakkel, Acumen Solutions             - Edited
 */
@isTest
private class AccountSalesSummaryBatchTest {

    static testMethod void testSalesSummaryforOneAccount() {
        integer numAccounts = 1;
        
        setupTestData.createCustomSettings();
        setupTestData.createMetricTypesforSalesSummaryCustomSettings();
        setupTestData.createSalesSummaryData(numAccounts);

        String accountId;

        accountId = SetupTestData.testAccounts[0].Id;
        System.debug('***RCDEBUG***: Account Id: ' + accountId);

        test.startTest();
        Job_Failure_Notification_Email__c jfn = new Job_Failure_Notification_Email__c();
        jfn.Name = 'Email Id';
        jfn.Notification_Email__c = UserInfo.getUserEmail();
        insert jfn;
        AccountSalesSummaryBatch b = new AccountSalesSummaryBatch(accountId);
        database.executeBatch(b);
        test.stopTest();
    }

    static testMethod void testAggregateSummary() {
        integer numSalesSummaryAccounts = 1;
        integer NUM_METRIC_TYPES = 7;
        Map<String, Metric_Types_for_Sales_Summary__c> metricTypesMap;
        List<String> metricTypes;
        
        setupTestData.createCustomSettings();
        setupTestData.createMetricTypesforSalesSummaryCustomSettings();
        setupTestData.createSalesSummaryData(numSalesSummaryAccounts);
        
        metricTypesMap = Metric_Types_for_Sales_Summary__c.getAll();
        metricTypes = new List<String>(metricTypesMap.keySet());
        
        System.debug('***RCDEBUG***: Num Sales Summary Records : ' + SetupTestData.testSalesSummaryRecords.size());
        System.debug('***RCDEBUG***: Num Metric Types Maps Size : ' + metricTypesMap.size());
        System.debug('***RCDEBUG***: Num Metric Types List Sie : ' + metricTypes.size());

        integer i = 0;
        for (Sales_Summary__c ss: SetupTestData.testSalesSummaryRecords){
            //  GJK: 01/05/16 - Adding new fields Metric_Type__c, Classification__c, Week__c as part of Phase 2 - Sales Summary Enhancements
            System.debug('***RCDEBUG***: MOD Value: ' + Math.mod(i, NUM_METRIC_TYPES));
            String metricType = metricTypes.get(Math.mod(i, NUM_METRIC_TYPES));
            ss.Metric_Type__c = metricType;
            ss.Classification__c = '';
            ss.Week__c = 'W02'; // Test Date ssTime is selected as '12-31-2015' in setupTestData.createSalesSummaryData method.
            ss.Total_Sales_Amount__c = 1100.00;
            ss.Sales_Discount__c = 100.00;

            i++;
        }
        update SetupTestData.testSalesSummaryRecords;

        test.startTest();
        Job_Failure_Notification_Email__c jfn = new Job_Failure_Notification_Email__c();
        jfn.Name = 'Email Id';
        jfn.Notification_Email__c = UserInfo.getUserEmail();
        insert jfn;
        AccountSalesSummaryBatch b = new AccountSalesSummaryBatch();
        database.executeBatch(b);
        test.stopTest();
        /*
        //  GJK: Include_In_Report__c is no longer being updated as part of performance improvements for Phase 2 - Sales Summary Enhancements
        List<Sales_Summary__c> ssList = [SELECT Id, Include_In_Report__c FROM Sales_Summary__c WHERE Include_In_Report__c=true];
        System.assert(!ssList.isEmpty());
        */
    }

}