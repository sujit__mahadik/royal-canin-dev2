/*
 * Date:             - Developer, Company                          - Description
 * 2015-12-03        - Maruthi Gotety, Royal Canin                 - Created
 */
 
public without sharing class AccountRejectedEmailToOwner {
   public String ApprovalRequest { get; set; }
   public  String custRegId{set;get;}
   public String dearName{set;get;}
   public Customer_Registration__c customerRegistractionObj{set;get;}
   public Account accountObj{get;set;}
   public AccountRejectedEmailToOwner() {
      System.debug('--------Calling Constructor------');
      getRegistrationInfo();
   }

   public void getRegistrationInfo() {
      String channel= null;
      System.debug('--------Calling getRegistrationInfo------');
      System.debug('custRegId ====>'+custRegId );
      if(custRegId != null) {
          customerRegistractionObj = [select id,name,Shipping_Street_Address1__c,Shipping_Zip_Code__c,Shipping_Street_Address2__c,Ship_To_State__c,
          Shipping_City__c,recordtype.name,First_Name__c,Last_Name__c,Rejected_Reason__c,Processed_Rejected_Date__c,Name_of_Business_Organization__c from Customer_Registration__c where id =:custRegId ];
          List<Account> accountsList = [select id, name,owner.name,Customer_Registration__r.id from account where Customer_Registration__r.id =:customerRegistractionObj.Id];     
          if(accountsList  != null && !accountsList .isEmpty()) {
              accountObj = accountsList[0];
              dearName = accountsList[0].owner.name;
          }else {
             if(customerRegistractionObj != null) {
                 Map<String,channel_related_recordtypes__c> channelRecordTypes = channel_related_recordtypes__c.getAll();
                 List<RecordType> recordTypes = [SELECT Id,name FROM RecordType WHERE SObjectType='Customer_Registration__c'];
                 Map<String,String> recordTypeIdNameMap = new Map<String,String>();
                 for(RecordType recc: recordTypes ) {
                      recordTypeIdNameMap.put(recc.id,recc.name);
                 }
                 for(String channels : channelRecordTypes.keyset()) {
                      if(channelRecordTypes.get(channels).Record_Types__c.contains(recordTypeIdNameMap.get(customerRegistractionObj.recordtypeId))) {
                          channel = channels;
                          break;
                      }
                 }
                 TerritoryAssignment__c teritoryAssignments = [select id,owner.name,name from TerritoryAssignment__c where PostalCodeID__r.ZipCode__c =:customerRegistractionObj.Shipping_Zip_Code__c and PostalCodeID__r.Channel__c =:channel limit 1 ];
                 if(teritoryAssignments != null ) {
                     dearName = teritoryAssignments.owner.name;
                 }
              }
           }
      }
   }
}