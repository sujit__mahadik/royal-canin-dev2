/**********************************************************
* Purpose    : The purpose of this class to handle recursions in the triggers.  
*              
* *******************************************************
* Author                        Date            Remarks
* Devanshu Sharma               20/09/2018     
*********************************************************/
public class giic_RecursiveTriggerHandler{
    
    public static Boolean isFirstTime = false;
    public static Boolean bInventoryAdjustmentStagingTrigger = false;
    public static Boolean isWSALTrigger = false;
    /***********************************************************
    * Method name : run
    * Description : to handle recurension in triggers
    * Return Type : Boolean
    * Parameter : none
    ***********************************************************/
    public static Boolean run(){
        if(!isFirstTime){
            isFirstTime = true;
            return isFirstTime;
        }
        return false;
    }
    
    public static Boolean handleRecursionInventoryAdjStagingTrg(){
        if(!bInventoryAdjustmentStagingTrigger){
            bInventoryAdjustmentStagingTrigger = true;
            return bInventoryAdjustmentStagingTrigger;
        }
        return false;
    }
    
    public static Boolean handleRecursionWSALTrg(){
        if(!isWSALTrigger){
            isWSALTrigger = true;
            return isWSALTrigger;
        }
        return false;
    }
    
}