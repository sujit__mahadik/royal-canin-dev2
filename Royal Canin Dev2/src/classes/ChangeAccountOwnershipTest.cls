/*
 * Date:       		 - Developer, Company					       - Description
 * 2015-08-10        - Stevie Yakkel, Acumen Solutions     		   - Created
 */

@isTest
private class ChangeAccountOwnershipTest
{
	@isTest
	static void ChangeAccountOnwershipWithAccountUserMapTest() {
		SetupTestData.createCustomSettings();
		ChangeAccountOwnership cao = new ChangeAccountOwnership();
		Map<Id, Id> accAndUserMap = new Map<Id, Id>();
		String s = cao.ChangeAccountOnwershipWithAccountUserMap(accAndUserMap);
		System.assertEquals(s, '');
	}

	@isTest
	static void ChangeAccountOwnershipWithZipAndUserTest() {
		SetupTestData.createCustomSettings();
		ChangeAccountOwnership cao = new ChangeAccountOwnership();
		Map<Id, Id> zipCodeAssignments = new Map<Id, Id>();
		PostalCode__c pc1 = new PostalCode__c();
		pc1.ZipCode__c = '44133';
		PostalCode__c pc2 = new PostalCode__c();
		pc2.ZipCode__c = '44134';
		insert pc1;
		insert pc2;
		SetupTestData.createAccounts(2);
		zipCodeAssignments.put(SetupTestData.testAccounts[0].Id, pc1.Id);
		zipCodeAssignments.put(SetupTestData.testAccounts[1].Id, pc2.Id);
		Map<Id, Id> managers = new Map<Id, Id>();
		String s = cao.ChangeAccountOwnershipWithZipAndUser(zipCodeAssignments, managers);
	}

	@isTest
	static void changeAccountOwnershipWithListOfAccountsTest() {
		SetupTestData.createCustomSettings();
		SetupTestData.createAccounts(2);
		List<Account> accList = new List<Account>();
		Territory__c t = new Territory__c();
		insert t;
		PostalCode__c pc1 = new PostalCode__c();
		pc1.ZipCode__c = '44133';
		insert pc1;
		Account acc1 = setupTestData.testAccounts[0];
		//acc1.Name = 'acc';
		acc1.PostalCodeID__c = pc1.Id;
		acc1.Exclude_from_Territory_Assignment__c = false;
		update acc1;
		accList.add(acc1);
		Fiscal_Year__c fy = new Fiscal_Year__c();
		fy.isActive__c = true;
		insert fy;
		TerritoryAssignment__c ta = new TerritoryAssignment__c ();
		ta.PostalCodeID__c = pc1.Id;
		ta.Fiscal_Year__c = fy.Id;
		ta.TerritoryID__c = t.Id;
		insert ta;
		ChangeAccountOwnership.changeAccountOwnershipWithListOfAccounts(accList);
	}

	@isTest
	static void UnAssignAccountsInZipTest() {
		SetupTestData.createCustomSettings();
		ChangeAccountOwnership cao = new ChangeAccountOwnership();
		List<String> zips = new List<String>();
		String s = cao.UnAssignAccountsInZip(zips);
		System.assertEquals(s, '');
	}

	@isTest
	static void UnAssignAccountsFromListTest() {
		SetupTestData.createCustomSettings();
		ChangeAccountOwnership cao = new ChangeAccountOwnership();
		List<Id> accIds = new List<Id>();
		String s = cao.UnAssignAccountsFromList(accIds);
		System.assertEquals(s, '');
	}
}