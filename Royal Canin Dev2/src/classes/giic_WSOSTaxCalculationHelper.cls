/************************************************************************************
Version : 1.0
Created Date : 17 jan 2019
Function : I
Modification Log : 

* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/
public with sharing class giic_WSOSTaxCalculationHelper {
    public static List<gii__WarehouseShippingOrderStaging__c> lstWSOS = new List<gii__WarehouseShippingOrderStaging__c>();
    public static List<string> lsrErrror = new List<String>();
    @auraEnabled
    public static Map<String,Object> CalculateTax(String sWSOSid) // null check
    {        
        Id salesOrderId = null;
        String strWarehouseCode = '';
        string strErrorMsg = '';
        boolean bTaxExempt = false;           
        List<String> lstSOLName =  new List<String>();  
        list<gii__SalesOrderLine__c> lstSOlineUpdate = new list<gii__SalesOrderLine__c>();
        Id errorRec = sWSOSid; 
        Map<String,Object> result = new Map<String,Object>();
        gii__WarehouseShippingOrderStaging__c objWSOS = new gii__WarehouseShippingOrderStaging__c();
        result.put('SUCCESS',false);        
        if(sWSOSid != null && sWSOSid != ''){                
            try
            {   
                lstWSOS = [SELECT Id, name, giic_SalesOrder__c,giic_IntegrationStatus__c, giic_Status__c, giic_SalesOrder__r.giic_OrderNo__c, giic_SalesOrder__r.giic_TaxExempt__c, giic_SalesOrder__r.gii__Warehouse__r.giic_WarehouseCode__c, (SELECT Id,giic_SalesOrderLine__r.Name, giic_SalesOrderLine__r.giic_LineNo__c, giic_WHShippingOrderStaging__c FROM Warehouse_Shipping_Order_Lines_Staging__r) FROM gii__WarehouseShippingOrderStaging__c where Id =: sWSOSid];
                // 
                // system.debug('lstWSOS-------->>>>>>> ' + lstWSOS);
                
                if(lstWSOS != null && lstWSOS.size() > 0 
                   && lstWSOS[0].giic_SalesOrder__c != null 
                   && lstWSOS[0].giic_SalesOrder__r.gii__Warehouse__c != null
                   && lstWSOS[0].giic_SalesOrder__r.gii__Warehouse__r.giic_WarehouseCode__c != null
                   && lstWSOS[0].Warehouse_Shipping_Order_Lines_Staging__r != null
                   && lstWSOS[0].Warehouse_Shipping_Order_Lines_Staging__r.size() > 0
                   && lstWSOS[0].giic_IntegrationStatus__c == giic_Constants.CALCULATE_TAX
                   && (lstWSOS[0].giic_Status__c == giic_Constants.FF_STATUS_IN_PROGRESS || lstWSOS[0].giic_Status__c == giic_Constants.FULLFILLMWNT_SUBMITTEDWITHERROR )
                  ){
                      
                      errorRec = lstWSOS[0].Id;
                      salesOrderId = lstWSOS[0].giic_SalesOrder__c;
                      bTaxExempt =  lstWSOS[0].giic_SalesOrder__r.giic_TaxExempt__c;
                      strWarehouseCode = lstWSOS[0].giic_SalesOrder__r.gii__Warehouse__r.giic_WarehouseCode__c;
                      if(lstWSOS[0].Warehouse_Shipping_Order_Lines_Staging__r != null && lstWSOS[0].Warehouse_Shipping_Order_Lines_Staging__r.size() > 0){
                          for(gii__WarehouseShippingOrderLinesStaging__c obj : lstWSOS[0].Warehouse_Shipping_Order_Lines_Staging__r){  
                              lstSOLName.add((obj.giic_SalesOrderLine__r.Name).trim()); //giic_OrderLineNo__c - text field
                          }
                      }
                  }else{
                      // create error Log
                    
                    if(lstWSOS.size() == 0 
                    || lstWSOS[0].giic_SalesOrder__r.gii__Warehouse__c == null 
                    || lstWSOS[0].giic_SalesOrder__r.gii__Warehouse__r.giic_WarehouseCode__c == null 
                    || lstWSOS[0].Warehouse_Shipping_Order_Lines_Staging__r == null ){
                        strErrorMsg = strErrorMsg + giic_Constants.TAX_CALCULATION_ERROR1;
                    }
                    
                    if(lstWSOS.size() > 0 && lstWSOS[0].giic_IntegrationStatus__c != giic_Constants.CALCULATE_TAX ){
                        strErrorMsg = strErrorMsg +'   ' + label.giic_WSOSTaxCalcuStatusError;
                    }
                    else if(lstWSOS.size() > 0 && (lstWSOS[0].giic_Status__c != giic_Constants.FF_STATUS_IN_PROGRESS || lstWSOS[0].giic_Status__c != giic_Constants.FULLFILLMWNT_SUBMITTEDWITHERROR )){
                        strErrorMsg = strErrorMsg + '   ' + giic_Constants.TAX_CALCULATION_STATUS_ERROR; 
                    }
                    result.put('ERROR', strErrorMsg);
                    giic_CyberSourceRCCCUtility.handleErrorFuture(errorRec, giic_Constants.TAX_CALCULATION_FROM_WSOS, giic_Constants.TAX_CALCULATION_FROM_WSOS,strErrorMsg );
                    
                    // system.debug('result-------->>>>>>> ' + result);
                    
                    return result;
                  }
                // system.debug('lstSOLName-------->>>>>>> ' + lstSOLName);
                if(lstSOLName.size()>0){
                    //Check for Account Tax Exemption before Calculating Tax (bTaxExempt  = gii__SalesOrder__c.giic_TaxExempt__c
                    
                    if(!bTaxExempt) // if(!lstobjOrder[0].giic_TaxExempt__c)
                    {
                        result = giic_AvalaraServiceProviderClass.CalculateTaxforSO(salesOrderId,lstSOLName,strWarehouseCode,giic_Constants.COMMIT_T_FALSE ,true);
                        
                        // system.debug('result--->>>>>>>>>>> ' + result);
                        
                        if(result.containsKey('ISSUCCESS') && result.containsKey('ISSUCCESS') == true) 
                        {
                            result.put('SUCCESS',true);
                        }
                        else {
                            result.put('ERROR', label.giic_TaxCalculationFailingError); 
                            giic_CyberSourceRCCCUtility.handleErrorFuture(errorRec, giic_Constants.TAX_CALCULATION_FROM_WSOS, giic_Constants.TAX_CALCULATION_FROM_WSOS,label.giic_TaxCalculationFailingError);
                        }
                    }else
                    {  
                       result.put('SUCCESS',true);
                    }
                }
            }catch(Exception ex) {
                system.debug('exp:' + ex.getMessage() + ex.getLineNumber()); 
                result.put('ERROR', label.giic_TaxCalculationFailingError);
                giic_CyberSourceRCCCUtility.handleErrorFuture(errorRec, giic_Constants.TAX_CALCULATION_FROM_WSOS, giic_Constants.TAX_CALCULATION_FROM_WSOS, ex.getMessage()+ giic_Constants.LINE_NUMBER + ex.getLineNumber() );
            }
        }
        return result;
        
    }
    
    
    @auraEnabled
    public static Map<String,Object> deleteSOPS_AndCreatenewSPOS(String sWSOSid) // null check
    {   
        string salesOrderId = '';
        Savepoint sp = Database.setSavepoint();
        Map<String,Object> result = new Map<String,Object>();
        result.put('SUCCESS',false);
        try{
            
             
            lstWSOS = [SELECT Id, name, giic_SalesOrder__c, giic_SalesOrder__r.giic_OrderNo__c, giic_SalesOrder__r.giic_TaxExempt__c, giic_SalesOrder__r.gii__Warehouse__r.giic_WarehouseCode__c, (SELECT Id,giic_SalesOrderLine__r.Name, giic_SalesOrderLine__r.giic_LineNo__c, giic_WHShippingOrderStaging__c FROM Warehouse_Shipping_Order_Lines_Staging__r) FROM gii__WarehouseShippingOrderStaging__c where giic_IntegrationStatus__c =: giic_Constants.CALCULATE_TAX AND Id =: sWSOSid];
            
             if(lstWSOS != null && lstWSOS.size()> 0 && lstWSOS[0].giic_SalesOrder__c != null){
                salesOrderId =  lstWSOS[0].giic_SalesOrder__c;
            }
            // delete existing SOPS
            boolean bSOPSDeleted = deleteSalesOrderPaymentSettlement(lstWSOS);
            
            if(!bSOPSDeleted){
                Database.rollback( sp );
                result.put('ERROR', label.giic_TaxCalculationFailingError); 
                giic_CyberSourceRCCCUtility.handleErrorFuture(sWSOSid, giic_Constants.TAX_CALCULATION_FROM_WSOS, giic_Constants.TAX_CALCULATION_FROM_WSOS,label.giic_TaxCalculationFailingError + '  ' + string.join(lsrErrror,','));
                return result;
            }
            // call  SalesOrderPaymentSettlement creation function
            giic_WhShippingOrderStagingWrapper ObjWHShipOdrStg  = getWarehouseShippingOSWrapper(lstWSOS);
            // below methos is being called to create Sales Order payment settlement records 
           // system.debug( ' ObjWHShipOdrStg----------->>>>>>>>>>  ' + ObjWHShipOdrStg );
            lsrErrror = giic_UpdateTaxAndCreateSOPSWS.processSOPSettlementRequest(ObjWHShipOdrStg);
            // system.debug( ' lsrErrror----------->>>>>>>>>>  ' + lsrErrror );
            if(lsrErrror != Null && lsrErrror.size() > 0){
                Database.rollback( sp );
                result.put('ERROR', label.giic_TaxCalculationFailingError + ' ' + string.join(lsrErrror,',')); 
                giic_CyberSourceRCCCUtility.handleErrorFuture(sWSOSid, giic_Constants.TAX_CALCULATION_FROM_WSOS, giic_Constants.TAX_CALCULATION_FROM_WSOS,label.giic_TaxCalculationFailingError + '  ' + string.join(lsrErrror,','));
                return result; 
            }else{
                result.put('SUCCESS',true);           
            }  
        }catch(Exception ex){
            Database.rollback( sp );
            lsrErrror.add(ex.getMessage() +   ex.getLineNumber() );
            result.put('ERROR', label.giic_TaxCalculationFailingError); 
            giic_CyberSourceRCCCUtility.handleErrorFuture(sWSOSid, giic_Constants.TAX_CALCULATION_FROM_WSOS, giic_Constants.TAX_CALCULATION_FROM_WSOS,label.giic_TaxCalculationFailingError + '  ' + string.join(lsrErrror,','));
        }
        return result; 
    }    
    
    public static boolean deleteSalesOrderPaymentSettlement(List<gii__WarehouseShippingOrderStaging__c> lstWSOS){
        boolean bSOPSDeleted = false;
        List<gii__SalesOrderPaymentSettlement__c> lstSOPS = [select id from gii__SalesOrderPaymentSettlement__c where giic_WarehouseShippingOrderStaging__c in : lstWSOS];
        // system.debug( ' Deleted----------->>>>>>>>>>  ' + lstSOPS );
        if(lstSOPS == null || lstSOPS.size() == 0 ){
            bSOPSDeleted = true;
        }
        else if(lstSOPS != null && lstSOPS.size() > 0 ){
            try{
                
                delete lstSOPS;
                bSOPSDeleted = true;
            }catch(Exception exc){
                bSOPSDeleted = false;
                lsrErrror.add( exc.getMessage()+ ' ' + exc.getLineNumber());
            }
        }
        return bSOPSDeleted;
    }
    
    public static giic_WhShippingOrderStagingWrapper getWarehouseShippingOSWrapper(List<gii__WarehouseShippingOrderStaging__c> lstWSOS){
        giic_WhShippingOrderStagingWrapper ObjWHShipOdrStg = new giic_WhShippingOrderStagingWrapper();
        List<giic_WhShippingOrderStagingWrapper.WarehouseShippingOrderStaging> listWSOS = new List<giic_WhShippingOrderStagingWrapper.WarehouseShippingOrderStaging>();
        giic_WhShippingOrderStagingWrapper.WarehouseShippingOrderStaging objWSOS;
        if(lstWSOS != null && lstWSOS.size() > 0 ){
            for(gii__WarehouseShippingOrderStaging__c oWSOS : lstWSOS ){
                objWSOS = new giic_WhShippingOrderStagingWrapper.WarehouseShippingOrderStaging();
                objWSOS.WSOS_Id = oWSOS.Id;
                objWSOS.WSOS_SOId = oWSOS.giic_SalesOrder__c;
                List<giic_WhShippingOrderStagingWrapper.WarehouseShippingOrderLineStaging> WhSOdrStgsLines;
                giic_WhShippingOrderStagingWrapper.WarehouseShippingOrderLineStaging objWSOLS;
                if(oWSOS.Warehouse_Shipping_Order_Lines_Staging__r != null && oWSOS.Warehouse_Shipping_Order_Lines_Staging__r.size() > 0 ){
                    WhSOdrStgsLines = new List<giic_WhShippingOrderStagingWrapper.WarehouseShippingOrderLineStaging>();
                    for(gii__WarehouseShippingOrderLinesStaging__c oWSOLS : oWSOS.Warehouse_Shipping_Order_Lines_Staging__r  ){
                        objWSOLS = new giic_WhShippingOrderStagingWrapper.WarehouseShippingOrderLineStaging();
                        objWSOLS.WSOLS_SOLId = oWSOLS.giic_SalesOrderLine__c;
                        WhSOdrStgsLines.add(objWSOLS);
                    }
                    objWSOS.WhSOdrStgsLines = WhSOdrStgsLines;
                }
                listWSOS.add(objWSOS);
            }
        }
        ObjWHShipOdrStg.WhSOdrStgs = listWSOS;
        
        return ObjWHShipOdrStg;
    }   
}