/************************************************************************************
Version : 1.0
Name : giic_Test_SOSAddressValidationBatch
Created Date : 10 Sep 2018
Function : Test giic_SOSAddressValidationBatch class
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/
@isTest(SeeAllData = false)
private class giic_Test_SOSAddressValidationBatch {   
    
        @testSetup static void testData(){
             giic_Test_DataCreationUtility.insertWarehouse();
            giic_Test_DataCreationUtility.testSystemPolicyCreation();
            giic_Test_DataCreationUtility.insertGlobalParametersCustomSetting();
            giic_Test_DataCreationUtility.insertConsumerAccount();
            giic_Test_DataCreationUtility.insertProduct();
            giic_Test_DataCreationUtility.insertSalesOrderStaging();
            giic_Test_DataCreationUtility.insertSalesOrderLineStaging();
            giic_Test_DataCreationUtility.CreateAdminUser();
        }
        private static testMethod void test() {
             User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
             
             system.runAs(u){
                    List<gii__SalesOrderStaging__c> lstSOStaging =  [ select id,giic_ShipToName__c ,giic_ShipToZipPostalCode__c ,giic_ShipToStateProvince__c ,giic_Carrier__c ,giic_CustEmail__c ,
                                                            giic_PaymentTerms__c ,giic_OrderDate__c ,giic_ShipToCity__c ,giic_OrderOrigin__c ,giic_OrderNumber__c ,giic_WarehouseCode__c ,
                                                            giic_PromotionCalculated__c ,giic_ShipToStreet__c ,giic_ExtDocNo__c ,giic_TaxCalculated__c ,giic_BillToCustID__c ,
                                                            giic_Comments__c ,giic_CCAuthorized__c ,giic_BillingStateProvince__c ,giic_BillingStreet__c ,giic_AddressType__c ,
                                                            giic_BillingZipPostalCode__c ,giic_BillingCity__c ,giic_Source__c ,giic_PaymentMethod__c ,giic_ShippingCounty__c ,giic_BillingCounty__c ,
                                                            giic_CustPhone__c ,giic_ShipToCountry__c ,giic_CustomerPODate__c ,giic_Status__c ,giic_AddressValidated__c ,
                                                            giic_Account__c from gii__SalesOrderStaging__c ];
                    lstSOStaging[0].giic_AddressValidated__c = false;
                    update lstSOStaging[0];
               
                    Test.startTest();    
                    giic_SOSAddressValidationBatch obj = new giic_SOSAddressValidationBatch ();
                    Database.QueryLocator ql = obj .start(null);
                    obj.execute(null,giic_Test_DataCreationUtility.lstSOStaging);
                    obj.Finish(null);
                    Test.stopTest();
             }
    }
    
    

}