/************************************************************************************
Version : 1.0
Created Date : 25-Oct-2018
Function : Mock Test class for HTTPCallout
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/


@isTest
global class giic_Test_ErrAvalaraCalloutMockImpl implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {        
        // Create a fake response       
        String body = '{"error":123456789}'; 
        
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody(body);
        res.setStatusCode(200);
        return res;
    }
}