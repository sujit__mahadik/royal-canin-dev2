/*
 * Date:       		 - Developer, Company					       - Description
 * 2015-05-04        - Mayank Srivastava, Acumen Solutions         - Created
 * 2015-08-10		 - Stevie Yakkel, Acumen Solutions			   - Edited
 */
public with sharing class ChangeAccountOwnershipHelperUtil {
	
	public static Fiscal_Year__c getCurrentFiscalYear(){
			Fiscal_Year__c fy = [Select id, Name, Start_Date__c, Activation_Date__c, isActive__c from Fiscal_Year__c where IsActive__c = :TRUE];
			return fy;
	}	

	public static Fiscal_Year__c getFiscalYearForPromotion(){
		List<Fiscal_Year__c> fyForPromotion = [Select id, Name, Start_Date__c,Activation_Date__c,isActive__c from Fiscal_Year__c where isActive__c = :False and Activation_Date__c <= TODAY
							 and End_Date__c > :System.now().date() order by End_Date__c desc limit 1];
		if(fyForPromotion.size()>0){
			return fyForPromotion[0];
		}
		return null;					 
	}

	public static void promoteFiscalYearToActive(Fiscal_Year__c fyForPromotion){
		fyForPromotion.isActive__c = true;
		Fiscal_Year__c currentFiscalYear = getCurrentFiscalYear();
		currentFiscalYear.isActive__c =  false;
		update currentFiscalYear;
		update fyForPromotion;
	}
		
}