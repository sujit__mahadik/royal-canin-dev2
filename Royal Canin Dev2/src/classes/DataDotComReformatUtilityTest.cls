/*
 * Date:             - Developer, Company                          - description
 * 2015-08-05        - Stevie Yakkel, Acumen Solutions             - Created
 */

@isTest
private class DataDotComReformatUtilityTest
{
	@isTest
	static void dataDotComAccountPhoneReformatTest() {
		SetupTestData.createCustomSettings();
		SetupTestData.createAccounts(2);
		Account accountWithPhoneFormatIssues = SetupTestData.testAccounts[0];
		Account accountWithoutPhoneFormatIssues = SetupTestData.testAccounts[1];

		accountWithoutPhoneFormatIssues.Name = 'Test';
		accountWithoutPhoneFormatIssues.Phone = '(123) 456-7890';

		accountWithPhoneFormatIssues.Name = 'Tester';
		accountWithPhoneFormatIssues.Phone = '+1.223.456.7890';

		Test.startTest();
		update accountWithPhoneFormatIssues;
		update accountWithoutPhoneFormatIssues;
		Test.stopTest();

		Account acc = [SELECT Phone FROM Account WHERE Id = :accountWithPhoneFormatIssues.Id];
		Account acc2 = [SELECT Phone FROM Account WHERE Id = :accountWithoutPhoneFormatIssues.Id];

		System.assertEquals('(123) 456-7890', acc2.Phone);
		System.assertEquals('(223) 456-7890', acc.Phone);
	}

	@isTest
	static void dataDotComAccountFaxReformatTest() {
		SetupTestData.createCustomSettings();
		SetupTestData.createAccounts(2);
		Account accountWithoutFaxFormatIssues = SetupTestData.testAccounts[0];
		Account accountWithFaxFormatIssues = SetupTestData.testAccounts[1];

		accountWithoutFaxFormatIssues.Name = 'Test';
		accountWithoutFaxFormatIssues.Fax = '(123) 456-7890';

		accountWithFaxFormatIssues.Name = 'Tester';
		accountWithFaxFormatIssues.Fax = '+1.223.456.7890';

		Test.startTest();
		update accountWithFaxFormatIssues;
		update accountWithoutFaxFormatIssues;
		Test.stopTest();

		Account acc = [SELECT Fax FROM Account WHERE Id = :accountWithFaxFormatIssues.Id];
		Account acc2 = [SELECT Fax FROM Account WHERE Id = :accountWithoutFaxFormatIssues.Id];

		System.assertEquals('(123) 456-7890', acc2.Fax);
		System.assertEquals('(223) 456-7890', acc.Fax);
	}

	@isTest
	static void dataDotComAccountShippingPostalCodeReformatTest() {
		SetupTestData.createCustomSettings();
		SetupTestData.createAccounts(2);
		Account accountWithoutShippingPostalCodeFormatIssues = SetupTestData.testAccounts[0];
		Account accountWithShippingPostalCodeFormatIssues = SetupTestData.testAccounts[1];

		accountWithoutShippingPostalCodeFormatIssues.Name = 'Test';
		accountWithoutShippingPostalCodeFormatIssues.ShippingPostalCode = '44134';

		accountWithShippingPostalCodeFormatIssues.Name = 'Tester';
		accountWithShippingPostalCodeFormatIssues.ShippingPostalCode = '44134-4235';

		Test.startTest();
		update accountWithoutShippingPostalCodeFormatIssues;
		update accountWithShippingPostalCodeFormatIssues;
		Test.stopTest();

		Account acc = [SELECT ShippingPostalCode FROM Account WHERE Id = :accountWithoutShippingPostalCodeFormatIssues.Id];
		Account acc2 = [SELECT ShippingPostalCode FROM Account WHERE Id = :accountWithShippingPostalCodeFormatIssues.Id];

		System.assertEquals('44134', acc.ShippingPostalCode);
		System.assertEquals('44134', acc2.ShippingPostalCode);
	}

	@isTest
	static void dataDotComAccountBillingPostalCodeReformatTest() {
		SetupTestData.createCustomSettings();
		SetupTestData.createAccounts(2);
		Account accountWithBillingPostalCodeFormatIssues = SetupTestData.testAccounts[0];
		Account accountWithoutBillingPostalCodeFormatIssues = SetupTestData.testAccounts[1];

		accountWithoutBillingPostalCodeFormatIssues.Name = 'Test';
		accountWithoutBillingPostalCodeFormatIssues.BillingPostalCode = '44134';

		accountWithBillingPostalCodeFormatIssues.Name = 'Tester';
		accountWithBillingPostalCodeFormatIssues.BillingPostalCode = '44134-4235';

		Test.startTest();
		update accountWithoutBillingPostalCodeFormatIssues;
		update accountWithBillingPostalCodeFormatIssues;
		Test.stopTest();

		Account acc = [SELECT BillingPostalCode FROM Account WHERE Id = :accountWithoutBillingPostalCodeFormatIssues.Id];
		Account acc2 = [SELECT BillingPostalCode FROM Account WHERE Id = :accountWithBillingPostalCodeFormatIssues.Id];

		System.assertEquals('44134', acc.BillingPostalCode);
		System.assertEquals('44134', acc2.BillingPostalCode);
	}

	@isTest
	static void dataDotComContactPhoneReformatTest() {
		SetupTestData.createCustomSettings();
		List<Contact> contacts = new List<Contact>();
		Contact contactWithPhoneFormatIssues = new Contact();
		Contact contactWithoutPhoneFormatIssues = new Contact();

		contactWithoutPhoneFormatIssues.FirstName = 'Test';
		contactWithoutPhoneFormatIssues.LastName = 'Test';
		contactWithoutPhoneFormatIssues.Phone = '(123) 456-7890';

		contactWithPhoneFormatIssues.FirstName = 'Tester';
		contactWithPhoneFormatIssues.LastName = 'Tester';
		contactWithPhoneFormatIssues.Phone = '+1.223.456.7890';

		Test.startTest();
		System.debug('start');
		insert contactWithPhoneFormatIssues;
		System.debug('here we go');
		insert contactWithoutPhoneFormatIssues;
		Test.stopTest();

		Contact con1 = [SELECT Phone FROM Contact WHERE Id = :contactWithPhoneFormatIssues.Id];
		Contact con2 = [SELECT Phone FROM Contact WHERE Id = :contactWithoutPhoneFormatIssues.Id];

		System.assertEquals('(123) 456-7890', con2.Phone);
		System.assertEquals('(223) 456-7890', con1.Phone);
	}

	@isTest
	static void dataDotComContactFaxReformatTest() {
		SetupTestData.createCustomSettings();
		List<Contact> contacts = new List<Contact>();
		Contact contactWithFaxFormatIssues = new Contact();
		Contact contactWithoutFaxFormatIssues = new Contact();

		contactWithoutFaxFormatIssues.FirstName = 'Test';
		contactWithoutFaxFormatIssues.LastName = 'Test';
		contactWithoutFaxFormatIssues.Fax = '(123) 456-7890';

		contactWithFaxFormatIssues.FirstName = 'Tester';
		contactWithFaxFormatIssues.LastName = 'Tester';
		contactWithFaxFormatIssues.Fax = '+1.223.456.7890';

		Test.startTest();
		insert contactWithoutFaxFormatIssues;
		insert contactWithFaxFormatIssues;
		Test.stopTest();

		Contact con1 = [SELECT Fax FROM Contact WHERE Id = :contactWithoutFaxFormatIssues.Id];
		Contact con2 = [SELECT Fax FROM Contact WHERE Id = :contactWithFaxFormatIssues.Id];

		System.assertEquals('(123) 456-7890', con1.Fax);
		System.assertEquals('(223) 456-7890', con2.Fax);
	}

	@isTest
	static void dataDotComContactMailingPostalCodeReformatTest() {
		SetupTestData.createCustomSettings();
		List<Contact> contacts = new List<Contact>();
		Contact contactWithoutMailingPostalCodeFormatIssues = new Contact();
		Contact contactWithMailingPostalCodeFormatIssues = new Contact();

		contactWithoutMailingPostalCodeFormatIssues.FirstName = 'Test';
		contactWithoutMailingPostalCodeFormatIssues.LastName = 'Test';
		contactWithoutMailingPostalCodeFormatIssues.MailingPostalCode = '44134';

		contactWithMailingPostalCodeFormatIssues.FirstName = 'Tester';
		contactWithMailingPostalCodeFormatIssues.LastName = 'Tester';
		contactWithMailingPostalCodeFormatIssues.MailingPostalCode = '44134-4235';

		contacts.add(contactWithoutMailingPostalCodeFormatIssues);
		contacts.add(contactWithMailingPostalCodeFormatIssues);
		Test.startTest();
		insert contacts;
		Test.stopTest();

		Contact con1 = [SELECT MailingPostalCode FROM Contact WHERE Id = :contactWithoutMailingPostalCodeFormatIssues.Id];
		Contact con2 = [SELECT MailingPostalCode FROM Contact WHERE Id = :contactWithMailingPostalCodeFormatIssues.Id];

		System.assertEquals('44134', con1.MailingPostalCode);
		System.assertEquals('44134', con2.MailingPostalCode);
	}

	@isTest
	static void dataDotComContactOtherPostalCodeReformatTest() {
		SetupTestData.createCustomSettings();
		List<Contact> contacts = new List<Contact>();
		Contact contactWithOtherPostalCodeFormatIssues = new Contact();
		Contact contactWithoutOtherPostalCodeFormatIssues = new Contact();

		contactWithoutOtherPostalCodeFormatIssues.FirstName = 'Test';
		contactWithoutOtherPostalCodeFormatIssues.LastName = 'Test';
		contactWithoutOtherPostalCodeFormatIssues.OtherPostalCode = '44134';

		contactWithOtherPostalCodeFormatIssues.FirstName = 'Tester';
		contactWithOtherPostalCodeFormatIssues.LastName = 'Tester';
		contactWithOtherPostalCodeFormatIssues.OtherPostalCode = '44134-4235';

		contacts.add(contactWithoutOtherPostalCodeFormatIssues);
		contacts.add(contactWithOtherPostalCodeFormatIssues);
		Test.startTest();
		insert contacts;
		Test.stopTest();

		Contact con1 = [SELECT OtherPostalCode FROM Contact WHERE Id = :contactWithOtherPostalCodeFormatIssues.Id];
		Contact con2 = [SELECT OtherPostalCode FROM Contact WHERE Id = :contactWithoutOtherPostalCodeFormatIssues.Id];

		System.assertEquals('44134', con1.OtherPostalCode);
		System.assertEquals('44134', con2.OtherPostalCode);
	}
}