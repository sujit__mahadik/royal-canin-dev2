/************************************************************************************
Version : 1.0
Name : giic_IntegrationCustomMetaDataWrapper
Created Date : 16 Aug 2018
Function :  Wrapper class for Conversion of Sales Order Staging to Sales Order
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/
global class giic_IntegrationCustomMetaDataWrapper{
    public String userStory{get;set;}
    public String strSoql {get;set;}
    public String sourceSobjectApi {get;set;}
    public String strWhereClause {get;set;}
    public String targetSobjectApi {get;set;}
    public String convertedFieldApi {get;set;}
    public Map<String, String> fieldApiForErrorLog{get;set;}
    public Map<String, giic_IntegrationMapping__mdt> mapIDetail {get;set;}
    public Map<String, Map<String, Map<String, giic_IntegrationValidation__mdt>>> mapValidation {get;set;}
    public Map<String, Map<String, Map<String, giic_IntegrationDefaultData__mdt>>> mapDefaultValue {get;set;}
    
    public giic_IntegrationCustomMetaDataWrapper(){
        userStory = '';
        strSoql = '';
        strWhereClause = '';
        sourceSobjectApi = '';
        targetSobjectApi = '';
        convertedFieldApi = '';
        fieldApiForErrorLog = new Map<String, String>();//key => field api of Error sobject and value api of source field
        mapIDetail = new Map<String, giic_IntegrationMapping__mdt>();
        mapValidation = new Map<String, Map<String, Map<String, giic_IntegrationValidation__mdt>>>();//Source --> Sobject --> Fields
        mapDefaultValue = new Map<String, Map<String, Map<String, giic_IntegrationDefaultData__mdt>>>();//Source --> Sobject --> Fields
    }
}