/************************************************************************************
Version : 1.0
Name : giic_WhShippingOrderStagingWrapper 
Created Date : 03 Dec 2018
Function :  wrapper class used in giic_WhShippingOdrStagQueueable, giic_UpdateTaxAndCreateSOPSWS
Modification Log :
Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/
global class giic_WhShippingOrderStagingWrapper{
    
    public List<WarehouseShippingOrderStaging> WhSOdrStgs;

    public giic_WhShippingOrderStagingWrapper(){
        WhSOdrStgs = new List<WarehouseShippingOrderStaging>();
    }
    
    public class WarehouseShippingOrderStaging{
        public string WSOS_Id;
        public string WSOS_SOId;
        public List<WarehouseShippingOrderLineStaging> WhSOdrStgsLines;
    }

    public class WarehouseShippingOrderLineStaging{
        public string WSOLS_SOLId; 
        public Decimal WSOLS_TaxAmount;
        
    }
    

}