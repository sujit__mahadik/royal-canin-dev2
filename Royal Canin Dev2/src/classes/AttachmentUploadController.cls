public with sharing class AttachmentUploadController {
/*
 * Controller for multi attachment component
 */
	// the parent object it
	public Id sobjId { get; set; }
	public string appId { get; set; }

	public CustomerOnboardingController myctrl {get;set;}

	// list of existing attachments - populated on demand
	public  List<Attachment> attachments;

	// list of new attachments to add
    public  List<Attachment> newAttachments  {
        
    get {
        
        if (newAttachments!=null){
            for (Attachment a: newAttachments){
               // a.body=null;
            }
        }
        else {
            newAttachments = new List <Attachment>();
        }
        return newAttachments;
    }
    set {
        
    }
    }
	public Customer_Registration__c custReg { get; set; }
	
	public string selectedAttachmentId { get; set; }
	public String passedParam { get; set; }
	public string attachToDeleteId { get; set;}
	public Map<String, Id> registrationRecordTypes = new Map<String, Id>();
	public Map<String, String> attDescriptions = new Map<String, String>();

	public string ACHLink { get; set; }
	public boolean renderACH { get; set; }
	public String SalesAndUseTaxForm { get; set; }
	public boolean renderSalesAndUseTaxForm { get; set; }
	public boolean renderRequiredDocs {
		get {
			if(custReg != null) {
				if(custReg.RecordTypeId == registrationRecordTypes.get('Veterinarian') ) {
					return true;
				}
				else if(custReg.RecordTypeId == registrationRecordTypes.get('Shelter') ){
					return true;
				}
				else if(custReg.RecordTypeId == registrationRecordTypes.get('Retailer') ){
					system.debug('&&&&&reqdocs true');
					return true;
				}
				else if(custReg.RecordTypeId == registrationRecordTypes.get('Retail_Sell_To')){
					return true;
				}
				else if(custReg.RecordTypeId == registrationRecordTypes.get('Vet_Sell_To')){
					return true;
				}
				else if(custReg.RecordTypeId == registrationRecordTypes.get('Working_Dog')){
					return true;
				}
				else {
					system.debug('&&&&&reqdocs false');
					return false;
				}
			}
			else {
				system.debug('&&&&&reqdocs false bc id null');
				return false;
			}
		}
	}

	public boolean renderExistingDocs { 
		get {
			if(custReg != null) {
				if(custReg.RecordTypeId == registrationRecordTypes.get('Veterinarian') && (custReg.Upload_Complete_Practitioner__c == true || custReg.Upload_Complete_Resale__c == true)) {
					return true;
				}
				else if(custReg.RecordTypeId == registrationRecordTypes.get('Shelter') && custReg.Upload_Complete_Tax__c == true){
					return true;
				}
				else if(custReg.RecordTypeId == registrationRecordTypes.get('Retailer') && custReg.Upload_Complete_Resale__c == true){
					system.debug('&&&&&existingdocs true');
					return true;
				}
				else if(custReg.RecordTypeId == registrationRecordTypes.get('Retail_Sell_To') && custReg.Upload_Complete_Resale__c == true){
					system.debug('&&&&&existingdocs true');
					return true;
				}
				else if(custReg.RecordTypeId == registrationRecordTypes.get('Vet_Sell_To') && (custReg.Upload_Complete_Practitioner__c == true || custReg.Upload_Complete_Resale__c == true)) {
					system.debug('&&&&&existingdocs true');
					return true;
				}
				else if(custReg.RecordTypeId == registrationRecordTypes.get('Working_Dog') && custReg.Upload_Complete_Practitioner__c == true){
					return true;
				}
				else {
					system.debug('&&&&&existingdocs false');
					return false;
				}
			}
			else {
				system.debug('&&&&&existingdocs false bc id null');
				return false;
			}
		}
	}

	public boolean renderDelete {
		get {
			if(custReg != null && (custReg.Application_Status__c != 'Submitted' || custReg.Application_Status__c != 'Approved')) {
				return true;
			} else {
				return false;
			}
		}
	}

	// constructor
	public AttachmentUploadController()
	{
		system.debug('^^^^^constructor url: ' + ApexPages.currentPage());
		system.debug('^^^^^constructor appId param|' + ApexPages.currentPage().getParameters().get('applicationid') + '|');
		try {
            List<RecordType> rtypes = [select developerName, id from RecordType where sObjectType = 'Customer_Registration__c'];
            for (RecordType recType : rtypes) {
                registrationRecordTypes.put(recType.developerName, recType.Id);
            }
        } catch (Exception ex) {}

        attDescriptions.put('TaxExempt', 'Please upload your Sales tax exemption certificate(s) and/or Federal 501(C)(3) document and save to attach your file to your application. This does not apply if you are in the state of Oregon, New Hampshire, Alaska, Montana, or Delaware. This also does not need to be completed if you will pay Royal Canin sales tax. If you attach a file, please make sure you have entered the information in the tax exempt fields on the previous page.');
        attDescriptions.put('License', 'Please upload your Practitioner/Licensee\'s DVM License and save to attach your file to your application.');
        attDescriptions.put('ResaleCert', 'Please upload your State Resale Certificate or completed copy of the Sales and Use Certificate, available below for download. Please save and attach your file to your application. This does not apply if you are in the state of Oregon, New Hampshire, Alaska, Montana, or Delaware. This also does not need to be completed if you will pay Royal Canin sales tax. If you attach a file, please make sure you have entered the information in the tax exempt fields on the previous page.');

		if (ApexPages.currentPage().getParameters().get('applicationid') != null) {
            appId = ApexPages.currentPage().getParameters().get('applicationid');
            system.debug('%%%%%appId in constructor:' + appId);

            setSobjId();

            newAttachments = new List<Attachment>();

            system.debug('@@@@@reg:' + custReg);

			if(custReg.RecordTypeId == registrationRecordTypes.get('Veterinarian')
				|| custReg.RecordTypeId == registrationRecordTypes.get('Vet_Sell_To')){
                //if(custReg.Upload_Complete_Practitioner__c == false){
                    system.debug('@@@@@1');
                    newAttachments.Add(new Attachment(Description=attDescriptions.get('License')));
                //}
                //if(custReg.Upload_Complete_Resale__c == false) {
                    newAttachments.Add(new Attachment(Description=attDescriptions.get('ResaleCert')));
                    system.debug('@@@@@2');
                //}
                system.debug('^^^^^Payment_Options__c:' + custReg.Payment_Options__c);
                if(custReg.Payment_Options__c == 'ACH Bank Debit' && custReg.RecordTypeId == registrationRecordTypes.get('Veterinarian')){
                    renderACH = true;
                }
                renderSalesAndUseTaxForm = true;
            }
            else if(custReg.RecordTypeId == registrationRecordTypes.get('Shelter') ){
                newAttachments.Add(new Attachment(Description=attDescriptions.get('TaxExempt')));
                system.debug('@@@@@3');
            }
            else if(custReg.RecordTypeId == registrationRecordTypes.get('Retailer')
            		|| custReg.RecordTypeId == registrationRecordTypes.get('Retail_Sell_To')){
                //if(custReg.Upload_Complete_Resale__c == false){
                    newAttachments.Add(new Attachment(Description=attDescriptions.get('ResaleCert')));
                //}
                system.debug('^^^^^Payment_Options__c:' + custReg.Payment_Options__c);
                if(custReg.Payment_Options__c == 'ACH Bank Debit' && custReg.RecordTypeId == registrationRecordTypes.get('Retailer')){
                    renderACH = true;
                }
                system.debug('@@@@@4');
                renderSalesAndUseTaxForm = true;
            }
            else if(custReg.RecordTypeId == registrationRecordTypes.get('Working_Dog')){
            	  newAttachments.Add(new Attachment(Description=attDescriptions.get('ResaleCert')));
            	  renderSalesAndUseTaxForm = true;
            }
        }
		
		try {
            ACHLink = '/servlet/servlet.FileDownload?file=' + [select id, url from Document where developerName = 'ACH_FORM'].id;
            SalesAndUseTaxForm = '/servlet/servlet.FileDownload?file=' + [select id, url from Document where developerName = 'UNIFORM_SALES_USE_TAX_CERTIFICATE_MULTIJURISDICTION'].id;
        } catch (Exception ex) {}
	}
	
	public void setSobjId(){
		system.debug('%%%%%appId:' + appId);
		if (appId!=null){
			try{
    			custReg = [select id, customer_type__c, recordType.developername, Upload_Complete_Practitioner__c, Upload_Complete_Resale__c, Upload_Complete_Tax__c, Payment_Options__c, Application_Status__c
						   from Customer_Registration__c 
						   where application_id__c = :appId];
    			sobjId = custReg.id;
   			}
   			catch(Exception ex){

   			}
    	}
    	system.debug('%%%%%sobjId:' + sobjId);
	}
	
    public List<Attachment> getAttachments()
    {
    	setSobjId();
  		//if (null==attachments)
	   //	{
	   		attachments=[select Id, ParentId, Name, Description from Attachment where parentId=:sobjId];
	   //	}
    	return attachments;
    }
	
	public void deleteAttachment(){
		system.debug('^^^^^selectedAttachmentId'+attachToDeleteId);
		Attachment att = [select id, Description from Attachment where id=:attachToDeleteId];

		if(att.Description == attDescriptions.get('TaxExempt')){
			custReg.Upload_Complete_Tax__c = false;
		}
		else if(att.Description == attDescriptions.get('License')){
			custReg.Upload_Complete_Practitioner__c = false;
		}
		else if(att.Description == attDescriptions.get('ResaleCert')){
			custReg.Upload_Complete_Resale__c = false;
		}

        delete att;
        update custReg;

        
		getAttachments();
		
			}
	
	public PageReference save()
	{
		setSobjId();
		system.debug('parentID'+sobjId);
		List<Attachment> toInsert=new List<Attachment>();
		
		for (Attachment newAtt : newAttachments)
		{
			if (newAtt.Body!=null)
			{
				if (newAtt.Description == attDescriptions.get('License')) {
					custReg.Upload_Complete_Practitioner__c = true;
				}
				else if (newAtt.Description == attDescriptions.get('ResaleCert')) {
					custReg.Upload_Complete_Resale__c = true;
				}
				else if (newAtt.Description == attDescriptions.get('TaxExempt')) {
					custReg.Upload_Complete_Tax__c = true;
				}
				
				newAtt.parentId=sobjId;
				toInsert.add(newAtt);
			}
		}

		insert toInsert;
		update custReg;

		setSobjId();

		newAttachments.clear();
		newAttachments.add(new Attachment());
		
		return ApexPages.currentPage().setRedirect(false);
	}

	public PageReference prevPage(){
		newAttachments.clear();
		newAttachments.add(new Attachment());
	 	return myctrl.prev_pr();
	 }

	 public PageReference submitApp(){
		newAttachments.clear();
		newAttachments.add(new Attachment());
		myctrl.submit_pr();
		PageReference pageRef = new PageReference(ApexPages.currentPage().getUrl());
        pageRef.setRedirect(true);
        return pageRef;
	 	//return ApexPages.currentPage().setRedirect(false);
	 }

	 public PageReference saveApp(){
	 	return myctrl.save_pr();
	 }



	/******************************************************
	 *
	 * Unit Tests
	 *
	 ******************************************************/
	 
	/*private static testMethod void testController()
	{
		Account acc=new Account(Name='Unit Test');
		insert acc;
		AttachmentUploadController controller=new AttachmentUploadController();
		controller.sobjId=acc.id;
		
		System.assertEquals(0, controller.getAttachments().size());
		
		System.assertEquals(1, controller.newAttachments.size());

		
		System.assertEquals(1 + NUM_ATTACHMENTS_TO_ADD, controller.newAttachments.size());
		
		// populate the first and third new attachments
		List<Attachment> newAtts=controller.newAttachments;
		newAtts[0].Name='Unit Test 1';
		newAtts[0].Description='Unit Test 1';
		newAtts[0].Body=Blob.valueOf('Unit Test 1');

		newAtts[2].Name='Unit Test 2';
		newAtts[2].Description='Unit Test 2';
		newAtts[2].Body=Blob.valueOf('Unit Test 2');
		
		controller.save();
		
		System.assertEquals(2, controller.getAttachments().size());
		System.assertNotEquals(null, controller.done());
	}*/
}