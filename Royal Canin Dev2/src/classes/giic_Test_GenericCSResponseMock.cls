/************************************************************************************
Version : 1.0
Name : giic_Test_GenericCSResponseMock 
Created Date : 7 Oct 2018
Function : create mock responce for all the http callouts in Cybersource CC Service
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/
@isTest
global class giic_Test_GenericCSResponseMock implements HttpCalloutMock{
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        // Create a fake response
        Boolean AuthValidateService = req.getBody().contains('payerAuthValidateService') ? true : false;
        Boolean paySubscriptionCreateService = req.getBody().contains('paySubscriptionCreateService') ? true : false;
        Boolean paySubscriptionUpdateService = req.getBody().contains('paySubscriptionUpdateService') ? true : false;
        Boolean paySubscriptionDeleteService = req.getBody().contains('paySubscriptionDeleteService') ? true : false;
        Boolean ccAuthService = req.getBody().contains('ccAuthService') ? true : false;
        Boolean ccAuthReversalService = req.getBody().contains('ccAuthReversalService') ? true : false;
        Boolean ccCaptureService = req.getBody().contains('ccCaptureService') ? true : false; //Capture Payment
        Boolean ccCreditServiceRefund = req.getBody().contains('refundReason') ? true : false; //Refund
        Boolean ccCreditService = req.getBody().contains('ccCreditService')  ? true : false; //Settlement
        
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'text/xml; charset=utf-8');
        
        if(AuthValidateService){
            res.setBody('<?xml version="1.0" encoding="utf-8"?>'+
                        '<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">'+
                        '<soap:Header>'+
                        '<wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">'+
                        '<wsu:Timestamp xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" wsu:Id="Timestamp-352823978">'+
                        '	<wsu:Created>2018-10-07T14:28:10.526Z</wsu:Created>'+
                        '</wsu:Timestamp>'+
                        '</wsse:Security>'+
                        '</soap:Header>'+
                        '<soap:Body>'+
                        '<c:replyMessage xmlns:c="urn:schemas-cybersource-com:transaction-data-1.149">'+
                        '	<c:merchantReferenceCode>1234</c:merchantReferenceCode>'+
                        '	<c:requestID>5389224898746455204011</c:requestID>'+
                        '	<c:decision>ACCEPT</c:decision>'+
                        '	<c:reasonCode>100</c:reasonCode>'+
                        '	<c:requestToken>AhjzbwSTJCaFBBKY+KCrEQFLjFNVMg0gBAsQyaSZejFdwHWAmAAAUQv0</c:requestToken>'+
                        '	<c:payerAuthValidateReply>'+
                        '		<c:reasonCode>100</c:reasonCode>'+
                        '		<c:commerceIndicator>internet</c:commerceIndicator>'+
                        '		<c:eci>07</c:eci>'+
                        '	</c:payerAuthValidateReply>'+
                        '</c:replyMessage>'+
                        '</soap:Body>'+
                        '</soap:Envelope>');
        }
        
        else if(paySubscriptionCreateService){
            res.setBody('<?xml version="1.0" encoding="utf-8"?>'+
                        '<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">'+
                        '	<soap:Header>'+
                        '		<wsse:Security	xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">'+
                        '			<wsu:Timestamp xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" wsu:Id="Timestamp-696266068">'+
                        '				<wsu:Created>2018-10-10T11:48:11.707Z</wsu:Created>'+
                        '			</wsu:Timestamp>'+
                        '		</wsse:Security>'+
                        '	</soap:Header>'+
                        '	<soap:Body>'+
                        '		<c:replyMessage	xmlns:c="urn:schemas-cybersource-com:transaction-data-1.149">'+
                        '			<c:merchantReferenceCode>1234</c:merchantReferenceCode>'+
                        '			<c:requestID>5391720915086568504012</c:requestID>'+
                        '			<c:decision>ACCEPT</c:decision>'+
                        '			<c:reasonCode>100</c:reasonCode>'+
                        '			<c:requestToken>Ahj/7wSTJEkopbWi/rrMESDdi3aMGTRrFm14cGS1npb62U5IYClvrZTkh6QAgW4ZNJMvRiu4DrgTkyRJKKW1ov66zAAA+gM2</c:requestToken>'+
                        '			<c:purchaseTotals>'+
                        '				<c:currency>USD</c:currency>'+
                        '			</c:purchaseTotals>'+
                        '			<c:ccAuthReply>'+
                        '				<c:reasonCode>100</c:reasonCode>'+
                        '				<c:amount>0.00</c:amount>'+
                        '				<c:authorizationCode>888888</c:authorizationCode>'+
                        '				<c:avsCode>X</c:avsCode>'+
                        '				<c:avsCodeRaw>I1</c:avsCodeRaw>'+
                        '				<c:authorizedDateTime>2018-10-10T11:48:11Z</c:authorizedDateTime>'+
                        '				<c:processorResponse>100</c:processorResponse>'+
                        '				<c:reconciliationID>71740245EMWCAI5O</c:reconciliationID>'+
                        '				<c:paymentNetworkTransactionID>123456789000000</c:paymentNetworkTransactionID>'+
                        '			</c:ccAuthReply>'+
                        '			<c:paySubscriptionCreateReply>'+
                        '				<c:reasonCode>100</c:reasonCode>'+
                        '				<c:subscriptionID>77DF56B70366E57BE05341588E0A2A68</c:subscriptionID>'+
                        '				<c:instrumentIdentifierID>75D867B1215173DEE05341588E0AB429</c:instrumentIdentifierID>'+
                        '				<c:instrumentIdentifierStatus>ACTIVE</c:instrumentIdentifierStatus>'+
                        '				<c:instrumentIdentifierNew>N</c:instrumentIdentifierNew>'+
                        '			</c:paySubscriptionCreateReply>'+
                        '		</c:replyMessage>'+
                        '	</soap:Body>'+
                        '</soap:Envelope>');
        }
        
        else if(paySubscriptionUpdateService){
            res.setBody('<?xml version="1.0" encoding="utf-8"?>'+
                        '<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">'+
                        '	<soap:Header>'+
                        '		<wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">'+
                        '			<wsu:Timestamp xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" wsu:Id="Timestamp-258594362">'+
                        '				<wsu:Created>2018-10-09T12:12:40.108Z</wsu:Created>'+
                        '			</wsu:Timestamp>'+
                        '		</wsse:Security>'+
                        '	</soap:Header>'+
                        '	<soap:Body>'+
                        '		<c:replyMessage xmlns:c="urn:schemas-cybersource-com:transaction-data-1.149">'+
                        '			<c:merchantReferenceCode>1234</c:merchantReferenceCode>'+
                        '			<c:requestID>5390871597896562104010</c:requestID>'+
                        '			<c:decision>ACCEPT</c:decision>'+
                        '			<c:reasonCode>100</c:reasonCode>'+
                        '			<c:requestToken>AhizbwSTJD1fRDmfB9bKEQFLk8a/sXAQLUMmkmXoxXcB1gJgux05</c:requestToken>'+
                        '			<c:paySubscriptionUpdateReply>'+
                        '				<c:reasonCode>100</c:reasonCode>'+
                        '				<c:subscriptionID>76AF86BDC1CB57ECE05341588E0A395B</c:subscriptionID>'+
                        '				<c:ownerMerchantID>abhishekfujitsu</c:ownerMerchantID>'+
                        '				<c:instrumentIdentifierID>75D867B1215173DEE05341588E0AB429</c:instrumentIdentifierID>'+
                        '				<c:instrumentIdentifierStatus>ACTIVE</c:instrumentIdentifierStatus>'+
                        '				<c:instrumentIdentifierNew>N</c:instrumentIdentifierNew>'+
                        '			</c:paySubscriptionUpdateReply>'+
                        '		</c:replyMessage>'+
                        '	</soap:Body>'+
                        '</soap:Envelope>');
        }
        else if(paySubscriptionDeleteService){
            res.setBody('<?xml version="1.0" encoding="utf-8"?>'+
                        '<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">'+
                        '	<soap:Header>'+
                        '		<wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">'+
                        '			<wsu:Timestamp xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" wsu:Id="Timestamp-1239366979">'+
                        '				<wsu:Created>2018-10-09T12:31:16.864Z</wsu:Created>'+
                        '			</wsu:Timestamp>'+
                        '		</wsse:Security>'+
                        '	</soap:Header>'+
                        '	<soap:Body>'+
                        '		<c:replyMessage xmlns:c="urn:schemas-cybersource-com:transaction-data-1.149">'+
                        '			<c:merchantReferenceCode>1234</c:merchantReferenceCode>'+
                        '			<c:requestID>5390882767166896304008</c:requestID>'+
                        '			<c:decision>ACCEPT</c:decision>'+
                        '			<c:reasonCode>100</c:reasonCode>'+
                        '			<c:requestToken>AhijLwSTJD2G8p4Fs2+IEUuE3/57YBAtQyaSZejFdwHWAAAAZA+k</c:requestToken>'+
                        '			<c:paySubscriptionDeleteReply>'+
                        '				<c:reasonCode>100</c:reasonCode>'+
                        '				<c:subscriptionID>76AF9038E3E8645FE05341588E0A0E50</c:subscriptionID>'+
                        '			</c:paySubscriptionDeleteReply>'+
                        '		</c:replyMessage>'+
                        '	</soap:Body>'+
                        '</soap:Envelope>');
        }
        else if(ccAuthService){
            res.setBody('<?xml version="1.0" encoding="utf-8"?>'+
                        '<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">'+
                        '	<soap:Header>'+
                        '		<wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">'+
                        '			<wsu:Timestamp	xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" wsu:Id="Timestamp-1969600949">'+
                        '				<wsu:Created>2018-10-09T12:51:24.720Z</wsu:Created>'+
                        '			</wsu:Timestamp>'+
                        '		</wsse:Security>'+
                        '	</soap:Header>'+
                        '	<soap:Body>'+
                        '		<c:replyMessage xmlns:c="urn:schemas-cybersource-com:transaction-data-1.149">'+
                        '			<c:merchantReferenceCode>1234</c:merchantReferenceCode>'+
                        '			<c:requestID>5390894846106236604010</c:requestID>'+
                        '			<c:decision>ACCEPT</c:decision>'+
                        '			<c:reasonCode>100</c:reasonCode>'+
                        '			<c:requestToken>Ahj/7wSTJD2x3Fk+V85qESDdi3aMGTJnFm14cFvSkJb62UpUgClvrZSlSaQP6mEC1DJpJl6MV3AdcCcmSHtjuLJ8r5zUAAAA7QP8</c:requestToken>'+
                        '			<c:purchaseTotals>'+
                        '				<c:currency>USD</c:currency>'+
                        '			</c:purchaseTotals>'+
                        '			<c:ccAuthReply>'+
                        '				<c:reasonCode>100</c:reasonCode>'+
                        '				<c:amount>300.00</c:amount>'+
                        '				<c:authorizationCode>888888</c:authorizationCode>'+
                        '				<c:avsCode>X</c:avsCode>'+
                        '			<c:avsCodeRaw>I1</c:avsCodeRaw>'+
                        '				<c:authorizedDateTime>2018-10-09T12:51:24Z</c:authorizedDateTime>'+
                        '				<c:processorResponse>100</c:processorResponse>'+
                        '				<c:reconciliationID>71740223EMWCA7RH</c:reconciliationID>'+
                        '			</c:ccAuthReply>'+
                        '		</c:replyMessage>'+
                        '	</soap:Body>'+
                        '</soap:Envelope>');
        }
        
        else if(ccAuthReversalService){
            res.setBody('<?xml version="1.0" encoding="utf-8"?>'+
                        '<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">'+
                        '<soap:Header>'+
                        '<wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">'+
                        '<wsu:Timestamp xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" wsu:Id="Timestamp-1403201629">'+
                        '<wsu:Created>2018-10-07T10:49:45.239Z</wsu:Created>'+
                        '</wsu:Timestamp>'+
                        '</wsse:Security>'+
                        '</soap:Header>'+
                        '<soap:Body>'+
                        '<c:replyMessage xmlns:c="urn:schemas-cybersource-com:transaction-data-1.149">'+
                        '<c:merchantReferenceCode>1234</c:merchantReferenceCode>'+
                        '<c:requestID>5389093851166427304008</c:requestID>'+
                        '<c:decision>ACCEPT</c:decision>'+
                        '<c:reasonCode>100</c:reasonCode>'+
                        '<c:requestToken>Ahj//wSTJCSzcP4wu9BIESDdizYuWDVvFm14bibRcJb62SZcQClxEzT5B6QM2sgWIZNJMvRiu4DrFAnJkhJZOed2w7CmAAAArwaP</c:requestToken>'+
                        '<c:purchaseTotals>'+
                        '<c:currency>USD</c:currency>'+
                        '</c:purchaseTotals>'+
                        '<c:ccAuthReversalReply>'+
                        '<c:reasonCode>100</c:reasonCode>'+
                        '<c:amount>35.00</c:amount>'+
                        '<c:processorResponse>100</c:processorResponse>'+
                        '<c:requestDateTime>2018-10-07T10:49:45Z</c:requestDateTime>'+
                        '</c:ccAuthReversalReply>'+
                        '</c:replyMessage>'+
                        '</soap:Body>'+
                        '</soap:Envelope>');
        }
        
        else if(ccCaptureService){
            res.setBody('<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">'+
            '   <soap:Header>'+
            '      <wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">'+
             '       <wsu:Timestamp wsu:Id="Timestamp-2094384131" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">'+
              '          <wsu:Created>2018-09-10T12:22:16.421Z</wsu:Created>'+
               '      </wsu:Timestamp>'+
                '  </wsse:Security>'+
            '   </soap:Header>'+
             '  <soap:Body>'+
              '    <c:replyMessage xmlns:c="urn:schemas-cybersource-com:transaction-data-1.149">'+
               '      <c:merchantReferenceCode>1234</c:merchantReferenceCode>'+
                '     <c:requestID>5365821361246326903007</c:requestID>'+
                 '    <c:decision>ACCEPT</c:decision>'+
                  '   <c:reasonCode>100</c:reasonCode>'+
                   '  <c:requestToken>Ahj//wSTIuG68raqSlDfESDdizatGrdxHmT7UaLHopcJja4OwClwmNrhDaQN2AQHsMmkmXoxXcB1hgTkyLhkszxAjAgBAAAA/RBr</c:requestToken>'+
                    ' <c:purchaseTotals>'+
                     '   <c:currency>USD</c:currency>'+
                     '</c:purchaseTotals>'+
                     '<c:ccCaptureReply>'+
                      '  <c:reasonCode>100</c:reasonCode>'+
                       ' <c:requestDateTime>2018-09-10T12:22:16Z</c:requestDateTime>'+
                        '<c:amount>69.12</c:amount>'+
                        '<c:reconciliationID>71354578GLOZFEGQ</c:reconciliationID>'+
                     '</c:ccCaptureReply>'+
                  '</c:replyMessage>'+
            '   </soap:Body>'+
            '</soap:Envelope>');
        }
        else if(ccCreditServiceRefund){
            res.setBody('<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">'+
            '   <soap:Header>'+
            '      <wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">'+
            '         <wsu:Timestamp wsu:Id="Timestamp-1886963035" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">'+
             '           <wsu:Created>2018-09-03T10:20:49.752Z</wsu:Created>'+
              '       </wsu:Timestamp>'+
              '    </wsse:Security>'+
               '</soap:Header>'+
            '   <soap:Body>'+
            '      <c:replyMessage xmlns:c="urn:schemas-cybersource-com:transaction-data-1.149">'+
             '        <c:merchantReferenceCode>1234</c:merchantReferenceCode>'+
              '       <c:requestID>5359700493906377203010</c:requestID>'+
               '      <c:decision>ACCEPT</c:decision>'+
                '     <c:reasonCode>100</c:reasonCode>'+
                 '    <c:requestToken>Ahj/7wSTIozJQkwvTF1CESDdy5aMmDlpImQ3EyrSspcYZeGvoClxhl4a+6QN2AQG0MmkmXoxXcB1gJyZFGZKEmF6YuoQoghJ</c:requestToken>'+
                  '   <c:purchaseTotals>'+
                   '     <c:currency>USD</c:currency>'+
                    ' </c:purchaseTotals>'+
                     '<c:ccCreditReply>'+
                      '  <c:reasonCode>100</c:reasonCode>'+
                       ' <c:requestDateTime>2018-09-03T10:20:49Z</c:requestDateTime>'+
                        '<c:amount>69.12</c:amount>'+
                        '<c:reconciliationID>79942094HLC8LURY</c:reconciliationID>'+
                     '</c:ccCreditReply>'+
            '      </c:replyMessage>'+
            '   </soap:Body>'+
            '</soap:Envelope>');
        }
        else{
            System.debug('ccCreditService'+ccCreditService);
            res.setBody('<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">'+
            '<soap:Header>'+
            '<wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">'+
            '<wsu:Timestamp wsu:Id="Timestamp-472433179" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">'+
            '<wsu:Created>2018-09-10T12:26:02.828Z</wsu:Created>'+
            '</wsu:Timestamp>'+
            '</wsse:Security>'+
            '</soap:Header>'+
            '<soap:Body>'+
            '<c:replyMessage xmlns:c="urn:schemas-cybersource-com:transaction-data-1.149">'+
            '<c:merchantReferenceCode>1234</c:merchantReferenceCode>'+
            '<c:requestID>5365823625726546903012</c:requestID>'+
            '<c:decision>ACCEPT</c:decision>'+
            '<c:reasonCode>100</c:reasonCode>'+
            '<c:requestToken>Ahj/7wSTIuHC/j8NZ3/kESDdi0Yt2bJzKmUGkmtGmJcJja4OwClydbPML6QN2AQHsMmkmXoxXcB1wJyZFwyWZ4gRgQAgMDTU</c:requestToken>'+
            '<c:purchaseTotals>'+
            '<c:currency>USD</c:currency>'+
            '</c:purchaseTotals>'+
            '<c:ccCreditReply>'+
            '<c:reasonCode>100</c:reasonCode>'+
            '<c:requestDateTime>2018-09-10T12:26:02Z</c:requestDateTime>'+
            '<c:amount>69.12</c:amount>'+
            '<c:reconciliationID>71417329JLP4IVFL</c:reconciliationID>'+
            '</c:ccCreditReply>'+
            '</c:replyMessage>'+
            '</soap:Body>'+
            '</soap:Envelope>');
        }
                    
        res.setStatusCode(200);
        return res;
        
    }
    
}