/*
 * Date:       		 - Developer, Company					       - Description
 * 2015-08-04		 - Joseph Wortman, Acumen Solutions			   - Created
 */
@isTest
private class FiscalYearTriggerHelperTest {
	
	@isTest static void test_method_one() {
		List<Fiscal_Year__c> testList = [SELECT Id FROM Fiscal_Year__c WHERE IsActive__c = true];
		System.debug('Before 1: Number of fiscal years that are active: ' + testList.size());



		Fiscal_Year__c testFiscalYear = new Fiscal_Year__c();
		testFiscalYear.Name = '2020';
		testFiscalYear.IsActive__c = true;
		testFiscalYear.Start_Date__c = date.parse('1/1/2020');
		testFiscalYear.End_Date__c = date.parse('12/31/2020');

		Test.startTest();
		insert testFiscalYear;

		testList = [SELECT Id FROM Fiscal_Year__c WHERE IsActive__c = true];
		System.debug('After 1: Number of fiscal years that are active: ' + testList.size());

		testList = [SELECT Id, Activation_Date__c, Start_Date__c FROM Fiscal_Year__c WHERE Id = :testFiscalYear.Id];

		System.assertEquals(testList[0].Activation_Date__c, testList[0].Start_Date__c);

		Test.stopTest();	
	}
	
	@isTest static void test_method_two() {
		List<Fiscal_Year__c> testList = [SELECT Id FROM Fiscal_Year__c WHERE IsActive__c = true];
		System.debug('Before 2: Number of fiscal years that are active: ' + testList.size());

		Fiscal_Year__c testFiscalYear = new Fiscal_Year__c();
		testFiscalYear.Name = '2020';
		testFiscalYear.IsActive__c = false;
		testFiscalYear.Start_Date__c = date.parse('1/1/2020');
		testFiscalYear.End_Date__c = date.parse('12/31/2020');

		insert testFiscalYear;

		Fiscal_Year__c testFiscalYear2 = new Fiscal_Year__c();
		testFiscalYear2.Name = '2021';
		testFiscalYear2.IsActive__c = true;
		testFiscalYear2.Start_Date__c = date.parse('1/1/2021');
		testFiscalYear2.End_Date__c = date.parse('12/31/2021');	

		try {
			Test.startTest();
			insert testFiscalYear2;

			testFiscalYear.IsActive__c = true;
			update testFiscalYear;

			testList = [SELECT Id FROM Fiscal_Year__c WHERE IsActive__c = true];
			System.debug('After 2: Number of fiscal years that are active: ' + testList.size());
			Test.stopTest();
		} catch (Exception e) {
			System.Assert(e.getMessage().contains('You cannot have more than one active Fical Year. Please uncheck isActive Flag for an existing Fiscal Year'));
		}
	}
	
}