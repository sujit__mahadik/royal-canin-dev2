/************************************************************************************
Version : 1.0
Created Date : 20 Aug 2018
Function : This class is used for the manual allocation from the sales order lines detail page button
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/
public class giic_SOLAllocation {
    public gii__salesorderline__c objSOL;
    public giic_SOLAllocation(ApexPages.StandardController stdController) {
        this.objSOL = (gii__salesorderline__c)stdController.getRecord();   
    }
    	/*
    * Method name : updateAllocation
    * Description : process the sales order line for allocation 
    * Return Type : 
    * Parameter : 
    */
    public void updateAllocation(){
        
    }
}