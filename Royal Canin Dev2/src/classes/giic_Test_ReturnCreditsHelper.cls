@isTest(seeAllData = false)
private class giic_Test_ReturnCreditsHelper {
    public static gii__InvoiceAdditionalCharge__c oIAC;
    public static giic_InvoicedProductWrapper objIPW;
    public static List<gii__OrderInvoiceDetail__c> lstInvDet;
    public static  gii__AdditionalCharge__c oAC;
    
    @testSetup
    static void testDataCreation() {
        //create user
        giic_Test_DataCreationUtility.CreateAdminUser();
        
        // insert default warehouse
        List<gii__Warehouse__c> defaultWHList = giic_Test_DataCreationUtility.insertWarehouse();        
        // create system policy
        giic_Test_DataCreationUtility.testSystemPolicyCreation2();        
        SetupTestData.createCustomSettings();
        // insert 5 warehouse ie. in bluk
        List<gii__Warehouse__c> testWareListN = giic_Test_DataCreationUtility.insertWarehouse_N(5);
        
        List<Account> testConsumerAccountList = giic_Test_DataCreationUtility.insertConsumerAccount_N(5);
        
      //  System.debug('account list' + testConsumerAccountList);
        
        	
        List<gii__Product2Add__c> testProductList = giic_Test_DataCreationUtility.insertProduct_N(); 
        giic_Test_DataCreationUtility.insertUnitofMeasure(); 
       
        // code to insert Product UoM
        giic_Test_DataCreationUtility.productUnitofMeasureCreation();
        system.debug('\n--lstProductUnitofMeasure--'+giic_Test_DataCreationUtility.lstProductUnitofMeasure);
        
        
        giic_Test_DataCreationUtility.insertProductInventoryQTYDetails_N(5);
        
        List<gii__SalesOrder__c> testSOList1 = giic_Test_DataCreationUtility.insertSalesOrder_N(1);
        system.debug('testSOList1--->>>>>' + testSOList1);
        // create SOL
        giic_Test_DataCreationUtility.insertSOLine_N();
        String sAction = 'Inventory Reserve';
        map<Id, gii__SalesOrderLine__c> mapSOL = new map<Id, gii__SalesOrderLine__c>();
        
        oAC = new gii__AdditionalCharge__c();
        oAC.Name = 'Shipping Charge';
        oAC.gii__UnitPrice__c = 1.00;
        insert  oAC;
        
        gii__SalesOrderAdditionalCharge__c oSOAC = new gii__SalesOrderAdditionalCharge__c();
        oSOAC.gii__SalesOrder__c = giic_Test_DataCreationUtility.lstSalesOrder_N[0].Id;
        oSOAC.gii__AdditionalCharge__c = oAC.Id;
        oSOAC.gii__Quantity__c  = 1.00;
        oSOAC.gii__UnitPrice__c = 1.00;
        oSOAC.gii__ForwardToInvoice__c  = true;
        oSOAC.gii__Invoiced__c  = true;
        Insert oSOAC;
        
        
        
        map<id,Double> mapSOLQuantity = new map<id,Double> ();
        
        for(gii__SalesOrderLine__c objSOL : giic_Test_DataCreationUtility.lstSalesOrderLine_N ){
            mapSOL.put(objSOL.Id, objSOL);
            mapSOLQuantity.put(objSOL.Id, objSOL.gii__OrderQuantity__c);
        }
        
        
        // 1. Create Inventory reserve
        giic_Test_DataCreationUtility.createInventoryReservation(mapSOLQuantity, false, sAction);
        
        List<gii__InventoryReserve__c> lstIR1 = [Select Id,gii__ReserveQuantity__c, Name,gii__SalesOrder__c, gii__SalesOrder__r.Name, gii__SalesOrderLine__c, gii__SalesOrderLine__r.Name, gii__ProductInventoryQuantityDetail__c From gii__InventoryReserve__c Where gii__SalesOrderLine__c IN : mapSOLQuantity.keySet()];


        // 2. now create Shipment 
        List<Id> lstIRIds = new List<Id>();
        Date shipmentDate = system.today();
        string shippedDateString = string.valueOf(shipmentDate);
        // String shippedDateString = system.today().year() + '-' + system.today().month() + '-' + system.today().day() ;   
        
        Map<String, List<Id>> mapIReservsQuickShip = new Map<String, List<Id>> ();
        List<gii__InventoryReserve__c> lstIR = [select id, name from gii__InventoryReserve__c where gii__SalesOrderLine__c in : mapSOLQuantity.keySet()];
       // system.debug('lstIR------->>>>>>>>>> ' + lstIR);
        System.assertEquals(lstIR.size()>0 , true);
        if(lstIR != null && lstIR.size() > 0 ){
            for(gii__InventoryReserve__c objIR : lstIR ){
                lstIRIds.add(objIR.Id);
            }
            
            mapIReservsQuickShip.put(shippedDateString,lstIRIds );
        }
        system.assertEquals(mapIReservsQuickShip.size()>0, true);
       // system.debug('mapIReservsQuickShip------->>>>>> ' + mapIReservsQuickShip);
        // giic_Test_DataCreationUtility.createQuickShip(mapIReservsQuickShip); 
       // giic_Test_DataCreationUtility.insertWarehouseShipmentAdviceStaging(giic_Test_DataCreationUtility.lstSalesOrder_N);
       
        System.debug('fulfillment record count:::' + [select id from gii__WarehouseShippingOrderStaging__c]);
        gii__Shipment__c shipmentObj = new gii__Shipment__c();  
        shipmentObj.gii__Account__c = testConsumerAccountList[0].Id ;
        shipmentObj.gii__SalesOrder__c = lstIR1[0].gii__SalesOrder__c;
        shipmentObj.gii__ForwardToInvoice__c =  true;      
        database.insert(shipmentObj);
        //Shipment Detail Data Creation
        gii__ShipmentDetail__c shipmentDetailObj = new gii__ShipmentDetail__c();
        shipmentDetailObj.gii__Account__c = testConsumerAccountList[0].Id ;       
        shipmentDetailObj.gii__SalesOrder__c = lstIR1[0].gii__SalesOrder__c;
        shipmentDetailObj.gii__ShippedQuantity__c = 2;
        shipmentDetailObj.gii__SalesOrderLine__c = lstIR1[0].gii__SalesOrderLine__c;
        shipmentDetailObj.gii__Shipment__c = shipmentObj .Id;
        Database.insert(shipmentDetailObj);
        
        
        
        // List<gii__Shipment__c> lstShipment = [Select Id, gii__SalesOrder__c From gii__Shipment__c where gii__ShippedDate__c = :shipmentDate  and gii__SalesOrder__r.Name = :testSOList1[0].Name ];
        List<gii__Shipment__c> lstShipment = [Select Id, gii__SalesOrder__c From gii__Shipment__c  ];
        
        system.assertEquals(lstShipment.size()>0, true);
        //system.debug(' lstShipment ------->>>>>>>>>> ' + lstShipment);
        List<Id> lstShipmentId = new List<Id>();
        
        if(lstShipment != null && lstShipment.size() > 0 ){
            for(gii__Shipment__c objSM : lstShipment ){
                lstShipmentId.add(objSM.id);
            }
        }
        // 3. create ForwardToInvoice
        if(lstShipmentId != null && lstShipmentId.size() > 0 ){
            giic_Test_DataCreationUtility.createForwardToInvoice(lstShipmentId);
        }
        
       giic_Test_DataCreationUtility.CreateAdminUser();
        
    }

    
    private static testMethod void test_MultiFuns() {
        User test_Admin_user=[select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        
        giic_Test_DataCreationUtility.lstProdRef_N = [SELECT Id, gii__LotControlled__c, gii__ProductReference__c, gii__DefaultWarehouse__c, gii__ProductReference__r.Name, gii__NonStock__c, gii__ProductReference__r.ProductCode, gii__ProductReference__r.Family, gii__ProductReference__r.Product_Category__c, gii__ProductReference__r.SKU__c, gii__ProductReference__r.Food_Type__c, gii__ProductReference__r.Weight_Unit__c, gii__ProductReference__r.Weight__c from gii__Product2Add__c];
        
        giic_Test_DataCreationUtility.lstAccount_N =  [select Name, BillingCountry , BillingPostalCode, BillingState , BillingCity , BillingStreet ,  ShippingCountry , ShippingPostalCode , ShippingState , ShippingCity , ShippingStreet , ClinicCustomerNo__c , ShipToCustomerNo__c , giic_WarehouseDistanceMapping__c from Account];
        giic_Test_DataCreationUtility.lstUnitofMeasure = [ select id, Name  , gii__Description__c , gii__DisplayName__c  ,  gii__UniqueId__c from gii__UnitofMeasure__c];
        
        giic_Test_DataCreationUtility.lstSalesOrder_N = [select gii__Account__c , giic_CustomerPriority__c, gii__ScheduledDate__c , gii__OrderDate__c, gii__Status__c , giic_SOStatus__c,gii__Warehouse__c , giic_OrderNo__c, giic_TaxCal__c, gii__Carrier__c, giic_NewPickDate__c, gii__Released__c, giic_AddressValidated__c, giic_CCAuthorized__c, giic_PromoCal__c, giic_ShipFeeCal__c from gii__SalesOrder__c limit 1];
        // giic_Test_DataCreationUtility.insertUnitofMeasure();
        List<gii__SalesOrderPayment__c> lstSOP = new List<gii__SalesOrderPayment__c>();
        for(gii__SalesOrder__c oSO : giic_Test_DataCreationUtility.lstSalesOrder_N ){
            gii__SalesOrderPayment__c oSOP = new gii__SalesOrderPayment__c();
            oSOP.gii__SalesOrder__c  = oSO.Id;
            lstSOP.add(oSOP);
        }
        if(lstSOP != null && lstSOP.size() > 0){
            insert lstSOP;
        }
        
        
        gii__SalesOrderAdditionalCharge__c oSOAC = [select gii__SalesOrder__c, gii__AdditionalCharge__c, gii__Quantity__c, gii__UnitPrice__c from gii__SalesOrderAdditionalCharge__c limit 1];
        
        gii__AdditionalCharge__c oAC = [select id, Name, gii__UnitPrice__c from gii__AdditionalCharge__c limit 1];
        System.runAs(test_Admin_user){
        Test.startTest();
        
        
        
        // 3rd step ForwardToInvoice is already done. Now proceed for 4th step
        
        List<gii__Shipment__c> lstShipment = [Select Id, gii__SalesOrder__c From gii__Shipment__c  ];
        List<Id> lstShipmentId = new List<Id>();
        
        if(lstShipment != null && lstShipment.size() > 0 ){
            for(gii__Shipment__c objSM : lstShipment ){
                lstShipmentId.add(objSM.id);
            }
        }
        List<Id> lstFTIIds = new List<Id>();
        List<gii__ForwardToInvoiceQueue__c> lstFTI = new List<gii__ForwardToInvoiceQueue__c>();
        lstFTI = [Select Name, Id, gii__SalesOrder__c, gii__SalesOrderLine__c, gii__Shipment__c, gii__ServiceOrderLine__c, gii__SalesOrderAdditionalCharge__c, gii__ServiceOrderCompletion__c From gii__ForwardToInvoiceQueue__c Where gii__Shipment__c in :lstShipmentId]; 
        
        if(lstFTI != null && lstFTI.size() > 0 ){
            for(gii__ForwardToInvoiceQueue__c objFTI : lstFTI ){
                lstFTIIds.add(objFTI.Id);
            }
        }
        
        if(lstFTIIds != null && lstFTIIds.size() > 0 ){
            giic_Test_DataCreationUtility.createInvoice(lstFTIIds);
        }
        
        
        
        string strSKU = giic_Test_DataCreationUtility.lstProdRef_N[0].gii__ProductReference__r.SKU__c;
        List<gii__OrderInvoice__c> lstInv = [select id,name, gii__NetAmount__c, gii__PaymentMethod__c, gii__PaidAmount__c,  giic_RefInvoiceNo__c from gii__OrderInvoice__c];
        for(Integer i=0; i< lstInv.size() ; i++ ){
            lstInv[i].giic_RefInvoiceNo__c = strSKU + string.valueOf(i);
        }
        update lstInv;
        gii__OrderInvoice__c ObjInv = lstInv[0];
        // system.debug('Invoice=== '+ lstInv);
        
         System.assertEquals(lstInv.size()>0, true);
         
        lstInvDet =[SELECT Id, gii__Invoice__c, gii__Invoice__r.name, Name, LastModifiedDate, gii__Invoice__r.gii__Account__c, gii__RemainingCreditQuantity__c, gii__Invoice__r.gii__InvoiceCreationCompleted__c, gii__Invoice__r.giic_RefInvoiceNo__c,gii__Invoice__r.gii__SalesOrder__c, gii__Invoice__r.gii__SalesOrder__r.Name, gii__ProductReference__r.gii__ProductReference__c, gii__ProductReference__r.gii__ProductReference__r.SKU__c FROM gii__OrderInvoiceDetail__c ];
        
        
        
        oIAC = new gii__InvoiceAdditionalCharge__c();
        oIAC.gii__AdditionalCharge__c = oAC.Id;
        oIAC.gii__SalesOrder__c = giic_Test_DataCreationUtility.lstSalesOrder_N[0].Id;
        oIAC.gii__SalesOrderAdditionalCharge__c = oSOAC.Id;
        oIAC.gii__Invoice__c = ObjInv.Id;
        oIAC.gii__Quantity__c = 1.00;
        oIAC.gii__UnitCost__c = 1.00;
        oIAC.gii__UnitPrice__c = 1.00;      
        insert oIAC;
        
        
        giic_CreditCardServiceHandler.createArInvPayments(ObjInv);
        
        Case cas = new Case(AccountId = giic_Test_DataCreationUtility.lstAccount_N[0].id, Priority = 'Medium', Origin = 'Email', Approval_Status__c ='Pending', Status = 'Draft');
        insert cas;
        string sSKU = giic_Test_DataCreationUtility.lstProdRef_N[0].gii__ProductReference__r.SKU__c;

        Object prodObj = giic_ReturnCreditsHelper.getProductList(cas.id,sSKU).get('productList');
        
        giic_ReturnCreditsHelper.getInvoiceAdditionalCharge(ObjInv.id);
        //giic_ReturnCreditsHelper.submitforApproval(cas.id);
        giic_ReturnCreditsHelper.createAR(cas.id);
       
        Set<String> rejectedCases  = new Set<String>();
        rejectedCases.add(cas.id);
        giic_ReturnCreditsHelper.deleteARCredit(rejectedCases);
        String recordId =cas.id;
        System.debug('UOM Detail' + giic_Test_DataCreationUtility.lstUnitofMeasure);
        List<giic_InvoicedProductWrapper> lstIPW = new List<giic_InvoicedProductWrapper>();
            giic_InvoicedProductWrapper wrapperinvoice = new giic_InvoicedProductWrapper();
            wrapperinvoice.isSelected=true;
            wrapperinvoice.productId= giic_Test_DataCreationUtility.lstProdRef_N[0].id;
            wrapperinvoice.productSKU='sjlsj';
            wrapperinvoice.productDescription='shsk';
            wrapperinvoice.invoiceId= ObjInv.id;
            wrapperinvoice.invoiceNumber='hkshksh';
            wrapperinvoice.soId= giic_Test_DataCreationUtility.lstSalesOrder_N[0].id;
            wrapperinvoice.soPaymentMethod='sgjsgk';
            wrapperinvoice.soNumber='sssss';
            wrapperinvoice.invoiceDetailId=lstInvDet[0].id;
            wrapperinvoice.orderDate='03-01-2019';
            wrapperinvoice.orderQuantity=10;
            wrapperinvoice.qtyAvailableForReturn=5;
            wrapperinvoice.invoiceAmount=100;
            wrapperinvoice.invoiceAmountStocking=90;
            wrapperinvoice.UOM='EA';
            wrapperinvoice.sellingUOM='ajlaj';
            wrapperinvoice.sellingUOMId=giic_Test_DataCreationUtility.lstUnitofMeasure[0].id;
            wrapperinvoice.stockingUOM='EA';
            wrapperinvoice.stockingUOMId=giic_Test_DataCreationUtility.lstUnitofMeasure[0].id;
            wrapperinvoice.returnQty=20;
            wrapperinvoice.returnQtyOld=50;
            wrapperinvoice.returnReason='Pet Related (RE-PET)';
            wrapperinvoice.returnAmount=100;
            wrapperinvoice.returnRewardAmount=10;
            lstIPW.add(wrapperinvoice);
            
            
            gii__InvoiceAdditionalCharge__c objIAC= new gii__InvoiceAdditionalCharge__c();
            objIAC.gii__UnitPrice__c = 1;
            objIAC.gii__UnitCost__c =1;
            objIAC.gii__Quantity__c = 1;
            objIAC.gii__TaxAmount__c = 0;
            objIAC.gii__SalesOrder__c = giic_Test_DataCreationUtility.lstSalesOrder_N[0].id;
            objIAC.gii__Invoice__c = ObjInv.id;
            insert objIAC;
            
            gii__AdditionalCharge__c objAC = new gii__AdditionalCharge__c();
            objAC.gii__UnitPrice__c = 1;
            objAC.gii__UnitCost__c =1;            
            insert objAC;
            
            
            
         List<giic_InvoicedProductWrapper> lstInvAC = new List<giic_InvoicedProductWrapper>();
            giic_InvoicedProductWrapper wrapperinvoice2 = new giic_InvoicedProductWrapper();
            wrapperinvoice2.isSelected=true;
            wrapperinvoice2.invoiceId= ObjInv.id;
            wrapperinvoice2.invoiceNumber=ObjInv.name;
            wrapperinvoice2.soId= giic_Test_DataCreationUtility.lstSalesOrder_N[0].id;
            wrapperinvoice2.additionalChargeAmount = 100;
            wrapperinvoice2.additionalChargeQuantity = 1;
            wrapperinvoice2.additionalChargeInvId = objIAC.id;
            wrapperinvoice2.additionalChargeId = objAC.id;
            lstInvAC.add(wrapperinvoice2);
            
            String strInvACList = JSON.serialize(lstInvAC);
            String strProdList=JSON.serialize(lstIPW);
            String strWrapper =JSON.serialize(wrapperinvoice);
            
            System.assert(strWrapper!=null);
            System.assertEquals(strWrapper.length()>0, strWrapper!='');
            
        giic_ReturnCreditsHelper.getProductwithChangedUOM(strWrapper);
        giic_ReturnCreditsHelper.saveReturnList(recordId,strProdList,strInvACList);
        
        // Creating AR Invoice payment Record
        List<gii__ARInvoicePayment__c> lstARIPayment = new List<gii__ARInvoicePayment__c>();
        gii__ARInvoicePayment__c ARIPayment = new gii__ARInvoicePayment__c();
        ARIPayment.gii__PaidAmount__c = 50;
        ARIPayment.gii__Invoice__c = ObjInv.Id;
        ARIPayment.gii__PaymentMethod__c = giic_Constants.CREDIT_CARD;
        lstARIPayment.add(ARIPayment);
         gii__ARInvoicePayment__c ARIPayment1 = new gii__ARInvoicePayment__c();
        ARIPayment1.gii__PaidAmount__c = 100;
        ARIPayment1.gii__Invoice__c = ObjInv.Id;
        ARIPayment1.gii__PaymentMethod__c = 'ACH';
        lstARIPayment.add(ARIPayment1);
        insert lstARIPayment;
        
        
        // creating ARCreditPayment Record.
        List<gii__ARCredit__c> ARCobj = [Select id from gii__ARCredit__c where gii__Invoice__c =:ObjInv.Id limit 1];
        list<gii__ARCreditPayment__c> lstARCPayment = new list<gii__ARCreditPayment__c>();
        gii__ARCreditPayment__c ARCPayment = new gii__ARCreditPayment__c();
        ARCPayment.gii__PaidAmount__c = 50;
        ARCPayment.gii__ARCredit__c = ARCobj[0].id;
        ARCPayment.gii__PaymentMethod__c = giic_Constants.CREDIT_CARD;
        gii__ARCreditPayment__c ARCPayment1 = new gii__ARCreditPayment__c();
        ARCPayment1.gii__PaidAmount__c = 50;
        ARCPayment1.gii__ARCredit__c = ARCobj[0].id;
        ARCPayment1.gii__PaymentMethod__c = 'ACH';
        lstARCPayment.add(ARCPayment);
        lstARCPayment.add(ARCPayment1);
        insert lstARCPayment;
        
       
        // creating payment methods
         List<gii__PaymentMethod__c> lstPM = new List<gii__PaymentMethod__c>();
                
                gii__PaymentMethod__c objPM1 = new gii__PaymentMethod__c();
                objPM1.Name = giic_Constants.CREDIT_CARD;
                objPM1.giic_CreditSequence__c = 1;
                objPM1.giic_PaymentConsumptionSeq__c = 2;
                lstPM.add(objPM1);
                
                gii__PaymentMethod__c objPM2 = new gii__PaymentMethod__c();
                objPM2.Name = giic_Constants.PAYMENT_REWARD_POINTS;
                objPM2.giic_CreditSequence__c = 5;
                objPM2.giic_PaymentConsumptionSeq__c = 1;
                lstPM.add(objPM2);
                
                gii__PaymentMethod__c objPM3 = new gii__PaymentMethod__c();
                objPM3.Name = 'ACH';
                objPM3.giic_CreditSequence__c = 3;
                objPM3.giic_PaymentConsumptionSeq__c = 3;
                lstPM.add(objPM3);
                insert lstPM;
        cas.Approval_Status__c = giic_Constants.APPROVED_STATUS;
        // Setting up API Mock
        giic_RecursiveTriggerHandler.isFirstTime = false;
        update cas;
        Test.stopTest();
        }
    }
    
    
    private static testMethod void test_RCQueuable() {
        
         giic_Test_DataCreationUtility.lstProdRef_N = [SELECT Id, gii__LotControlled__c, gii__ProductReference__c, gii__DefaultWarehouse__c, gii__ProductReference__r.Name, gii__NonStock__c, gii__ProductReference__r.ProductCode, gii__ProductReference__r.Family, gii__ProductReference__r.Product_Category__c, gii__ProductReference__r.SKU__c, gii__ProductReference__r.Food_Type__c, gii__ProductReference__r.Weight_Unit__c, gii__ProductReference__r.Weight__c from gii__Product2Add__c];
        
        giic_Test_DataCreationUtility.lstAccount_N =  [select Name, BillingCountry , BillingPostalCode, BillingState , BillingCity , BillingStreet ,  ShippingCountry , ShippingPostalCode , ShippingState , ShippingCity , ShippingStreet , ClinicCustomerNo__c , ShipToCustomerNo__c , giic_WarehouseDistanceMapping__c from Account];
        giic_Test_DataCreationUtility.lstUnitofMeasure = [ select id, Name  , gii__Description__c , gii__DisplayName__c  ,  gii__UniqueId__c from gii__UnitofMeasure__c];
        
        giic_Test_DataCreationUtility.lstSalesOrder_N = [select gii__Account__c , giic_CustomerPriority__c, gii__ScheduledDate__c , gii__OrderDate__c, gii__Status__c , giic_SOStatus__c,gii__Warehouse__c , giic_OrderNo__c, giic_TaxCal__c, gii__Carrier__c, giic_NewPickDate__c, gii__Released__c, giic_AddressValidated__c, giic_CCAuthorized__c, giic_PromoCal__c, giic_ShipFeeCal__c from gii__SalesOrder__c limit 1];
        List<gii__SalesOrderPayment__c> lstSOP = new List<gii__SalesOrderPayment__c>();
        for(gii__SalesOrder__c oSO : giic_Test_DataCreationUtility.lstSalesOrder_N ){
            gii__SalesOrderPayment__c oSOP = new gii__SalesOrderPayment__c();
            oSOP.gii__SalesOrder__c  = oSO.Id;
            lstSOP.add(oSOP);
        }
        if(lstSOP != null && lstSOP.size() > 0){
            insert lstSOP;
        }
        User test_Admin_user=[select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        System.runAs(test_Admin_user){
        Test.startTest();
        gii__SalesOrderAdditionalCharge__c oSOAC = [select gii__SalesOrder__c, gii__AdditionalCharge__c, gii__Quantity__c, gii__UnitPrice__c from gii__SalesOrderAdditionalCharge__c limit 1];
        
        gii__AdditionalCharge__c oAC = [select id, Name, gii__UnitPrice__c from gii__AdditionalCharge__c limit 1];
        
        
        
        
        
        // 3rd step ForwardToInvoice is already done. Now proceed for 4th step
        
        List<gii__Shipment__c> lstShipment = [Select Id, gii__SalesOrder__c From gii__Shipment__c  ];
        List<Id> lstShipmentId = new List<Id>();
        
        if(lstShipment != null && lstShipment.size() > 0 ){
            for(gii__Shipment__c objSM : lstShipment ){
                lstShipmentId.add(objSM.id);
            }
        }
        List<Id> lstFTIIds = new List<Id>();
        List<gii__ForwardToInvoiceQueue__c> lstFTI = new List<gii__ForwardToInvoiceQueue__c>();
        lstFTI = [Select Name, Id, gii__SalesOrder__c, gii__SalesOrderLine__c, gii__Shipment__c, gii__ServiceOrderLine__c, gii__SalesOrderAdditionalCharge__c, gii__ServiceOrderCompletion__c From gii__ForwardToInvoiceQueue__c Where gii__Shipment__c in :lstShipmentId]; 
        
        if(lstFTI != null && lstFTI.size() > 0 ){
            for(gii__ForwardToInvoiceQueue__c objFTI : lstFTI ){
                lstFTIIds.add(objFTI.Id);
            }
        }
        
        if(lstFTIIds != null && lstFTIIds.size() > 0 ){
            giic_Test_DataCreationUtility.createInvoice(lstFTIIds);
        }
        
        
        
        string strSKU = giic_Test_DataCreationUtility.lstProdRef_N[0].gii__ProductReference__r.SKU__c;
        List<gii__OrderInvoice__c> lstInv = [select id,name, gii__NetAmount__c, gii__PaymentMethod__c, gii__PaidAmount__c,  giic_RefInvoiceNo__c from gii__OrderInvoice__c];
        for(Integer i=0; i< lstInv.size() ; i++ ){
            lstInv[i].giic_RefInvoiceNo__c = strSKU + string.valueOf(i);
        }
        update lstInv;
        gii__OrderInvoice__c ObjInv = lstInv[0];
        
        // system.debug('Invoice=== '+ lstInv);
         system.assertEquals(lstInv.size()>0,true);
         
        lstInvDet =[SELECT Id, gii__Invoice__c, gii__Invoice__r.name, Name, LastModifiedDate, gii__Invoice__r.gii__Account__c, gii__RemainingCreditQuantity__c, gii__Invoice__r.gii__InvoiceCreationCompleted__c, gii__Invoice__r.giic_RefInvoiceNo__c,gii__Invoice__r.gii__SalesOrder__c, gii__Invoice__r.gii__SalesOrder__r.Name, gii__ProductReference__r.gii__ProductReference__c, gii__ProductReference__r.gii__ProductReference__r.SKU__c FROM gii__OrderInvoiceDetail__c ];
        
        
        
        oIAC = new gii__InvoiceAdditionalCharge__c();
        oIAC.gii__AdditionalCharge__c = oAC.Id;
        oIAC.gii__SalesOrder__c = giic_Test_DataCreationUtility.lstSalesOrder_N[0].Id;
        oIAC.gii__SalesOrderAdditionalCharge__c = oSOAC.Id;
        oIAC.gii__Invoice__c = ObjInv.Id;
        oIAC.gii__Quantity__c = 1.00;
        oIAC.gii__UnitCost__c = 1.00;
        oIAC.gii__UnitPrice__c = 1.00;      
        insert oIAC;
        
        
        giic_CreditCardServiceHandler.createArInvPayments(ObjInv);
        
        Case cas = new Case(AccountId = giic_Test_DataCreationUtility.lstAccount_N[0].id, Priority = 'Medium', Origin = 'Email', Approval_Status__c ='Pending', Status = 'Draft');
        insert cas;
        string sSKU = giic_Test_DataCreationUtility.lstProdRef_N[0].gii__ProductReference__r.SKU__c;

        Object prodObj = giic_ReturnCreditsHelper.getProductList(cas.id,sSKU).get('productList');
        system.assert(prodObj!= null);
      //  system.debug('prod list ' + prodObj);
       // system.debug('ObjInv.Id------>>>>>> ' + ObjInv.Id);
        system.assert(ObjInv.Id != null);
        giic_ReturnCreditsHelper.getInvoiceAdditionalCharge(ObjInv.id);
        //giic_ReturnCreditsHelper.submitforApproval(cas.id);
        giic_ReturnCreditsHelper.createAR(cas.id);
       
        Set<String> rejectedCases  = new Set<String>();
        rejectedCases.add(cas.id);
        giic_ReturnCreditsHelper.deleteARCredit(rejectedCases);
        String recordId =cas.id;
        System.debug('UOM Detail' + giic_Test_DataCreationUtility.lstUnitofMeasure);
        List<giic_InvoicedProductWrapper> lstIPW = new List<giic_InvoicedProductWrapper>();
            giic_InvoicedProductWrapper wrapperinvoice = new giic_InvoicedProductWrapper();
            wrapperinvoice.isSelected=true;
            wrapperinvoice.productId= giic_Test_DataCreationUtility.lstProdRef_N[0].id;
            wrapperinvoice.productSKU='sjlsj';
            wrapperinvoice.productDescription='shsk';
            wrapperinvoice.invoiceId= ObjInv.id;
            wrapperinvoice.invoiceNumber='hkshksh';
            wrapperinvoice.soId= giic_Test_DataCreationUtility.lstSalesOrder_N[0].id;
            wrapperinvoice.soPaymentMethod='sgjsgk';
            wrapperinvoice.soNumber='sssss';
            wrapperinvoice.invoiceDetailId=lstInvDet[0].id;
            wrapperinvoice.orderDate='03-01-2019';
            wrapperinvoice.orderQuantity=10;
            wrapperinvoice.qtyAvailableForReturn=5;
            wrapperinvoice.invoiceAmount=100;
            wrapperinvoice.invoiceAmountStocking=90;
            wrapperinvoice.UOM='EA';
            wrapperinvoice.sellingUOM='ajlaj';
            wrapperinvoice.sellingUOMId=giic_Test_DataCreationUtility.lstUnitofMeasure[0].id;
            wrapperinvoice.stockingUOM='EA';
            wrapperinvoice.stockingUOMId=giic_Test_DataCreationUtility.lstUnitofMeasure[0].id;
            wrapperinvoice.returnQty=20;
            wrapperinvoice.returnQtyOld=50;
            wrapperinvoice.returnReason='Pet Related (RE-PET)';
            wrapperinvoice.returnAmount=100;
            wrapperinvoice.returnRewardAmount=10;
            lstIPW.add(wrapperinvoice);
            
            
            gii__InvoiceAdditionalCharge__c objIAC= new gii__InvoiceAdditionalCharge__c();
            objIAC.gii__UnitPrice__c = 1;
            objIAC.gii__UnitCost__c =1;
            objIAC.gii__Quantity__c = 1;
            objIAC.gii__TaxAmount__c = 0;
            objIAC.gii__SalesOrder__c = giic_Test_DataCreationUtility.lstSalesOrder_N[0].id;
            objIAC.gii__Invoice__c = ObjInv.id;
            insert objIAC;
            
            gii__AdditionalCharge__c objAC = new gii__AdditionalCharge__c();
            objAC.gii__UnitPrice__c = 1;
            objAC.gii__UnitCost__c =1;            
            insert objAC;
            
            
           
          
         List<giic_InvoicedProductWrapper> lstInvAC = new List<giic_InvoicedProductWrapper>();
            giic_InvoicedProductWrapper wrapperinvoice2 = new giic_InvoicedProductWrapper();
            wrapperinvoice2.isSelected=true;
            wrapperinvoice2.invoiceId= ObjInv.id;
            wrapperinvoice2.invoiceNumber=ObjInv.name;
            wrapperinvoice2.soId= giic_Test_DataCreationUtility.lstSalesOrder_N[0].id;
            wrapperinvoice2.additionalChargeAmount = 100;
            wrapperinvoice2.additionalChargeQuantity = 1;
            wrapperinvoice2.additionalChargeInvId = objIAC.id;
            wrapperinvoice2.additionalChargeId = objAC.id;
            lstInvAC.add(wrapperinvoice2);
            
            String strInvACList = JSON.serialize(lstInvAC);
            String strProdList=JSON.serialize(lstIPW);
            String strWrapper =JSON.serialize(wrapperinvoice);
            
            System.assert(strWrapper!=null);
            System.assertEquals(strWrapper.length()>0, strWrapper!='');
            
        giic_ReturnCreditsHelper.getProductwithChangedUOM(strWrapper);
        giic_ReturnCreditsHelper.saveReturnList(recordId,strProdList,strInvACList);
        
        // Creating AR Invoice payment Record
        List<gii__ARInvoicePayment__c> lstARIPayment = new List<gii__ARInvoicePayment__c>();
        gii__ARInvoicePayment__c ARIPayment = new gii__ARInvoicePayment__c();
        ARIPayment.gii__PaidAmount__c = 50;
        ARIPayment.gii__Invoice__c = ObjInv.Id;
        ARIPayment.gii__PaymentMethod__c = giic_Constants.CREDIT_CARD;
        lstARIPayment.add(ARIPayment);
         gii__ARInvoicePayment__c ARIPayment1 = new gii__ARInvoicePayment__c();
        ARIPayment1.gii__PaidAmount__c = 100;
        ARIPayment1.gii__Invoice__c = ObjInv.Id;
        ARIPayment1.gii__PaymentMethod__c = 'ACH';
        lstARIPayment.add(ARIPayment1);
        insert lstARIPayment;
        
        
        // creating ARCreditPayment Record.
        Map<id, gii__ARCredit__c> ARCobj = new Map<id, gii__ARCredit__c>([Select Id, gii_Case__c, gii_Case__r.giic_TotalReturnAmount__c, gii__Invoice__c,
                                                                                   gii__Invoice__r.giic_WarehouseShippingOrderStaging__r.Name,
                                                                                   gii__SalesOrder__r.Name,
                                                                                   gii__BillingName__c, gii__BillingStateProvince__c,
                                                                                   gii__BillingStreet__c, gii__BillingZipPostalCode__c, gii__BillingCountry__c, gii__Invoice__r.name,
                                                                                   gii__BillingCity__c, gii__NetAmount__c, gii__TotalPaidAmount__c, gii__Account__r.Bill_To_Customer_ID__c, gii__Account__r.ShipToCustomerNo__c 
                                                                                   from gii__ARCredit__c where gii__Invoice__c =:ObjInv.Id limit 1]);
        list<gii__ARCreditPayment__c> lstARCPayment = new list<gii__ARCreditPayment__c>();
        gii__ARCreditPayment__c ARCPayment = new gii__ARCreditPayment__c();
        ARCPayment.gii__PaidAmount__c = 50;
        ARCPayment.gii__ARCredit__c = ARCobj.values()[0].id;
        ARCPayment.gii__PaymentMethod__c = giic_Constants.CREDIT_CARD;
        lstARCPayment.add(ARCPayment);
        System.enqueueJob(new giic_ReturnCreditsQueueable(ARCobj, lstARCPayment)); // for accepted response.
        ARCPayment.gii__PaidAmount__c = 70;
        System.enqueueJob(new giic_ReturnCreditsQueueable(ARCobj, lstARCPayment)); // for rejected response.
      Test.stopTest();
        }
        
    }
    
}