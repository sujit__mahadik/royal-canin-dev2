public class giic_LoyaltyManagementCommunityCtlr{
    public String actName{get;set;}
    public String TotallifetimePoint{get;set;}
    public List<String> TotalCurrentPoint{get;set;}
    public String strMemberShipId{get;set;}
    public Integer strMembershipYear{get;set;}
    public Integer TotalLastMonthPoint{get;set;}
    public List<String> TotalRewardPointBalance{get;set;}
    public Account AccountObj{get;set;}
    Public Id accountId{get;set;}
    public List<User> usrObj{get;set;}
    public List<giic_MembersLoyaltyPoints__c> memberLoyaltyPointLst{get;set;}
    Public List<gii__SalesOrderPayment__c> lstSOPayment{get;set;}
    public String strMemberShipDetails{get;set;}
    public String strNextLevelMemberShipDetails{get;set;}
    public String strNextLevelMemberShipPoints{get;set;}
    public List<SOPaymentWrapper> lstSOPaymentWrapper{get;set;}
    public List<giic_LoyaltyPointTracker__c> lstLoyaltyTracker{get;set;}
    //Private Map<String,String> mapNextLevel=new Map<String,String>();
    public class SOPaymentWrapper{
        public string strTransactionId{get;set;}
        public string strPaymentMethod{get;set;}
        public string strPaymentAmount{get;set;}
        public string strPaymentDate{get;set;}
        public string strRewardPointNumber{get;set;}
        public string strPointEarned{get;set;}
        public string strProductName{get;set;}
    }
    public string strErrorMsg{get;set;}
    public Boolean BoolError{get;set;}
    public giic_LoyaltyManagementCommunityCtlr(){
        actName = '';
        lstLoyaltyTracker=new List<giic_LoyaltyPointTracker__c>();
        lstSOPaymentWrapper=new List<SOPaymentWrapper>();
        lstSOPayment=new List<gii__SalesOrderPayment__c>();
        memberLoyaltyPointLst=new List<giic_MembersLoyaltyPoints__c>();
        //mapNextLevel=new Map<string,string>();
        BoolError=false;
        strErrorMsg='';
        TotalRewardPointBalance=new List<String> ();
        //TotallifetimePoint=new List<String> ();
        TotalCurrentPoint=new List<String> ();
        
        usrObj=[select id,Contact.AccountId,
        Contact.Account.giic_CurrentPoints__c,
        Contact.Account.giic_TypeOfMembership__c,
        Contact.Account.giic_LifeTimePoints__c,
        Contact.Account.giic_TypeOfMembership__r.Name,
        Contact.Account.giic_TypeOfMembership__r.giic_QualifyingPoints__c,
        Contact.Account.giic_TotalAvailableRewards__c,
        Contact.Account.Name,CreatedDate
        from User where Id=: UserInfo.getUserId()];
        
        String actId = '';
        if(usrObj!=null && !usrObj.isEmpty() && usrObj[0].ContactId!=null){
            actId = usrObj[0].Contact.AccountId;
        }else{
            actId = ApexPages.currentPage().getParameters().get('id');
        }
        
        if(actId != null && actId != '')
        AccountObj =[select id,
            giic_CurrentPoints__c,
            giic_TypeOfMembership__c,
            giic_LifeTimePoints__c,
            giic_TypeOfMembership__r.Name,
            giic_TypeOfMembership__r.giic_QualifyingPoints__c,
            giic_TotalAvailableRewards__c,
            Name,CreatedDate
            from Account where Id=:actId ];
            
        if(AccountObj !=null ){
            actName = AccountObj.Name;
            getMemberLoyaltyPointDetails();
            getCoupanBalance();
            getOrderHistory();
            getCurrentPoint();
            getTotalLifeimePoints();
            getMemberShipDetais();
            getLoyaltyTrackerHistory();
            getNextLevelMemberShip();
        }
        else{
            
        }
        
    }
    
    public void getNextLevelMemberShip(){
            Map<string,string> mapNextLevel=new Map<string,string>();
            mapNextLevel.put('Silver','Gold');
            mapNextLevel.put('Gold','Platinum');
            mapNextLevel.put('Platinum','Diamond');
            String strTemp='';
            if(AccountObj.giic_TypeOfMembership__r.Name.contains('Diamond')){
                    strTemp='Diamond';
                }
                else if(AccountObj.giic_TypeOfMembership__r.Name.contains('Gold')){
                    strTemp='Platinum';
                }
                else if(AccountObj.giic_TypeOfMembership__r.Name.contains('Platinum')){
                    strTemp='Diamond';
                }
                else if(AccountObj.giic_TypeOfMembership__r.Name.contains('Silver')){
                    strTemp='Gold';
                }
            String strNxtLevel=strTemp;
            strNextLevelMemberShipDetails=strTemp;
            strNxtLevel=strNxtLevel + ' Membership';
            list<giic_MembershipType__c> lstMemberShipType=new List<giic_MembershipType__c>();
            System.debug('strNxtLevel=='+strNxtLevel);
            lstMemberShipType=[select Id,name,giic_QualifyingPoints__c from giic_MembershipType__c where Name=:strNxtLevel limit 1];
            System.debug('lstMemberShipType=='+lstMemberShipType);
            strNextLevelMemberShipPoints= String.ValueOf(Integer.valueOf(lstMemberShipType[0].giic_QualifyingPoints__c-AccountObj.giic_CurrentPoints__c));
    }
    public pagereference getValidateRequest(){
        if(usrObj!=null && !usrObj.isEmpty() && usrObj[0].ContactId!=null){
            
        }
        else{
            BoolError=true;
            strErrorMsg='This Page can be only accessed through partner/Customer Users profile';
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,strErrorMsg);
            ApexPages.addMessage(myMsg);
            return null;
        }
        return null;
    }
    public void getMemberShipDetais(){
        if(AccountObj!=null){
            System.debug('RewardPoint=='+AccountObj.giic_TypeOfMembership__r.Name);
            if(AccountObj.giic_TypeOfMembership__c!=null ){
                if(AccountObj.giic_TypeOfMembership__r.Name.contains('Diamond')){
                    strMemberShipDetails='Diamond';
                }
                else if(AccountObj.giic_TypeOfMembership__r.Name.contains('Gold')){
                    strMemberShipDetails='Gold';
                }
                else if(AccountObj.giic_TypeOfMembership__r.Name.contains('Platinum')){
                    strMemberShipDetails='Platinum';
                }
                else if(AccountObj.giic_TypeOfMembership__r.Name.contains('Silver')){
                    strMemberShipDetails='Silver';
                }
            }
            
            
        }
    }
    
    public void getCoupanBalance(){
        if(AccountObj!=null ){
            System.debug('RewardPoint=='+AccountObj.giic_TotalAvailableRewards__c);
            if(AccountObj.giic_TotalAvailableRewards__c!=null && integer.ValueOf(AccountObj.giic_TotalAvailableRewards__c)>0){
                String[] lststrTemp=String.valueOf(AccountObj.giic_TotalAvailableRewards__c).split('\\.');
                String strTemp=lststrTemp[0];
                
                TotalRewardPointBalance=strTemp.split('');
            }
            else{
                TotalRewardPointBalance=String.valueOf('00').split('');
                
            }
            
        }
    }
    
    public void getCurrentPoint(){
        if(AccountObj!=null ){
            System.debug('RewardPoint=='+AccountObj.giic_CurrentPoints__c);
            if(AccountObj.giic_CurrentPoints__c!=null && integer.ValueOf(AccountObj.giic_CurrentPoints__c)>0){
                String[] lststrTemp=String.valueOf(AccountObj.giic_CurrentPoints__c).split('\\.');
                String strTemp=lststrTemp[0];
                TotalCurrentPoint=strTemp.split('');
            }
            else{
                TotalCurrentPoint=String.valueOf('00').split('');
                
            }
            
        }
    }
    
    public void getTotalLifeimePoints(){
        if(AccountObj!=null ){
            System.debug('RewardPoint=='+AccountObj.giic_LifeTimePoints__c);
            if(AccountObj.giic_LifeTimePoints__c!=null && integer.ValueOf(AccountObj.giic_LifeTimePoints__c)>0){
                String[] lststrTemp=String.valueOf(AccountObj.giic_LifeTimePoints__c).split('\\.');
                TotallifetimePoint=lststrTemp[0];
            }
            else{
                TotallifetimePoint='00';
            }
        }
    }
    
    public void getMemberLoyaltyPointDetails(){
    
        if(AccountObj!=null){
            strMembershipYear=usrObj[0].CreatedDate.year();
            memberLoyaltyPointLst=[select id,Name,giic_LoyaltyMembershipID__c from giic_MembersLoyaltyPoints__c where giic_Account__c =:AccountObj.Id];
            strMemberShipId=memberLoyaltyPointLst[0].giic_LoyaltyMembershipID__c;
        }
        
    }
    
    public void getOrderHistory(){
        if(AccountObj!=null ){
            lstSOPayment=[select Id,Name,gii__PaidAmount__c,gii__PaymentDate__c,gii__PaymentMethod__c,
                            gii__SalesOrder__c,gii__SalesOrder__r.gii__Account__c,createdDate,giic_RewardPointNum__c
                            from  gii__SalesOrderPayment__c
                            where gii__SalesOrder__r.gii__Account__c=:AccountObj.Id ];
            System.debug('lstSOPayment=='+lstSOPayment);  
            
            
            if(lstSOPayment!=null && !lstSOPayment.isEmpty()){
                for(gii__SalesOrderPayment__c sop:lstSOPayment){
                    SOPaymentWrapper sopWrp=new SOPaymentWrapper();
                    sopWrp.strPaymentMethod=sop.gii__PaymentMethod__c;
                    sopWrp.strPaymentAmount=String.valueof(sop.gii__PaidAmount__c);
                    sopWrp.strTransactionId=sop.Name;
                    if(sop.giic_RewardPointNum__c!=null)
                    sopWrp.strRewardPointNumber=sop.giic_RewardPointNum__c;
                    else
                    sopWrp.strRewardPointNumber='';
                    sopWrp.strPaymentDate=sop.createdDate.Month()+'/'+sop.createdDate.Day()+'/'+sop.createdDate.year();
                    
                    //sopWrp.strPaymentDate=sop.giic_RewardPointNum__c;
                    lstSOPaymentWrapper.add(sopWrp);
                }
            }
                
        }
    }
    
    public void getLoyaltyTrackerHistory(){
        if(AccountObj!=null ){
        
            lstLoyaltyTracker=[ select id,name,giic_SalesOrderLine__r.gii__TotalAmount__c,createdDate,giic_SalesOrderLine__r.gii__Product__c,
                                giic_SalesOrderLine__r.gii__Product__r.Name,giic_PointFromPromotion__c,giic_PointsEarned__c,giic_Promotion__c
                                from giic_LoyaltyPointTracker__c 
                                where giic_SalesOrderLine__c !=null and giic_SalesOrderLine__r.gii__SalesOrder__c!=null 
                                      and giic_SalesOrderLine__r.gii__SalesOrder__r.gii__Account__c=:AccountObj.Id ];
            
            System.debug('lstLoyaltyTracker=='+lstLoyaltyTracker);  
            
            
            if(lstLoyaltyTracker!=null && !lstLoyaltyTracker.isEmpty()){
                for(giic_LoyaltyPointTracker__c sop:lstLoyaltyTracker){
                    SOPaymentWrapper sopWrp=new SOPaymentWrapper();
                    sopWrp.strPaymentMethod='Cash';
                    sopWrp.strPaymentAmount=String.valueof(sop.giic_SalesOrderLine__r.gii__TotalAmount__c);
                    sopWrp.strTransactionId=sop.Name;
                    if(sop.giic_Promotion__c!=null && sop.giic_Promotion__c.trim()!='')
                    sopWrp.strRewardPointNumber=sop.giic_Promotion__c;
                    else
                    sopWrp.strRewardPointNumber='';
                    sopWrp.strPaymentDate=sop.createdDate.Month()+'/'+sop.createdDate.Day()+'/'+sop.createdDate.year();
                    sopWrp.strPointEarned=String.valueof(sop.giic_PointsEarned__c);
                    sopWrp.strProductName=String.valueof(sop.giic_SalesOrderLine__r.gii__Product__r.Name);
                    lstSOPaymentWrapper.add(sopWrp);
                }
            }
                
        }
    }
}