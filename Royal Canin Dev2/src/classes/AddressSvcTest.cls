@isTest
private class AddressSvcTest {
	
	@isTest static void TestAddressSvc() {
		AddressSvc addressSvc = new AddressSvc();
		addressSvc.AddressSvcSoap svcSoap = new addressSvc.AddressSvcSoap('testURL.com');
		new addressSvc.ValidateRequest();
		new addressSvc.PingResult();
		new addressSvc.IsAuthorizedResult();
		new addressSvc.ValidateResult();
		new addressSvc.Message();
		new addressSvc.ArrayOfValidAddress();
		new addressSvc.Profile();
		new addressSvc.ArrayOfMessage();
		new addressSvc.Security();
		new addressSvc.BaseAddress();
		new addressSvc.ValidAddress();
		new addressSvc.UsernameToken_element();
		new addressSvc.IsAuthorizedResponse_element();
		new addressSvc.PingResponse_element();
		new addressSvc.Ping_element();
		new addressSvc.ValidateResponse_element();
		new addressSvc.IsAuthorized_element();
		new addressSvc.Validate_element();
	}

	@isTest static void TestIsAuthorized(){
		AddressSvc addressSvc = new AddressSvc();
		addressSvc.AddressSvcSoap svcSoap = new addressSvc.AddressSvcSoap('testURL.com');
		Test.startTest();
			Test.setMock(WebServiceMock.class, new AddressSvcMock());
			AddressSvc.IsAuthorizedResult isAuthorizedResult = svcSoap.IsAuthorized('TestOperations');
		Test.stopTest();

		System.assertEquals('Success', isAuthorizedResult.ResultCode, 'Web service mock should return Success result code.');
		System.assertEquals('TestOperations', isAuthorizedResult.Operations, 'Web service mock should return the same operations that were passed in.');
	}

	@isTest static void TestValidate(){
		AddressSvc addressSvc = new AddressSvc();
		addressSvc.AddressSvcSoap svcSoap = new addressSvc.AddressSvcSoap('testURL.com');
		Test.startTest();
			Test.setMock(WebServiceMock.class, new AddressSvcMock());
			AddressSvc.ValidateRequest validateRequest = new AddressSvc.ValidateRequest();
			AddressSvc.ValidateResult validateResult = svcSoap.Validate(validateRequest);
		Test.stopTest();

		System.assertEquals('Success', validateResult.ResultCode, 'Web service mock should return Success result code.');
	}

	@isTest static void TestPing(){
		AddressSvc addressSvc = new AddressSvc();
		addressSvc.AddressSvcSoap svcSoap = new addressSvc.AddressSvcSoap('testURL.com');
		Test.startTest();
			Test.setMock(WebServiceMock.class, new AddressSvcMock());
			AddressSvc.PingResult pingResult = svcSoap.Ping('TestMessage');
		Test.stopTest();

		System.assertEquals('Success', pingResult.ResultCode, 'Web service mock should return Success result code.');
	}
}