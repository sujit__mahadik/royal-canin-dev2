/************************************************************************************
Version : 1.0
Created Date : 2nd Jan 2019
Function : giic_Test_SalesOrderStagingTrigHelp is test class for giic_SalesOrderStagingTriggerHelper
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/
@isTest(seeAllData = false)
public class giic_Test_SalesOrderStagingTrigHelp {
    @testSetup 
    static void setup(){
        giic_Test_DataCreationUtility.CreateAdminUser();
        List<gii__Warehouse__c> defaultWHList = giic_Test_DataCreationUtility.insertWarehouse();
        giic_Test_DataCreationUtility.testSystemPolicyCreation2();
        SetupTestData.createCustomSettings();
        // createing custom setting giic_Disable_Triggers__c
        List<giic_Disable_Triggers__c> lstDtri = [select id, giic_Disable_Trigger_Sales_Order_Staging__c from giic_Disable_Triggers__c];
        system.debug('lstDtri--->>>' + lstDtri);
        if(lstDtri == null || lstDtri.size() == 0){
            giic_Disable_Triggers__c oDT = new giic_Disable_Triggers__c();
            oDT.giic_Disable_Trigger_Sales_Order_Staging__c = false;
            insert oDT;
        }
        
        
        List<gii__Warehouse__c> testWareListN = giic_Test_DataCreationUtility.insertWarehouse_N(5); // lstWarehouse_N
        list<giic_CommonUtility.WarehouseDistance> lstWDistance = new list<giic_CommonUtility.WarehouseDistance>();
        giic_CommonUtility.WarehouseDistance objWD;
        integer intDistence = 300;
        for(gii__Warehouse__c objW :  giic_Test_DataCreationUtility.lstWarehouse_N){
            objWD = new giic_CommonUtility.WarehouseDistance(objW.Id, intDistence);
            intDistence = intDistence + 20;
            lstWDistance.add(objWD);
        }
        intDistence = intDistence + 55;
        objWD = new giic_CommonUtility.WarehouseDistance(defaultWHList[0].Id, intDistence);
        lstWDistance.add(objWD);
        string sWarehouseDistanceMapping = JSON.serialize(lstWDistance);
        List<Account> testConsumerAccountList = giic_Test_DataCreationUtility.insertConsumerAccount_N(5);
        for(Account OAc : giic_Test_DataCreationUtility.lstAccount_N ){
            OAc.giic_WarehouseDistanceMapping__c = sWarehouseDistanceMapping;
        }
        update giic_Test_DataCreationUtility.lstAccount_N;
        List<gii__Product2Add__c> testProductList = giic_Test_DataCreationUtility.insertProduct_N(); 
        giic_Test_DataCreationUtility.createCarrier(); 
        
        integer intCount = 0;
        for(gii__Product2Add__c objPR : giic_Test_DataCreationUtility.lstProdRef_N ){
            objPR.gii__DefaultWarehouse__c = testWareListN[math.mod(intCount,5)].Id;
            intCount++;
        }
        
        giic_Test_DataCreationUtility.lstProdRef_N[3].gii__NonStock__c = true;
        
        update giic_Test_DataCreationUtility.lstProdRef_N;
        // List<gii__SalesOrder__c> testSOList1 = giic_Test_DataCreationUtility.insertSalesOrder_N(10);
        
        giic_Test_DataCreationUtility.insertSalesOrderStaging_N(10);
        giic_Test_DataCreationUtility.insertSalesOrderLineStaging_N();
        giic_Test_DataCreationUtility.insertSalesOrderPaymentStaging_N();
        
    }
    
    
    public static testMethod void Test_OnAfterInsert(){
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];        
        System.Runas(u)        
        {   
            List<gii__SalesOrderStaging__c> lstSOS = [select id, name, giic_AddressType__c, giic_AddressValidated__c, giic_BillingCity__c, giic_BillingCounty__c, giic_BillingStateProvince__c, giic_BillingStreet__c, 
                                                      giic_BillingZipPostalCode__c, giic_BillToCustID__c, giic_BTEmail__c, giic_CCAuthorized__c, giic_Comments__c, giic_CustEmail__c, giic_CustomerPODate__c, giic_CustPhone__c, 
                                                      giic_ExtDocNo__c, giic_OrderDate__c, giic_OrderNumber__c, giic_OrderOrigin__c, giic_PaymentMethod__c, giic_PaymentTerms__c, giic_PromotionCalculated__c, giic_ShippingCounty__c,
                                                      giic_ShipToCity__c, giic_ShipToCountry__c, giic_Account__c, giic_ShipToName__c, giic_ShipToStateProvince__c, giic_ShipToStreet__c, giic_ShipToZipPostalCode__c, giic_Source__c, 
                                                      giic_Status__c, giic_TaxCalculated__c, giic_WarehouseCode__c, giic_ExecuteTrigger__c from gii__SalesOrderStaging__c limit 10 ];
            for(gii__SalesOrderStaging__c sosObj: lstSOS){
                sosObj.giic_ExecuteTrigger__c = true;
            }
            update lstSOS;
            System.debug(lstSOS.size());
            system.assertEquals(lstSOS.size()>0, true);
        }
    } 
}