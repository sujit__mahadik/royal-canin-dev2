global class AccountSalesSummaryBatch  implements Database.batchable<sObject>{
    /*
    Execute the appropriate commands below in 'Execute Anonymous' window

    If you want to run it with the default batch size of 200 records use the command below:

AccountSalesSummaryBatch a = new AccountSalesSummaryBatch();
Database.executeBatch(a);

    If you want to run it with a different batch size use the command below:

AccountSalesSummaryBatch a = new AccountSalesSummaryBatch();
Database.executeBatch(a, 20);

    If you want to run for a specific account use command below:

AccountSalesSummaryBatch a = new AccountSalesSummaryBatch('001j000000XzpRS');
Database.executeBatch(a);

    */

    global String query;
    global Id accountId;

    global AccountSalesSummaryBatch(string AccountId) {
        // GJK: 02/02/16 - Modified the query to process only Account records that have not been processed today.
        //this.query = 'Select id, PY_Rolling_Sales_Last_3_Periods__c, CY_Rolling_Sales_Last_3_Periods__c, Current_Fiscal_Period__c, Current_Fiscal_Quarter__c,Current_Fiscal_YTD__c,Prior_Fiscal_Year__c,Prior_FY_Period__c,Prior_FY_Quarter__c from account where Bill_To_Customer_ID__c != null';
        this.query = 'SELECT id, PY_Rolling_Sales_Last_3_Periods__c, CY_Rolling_Sales_Last_3_Periods__c ';
        this.query += ' , Current_Fiscal_Period__c, Current_Fiscal_Quarter__c,Current_Fiscal_YTD__c ';
        this.query += ' , Prior_Fiscal_Year__c,Prior_FY_Period__c,Prior_FY_Quarter__c ';
        this.query += ' , Sales_Summarization_Completed__c ';
        this.query += ' FROM Account ';
        this.query += ' WHERE Bill_To_Customer_ID__c != null ';
        // GJK: 02/02/16 - Modified the query to process only Account records that have not been processed today.
        this.query += ' AND Sales_Summarization_Completed__c = false ';
        if (AccountId!=null) this.query+=' and id =\''+accountId+'\'';
    }

    global AccountSalesSummaryBatch( ) {
        // GJK: 02/02/16 - Modified the query to process only Account records that have not been processed today.
        //this.query = 'Select id, PY_Rolling_Sales_Last_3_Periods__c, CY_Rolling_Sales_Last_3_Periods__c, Current_Fiscal_Period__c, Current_Fiscal_Quarter__c,Current_Fiscal_YTD__c,Prior_Fiscal_Year__c,Prior_FY_Period__c,Prior_FY_Quarter__c from account where Bill_To_Customer_ID__c != null';
        this.query = 'SELECT id, PY_Rolling_Sales_Last_3_Periods__c, CY_Rolling_Sales_Last_3_Periods__c ';
        this.query += ' , Current_Fiscal_Period__c, Current_Fiscal_Quarter__c,Current_Fiscal_YTD__c ';
        this.query += ' , Prior_Fiscal_Year__c,Prior_FY_Period__c,Prior_FY_Quarter__c ';
        this.query += ' , Sales_Summarization_Completed__c ';
        this.query += ' FROM Account ';
        this.query += ' WHERE Bill_To_Customer_ID__c != null ';
        // GJK: 02/02/16 - Modified the query to process only Account records that have not been processed today.
        this.query += ' AND Sales_Summarization_Completed__c = false ';
    }
    
    private Double getDirectDMSalesAmount(Sales_Summary__c ss, Set<String> metricTypesSet, Double ssPriorYear, String ssMaxPYWeek) {
        Double  Total_Sales_Amount, 
                Total_Sales_Amount_With_Discount,
                Direct_DM_Sales_Amount;
                
        if ( double.valueOf(ss.Year__c) == ssPriorYear && ss.Week__c > ssMaxPYWeek) {
            // GJK: 01/07/15 - Discard any Sales Summary Data for Prior Fiscal Year where Week > Maximum Week for this Fiscal Year
            Total_Sales_Amount                  = 0.0;
            Total_Sales_Amount_With_Discount    = 0.0;
        } else {
            if (metricTypesSet.contains(ss.Metric_Type__c)) {
                Total_Sales_Amount                  = ss.Total_Sales_Amount__c;
                // GJK: 01/27/16 - The Sales_Summary__c.DM_Sales_Amount field now includes the Discount.
                //                  There is no need to subtract the ss.Sales_Discount__c
                //Total_Sales_Amount_With_Discount  = ss.Total_Sales_Amount__c - ss.Sales_Discount__c;
                Total_Sales_Amount_With_Discount    = ss.Total_Sales_Amount__c;
            } else {
                // GJK: 01/04/16 - No exclusion rules have YET been defined if Metric Type is NOT a valid one.
                //                  - However as per current requirements we are only taking the filtered data and zeroing out if 
                //                    not part of the filter. 
                Total_Sales_Amount                  = 0.0;
                Total_Sales_Amount_With_Discount    = 0.0;
                // GJK: 01/07/15 - Uncomment out the following two lines if we need to add figures that do NOT have valid Metric Types
                /* 
                Total_Sales_Amount                  = ss.Total_Sales_Amount__c;
                Total_Sales_Amount_With_Discount    = ss.Total_Sales_Amount__c - ss.Sales_Discount__c;
                */
            }
        }
        
        if (ss.OLP__c) {
            // GJK: 01/13/15 - As per older requirements, We do not add in the Discount for OLP Sales Summary Records;
            Direct_DM_Sales_Amount = Total_Sales_Amount;    
        } else {
            Direct_DM_Sales_Amount = Total_Sales_Amount_With_Discount;  
        }

        return Direct_DM_Sales_Amount;
    }


    private Double getPYDirectDMSalesAmount(Sales_Summary__c ss, Set<String> metricTypesSet, Double ssPriorYear) {
        Double  Total_Sales_Amount, 
                Total_Sales_Amount_With_Discount,
                PY_Direct_DM_Sales_Amount;
                
        if ( double.valueOf(ss.Year__c) == ssPriorYear) {
            if (metricTypesSet.contains(ss.Metric_Type__c)) {
                Total_Sales_Amount                  = ss.Total_Sales_Amount__c;
                Total_Sales_Amount_With_Discount    = ss.Total_Sales_Amount__c;
            } else {
                Total_Sales_Amount                  = 0.0;
                Total_Sales_Amount_With_Discount    = 0.0;
            }
        } else {
            Total_Sales_Amount                  = 0.0;
            Total_Sales_Amount_With_Discount    = 0.0;
        }
        
        if (ss.OLP__c) {
            // GJK: 01/13/15 - As per older requirements, We do not add in the Discount for OLP Sales Summary Records;
            PY_Direct_DM_Sales_Amount = Total_Sales_Amount; 
        } else {
            PY_Direct_DM_Sales_Amount = Total_Sales_Amount_With_Discount;   
        }

        return PY_Direct_DM_Sales_Amount;
    }


    //get Querylocator with the specitied query
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext bc, List<sObject> accounts) {
        List<Account> updatedAccounts = new List<Account>();
        List <Sales_Summary__c> updSalesSummaryList = new List <Sales_Summary__c>();
        Set <Id> accountIds = new Set <Id>();
        Map <Id, Map <String, List <Sales_Summary__c>>> ssMap = new Map <Id, Map <String, List <Sales_Summary__c>>>(); //map will hold customerID to map of key representing the year-period and a list of all summary records related to that period
        Map <Id, List <Sales_Summary__c>> ssListMap = new Map <Id, List <Sales_Summary__c>>();  //variable will hold all of the sales summary records for the customer

        Map<String, Metric_Types_for_Sales_Summary__c> metricTypesMap;
        Set<String> metricTypesSet;

        metricTypesMap = Metric_Types_for_Sales_Summary__c.getAll();
        metricTypesSet = metricTypesMap.keySet();

        for(sObject s : accounts)
        {
            Account a = (Account)s;
            accountIds.add(a.id);

            ssListMap.put(a.id, new List <Sales_Summary__c>());
            ssmap.put(a.id, new Map<String, List<Sales_Summary__c>>());
            //ssLast3Map.put(a.id, new List <Sales_Summary__c>());
        }

        /*
        //  GJK: 01/04/15 - Commenting out old Sales_Summary__c query and keeping for reference.
        for (Sales_Summary__c ss: [Select s.Year__c, s.Type__c, s.Total_Sales_Amount__c, s.Sales_Discount__c, 
                                        s.Tonnage__c, 
                                        s.SalesSumID__c, s.Quarter__c, s.Product_Category__c,  
                                        s.Period__c, s.Period_Start_Date__c, s.Period_End_Date__c, s.Id, s.Customer__c, 
                                        s.Current_FYTD__c, s.Current_FQTD__c, s.Current_FMTD__c, s.OLP__c
                                     From Sales_Summary__c s
                                     Where Customer__c in :accountIds
                                     and type__c = 'Fiscal'
                                     order by Customer__c, s.Period_End_Date__c desc]) {
        */

        //  GJK: 01/04/15 - Modifying the query to do the following:
        //                  - Remove the Type__c field that held 'Fiscal' or 'Calendar' types
        //                  - Added Metric_Type__c, Week__c fields
        for (Sales_Summary__c ss: [Select s.Year__c, s.Total_Sales_Amount__c, s.Sales_Discount__c, 
                                        s.Tonnage__c, 
                                        s.SalesSumID__c, s.Quarter__c, s.Product_Category__c,  
                                        s.Period__c, s.Period_Start_Date__c, s.Period_End_Date__c, s.Id, s.Customer__c, 
                                        s.Current_FYTD__c, s.Current_FQTD__c, s.Current_FMTD__c, s.OLP__c,
                                        s.Metric_Type__c, s.Week__c
                                     From Sales_Summary__c s
                                     Where Customer__c in :accountIds
                                     order by Customer__c, s.Period_End_Date__c desc]) {

            string sskey = ss.Year__c+'-'+ss.Period__c;
            
            if (ssMap.get(ss.Customer__c) != null && ssMap.get(ss.Customer__c).get(sskey)!=null) {  //setting up the map for the periods and all related salas summaries
                ssMap.get(ss.Customer__c).get(sskey).add(ss);
            }
            else {
                List <Sales_Summary__c> newssList = new List <Sales_Summary__c>{ss};
                ssMap.get(ss.Customer__c).put(sskey,newssList);
            }

            ssListMap.get(ss.Customer__c).add(ss);
        }
        
        Set <String> currentYearPeriods = new Set <String>();
        Set <String> currentYearRollingKeys = new Set <String>();

        // GJK: 01/07/06    - Moved code from original location within Account Loop to outside since it is invariant in the Loop
        Global_Parameters__c maxDateCs = Global_Parameters__c.getValues('maxSalesSummaryDate');
        Integer maxPeriod = Integer.valueOf(maxDateCs.Attribute1__c);
                
        // GJK: 01/07/06    - Query the Maximum Week to be used to discard data for Prior Year 
        //                    that is greater than the Maximum Week in the current Fiscal Year.
        String ssMaxPYWeek      = maxDateCs.Attribute4__c;
        Double ssPriorYear      = double.valueOf(maxDateCs.Attribute3__c) - 1;
        
        for(sObject s : accounts)
        {
            Account a = (Account)s;
            a.Current_Fiscal_Period__c= 0;
            a.Current_Fiscal_Quarter__c= 0;
            a.Current_Fiscal_YTD__c= 0;

            a.Prior_Fiscal_Year__c= 0;
            a.Prior_FY_Period__c= 0;
            a.Prior_FY_Quarter__c= 0;

            a.PY_Rolling_Sales_Last_3_Periods__c=0;
            a.CY_Rolling_Sales_Last_3_Periods__c=0;

            a.OLP_Current_Fiscal_YTD__c=0;
            a.OLP_Prior_Fiscal_Year__c=0;


            /*
            // GJK: 01/07/06    - Moved code from original location within Account Loop to outside before loop since it is invariant in the Loop
            Global_Parameters__c maxDateCs = Global_Parameters__c.getValues('maxSalesSummaryDate');
            Integer maxPeriod = Integer.valueOf(maxDateCs.Attribute1__c);
            */
            
            //Sales_Summary__c ssMax;
            Sales_Summary__c tempssMax; //temporary variable to not impact the ssMax needed in calcs below
            List <Sales_Summary__c> ssList = ssListMap.get(a.id);
                
            if (!ssList.isEmpty()){

                for (integer i=1; i<=maxPeriod; i++){
                    currentYearPeriods.add(string.valueOf(i));
                }

                string sskey = maxDateCs.value__c;

                /*currentYearRollingKeys.add(sskey);


                for (Sales_Summary__c ss: ssList){
                    if (maxDateCs.date__c.addDays(-1)==ss.period_end_Date__c){
                        tempssMax = ss;
                        break;
                    }
                }

                if (tempSsMax!=null){
                    sskey = tempssMax.Year__c+'-'+tempssMax.Period__c;

                    currentYearRollingKeys.add(sskey);

                    for (Sales_Summary__c ss: ssList){
                        if (tempssMax.period_start_date__c.addDays(-1)==ss.period_end_Date__c){
                            tempssMax = ss;
                            break;
                        }
                    }
                }

                if (tempSsMax!=null){
                    sskey = tempssMax.Year__c+'-'+tempssMax.Period__c;
                    currentYearRollingKeys.add(sskey);
                }*/

                Double  Total_Sales_Amount, 
                        Total_Sales_Discount,
                        Total_Sales_Amount_With_Discount,
                        PY_Direct_DM_Sales_Amount,
                        Direct_DM_Sales_Amount;

                Double  DM_Full_Truck_Load_Sales,
                        DM_Independent_Retailer_Sales,
                        DM_PRO_Sales,
                        DM_Vet_Clinic_CFP_Sales,
                        DM_Vet_Diet_Sales,
                        OLP_DM_Sales,
                        Vet_DM_OLP_Drop_Ship_Sales;

                Double  Current_Fiscal_Period_Amount,
                        Prior_FY_Period_Amount,
                        Current_Fiscal_Quarter_Amount,
                        Prior_FY_Quarter_Amount,
                        Current_Fiscal_YTD_Amount,
                        Prior_Fiscal_Year_Amount,
                        PY_Total_DM_Sales_Amount,
                        OLP_Current_Fiscal_YTD_Amount,
                        OLP_Prior_Fiscal_Year_Amount;

                // Reset Aggregations by Account
                Total_Sales_Amount                  = 0.0;
                Total_Sales_Discount                = 0.0;
                Total_Sales_Amount_With_Discount    = 0.0;
                Direct_DM_Sales_Amount              = 0.0;
                PY_Direct_DM_Sales_Amount           = 0.0;

                // Reset Metric Type Aggregations by Account
                DM_Full_Truck_Load_Sales        = 0.0;
                DM_Independent_Retailer_Sales   = 0.0;
                DM_PRO_Sales                    = 0.0;
                DM_Vet_Clinic_CFP_Sales         = 0.0;
                DM_Vet_Diet_Sales               = 0.0;
                OLP_DM_Sales                    = 0.0;
                Vet_DM_OLP_Drop_Ship_Sales      = 0.0;

                // Reset Period Type Aggregations by Account
                Current_Fiscal_Period_Amount    = 0.0;
                Prior_FY_Period_Amount          = 0.0;
                Current_Fiscal_Quarter_Amount   = 0.0;
                Prior_FY_Quarter_Amount         = 0.0;
                Current_Fiscal_YTD_Amount       = 0.0;
                Prior_Fiscal_Year_Amount        = 0.0;
                PY_Total_DM_Sales_Amount        = 0.0;
                OLP_Current_Fiscal_YTD_Amount   = 0.0;
                OLP_Prior_Fiscal_Year_Amount    = 0.0;

                for (Sales_Summary__c ss: ssList){
                    
                    if (ss.Metric_Type__c != null && ss.Total_Sales_Amount__c != null && ss.Sales_Discount__c != null) {
                        
                        Direct_DM_Sales_Amount      = getDirectDMSalesAmount(ss, metricTypesSet, ssPriorYear, ssMaxPYWeek);
                        PY_Direct_DM_Sales_Amount   = getPYDirectDMSalesAmount(ss, metricTypesSet, ssPriorYear);

                        /*
                        //  GJK: 01/13/16 - Uncomment this commented out code if the requirements change to store 
                        //                  individual metric type aggregations on the Account.

                        // Aggregate by Metric Type
                        if (ss.Metric_Type__c == 'DM Full Truck Load Sales') {
                            DM_Full_Truck_Load_Sales += Direct_DM_Sales_Amount;
                        }
                        if (ss.Metric_Type__c == 'DM Independent Retailer Sales') {
                            DM_Independent_Retailer_Sales += Direct_DM_Sales_Amount;
                        }
                        if (ss.Metric_Type__c == 'DM PRO Sales') {
                            DM_PRO_Sales += Direct_DM_Sales_Amount;
                        }
                        if (ss.Metric_Type__c == 'DM Vet Clinic CFP Sales') {
                            DM_Vet_Clinic_CFP_Sales += Direct_DM_Sales_Amount;
                        }
                        if (ss.Metric_Type__c == 'DM Vet Diet Sales (at PetSmart)') {
                            DM_Vet_Diet_Sales += Direct_DM_Sales_Amount;
                        }
                        if (ss.Metric_Type__c == 'OLP DM Sales') {
                            OLP_DM_Sales += Direct_DM_Sales_Amount;
                        }
                        if (ss.Metric_Type__c == 'Vet DM OLP Drop Ship Sales') {
                            Vet_DM_OLP_Drop_Ship_Sales += Direct_DM_Sales_Amount;
                        }
                        */
                    }                           
                    
                    // Current & Prior Fiscal Period Data
                    if (ss.period_start_date__c == maxDateCs.date__c && 
                            //!ss.OLP__c && ss.Total_Sales_Amount__c != null && ss.Sales_Discount__c != null ){  //just get the total related to the max date in the system for current
                            ss.Total_Sales_Amount__c != null && ss.Sales_Discount__c != null ){  //just get the total related to the max date in the system for current
                        Current_Fiscal_Period_Amount += Direct_DM_Sales_Amount;
                    }
                    if (ss.Period__c == maxPeriod && 
                            ss.Week__c <= maxDateCs.Attribute4__c && 
                            double.valueOf(ss.Year__c) == double.valueOf(maxDateCs.Attribute3__c)-1 && 
                            //!ss.OLP__c && ss.Total_Sales_Amount__c != null && ss.Sales_Discount__c != null){  //period based on max's period and year -1
                            ss.Total_Sales_Amount__c != null && ss.Sales_Discount__c != null){  //period based on max's period and year -1
                        Prior_FY_Period_Amount += Direct_DM_Sales_Amount;
                    }
                    
                    // Current & Prior Fiscal Quarter Data
                    if (ss.Quarter__c == maxDateCs.Attribute2__c && ss.Year__c == maxDateCs.Attribute3__c && 
                            //!ss.OLP__c && ss.Total_Sales_Amount__c != null && ss.Sales_Discount__c != null){
                            ss.Total_Sales_Amount__c != null && ss.Sales_Discount__c != null){
                        Current_Fiscal_Quarter_Amount += Direct_DM_Sales_Amount;
                    }
                    if (currentYearPeriods.contains(String.valueOf(ss.Period__c)) && 
                            ss.Week__c <= maxDateCs.Attribute4__c && 
                            ss.Quarter__c == maxDateCs.Attribute2__c && 
                            double.valueOf(ss.Year__c) == double.valueOf(maxDateCs.Attribute3__c)-1 &&
                            //!ss.OLP__c && ss.Total_Sales_Amount__c != null && ss.Sales_Discount__c != null){
                            ss.Total_Sales_Amount__c != null && ss.Sales_Discount__c != null){
                        Prior_FY_Quarter_Amount += Direct_DM_Sales_Amount;
                    }
                    
                    // Current & Prior Fiscal Year-To-Date Data
                    if (ss.Year__c == maxDateCs.Attribute3__c && 
                            !ss.OLP__c && ss.Total_Sales_Amount__c != null && ss.Sales_Discount__c != null){
                            //ss.Total_Sales_Amount__c != null && ss.Sales_Discount__c != null){
                        Current_Fiscal_YTD_Amount += Direct_DM_Sales_Amount;
                    }
                    if (currentYearPeriods.contains(String.valueOf(ss.Period__c)) &&
                            ss.Week__c <= maxDateCs.Attribute4__c && 
                            double.valueOf(ss.Year__c) == double.valueOf(maxDateCs.Attribute3__c)-1 &&
                            !ss.OLP__c && ss.Total_Sales_Amount__c != null && ss.Sales_Discount__c != null){
                            //ss.Total_Sales_Amount__c != null && ss.Sales_Discount__c != null){
                        Prior_Fiscal_Year_Amount += Direct_DM_Sales_Amount;
                    }

                    // Prior Year Total DM Sales Data
                    if (double.valueOf(ss.Year__c) == double.valueOf(maxDateCs.Attribute3__c)-1 &&
                            ss.Total_Sales_Amount__c != null && ss.Sales_Discount__c != null){
                            //ss.Total_Sales_Amount__c != null && ss.Sales_Discount__c != null){
                        PY_Total_DM_Sales_Amount += PY_Direct_DM_Sales_Amount;
                    }
                    
                    // Current & Prior OLP Fiscal Year-To-Date Data
                    if(ss.Year__c == maxDateCs.Attribute3__c && 
                            ss.OLP__c && ss.Total_Sales_Amount__c != null){
                        OLP_Current_Fiscal_YTD_Amount += Direct_DM_Sales_Amount;
                    }
                    if (currentYearPeriods.contains(String.valueOf(ss.Period__c)) &&
                            ss.Week__c <= maxDateCs.Attribute4__c && 
                            double.valueOf(ss.Year__c) == double.valueOf(maxDateCs.Attribute3__c)-1 &&
                            ss.OLP__c && ss.Total_Sales_Amount__c != null){
                        OLP_Prior_Fiscal_Year_Amount += Direct_DM_Sales_Amount;
                    }

                    /*
                    //  GJK: 01/04/16 - Commenting out old Sales_Summary__c aggregation code and keeping for reference.

                    if (ss.period_start_date__c == maxDateCs.date__c && !ss.OLP__c && ss.Total_Sales_Amount__c != null && ss.Sales_Discount__c != null ){  //just get the total related to the max date in the system for current
                        a.Current_Fiscal_Period__c += (ss.Total_Sales_Amount__c - ss.Sales_Discount__c);
                    }
                    if (ss.Period__c == maxPeriod && double.valueOf(ss.Year__c)==double.valueOf(maxDateCs.Attribute3__c)-1 && !ss.OLP__c && ss.Total_Sales_Amount__c != null && ss.Sales_Discount__c != null){  //period based on max's period and year -1
                        a.Prior_FY_Period__c += (ss.Total_Sales_Amount__c - ss.Sales_Discount__c);
                    }
                    //if (ss.Quarter__c== maxDateCs.Attribute2__c && ss.Year__c==maxDateCs.Attribute3__c && !ss.OLP__c){
                    if (ss.Quarter__c== maxDateCs.Attribute2__c && ss.Year__c==maxDateCs.Attribute3__c && !ss.OLP__c && ss.Total_Sales_Amount__c != null && ss.Sales_Discount__c != null){
                        a.Current_Fiscal_Quarter__c += (ss.Total_Sales_Amount__c - ss.Sales_Discount__c);
                    }
                    if (currentYearPeriods.contains(String.valueOf(ss.Period__c)) && 
                            ss.Quarter__c==maxDateCs.Attribute2__c&& double.valueOf(ss.Year__c)==double.valueOf(maxDateCs.Attribute3__c)-1 &&
                            !ss.OLP__c && ss.Total_Sales_Amount__c != null && ss.Sales_Discount__c != null){
                        a.Prior_FY_Quarter__c += (ss.Total_Sales_Amount__c - ss.Sales_Discount__c);
                    }
                    if (ss.Year__c==maxDateCs.Attribute3__c && !ss.OLP__c && ss.Total_Sales_Amount__c != null && ss.Sales_Discount__c != null){
                        a.Current_Fiscal_YTD__c += (ss.Total_Sales_Amount__c - ss.Sales_Discount__c);
                    }
                    if (currentYearPeriods.contains(String.valueOf(ss.Period__c))&&
                                double.valueOf(ss.Year__c)==double.valueOf(maxDateCs.Attribute3__c)-1&&
                                !ss.OLP__c && ss.Total_Sales_Amount__c != null && ss.Sales_Discount__c != null){
                        a.Prior_Fiscal_Year__c += (ss.Total_Sales_Amount__c - ss.Sales_Discount__c);
                    }
                    if(ss.Year__c==maxDateCs.Attribute3__c && ss.OLP__c && ss.Total_Sales_Amount__c != null){
                        a.OLP_Current_Fiscal_YTD__c += (ss.Total_Sales_Amount__c);
                    }
                    if (currentYearPeriods.contains(String.valueOf(ss.Period__c))&&
                                double.valueOf(ss.Year__c)==double.valueOf(maxDateCs.Attribute3__c)-1&&
                                ss.OLP__c && ss.Total_Sales_Amount__c != null){
                        a.OLP_Prior_Fiscal_Year__c += (ss.Total_Sales_Amount__c);
                    }
                    */

                    /*
                    //  GJK: 01/04/16 - Making the folloing changes for Performance Enhancements:
                    //                  Commenting out the code that updates the 'Include_In_Report__c' field on Sales_Summary__c

                    if (currentYearPeriods.contains(String.valueOf(ss.Period__c))){
                        updSalesSummaryList.add(new Sales_Summary__c(id=ss.id, Include_In_Report__c = true));
                    }
                    */
                }

                a.Current_Fiscal_Period__c          = Current_Fiscal_Period_Amount;
                a.Prior_FY_Period__c                = Prior_FY_Period_Amount;

                a.Current_Fiscal_Quarter__c         = Current_Fiscal_Quarter_Amount;
                a.Prior_FY_Quarter__c               = Prior_FY_Quarter_Amount;
                    
                a.Current_Fiscal_YTD__c             = Current_Fiscal_YTD_Amount;
                a.Prior_Fiscal_Year__c              = Prior_Fiscal_Year_Amount;
                
                a.PY_Total_DM_Sales__c              = PY_Total_DM_Sales_Amount;

                a.OLP_Current_Fiscal_YTD__c         = OLP_Current_Fiscal_YTD_Amount;
                a.OLP_Prior_Fiscal_Year__c          = OLP_Prior_Fiscal_Year_Amount;

                //Code changes for rolling keys - 12/08/155 - MS Begin
                //second sskey for rolling keys
                integer yearInt = integer.valueOf(maxDateCs.value__c.split('-')[0]);
                //Code changes for rolling keys - 12/08/155 - MS End
                currentYearRollingKeys.add(maxDateCs.value__c);
                currentYearRollingKeys.add(yearInt+'-'+(maxPeriod-1));
                currentYearRollingKeys.add(yearInt+'-'+(maxPeriod-2));

                for (string key: currentYearRollingKeys) {
                   if(ssMap.get(a.id) != null && ssMap.get(a.id).get(key) != null){
                        for (Sales_Summary__c ss: ssMap.get(a.id).get(key)){
                            //if(!ss.OLP__c && ss.Total_Sales_Amount__c != null && ss.Sales_Discount__c != null){
                            if(ss.Total_Sales_Amount__c != null && ss.Sales_Discount__c != null){
                                //a.CY_Rolling_Sales_Last_3_Periods__c += (ss.Total_Sales_Amount__c - ss.Sales_Discount__c);

                                Direct_DM_Sales_Amount  = getDirectDMSalesAmount(ss, metricTypesSet, ssPriorYear, ssMaxPYWeek);
                                a.CY_Rolling_Sales_Last_3_Periods__c += Direct_DM_Sales_Amount;
                            }
                        }
                    }
                    
                    string year = string.valueOf(yearInt-1);
                    sskey = year+'-'+key.split('-')[1];
                    if (ssMap.get(a.id) != null && ssMap.get(a.id).get(sskey)!=null){
                        for (Sales_Summary__c ss: ssMap.get(a.id).get(sskey)){
                            if (ss.Week__c <= ssMaxPYWeek) {
                                // GJK: 01/07/15 - Discard any Sales Summary Data that is for Prior Fiscal Year and Week > Maximum Week for this Fiscal Year
                                //if(!ss.OLP__c && ss.Total_Sales_Amount__c != null && ss.Sales_Discount__c != null){
                                if(ss.Total_Sales_Amount__c != null && ss.Sales_Discount__c != null){
                                    //a.PY_Rolling_Sales_Last_3_Periods__c += (ss.Total_Sales_Amount__c - ss.Sales_Discount__c);

                                    Direct_DM_Sales_Amount  = getDirectDMSalesAmount(ss, metricTypesSet, ssPriorYear, ssMaxPYWeek);
                                    a.PY_Rolling_Sales_Last_3_Periods__c += Direct_DM_Sales_Amount;
                                }
                            }
                        }
                    }
                }
            }

            // GJK: 02/02/16 - Mark the Account Sales Summarization as completed.
            a.Sales_Summarization_Completed__c  = true; 

            try{
                updatedAccounts.add(a);
            }
            catch (Exception ex){
                //do nothing
            }
        }

        // GJK: 02/02/16 - Modified the Update clause to use Databaase.Update instead.
        //                  Also added the second parameter, allOrNone, to false so that succesful
        //                  updates to the batched records can be commited.
        //update updatedAccounts;
        Database.update(updatedAccounts, false);

        /*
        //  GJK: 01/05/16 - Making the folloing changes for Performance Enhancements:
        //                  Commenting out the code that updates Sales_Summary__c
        update updSalesSummaryList;
        */
    }

    global void finish(Database.BatchableContext bc) {
        
        AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,TotalJobItems, CreatedBy.Email FROM AsyncApexJob WHERE Id = :BC.getJobId()];
        
         if(a.status == 'Failed') {
            Job_Failure_Notification_Email__c myemail = Job_Failure_Notification_Email__c.getValues('Email Id');
            String myEmails = myemail.Notification_Email__c;
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {myemails};
            mail.setToAddresses(toAddresses);
            mail.setSubject('Apex Sharing Recalculation ' + a.Status);
            mail.setPlainTextBody('The AccountSalesSummaryBatch job has Failed');
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
         }
        
         if(a.status == 'Completed' || a.TotalJobItems == 0 || a.NumberOfErrors > 0) {
            Job_Failure_Notification_Email__c myemail = Job_Failure_Notification_Email__c.getValues('Email Id');
            String myEmails = myemail.Notification_Email__c;
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {myemails};
            mail.setToAddresses(toAddresses);
            
            if(test.isrunningtest()) {
                mail.setSubject('Test execution mail Apex Sharing Recalculation ' + a.Status);
            }else {
                mail.setSubject('Apex Sharing Recalculation ' + a.Status);
            }
            
            mail.setPlainTextBody('The AccountSalesSummaryBatch job processed ' + a.TotalJobItems + ' batches with '+ a.NumberOfErrors + ' failures.');
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
         }   
          
         if(a.status !='Failed' && a.TotalJobItems != 0 && a.TotalJobItems != a.NumberOfErrors && !test.isrunningtest()) {
            Sales_Summary_Batch_Size_1_Run__c salesBatchSettings = Sales_Summary_Batch_Size_1_Run__c.getOrgDefaults();
            String alreadyRunBatch = salesBatchSettings.Batch_Run__c;
            System.debug('alreadyRunBatch=======>'+alreadyRunBatch);
            if(alreadyRunBatch == 'No') {
                salesBatchSettings.Batch_Run__c = 'Yes'; update salesBatchSettings;
                AccountSalesSummaryBatch salesBatch = new AccountSalesSummaryBatch();
                Database.executeBatch(salesBatch,1);         
            }else {
                salesBatchSettings.Batch_Run__c = 'No'; update salesBatchSettings;
                AccountPreviousRankingCleanupBatch obj = new AccountPreviousRankingCleanupBatch();
                Database.executeBatch(obj);
             }                    
         }           
    }
}