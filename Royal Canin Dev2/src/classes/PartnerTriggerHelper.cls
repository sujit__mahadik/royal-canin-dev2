public with sharing class PartnerTriggerHelper {

	private static Map<string, string> rolesToReverseRoles;

	static {
		rolesToReverseRoles = new Map<string, string>();
		for(PartnerRole role : [SELECT Id, MasterLabel, ReverseRole FROM PartnerRole]) {
			rolesToReverseRoles.put(role.MasterLabel, role.ReverseRole);
		}
	}

	public static void createReversePartner(List<Partner__c> partners) {
		List<Partner__c> newPartners = new List<Partner__c>();
		Set<Id> partnerIds = new Set<Id>();
		Set<Id> accountIds = new Set<Id>();

		for(Partner__c partner : partners) {
			if(partner.ReversePartnerId__c == null) {
				newPartners.add(partner);
				partnerIds.add(partner.Id);
				accountIds.add(partner.AccountToId__c);
			}
		}

		//next line stops infinite loop
		partners = new List<Partner__c>(newPartners);
		List<Partner__c> reversePartners = new List<Partner__c>();
		Map<Id, Account> accountMap = new Map<Id, Account>([select id, Name from Account where id in : accountIds]);

		for(Partner__c partner : partners) {
			reversePartners.add(createPartner(partner, accountMap));
		}

		insert reversePartners;

		Map<Id, Partner__c> partnerMap = new Map<Id, Partner__c>();
		for(Partner__c partner : reversePartners) {
			partnerMap.put(partner.ReversePartnerId__c, partner);
		}

		List<Partner__c> partnersToUpdate = [select ReversePartnerId__c from Partner__c where id in : partnerIds];
		for(Partner__c partner : partnersToUpdate) {
			partner.ReversePartnerId__c = partnerMap.get(partner.Id).Id;
		}

		update partnersToUpdate;
	}

	public static Partner__c createPartner(Partner__c partner, Map<Id, Account> accountMap) {
		Partner__c reversePartner = new Partner__c();
		reversePartner.AccountFromId__c = partner.AccountToId__c;
		reversePartner.AccountToId__c = partner.AccountFromId__c;
		reversePartner.Role__c = rolesToReverseRoles.get(partner.Role__c);
		reversePartner.ReversePartnerId__c = partner.Id;
		return reversePartner;
	}
}