@isTest
private class OrderDetailsControllerTest {
    
        static testMethod void testOrderDetails2() {
             SetupTestData.createCustomSettings();
             
             List<Global_Parameters__c> globalParametersList = new List<Global_Parameters__c>();
             
             Global_Parameters__c dtendpoint = new Global_Parameters__c();
             dtendpoint.Name = 'RCOrderDetailAPIEndpoint';
             dtendpoint.Value__c = '12345678901234567890123456789012';//Must be exactly 32 characters
             
             globalParametersList.add(dtendpoint);
             
             Global_Parameters__c clientid = new Global_Parameters__c();
             clientid.Name = 'client_id';
             clientid.Value__c = '12345678901234567890123456789012';//Must be exactly 32 characters

             globalParametersList.add(clientid);
             
             Global_Parameters__c clientsecret = new Global_Parameters__c();
             clientsecret.Name = 'client_secret';
             clientsecret.Value__c = '12345678901234567890123456789012';//Must be exactly 32 characters
             
             globalParametersList.add(clientsecret);
             
             insert globalParametersList;
             
             Account testAccount = new Account(Name='Test Company Name123');
             testAccount.ShippingStreet = 'address';
             testAccount.ShippingCity = 'San Francisco';
             testAccount.ShippingState = 'CA';
             testAccount.ShippingPostalCode = '63301';
             testAccount.ShippingCountry = 'US';
             testAccount.Key_Account_Designation__c = 'OLP KEY ACCOUNT';
             testAccount.Client_Type_Description__c = 'ONLINE';
             
             insert testAccount;
             ApexPages.StandardController sc = new ApexPages.StandardController(testAccount);
             OrderDetailsController c = new OrderDetailsController(sc);
        }
        static testMethod void testOrderDetails() {
             SetupTestData.createCustomSettings();
             
             List<Global_Parameters__c> globalParametersList = new List<Global_Parameters__c>();
             
             Global_Parameters__c dtendpoint = new Global_Parameters__c();
             dtendpoint.Name = 'RCOrderDetailAPIEndpoint';
             dtendpoint.Value__c = '12345678901234567890123456789012';//Must be exactly 32 characters
             
             globalParametersList.add(dtendpoint);
             
             Global_Parameters__c clientid = new Global_Parameters__c();
             clientid.Name = 'client_id';
             clientid.Value__c = '12345678901234567890123456789012';//Must be exactly 32 characters

             globalParametersList.add(clientid);
             
             Global_Parameters__c clientsecret = new Global_Parameters__c();
             clientsecret.Name = 'client_secret';
             clientsecret.Value__c = '12345678901234567890123456789012';//Must be exactly 32 characters
             
             globalParametersList.add(clientsecret);
             
             insert globalParametersList;
             
             Account testAccount = new Account(Name='Test Company Name123');
             testAccount.ShipToCustomerNo__c = '1010';
             testAccount.ShippingStreet = 'address';
             testAccount.ShippingCity = 'San Francisco';
             testAccount.ShippingState = 'CA';
             testAccount.ShippingPostalCode = '63301';
             testAccount.ShippingCountry = 'US';
             testAccount.Key_Account_Designation__c = 'OLP KEY ACCOUNT';
             testAccount.Client_Type_Description__c = 'ONLINE';
             
             insert testAccount;
             ApexPages.StandardController sc = new ApexPages.StandardController(testAccount);
             OrderDetailsController c = new OrderDetailsController(sc);

             c.goback();
             c.resetNavButtons();
             
             PageReference pageRef = ApexPages.currentPage();
             pageRef.getParameters().put('dir', 'next');
             pageRef.getParameters().put('type', 'Direct');
             Test.setCurrentPage(pageRef);
             c.nav();
             c.nav_mobile();
             
             pageRef = ApexPages.currentPage();
             pageRef.getParameters().put('dir', 'last');
             pageRef.getParameters().put('type', 'Direct');
             Test.setCurrentPage(pageRef);
             c.nav();
             
             pageRef = ApexPages.currentPage();
             pageRef.getParameters().put('dir', 'next');
             pageRef.getParameters().put('type', 'OPH');
             Test.setCurrentPage(pageRef);
             c.nav();
             
             pageRef = ApexPages.currentPage();
             pageRef.getParameters().put('dir', 'last');
             pageRef.getParameters().put('type', 'OPH');
             Test.setCurrentPage(pageRef);
             c.nav();
        } 
}