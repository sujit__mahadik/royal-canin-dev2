global with sharing class OrderDetailsWrapper {

	global List<OrderHistoryDetails> OrderHistoryDetails {get;set;}

	global class OrderHistoryDetails {
		global String type {get;set;}
		global Integer count {get;set;}
		//to display paginated in main table
		global List<OrderList> OrderList {get;set;}
	}

	global class OrderList {
		global String billToCustomerNumber {get;set;}
		global String sellToCustomerNumber {get;set;}
		global String sellToCustomerName {get;set;}
		global String billToCustomerName {get;set;}
		//for view detail col
		global List<LineList> LineList {get;set;}
		global String OrderNumber {get;set;}
		global Double OrderTotal {get;set;}
		global String OrderSource {get;set;}
		global String CreatedBy {get;set;}
		global String RefCustomerName {get;set;}
		global String PaymentMethod {get;set;}
		global String ShipmentDate {get;set;}
		global String OrderDate {get;set;}
	}

	global class LineList {
		global String Item {get;set;}
		global Double DiscountAmount {get;set;}
		global String Type {get;set;}
		global String Classification {get;set;}
		global String Category {get;set;}
		global Double Tonnage {get;set;}
		global Double Amount {get;set;}
		global Double InvDiscountAmount {get;set;}
		global Double LineDiscountAmount {get;set;}
		global Integer Quantity {get;set;}
	}
	
	global static OrderDetailsWrapper parse(String json) {
		return (OrderDetailsWrapper) System.JSON.deserialize(json, OrderDetailsWrapper.class);
	}
}