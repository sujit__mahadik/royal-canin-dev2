/************************************************************************************
Version : 1.0
Name : test class to cover giic_UpdateFulfillmentByDC
Created Date : 21 Aug 2018
Function : Create Test data for test classes
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/

@isTest(seeAllData = false)
public class giic_Test_UpdateFByDC {
    
    @testSetup
    static void testDataCreation() {
        
     giic_Test_DataCreationUtility.insertWarehouse();
        giic_Test_DataCreationUtility.testSystemPolicyCreation2();
        giic_Test_DataCreationUtility.insertGlobalParametersCustomSetting();
        giic_Test_DataCreationUtility.insertConsumerAccount();
        
        giic_Test_DataCreationUtility.createCarrier();
        giic_Test_DataCreationUtility.insertUnitofMeasure();
        //giic_Test_DataCreationUtility.insertConsumerAccount_N(10);
        giic_Test_DataCreationUtility.insertProduct();
        
        List<gii__PriceBook__c> lstPriBook = [select id from gii__PriceBook__c where Name = 'Standard'];
        gii__PriceBook__c objPB;
        if(lstPriBook == null || lstPriBook.size() == 0){
            objPB = new gii__PriceBook__c();
            objPB.Name = 'Standard';
            insert objPB;
        }else{
            objPB = lstPriBook[0];
        }
        // update price book
        if(giic_Test_DataCreationUtility.lstAccount != null && giic_Test_DataCreationUtility.lstAccount.size() > 0 ){
            List<gii__AccountAdd__c> lstAccountRef = [select id,gii__Account__c,gii__Account__r.Name,gii__Account__r.Bill_To_Customer_ID__c,gii__Account__r.ShipToCustomerNo__c,gii__PriceBookName__c, gii__Account__r.giic_Carrier__c,gii__Account__r.giic_CustomerPriority__c, gii__Account__r.blocked__c,gii__Account__r.Check_if_you_are_Tax_Exempt__c, gii__Account__r.Resale_Sales_Tax_Cert_Expiration_Date__c,gii__Account__r.Payment_Method__c,gii__Account__r.Validated__c from gii__AccountAdd__c where gii__Account__c in : giic_Test_DataCreationUtility.lstAccount ];     
            
            for(gii__AccountAdd__c oAccRef : lstAccountRef){
                oAccRef.gii__PriceBookName__c = 'Standard';
            }

            update lstAccountRef;
            system.debug('lstAccountRef--->>>>> ' + lstAccountRef);
        }
        
        map<Id, gii__PriceBookEntry__c> mapPriceBook = new map<Id, gii__PriceBookEntry__c>([select id, gii__Product__c, gii__PriceBook__c, gii__UnitPrice__c from  gii__PriceBookEntry__c]);
        
        map<string, gii__PriceBookEntry__c> mapProductsHaingPricebook = new map<string, gii__PriceBookEntry__c>();
        
        //system.debug(' mapPriceBook-->>>> ' + mapPriceBook);
        system.assertEquals(mapPriceBook.size() > 0 ,true);
        for(String str: mapPriceBook.keySet()){
            mapProductsHaingPricebook.put(mapPriceBook.get(str).gii__Product__c,mapPriceBook.get(str));
        }
        
        List<gii__Product2Add__c> lstProd = new List<gii__Product2Add__c>();
        lstProd = [SELECT Id, gii__LotControlled__c, giic_ProductSKU__c, gii__ProductReference__c, gii__ProductReference__r.Name, gii__NonStock__c, gii__ProductReference__r.ProductCode, gii__ProductReference__r.Family,gii__ProductReference__r.Product_Category__c, gii__ProductReference__r.SKU__c, gii__ProductReference__r.Food_Type__c,gii__ProductReference__r.Weight_Unit__c,gii__Style__c, gii__ProductReference__r.Weight__c from gii__Product2Add__c ];
        List<gii__PriceBookEntry__c> lstPB  = new List<gii__PriceBookEntry__c>();
        if(lstProd != null && lstProd.size() > 0 ){
            for(gii__Product2Add__c oProdRef : lstProd ){
                if(mapProductsHaingPricebook != null && ! mapProductsHaingPricebook.containsKey(oProdRef.Id) ){
                    gii__PriceBookEntry__c ObjPriceBookEntry = new gii__PriceBookEntry__c();
                    ObjPriceBookEntry.gii__Product__c= oProdRef.Id;
                    ObjPriceBookEntry.gii__PriceBook__c = objPB.id;
                    ObjPriceBookEntry.gii__UnitPrice__c = 5;
                    lstPB.Add(ObjPriceBookEntry); 
                }
            }
        }
        
        if(lstPB != null && lstPB.size() > 0 ){
            insert lstPB;
        }
        
        map<Id, gii__PriceBookEntry__c> mapPriceBook11 = new map<Id, gii__PriceBookEntry__c>([select id, gii__Product__c, gii__PriceBook__c, gii__UnitPrice__c from  gii__PriceBookEntry__c]);
        
        //system.debug(' mapPriceBook11-->>>> ' + mapPriceBook11);
        system.assertEquals(mapPriceBook11.size()>0, true);
        
        giic_Test_DataCreationUtility.insertLocations();
        giic_Test_DataCreationUtility.insertProductInventory(1);
        giic_Test_DataCreationUtility.insertProductInventoryByLoc();
        giic_Test_DataCreationUtility.insertProductInventoryQTYDetails();
        giic_Test_DataCreationUtility.insertSalesOrder(); // 0 - open sales order , 1 - release sales order
        
        giic_Test_DataCreationUtility.lstSalesOrder[1].giic_TaxCal__c = false;
        giic_Test_DataCreationUtility.lstSalesOrder[1].giic_SOStatus__c = giic_Constants.SO_PICKED;
        update giic_Test_DataCreationUtility.lstSalesOrder[1];
        giic_Test_DataCreationUtility.insertSOLine(giic_Test_DataCreationUtility.lstSalesOrder);
        List<gii__AdditionalCharge__c> lstAdditionalCharge = giic_Test_DataCreationUtility.createAdditionalCharge();
        
        List<gii__SalesOrderAdditionalCharge__c> lstSOADC = new List<gii__SalesOrderAdditionalCharge__c>();
        if(giic_Test_DataCreationUtility.lstSalesOrder  != null && giic_Test_DataCreationUtility.lstSalesOrder.size() > 0){
            integer i=0;
            for(gii__SalesOrder__c oSOdr : giic_Test_DataCreationUtility.lstSalesOrder  ){
                gii__SalesOrderAdditionalCharge__c oSOADC = new gii__SalesOrderAdditionalCharge__c();
                oSOADC.gii__SalesOrder__c = oSOdr.Id;
                oSOADC.gii__AdditionalCharge__c = lstAdditionalCharge[math.mod(i,2)].Id;
                oSOADC.gii__UnitPrice__c = 4.99;
                lstSOADC.add(oSOADC);
                i++;
            }
            if(lstSOADC != null && lstSOADC.size() > 0 ){
                insert lstSOADC;
            }
        }
        
        giic_Test_DataCreationUtility.CreateAdminUser();   
       
        
    }
    
        public static void QueryData(){
        giic_Test_DataCreationUtility.lstSalesOrder = [ select id, name, gii__Account__c, giic_CustomerPriority__c, gii__ScheduledDate__c, gii__OrderDate__c, gii__Status__c, giic_SOStatus__c, gii__Warehouse__c, giic_OrderNo__c, giic_TaxCal__c, gii__PaymentMethod__c, gii__Carrier__c, giic_NewPickDate__c, gii__Released__c from gii__SalesOrder__c];
        
        giic_Test_DataCreationUtility.lstSalesOrderLine = [select id, gii__SalesOrder__c, gii__OrderQuantity__c , gii__ReservedQuantity__c , gii__Product__c , giic_LineStatus__c , gii__NonStockQuantity__c, gii__UnitPrice__c, gii__OriginalUnitPrice__c, giic_EDIPrice__c from gii__SalesOrderLine__c where gii__SalesOrder__c in : giic_Test_DataCreationUtility.lstSalesOrder];
        
        
        
        List<gii__AdditionalCharge__c> lstAdditionalCharge = [select id, name, giic_AdditionalChargeCode__c  , gii__UnitPrice__c, gii__Description__c  from gii__AdditionalCharge__c ];
        
        List<gii__SalesOrderAdditionalCharge__c> lstSalesOrderADC = [select id, name,gii__SalesOrder__c, gii__AdditionalCharge__c from gii__SalesOrderAdditionalCharge__c where gii__AdditionalCharge__c in : lstAdditionalCharge AND gii__SalesOrder__c in :  giic_Test_DataCreationUtility.lstSalesOrder ];
    }
    
     private static testMethod void test_API() {
          User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
          System.Runas(u)        
        {
            
          QueryData();
        giic_Test_DataCreationUtility.createPM_RewardPointsHavingHighConsumptionSeq();
        
        system.assertEquals(giic_Test_DataCreationUtility.lstSalesOrder.size()>0, true);
        //system.debug('giic_Test_DataCreationUtility.lstSalesOrder--->>>>> ' + giic_Test_DataCreationUtility.lstSalesOrder);
        system.assertEquals(giic_Test_DataCreationUtility.lstSalesOrderLine.size()>0, true);
        //system.debug('giic_Test_DataCreationUtility.lstSalesOrderLine--->>>>> ' + giic_Test_DataCreationUtility.lstSalesOrderLine);
        
        List<gii__WarehouseShippingOrderStaging__c> listWSOS = new List<gii__WarehouseShippingOrderStaging__c>();
        if(giic_Test_DataCreationUtility.lstSalesOrder != null && giic_Test_DataCreationUtility.lstSalesOrder.size() > 0 ){
            for(gii__SalesOrder__c oSO : giic_Test_DataCreationUtility.lstSalesOrder){
                gii__WarehouseShippingOrderStaging__c obj = new  gii__WarehouseShippingOrderStaging__c(giic_SalesOrder__c = oSO.id);
                listWSOS.add(obj);
            }
        }
        insert listWSOS;
        map<string, string> mapSOIdVsWHId = new map<string, string>();
        for(gii__WarehouseShippingOrderStaging__c oWSOS : listWSOS ){
            mapSOIdVsWHId.put(oWSOS.giic_SalesOrder__c,oWSOS.Id);
        }
        
        List<gii__WarehouseShippingOrderLinesStaging__c> lstWSOSL = new List<gii__WarehouseShippingOrderLinesStaging__c>();
        for(gii__SalesOrderLine__c oSOL : giic_Test_DataCreationUtility.lstSalesOrderLine)
        {
            gii__WarehouseShippingOrderLinesStaging__c obj = new  gii__WarehouseShippingOrderLinesStaging__c(giic_WHShippingOrderStaging__c = mapSOIdVsWHId.get(oSOL.gii__SalesOrder__c), giic_SalesOrderLine__c = oSOL.id, giic_Product__c = oSOL.gii__Product__c);
            lstWSOSL.add(obj);
        }
        if(lstWSOSL != null && lstWSOSL.size() >0 ){
            insert lstWSOSL;
        }
        
        //System.debug('Fulfillment record:::' + listWSOS);
        system.assertEquals(listWSOS.size()>0, true);
        
        Map<String, String> requestMap = new Map<String, String>();
        requestMap.put('fulfillmentId', String.valueOf(listWSOS[0].id));
        requestMap.put('Status', giic_Constants.REJECTED);
        
        String JsonMsg=JSON.serialize(requestMap);
        Test.setMock(WebServiceMock.class, new giic_Test_GenericRCCCServiceMock());
        Test.startTest();
            giic_UpdateFulfillmentByDC.processPostRequest(String.valueOf(listWSOS[0].id), 'rejectedfalse');
            giic_UpdateFulfillmentByDC.processPostRequest(null, giic_Constants.REJECTED);
            giic_UpdateFulfillmentByDC.processPostRequest(String.valueOf(listWSOS[0].id), giic_Constants.REJECTED);
            giic_UpdateFulfillmentByDC.processPostRequest(String.valueOf(listWSOS[0].id), giic_Constants.ACCEPTED);
            giic_UpdateFulfillmentByDC.processPostRequest(String.valueOf(listWSOS[0].id), giic_Constants.SUBMITTED);
            giic_UpdateFulfillmentByDC.processPostRequest(String.valueOf(listWSOS[0].id), giic_Constants.SUBMITTED_WITH_ERROR);
             if(!giic_Test_DataCreationUtility.lstSalesOrder.isEmpty())giic_UpdateFulfillmentByDC.processPostRequest(String.valueOf(giic_Test_DataCreationUtility.lstSalesOrder[0].id), giic_Constants.SUBMITTED);
        Test.stopTest();
     }
}
}