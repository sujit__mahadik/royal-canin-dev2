/**************************************************
Name    : giic_UomCommonUtility
Author  : Shivdeep Gupta
Purpose : utility class for UOM Conversion & glovia Api call 
Created Date: 10 OCT 2018
*****************************************************/
public without sharing class giic_UomCommonUtility {    
    
    private static Boolean isRunDML = true;
    //Method to create the Key product reference id + ' ' + source UoM record id + ' ' + target UoM record id 
    public static String createProductUoMUniqueKey(Id idProductRef, Id idSourceUoM, Id idTargetUoM)
    {
        String strProdUoMKey = '';
        strProdUoMKey = idProductRef + ' ' + idSourceUoM + ' ' + idTargetUoM;
        return strProdUoMKey;
    } 
    
    /************************
    *   Method  : ConvertUOM
    *   Purpose : Method to convert the UOM values (UoM & Decimal)
    *   DATE : 10 OCT 2018
    * ***************/
   public static decimal ConvertUOM(Id idProductRef, Id idSourceUoM, Id idTargetUoM, decimal quantityForConvert,Map<String, gii__ProductUnitofMeasureConversion__c> mapProductUnitofMeasureConversion)
    {
        gii.UnitofMeasureHelper.ProductUnitofMeasureConversionDetail inputPUoMConversionObject = new  gii.UnitofMeasureHelper.ProductUnitofMeasureConversionDetail();
        gii.UnitofMeasureHelper.ProductUnitofMeasureConversionDetail outputPUoMConversionObject = new  gii.UnitofMeasureHelper.ProductUnitofMeasureConversionDetail();
        inputPUoMConversionObject.productId = idProductRef;
        inputPUoMConversionObject.sourceUnitofMeasureId = idSourceUoM;
        inputPUoMConversionObject.targetUnitofMeasureId = idTargetUoM;
        inputPUoMConversionObject.quantityToBeConverted = quantityForConvert;
        inputPUoMConversionObject.roundingDecimals = null;
        outputPUoMConversionObject =  gii.UnitofMeasureHelper.getConversionFactorandConvertedQuantity(mapProductUnitofMeasureConversion, inputPUoMConversionObject);
        return outputPUoMConversionObject.convertedQuantity;
   }
   
    /************************
    *   Method  : ConvertUOM
    *   Purpose : This Method will return converted Quantity & reverse converted Quantity
    *   DATE : 10 OCT 2018
    * ***************/
    public static map<String,decimal> ConvertUoMBulk(list<UoMConversion> lstParameter, Boolean isDML){
        isRunDML = isDML;
        return ConvertUoMBulk(lstParameter);
    }
    
    /************************
    *   Method  : ConvertUOM
    *   Purpose : This Method will return converted Quantity & reverse converted Quantity
    *   DATE : 10 OCT 2018
    * ***************/
    public static map<String,decimal> ConvertUoMBulk(list<UoMConversion> lstParameter)
    {
        Set<String> setProductUMConversionUniqueIds = new Set<String>();
        map<String,decimal> uniqueIdToDecimal = new  map<String,decimal>();
        
        Map<String, gii__ProductUnitofMeasureConversion__c> mapProductUnitofMeasureConversion = new Map<String, gii__ProductUnitofMeasureConversion__c>();
        for(UoMConversion obj: lstParameter)
        {
           if(obj.ProductId != null && obj.SourceUoMId != null && obj.TargetUoMId != null){
               setProductUMConversionUniqueIds.add(createProductUoMUniqueKey(obj.ProductId,obj.SourceUoMId,obj.TargetUoMId));
               setProductUMConversionUniqueIds.add(createProductUoMUniqueKey(obj.ProductId,obj.TargetUoMId,obj.SourceUoMId));
           }
        }
        
        //Get all conversion and reversion factor 
        mapProductUnitofMeasureConversion = gii.UnitofMeasureHelper.getMapProductUnitofMeasureConversion(setProductUMConversionUniqueIds);
        
        //Set for remianing products for which ProductUnitOfMeasures Conversion records not found
        Set<Id> remainingProducts = new Set<Id>();
        Set<Id> allRemainingUoms = new Set<Id>();
        for(String uniqueId: setProductUMConversionUniqueIds){
            String[] ids = uniqueId.split(' ');
            String uniqueId1 = createProductUoMUniqueKey(ids[0],ids[2],ids[1]);
            if(!mapProductUnitofMeasureConversion.containsKey(uniqueId) && !mapProductUnitofMeasureConversion.containsKey(uniqueId1) && ids[1] != ids[2]){
                remainingProducts.add(ids[0]);
                allRemainingUoms.add(ids[1]);
                allRemainingUoms.add(ids[2]);
                
            }
        }
        
        //fetching all ProductUnitOfMeasures Conversion records for calulcationg convesion factor for no direct relations
        List<gii__ProductUnitofMeasureConversion__c> PuomList = [SELECT id, gii__Product__c, gii__UnitofMeasureIn__c, gii__UnitofMeasureOut__c, gii__ConversionFactor__c, gii__UniqueId__c, gii__ReverseConversionFactor__c FROM gii__ProductUnitofMeasureConversion__c where gii__Product__c in: remainingProducts AND (gii__UnitofMeasureOut__c in: allRemainingUoms OR gii__UnitofMeasureIn__c in: allRemainingUoms)];
        
        Map<String, decimal> uniqueIdConversionMap = new Map<String, decimal>();
        
        List<gii__ProductUnitofMeasureConversion__c> puomToInsert = new List<gii__ProductUnitofMeasureConversion__c>();
        
        Set<String> processedUniqueIds = new Set<String>();
        //calcuulating conversion fcator for no direct relation Uoms    
        for(gii__ProductUnitofMeasureConversion__c puom: PuomList){
            for(gii__ProductUnitofMeasureConversion__c puomi: PuomList){
                if(puom.Id != puomi.Id && puom.gii__Product__c == puomi.gii__Product__c){
                    
                    gii__ProductUnitofMeasureConversion__c newPuom;
                    decimal convF;
                    String uid;
                    String ruid;
                    if(puom.gii__UnitofMeasureIn__c == puomi.gii__UnitofMeasureIn__c){
                        convF = puomi.gii__ConversionFactor__c/puom.gii__ConversionFactor__c;
                        
                        uid = createProductUoMUniqueKey(puom.gii__Product__c,puom.gii__UnitofMeasureOut__c, puomi.gii__UnitofMeasureOut__c);
                        ruid = createProductUoMUniqueKey(puom.gii__Product__c,puomi.gii__UnitofMeasureOut__c, puom.gii__UnitofMeasureOut__c);

                        if(processedUniqueIds.contains(uid) || processedUniqueIds.contains(ruid))
                            continue;
                        if(setProductUMConversionUniqueIds.contains(uid))    
                            newPuom = new gii__ProductUnitofMeasureConversion__c(gii__Product__c= puom.gii__Product__c, gii__UnitofMeasureIn__c= puom.gii__UnitofMeasureOut__c, gii__UnitofMeasureOut__c = puomi.gii__UnitofMeasureOut__c, gii__QuantityIn__c= 1, gii__QuantityOut__c= convF);
                    }
                    else if(puom.gii__UnitofMeasureOut__c == puomi.gii__UnitofMeasureOut__c){
                        convF = 1/(puom.gii__ConversionFactor__c/puomi.gii__ConversionFactor__c);
                        
                        uid = createProductUoMUniqueKey(puom.gii__Product__c,puom.gii__UnitofMeasureIn__c, puomi.gii__UnitofMeasureIn__c);
                        ruid = createProductUoMUniqueKey(puom.gii__Product__c,puomi.gii__UnitofMeasureIn__c, puom.gii__UnitofMeasureIn__c);
                        
                        if(processedUniqueIds.contains(uid) || processedUniqueIds.contains(ruid))
                            continue;
                        if(setProductUMConversionUniqueIds.contains(uid))    
                            newPuom = new gii__ProductUnitofMeasureConversion__c(gii__Product__c= puom.gii__Product__c, gii__UnitofMeasureIn__c= puom.gii__UnitofMeasureIn__c, gii__UnitofMeasureOut__c = puomi.gii__UnitofMeasureIn__c, gii__QuantityIn__c= 1, gii__QuantityOut__c= convF);
                    }
                    else if(puom.gii__UnitofMeasureIn__c == puomi.gii__UnitofMeasureOut__c){
                        convF = puom.gii__ConversionFactor__c*puomi.gii__ConversionFactor__c;
                        
                        uid = createProductUoMUniqueKey(puom.gii__Product__c,puom.gii__UnitofMeasureOut__c, puomi.gii__UnitofMeasureIn__c);
                        ruid = createProductUoMUniqueKey(puom.gii__Product__c, puomi.gii__UnitofMeasureIn__c,puom.gii__UnitofMeasureOut__c);
                        
                        if(processedUniqueIds.contains(uid) || processedUniqueIds.contains(ruid))
                            continue;
                        if(setProductUMConversionUniqueIds.contains(uid))    
                            newPuom = new gii__ProductUnitofMeasureConversion__c(gii__Product__c= puom.gii__Product__c, gii__UnitofMeasureIn__c= puom.gii__UnitofMeasureOut__c, gii__UnitofMeasureOut__c = puomi.gii__UnitofMeasureIn__c, gii__QuantityIn__c= 1, gii__QuantityOut__c= convF);
                    }
                    else if(puom.gii__UnitofMeasureOut__c == puomi.gii__UnitofMeasureIn__c){
                        convF = puom.gii__ConversionFactor__c*puomi.gii__ConversionFactor__c;
                        
                        uid = createProductUoMUniqueKey(puom.gii__Product__c,puom.gii__UnitofMeasureIn__c, puomi.gii__UnitofMeasureOut__c);
                        ruid = createProductUoMUniqueKey(puom.gii__Product__c,puomi.gii__UnitofMeasureOut__c,puom.gii__UnitofMeasureIn__c);
                        
                        if(processedUniqueIds.contains(uid) || processedUniqueIds.contains(ruid))
                            continue;
                        if(setProductUMConversionUniqueIds.contains(uid))    
                            newPuom = new gii__ProductUnitofMeasureConversion__c(gii__Product__c= puom.gii__Product__c, gii__UnitofMeasureIn__c= puom.gii__UnitofMeasureIn__c, gii__UnitofMeasureOut__c = puomi.gii__UnitofMeasureOut__c, gii__QuantityIn__c= 1, gii__QuantityOut__c= convF);
                    }
                    if(convF !=  null){
                        uniqueIdConversionMap.put(uid, convF);
                        uniqueIdConversionMap.put(ruid, 1/convF);
                        processedUniqueIds.add(uid);
                        processedUniqueIds.add(ruid);
                    }
                    
                    // Inserting new Product unit Of Measure Records
                    if(newPuom != null && isRunDML)
                        puomToInsert.add(newPuom);
                }
            }
        }
        
        if(puomToInsert.size()>0)
            insert puomToInsert;
          
        //do calculation using glovia api
        for(UoMConversion obj: lstParameter){
            gii.UnitofMeasureHelper.ProductUnitofMeasureConversionDetail inputPUoMConversionObject = new  gii.UnitofMeasureHelper.ProductUnitofMeasureConversionDetail();
            gii.UnitofMeasureHelper.ProductUnitofMeasureConversionDetail outputPUoMConversionObject = new  gii.UnitofMeasureHelper.ProductUnitofMeasureConversionDetail();
            inputPUoMConversionObject.productId = obj.ProductId;
            inputPUoMConversionObject.sourceUnitofMeasureId = obj.SourceUoMId;
            inputPUoMConversionObject.targetUnitofMeasureId = obj.TargetUoMId;
            inputPUoMConversionObject.quantityToBeConverted = obj.qtyNeedToBeConvert;
            if(obj.qtyNeedToBeConvert == 1)
                inputPUoMConversionObject.roundingDecimals = Integer.valueOf(Label.giic_Coversion_Rounding_Decimals);
            else
                inputPUoMConversionObject.roundingDecimals = null;
            String uniqueId = createProductUoMUniqueKey(obj.ProductId,obj.SourceUoMId,obj.TargetUoMId);
            String uniqueId1 = createProductUoMUniqueKey(obj.ProductId,obj.TargetUoMId,obj.SourceUoMId);
            if(uniqueIdConversionMap.containsKey(uniqueId)){
                uniqueIdToDecimal.put(uniqueId, obj.qtyNeedToBeConvert*uniqueIdConversionMap.get(uniqueId));
                uniqueIdToDecimal.put(uniqueId1, (1/uniqueIdConversionMap.get(uniqueId))*obj.qtyNeedToBeConvert);
            }
            else{
                outputPUoMConversionObject =  gii.UnitofMeasureHelper.getConversionFactorandConvertedQuantity(mapProductUnitofMeasureConversion, inputPUoMConversionObject);
                uniqueIdToDecimal.put(uniqueId, outputPUoMConversionObject.convertedQuantity);                
                inputPUoMConversionObject.sourceUnitofMeasureId = obj.TargetUoMId;
                inputPUoMConversionObject.targetUnitofMeasureId = obj.SourceUoMId;
                outputPUoMConversionObject =  gii.UnitofMeasureHelper.getConversionFactorandConvertedQuantity(mapProductUnitofMeasureConversion, inputPUoMConversionObject);
                uniqueIdToDecimal.put(uniqueId1, outputPUoMConversionObject.convertedQuantity);
            }
        }

        return uniqueIdToDecimal;
    }

      
   
    public class UoMConversion
    {
        public Id SourceUoMId{get;set;}
        public Id TargetUoMId{get;set;}
        public Id ProductId{get;set;}
        public decimal qtyNeedToBeConvert{get;set;}
    
    }
}