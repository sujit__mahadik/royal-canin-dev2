/************************************************************************************
Version : 1.0
Created Date : 19 Sep 2018
Function : Controller for giic_ReleaseSalesOrder page.
Author : Abhishek Tripathi
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/

public class giic_ReleaseSalesOrderController {
    public gii__SystemPolicy__c objSystemPolicy;
    public List<BatchJob> batchJobs;
    private Set<Id> batchIds = new Set<Id>();
    public Boolean displayProcessedBatches {get;set;}
    public Id syPoID {get;set;}
    
    public giic_ReleaseSalesOrderController(ApexPages.StandardController stdController){
        objSystemPolicy=(gii__SystemPolicy__c)stdController.getRecord();
        displayProcessedBatches =true;
        syPoID = objSystemPolicy.Id;
    }
    /*
* Method name : releaseSO
* Description : execute to release the SO
* Return Type : PageReference
* Parameter : 
*/
    public PageReference releaseSO(){
        try{
            Set<String> completionStatuses = new Set<String>{'Completed', 'Aborted', 'Failed'};
            Map<Id, AsyncApexJob> asyncJMap = new Map<Id, AsyncApexJob>([Select Id From AsyncApexJob WHERE JobType='BatchApex' AND ApexClass.Name = : Label.giic_ReleaseSOBatchName AND Status NOT IN : completionStatuses ]);
            if(asyncJMap.size() == 0){
               Id batchId=Database.executeBatch(new giic_SOReleaseBatch());
               batchIds.add(batchId);
            }else{
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, Label.giic_BatchesAlreadyRunning));
                batchIds=asyncJMap.keySet();
            }
        }catch(DmlException e){
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR,e.getMessage()));
            return null;
        }
        return null;
    }
    
    /*
* Method name : getBatchJobs
* Description : poll for status of batch jobs
* Return Type : List<BatchJob>
* Parameter : none
*/    
    public List<BatchJob> getBatchJobs(){
        //Create new list of BatchJobs, a wrapper class that includes the job and percent complete.
        batchJobs = new List<BatchJob>();
        
        map<string,string> bgColorMap=new map<string,string>();
        map<string,string> fgColorMap=new map<string,string>();
         for(giic_BatchColorCode__mdt colorCode : [SELECT Id, DeveloperName, QualifiedApiName, giic_fgColor__c, giic_bgColor__c 
                                                  FROM giic_BatchColorCode__mdt]){
            bgColorMap.put(colorCode.DeveloperName, colorCode.giic_bgColor__c);
            fgColorMap.put(colorCode.DeveloperName, colorCode.giic_fgColor__c);
          }

        //Query the Batch apex jobs
        for(AsyncApexJob asyncJ : [select TotalJobItems, Status, NumberOfErrors, MethodName, JobType, JobItemsProcessed, ExtendedStatus, Id, CreatedDate, CreatedById, CompletedDate, ApexClassId, ApexClass.Name From AsyncApexJob WHERE id IN : batchIds ]){
            Double itemsProcessed = asyncJ.JobItemsProcessed;
            Double totalItems = asyncJ.TotalJobItems;
            
            BatchJob bJob = new BatchJob();
            bJob.job = asyncJ;
            
            //Determine the pecent complete based on the number of batches complete
            if(totalItems == 0){
                //A little check here as we don't want to divide by 0.
                bJob.percentComplete = 0;
            }else{
                bJob.percentComplete = ((itemsProcessed  / totalItems) * 100.0).intValue();
            }
            bJob.bgStatusColor=bgColorMap.get(asyncJ.Status);
            bJob.fgStatusColor=fgColorMap.get(asyncJ.Status);
            if(asyncJ.status == 'Completed'){
                bJob.percentComplete = 100;
                bJob.bgStatusColor=bgColorMap.get(asyncJ.Status);
                bJob.fgStatusColor=fgColorMap.get(asyncJ.Status);
            }
            batchJobs.add(bJob);
        }
        return batchJobs;
    }
    
     /*
* Method name : scheduleBatch
* Description : Schedules the batch through dynamic scheduling.
* Return Type : none
* Parameter : 
*/
 public void scheduleBatch(){
     displayProcessedBatches = false;
     
     try{
         List<gii__SystemPolicy__c> Sp = new List<gii__SystemPolicy__c>([Select Id, giic_JobTime__c from gii__SystemPolicy__c where Id= :objSystemPolicy.Id]);
         if(Sp[0].giic_JobTime__c!=null){ // There can only be single record in system policy.
           giic_CommonUtility.scheduleBatchJobs(Integer.valueOf(Sp[0].giic_JobTime__c), new giic_SOReleaseBatch(), Label.giic_ReleaseSOBatchName, true);
         }else{
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, Label.giic_EnterBatchJobTimeErrror));
        }
     }catch(Exception e){
         ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR,e.getMessage()));
     }
   
 }
    
    
    //This is the wrapper class the includes the job itself and a value for the percent complete
    public Class BatchJob{
        public AsyncApexJob job {get; set;}
        public Integer percentComplete {get; set;}
        public string bgStatusColor {get;set;}
        public string fgStatusColor {get;set;}
        
        public BatchJob(){
            this.job=null;
            this.percentComplete=0;
            bgStatusColor='';
            fgStatusColor='';
        }
    }
}