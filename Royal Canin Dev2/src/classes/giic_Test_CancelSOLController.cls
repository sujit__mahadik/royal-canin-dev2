/************************************************************************************
Version : 1.0
Created Date : 27-Aug-2018
Function : Test class for giic_CancelSOLController
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/
@isTest(seeAllData=false)
private class giic_Test_CancelSOLController {
    @testSetup
    static void setup(){
        List<Account> testConsumerAccountList = giic_Test_DataCreationUtility.insertConsumerAccount();
        List<gii__Warehouse__c> testWareList = giic_Test_DataCreationUtility.insertWarehouse();
        SetupTestData.createCustomSettings();
        giic_Test_DataCreationUtility.testSystemPolicyCreation();
        List<gii__AccountAdd__c> testAccRefList = giic_Test_DataCreationUtility.getAccountReference(testConsumerAccountList);
        Database.insert(testAccRefList);
        List<gii__Product2Add__c> testProductList = giic_Test_DataCreationUtility.insertProduct();
        List<gii__SalesOrder__c> testSOList = giic_Test_DataCreationUtility.insertSalesOrder(testConsumerAccountList[0].Id, testAccRefList[0].Id);
        List<gii__SalesOrderLine__c> testSOLine = giic_Test_DataCreationUtility.insertSOLine(testSOList[0].Id, testProductList[0].Id);
    }
    
    @isTest public static void cancelSalesOrderLinesPositiveTest(){
        
        Account TestAcc = [SELECT Id, Name FROM Account WHERE Name LIKE 'Test%' LIMIT 1];
        gii__SalesOrder__c testSO = [SELECT Id, giic_SOStatus__c, gii__Released__c, gii__Account__c FROM gii__SalesOrder__c WHERE gii__Account__c =: TestAcc.Id LIMIT 1];
        List<gii__SalesOrderLine__c> testStdSetCont = [SELECT Id, gii__SalesOrder__c, gii__ReservedQuantity__c, gii__CancelDate__c, gii__CancelledQuantity__c, gii__CancelReason__c  FROM gii__SalesOrderLine__c LIMIT 10];
        testStdSetCont[0].giic_LineStatus__c = giic_Constants.SOL_BACKORDERSTATUS;
        update testStdSetCont[0];
        Test.startTest();
        Test.setCurrentPage(Page.giic_CancelSalesORderLines);
        ApexPages.StandardSetController cont = new ApexPages.StandardSetController(testStdSetCont);
        cont.setSelected(testStdSetCont);
        giic_CancelSOLController obj = new giic_CancelSOLController(cont);
        obj.cancelSalesOrderLines(); //Reserved qty  = 0.0       
        Test.stopTest();
        
        System.assertEquals(0.0, testStdSetCont[0].gii__ReservedQuantity__c);
    }
    
    
    @isTest public static void cancelSalesOrderLines_SO_NotReleased(){
        // this case when gii__Released__c = false
        Account TestAcc = [SELECT Id, Name FROM Account WHERE Name LIKE 'Test%' LIMIT 1];
        gii__SalesOrder__c testSO = [SELECT Id, giic_SOStatus__c, gii__Released__c, gii__Account__c FROM gii__SalesOrder__c WHERE gii__Account__c =: TestAcc.Id LIMIT 1];
        testSO.gii__Released__c = false;
        testSO.giic_SOStatus__c = giic_Constants.SOSTATUSOPEN;
        update testSO;
        List<gii__SalesOrderLine__c> testStdSetCont = [SELECT Id, gii__SalesOrder__c, gii__ReservedQuantity__c, gii__CancelDate__c, gii__CancelledQuantity__c, gii__CancelReason__c  FROM gii__SalesOrderLine__c LIMIT 10];
        Test.startTest();
        Test.setCurrentPage(Page.giic_CancelSalesORderLines);
        ApexPages.StandardSetController cont = new ApexPages.StandardSetController(testStdSetCont);
        cont.setSelected(testStdSetCont);
        giic_CancelSOLController obj = new giic_CancelSOLController(cont);
        obj.cancelSalesOrderLines(); //Reserved qty  = 0.0       
        Test.stopTest();
        
    }
    
    @isTest public static void cancelSOL_NoSOLSelectedToCancel(){
        // click on cancell button without selecting any SOL
        Account TestAcc = [SELECT Id, Name FROM Account WHERE Name LIKE 'Test%' LIMIT 1];
        gii__SalesOrder__c testSO = [SELECT Id, giic_SOStatus__c, gii__Released__c, gii__Account__c FROM gii__SalesOrder__c WHERE gii__Account__c =: TestAcc.Id LIMIT 1];
        // List<gii__SalesOrderLine__c> testStdSetCont = [SELECT Id, gii__SalesOrder__c, gii__ReservedQuantity__c, gii__CancelDate__c, gii__CancelledQuantity__c, gii__CancelReason__c  FROM gii__SalesOrderLine__c LIMIT 10];
        List<gii__SalesOrderLine__c> testStdSetCont = new List<gii__SalesOrderLine__c>();
        Test.startTest();
        Test.setCurrentPage(Page.giic_CancelSalesORderLines);
        ApexPages.StandardSetController cont = new ApexPages.StandardSetController(testStdSetCont);
        cont.setSelected(testStdSetCont);
        giic_CancelSOLController obj = new giic_CancelSOLController(cont);
        obj.cancelSalesOrderLines(); //Reserved qty  = 0.0       
        Test.stopTest();
        
    }
    
    
    
    @isTest public static void cancelSalesOrderLinesNegativeTest(){
        
        Account TestAcc = [SELECT Id, Name FROM Account WHERE Name LIKE 'Test%' LIMIT 1];
        List<gii__SalesOrder__c> lstSO = [SELECT Id, giic_SOStatus__c,gii__Released__c, gii__Account__c FROM gii__SalesOrder__c WHERE gii__Account__c =: TestAcc.Id ];
        
        
        gii__SalesOrder__c testSO = [SELECT Id, giic_SOStatus__c,gii__Released__c, gii__Account__c FROM gii__SalesOrder__c WHERE gii__Account__c =: TestAcc.Id LIMIT 1];
        List<gii__SalesOrderLine__c> testStdSetCont = [SELECT Id, gii__SalesOrder__c, gii__ReservedQuantity__c, gii__CancelDate__c, gii__CancelledQuantity__c, gii__CancelReason__c  FROM gii__SalesOrderLine__c  LIMIT 10];
        
        
        for(gii__SalesOrderLine__c line : testStdSetCont){
            line.gii__ReservedQuantity__c = 1.0;
        }
        Database.update(testStdSetCont);
        Test.startTest();
            Test.setCurrentPage(Page.giic_CancelSalesORderLines);
            ApexPages.StandardSetController cont = new ApexPages.StandardSetController(testStdSetCont);
            cont.setSelected(testStdSetCont);
            giic_CancelSOLController obj = new giic_CancelSOLController(cont);
            obj.cancelSalesOrderLines(); //Reserved qty  > 0.0       
        Test.stopTest();
        
        System.assertNotEquals(0.0, testStdSetCont[0].gii__ReservedQuantity__c);
    }
    
    @isTest public static void testCancelSalesOrderLinesFew(){
        
        Account TestAcc = [SELECT Id, Name FROM Account WHERE Name LIKE 'Test%' LIMIT 1];
        gii__SalesOrder__c testSO = [SELECT Id, giic_SOStatus__c, gii__Account__c, giic_OrderLineCancelled__c FROM gii__SalesOrder__c WHERE gii__Account__c =: TestAcc.Id LIMIT 1];
        List<gii__SalesOrderLine__c> testStdSetCont = [SELECT Id, gii__SalesOrder__c, gii__ReservedQuantity__c, gii__CancelDate__c, gii__CancelledQuantity__c, gii__CancelReason__c
                                                        FROM gii__SalesOrderLine__c 
                                                        WHERE gii__SalesOrder__c =: testSO.Id];
        
        Test.startTest();
            Test.setCurrentPage(Page.giic_CancelSalesORderLines);
            if(testStdSetCont.size() > 0){
                List<gii__SalesOrderLine__c> testStdSetCont2 = new List<gii__SalesOrderLine__c>();
                for(gii__SalesOrderLine__c soL : testStdSetCont){
                    testStdSetCont2.add(soL);
                    if(testStdSetCont2.size()>0) break;
                }
                ApexPages.StandardSetController cont = new ApexPages.StandardSetController(testStdSetCont2);
                cont.setSelected(testStdSetCont2);
                giic_CancelSOLController obj = new giic_CancelSOLController(cont);
                obj.cancelSalesOrderLines(); //Reserved qty  > 0.0
               
            }
        Test.stopTest();
        
        System.assertEquals(0.0, testStdSetCont[0].gii__ReservedQuantity__c);
    }
    
    @isTest public static void testCancelSOLFewOLCancelled(){
        
        Account TestAcc = [SELECT Id, Name FROM Account WHERE Name LIKE 'Test%' LIMIT 1];
        
        gii__SalesOrder__c testSO = [SELECT Id, giic_SOStatus__c, gii__Account__c, giic_OrderLineCancelled__c FROM gii__SalesOrder__c WHERE gii__Account__c =: TestAcc.Id LIMIT 1];
        testSO.giic_OrderLineCancelled__c = true;
        update testSO;
        
        List<gii__SalesOrderLine__c> testStdSetCont = [SELECT Id, gii__SalesOrder__c, gii__ReservedQuantity__c, gii__CancelDate__c, gii__CancelledQuantity__c, gii__CancelReason__c
                                                        FROM gii__SalesOrderLine__c 
                                                        WHERE gii__SalesOrder__c =: testSO.Id];
        
        
        
        Test.startTest();
            Test.setCurrentPage(Page.giic_CancelSalesORderLines);
            if(testStdSetCont.size() > 0){
                List<gii__SalesOrderLine__c> testStdSetCont2 = new List<gii__SalesOrderLine__c>();
                for(gii__SalesOrderLine__c soL : testStdSetCont){
                    testStdSetCont2.add(soL);
                    if(testStdSetCont2.size()>0) break;
                }
                ApexPages.StandardSetController cont = new ApexPages.StandardSetController(testStdSetCont2);
                cont.setSelected(testStdSetCont2);
                giic_CancelSOLController obj = new giic_CancelSOLController(cont);
                obj.cancelSalesOrderLines(); //Reserved qty  > 0.0
                
            }
        Test.stopTest();
        
        System.assertEquals(0.0, testStdSetCont[0].gii__ReservedQuantity__c);
    }
    
    
    /*
    *@Description: test method for back button
    *@Param: NA
    *@Retunr: NA
    */
   
    @isTest public static void testGoBack(){
        test.startTest();
        List<gii__SalesOrderLine__c> testSOLines = [SELECT Id FROM gii__SalesOrderLine__c];
        ApexPages.StandardSetController sc = new ApexPages.StandardSetController(testSOLines);
        sc.setSelected(testSOLines);
        giic_CancelSOLController objSOL = new giic_CancelSOLController(sc);
        objSOL.goBack();
        test.stopTest();
    }
    
}