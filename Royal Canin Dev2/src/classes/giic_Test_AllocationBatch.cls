/************************************************************************************
Version : 1.0
Created Date : 27 Aug 2018
Function : giic_Test_AllocationBatch is giic_AllocationBatch, giic_AllocationBatchForNonStockSO, giic_AllocationBatchScheduler
Modification Log : NA
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/

@isTest
private class giic_Test_AllocationBatch {
    public static User objTestClassUser;
    @testSetup
    static void setup(){
        // insert default warehouse
        List<gii__Warehouse__c> defaultWHList = giic_Test_DataCreationUtility.insertWarehouse();
        system.debug('defaultWHList--->>> ' + defaultWHList);
        // create system policy
        giic_Test_DataCreationUtility.testSystemPolicyCreation2();        
        SetupTestData.createCustomSettings();
        giic_Test_DataCreationUtility.CreateAdminUser();
        // insert 5 warehouse ie. in bluk
        List<gii__Warehouse__c> testWareListN = giic_Test_DataCreationUtility.insertWarehouse_N(5);
        
        list<giic_CommonUtility.WarehouseDistance> lstWDistence = new list<giic_CommonUtility.WarehouseDistance>();
        giic_CommonUtility.WarehouseDistance objWD;
        // Generate WarehouseDistanceMapping for account ie. sWarehouseDistanceMapping
        integer intDistence = 300;
        for(gii__Warehouse__c objW :  giic_Test_DataCreationUtility.lstWarehouse_N){
            objWD = new giic_CommonUtility.WarehouseDistance(objW.Id, intDistence);
            intDistence = intDistence + 20;
            lstWDistence.add(objWD);
        }
        intDistence = intDistence + 55;
        objWD = new giic_CommonUtility.WarehouseDistance(defaultWHList[0].Id, intDistence);
        lstWDistence.add(objWD);
        string sWarehouseDistanceMapping = JSON.serialize(lstWDistence);
        system.debug('sWarehouseDistanceMapping-->>>> ' + sWarehouseDistanceMapping);
        List<Account> testConsumerAccountList = giic_Test_DataCreationUtility.insertConsumerAccount_N(5);
        for(Account OAc : giic_Test_DataCreationUtility.lstAccount_N ){
            OAc.giic_WarehouseDistanceMapping__c = sWarehouseDistanceMapping;
        }
        update giic_Test_DataCreationUtility.lstAccount_N; 
        List<gii__Product2Add__c> testProductList = giic_Test_DataCreationUtility.insertProduct_N(); 
        giic_Test_DataCreationUtility.createCarrier(); 
        
        integer intCount = 0;
        for(gii__Product2Add__c objPR : giic_Test_DataCreationUtility.lstProdRef_N ){
            objPR.gii__DefaultWarehouse__c = testWareListN[math.mod(intCount,5)].Id;
            intCount++;
        }
        
        // Make 4th product gii__NonStock__c = true
        giic_Test_DataCreationUtility.lstProdRef_N[3].gii__NonStock__c = true;
        
        update giic_Test_DataCreationUtility.lstProdRef_N;
        system.debug('giic_Test_DataCreationUtility.lstProdRef_N--->>>' + giic_Test_DataCreationUtility.lstProdRef_N);
        giic_Test_DataCreationUtility.insertProductInventoryQTYDetails_N(5);
        
        List<gii__SalesOrder__c> testSOList1 = giic_Test_DataCreationUtility.insertSalesOrder_N(5);
        
        system.debug('testSOList1--->>>>>' + testSOList1);
        // create SOL
        giic_Test_DataCreationUtility.insertSOLine_N();
        system.debug('giic_Test_DataCreationUtility.lstSalesOrderLine_N--->>>>>' + giic_Test_DataCreationUtility.lstSalesOrderLine_N);
        // giic_Test_DataCreationUtility.lstProdRef_N[3] --->>> it is not stock product
        List<gii__SalesOrderLine__c> liSOLine = new List<gii__SalesOrderLine__c> ();
        List<gii__InventoryReserve__c> lstIR = [select id from gii__InventoryReserve__c limit 1];
        system.debug('lstIR--->>>>>' + lstIR);
        for(gii__SalesOrderLine__c oSOL : giic_Test_DataCreationUtility.lstSalesOrderLine_N){
            if(oSOL.gii__Product__c == giic_Test_DataCreationUtility.lstProdRef_N[3].Id){
                oSOL.giic_LineStatus__c = 'Back Order';
                oSOL.gii__Status__c = 'Open';
                // case when SOL warehouse in not same as product default warehouse.
                oSOL.gii__Warehouse__c = giic_Test_DataCreationUtility.lstWarehouse[0].Id;
                liSOLine.add(oSOL);
            }
        }
        update liSOLine;
        system.debug('liSOLine--->>>>>' + liSOLine);
        giic_Test_DataCreationUtility.insertExceptionRoute_N();
        giic_Test_DataCreationUtility.insertRoute_N();
        system.debug('lstExceptionRoute_N--->>>>>' + giic_Test_DataCreationUtility.lstExceptionRoute_N.size());
        system.debug('lstRoute_N--->>>>>' + giic_Test_DataCreationUtility.lstRoute_N.size());
        
        
    }
    
    public static testMethod void testExecute(){
        Test.startTest();
        objTestClassUser = giic_Test_DataCreationUtility.getTestClassUser();
        system.assertEquals(objTestClassUser.Alias,system.label.giic_TestUserAlias);
        system.runAs(objTestClassUser){
            SchedulableContext sc;
            giic_AllocationBatch allocation = new giic_AllocationBatch();
            // execute batch process
            Database.executeBatch(allocation);
            allocation.execute(sc);
            String cronExp = '0 00 01 ? * *'; //1 AM everyday
            giic_AllocationBatch.scheduler(cronExp);
        }
         Test.stopTest();
        List<gii__SalesOrder__c> soList = [Select Id from gii__SalesOrder__c ];
        List<gii__SalesOrderLine__c> solList = [Select Id from gii__SalesOrderLine__c];
       
        System.debug('Size is' +soList.size() + ' :::' + solList.size());
        
    } 
    
    
    public static testMethod void testdelInvResandgetAllSOLToUpdate(){
        Test.startTest();
        objTestClassUser = giic_Test_DataCreationUtility.getTestClassUser();
        system.assertEquals(objTestClassUser.Alias,system.label.giic_TestUserAlias);
        system.runAs(objTestClassUser){
            Map<Id, gii__SalesOrderLine__c> testSOLine =new Map<Id, gii__SalesOrderLine__c>([Select Id from gii__SalesOrderLine__c Limit 10]);
            giic_AllocationHelper.deleteInventoryReserve(testSOLine.keySet());
            // giic_AllocationHelper.getAllSOLToUpdate(testSOLine.keySet(), testSOLine.values());
        }   
        Test.stopTest();
    } 
    
    public static testMethod void testCreateWarehouseShippingOSRecords(){
        Test.startTest();
        objTestClassUser = giic_Test_DataCreationUtility.getTestClassUser();
        system.assertEquals(objTestClassUser.Alias,system.label.giic_TestUserAlias);
        system.runAs(objTestClassUser){
            Map<Id, gii__SalesOrderLine__c> testSOLine =new Map<Id, gii__SalesOrderLine__c>([SELECT gii__SalesOrder__c, gii__ReservedQuantity__c , Id FROM gii__SalesOrderLine__c Limit 10]);         
            giic_AllocationHelper.updateSOLWithConfirmedStatus(testSOLine.keySet());
            Set<Id> soId = new Set<Id>();
            Map<Id, Double> mapSOLIdQuantity= new Map<Id, Double>();
            for(Id soL :  testSOLine.keySet()){
                mapSOLIdQuantity.put(soL, testSOLine.get(soL).gii__ReservedQuantity__c);
                soId.add(testSOLine.get(soL).gii__SalesOrder__c);
            }
            Map<String, Object> paramObj = new Map<String, Object>{'RESERVEDSOL' => mapSOLIdQuantity, 'RESERVSOIDS' => soId} ;
                giic_AllocationHelper.createWarehouseShippingOSRecords(paramObj);
            giic_AllocationHelper.clearBackOrder(testSOLine.keySet());
        
        }
        
    } 
    
    
    
    // below is test method for giic_AllocationBatchScheduler
    public static testMethod void test_AllocationBatchScheduler(){
        Test.startTest();
        objTestClassUser = giic_Test_DataCreationUtility.getTestClassUser();
        system.assertEquals(objTestClassUser.Alias,system.label.giic_TestUserAlias);
        system.runAs(objTestClassUser){
            // getting system policy
            gii__SystemPolicy__c objSystemPolicy = [select Name, gii__Description__c, gii__PriceBookName__c, gii__StockUM__c, gii__ServiceTicketPricingOverriddenReason__c, gii__AutoReleaseOrder__c, gii__AllowZeroUnitPrice__c, gii__OppConversionPricingOverriddenReason__c, gii__SQConversionPricingOverriddenReason__c, gii__Warehouse__c, gii__AutoApprovePurchaseOrders__c, giic_JobTime__c from gii__SystemPolicy__c limit 1];
            apexpages.standardcontroller stdController = new apexpages.standardcontroller(objSystemPolicy);
            giic_AllocationBatchScheduler objABS = new giic_AllocationBatchScheduler(stdController);
            objABS.scheduleBatch();
            // need to recall scheduleBatch() method to reun schedular when a schedular is already running 
            objABS.scheduleBatch();
            
            // Doing negative testing 
            objSystemPolicy.giic_JobTime__c = null;
            update objSystemPolicy;
            apexpages.standardcontroller stdControllerNull = new apexpages.standardcontroller(objSystemPolicy);
            giic_AllocationBatchScheduler objABSNull = new giic_AllocationBatchScheduler(stdControllerNull);
            objABSNull.scheduleBatch();
            
        }
        Test.stopTest();
    }   
    
    
}