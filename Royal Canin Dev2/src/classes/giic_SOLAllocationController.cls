/************************************************************************************
Version : 1.0
Created Date : 20 Aug 2018
Function : This class is used for the manual allocation from the sales order lines detail page button
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/
public class giic_SOLAllocationController {
    public gii__salesorderline__c objSOL;
    public giic_SOLAllocationController(ApexPages.StandardController stdController) {
        this.objSOL = (gii__salesorderline__c)stdController.getRecord();   
    }
    	/*
    * Method name : updateAllocation
    * Description : process the sales order line for allocation 
    * Return Type : 
    * Parameter : 
    */
    public PageReference updateAllocation(){
        try{
            PageReference pgRef=new PageReference('/lightning/r/gii__SalesOrderLine__c/'+objSOL.id+'/view');
            list<gii__salesorderline__c> lstSOL=[select id,gii__Warehouse__c,gii__Product__c,gii__OrderQuantity__c,gii__SalesOrder__r.gii__Account__r.Blocked__c from gii__salesorderline__c where id=:objSOL.id];
            if(lstSOL != null && lstSOL.size() > 0){
                if(lstSOL[0].gii__SalesOrder__r.gii__Account__r.Blocked__c){
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,system.Label.giic_AccountBlockedAllocationErrorMSG);//TODO change the text into Label
                    ApexPages.addMessage(myMsg);
                    return null;
                }
                map<string,Object> mapResponse = giic_AllocationHelper.getPIQDInfo(new set<Id>{ lstSOL[0].gii__Warehouse__c},new set<Id>{ lstSOL[0].gii__Product__c});
                system.debug('***mapResponse**'+mapResponse);
                if(mapResponse != null && mapResponse.size() > 0){
                    Map<String, Integer> mapInvQtyDtl=mapResponse.containsKey('INQTYDTL') ? (Map<String, Integer>)mapResponse.get('INQTYDTL') : new Map<String, Integer>(); 
                    String key = lstSOL[0].gii__Product__c + '-' + lstSOL[0].gii__Warehouse__c;
                    if(mapInvQtyDtl.containsKey(key) && mapInvQtyDtl.get(key) - lstSOL[0].gii__OrderQuantity__c >= 0){
                        map<string,Object> mapAllocationRequest=new map<string,Object>{'RESERVEDSOL'=>new map<Id,Double>{lstSOL[0].id=>lstSOL[0].gii__OrderQuantity__c}};
                        giic_AllocationHelper.createAllocation(mapAllocationRequest);
                    }
                    else{
                        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,system.Label.giic_ProductInventoryAvailableQTYMSG);
                        ApexPages.addMessage(myMsg);
                        return null;
                    }
                    return pgRef;
                }
            }
            
        }
        catch(exception ex){
            
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,ex.getMessage());
            ApexPages.addMessage(myMsg);
            return null;
        }
        return null;
    }
}