@isTest
private class AttachmentUploadControllerTest {
	
	@isTest static void testRenderVetDocuments() {
		SetupTestData.createCustomSettings();
		List<RecordType> rtypes = [select developerName, id from RecordType where sObjectType = 'Customer_Registration__c' AND DeveloperName = 'Veterinarian' LIMIT 1];
		Customer_Registration__c testRegistration = SetupTestData.createCustomerRegistration();
		testRegistration.RecordTypeId = rtypes[0].Id;
		testRegistration.Upload_Complete_Practitioner__c = true;
		testRegistration.Upload_Complete_Resale__c = true;
		testRegistration.Application_Status__c = 'Submitted';
		testRegistration.Terms_and_Conditions_Accepted__c = true;
		insert testRegistration;

		Attachment testAttachment = new Attachment();
		testAttachment.Name = 'Test Attachment';
		testAttachment.ParentId = testRegistration.Id;
		testAttachment.Body = Blob.valueOf('Test');
		insert testAttachment;

		Customer_Registration__c retrievedReg = [SELECT Id, Application_Id__c FROM Customer_Registration__c WHERE Id =: testRegistration.Id LIMIT 1];

		PageReference pageRef = Page.AttachmentUpload;
		pageRef.getParameters().put('applicationId', retrievedReg.Application_Id__c);
		Test.setCurrentPage(pageRef);
		AttachmentUploadController controller= new AttachmentUploadController();

		System.assert(controller.renderRequiredDocs);
		System.assert(controller.renderExistingDocs);
		System.assert(controller.renderDelete);
		System.assert(controller.renderSalesAndUseTaxForm);
	}

	@isTest static void testRenderShelterDocuments() {
		SetupTestData.createCustomSettings();
		List<RecordType> rtypes = [select developerName, id from RecordType where sObjectType = 'Customer_Registration__c' AND DeveloperName = 'Shelter' LIMIT 1];
		Customer_Registration__c testRegistration = SetupTestData.createCustomerRegistration();
		testRegistration.RecordTypeId = rtypes[0].Id;
		testRegistration.Upload_Complete_Practitioner__c = true;
		testRegistration.Upload_Complete_Tax__c = true;
		testRegistration.Application_Status__c = 'Approved';
		insert testRegistration;

		Customer_Registration__c retrievedReg = [SELECT Id, Application_Id__c FROM Customer_Registration__c WHERE Id =: testRegistration.Id LIMIT 1];

		PageReference pageRef = Page.AttachmentUpload;
		pageRef.getParameters().put('applicationId', retrievedReg.Application_Id__c);
		Test.setCurrentPage(pageRef);
		AttachmentUploadController controller= new AttachmentUploadController();

		System.assert(controller.renderRequiredDocs);
		System.assert(controller.renderExistingDocs);
		System.assert(controller.renderDelete);
		System.assertEquals(null, controller.renderSalesAndUseTaxForm);
	}

	@isTest static void testRenderRetailerDocuments() {
		SetupTestData.createCustomSettings();
		List<RecordType> rtypes = [select developerName, id from RecordType where sObjectType = 'Customer_Registration__c' AND DeveloperName = 'Retailer' LIMIT 1];
		Customer_Registration__c testRegistration = SetupTestData.createCustomerRegistration();
		testRegistration.RecordTypeId = rtypes[0].Id;
		testRegistration.Upload_Complete_Practitioner__c = true;
		testRegistration.Upload_Complete_Resale__c = true;
		insert testRegistration;

		Customer_Registration__c retrievedReg = [SELECT Id, Application_Id__c FROM Customer_Registration__c WHERE Id =: testRegistration.Id LIMIT 1];

		PageReference pageRef = Page.AttachmentUpload;
		pageRef.getParameters().put('applicationId', retrievedReg.Application_Id__c);
		Test.setCurrentPage(pageRef);
		AttachmentUploadController controller= new AttachmentUploadController();

		System.assert(controller.renderRequiredDocs);
		System.assert(controller.renderExistingDocs);
		System.assert(controller.renderSalesAndUseTaxForm);
	}

	@isTest static void testGetAttachments() {
		SetupTestData.createCustomSettings();
		List<RecordType> rtypes = [select developerName, id from RecordType where sObjectType = 'Customer_Registration__c' AND DeveloperName = 'Veterinarian' LIMIT 1];
		Customer_Registration__c testRegistration = SetupTestData.createCustomerRegistration();
		testRegistration.RecordTypeId = rtypes[0].Id;
		testRegistration.Upload_Complete_Practitioner__c = true;
		testRegistration.Upload_Complete_Resale__c = true;
		testRegistration.Application_Status__c = 'Submitted';
		testRegistration.Terms_and_Conditions_Accepted__c = true;
		insert testRegistration;

		Attachment testAttachment = new Attachment();
		testAttachment.Name = 'Test Attachment';
		testAttachment.ParentId = testRegistration.Id;
		testAttachment.Body = Blob.valueOf('Test');
		testAttachment.Description = 'Please upload your Practitioner/Licensee�s DVM License and save to attach your file to your application.';
		insert testAttachment;

		Customer_Registration__c retrievedReg = [SELECT Id, Application_Id__c FROM Customer_Registration__c WHERE Id =: testRegistration.Id LIMIT 1];

		PageReference pageRef = Page.AttachmentUpload;
		pageRef.getParameters().put('applicationId', retrievedReg.Application_Id__c);
		Test.setCurrentPage(pageRef);
		AttachmentUploadController controller= new AttachmentUploadController();

		System.assertEquals(1, controller.getAttachments().size());

		Test.startTest();
			controller.attachToDeleteId = testAttachment.Id;
			controller.deleteAttachment();
		Test.stopTest();

		System.assertEquals(0, controller.getAttachments().size());
	}

	@isTest static void testSaveAttachments() {
		SetupTestData.createCustomSettings();
		List<RecordType> rtypes = [select developerName, id from RecordType where sObjectType = 'Customer_Registration__c' AND DeveloperName = 'Veterinarian' LIMIT 1];
		Customer_Registration__c testRegistration = SetupTestData.createCustomerRegistration();
		testRegistration.RecordTypeId = rtypes[0].Id;
		testRegistration.Upload_Complete_Practitioner__c = true;
		testRegistration.Upload_Complete_Resale__c = true;
		testRegistration.Application_Status__c = 'Submitted';
		testRegistration.Terms_and_Conditions_Accepted__c = true;
		insert testRegistration;

		Attachment testAttachment = new Attachment();
		testAttachment.Name = 'Test Attachment';
		testAttachment.ParentId = testRegistration.Id;
		testAttachment.Body = Blob.valueOf('Test');
		testAttachment.Description = 'Please upload your State Resale Certificate or completed copy of the attached Sales and Use Certificate and save to attach your file to your application.  This does not apply if you are in the states of Oregon, New Hampshire, Alaska, Montana or Delaware. This also does not need to be completed if you will pay Royal Canin sales tax.';

		Customer_Registration__c retrievedReg = [SELECT Id, Application_Id__c FROM Customer_Registration__c WHERE Id =: testRegistration.Id LIMIT 1];

		PageReference pageRef = Page.AttachmentUpload;
		pageRef.getParameters().put('applicationId', retrievedReg.Application_Id__c);
		Test.setCurrentPage(pageRef);
		AttachmentUploadController controller= new AttachmentUploadController();

		controller.newAttachments.add(testAttachment);
		Test.startTest();
			controller.save();
		Test.stopTest();

		System.assertEquals(1, controller.getAttachments().size());
	}
}