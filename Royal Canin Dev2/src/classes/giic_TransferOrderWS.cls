/************************************************************************************
Version : 1.0
Created Date : 29 Oct 2018
Function : transfer order service for the request from Insite
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/
@RestResource(urlMapping='/TransferOrder/v0.1/*')
global class giic_TransferOrderWS {
    /******************************************************
POST : To insert the transfer Order and Transfer Order Line
*******************************************************/
    public static List<string> ListOfErrors = new List<string>();
    
    @HttpPost 
    global static giic_TransferOrderWrapper.TransferOrderResult createTransferOrder(){
        
        /************************** RestRequest from Insite *****************************/
        RestRequest req = RestContext.request;
        set<string> setProdCode = new set<string>();
        set<string> setWarehouse = new set<string>();
        set<string> setTOL = new set<string>();
        set<string> setTO = new set<string>();
        set<string> setAccount = new set<string>();
        set<string> setCarrier = new set<string>();
        map<string, Account> mapAccount = new map<string, Account>();
        map<string, gii__Carrier__c> mapOfCarrier = new map<string, gii__Carrier__c>();
        map<string, gii__Product2Add__c> mapProds = new map<string, gii__Product2Add__c>();
        map<string, gii__Warehouse__c> mapWarehouses = new map<string, gii__Warehouse__c>();
        map<string, gii__TransferOrder__c> mapExistingTO = new map<string, gii__TransferOrder__c>();
        map<string, gii__TransferOrderLine__c> mapExistingTOL = new map<string, gii__TransferOrderLine__c>();
        map<string, gii__ProductInventoryQuantityDetail__c> mapPIQD = new map<string, gii__ProductInventoryQuantityDetail__c>();
        List<gii__TransferOrder__c> lstTransferOrderToCreate = new List<gii__TransferOrder__c>();
        List<gii__TransferOrderLine__c> lstTransferOrderLineToCreate = new List<gii__TransferOrderLine__c>();
        map<string,gii__TransferOrderLine__c> mapTransferOrderLine = new map<string,gii__TransferOrderLine__c>();
        
        Savepoint sp = Database.setSavepoint();
        giic_TransferOrderWrapper TOWForFaildRecords = new giic_TransferOrderWrapper();
        giic_TransferOrderWrapper TOWForSuccessRecords = new giic_TransferOrderWrapper();

        giic_TransferOrderWrapper.TransferOrderResult oTransferOrderResult = new giic_TransferOrderWrapper.TransferOrderResult();
        
        if(req != null){
            String request  = req.requestBody.tostring();
            giic_TransferOrderWrapper transferOrderWrapper ;
            try{
                transferOrderWrapper =(giic_TransferOrderWrapper)json.deserialize(request, giic_TransferOrderWrapper.class);
            }catch(exception exp){
                return returnResponse(giic_Constants.ERROR_CODE_400, giic_Constants.ERROR_MSG_FAILURE, new List<string> {exp.getMessage(), giic_Constants.STR_DESERIALIZATION_FAILED});
            }
            
            
            if(transferOrderWrapper != null && transferOrderWrapper.TransferOrders!= null && transferOrderWrapper.TransferOrders.size() > 0 ){
                for(giic_TransferOrderWrapper.TransferOrder oTO  : transferOrderWrapper.TransferOrders){
                    setAccount.add(oTO.TO_ShipToCustomerNumber);
                    setWarehouse.add(oTO.TO_ToWarehouse);
                    setWarehouse.add(oTO.TO_FromWarehouse);
                    setCarrier.add(oTO.TO_Carrier);
                    setTO.add(oTO.TO_OrderNumber);
                    if(oTO.transferOrderLines != null && oTO.transferOrderLines.size() > 0 ){
                        for(giic_TransferOrderWrapper.TransferOrderLine oTOL : oTO.transferOrderLines){
                            setProdCode.add(oTOL.TOL_ProductCode);
                            setWarehouse.add(oTOL.TOL_FromWarehouseCode); 
                            if(setTOL != null && oTOL.TOL_OrderLineNumber != null && setTOL.contains(oTOL.TOL_OrderLineNumber)){
                                // giic_TOL_DuplicateTOL = Duplicate TOL_OrderLineNumber
                                ListOfErrors.add( label.giic_TOL_DuplicateTOL + oTOL.TOL_OrderLineNumber);
                            }
                            setTOL.add(oTOL.TOL_OrderLineNumber);
                            
                        }
                    }
                }
            }
            if(setAccount != null && setAccount.size() > 0){
                mapAccount = getAccount(setAccount);
            }
            if(setCarrier != null && setCarrier.size() > 0){
                mapOfCarrier = getCarrier(setCarrier);
            }
            if(setProdCode != null && setProdCode.size() > 0){
                mapProds = getProducts(setProdCode);
            }
            
            if(setWarehouse != null && setWarehouse.size() > 0 ){
                mapWarehouses = getWarehouse(setWarehouse);
            }
            
            if(setTO != null && setTO.size() > 0 ){
                mapExistingTO = getExistingTO(setTO);
            }
            
            if(setTOL != null && setTOL.size() > 0 ){
                mapExistingTOL = getExistingTOL(setTOL);
            }
            
            if(setProdCode != null && setProdCode.size() > 0 && setWarehouse != null && setWarehouse.size() > 0 ){
                mapPIQD = getPIQD(setProdCode, setWarehouse);
            }
            validateRequest(mapAccount,mapOfCarrier, mapProds,mapWarehouses, mapExistingTO, mapExistingTOL, transferOrderWrapper);
            
            if(transferOrderWrapper != null && transferOrderWrapper.TransferOrders!= null && transferOrderWrapper.TransferOrders.size() > 0 ){
                gii__TransferOrder__c objTO;
                for(giic_TransferOrderWrapper.TransferOrder oTO  : transferOrderWrapper.TransferOrders){
                    objTO = new gii__TransferOrder__c();
                    if(mapAccount != null && mapAccount.containsKey(oTO.TO_ShipToCustomerNumber)){
                        objTO.gii__Account__c = mapAccount.get(oTO.TO_ShipToCustomerNumber).Id;
                    }
                    
                    if(mapWarehouses != null && mapWarehouses.containsKey(oTO.TO_ToWarehouse)){
                        objTO.gii__TransferToWarehouse__c = mapWarehouses.get(oTO.TO_ToWarehouse).Id;
                    }
                    
                    if(mapWarehouses != null && mapWarehouses.containsKey(oTO.TO_FromWarehouse) ){
                        objTO.gii__FromWarehouse__c = mapWarehouses.get(oTO.TO_FromWarehouse).Id;
                    }
                    
                    if(mapOfCarrier != null && mapOfCarrier.containsKey(oTO.TO_Carrier) ){
                        objTO.gii__Carrier__c = mapOfCarrier.get(oTO.TO_Carrier).Id;
                    }
                    
                    objTO.giic_OrderNo__c = oTO.TO_OrderNumber;
                    objTO.gii__OrderDate__c = oTO.TO_OrderDate;
                    objTO.gii__RequiredDate__c = oTO.TO_RequiredDate;
                    objTO.gii__ShipToCity__c = oTO.TO_ShipToCity;
                    objTO.gii__ShipToCountry__c = oTO.TO_ShipToCountry;
                    objTO.gii__ShipToName__c = oTO.TO_ShipToName;
                    objTO.gii__ShipToState__c = oTO.TO_ShipToState;
                    objTO.gii__ShipToStreet__c = oTO.TO_ShipToStreet;
                    objTO.gii__ShipToZipPostalCode__c = oTO.TO_ShipToZipCode;
                    lstTransferOrderToCreate.add(objTO);
                }
            }
            
            if(ListOfErrors != null && ListOfErrors.size() > 0){
                // if any error found, failuer response will be returned from here and further code execution will be stoped here.
                return returnResponse(giic_Constants.ERROR_CODE_400, giic_Constants.ERROR_MSG_FAILURE, ListOfErrors);
            }
            
            // create Transfer Order   
            Database.SaveResult[] TOSaveResult;
            Database.SaveResult[] TOLSaveResult;
            if(lstTransferOrderToCreate != null && lstTransferOrderToCreate.size() > 0){
                TOSaveResult = Database.insert(lstTransferOrderToCreate, false);
                Integer intTOCounter = 0;
                for(Database.SaveResult TOSR : TOSaveResult ){
                    if (TOSR.isSuccess()) {
                        giic_TransferOrderWrapper.TransferOrder oSuccessTO = transferOrderWrapper.TransferOrders[intTOCounter];
                        TOWForSuccessRecords.TransferOrders.add(oSuccessTO);
                        // Operation was successful, so get the ID of the record that was processed
                        if(transferOrderWrapper.TransferOrders[intTOCounter].transferOrderLines != null && transferOrderWrapper.TransferOrders[intTOCounter].transferOrderLines.size() >0 ){
                            gii__TransferOrderLine__c objTOL;
                            for(giic_TransferOrderWrapper.TransferOrderLine oTOL : transferOrderWrapper.TransferOrders[intTOCounter].transferOrderLines){
                                objTOL = new gii__TransferOrderLine__c();
                                
                                if(mapWarehouses != null && mapWarehouses.containsKey(oTOL.TOL_FromWarehouseCode)){
                                    objTOL.gii__Warehouse__c =  mapWarehouses.get(oTOL.TOL_FromWarehouseCode).Id;
                                }else{
                                    ListOfErrors.add( label.giic_TOLFromWarehouseNotFound + oTOL.TOL_FromWarehouseCode);
                                    continue;
                                }
                                
                                if(mapProds != null && mapProds.containsKey(oTOL.TOL_ProductCode)){
                                    objTOL.gii__Product__c = mapProds.get(oTOL.TOL_ProductCode).Id;
                                }else{
                                    ListOfErrors.add( label.giic_TOLProductCodeNotFound + oTOL.TOL_ProductCode);
                                    continue;
                                }
                                
                                objTOL.gii__StockUM__c = oTOL.TOL_StockUOM;
                                objTOL.gii__Quantity__c = oTOL.TOL_Quantity;
                                objTOL.giic_OrderLineNo__c = oTOL.TOL_OrderLineNumber;
                                objTOL.gii__NonStock__c = oTOL.TOL_IsNonStock;
                                objTOL.gii__TransferOrder__c = TOSR.getId() ;
                                lstTransferOrderLineToCreate.add(objTOL);
                            }
                        }
                    }
                    else {
                        string sTOError = '';
                        // Operation failed, so get all errors                
                        for(Database.Error err : TOSR.getErrors()) { 
                            sTOError = err.getStatusCode() + ': ' + err.getMessage() +  giic_Constants.DUE_TO + err.getFields() + ' ';
                            giic_TransferOrderWrapper.TransferOrder failedTO = new giic_TransferOrderWrapper.TransferOrder();
                            failedTO = transferOrderWrapper.TransferOrders[intTOCounter];
                            if(transferOrderWrapper.TransferOrders[intTOCounter].transferOrderLines != null && transferOrderWrapper.TransferOrders[intTOCounter].transferOrderLines.size() > 0){
                                failedTO.transferOrderLines = transferOrderWrapper.TransferOrders[intTOCounter].transferOrderLines;
                            }
                            TOWForFaildRecords.TransferOrders.add(failedTO);
                        }
                        ListOfErrors.add(sTOError );
                    }
                    intTOCounter ++;
                }
                
                
                //  create Transfer Order Line gii__TransferOrderLine__c
                if(lstTransferOrderLineToCreate != null && lstTransferOrderLineToCreate.size()>0 ){
                    TOLSaveResult = Database.insert(lstTransferOrderLineToCreate, false);
                }else{
                    // if no line is created then roll back transfer Order Created
                    // giic_TOL_NoTOLCreated = No Transfer Order Line Created
                    ListOfErrors.add(Label.giic_TOL_NoTOLCreated );
                    Database.rollback(sp);
                    return returnResponse(giic_Constants.ERROR_CODE_400, giic_Constants.ERROR_MSG_FAILURE, ListOfErrors);
                }
                Integer intTOLCounter = 0;  
                if(TOLSaveResult != null && TOLSaveResult.size() >0 ){
                    for(Database.SaveResult TOLSR : TOLSaveResult ){
                        if (TOLSR.isSuccess()) {
                            mapTransferOrderLine.put(lstTransferOrderLineToCreate[intTOLCounter].giic_OrderLineNo__c,lstTransferOrderLineToCreate[intTOLCounter]);
                        }
                        else {
                            string sTOLError = ' ';
                            // Operation failed, so get all errors                
                            for(Database.Error err : TOLSR.getErrors()) {
                                sTOLError = err.getStatusCode() + ': ' + err.getMessage() +  giic_Constants.DUE_TO + err.getFields() + ' ';
                            }
                            ListOfErrors.add(sTOLError );
                        }
                        intTOLCounter ++;
                    }
                }
                
                if(mapTransferOrderLine != null && mapTransferOrderLine.size() > 0 && mapPIQD != null && mapPIQD.size() > 0){
                    oTransferOrderresult = CreateReservationandQuickShipFunction(mapTransferOrderLine,mapPIQD, TOWForSuccessRecords, ListOfErrors, transferOrderWrapper);
                    
                }
                else{
                    
                    Database.rollback(sp);
                    createErrorLog(null, null, ListOfErrors, transferOrderWrapper);
                    return returnResponse(giic_Constants.ERROR_CODE_400, giic_Constants.ERROR_MSG_FAILURE, ListOfErrors);
                }
            }
            else{
                // giic_TO_InvalidData = invalid data to create Transfer Order ie. No transfer Order created
                ListOfErrors.add(label.giic_TO_InvalidData );
                return returnResponse(giic_Constants.ERROR_CODE_400, giic_Constants.ERROR_MSG_FAILURE, ListOfErrors);
            } 
        }
        
        return oTransferOrderresult;
    }
    
    public static giic_TransferOrderWrapper.TransferOrderResult returnResponse(string code, string status, List<string> ListOfErrors){
        giic_TransferOrderWrapper.TransferOrderResult ObjTOR = new giic_TransferOrderWrapper.TransferOrderResult();
        ObjTOR.ListOfErrors = ListOfErrors;
        ObjTOR.status = status;
        ObjTOR.code = code;
        return ObjTOR;
    }
    public static giic_TransferOrderWrapper.TransferOrderResult CreateReservationandQuickShipFunction(map<string,gii__TransferOrderLine__c> mapTransferOrderLine,map<string, gii__ProductInventoryQuantityDetail__c> mapPIQD, giic_TransferOrderWrapper TOWForSuccessRecords ,List<string> ListOfErrors, giic_TransferOrderWrapper transferOrderWrapper ){
        giic_TransferOrderWrapper.TransferOrderResult oTOR = new giic_TransferOrderWrapper.TransferOrderResult();
        try{
            map<string, string> mapTOLVsTO = new map<string, string>();
            // create list for inventory reservation records
            List<gii__InventoryReserve__c> listInventoryReserve = new List<gii__InventoryReserve__c>(); 
            
            if(TOWForSuccessRecords != null && TOWForSuccessRecords.TransferOrders!= null && TOWForSuccessRecords.TransferOrders.size() > 0 ){
                for(giic_TransferOrderWrapper.TransferOrder oTO  : TOWForSuccessRecords.TransferOrders){
                    
                    if(oTO.transferOrderLines != null && oTO.transferOrderLines.size() > 0 ){
                        for(giic_TransferOrderWrapper.TransferOrderLine oTOL : oTO.transferOrderLines){
                            // create inventory reservation  record
                            gii__InventoryReserve__c ir = new gii__InventoryReserve__c();
                            // assign Transfer Order line record id
                            if(mapTransferOrderLine != null && mapTransferOrderLine.containsKey(oTOL.TOL_OrderLineNumber)){
                                ir.gii__TransferOrderLine__c = mapTransferOrderLine.get(oTOL.TOL_OrderLineNumber).Id;  
                            }else{ // giic_TOLNotFound = 'Transfer Order line not found '
                                ListOfErrors.add( label.giic_TOLNotFound); 
                            }
                            
                            // assign Product Inventory Quantity Detail id of the location and product lot
                            if(mapPIQD != null && mapPIQD.containsKey(oTOL.TOL_ProductCode + '~'+ oTOL.TOL_FromWarehouseCode)){
                                ir.gii__ProductInventoryQuantityDetail__c = mapPIQD.get(oTOL.TOL_ProductCode + '~'+ oTOL.TOL_FromWarehouseCode).Id;
                            }else{
                                ListOfErrors.add(label.giic_TOL_PIQD_NotFound + oTOL.TOL_ProductCode + ' TOL_FromWarehouseCode = ' +  oTOL.TOL_FromWarehouseCode);
                            }
                            
                            // quantity to be reserved from the location for the Transfer Order line (stocking UM)
                            ir.gii__ReserveQuantity__c = oTOL.TOL_ShippedQuantity;  
                            //add reservation record to the list
                            listInventoryReserve.add(ir);
                        }
                    }
                }
            }
            
            // input object for reservation creation
            gii.TransferOrderDirectReservation.inputDirectReservation inputObj = new gii.TransferOrderDirectReservation.inputDirectReservation();
            // assign the list of reservations to the input object  
            inputObj.listInvReserve = listInventoryReserve;
            inputObj.Action = giic_Constants.RESERVE_AND_SHIP;  
            // RESERVE_AND_SHIP = ReserveandShip
            inputObj.ShippedDate = system.today();  // specify shipment date
            
            // output for DirectReservationResult
            gii.TransferOrderDirectReservation.DirectReservationResult resultObj = new gii.TransferOrderDirectReservation.DirectReservationResult();
            
            // execute direct reservaion and quick ship method
            resultObj = gii.TransferOrderDirectReservation.CreateReservationandQuickShip(inputObj);
            // Exception Handling
            if (resultObj.Exceptions != null) {
                
                if (resultObj.Exceptions.size() > 0) {
                    List<String> ErrorMsgList = new List<String>();
                    for(gii.TransferOrderDirectReservation.GOMException e : resultObj.Exceptions) { 
                        ErrorMsgList.add(e.getMessage());
                    }
                    if(ErrorMsgList.size()>0){
                        ListOfErrors = ErrorMsgList;
                    }
                    if(mapTransferOrderLine != null && mapTransferOrderLine.size() > 0){
                        for(gii__TransferOrderLine__c objTOL : mapTransferOrderLine.values()){
                            mapTOLVsTO.put(objTOL.Id, objTOL.gii__TransferOrder__c);
                        }
                    }
                    createErrorLog(resultObj, mapTOLVsTO, ListOfErrors, transferOrderWrapper);
                }          
                oTOR = returnResponse(giic_Constants.ERROR_CODE_417, giic_Constants.ERROR_MSG_FAILURE, ListOfErrors);
            }    
            else If(ListOfErrors != null && ListOfErrors.size() > 0 ){
                oTOR = returnResponse(giic_Constants.ERROR_CODE_207, giic_Constants.PARTIAL_SUCCESS, ListOfErrors);
            }
            
            if(ListOfErrors ==  null || ListOfErrors.size() == 0){
                oTOR = returnResponse(giic_Constants.SUCCESS_CODE_200, giic_Constants.SUCCESS_MSG, ListOfErrors);
            }
        }
        catch(exception exp){
            return returnResponse(giic_Constants.ERROR_CODE_417, giic_Constants.ERROR_MSG_FAILURE, new List<string> {exp.getMessage()});
        }
        
        return oTOR ;
        
        
    }
    
    public static void createErrorLog(gii.TransferOrderDirectReservation.DirectReservationResult resultObj, map<string, string> mapTOLVsTO, List<string> ListOfErrors, giic_TransferOrderWrapper transferOrderWrapper ){
        List<Integration_Error_Log__c> lstError =  new List<Integration_Error_Log__c>();
        string JSONRequest = JSOn.serialize(transferOrderWrapper);
        Integration_Error_Log__c objErr;
        if(resultObj != null && resultObj.Exceptions != null && resultObj.Exceptions.size() >0){
            if(mapTOLVsTO.keySet() != null && mapTOLVsTO.keySet().size() > 0 ){
                integer intCounter = 0;
                for(string oTOLId :  mapTOLVsTO.keySet()){
                    objErr = new Integration_Error_Log__c();
                    objErr.Name = giic_Constants.TRANSFER_ORDER;
                    objErr.Error_Code__c = giic_Constants.TRANSFER_ORDER_RESERVATION_FAILED;
                    objErr.giic_IsActive__c = true;
                    objErr.giic_TransferOrder__c = mapTOLVsTO.containsKey(oTOLId) ? mapTOLVsTO.get(oTOLId) : null;
                    objErr.giic_TransferOrderLine__c = oTOLId;
                    string strErrorMsg = string.valueOf(ListOfErrors) + giic_Constants.JSON_REQUEST + JSONRequest;
                    if(strErrorMsg.length()> 32768){
                        strErrorMsg = strErrorMsg.substring(0, 32767);
                    }
                    objErr.Error_Message__c = strErrorMsg;
                    lstError.add(objErr);
                    intCounter ++;
                }
            }
        }
        if(resultObj == null && mapTOLVsTO == null && ListOfErrors != null ){
            objErr = new Integration_Error_Log__c();
            objErr.Name = giic_Constants.TRANSFER_ORDER;
            objErr.Error_Code__c = giic_Constants.TRANSFER_ORDER_RESERVATION_FAILED;
            objErr.giic_IsActive__c = true;
            string strErrorMsg = string.valueOf(ListOfErrors) + giic_Constants.JSON_REQUEST + JSONRequest;
            if(strErrorMsg.length()> 32768){
                strErrorMsg = strErrorMsg.substring(0, 32767);
            }
            objErr.Error_Message__c = strErrorMsg;
            
            lstError.add(objErr);
        }
        if(lstError != null && lstError.size() > 0){
            insert lstError;
        }
        
        
    }
    public static map<string, Account> getAccount(set<string> setAccount){
        map<string, Account> mapOfAcc = new map<string, Account>();
        List<Account> latAcc = [select id, name, ShipToCustomerNo__c from account where ShipToCustomerNo__c in : setAccount];
        if(latAcc != null && latAcc.size() > 0){
            for(Account oAcc : latAcc){
                mapOfAcc.put(oAcc.ShipToCustomerNo__c,oAcc);
            }
        }
        return mapOfAcc;
    }
    
    public static map<string, gii__Carrier__c> getCarrier(set<string> setCarrier){
        map<string, gii__Carrier__c> mapOfCarr = new map<string, gii__Carrier__c>();
        List<gii__Carrier__c> lstCarr = [select id, name from gii__Carrier__c where Name in : setCarrier];
        if(lstCarr != null && lstCarr.size() > 0){
            for(gii__Carrier__c oCarr : lstCarr){
                mapOfCarr.put(oCarr.Name,oCarr);
            }
        }
        return mapOfCarr;
    }
    
    
    public static map<string, gii__Product2Add__c> getProducts(set<string> setProdCode){
        map<string, gii__Product2Add__c> mapProducts = new map<string, gii__Product2Add__c>();
        List<gii__Product2Add__c> lstProd = [select Id, giic_ProductSKU__c from gii__Product2Add__c where giic_ProductSKU__c in : setProdCode];
        for(gii__Product2Add__c oProd : lstProd ){
            mapProducts.put(oProd.giic_ProductSKU__c ,oProd);
        }
        return mapProducts;
    }
    
    public static map<string, gii__Warehouse__c> getWarehouse(set<string> setWarehouse){
        map<string, gii__Warehouse__c> mapWH = new map<string, gii__Warehouse__c>();
        List<gii__Warehouse__c> lstWH = [select Id, giic_WarehouseCode__c from gii__Warehouse__c where giic_WarehouseCode__c in : setWarehouse ];
        for(gii__Warehouse__c oWH : lstWH ){
            mapWH.put(oWH.giic_WarehouseCode__c,oWH);
        }
        return mapWH;
    }
    
    public static map<string, gii__TransferOrder__c> getExistingTO(set<string> setTO){
        map<string, gii__TransferOrder__c> mapOfTO = new map<string, gii__TransferOrder__c>();
        List<gii__TransferOrder__c> lstTO = [select id, giic_OrderNo__c from gii__TransferOrder__c where giic_OrderNo__c in : setTO];
        if(lstTO != null && lstTO.size() >  0 ){
            for(gii__TransferOrder__c oTranOdr :  lstTO){
                mapOfTO.put(oTranOdr.giic_OrderNo__c, oTranOdr);
            }
        }
        return mapOfTO;
    }
    
    public static map<string, gii__TransferOrderLine__c> getExistingTOL(set<string> setTOL){
        map<string, gii__TransferOrderLine__c> mapOfTOL = new map<string, gii__TransferOrderLine__c>();
        List<gii__TransferOrderLine__c> lstTOL = [select id, giic_OrderLineNo__c from gii__TransferOrderLine__c where giic_OrderLineNo__c in : setTOL];
        if(lstTOL != null && lstTOL.size() > 0){
            for(gii__TransferOrderLine__c oTranOdrLine :  lstTOL){
                mapOfTOL.put(oTranOdrLine.giic_OrderLineNo__c, oTranOdrLine);
            }
        }
        return mapOfTOL;
    }
    
    public static map<string, gii__ProductInventoryQuantityDetail__c> getPIQD(set<string> setProdCode, set<string> setWarehouse){
        map<string, gii__ProductInventoryQuantityDetail__c> mapPIQD = new map<string, gii__ProductInventoryQuantityDetail__c>();
        List<gii__ProductInventoryQuantityDetail__c> lstPIQD = [select Id, gii__Warehouse__c, gii__Warehouse__r.giic_WarehouseCode__c, gii__Product__r.giic_ProductSKU__c from gii__ProductInventoryQuantityDetail__c where gii__Warehouse__r.giic_WarehouseCode__c in : setWarehouse AND gii__Product__r.giic_ProductSKU__c in : setProdCode];
        for(gii__ProductInventoryQuantityDetail__c oPIQD : lstPIQD ){
            mapPIQD.put(oPIQD.gii__Product__r.giic_ProductSKU__c  +'~'+ oPIQD.gii__Warehouse__r.giic_WarehouseCode__c,oPIQD);
        }
        return mapPIQD;
    }
    
    
    public static List<string> validateRequest(map<string, Account> mapAccount,map<string, gii__Carrier__c> mapOfCarrier, map<string, gii__Product2Add__c> mapProds,map<string, gii__Warehouse__c> mapWarehouses, map<string, gii__TransferOrder__c> mapExistingTO, map<string, gii__TransferOrderLine__c> mapExistingTOL, giic_TransferOrderWrapper transferOrderWrapper ){
        if(transferOrderWrapper != null && transferOrderWrapper.TransferOrders!= null && transferOrderWrapper.TransferOrders.size() > 0 ){
            for(giic_TransferOrderWrapper.TransferOrder oTO  : transferOrderWrapper.TransferOrders){
                
            
                if(mapAccount == null || mapAccount.size()==0 ||(mapAccount != null && !mapAccount.containsKey(oTO.TO_ShipToCustomerNumber) && oTO.TO_ShipToCustomerNumber != null)) {
                    // Account Not Found. Account No.=
                    ListOfErrors.add( label.giic_TOAccountNotFound + oTO.TO_ShipToCustomerNumber);
                }
                
                if(oTO.TO_ToWarehouse == '' || oTO.TO_ToWarehouse == null){
                    // giic_TO_WhMissing =  To warehouse missing
                    ListOfErrors.add( label.giic_TO_WhMissing );
                } 
                if (mapWarehouses == null || mapWarehouses.size() == 0 || (oTO.TO_ToWarehouse != null && mapWarehouses != null && !mapWarehouses.containsKey(oTO.TO_ToWarehouse))){
                    ListOfErrors.add( label.giic_TO_ToWrehouseNotFound   + oTO.TO_ToWarehouse);
                }
                
                if(oTO.TO_FromWarehouse == '' || oTO.TO_FromWarehouse == null){
                    // giic_TO_From_WhMissing = From warehouse missing at parent level
                    ListOfErrors.add(label.giic_TO_From_WhMissing);
                } 
                if(mapWarehouses == null || mapWarehouses.size() ==0 || (oTO.TO_FromWarehouse != null && mapWarehouses != null && !mapWarehouses.containsKey(oTO.TO_FromWarehouse) )){
                    ListOfErrors.add( label.giic_TO_FromWarehouseNotFound + oTO.TO_FromWarehouse);
                }
                
                if(oTO.TO_Carrier == '' || oTO.TO_Carrier == null ){
                    ListOfErrors.add(label.giic_TO_CarrierMissing);
                }
                if(mapOfCarrier == null || mapOfCarrier.size() == 0 || (oTO.TO_Carrier != null && mapOfCarrier != null && !mapOfCarrier.containsKey(oTO.TO_Carrier) )) {
                    ListOfErrors.add( label.giic_TO_CarrierNotFound  + oTO.TO_Carrier);
                }
                
                if(oTO.TO_OrderNumber == '' || oTO.TO_OrderNumber == null){
                    ListOfErrors.add( label.giic_TO_NumberMissing);
                }
                if(mapExistingTO != null && mapExistingTO.containsKey(oTO.TO_OrderNumber) && oTO.TO_OrderNumber != null ){
                    ListOfErrors.add( label.giic_TO_DuplicateTONum + ' ' + oTO.TO_OrderNumber);
                }
                
                if(oTO.transferOrderLines != null && oTO.transferOrderLines.size() > 0 ){
                    for(giic_TransferOrderWrapper.TransferOrderLine oTOL : oTO.transferOrderLines){
                        
                        if(oTOL.TOL_FromWarehouseCode == '' || oTOL.TOL_FromWarehouseCode == null){
                            ListOfErrors.add( label.giic_TOL_FromWhMissing);
                        }
                        if(mapWarehouses == null || mapWarehouses.size() ==0 ||  (oTOL.TOL_FromWarehouseCode != null && mapWarehouses != null && !mapWarehouses.containsKey(oTOL.TOL_FromWarehouseCode))){
                            ListOfErrors.add( label.giic_TOLFromWarehouseNotFound + oTOL.TOL_FromWarehouseCode);
                        }
                        
                        if(oTOL.TOL_ProductCode == '' || oTOL.TOL_ProductCode == null){
                            ListOfErrors.add( label.giic_TOL_ProdCodeMissing );
                        }
                        if(mapProds == null || mapProds.size() ==0 || ( oTOL.TOL_ProductCode != null && mapProds != null && !mapProds.containsKey(oTOL.TOL_ProductCode))){
                            ListOfErrors.add( label.giic_TOLProductCodeNotFound + oTOL.TOL_ProductCode);
                        }
                        
                        if(oTOL.TOL_OrderLineNumber == '' || oTOL.TOL_OrderLineNumber == null){
                            ListOfErrors.add( Label.giic_TOL_OdrLineNumMissing);
                        }
                        if( mapExistingTOL != null && mapExistingTOL.containsKey(oTOL.TOL_OrderLineNumber)){
                            ListOfErrors.add( label.giic_TOL_DuplicateTOL + oTOL.TOL_OrderLineNumber);
                        }
                    }
                }
            }
        }
        return ListOfErrors;
    }
}