/*
 * Date:       		 - Developer, Company					       - Description
 * XXXX-XX-XX        - Joe Wrabel, Acumen Solutions                - Created
 * 2015-08-04		 - Joe Wortman, Acumen Solutions			   - Edited
 */
@isTest
private class PartnerTriggerHelperTest {
	
	@isTest static void test_method_one() {
		SetupTestData.createCustomSettings();
		SetupTestData.createAccounts(2);
		Account acc = SetupTestData.testAccounts[0];
		acc.Name = 'from';
		Account acc2 = SetupTestData.testAccounts[1];
		acc2.Name = 'to';
		update acc;
		update acc2;

		Partner__c part = new Partner__c();
		part.AccountFromId__c = acc.Id;
		part.AccountToId__c = acc2.Id;
		part.Role__c = 'Employer';
		insert part;

		part = [select id, accountfromid__c, accounttoid__c, role__c, ReversePartnerId__c from Partner__c where id =: part.Id];
		Partner__c part2 = [select id, accountfromid__c, accounttoid__c, role__c, ReversePartnerId__c from Partner__c where ReversePartnerId__c =: part.Id];

		system.debug(part);
		system.debug(part2);
		System.assertNotEquals(part2, null);
		System.assertEquals(part.ReversePartnerId__c, part2.Id);
		System.assertEquals(part2.ReversePartnerId__c, part.Id);
	}	
}