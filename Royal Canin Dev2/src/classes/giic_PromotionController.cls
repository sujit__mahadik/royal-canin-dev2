/************************************************************************************
Version : 1.0
Created Date : 20 Sep 2018
Function : all promotion method called from Aura components
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/
public class giic_PromotionController {
     /*
    * Method name : getAutoPromotion
    * Description : apply all the auto promotion on the sales order
    * Return Type : list<giic_PromotionWrapper.PromoRequestHeader>
    * Parameter : sales order id
    */
    @auraEnabled
    public static map<string,object> applyAutoPromotion(map<string,object> mapRequest){
        map<string,object> mapResponse=new map<string,object>();
        if(!mapRequest.isEmpty()){
            string soId=mapRequest.containsKey('soId') ? (string)mapRequest.get('soId') : '';
            if(soId != null && soId != ''){
                list<giic_PromotionWrapper.PromoRequestHeader> lstPromoReqHeader=new list<giic_PromotionWrapper.PromoRequestHeader>();
                lstPromoReqHeader= preparepromotionsRequest(new set<string>{soId});
                map<string,object> mapPromoRequest= new map<string,object>{'PROMOTIONREQUESTWRAPPER'=>JSON.serialize(lstPromoReqHeader),'SOIDS'=>new set<string>{soId},'PROMOTYPEAUTO'=>true,'ISINIT'=>mapRequest.get('isInit')};
                
                if(mapRequest.containsKey(giic_Constants.PROMOCODE)){
                    mapPromoRequest.put(giic_Constants.PROMOCODE,mapRequest.get(giic_Constants.PROMOCODE));
                }
                //call promotion helper to update the auto promotion on sales order and return the JSON for promotion screen
                mapResponse = giic_PromotionHelper.applyPromotion(mapPromoRequest);
            }
        }
        return mapResponse;
    }
    
    /*
    * Method name : preparepromotionsRequest
    * Description : prepare the request format for promotion
    * Return Type : list<giic_PromotionWrapper.PromoRequestHeader>
    * Parameter : setSOIds = > set of sales order id
    */
    public static list<giic_PromotionWrapper.PromoRequestHeader> preparepromotionsRequest(set<string> setSOIds)
    {
        list<gii__salesorder__c> lstSalesOrder = [select id,gii__account__c,gii__PaymentMethod__c,gii__Promotion__c,giic_HPROMO1__c,giic_HPROMO2__c,giic_FREESHIP__c,gii__ProductAmount__c,gii__AdditionalChargeAmount__c,gii__TotalAmount__c,gii__ShipToCountry__c,gii__Carrier__c,gii__DropShip__c, gii__ProductExtensionAmount__c	,gii__PriceBookName__c,gii__NetAmount__c,gii__DiscountPercent__c,gii__TotalWeight__c, gii__AccountReference__c,gii__account__r.Insite_Customer_Type__c,
                                                          (select id,gii__LineStatus__c, gii__OrderQuantity__c,gii__CancelledQuantity__c,gii__OpenQuantity__c,gii__UnitPrice__c,gii__Product__r.gii__ProductStyle__c,gii__Product__c,gii__Product__r.giic_ProductSKU__c from gii__SalesOrder__r where gii__CancelReason__c = null ) from gii__salesorder__c where id in :setSOIds];
        list<giic_PromotionWrapper.PromoRequestHeader> lstPRHeader=new list<giic_PromotionWrapper.PromoRequestHeader>();
        lstPRHeader=giic_PromotionHelper.getPromotionRequest(lstSalesOrder);
        return lstPRHeader;
    }
    /*
    * Method name : deletePromotion
    * Description : delete the promotion based on promotion id and sales order id
    * Return Type : list<giic_PromotionWrapper.PromoRequestHeader>
    * Parameter : SOIDS = > set of sales order id, Set of promotion ids
    */
     @auraEnabled
    Public static map<string,object> deletePromotion(map<string,object> mapRequest)
    {
       map<string,object> mapResponse;
       try
       {
            String soid =  (mapRequest != null && mapRequest.containsKey(giic_Constants.SOIDS))? String.valueOf(mapRequest.get(giic_Constants.SOIDS)):null;
            if(soid != null)
            {   
                list<giic_PromotionWrapper.PromoRequestHeader> lstPromoReqHeader = preparepromotionsRequest(new set<string>{soId});
                
                if(mapRequest != null && mapRequest.containsKey(giic_Constants.PROMOCODE)) mapRequest.put(giic_Constants.PROMOCODE, new set<String>{String.valueOf((mapRequest.get(giic_Constants.PROMOCODE)))});
                if(mapRequest != null && mapRequest.containsKey(giic_Constants.SOIDS)) mapRequest.put(giic_Constants.SOIDS, new set<String>{String.valueOf(mapRequest.get(giic_Constants.SOIDS))});
                if(mapRequest != null && mapRequest.containsKey('PCDELETEIDS')) mapRequest.put('PCDELETEIDS', new set<String>{String.valueOf(mapRequest.get('PCDELETEIDS'))});
                
                mapRequest.put('PROMOTIONREQUESTWRAPPER',JSON.serialize(lstPromoReqHeader));
                mapResponse = giic_PromotionHelper.applyPromotion(mapRequest);
            }
       }catch(Exception ex)
       {
           throw new AuraHandledException(ex.getMessage()); 
       }
        return mapResponse;
         
        
    }
    /*
    * Method name : applyPromotion
    * Description : apply the promotion for the promo code entered on the screen
    * Return Type : list<giic_PromotionWrapper.PromoRequestHeader>
    * Parameter : SOIDS = > set of sales order id
    */
    @auraEnabled
    Public static map<string,object> applyPromotion(map<string,object> mapRequest)
    {
       map<string,object> mapResponse;
       try
       {
            
            String soid =  (mapRequest != null && mapRequest.containsKey(giic_Constants.SOIDS))? String.valueOf(mapRequest.get(giic_Constants.SOIDS)):null;
            if(soid != null)
            {   
                list<giic_PromotionWrapper.PromoRequestHeader> lstPromoReqHeader = preparepromotionsRequest(new set<string>{soId});
                
                if(mapRequest != null && mapRequest.containsKey(giic_Constants.PROMOCODE)) mapRequest.put(giic_Constants.PROMOCODE, new set<String>{String.valueOf((mapRequest.get(giic_Constants.PROMOCODE)))});
                if(mapRequest != null && mapRequest.containsKey(giic_Constants.SOIDS)) mapRequest.put(giic_Constants.SOIDS, new set<String>{String.valueOf(mapRequest.get(giic_Constants.SOIDS))});
                //if(mapRequest != null && mapRequest.containsKey('PCDELETEIDS')) mapRequest.put('PCDELETEIDS', new set<String>{String.valueOf(mapRequest.get('PCDELETEIDS'))});
                mapRequest.put('ISMANUALPROMO',true);
                mapRequest.put('PROMOTIONREQUESTWRAPPER',JSON.serialize(lstPromoReqHeader));
                mapResponse = giic_PromotionHelper.validatePromoCode(mapRequest);
                //giic_AvalaraServieProviderClass.CalculateTaxforSOL(soid);
            }
       }catch(Exception ex)
       {
           throw new AuraHandledException(ex.getMessage()+ex.getLineNumber()); 
       }
        return mapResponse;
         
        
    }
    /*
    * Method name : calculateTax
    * Description : calculate the tax and return the promotion wrapper 
    * Return Type : list<giic_PromotionWrapper.PromoRequestHeader>
    * Parameter : soId = > sales order id
    */
    @auraEnabled
    public static Map<String,Object> calculateTax(String soId){
        List<gii__SalesOrder__c>  lstobjOrder = [select id,gii__Warehouse__r.giic_WarehouseCode__c, 
        					  gii__Account__r.Check_if_you_are_Tax_Exempt__c,
                              gii__Account__r.Resale_Sales_Tax_Cert_Expiration_Date__c,giic_TaxExempt__c
        					  from gii__SalesOrder__c where id =: soId ];
        if(lstobjOrder.size()>0)
        {
            //Check for Account Tax Exemption before Calculating Tax
            /*if(lstobjOrder[0].gii__Account__r.Check_if_you_are_Tax_Exempt__c != true || 
               (lstobjOrder[0].gii__Account__r.Check_if_you_are_Tax_Exempt__c == true && 
                lstobjOrder[0].gii__Account__r.Resale_Sales_Tax_Cert_Expiration_Date__c < System.Today()))*/
            if(!lstobjOrder[0].giic_TaxExempt__c)
            {
	            Map<String,Object> result = giic_AvalaraServiceProviderClass.CalculateTaxforSO(lstobjOrder[0].id,null,lstobjOrder[0].gii__Warehouse__r.giic_WarehouseCode__c,giic_Constants.COMMIT_T_FALSE,True);
	            return applyAutoPromotion(new map<string,object>{'soId'=>lstobjOrder[0].id,'isInit'=> true});
            }
            else
            {
            	return applyAutoPromotion(new map<string,object>{'soId'=>lstobjOrder[0].id,'isInit'=> true});
            }
            
        }
        return new Map<String,Object>();
    }
    
   
   
}