/************************************************************************************
Version : 1.0
Name : giic_CalculateTaxforSOLQueue
Created Date : 09 Oct 2018
Function : queue to calculate tax
Modification Log :
Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/
public class giic_CalculateTaxforSOLQueue implements Queueable,Database.AllowsCallouts 
{
    private Map<String, Object> mapUpdateResult;
    private Map<String, Object> mapResult;
    private Id whasId;
    private List<gii__WarehouseShipmentAdviceLinesStaging__c> lstWHSAStagingLines;
    private Boolean isTaxExempt;
    
    public giic_CalculateTaxforSOLQueue (Map<String, Object> mapUpdateResult, Map<String, Object> mapResult, Id whasId, List<gii__WarehouseShipmentAdviceLinesStaging__c> lstWHSAStagingLines, Boolean isTaxExempt) {
            this.mapUpdateResult = mapUpdateResult;
            this.mapResult = mapResult;
            this.whasId = whasId;
            this.lstWHSAStagingLines = lstWHSAStagingLines; 
            this.isTaxExempt = isTaxExempt;
    }
    
    public void execute(QueueableContext queCont) {
        try{
            if(!isTaxExempt){
                //Call Avalara Service      
                Map<String,Object> result = giic_CreateInvoiceHelper.CalculateTax(whasId); // calculate tax
                if(result.get('SUCCESS') == true){
                    gii__WarehouseShipmentAdviceStaging__c objWSAS = new gii__WarehouseShipmentAdviceStaging__c(Id = whasId, giic_isTaxCommitted__c = true);
                    update objWSAS;
                    if(!Test.isRunningTest()) ID jobID = System.enqueueJob(new giic_SOShipmentQueue(mapUpdateResult, mapResult, whasId, lstWHSAStagingLines)); // call giic_SOShipmentQueue for create shipment and invoice
                }else{
                     update new gii__WarehouseShipmentAdviceStaging__c(id = whasId, giic_ProcessStatus__c = giic_Constants.PROCESSEDWITHERROR);
                }
            }else{ // dont call avelara update tax commited true
                gii__WarehouseShipmentAdviceStaging__c objWSAS = new gii__WarehouseShipmentAdviceStaging__c(Id = whasId, giic_isTaxCommitted__c = true);
                update objWSAS;
                if(!Test.isRunningTest()) ID jobID = System.enqueueJob(new giic_SOShipmentQueue(mapUpdateResult, mapResult, whasId, lstWHSAStagingLines)); // call giic_SOShipmentQueue for create shipment and invoice
            }
        }catch(exception e){
            System.debug('Exception::' + e.getMessage() + 'Line::' + e.getLineNumber());
            update new gii__WarehouseShipmentAdviceStaging__c(id = whasId, giic_ProcessStatus__c = giic_Constants.PROCESSEDWITHERROR);
            giic_CyberSourceRCCCUtility.handleErrorFuture(whasId, giic_Constants.USER_STORY_INVOICECREATION, giic_Constants.DML_EX_CODE, e.getMessage() + 'Line::' + e.getLineNumber());
        }
        
    }   
}