public with sharing class Utilities {

    public static List<Id> getMatrixedRolesForManager(Id currentManagerRoleId){
        List<Id> matrixedRoleIds = new List<Id>();
        //get all groups related to current manager role and type = 'Role'
        List<Group> allCurrentManagerGroups = [select Id from Group where relatedId =: currentManagerRoleId and Type = 'Role'];
        if(allCurrentManagerGroups.size() > 0)
        {
            //get all group members with the userorgroupid being the result of query above
            List<Id> grpids = new List<Id>();
            for(Group grp : allCurrentManagerGroups)
            {
                grpids.add(grp.id);
            }
            List<GroupMember> allLvl1GroupMembers = [select id, groupid from GroupMember where UserOrGroupId in : grpids];
            if(allLvl1GroupMembers.size() > 0)
            {
                //get the groups related to the group memebers and get the ones that have matrix in them
                List<Id> grpids2 = new List<Id>();
                for(GroupMember gmem : allLvl1GroupMembers)
                {
                    grpids2.add(gmem.groupid);
                }
                List<Group> lvl2Groups = [select id, name from Group where id in : grpids2 and name like '%Matrix%'];
                if(lvl2Groups.size() > 0)
                {
                    //get group members with the group id from above query
                    List<Id> grpids3 = new List<Id>();
                    for(Group grps : lvl2Groups)
                    {
                        grpids3.add(grps.id);
                    }
                    List<GroupMember> lvl2GroupMembers = [select id, userorgroupid from GroupMember where groupId in : grpids3];
                    if(lvl2GroupMembers.size() > 0)
                    {
                        //get related to id from groups with group id from above
                        List<Id> grpids4 = new List<Id>();
                        for(GroupMember grps : lvl2GroupMembers)
                        {
                            grpids4.add(grps.userorgroupid);
                        }                       
                        List<Group> lastRoundGroups = [select id, relatedid from Group where id in : grpids4 and relatedid !=: currentManagerRoleId];
                        if(lastRoundGroups.size() > 0)
                        {
                            for(Group gp : lastRoundGroups)
                            {
                                matrixedRoleIds.add(gp.relatedid);
                            }
                        }
                    }
                }
            }
        }
        return  matrixedRoleIds;
    }
    
     public static String AddTeamMembers(List<Account> accts)
    {
        /*Set<Id> ownerIds = new Set<Id>();
        List<AccountTeamMember> atms = new List<AccountTeamMember>();
        List<AccountShare> accss = new List<AccountShare>();
        AccountTeamMember atm;
        AccountShare accs;
        for(Account acct : accts)
        {
            if(acct.LeadStatus__c != 'Archived') {
                System.debug('Owner of accout ' + acct.Id + ' is ' + acct.OwnerId);
                ownerIds.add(acct.OwnerId);
            }
                
        }
        Map<id, List<Id>> managerReps = Utilities.getManagersAndSubordinates(ownerIds);
        for(Account acct : accts)
        {
            if(managerReps.get(acct.OwnerId) != null)
            {
                for(Id repId : managerReps.get(acct.OwnerId))
                {
                    atm = new AccountTeamMember();
                    atm.AccountId = acct.id;
                    atm.UserId = repId;
                    atm.TeamMemberRole = 'Sales Representative';
                    atms.add(atm);
                    accs = new AccountShare();
                    accs.AccountAccessLevel = 'Edit';
                    accs.OpportunityAccessLevel = 'None';
                    //accs.ContactAccessLevel = 'None';
                    accs.CaseAccessLevel = 'None';
                    accs.AccountId = acct.id;
                    accs.UserOrGroupId = repId;
                    accss.add(accs);
                }
            }
        }
        insert atms;
        insert accss;
        return '';*/
        return '';
    }

    public static Id getGlobalUnassignedUser()
    {
    	String resaleAdmin = ResaleGlobalVariables__c.getinstance('GlobalResaleAdmin').value__c;
    	resaleAdmin = '%' + resaleAdmin + '%';
        User GUUser = [select id, name from User where Name like : resaleAdmin Limit 1];
        return GUUser.Id;
    }
    
}