/************************************************************************************
Version : 1.0
Created Date : 10 Oct 2018
Function : THis API will be called from insite to send the promotion detials
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/
@RestResource(urlMapping='/PromotionDetails/*')
global with sharing class giic_PromotionDetailAPI {
    /*
    * Method name : getPromotionDetails
    * Description : get the Promotion details using the request and send back the promotion applied on the order
    * Return Type : JSON response  list<giic_PromotionWrapper.SalesOrder>
    * Parameter : RestContext.request json format request
    */
    @HttpPOST
    global static list<giic_PromotionWrapper.SalesOrder> getPromotionDetails() {
        RestRequest request = RestContext.request;
        RestResponse response = RestContext.response;
        map<string,object> mapResponse = giic_PromotionHelper.validatePromoCode(new map<string,object>{'PROMOTIONREQUESTWRAPPER'=>request.requestBody.toString(),'APICALL'=>true});
        return (list<giic_PromotionWrapper.SalesOrder>)mapResponse.get('SOINFOAPI');
    }
}