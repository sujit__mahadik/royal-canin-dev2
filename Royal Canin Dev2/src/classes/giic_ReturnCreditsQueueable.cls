/************************************************************************************
Version : 1.0
Name : giic_ReturnCreditsQueueable
Created Date : 05 Oct 2018
Function : Helper class for Case Trigger
Author : Abhishek Tripathi
Modification Log :
Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/
public class giic_ReturnCreditsQueueable implements Queueable, Database.AllowsCallouts{
    private List<gii__ARCredit__c> lstARC;
    private List<gii__ARCreditPayment__c> lstArcPay;
    private Map<id, gii__ARCredit__c> mapARCRef;


        /***********************************************************
* Method name : giic_ReturnCreditsQueueable
* Description : Constructor
* Return Type : N/A
* Parameter : Map<id, gii__ARCredit__c> mapARC, List<gii__ARCreditPayment__c> lstARCPaymnt
***********************************************************/
    public giic_ReturnCreditsQueueable(Map<id, gii__ARCredit__c> mapARC, List<gii__ARCreditPayment__c> lstARCPaymnt){
        lstARC = mapARC.values(); 
        lstArcPay = lstARCPaymnt;
        mapARCRef = mapARC;
    }
    
            /***********************************************************
* Method name : execute
* Description : Interface implementation
* Return Type :None
* Parameter : QueueableContext context
***********************************************************/
    
    public void execute(QueueableContext context) {
        
        List<Integration_Error_Log__c> lstErrors = new List<Integration_Error_Log__c>();
        List<gii__ARCreditPayment__c> arcpList = new List<gii__ARCreditPayment__c>();
        Map<String, Object> mapParams = new Map<String, Object>();
        gii__ARCreditPayment__c arcpayObj = lstArcPay[0]; // AR Credit Payment record for which refund is being made.
        gii__ARCredit__c ar = mapARCRef.get(arcpayObj.gii__ARCredit__c);
            giic_BillToWrapper billTo = new giic_BillToWrapper();
                billTo.OrderNumber = ar.gii__SalesOrder__r.Name;
            billTo.FulfillmentNumber = ar.gii__Invoice__r.giic_WarehouseShippingOrderStaging__r.Name;
            mapParams.put('billTo', billTo);
            mapParams.put('totalAmount', arcpayObj.gii__PaidAmount__c);
    
     Map<String, Object> mapResult;
        if(Test.isRunningTest()){
            if(arcpayObj.gii__PaidAmount__c == 70){
                mapResult = new Map<String, Object>{'decision' => 'ACCEPT','amount' => 100 }; 
            }else{
               mapResult = new Map<String, Object>{'decision' => 'REJECT','amount' => 100 }; 
            }
        }else{
          mapResult = giic_CyberSourceRCCCUtility.refundRequestRCCC(mapParams); 
        }
        createARPayment(ar, mapResult, arcpList, lstErrors);
        if(!arcpList.isEmpty()) insert arcpList;
        if(!lstErrors.isEmpty()) insert lstErrors;
        if(ar != null) update ar;
        lstArcPay.remove(0);
        if(!lstArcPay.isEmpty()) ID jobID = System.enqueueJob(new giic_ReturnCreditsQueueable(mapARCRef, lstArcPay));
    }
   
   
/***********************************************************
* Method name : createARPayment
* Description : AR Payment record creation.
* Return Type :None
* Parameter :  gii__ARCredit__c ar, Map<String, Object> mapResult, List<gii__ARCreditPayment__c> arcpList,  List<Integration_Error_Log__c> lstErrors
***********************************************************/
    
   public static void createARPayment(gii__ARCredit__c ar, Map<String, Object> mapResult, List<gii__ARCreditPayment__c> arcpList,  List<Integration_Error_Log__c> lstErrors){
        if(!mapResult.isEmpty() && String.valueOf(mapResult.get('decision')).equalsIgnoreCase('ACCEPT')) {
            gii__ARCreditPayment__c acp = new gii__ARCreditPayment__c();
            acp.gii__ARCredit__c = ar.Id;
            acp.gii__PaymentDate__c = System.today();
            acp.gii__PaidAmount__c = Decimal.valueOf(String.valueOf(mapResult.get('amount')));
            acp.gii__PaymentMethod__c = giic_Constants.CREDIT_CARD;
            arcpList.add(acp);
         
            ar.giic_SettlementStatus__c	= giic_Constants.CONFIRMED_STATUS;
        }else if(String.valueOf(mapResult.get('decision')).equalsIgnoreCase('REJECT')) {
            // added the line below to update status of giic_SettlementStatus__c in ARCredit
            ar.giic_SettlementStatus__c	= giic_Constants.PROCESSWITHERROR;
            Set<String> stErrorIds = new Set<String>();
            String userStory = giic_Constants.ARCREDIT_USERSTORY;
            Map<String, String> relatedToFieldApi = new Map<String, String>{'giic_ARCredit__c' => 'Id'};
                String errorCode = String.valueOf(mapResult.get('reasonCode'));
            String errorMessage = Label.giic_RefundNotSuccess;
            giic_IntegrationCustomMetaDataUtil.collectErrors(lstErrors, stErrorIds, userStory, relatedToFieldAPi, ar, errorCode, errorMessage);
        }
    }
    
}