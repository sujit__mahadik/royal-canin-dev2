/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class OneTimeConsumerAccountCleanupBatchTest {

    static testMethod void testConsumerCleanupInsertPartersTest() {
		integer numAccounts = 110;
		RecordType rt = [SELECT Id FROM RecordType WHERE SobjectType = 'Account' AND developerName='Consumer'];
		
		SetupTestData.createCustomSettings();
		SetupTestData.createAccounts(numAccounts);

		integer index = 1;
		for (Account a: SetupTestData.testAccounts){
			/*
			** First 10 Accounts are Clinic Accounts
			** Each subsequent 10 Accounts will have their respective Clinic Account in ParentAccountId field
			** Account 11 to 20 - Parent = Account 1
			** Account 21 to 30 - Parent = Account 2
			** ...
			** Account 101 to 110 - Parent = Account 10
			*/
			a.RecordTypeId = rt.Id;
			if (index <= 10) {
				// Do Nothing
			} else {
				integer n = index/10;
				a.ParentId = SetupTestData.testAccounts[n].Id;
			}
			//

			index ++;
		}
		
		update SetupTestData.testAccounts;

		test.startTest();
		OneTimeConsumerAccountCleanupBatch otcacb = new OneTimeConsumerAccountCleanupBatch(false, true);
		Database.executeBatch(otcacb);
		test.stopTest();
		
		System.debug(loggingLevel.DEBUG, '********** GJK: Testing');
		system.debug([SELECT Id, AccountFromId__c, AccountToId__c, Role__c FROM Partner__c]);
		system.assertEquals([SELECT count() FROM Partner__c WHERE Role__c = 'Employee'], 100);
		system.assertEquals([SELECT count() FROM Partner__c WHERE Role__c = 'Employer'], 100);
    }

    static testMethod void testConsumerCleanupClearParentIdTest() {
		integer numAccounts = 11;
		RecordType rt = [SELECT Id FROM RecordType WHERE SobjectType = 'Account' AND developerName='Consumer'];
		
		SetupTestData.createCustomSettings();
		SetupTestData.createAccounts(numAccounts);

		integer index = 1;
		for (Account a: SetupTestData.testAccounts){
			/*
			** First Account is a Clinic Account
			** The rest 10 Accounts will have the first Clinic Account in ParentAccountId field
			** Account 2 to 11 - Parent = Account 1
			*/

			a.RecordTypeId = rt.Id;

			if (index == 1) {
				// Do Nothing
			} else {
				a.ParentId = SetupTestData.testAccounts[0].Id;
			}

			index ++;
		}
		
		update SetupTestData.testAccounts;

		test.startTest();
		OneTimeConsumerAccountCleanupBatch otcacb = new OneTimeConsumerAccountCleanupBatch(true, false);
		Database.executeBatch(otcacb);
		test.stopTest();
		
		System.debug(loggingLevel.DEBUG, '********** GJK: Testing');
		system.debug([SELECT Id, AccountFromId__c, AccountToId__c, Role__c FROM Partner__c]);
		system.assertEquals([SELECT count() FROM Account WHERE ParentId = NULL], 11);
    }
}