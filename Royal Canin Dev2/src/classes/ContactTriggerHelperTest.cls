@isTest
private class ContactTriggerHelperTest {
    
/*
 * Date:             - Developer, Company                          - Description
 * XXXX-XX-XX        - Joe Wrabel, Acumen Solutions                - Created
 * 2015-08-12        - Stevie Yakkel, Acumen Solutions             - Edited
 * 2015-08-04        - Joe Wortman, Acumen Solutions               - Edited
 */

    
    @isTest static void TestProcessConsumerAccount() {
        SetupTestData.createCustomSettings();
        SetupTestData.createAccounts(2);
       
        Contact con = new Contact();
        con.AccountId = setupTestData.testAccounts[0].Id;
        con.FirstName = 'TestFirst';
        con.LastName = 'TestLast';
        con.Email = 'test@test.com';
     
     
        Test.startTest();
            insert con;
        Test.stopTest();

        String accName = con.FirstName + ' ' + con.LastName;
        
        System.debug('accName: ' + accName);
        List<Account> newAccounts = [SELECT Id FROM Account WHERE Name = :accName];
      //  System.assert(!newAccounts.isEmpty());
        System.assertNotEquals(2, newAccounts.size(), 'Should only be 1 Account.');

      //  List<Partner__c> partners = [SELECT Id FROM Partner__c WHERE AccountToId__c = :setupTestData.testAccounts[0].Id];
      //  System.assertEquals(1, partners.size(), 'Should only be 1 partner.');
    }

    @isTest
    static void preventConsumerAccountLkpUpdateTest() {

    }
    
    

    @isTest
    static void checkForAddressChangesWithChangeTest() {
        SetupTestData.createCustomSettings();
        List<Contact> contacts = new List<Contact>();
        SetupTestData.createAccounts(2);

        Contact contactWithAddressChange = new Contact();
        contactWithAddressChange.AccountId = setupTestData.testAccounts[0].Id;
        contactWithAddressChange.FirstName = 'Stevie';
        contactWithAddressChange.LastName = 'Tester';
        contactWithAddressChange.MailingStreet = '123 main st.';
        contactWithAddressChange.MailingCity = 'Oxford';
        contactWithAddressChange.MailingState = 'OH';
        contactWithAddressChange.MailingPostalCode = '41385';
        contactWithAddressChange.OtherStreet = '123 main st.';
        contactWithAddressChange.OtherCity = 'Oxford';
        contactWithAddressChange.OtherState = 'OH';
        contactWithAddressChange.OtherPostalCode = '41385';
        contacts.add(contactWithAddressChange);
        insert contacts;
        contactWithAddressChange.OtherCity ='Xenia';
        Test.startTest();
        update contacts;
        Test.stopTest();
    }

    @isTest
    static void checkForAddressChangesWithoutChangeTest() {
        SetupTestData.createCustomSettings();
        
                List<Contact> contacts = new List<Contact>();
        SetupTestData.createAccounts(2);

        Contact contactWithoutAddressChange = new Contact();
        contactWithoutAddressChange.AccountId = setupTestData.testAccounts[0].Id;
        contactWithoutAddressChange.FirstName = 'Stevie';
        contactWithoutAddressChange.LastName = 'Tester';
        contactWithoutAddressChange.MailingStreet = '123 main st.';
        contactWithoutAddressChange.MailingCity = 'Oxford';
        contactWithoutAddressChange.MailingState = 'OH';
        contactWithoutAddressChange.MailingPostalCode = '41385';
        contactWithoutAddressChange.OtherStreet = '123 main st.';
        contactWithoutAddressChange.OtherCity = 'Oxford';
        contactWithoutAddressChange.OtherState = 'OH';
        contactWithoutAddressChange.OtherPostalCode = '41385';
        contacts.add(contactWithoutAddressChange);
        insert contacts;
        contactWithoutAddressChange.FirstName = 'Tester';
        contactWithoutAddressChange.LastName = 'Stevie';
        Test.startTest();
        update contacts;
        Test.stopTest();
    }

    @isTest
    static void formatPhoneInsertTest() {
        SetupTestData.createCustomSettings();
            SetupTestData.createAccounts(2);
            

        Contact c = new Contact();
        c.AccountId = setupTestData.testAccounts[0].Id;
        c.FirstName = 'format phone';
        c.LastName = 'tester';
        c.Phone = '1234567890';
        
        Test.startTest();
        insert c;
        Test.stopTest();
        Contact c2 = [SELECT Phone FROM Contact WHERE Id=:c.Id];
        System.assertEquals(c2.Phone, '(123) 456-7890');
    }

    @isTest
    static void formatPhoneTestUpdate() {
        SetupTestData.createCustomSettings();
        SetupTestData.createAccounts(2);

        Contact c = new Contact();
        c.AccountId = setupTestData.testAccounts[0].Id;
        c.FirstName = 'format phone';
        c.LastName = 'tester';
        c.Phone = '1234567890';
        
        insert c;
        c.Phone = '0987654321';
        Test.startTest();
        update c;
        Test.stopTest();
        Contact c2 = [SELECT Phone FROM Contact WHERE Id=:c.Id];
        System.assertEquals(c2.Phone, '(098) 765-4321');
    }

    @isTest
    static void callAddressValidateTest() {
        
    }
    
       @isTest
    static void setContactToMultipleAccountsTest() {
        SetupTestData.createCustomSettings();
            SetupTestData.createAccounts(2);
            Account acc = new Account();
            acc.Customer_Type__c = 'Clinic feeding Participant';
            Account acc1 = new Account();
            acc1.Customer_Type__c = 'Veterinary';
            
        Contact c = new Contact();
        c.AccountId = setupTestData.testAccounts[0].Id;
        c.FirstName = 'Test CFP';
        c.LastName = 'tester';
        c.Phone = '9090090009';
        c.ClinicCustomerNo__c = acc1.ShipToCustomerNo__c;
        Test.startTest();
        insert c;
        Test.stopTest();
       
    }

    @isTest
    static void createConsumerAccountTest() {
        
    } 

}