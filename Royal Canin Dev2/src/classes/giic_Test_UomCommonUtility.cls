/*************************************************************************************************************************************************************
Company      :  Fujitsu America 
Date Created :  05-10-2018
Author       :  Shivdeep Gupta
Description  :  This is the test class for giic_UnitofConversionProductTrigger trigger and giic_UomCommonUtility, giic_ProductUnitofMeasureConversionHlpr class               
Modified     :   
**************************************************************************************************************************************************************/

@isTest(SeeAllData = false)
public class giic_Test_UomCommonUtility {
    
    
    @testSetup
    public static void testSetupData(){
        
        List<gii__Product2Add__c> productListToUpdate = new List<gii__Product2Add__c> ();
        giic_Test_DataCreationUtility.insertWarehouse();
        giic_Test_DataCreationUtility.testSystemPolicyCreation2();
        //// code to insert product reference
        List<gii__Product2Add__c> testProductList = giic_Test_DataCreationUtility.insertProduct_N(); 
        
        // code for inserting Unit Of Measure
        giic_Test_DataCreationUtility.insertUnitofMeasure();
        
        
        testProductList[1].gii__StockingUnitofMeasure__c = giic_Test_DataCreationUtility.lstUnitofMeasure[1].Id;
        testProductList[1].gii__SellingUnitofMeasure__c = giic_Test_DataCreationUtility.lstUnitofMeasure[0].Id;
        
        testProductList[2].gii__StockingUnitofMeasure__c = giic_Test_DataCreationUtility.lstUnitofMeasure[0].Id;
        testProductList[2].gii__SellingUnitofMeasure__c = giic_Test_DataCreationUtility.lstUnitofMeasure[1].Id;
        
        testProductList[3].gii__StockingUnitofMeasure__c = giic_Test_DataCreationUtility.lstUnitofMeasure[0].Id;
        testProductList[3].gii__SellingUnitofMeasure__c = giic_Test_DataCreationUtility.lstUnitofMeasure[1].Id;
        
        testProductList[4].gii__StockingUnitofMeasure__c = giic_Test_DataCreationUtility.lstUnitofMeasure[0].Id;
        testProductList[4].gii__SellingUnitofMeasure__c = giic_Test_DataCreationUtility.lstUnitofMeasure[1].Id;
        
        testProductList[0].gii__StockingUnitofMeasure__c = giic_Test_DataCreationUtility.lstUnitofMeasure[0].Id;
        testProductList[0].gii__SellingUnitofMeasure__c = giic_Test_DataCreationUtility.lstUnitofMeasure[1].Id;
        
        productListToUpdate.add(testProductList[1]);
        productListToUpdate.add(testProductList[2]);
        productListToUpdate.add(testProductList[3]);
        productListToUpdate.add(testProductList[4]);
        productListToUpdate.add(testProductList[0]);
        
        update productListToUpdate;
        
        // code to insert Product UoM
        // giic_Test_DataCreationUtility.productUnitofMeasureCreation();
        giic_Test_DataCreationUtility.createAdminUser();
        
    }
    
    public static void querySetupData(){
        giic_Test_DataCreationUtility.lstUnitofMeasure = [select Id, name,gii__Description__c,  gii__DisplayName__c from gii__UnitofMeasure__c];
        giic_Test_DataCreationUtility.lstProdRef_N = [SELECT Id, gii__LotControlled__c, gii__ProductReference__c, gii__DefaultWarehouse__c, gii__ProductReference__r.Name, gii__NonStock__c, gii__ProductReference__r.ProductCode, gii__ProductReference__r.Family, gii__ProductReference__r.Product_Category__c, gii__ProductReference__r.SKU__c, gii__ProductReference__r.Food_Type__c, gii__ProductReference__r.Weight_Unit__c, gii__ProductReference__r.Weight__c, giic_ConvFactor__c, giic_RConvFactor__c, gii__SellingUnitofMeasure__c, gii__SellingUnitofMeasure__r.gii__Description__c, gii__SellingUnitofMeasure__r.gii__DisplayName__c, gii__SellingUnitofMeasure__r.gii__UniqueId__c, gii__StockingUnitofMeasure__c, gii__StockingUnitofMeasure__r.gii__Description__c, gii__StockingUnitofMeasure__r.gii__DisplayName__c, gii__StockingUnitofMeasure__r.gii__UniqueId__c   from gii__Product2Add__c];

        giic_Test_DataCreationUtility.lstWarehouse_N = [select id, Name, giic_WarehouseCode__c from gii__Warehouse__c];
        

        // List<gii__ProductUnitofMeasureConversion__c> lstProductUnitofMeasure = [select Id, gii__Product__c,gii__UnitofMeasureIn__c,  gii__UnitofMeasureOut__c,gii__QuantityIn__c,gii__QuantityOut__c   from gii__ProductUnitofMeasureConversion__c];
        
    }
    
    static testMethod void Test_CreateProductofUnit() 
    {
        User oUser = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        System.Runas(oUser)  
        {   
            querySetupData();
            giic_Test_DataCreationUtility.productUnitofMeasureCreation();
            Test.startTest();
            List<gii__ProductUnitofMeasureConversion__c> productdata = [SELECT gii__Product__c FROM gii__ProductUnitofMeasureConversion__c];
            
            giic_UomCommonUtility.UoMConversion wrapperobj = new giic_UomCommonUtility.UoMConversion();        
            //wrapperobj.SourceUoMId = TestUtilityClass.lstUoM[0].id;
            wrapperobj.SourceUoMId = giic_Test_DataCreationUtility.lstUnitofMeasure[0].id;
            wrapperobj.TargetUoMId = giic_Test_DataCreationUtility.lstUnitofMeasure[2].id;
            wrapperobj.ProductId = productdata[0].gii__Product__c;
            wrapperobj.qtyNeedToBeConvert = 1; 
            giic_UomCommonUtility.ConvertUoMBulk(new list<giic_UomCommonUtility.UoMConversion>{wrapperobj}, true);
            giic_UomCommonUtility.ConvertUOM(productdata[0].gii__Product__c,giic_Test_DataCreationUtility.lstUnitofMeasure[0].id,giic_Test_DataCreationUtility.lstUnitofMeasure[2].id,2,new Map<String, gii__ProductUnitofMeasureConversion__c>());
            // to run afterupdate code in trigger 
            update giic_Test_DataCreationUtility.lstUnitofMeasure[1];
            Test.stopTest();
        }
    }
    
    static testMethod void Test_ProductUnitOfMeasureConvesion() 
    {
        User oUser = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        System.Runas(oUser)  
        {   
            map<string, List<gii__ProductUnitofMeasureConversion__c>> mapProdrefVsLstPUOMC = new map<string, List<gii__ProductUnitofMeasureConversion__c>> ();
            querySetupData();
            Test.startTest();
            // giic_Test_DataCreationUtility.productUnitofMeasureCreation();
            List<gii__ProductUnitofMeasureConversion__c> lstProductUnitofMeasure = new List<gii__ProductUnitofMeasureConversion__c>();
            
            for(gii__Product2Add__c p2A : giic_Test_DataCreationUtility.lstProdRef_N ) { 
                List<gii__ProductUnitofMeasureConversion__c> lstofProdUnitofMeasure = new List<gii__ProductUnitofMeasureConversion__c>();
                gii__ProductUnitofMeasureConversion__c obj = new gii__ProductUnitofMeasureConversion__c();
                obj.gii__Product__c = p2A.id;
                obj.gii__UnitofMeasureIn__c = giic_Test_DataCreationUtility.lstUnitofMeasure[0].id;
                obj.gii__UnitofMeasureOut__c = giic_Test_DataCreationUtility.lstUnitofMeasure[1].id;
                obj.gii__QuantityIn__c = 1;
                obj.gii__QuantityOut__c = 2;        
                lstofProdUnitofMeasure.add(obj);
                lstProductUnitofMeasure.add(obj);
                
                gii__ProductUnitofMeasureConversion__c obj1 = new gii__ProductUnitofMeasureConversion__c();
                obj1.gii__Product__c = p2A.id;
                obj1.gii__UnitofMeasureIn__c = giic_Test_DataCreationUtility.lstUnitofMeasure[0].id;
                obj1.gii__UnitofMeasureOut__c = giic_Test_DataCreationUtility.lstUnitofMeasure[2].id;
                obj1.gii__QuantityIn__c = 1;
                obj1.gii__QuantityOut__c = 4;        
                lstofProdUnitofMeasure.add(obj1);
                lstProductUnitofMeasure.add(obj1);
                mapProdrefVsLstPUOMC.put(p2A.id,lstofProdUnitofMeasure);
            }
            
            insert lstProductUnitofMeasure;
            
            
            
             List<gii__ProductUnitofMeasureConversion__c> lstPUOMC = [select id,gii__Product__c,gii__UnitofMeasureIn__c,gii__UnitofMeasureOut__c,gii__QuantityIn__c,gii__QuantityOut__c,gii__ConversionFactor__c, gii__ReverseConversionFactor__c  from gii__ProductUnitofMeasureConversion__c where id in : lstProductUnitofMeasure];
            
            List<gii__ProductUnitofMeasureConversion__c> ListOfPUOMCToupdate = new List<gii__ProductUnitofMeasureConversion__c> ();
            integer counter =0;
            string strFirtProdrefId = '';
            for(string strPRId : mapProdrefVsLstPUOMC.keySet()){
                if(counter == 0){ // making changes only for 1 Product Refrence
                    strFirtProdrefId = strPRId;
                    List<gii__ProductUnitofMeasureConversion__c> ListOfPUOMC = mapProdrefVsLstPUOMC.get(strPRId);
                    if(ListOfPUOMC.size() > 0){
                        ListOfPUOMC[0].gii__QuantityOut__c = ListOfPUOMC[0].gii__QuantityOut__c + 1;
                        ListOfPUOMCToupdate.add(ListOfPUOMC[0]);
                    }
                    
                    
                }
                
                counter++;
            }
            
            if(ListOfPUOMCToupdate.size()> 0){
                update ListOfPUOMCToupdate;
            }
            
            
            giic_ProductUnitofMeasureConversionHlpr objPUOMCHelp=new giic_ProductUnitofMeasureConversionHlpr();
            list<gii__Product2Add__c> lstProductRef = [select id,gii__SellingUnitofMeasure__c,gii__StockingUnitofMeasure__c from gii__Product2Add__c ];
            if(!lstProductRef.isEmpty()){
                lstProductRef[0].gii__SellingUnitofMeasure__c=giic_Test_DataCreationUtility.lstUnitofMeasure[0].id;
                lstProductRef[0].gii__StockingUnitofMeasure__c=giic_Test_DataCreationUtility.lstUnitofMeasure[2].id;
                update lstProductRef[0];
                objPUOMCHelp.returnUoMConversionList(new map<id,gii__Product2Add__c>(lstProductRef));
            }
            
            Test.stopTest();
        }
    }
    
}