/**********************************************************
* Purpose    : The purpose of this class to schedule Sales Order Staging Address Validation Batch.  
*              
* *******************************************************
* Author                        Date            Remarks
* Devanshu Sharma               20/09/2018     
*********************************************************/
public class giic_SOSAddressValidationScheduler{
    
    public gii__SystemPolicy__c objSystemPolicy;
    
    public giic_SOSAddressValidationScheduler(ApexPages.StandardController stdController){
        objSystemPolicy=(gii__SystemPolicy__c)stdController.getRecord();
    }
    
    
    /*
    * Method name : scheduleBatch
    * Description : Schedule batches after checking for duplicates.
    * Return Type : PageReference
    * Parameter : 
    */
    public PageReference scheduleBatch()
    {          
        try
        { 
            Map<Id, CronTrigger> cronMap = new Map<Id, CronTrigger>([Select Id From CronTrigger WHERE CronJobDetail.Name like 'giic_SOSAddressValidationBatchSchedule%']);
            System.debug(cronMap);
            if(cronMap.size() == 0 && cronMap != null)
            {
                scheduler();
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.CONFIRM, 'Batch Scheduled.'));
            } 
            else 
            {
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.INFO, 'Batch is already scheduled.'));      
            }  
        }
        catch(Exception ex)
        {
            System.debug('Excpetion+++'+ex.getMessage());
            System.debug('Excpetion+++'+ex.getLineNumber());
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, ex.getMessage()));   
        }
        return null;
    }
    
    /*
    * Method name : scheduler
    * Description : Schedules the batch through dynamic scheduling.
    * Return Type : PageReference
    * Parameter : 
    */   
    private void scheduler()
    {        
        List<gii__SystemPolicy__c> lstSP = new List<gii__SystemPolicy__c>([Select Id, giic_AddressValidationJobTime__c from gii__SystemPolicy__c where Name = 'GII' limit 1]);
        System.debug('lstSP ' + lstSP ) ;
        if(lstSP.size()>0)
        {
            if(lstSP[0].giic_AddressValidationJobTime__c < 60)
            {
                DateTime myDateTime = System.Now() ;                
                Integer iHour = myDateTime.hour() ;             
                Integer iMinute = myDateTime.minute() ;                
                Integer iSecond = myDateTime.second() ;                             
                for(Integer i = 0; i < 60 ; i = Integer.valueof(i + lstSP [0].giic_AddressValidationJobTime__c))
                { 
                    if(iSecond > 59){
                        iSecond = 0 ;
                    }  
                    if(iMinute > 55){
                        iMinute = 0 ;
                        iHour = iHour + 1 ;
                    } else{
                        iMinute = iMinute + Integer.valueof(lstSP [0].giic_AddressValidationJobTime__c) ;
                    }
                    String sExpression = String.valueOf(iSecond) + ' ' + String.valueOf(iMinute) + ' ' + '*' +  ' ' + '*' + ' ' + '*' + ' ' + '?' + ' ' + '*' ;                    
                    String jobName =  'giic_SOSAddressValidationBatchScheduler'+i;
                    giic_SOSAddressValidationBatchScheduler oScheduledBatchJob_APIName = new giic_SOSAddressValidationBatchScheduler () ;
                    System.schedule(jobName , sExpression, oScheduledBatchJob_APIName) ;
                }
            }
            else
            {            
                Integer jTime = Integer.valueOf(lstSP [0].giic_AddressValidationJobTime__c );
                String hr = (Integer.valueOf(jTime/60) > 0) ? '*/' + String.valueOf(jTime/60) : '*' ;
                String mnt=(Integer.valueOf(jTime/60) > 0) ? String.valueOf(Math.mod(jTime, 60)) : String.valueOf((Integer.valueOf(jTime)));
                String cronExp = '0 '+ mnt +' '+hr+' ? * *';                
                String jobName =  'giic_SOSAddressValidationBatchScheduler';
                giic_SOSAddressValidationBatchScheduler oScheduledBatchJob_APIName = new giic_SOSAddressValidationBatchScheduler () ;
                System.schedule(jobName , cronExp , oScheduledBatchJob_APIName) ;
                //giic_AllocationBatch.scheduler(cronExp);
            }            
        }     
    }
}