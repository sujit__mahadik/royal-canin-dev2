/************************************************************************************
Version : 1.0
Name : giic_CreditCardServiceHandler
Created Date : 12 Sep 2018
Function : Handle all credit card related services.
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/
public with sharing class giic_DemoPOCreditCardServiceHandler { 
    
    private static Decimal totalAmount = null;

    /*
    * Method name : getCardList
    * Description : call this method from js init method
    * Return Type : Map<String, Object>
    * Parameter : recordId in String format
    */
    @AuraEnabled
    public static Map<String, Object> getCardList(String recordId){
        Map<String, Object> mapResult = new Map<String, Object>();
        List<giic_CreditCardWrapper> lstCCWrapper = new List<giic_CreditCardWrapper>();
        String shipToCustomerNo = '';
        giic_BillToWrapper billToWrap = new giic_BillToWrapper();
       // Decimal totalAmount = 0;
        String sObjName = Id.valueOf(recordId).getSObjectType().getDescribe().getName();
 
        // To handle action from Invoice detail.
        if(sObjName.equals('gii__APInvoice__c')){

            gii__APInvoice__c objOI = [SELECT Id,gii__TotalDistributionAmount__c, gii__AccountReference__r.gii__Account__r.ShipToCustomerNo__c, giic_PaymentMethod__c, 
                                          gii__BalanceAmount__c,
                                           (select id from gii__APInvoicePayments__r)
                                            FROM gii__APInvoice__c WHERE Id =:recordId];
            if(objOI.gii__APInvoicePayments__r!= null && objOI.gii__APInvoicePayments__r.size() > 0){
                 mapResult.put('arListSize', objOI.gii__APInvoicePayments__r.size());
                 mapResult.put('isReleased', false); 
                 mapResult.put('isCancelled', false); 
                 return mapResult;
            }
            shipToCustomerNo = objOI.gii__AccountReference__r.gii__Account__r.ShipToCustomerNo__c;
            mapResult.put('paymentMethod', objOI.giic_PaymentMethod__c);
            //getting Authorized payment.

            Double avilableAuthAmount = objOI.gii__BalanceAmount__c;
            mapResult.put('authorizedAmount', avilableAuthAmount);
            mapResult.put('isReleased', false);
            mapResult.put('isCancelled', false); 
        }
        mapResult.put('TotalAmount', totalAmount);
        mapResult.put('CardList', giic_CyberSourceRCCCUtility.GetCCProcessorByIDRCCC(shipToCustomerNo));
        
        for(giic_CreditCardWrapper card : (List<giic_CreditCardWrapper>) mapResult.get('CardList')){
            card.utilFields = new Map<String,String>{'SObjName'=>sObjName, 'recordId' => recordId, 'shipToCustomerNo'=>shipToCustomerNo};
        }
        
        giic_CreditCardWrapper newCard = new giic_CreditCardWrapper();
        newCard.utilFields = new Map<String,String>{'SObjName'=>sObjName, 'recordId' => recordId, 'shipToCustomerNo'=>shipToCustomerNo};
        mapResult.put('NewCard', newCard);
        mapResult.put('sObjectType', sObjName); // Added to handle different views viz. Invoice/SalesOrder/Account
        
        return mapResult;
    }

    /*
    * Method name : editCreditCardInfo
    * Description : call this method to edit the card in CyberSource and update the return
                    value in RC-CC server in case of valid response.
    * Return Type : Map<String, Object>
    * Parameter : recordId in String format and card to edit in JSON string format
    */
    @AuraEnabled
    public static Map<String, Object> editCreditCardInfo(String editCard, String recId){
        
        giic_CreditCardWrapper editCardWrapper = (giic_CreditCardWrapper) JSON.deserialize(editCard, giic_CreditCardWrapper.class);

        Map<String, Object> mapResult = new Map<String, Object>(); 
        
        //mapResult = getCardList(recId); //Call this method of get utilFields
        
        Map<String, Object> updateCardDetailCCReq = new Map<String, Object>(); 
        updateCardDetailCCReq.put('card', editCardWrapper);
        
        Map<String, Object> updateCardDetailCCRes = giic_CyberSourceRCCCUtility.updateCardDetailCC(updateCardDetailCCReq);
        
        if(!updateCardDetailCCRes.containsKey('error')){ //check res.getStatusCode()
            if (updateCardDetailCCRes.get('decision').equals('ACCEPT') && updateCardDetailCCRes.get('reasonCode').equals('100')){
                
                String subscriptionID = (String)updateCardDetailCCRes.get('subscriptionID');
                //Preform update on RC-CC Server
                Map<String, Object> updateCCAtRCCCServRes = giic_CyberSourceRCCCUtility.updateCCAtRCCCServ(editCardWrapper, subscriptionID);
                
                if(updateCCAtRCCCServRes.containsKey('error')) mapResult.put('error', updateCCAtRCCCServRes.get('error')); //DML Error
                //Else show the updated list
                else  mapResult.put('CardList', giic_CyberSourceRCCCUtility.GetCCProcessorByIDRCCC((String)updateCCAtRCCCServRes.get('shipToCustomer')));
               
            }
            else{
                //Update integration error log with reasonCode and the operation
                 mapResult.put('error', System.Label.giic_CCProfileUpdateNoRes + System.Label.giic_ErrReasonCode +updateCardDetailCCRes.get('reasonCode') ); 
            }
            
        } else{
            //Update integration error log with res.getStatusCode() and the operation
            mapResult.put('error', updateCardDetailCCRes.get('error'));
        }

        return mapResult;
    }

    /*
    * Method name : getCardList
    * Description : call this method from js init method
    * Return Type : Map<String, Object>
    * Parameter : recordId in String format
    */
    
      //update default card on server
     @AuraEnabled
    public static String updateCardList(String strCardList){
        
        List<giic_CreditCardWrapper> cardList = (List<giic_CreditCardWrapper>) JSON.deserialize(strCardList, List<giic_CreditCardWrapper>.class);
        
       try{
           giic_CyberSourceRCCCUtility.updateCardListTOCSAndRCCC(cardList);
           return 'SUCCESS';
       }catch(exception ex){
           return ex.getmessage();
       }
     // return 'SUCCESS';
    }
    
    /*
    * Method name : getCardList
    * Description : call this method from js init method
    * Return Type : Map<String, Object>
    * Parameter : recordId in String format
    */
    
     // Auth Payment after avelara tax calc
    public static void authorizePayment(String soId){
        giic_CreditCardWrapper selectedCardWrapper = new giic_CreditCardWrapper();
         gii__APInvoice__c apInvoice = [SELECT Id, gii__AccountReference__r.gii__Account__r.BillingStreet, gii__AccountReference__r.gii__Account__r.BillingState, gii__AccountReference__r.gii__Account__r.BillingPostalCode, 
                              gii__AccountReference__r.gii__Account__r.BillingCity, gii__AccountReference__r.gii__Account__r.BillingCountry, gii__AccountReference__r.gii__Account__r.Business_Email__c, gii__AccountReference__r.gii__Account__r.ShipToCustomerNo__c
                              FROM gii__APInvoice__c 
                               WHERE Id =: soId];
                               
        String shipToCustomerNo = apInvoice.gii__AccountReference__r.gii__Account__r.ShipToCustomerNo__c;
        List<giic_CreditCardWrapper> lstCardWrapper = giic_CyberSourceRCCCUtility.GetCCProcessorByIDRCCC(shipToCustomerNo); 
        if(!lstCardWrapper.isEmpty()){
            for(giic_CreditCardWrapper objCard : lstCardWrapper){
                if(objCard.IsDefaultCard) selectedCardWrapper = objCard;
            }
            authorizePaymentToCybersource(JSON.serialize(selectedCardWrapper), soId);
        }
    
    }
    /*
    * Method name : getCardList
    * Description : call this method from js init method
    * Return Type : Map<String, Object>
    * Parameter : recordId in String format
    */
    
    
    //Authorize payment from Cybersource
     @AuraEnabled
    public static Map<String, Object> authorizePaymentToCybersource(String selectedCard, String recId){
        Map<String, Object> mapResult = new Map<String, Object>();
        giic_CreditCardWrapper selectedCardWrapper = (giic_CreditCardWrapper) JSON.deserialize(selectedCard, giic_CreditCardWrapper.class);
        
        giic_BillToWrapper billToWrap = new giic_BillToWrapper();
        
        Account acc = [SELECT Id, BillingStreet, BillingState, BillingPostalCode, 
                              BillingCity, BillingCountry, Business_Email__c 
                              FROM Account 
                               WHERE ShipToCustomerNo__c =: selectedCardWrapper.ShipToCustomerNo];
                
        billToWrap.firstName = selectedCardWrapper.NameOnCard.split(' ').get(0) != null ? selectedCardWrapper.NameOnCard.split(' ').get(0) : '';
        billToWrap.lastName = selectedCardWrapper.NameOnCard.split(' ').get(1) != null ? selectedCardWrapper.NameOnCard.split(' ').get(1) : '';
        billToWrap.street = acc.BillingStreet;
        billToWrap.city = acc.BillingCity;
        billToWrap.state = acc.BillingState;
        billToWrap.postalCode = acc.BillingPostalCode;
        billToWrap.country = acc.BillingCountry;
        billToWrap.email = acc.Business_Email__c;

        Id soID ;
        if(Id.valueOf(recId).getSObjectType().getDescribe().getName() == 'gii__SalesOrder__c'){
            totalAmount = [Select gii__NetAmount__c from gii__SalesOrder__c where Id =: recId ].gii__NetAmount__c;
            soID = recId;
        }else if(Id.valueOf(recId).getSObjectType().getDescribe().getName() == 'gii__OrderInvoice__c'){
            gii__OrderInvoice__c orderInvoiceObj = [SELECT gii__NetAmount__c, gii__SalesOrder__c FROM gii__OrderInvoice__c WHERE Id =:recId];
            totalAmount = orderInvoiceObj.gii__NetAmount__c;
            soID = orderInvoiceObj.gii__SalesOrder__c;
        }
        else if(Id.valueOf(recId).getSObjectType().getDescribe().getName() == 'gii__APInvoice__c'){
            gii__APInvoice__c apInvoiceObj = [SELECT gii__BalanceAmount__c FROM gii__APInvoice__c WHERE Id =:recId];
            totalAmount = apInvoiceObj.gii__BalanceAmount__c;
            soID = recId;
        }
        
        Map<String, Object> mapParams = new Map<String, Object>();  
        mapParams.put('billTo', billToWrap);
        mapParams.put('totalAmount', totalAmount);
        mapParams.put('card', selectedCardWrapper);
        Map<String, Object> mapAuthorizationResult =  giic_CyberSourceRCCCUtility.paymentAuthorizationCyberSource(mapParams);
        if(mapAuthorizationResult != null && mapAuthorizationResult.get('decision') == 'ACCEPT' && mapAuthorizationResult.get('reasonCode') == '100'){//If CC settlement is successfull
            mapResult.put('MakepaymentResponce','SUCCESS');
            giic_CyberSourceRCCCUtility.updateAuthTokensAtRCCC(String.valueOf(mapAuthorizationResult.get('requestID')), String.valueOf(mapAuthorizationResult.get('requestToken')),selectedCardWrapper.CardNumber,selectedCardWrapper.ShipToCustomerNo, selectedCardWrapper);
            createUpdateSOPayment(soID, totalAmount); // create sales order payment record after authorization or update existing record
            mapResult.put('authorizedAmount', totalAmount); // update value for Authorized amount on view 
            mapResult.put('CardList', giic_CyberSourceRCCCUtility.GetCCProcessorByIDRCCC(selectedCardWrapper.ShipToCustomerNo));
        }else{
            mapResult.put('MakepaymentResponce',giic_Constants.PAYMENT_AUTH_FAIL);
            mapResult.put('CardList', giic_CyberSourceRCCCUtility.GetCCProcessorByIDRCCC(selectedCardWrapper.ShipToCustomerNo));
            mapResult.put('authorizedAmount', 0);
        }
            
        return mapResult;
    }
    /*
    * Method name : createUpdateSOPayment
    * Description : call this method from js init method
    * Return Type : Map<String, Object>
    * Parameter : recordId in String format
    */
    // create sales order payment record after authorization or update existing record
    public static void createUpdateSOPayment(String soId, Decimal totalAmount){
        if(soId != null && soId != ''){
            gii__APInvoicePayment__c objPayment;
            List<gii__APInvoicePayment__c> lstPayment = [Select Id, Name, gii__PaymentMethod__c, gii__PaymentDate__c, gii__PaidAmount__c, gii__APInvoice__c from  gii__APInvoicePayment__c where gii__APInvoice__c =: soId];
            if(lstPayment.isEmpty()) objPayment =  new gii__APInvoicePayment__c(gii__APInvoice__c = soId);
            else objPayment = lstPayment[0];
            objPayment.gii__PaymentMethod__c = giic_Constants.CREDIT_CARD;
            objPayment.gii__PaymentDate__c = System.today();
            objPayment.gii__PaidAmount__c = totalAmount != null ? totalAmount : 0;
            
            upsert objPayment;
            
           // gii__APInvoice__c objSO = new gii__APInvoice__c(Id = soId , gii__PostingDate__c = System.today());
           // update objSO;
        }
    }
    /*
    * Method name : makePaymentToCybersource
    * Description : call this method from js init method
    * Return Type : Map<String, Object>
    * Parameter : recordId in String format
    */
     @AuraEnabled
    public static Map<String, Object> makePaymentToCybersource(String selectedCard, String recId){
        Map<String, Object> mapResult = new Map<String, Object>();
        giic_CreditCardWrapper selectedCardWrapper = (giic_CreditCardWrapper) JSON.deserialize(selectedCard, giic_CreditCardWrapper.class);
        
        giic_BillToWrapper billToWrap = new giic_BillToWrapper();
        
        Account acc = [SELECT Id, BillingStreet, BillingState, BillingPostalCode, 
                              BillingCity, BillingCountry, Business_Email__c 
                              FROM Account 
                               WHERE ShipToCustomerNo__c =: selectedCardWrapper.ShipToCustomerNo];
                
                billToWrap.firstName = selectedCardWrapper.NameOnCard.split(' ').get(0) != null ? selectedCardWrapper.NameOnCard.split(' ').get(0) : '';
            billToWrap.lastName = selectedCardWrapper.NameOnCard.split(' ').get(1) != null ? selectedCardWrapper.NameOnCard.split(' ').get(1) : '';
            billToWrap.street = acc.BillingStreet;
            billToWrap.city = acc.BillingCity;
            billToWrap.state = acc.BillingState;
            billToWrap.postalCode = acc.BillingPostalCode;
            billToWrap.country = acc.BillingCountry;
            billToWrap.email = acc.Business_Email__c;

        Id soID ;
        if(Id.valueOf(recId).getSObjectType().getDescribe().getName() == 'gii__SalesOrder__c'){
            totalAmount = [Select gii__NetAmount__c from gii__SalesOrder__c where Id =: recId ].gii__NetAmount__c;
            soID = recId;
        }else if(Id.valueOf(recId).getSObjectType().getDescribe().getName() == 'gii__OrderInvoice__c'){
            gii__OrderInvoice__c orderInvoiceObj = [SELECT gii__NetAmount__c, gii__SalesOrder__c FROM gii__OrderInvoice__c WHERE Id =:recId];
            totalAmount = orderInvoiceObj.gii__NetAmount__c;
            soID = orderInvoiceObj.gii__SalesOrder__c;
        }
       
        Map<String, Object> mapParams = new Map<String, Object>();
        mapParams.put('billTo', billToWrap);
        mapParams.put('totalAmount', totalAmount);
        mapParams.put('card', selectedCardWrapper);
        //Map<String, Object> mapReqParams = new Map<String, Object>();
        Map<String, Object> mapReqParams = giic_CyberSourceRCCCUtility.getAuthIDAndTokenFromRCCC(selectedCardWrapper.CardNumber, selectedCardWrapper.ShipToCustomerNo);
        if(!mapReqParams.isEmpty()){
            mapParams.put('mapReqParams', mapReqParams);
            Map<String, Object> mapCapParams = giic_CyberSourceRCCCUtility.capturePaymentCyberSource(mapParams); 
            if(mapCapParams != null && mapCapParams.get('decision') == 'ACCEPT' && mapCapParams.get('reasonCode') == '100'){//If CC Capture is successfull
                mapParams.put('mapReqParams', mapCapParams);
                Map<String, Object> mapSettlementResult = giic_CyberSourceRCCCUtility.paymentSettlementCyberSource(mapParams);
                if(mapSettlementResult != null && mapSettlementResult.get('decision') == 'ACCEPT' && mapSettlementResult.get('reasonCode') == '100'){//If CC settlement is successfull
                    mapResult.put('MakepaymentResponce','SUCCESS');
                    //createPaymentSettlement(String.valueOf(mapSettlementResult.get('reconciliationID')), soID, totalAmount);
                    // Create ARInvoice record
                    gii__APInvoicePayment__c arInvoicePayObj = new gii__APInvoicePayment__c();
                    arInvoicePayObj.gii__PaymentMethod__c='Credit Card';
                    arInvoicePayObj.gii__APInvoice__c =recId; // Assigning invoiceId
                    arInvoicePayObj.gii__PaidAmount__c=totalAmount;
                    arInvoicePayObj.gii__PaymentDate__c=System.now().Date();
                    arInvoicePayObj.gii__Reference__c = String.valueOf(mapSettlementResult.get('reconciliationID'));
                    
                    gii__SalesOrder__c objSO = new gii__SalesOrder__c(Id = soID, giic_SOStatus__c = 'Settled');
                    try{
                        update objSO;
                        insert arInvoicePayObj;
                    }catch(DMLException e){
                    system.debug(e.getMessage());
                }

                }else{
                    mapResult.put('MakepaymentResponce', giic_Constants.PAYMENT_SETTLE_FAIL + ' - ' + mapSettlementResult.get('reasonCode'));
                    mapResult.put('CardList', giic_CyberSourceRCCCUtility.GetCCProcessorByIDRCCC(selectedCardWrapper.ShipToCustomerNo));
                }
            }else{
                mapResult.put('MakepaymentResponce', giic_Constants.PAYMENT_CAPTURE_FAIL + ' - ' + mapCapParams.get('reasonCode'));
                mapResult.put('CardList', giic_CyberSourceRCCCUtility.GetCCProcessorByIDRCCC(selectedCardWrapper.ShipToCustomerNo));
                Integration_Error_Log__c ieLog = new Integration_Error_Log__c(Name = 'Payment Capture Error');
                ieLog.Error_Code__c = String.valueOf(mapCapParams.get('reasonCode'));
                ieLog.giic_SalesOrder__c=soID;
                ieLog.Error_Message__c = String.valueOf(mapCapParams.get('ReasonDescription'));
                try{
                    insert ieLog;
                }catch(DMLException e){
                    system.debug(e.getMessage());
                }
                
            }
        }else{
            mapResult.put('MakepaymentResponce',giic_Constants.CARD_AUTH_ERROR);
            mapResult.put('CardList', giic_CyberSourceRCCCUtility.GetCCProcessorByIDRCCC(selectedCardWrapper.ShipToCustomerNo));
        }
        return mapResult;
    }
    /*
    * Method name : getCardList
    * Description : call this method from js init method
    * Return Type : Map<String, Object>
    * Parameter : recordId in String format
    */
   // Create Payment Settlement record if payment is done


}