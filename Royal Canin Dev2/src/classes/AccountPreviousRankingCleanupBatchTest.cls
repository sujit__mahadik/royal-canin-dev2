@isTest
private class AccountPreviousRankingCleanupBatchTest {

    static testMethod void testRankingCleanup(){
        integer numAccounts = 100;
        
        SetupTestData.createCustomSettings();
        SetupTestData.createAccounts(numAccounts);

        integer index = 0;
        for (Account a: SetupTestData.testAccounts){
            if (Math.mod(index,2) == 0) {
                // Even numbered index = 'Customer'
                a.Account_Status__c = 'Customer';
                if (Math.mod(index,5) == 0) {
                    a.Current_Fiscal_YTD__c = 0.0;
                    a.OLP_Current_Fiscal_YTD__c = 0.0;
                } else {
                    a.Current_Fiscal_YTD__c = NULL;
                    a.OLP_Current_Fiscal_YTD__c = NULL;
                }
                a.Order_Placed__c = true;
            } else {
                // Odd numbered index = 'Prospect'
                a.Account_Status__c = 'Prospect';
            }
            index ++;
        }
        
        update SetupTestData.testAccounts;
        
        test.startTest();
        Job_Failure_Notification_Email__c jfn = new Job_Failure_Notification_Email__c();
        jfn.Name = 'Email Id';
        jfn.Notification_Email__c = UserInfo.getUserEmail();
        insert jfn;
        AccountPreviousRankingCleanupBatch aprcb = new AccountPreviousRankingCleanupBatch();
        database.executeBatch(aprcb);
        test.stopTest();
        
        system.debug([select Current_Year_Territory_Assignment__r.TerritoryID__r.Name,Current_Fiscal_YTD__c, Territory_Ranking__c,Region_Ranking__c from Account]);
        system.assertEquals([select count() from Account where Region_Ranking__c=99999 and account_status__c = 'Customer'], numAccounts/2);
        system.assertEquals([select count() from Account where Region_Ranking__c=NULL and account_status__c = 'Prospect'], numAccounts/2);
    }

}