/************************************************************************************
Version : 1.0
Name : giic_Test_PromotionController
Created Date : 4 Oct 2018
Function : test class for giic_PromotionController 
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/

@isTest
private class giic_Test_PromotionController {
    @testSetup static void testData(){
         giic_Test_DataCreationUtility.insertWarehouse();
        giic_Test_DataCreationUtility.testSystemPolicyCreation();
        giic_Test_DataCreationUtility.insertGlobalParametersCustomSetting();
        giic_Test_DataCreationUtility.insertConsumerAccount();
        giic_Test_DataCreationUtility.insertWarehouse_N(2);
        giic_Test_DataCreationUtility.getAccountReference_N(giic_Test_DataCreationUtility.lstAccount);
        giic_Test_DataCreationUtility.insertProduct();
        giic_Test_DataCreationUtility.insertBulkSalesOrder();
        giic_Test_DataCreationUtility.insertBulkSOLine();
        giic_Test_DataCreationUtility.createAdminUser();
    }

	private static testMethod void promotionPositiveTest() {
	     User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
	     
	     giic_Test_DataCreationUtility.lstAccount=[select id,RecordTypeId,Name,BillingStreet,BillingCountry,BillingPostalCode,
	                                                BillingState,BillingCity,ShippingStreet,ShippingCountry,ShippingPostalCode,ShippingState,ShippingCity,
	                                                ClinicCustomerNo__c,ShipToCustomerNo__c,Business_Email__c,Bill_To_Customer_ID__c from Account];
	                                                
	    giic_Test_DataCreationUtility.accRefList_N=[select id, Name, gii__Account__c, gii__Carrier__c, gii__DefaultWarehouse__c  from gii__AccountAdd__c ];
	   
	    giic_Test_DataCreationUtility.lstSalesOrder=[select id,gii__Account__c,giic_CustomerPriority__c,gii__ScheduledDate__c,giic_SOStatus__c,
	                                                    gii__OrderDate__c,gii__Warehouse__c,giic_OrderNo__c,giic_TaxCal__c,gii__PaymentMethod__c,
	                                                    gii__Carrier__c,giic_NewPickDate__c,gii__Released__c from gii__SalesOrder__c];
	   
	     System.runAs(u){
	     
        test.startTest();
        
        giic_PromotionWrapper.PromoRequestLines objPromoRequestLines = new  giic_PromotionWrapper.PromoRequestLines();
        List<giic_PromotionWrapper.PromoRequestLines> lstPromoRequestLines = new  List<giic_PromotionWrapper.PromoRequestLines>();
        objPromoRequestLines.orderQuantity = 10;
        objPromoRequestLines.unitPrice = 50;
        lstPromoRequestLines.add(objPromoRequestLines);
        giic_PromotionWrapper.PromoRequestHeader ObjPromotionreqwrapper = new giic_PromotionWrapper.PromoRequestHeader();
        ObjPromotionreqwrapper.accountId=giic_Test_DataCreationUtility.lstAccount[0].id;
        ObjPromotionreqwrapper.accountRefId=giic_Test_DataCreationUtility.accRefList_N[0].id;
        //ObjPromotionreqwrapper.userId=lstUser[0].id;
        ObjPromotionreqwrapper.userRole='CSR';
        ObjPromotionreqwrapper.orderId =giic_Test_DataCreationUtility.lstSalesOrder[0].id;
        ObjPromotionreqwrapper.isDropShip=true;
        ObjPromotionreqwrapper.SalesOrderLine =lstPromoRequestLines;
         
        
        list<giic_PromotionWrapper.PromoRequestHeader> lstProWrapper = new list<giic_PromotionWrapper.PromoRequestHeader>();
         lstProWrapper.add(ObjPromotionreqwrapper);
         string jsonStr = JSON.serialize(lstProWrapper);
         
           gii__SalesOrder__c objSO=new gii__SalesOrder__c(gii__Account__c = giic_Test_DataCreationUtility.lstAccount[0].id,
                                            giic_SOStatus__c='Open' ,gii__ScheduledDate__c = SYSTEM.today(),giic_NewPickDate__c = SYSTEM.today(),gii__OrderDate__c = SYSTEM.today(),
                                              gii__Status__c = 'Open', gii__Released__c = true);
                                              
          insert objSO;
        
        
        set<string> setSOIds = new set<string>();
        setSOIds.add(giic_Test_DataCreationUtility.lstSalesOrder[0].id);
        map<string,object> mapRequesting =new map<string,object>();
        mapRequesting.put('PROMOTIONREQUESTWRAPPER',jsonStr);
         
         map<string,object> mapRequest=new map<string,object>();
         mapRequest.put('soId',objSO.id);
         giic_PromotionController.applyAutoPromotion(mapRequest);
         giic_PromotionController.calculateTax('objSO.id');
         giic_PromotionController.applyPromotion(mapRequesting);
         //giic_PromotionController.submitSalesOrderFromPromotionScreen('objSO.id');
         test.stopTest();
	     }
	}

}