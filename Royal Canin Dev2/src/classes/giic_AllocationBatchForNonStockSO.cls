/************************************************************************************
Version : 1.0
Created Date : 13 Aug 2018
Function : Process the Open Sales order for Allocation
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/
global class giic_AllocationBatchForNonStockSO implements Database.Batchable<sObject>{
    /*
    * Method name : start
    * Description : Query all the Open Sales order 
    * Return Type : Database.QueryLocator
    * Parameter : Database.BatchableContext BC
    */
    global Database.QueryLocator start(Database.BatchableContext BC){
       string query='select id  from gii__SalesOrder__c';
        query += ' where gii__Released__c=true and gii__Account__r.Blocked__c=false and id in (select gii__salesorder__C from gii__salesorderline__c where (((giic_LineStatus__c =\''+giic_Constants.SOL_INITIALSTATUS+'\' or giic_LineStatus__c=\''+giic_Constants.SOL_BACKORDERSTATUS+'\') and gii__NonStockQuantity__c =0) or giic_LineStatus__c=\'Rejected by DC\' )  and gii__LineStatus__c=\'Open\') order by giic_CustomerPriority__c asc nulls last';
        return Database.getQueryLocator(query);
    } 
    /*
    * Method name : execute
    * Description : Process the Open Sales Order for allocation
    * Return Type : nill
    * Parameter : Database.BatchableContext BC, List<sObject> scope
    */
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        giic_AllocationHelper.processNonStockAllocation(scope);
    }
    /*
    * Method name : finish
    * Description : Process any pending work after the batch complete
    * Return Type : nill
    * Parameter : Database.BatchableContext BC,
    */
     global void finish(Database.BatchableContext BC){
    }
}