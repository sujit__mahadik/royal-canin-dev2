/************************************************************************************
Version : 1.0
Created Date : 13 Aug 2018
Function : Process the Open Sales order for Allocation
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/
global class giic_AllocationBatch implements Database.Batchable<sObject>, Schedulable{
    
    public static final String jobName = system.label.giic_AllocationBatchName;
    /*
    * Method name : start
    * Description : Query all the Open Sales order 
    * Return Type : Database.QueryLocator
    * Parameter : Database.BatchableContext BC
    */
    global Database.QueryLocator start(Database.BatchableContext BC){
        string query='select id,gii__Account__c,giic_CustomerPriority__c,giic_SOStatus__c,gii__DropShip__c,giic_DropShipWHMapping__c,gii__AccountReference__r.giic_WarehouseGroup__c, gii__ScheduledDate__c,giic_NewPickDate__c,gii__OrderDate__c,gii__Warehouse__c,gii__Account__r.giic_Carrier__c,gii__Carrier__r.Name,gii__Account__r.giic_WarehouseDistanceMapping__c  from gii__SalesOrder__c';
        query += ' where gii__Released__c=true and gii__Account__r.Blocked__c=false and id in (select gii__salesorder__C from gii__salesorderline__c where (((giic_LineStatus__c =\''+giic_Constants.SOL_INITIALSTATUS+'\' or giic_LineStatus__c=\''+giic_Constants.SOL_BACKORDERSTATUS+'\') and gii__ReservedQuantity__c =0) or giic_LineStatus__c=\''+ giic_Constants.LINESTATUS_REJECTEDBYDC+ '\' )  and gii__LineStatus__c=\''+giic_Constants.SOL_OPENSTATUS+'\') order by giic_CustomerPriority__c asc nulls last';
        return Database.getQueryLocator(query);
    } 
    /*
    * Method name : execute
    * Description : Process the Open Sales Order for allocation
    * Return Type : nill
    * Parameter : Database.BatchableContext BC, List<sObject> scope
    */
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        giic_AllocationHelper.processAllocation(scope);
    }
    /*
    * Method name : finish
    * Description : Process any pending work after the batch complete
    * Return Type : nill
    * Parameter : Database.BatchableContext BC,
    */
    global void finish(Database.BatchableContext BC){
        //Execute Non Stock Batch for allocation
        database.executeBatch(new giic_AllocationBatchForNonStockSO());
    }
    /*
    * Method name : schedular
    * Description : Process the Open Sales Order for allocation
    * Return Type : Returns the job in String format
    * Parameter : String cronExp
    */ 
    
    global static String scheduler(String cronExp){
        giic_AllocationBatch allocation = new giic_AllocationBatch();
        return System.schedule(jobName, cronExp, allocation); 
    }
    
    /*
    * Method name : Schedule classes
    * Description : Process the Open Sales Order for allocation
    * Return Type : void
    * Parameter : SchedulableContext ctx
    */
    global void execute(SchedulableContext ctx) {
        giic_AllocationBatch batch = new giic_AllocationBatch();
        Database.executeBatch(batch);
    }
}