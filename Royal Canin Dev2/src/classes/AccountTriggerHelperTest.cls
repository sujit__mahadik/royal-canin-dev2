/*
 * Date:             - Developer, Company                          - Description
 * 2015-07-14        - Stevie Yakkel, Acumen Solutions             - Created
 * 2015-08-04        - Joseph Wortman, Acumen Solutions            - Edited
 */

@isTest
private class AccountTriggerHelperTest
{
    @isTest
    static void testAccRefUpdate() {
        SetupTestData.createCustomSettings();
        SetupTestData.createAccounts(1);
        Account acc = SetupTestData.testAccounts[0];
        gii__AccountAdd__c accRef = new gii__AccountAdd__c(
            gii__Account__c = acc.Id
        );
        insert accRef;
    }
    
    @isTest
    static void checkForAddressChangesWithChangeTest() {
        List<Account> accounts = new List<Account>();
        SetupTestData.createCustomSettings();
        SetupTestData.createAccounts(1);
        Account accountWithAddressChange = SetupTestData.testAccounts[0];
        accountWithAddressChange.BillingCity ='Xenia';
        Test.startTest();
        update accounts;
        Test.stopTest();
    }

    @isTest
    static void checkForAddressChangesWithoutChangeTest() {
        List<Account> accounts = new List<Account>();
        SetupTestData.createCustomSettings();
        SetupTestData.createAccounts(1);
        Account accountWithoutAddressChange = SetupTestData.testAccounts[0];
        accountWithoutAddressChange.Name = 'Tester Stevie';
        Test.startTest();
        update accounts;
        Test.stopTest();
    }

    @isTest
    static void formatPhoneTestInsert() {
        SetupTestData.createCustomSettings();
        SetupTestData.createAccounts(1);
        Account acc = SetupTestData.testAccounts[0];
        acc.Name = 'format phone tester';
        acc.Phone = '1234567890';
        Test.startTest();
        update acc;
        Test.stopTest();
        Account acc2 = [SELECT Phone FROM Account WHERE Id=:acc.Id];
        System.assertEquals(acc2.Phone, '(123) 456-7890');
    }

    @isTest
    static void formatPhoneTestUpdate() {
        SetupTestData.createCustomSettings();
        SetupTestData.createAccounts(1);
        Account acc = SetupTestData.testAccounts[0];
        acc.Name = 'format phone tester';
        acc.Phone = '1234567890';
        update acc;
        acc.Phone = '0987654321';
        Test.startTest();
        update acc;
        Test.stopTest();
        Account acc2 = [SELECT Phone FROM Account WHERE Id=:acc.Id];
        System.assertEquals(acc2.Phone, '(098) 765-4321');
    }

    @isTest
    static void updateCustomerStatusTest() {
        SetupTestData.createCustomSettings();
        SetupTestData.createAccounts(1);
        Account acc = SetupTestData.testAccounts[0];
        acc.Name = 'Stevie Test Order Placed';
        acc.Account_Status__c = 'Prospect';
        acc.Order_Placed__c = false;
        update acc;

        acc.Order_Placed__c = true;
        Test.startTest();
        update acc;
        Test.stopTest();

        Account acc2 = [SELECT Account_Status__c FROM Account WHERE Id = :acc.Id];
        System.debug('acc2: ' + acc2);
        System.assertEquals('Customer', acc2.Account_Status__c);
    }
/*
    @isTest
    static void updateAddressAsIntegrationUser() {
        SetupTestData.createCustomSettings();
        SetupTestData.createAccounts(1);
        Account acc = SetupTestData.testAccounts[0];

        acc.BillingStreet = '124 main st.';
        acc.Order_Placed__c = true;

        User iuser = [select Id from User where alias = 'iuser' LIMIT 1];
        
        try
        {
            Test.startTest();
            System.runas(iuser)
            {
                update acc;
            }
            Test.stopTest();
        }
        catch(Exception e)
        {
            System.Assert(e.getMessage().contains('Integration User cannot update address fields on Account.'));
        } 
        

        //Account acc2 = [SELECT Account_Status__c FROM Account WHERE Id = :acc.Id];
        //System.debug('acc2: ' + acc2);
        //System.assertEquals('Customer', acc2.Account_Status__c);
    }
    */

    @isTest
    static void PriceBookCodeLinkInsertTest() {
        SetupTestData.createCustomSettings();
        Pricebook2 pb = new Pricebook2();
        pb.Name = 'ATEST';
        pb.PriceBookCode__c = 'Test';
        insert pb;

        SetupTestData.createAccounts(1);
        Account acc = SetupTestData.testAccounts[0];
        acc.PriceBookCode__c = 'Test';
        Test.startTest();
        update acc;
        Test.stopTest();

        Account acc2 = [SELECT Price_Book_List__c FROM Account WHERE Id = :acc.Id];

        System.assertEquals(pb.Id, acc2.Price_Book_List__c);
    }

    @isTest
    static void PriceBookCodeLinkUpdateTest() {
        SetupTestData.createCustomSettings();
        Pricebook2 pb = new Pricebook2();
        pb.Name = 'ATEST';
        pb.PriceBookCode__c = 'Test';
        insert pb;
        Pricebook2 pb2 = new Pricebook2();
        pb2.Name = 'BTEST';
        pb2.PriceBookCode__c = 'BTest';
        insert pb2;

        SetupTestData.createAccounts(1);
        Account acc = SetupTestData.testAccounts[0];
        acc.PriceBookCode__c = 'Test';
        update acc;

        acc.PriceBookCode__c = 'BTest';
        Test.startTest();
        update acc;
        Test.stopTest();

        Account acc2 = [SELECT Price_Book_List__c FROM Account WHERE Id = :acc.Id];

        System.assertEquals(pb2.Id, acc2.Price_Book_List__c);
    }

    

    @isTest
    static void PassDownKeyMessageUpdateTest() {
        SetupTestData.createCustomSettings();
        SetupTestData.createAccounts(2);
        
        
        Account parentAcc = SetupTestData.testAccounts[0];
        parentAcc.Key_Account_Message__c = 'hello';
        update parentAcc;
        Account childAcc = SetupTestData.testAccounts[1];
        childAcc.Parent = parentAcc;
        childAcc.ParentId = parentAcc.Id;
        update childAcc;
        

        parentAcc.Key_Account_Message__c = 'hello there';
        System.debug('about to update');
        Test.startTest();
        update parentAcc;
        Test.stopTest();

        Account acc = [SELECT Key_Account_Message__c FROM Account WHERE Id = :childAcc.Id];

        System.debug(acc);

        System.assertEquals('hello there', acc.Key_Account_Message__c);
    }
    
   /* @isTest
    static void addressChangeTest() {
        SetupTestData.createCustomSettings();
        SetupTestData.createAccounts(1);
        
        
        Account parentAcc = SetupTestData.testAccounts[0];
        parentAcc.BillingStreet= 'TestBillingStreet';
        parentAcc.ShippingCity = 'TestShippingCity';
        update parentAcc;
        
        parentAcc.Address_Approval_Process__c = 'Rejected';
        update parentAcc;
        
        parentAcc.Address_Approval_Process__c = 'Approved';
        update parentAcc;       
    } */
  
}