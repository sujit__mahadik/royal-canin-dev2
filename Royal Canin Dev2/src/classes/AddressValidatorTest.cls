@isTest
public class AddressValidatorTest implements WebServiceMock {

	public void doInvoke(Object stub, Object request, Map<String, Object> response, String endpoint, String soapAction, String requestName, String responseNS, String responseName, String responseType){
		
	}

	@isTest
	static void testValidateBillingFail(){
		Customer_Registration__c testCustReg = SetUpData();

		//fail to validate billing address
		Test.setMock(WebServiceMock.class, new AddressSvcMock());
		Test.startTest();
			testCustReg = [Select Id, First_Name__c, Last_Name__c, Email_Address__c, Billing_Street_Address1__c, Billing_Street_Address2__c, Billing_City__c, Bill_To_State__c, Billing_Zip_Code__c, Validated__c, Validation_Error_Reason__c, Pending_Avalara_Response__c From Customer_Registration__c Where First_Name__c = 'TestFirstName' limit 1];
			testCustReg.Billing_Street_Address1__c = '1234 Cobblestone Rd';//this should not validate properly
			testCustReg.Billing_Street_Address2__c = 'Fake Apartment Number';
			testCustReg.Billing_City__c = 'Fakecity';
			testCustReg.Bill_To_State__c = 'OH';
			testCustReg.Billing_Zip_Code__c = '99999';
			update testCustReg;
		Test.stopTest();

		testCustReg = [Select Id, First_Name__c, Last_Name__c, Email_Address__c, Billing_Street_Address1__c, Billing_Street_Address2__c, Billing_City__c, Bill_To_State__c, Billing_Zip_Code__c, Validated__c, Validation_Error_Reason__c, Pending_Avalara_Response__c From Customer_Registration__c Where First_Name__c = 'TestFirstName' limit 1];
		System.assertEquals(false, testCustReg.Pending_Avalara_Response__c);
		System.assertEquals(false, testCustReg.Validated__c);
		//System.assertNotEquals(null, testCustReg.Validation_Error_Reason__c);
		//System.assert(testCustReg.Validation_Error_Reason__c.contains('Billing Address Validation Returned Other Than Success: '));
	}

	@isTest
	static void testValidateBillingSuccess(){
		Customer_Registration__c testCustReg = SetUpData();
		AddressValidator valid = new AddressValidator();

		//successfully validate billing address
		Test.setMock(WebServiceMock.class, new AddressSvcMock());
		Test.startTest();
			testCustReg = [Select Id, First_Name__c, Last_Name__c, Email_Address__c, Billing_Street_Address1__c, Billing_Street_Address2__c, Billing_City__c, Bill_To_State__c, Billing_Zip_Code__c, Shipping_Street_Address1__c, Shipping_Street_Address2__c, Shipping_City__c, Ship_To_State__c, Shipping_Zip_Code__c, Validated__c, Validation_Error_Reason__c, Pending_Avalara_Response__c From Customer_Registration__c Where First_Name__c = 'TestFirstName' limit 1];
			testCustReg.Billing_Street_Address1__c = '500 Fountain Lakes Blvd';//main address for Royal Canin, should validate properly
			testCustReg.Billing_Street_Address2__c = 'Suite 100';
			testCustReg.Billing_City__c = 'St Charles';
			testCustReg.Bill_To_State__c = 'MO';
			testCustReg.Billing_Zip_Code__c = '63301';
			update testCustReg;
		Test.stopTest();

		testCustReg = [Select Id, First_Name__c, Last_Name__c, Email_Address__c, Billing_Street_Address1__c, Billing_Street_Address2__c, Billing_City__c, Bill_To_State__c, Billing_Zip_Code__c, Shipping_Street_Address1__c, Shipping_Street_Address2__c, Shipping_City__c, Ship_To_State__c, Shipping_Zip_Code__c, Validated__c, Validation_Error_Reason__c, Pending_Avalara_Response__c From Customer_Registration__c Where First_Name__c = 'TestFirstName' limit 1];
		System.assertEquals(false, testCustReg.Pending_Avalara_Response__c);
		//System.assertEquals(true, testCustReg.Validated__c);
		System.assertEquals(null, testCustReg.Validation_Error_Reason__c);
	}

	@isTest
	static void testValidateShippingFail(){
		Customer_Registration__c testCustReg = SetUpData();

		//successfully validate billing address
		Test.setMock(WebServiceMock.class, new AddressSvcMock());
		Test.startTest();
			testCustReg = [Select Id, First_Name__c, Last_Name__c, Email_Address__c, Billing_Street_Address1__c, Billing_Street_Address2__c, Billing_City__c, Bill_To_State__c, Billing_Zip_Code__c, Shipping_Street_Address1__c, Shipping_Street_Address2__c, Shipping_City__c, Ship_To_State__c, Shipping_Zip_Code__c, Validated__c, Validation_Error_Reason__c, Pending_Avalara_Response__c From Customer_Registration__c Where First_Name__c = 'TestFirstName' limit 1];
			testCustReg.Billing_Street_Address1__c = '500 fountain lakes blvd';//main address for Royal Canin, should validate properly
			testCustReg.Billing_Street_Address2__c = 'suite 100';
			testCustReg.Billing_City__c = 'st charles';
			testCustReg.Bill_To_State__c = 'mo';
			testCustReg.Billing_Zip_Code__c = '63301';
			testCustReg.Shipping_Street_Address1__c = '1234 Cobblestone Rd';//this should not validate correctly
			testCustReg.Shipping_Street_Address2__c = 'Fake Apartment Number';
			testCustReg.Shipping_City__c = 'Fakecity';
			testCustReg.Ship_To_State__c = 'OH';
			testCustReg.Shipping_Zip_Code__c = '99999';
			update testCustReg;
		Test.stopTest();

		testCustReg = [Select Id, First_Name__c, Last_Name__c, Email_Address__c, Billing_Street_Address1__c, Billing_Street_Address2__c, Billing_City__c, Bill_To_State__c, Billing_Zip_Code__c, Shipping_Street_Address1__c, Shipping_Street_Address2__c, Shipping_City__c, Ship_To_State__c, Shipping_Zip_Code__c, Validated__c, Validation_Error_Reason__c, Pending_Avalara_Response__c From Customer_Registration__c Where First_Name__c = 'TestFirstName' limit 1];
		System.assertEquals(false, testCustReg.Pending_Avalara_Response__c);
		System.assertEquals(false, testCustReg.Validated__c);
		//System.assertNotEquals(null, testCustReg.Validation_Error_Reason__c);
		//System.assert(testCustReg.Validation_Error_Reason__c.contains('Shipping Address Validation Returned Other Than Success: '));
	}

	@isTest
	static void testValidateShippingSuccess(){
		Customer_Registration__c testCustReg = SetUpData();

		//successfully validate billing address
		Test.setMock(WebServiceMock.class, new AddressSvcMock());
		Test.startTest();
			testCustReg = [Select Id, First_Name__c, Last_Name__c, Email_Address__c, Billing_Street_Address1__c, Billing_Street_Address2__c, Billing_City__c, Bill_To_State__c, Billing_Zip_Code__c, Shipping_Street_Address1__c, Shipping_Street_Address2__c, Shipping_City__c, Ship_To_State__c, Shipping_Zip_Code__c, Validated__c, Validation_Error_Reason__c, Pending_Avalara_Response__c From Customer_Registration__c Where First_Name__c = 'TestFirstName' limit 1];
			testCustReg.Billing_Street_Address1__c = '500 fountain lakes blvd';//main address for Royal Canin, should validate properly
			testCustReg.Billing_Street_Address2__c = 'suite 100';
			testCustReg.Billing_City__c = 'st charles';
			testCustReg.Bill_To_State__c = 'mo';
			testCustReg.Billing_Zip_Code__c = '63301';
			testCustReg.Shipping_Street_Address1__c = '500 fountain lakes blvd';
			testCustReg.Shipping_Street_Address2__c = '';
			testCustReg.Shipping_City__c = 'saint charles';
			testCustReg.Ship_To_State__c = 'Mo';
			testCustReg.Shipping_Zip_Code__c = '63301';
			update testCustReg;
		Test.stopTest();

		testCustReg = [Select Id, First_Name__c, Last_Name__c, Email_Address__c, Billing_Street_Address1__c, Billing_Street_Address2__c, Billing_City__c, Bill_To_State__c, Billing_Zip_Code__c, Shipping_Street_Address1__c, Shipping_Street_Address2__c, Shipping_City__c, Ship_To_State__c, Shipping_Zip_Code__c, Validated__c, Validation_Error_Reason__c, Pending_Avalara_Response__c From Customer_Registration__c Where First_Name__c = 'TestFirstName' limit 1];
		System.assertEquals(false, testCustReg.Pending_Avalara_Response__c);
		//System.assertEquals(true, testCustReg.Validated__c);
		System.assertEquals(null, testCustReg.Validation_Error_Reason__c);
	}

	static Customer_Registration__c SetUpData(){
		SetupTestData.createCustomSettings();
		Schema.DescribeSObjectResult rcSchema = Schema.SObjectType.Customer_Registration__c;
		Map<String,Schema.RecordTypeInfo> CustomerRegistrationRecordTypeInfo = rcSchema.getRecordTypeInfosByName();
		
		Customer_Registration__c testCustReg = new Customer_Registration__c();
		testCustReg.RecordTypeId = CustomerRegistrationRecordTypeInfo.get('Shelter').getRecordTypeId();
		testCustReg.First_Name__c = 'TestFirstName';
		testCustReg.Last_Name__c = 'TestLastName';
		testCustReg.Email_Address__c = 'test@test.com';
		testCustReg.Customer_Account_Id__c = '123456789';
		testCustReg.Payment_Options__c = 'Credit/Debit Card';
		insert testCustReg;

		return testCustReg;
	}
}