/************************************************************************************
Version : 1.0
Name : giic_LoyaltyManagementUtility
Created Date : 19 Nov 2018
Function : Loyalty Management related function
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/
public with sharing class giic_LoyaltyManagementUtility {
    /*
    *@Description: Return the available Reward Points available in this quarter
    *@Param: Account bill to customer number
    *@Return: List of giic_RewardPoint__c
    */
    public static List<giic_RewardPoint__c> getAvailableRewardPoints(String accountId,list<String> rewardPointsIds){
        try{
            list<giic_RewardPoint__c> lstRewardPointsfinal = new list<giic_RewardPoint__c>();
            Date todayDate = System.today();
            String QString =  ' SELECT Id, Name, giic_RewardPointValue__c,giic_NumOfTimesRedeemed__c,giic_AmountLeft__c,giic_IsOneTimeUse__c FROM giic_RewardPoint__c ';
            String whereClause = ' giic_ExpiryDate__c > :todayDate AND giic_AmountLeft__c > 0 ';
            if(rewardPointsIds != null && rewardPointsIds.size() > 0)
                whereClause += ' and id IN : rewardPointsIds';
            if(accountId != null && accountId != '')
                whereClause += ' and giic_Account__c =: accountId and createddate=THIS_QUARTER';
            QString = QString + ' where'  + whereClause;
            
            
            list<giic_RewardPoint__c> lstRewardPoints = database.query(QString);
            for(giic_RewardPoint__c rp: lstRewardPoints)
            {
                if(!rp.giic_IsOneTimeUse__c || (rp.giic_IsOneTimeUse__c && rp.giic_NumOfTimesRedeemed__c<= 0))
                {
                    lstRewardPointsfinal.add(rp);
                }
                
            }
            
            return lstRewardPointsfinal;
            
        }catch(Exception e){throw e;} 
    }
    
    
    /*
    *@Description: Return the available Reward Points available in this quarter
    *@Param: set of account ids, List of giic_RewardPoint__c
    *@Return: account with reward points
    */
    public static map<id,giic_RewardPoint__c> getAvailableRewardPoints(set<string> setAccuntIds,list<String> rewardPointsIds){
        try{
            map<id,giic_RewardPoint__c> mapRewardPointsfinal = new map<id,giic_RewardPoint__c>();
            Date todayDate = System.today();
            String QString =  ' SELECT Id, Name,giic_Account__c, giic_RewardPointValue__c,giic_NumOfTimesRedeemed__c,giic_AmountLeft__c,giic_IsOneTimeUse__c FROM giic_RewardPoint__c ';
            String whereClause = ' giic_ExpiryDate__c > :todayDate AND giic_AmountLeft__c > 0 ';
            if(rewardPointsIds != null && rewardPointsIds.size() > 0)
            
                whereClause += ' and id IN : rewardPointsIds';
            if(setAccuntIds != null && setAccuntIds.size() > 0)
            
                whereClause += ' and giic_Account__c IN :setAccuntIds and createddate=THIS_QUARTER';
            QString = QString + ' where'  + whereClause;
            
            list<giic_RewardPoint__c> lstRewardPoints = database.query(QString);
            for(giic_RewardPoint__c rewardPoint: database.query(QString))
            {
                if(!rewardPoint.giic_IsOneTimeUse__c || (rewardPoint.giic_IsOneTimeUse__c && rewardPoint.giic_NumOfTimesRedeemed__c<= 0))
                {
                    mapRewardPointsfinal.put(rewardPoint.giic_Account__c,rewardPoint);
                }
                
            }
            
            return mapRewardPointsfinal;
            
        }catch(Exception e){throw e;}
    }    
    
    /*
    *@Description: used to Insert Loayalty Points Tracker records against Members Loyalty Points.
    *@Param: map of Invoice Details and Customers (Accounts) , set of ids of Customer (Accounts)
    *@Return: void
    */
    public static void insertLoyaltyPointInTracker(Map<Id,Id> mapInvoiceDetailsAndCustomerId, Set<Id> setCustomerId,Map<id,gii__OrderInvoiceDetail__c> mapInvoiceDetailsRecord,Map<Id,Account> mapAccount)
    {
        try
        {
            Map<id,giic_MembersLoyaltyPoints__c> mapCustomerembersLoyaltyPoints = new Map<Id,giic_MembersLoyaltyPoints__c>();
            for(Id CustId :setCustomerId)
            {
                giic_MembersLoyaltyPoints__c TempObj = new giic_MembersLoyaltyPoints__c();
                TempObj.giic_Account__c = CustId;
                mapCustomerembersLoyaltyPoints.put(CustId,TempObj);
            }        
            
            List<giic_MembersLoyaltyPoints__c> lstCustomerLoyaltyRecord = new List<giic_MembersLoyaltyPoints__c>();
            lstCustomerLoyaltyRecord = [select id,giic_Account__c from giic_MembersLoyaltyPoints__c where giic_Account__c in: setCustomerId and createddate = THIS_QUARTER ];
            
            if(!(lstCustomerLoyaltyRecord.isempty()))
            {
                for(giic_MembersLoyaltyPoints__c TempLoyalPointObj : lstCustomerLoyaltyRecord)
                {
                    mapCustomerembersLoyaltyPoints.put(TempLoyalPointObj.giic_Account__c,TempLoyalPointObj);
                }
            }
            
            upsert mapCustomerembersLoyaltyPoints.values();
            
            Set<Id> setTypeOfMembershipId = new Set<id>();
            for(Account AccObj : mapAccount.values())
            {
                setTypeOfMembershipId.add(AccObj.giic_TypeOfMembership__c);
            }
            
            if(!(setTypeOfMembershipId.isempty()))
            {		
                Map<Id,List<giic_LoyaltyPointTracker__c>> map_Account_LPT = new Map<Id,List<giic_LoyaltyPointTracker__c>>();
                Map<Id,Decimal> map_Account_TotalPT = new Map<Id,Decimal>();
                Map<Id,giic_MembershipType__c> MembershipTypeMap = new Map<Id,giic_MembershipType__c>();
                MembershipTypeMap = new Map<Id,giic_MembershipType__c>([select id,name,(select id,name,giic_Amount__c,giic_PointsEquivToAmt__c from LoyaltyConfiguration__r 
                                                                                        where giic_Frequency__c =: System.Label.giic_LoyaltyBatchFrequency) 
                                                                        from giic_MembershipType__c where Id in :setTypeOfMembershipId]);     
                
                if(!(MembershipTypeMap.isempty()))
                {
                    List<giic_LoyaltyPointTracker__c> lstofLoyaltyPointTrackerToInsert = new List<giic_LoyaltyPointTracker__c>();
                    
                    for(Id InvoiceDetailsId : mapInvoiceDetailsAndCustomerId.keyset())
                    {
                        giic_LoyaltyPointTracker__c TempObj = new giic_LoyaltyPointTracker__c();
                        TempObj.giic_InvoiceDetail__c = InvoiceDetailsId;
                       
                        if(mapCustomerembersLoyaltyPoints.get(mapInvoiceDetailsAndCustomerId.get(InvoiceDetailsId)).id !=null)
                        {
                            TempObj.giic_MembersLoyaltyPts__c = mapCustomerembersLoyaltyPoints.get(mapInvoiceDetailsAndCustomerId.get(InvoiceDetailsId)).id;
                            if(mapInvoiceDetailsRecord.get(InvoiceDetailsId).gii__ProductAmount__c != null)					
                            {
                                decimal singleprdamount = 0;
                                if(mapInvoiceDetailsRecord.get(InvoiceDetailsId).gii__InvoicedQuantity__c != null && mapInvoiceDetailsRecord.get(InvoiceDetailsId).gii__InvoicedQuantity__c !=0
                                  && mapInvoiceDetailsRecord.get(InvoiceDetailsId).gii__NetInvoiceAmount__c !=null && mapInvoiceDetailsRecord.get(InvoiceDetailsId).gii__NetInvoiceAmount__c !=0 )
                                {
                                    singleprdamount= mapInvoiceDetailsRecord.get(InvoiceDetailsId).gii__InvoicedQuantity__c / mapInvoiceDetailsRecord.get(InvoiceDetailsId).gii__NetInvoiceAmount__c;
                                }                                 
                                decimal PaidAmount = mapInvoiceDetailsRecord.get(InvoiceDetailsId).gii__NetInvoiceAmount__c - (mapInvoiceDetailsRecord.get(InvoiceDetailsId).gii__ARCreditQuantity__c*singleprdamount);
                                
                                if(!(MembershipTypeMap.get(mapAccount.get(mapInvoiceDetailsAndCustomerId.get(InvoiceDetailsId)).giic_TypeOfMembership__c).LoyaltyConfiguration__r).isempty()) 
                                {                                    
                                    decimal ConfigureAmount = MembershipTypeMap.get(mapAccount.get(mapInvoiceDetailsAndCustomerId.get(InvoiceDetailsId)).giic_TypeOfMembership__c).LoyaltyConfiguration__r[0].giic_Amount__c;
                                    decimal ConfigurePoints = MembershipTypeMap.get(mapAccount.get(mapInvoiceDetailsAndCustomerId.get(InvoiceDetailsId)).giic_TypeOfMembership__c).LoyaltyConfiguration__r[0].giic_PointsEquivToAmt__c;
                                    decimal x = ConfigurePoints / ConfigureAmount;
                                    TempObj.giic_PointsEarned__c = PaidAmount * x;
                                    lstofLoyaltyPointTrackerToInsert.add(TempObj);                                   
                                }
                            }
                        }
                    }
                    
                    if(!(lstofLoyaltyPointTrackerToInsert.isempty()))
                    {
                        insert lstofLoyaltyPointTrackerToInsert;                        
                    }
                }
            } 
        }
        catch(Exception ex) 
        { 
            System.debug('Exception***'+ex.getMessage()+'-'+ex.getLineNumber());
        }
    }
    
    /*
    *@Description: create the reward point  for the loyalty Points generated for the customer
    *@Param: Loyalty Points Ids, membership id
    *@Return: 
    */
    public static void executeRewardPointsGeneration(list<giic_MembersLoyaltyPoints__c> scope,set<Id> setTypeOfMembershipId)
    {        
        //list<giic_MembersLoyaltyPoints__c> scope =[select id,name, giic_Account__c,giic_Account__r.name,giic_Account__r.giic_TypeOfMembership__c,giic_Account__r.giic_TypeOfMembership__r.name,giic_CurrentPoints__c,giic_LifeTimePoints__c,(select id,name,giic_MembersLoyaltyPts__c,giic_SalesOrderPayment__c,giic_PointRedeemed__c,giic_PointsEarned__c from LoyaltyPointTracker__r where giic_PointRedeemed__c = false ORDER BY giic_PointsEarned__c DESC) from giic_MembersLoyaltyPoints__c where id in :setmemberLayaltyPointIds];
        
        list<giic_RewardPointConfig__c> lstRewardPointConfig = new list<giic_RewardPointConfig__c>();
        lstRewardPointConfig  = [select id,name,giic_RewardPointVal__c,giic_NumOfPtsToGenRewardPts__c,giic_MembershipType__c,giic_MembershipType__r.giic_QualifyingPoints__c,giic_ApplyFixedRewardPointValue__c,giic_Tier6PtsToGenRewardPts__c,
                                    giic_Tier5PtsToGenRewardPts__c,giic_Tier4PtsToGenRewardPts__c,giic_Tier3PtsToGenRewardPts__c,giic_Tier2PtsToGenRewardPts__c,
                                    giic_Tier1PtsToGenRewardPts__c,giic_Tier1RewardPointValue__c,giic_Tier2RewardPointValue__c,giic_Tier3RewardPointValue__c,
                                    giic_Tier4RewardPointValue__c,giic_Tier5RewardPointValue__c,giic_Tier6RewardPointValue__c from giic_RewardPointConfig__c where giic_MembershipType__c in:setTypeOfMembershipId ];
        Map<id,list<giic_RewardPointConfig__c>> MembershipTypeRewardPointConfig = new Map<id,list<giic_RewardPointConfig__c>>();
        for(giic_RewardPointConfig__c RewardPointConfigObj : lstRewardPointConfig)
        {
            list<giic_RewardPointConfig__c> TempLst = new list<giic_RewardPointConfig__c>();
            TempLst.add(RewardPointConfigObj);
            if(MembershipTypeRewardPointConfig.containskey(RewardPointConfigObj.giic_MembershipType__c))
            {
                TempLst.addall(MembershipTypeRewardPointConfig.get(RewardPointConfigObj.giic_MembershipType__c));
                MembershipTypeRewardPointConfig.put(RewardPointConfigObj.giic_MembershipType__c,TempLst);
            }
            else
            {
                MembershipTypeRewardPointConfig.put(RewardPointConfigObj.giic_MembershipType__c,TempLst);
            }
        }
        Map<giic_MembersLoyaltyPoints__c,giic_RewardPointConfig__c> mapQualifiedMembersForRewardPoint = new Map<giic_MembersLoyaltyPoints__c,giic_RewardPointConfig__c>();
        for(giic_MembersLoyaltyPoints__c TempObj : scope)
        {
            // Members_Loyalty_Points__c TempObj = (Members_Loyalty_Points__c) s;
            if(MembershipTypeRewardPointConfig.containskey(TempObj.giic_Account__r.giic_TypeOfMembership__c))
            {
                for(giic_RewardPointConfig__c RewardPointConfigTempObj : MembershipTypeRewardPointConfig.get(TempObj.giic_Account__r.giic_TypeOfMembership__c))
                {
                    if(TempObj.giic_CurrentPoints__c >= RewardPointConfigTempObj.giic_MembershipType__r.giic_QualifyingPoints__c)
                    {
                        mapQualifiedMembersForRewardPoint.put(TempObj,RewardPointConfigTempObj);
                        break;
                    }
                }
            }
        }
        list<giic_RewardPoint__c> lstOfRewardPointToInsert = new list<giic_RewardPoint__c>();
        if(!(mapQualifiedMembersForRewardPoint.isempty()))
        {
            
            list<giic_LoyaltyPointTracker__c> lstLoyaltyPointToUpdate = new list<giic_LoyaltyPointTracker__c>();
            list<giic_LoyaltyPointTracker__c> lstLoyaltyPointToinsert = new list<giic_LoyaltyPointTracker__c>();
            for(giic_MembersLoyaltyPoints__c LoyaltyObj : mapQualifiedMembersForRewardPoint.keyset())
            {
                decimal CurrentPoints = LoyaltyObj.giic_CurrentPoints__c;
                decimal PointsToBeDeducted = 0.00;
                decimal RewardPointValue = 0.00;
                map<decimal,decimal> mapTierInfo=new map<decimal,decimal>();
                if(mapQualifiedMembersForRewardPoint.get(LoyaltyObj).giic_ApplyFixedRewardPointValue__c){
                    mapTierInfo=getTierInfoForRewards(mapQualifiedMembersForRewardPoint.get(LoyaltyObj));
                    if(mapTierInfo.size() > 0){
                        list<decimal> lstTierInfo=(list<decimal>)JSON.deserialize(JSON.serialize(mapTierInfo.keyset()), list<decimal>.class);
                        for(decimal tierPoints : lstTierInfo){
                            if(CurrentPoints >= tierPoints){
                                RewardPointValue=mapTierInfo.get(tierPoints);
                                CurrentPoints=0;
                                PointsToBeDeducted = LoyaltyObj.giic_CurrentPoints__c;
                                break;
                            }
                        }
                    }
                }
                else{
                    while(CurrentPoints >= mapQualifiedMembersForRewardPoint.get(LoyaltyObj).giic_Tier1PtsToGenRewardPts__c)
                    {
                        RewardPointValue = RewardPointValue + mapQualifiedMembersForRewardPoint.get(LoyaltyObj).giic_Tier1RewardPointValue__c;
                        CurrentPoints = CurrentPoints - mapQualifiedMembersForRewardPoint.get(LoyaltyObj).giic_Tier1PtsToGenRewardPts__c;
                        PointsToBeDeducted = PointsToBeDeducted + mapQualifiedMembersForRewardPoint.get(LoyaltyObj).giic_Tier1PtsToGenRewardPts__c;
                    } 
                    
                }
                if(RewardPointValue != 0.00)
                {
                    giic_RewardPoint__c RewardPointObj = new giic_RewardPoint__c();
                    RewardPointObj.giic_Account__c = LoyaltyObj.giic_Account__c;
                    RewardPointObj.giic_RewardPointValue__c = RewardPointValue;
                    RewardPointObj.giic_ExpiryDate__c = getLastDateOfQuarter(system.today());
                    if(mapQualifiedMembersForRewardPoint.get(LoyaltyObj).giic_ApplyFixedRewardPointValue__c){
                        RewardPointObj.giic_IsOneTimeUse__c=true;
                    }
                    lstOfRewardPointToInsert.add(RewardPointObj);
                }
                Decimal TotalPointsDeducted = 0.00;
                giic_LoyaltyPointTracker__c TempRecord = new giic_LoyaltyPointTracker__c();
                while(TotalPointsDeducted < PointsToBeDeducted)
                {
                    for(giic_LoyaltyPointTracker__c LoyaltyPointTrackerObj : LoyaltyObj.LoyaltyPointTracker__r)
                    {
                        LoyaltyPointTrackerObj.giic_PointRedeemed__c = true;
                        lstLoyaltyPointToUpdate.add(LoyaltyPointTrackerObj);
                        TempRecord = LoyaltyPointTrackerObj;
                        TotalPointsDeducted = TotalPointsDeducted + LoyaltyPointTrackerObj.giic_PointsEarned__c;
                    }
                }
                if(TotalPointsDeducted - PointsToBeDeducted > 0 && !mapQualifiedMembersForRewardPoint.get(LoyaltyObj).giic_ApplyFixedRewardPointValue__c)
                {
                    giic_LoyaltyPointTracker__c RemaningPointRecord = new giic_LoyaltyPointTracker__c();
                    RemaningPointRecord = TempRecord.clone();
                    RemaningPointRecord.giic_PointsEarned__c = TotalPointsDeducted - PointsToBeDeducted;
                    RemaningPointRecord.giic_PointRedeemed__c = false;
                    RemaningPointRecord.giic_RemainingAmtFrmRewardGen__c = true;
                    lstLoyaltyPointToinsert.add(RemaningPointRecord);
                }
            }
            
            if(!(lstLoyaltyPointToUpdate.isempty()))
                update lstLoyaltyPointToUpdate;
            
           
            if(!(lstLoyaltyPointToinsert.isempty()))
                insert lstLoyaltyPointToinsert;
            
            if(!(lstOfRewardPointToInsert.isempty()))
                insert lstOfRewardPointToInsert;          
        }
        //init();
        //FinishRewardPointsGeneration(LstOfRewardPointToInsert);
    }
    /*
    *@Description: return the last date of the quarter
    *@Param: current date
    *@Return: 
    */
    public static date getLastDateOfQuarter(date dt){
        Integer currentMnt =dt.month();
        Integer currentQ =((currentMnt-1)/3) + 1;
        Date endOfQDate = date.newInstance(dt.year(),currentMnt + (4 - (currentMnt - ((currentQ -1)*3))) , 1).addDays(-1);
        return endOfQDate;
    }
    /*
    *@Description: return the sorted list Tier info for rewards
    *@Param: giic_RewardPointConfig__c reward point config
    *@Return: 
    */
    public static map<decimal,decimal> getTierInfoForRewards(giic_RewardPointConfig__c RewardPointConfigObj ){
        map<decimal,decimal> mapTierInfo=new map<decimal,decimal>();
        if(RewardPointConfigObj.giic_Tier6PtsToGenRewardPts__c != null)
            mapTierInfo.put(RewardPointConfigObj.giic_Tier6PtsToGenRewardPts__c,RewardPointConfigObj.giic_Tier6RewardPointValue__c);
        if(RewardPointConfigObj.giic_Tier5PtsToGenRewardPts__c != null)
            mapTierInfo.put(RewardPointConfigObj.giic_Tier5PtsToGenRewardPts__c,RewardPointConfigObj.giic_Tier5RewardPointValue__c);
        if(RewardPointConfigObj.giic_Tier4PtsToGenRewardPts__c != null)
            mapTierInfo.put(RewardPointConfigObj.giic_Tier4PtsToGenRewardPts__c,RewardPointConfigObj.giic_Tier4RewardPointValue__c);
        if(RewardPointConfigObj.giic_Tier3PtsToGenRewardPts__c != null)
            mapTierInfo.put(RewardPointConfigObj.giic_Tier3PtsToGenRewardPts__c,RewardPointConfigObj.giic_Tier3RewardPointValue__c);
         if(RewardPointConfigObj.giic_Tier2PtsToGenRewardPts__c != null)
            mapTierInfo.put(RewardPointConfigObj.giic_Tier2PtsToGenRewardPts__c,RewardPointConfigObj.giic_Tier2RewardPointValue__c);
        if(RewardPointConfigObj.giic_Tier1PtsToGenRewardPts__c != null)
            mapTierInfo.put(RewardPointConfigObj.giic_Tier1PtsToGenRewardPts__c,RewardPointConfigObj.giic_Tier1RewardPointValue__c);
         
        return mapTierInfo;
    }
    /*
    *@Description: update reward points
    *@Param: list<giic_RewardPoint__c> ,total rewards
    *@Return: 
    */
    public static void updateRewardPoints(list<giic_RewardPoint__c> rewardPoints,decimal totalReward)
    {
        for( giic_RewardPoint__c rp : rewardPoints)
        {
            if(rp.giic_IsOneTimeUse__c)
                rp.giic_RewardPointValue__c = 0;
            else
                rp.giic_RewardPointValue__c -= totalReward;
            
        }
        
        update rewardPoints;
        
    }
}