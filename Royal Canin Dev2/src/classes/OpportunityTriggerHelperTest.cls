/*
 * Date:             - Developer, Company                          - description
 * 2015-08-05        - Stevie Yakkel, Acumen Solutions             - Created
 */

@isTest
private class OpportunityTriggerHelperTest
{	
	@isTest
	static void assignPricebook2IDWithPriceBookTest() {
		SetupTestData.createCustomSettings();
		SetupTestData.createAccounts(1);
		Account accountWithPriceBookCode = SetupTestData.testAccounts[0];
		accountWithPriceBookCode.Name = 'Stevie Tester';
		accountWithPriceBookCode.PriceBookCode__c = 'SYTESTA';
		update accountWithPriceBookCode;

		PriceBook2 pb = new PriceBook2();
		pb.Name = 'Stevie Tester';
		pb.PriceBookCode__c = 'SYTESTA';
		insert pb;

		Opportunity opp = new Opportunity();
		opp.AccountId = accountWithPriceBookCode.Id;
		opp.Account = accountWithPriceBookCode;
		opp.Name = 'Stevie Tester';
		opp.StageName = 'Stevie Opp Stage Name';
		opp.CloseDate = Date.Today().addDays(3);
		Test.startTest();
		insert opp;
		Test.stopTest();

		Opportunity opp2 = [select id, Pricebook2Id, PriceBook2.Name, PriceBook2.PriceBookCode__c from Opportunity where id=: opp.Id LIMIT 1];

		System.assertEquals('SYTESTA', opp2.PriceBook2.PriceBookCode__c);

	}

	@isTest
	static void assignPricebook2IDWithoutPriceBookTest() {
		SetupTestData.createCustomSettings();
		SetupTestData.createAccounts(1);
		Account accountWithoutPriceBookCode = SetupTestData.testAccounts[0];
		accountWithoutPriceBookCode.Name = 'Stevie Tester';
		update accountWithoutPriceBookCode;

		PriceBook2 pb = new PriceBook2();
		pb.Name = 'Stevie Tester';
		pb.PriceBookCode__c = 'SYTESTA';
		insert pb;

		Opportunity opp = new Opportunity();
		opp.AccountId = accountWithoutPriceBookCode.Id;
		opp.Account = accountWithoutPriceBookCode;
		opp.Name = 'Stevie Tester';
		opp.StageName = 'Stevie Opp Stage Name';
		opp.CloseDate = Date.Today().addDays(3);
		Test.startTest();
		insert opp;
		Test.stopTest();
		
		Opportunity opp2 = [select id, Pricebook2Id, PriceBook2.Name, PriceBook2.PriceBookCode__c from Opportunity where id=: opp.Id LIMIT 1];

		System.assertEquals(null, opp2.PriceBook2.PriceBookCode__c);

	}

	@isTest
	static void changeOpportunityNameOnInsertWithAccountTest() {
		SetupTestData.createCustomSettings();
		SetupTestData.createAccounts(1);
		Account acc = SetupTestData.testAccounts[0];
		acc.Name = 'Stevie Test Account';
		update acc;

		Opportunity opp = new Opportunity();
		opp.AccountId = acc.Id;
		opp.Account = acc;
		opp.Name = 'Test Opp';
		opp.StageName = 'Stevie Opp Stage Name';
		opp.CloseDate = Date.Today().addDays(3);
		Test.startTest();
		insert opp;
		Test.stopTest();

		Opportunity opp2 = [select Name from Opportunity where id=: opp.Id];

		System.assertEquals('Stevie Test Account - Test Opp', opp2.name);
	}

	@isTest
	static void changeOpportunityNameOnInsertWithoutAccountTest() {

		Opportunity opp = new Opportunity();
		opp.Name = 'Test Opp';
		opp.StageName = 'Stevie Opp Stage Name';
		opp.CloseDate = Date.Today().addDays(3);
		Test.startTest();
		insert opp;
		Test.stopTest();

		Opportunity opp2 = [select Name from Opportunity where id=: opp.Id];

		System.assertEquals('Test Opp', opp2.name);
	}
}