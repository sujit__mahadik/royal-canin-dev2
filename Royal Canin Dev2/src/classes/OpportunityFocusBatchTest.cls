@isTest
private class OpportunityFocusBatchTest {
	
	@isTest static void opportunityFocusBatchTest() {
		SetupTestData.createCustomSettings();
		Map<String, Id> accRecTypeMap;
		Map<String, Id> oppRecTypeMap;
		oppRecTypeMap = new Map<String, Id>();
		for(RecordType rec : [select id, developerName from RecordType where SObjectType = 'Opportunity']) {
			oppRecTypeMap.put(rec.DeveloperName, rec.Id);
		}

		accRecTypeMap = new Map<String, Id>();
		for(RecordType rec : [select id, developerName from RecordType where SObjectType = 'Account']) {
			accRecTypeMap.put(rec.DeveloperName, rec.Id);
		}

		Map<String, Id> channelToOppRecTypeMap = new Map<String, Id>();
		Map<String, Id> channelToAccRecTypeMap = new Map<String, Id>();
		for(Focused_opp_RecType_Mapping__c mapping : Focused_opp_RecType_Mapping__c.getAll().values()) {
			System.debug('mapping.name: ' + mapping.name);
			System.debug('mapping.OppRecType__c: ' + mapping.OppRecType__c);
			System.debug('mapping.AccRecType__c: ' + mapping.AccRecType__c);
			System.debug('opprect: ' + oppRecTypeMap.get(mapping.OppRecType__c));
			System.debug('accrect: ' + accRecTypeMap.get(mapping.AccRecType__c));
			channelToOppRecTypeMap.put(mapping.Name, oppRecTypeMap.get(mapping.OppRecType__c));
			channelToAccRecTypeMap.put(mapping.Name, accRecTypeMap.get(mapping.AccRecType__c));
		}
		Focused_Opportunities__c testOpportunity = new Focused_Opportunities__c();
		testOpportunity.Processed__c = false;
		testOpportunity.Effective_Date__c = Date.today().toStartOfMonth();
		testOpportunity.Name = 'Test Opportunity';
		testOpportunity.Channel__c = 'Veterinary';
		testOpportunity.Opportunity_Close_Date__c = Date.today();
		testOpportunity.Opportunity_Type__c = 'ISO'; 
		testOpportunity.Product_Category__c = 'Renal';
		insert testOpportunity;

		//accRecTypeMap = new Map<String, Id>();
		//for(RecordType rec : [select id, developerName from RecordType where SObjectType = 'Account']) {
		//	accRecTypeMap.put(rec.DeveloperName, rec.Id);
		//	System.debug('accrectypemap tester: ' + accRecTypeMap);
		//}

		Account testAccount = new Account();
		testAccount.Name = 'Test Account';
		testAccount.Account_Status__c = 'Prospect';
		testAccount.RecordTypeId = accRecTypeMap.get('Veterinarian');
		testAccount.BillingCity = 'Columbus';
		testAccount.BillingState = 'OH';
		testAccount.ShippingState = 'OH';
		testAccount.BillingPostalCode = '43212';
		insert testAccount;



		Test.startTest();
		Database.executeBatch(new OpportunityFocusBatch() ,20); 
		Test.stopTest();

		testOpportunity = [SELECT Id, Processed__c FROM Focused_Opportunities__c WHERE Id = :testOpportunity.Id];
		System.assertEquals(testOpportunity.Processed__c, true);
	}	
}