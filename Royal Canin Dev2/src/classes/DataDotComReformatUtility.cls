/*
 * Date:             - Developer, Company                          - description
 * 2015-08-05        - Stevie Yakkel, Acumen Solutions             - Created
 */

public with sharing class DataDotComReformatUtility {

	public static void dataDotComPhoneAndFaxReformat(List<sObject> sObjects) {
		Pattern MyPattern = Pattern.compile('\\+\\d{1}\\.\\d{3}\\.\\d{3}\\.\\d{4}');

        String areaCode;
        String secondThreeNumbers;
        String lastFourNumbers;
        Set<String> fields = new Set<String> {'Phone', 'Fax'};

        for(sObject sObj : sObjects) {
        	for(String field : fields) {
	            if(sObj.get(field) != null) {
	                System.debug(sObj.get(field));
	                String value = (String)sObj.get(field);
	                Matcher MyMatcher = MyPattern.matcher(value);
	                if(MyMatcher.matches()) {
	                	system.debug('found match!!@#');
		                if(value.contains('+1')) {
		                	value = value.remove('+1');
		                    sObj.put(field, value);
		                    System.debug('first if: ' + value);
		                }
		                if(value.contains('.')) {
		                	value = value.remove('.');
		                    sObj.put(field, value);
		                    System.debug('second if: ' + value);
		                }
		                if(value.contains(' ')) {
		                	value = value.remove(' ');
		                    sObj.put(field, value);
		                    System.debug('third if: ' + value);
		                }
		                if(!value.contains('(') && !value.contains(')')) {
		                	value = (String)sObj.get(field);
		                    areaCode = value.substring(0, 3);
		                    secondThreeNumbers = value.substring(3, 6);
		                    lastFourNumbers = value.substring(6, 10);
		                    sObj.put(field, '(' + areaCode + ') ' + secondThreeNumbers + '-' + lastFourNumbers);
		                } else {
		                	System.debug('length: ' + value.length());
		                	value = (String)sObj.get(field);
		                    areaCode = value.substring(0, 5);
		                    secondThreeNumbers = value.substring(5, 8);
		                    lastFourNumbers = value.substring(9, 13);
		                    sObj.put(field, areaCode + ' ' + secondThreeNumbers + '-' + lastFourNumbers);
		                }
		            }
	            }
            }
        }
    }

    public static void dataDotComAccountPostalCodeReformat(List<sObject> sObjects) {

    	Set<String> fields = new Set<String> {'BillingPostalCode', 'ShippingPostalCode'};

        for(sObject sObj : sObjects) {
        	for(String field : fields) {
	            if(sObj.get(field) != null) {
	            	String value = (String)sObj.get(field);
	                if(value.length() == 10)
	                    sObj.put(field, value.substring(0, 5));
	            }
	        }
        }
    }

    public static void dataDotComContactPostalCodeReformat(List<sObject> sObjects) {

    	Set<String> fields = new Set<String> {'MailingPostalCode', 'OtherPostalCode'};

        for(sObject sObj : sObjects) {
        	for(String field : fields) {
	            if(sObj.get(field) != null) {
	            	String value = (String)sObj.get(field);
	                if(value.length() == 10)
	                    sObj.put(field, value.substring(0, 5));
	            }
	        }
        }
    }
}