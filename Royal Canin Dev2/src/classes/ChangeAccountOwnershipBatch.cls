/*
 * Date:       		 - Developer, Company					       - Description
 * 2015-05-04        - Mayank Srivatava, Acumen Solutions             - Created
 */
global class ChangeAccountOwnershipBatch implements Database.Batchable<sObject> {
	
	String query;
	Fiscal_Year__c fy;
	Id fyId ;
	boolean fyChanges = false;
	boolean fiscalYearPromoted = false;
	
	global ChangeAccountOwnershipBatch() {
		Fiscal_Year__c fyForPromotion = ChangeAccountOwnershipHelperUtil.getFiscalYearForPromotion();
		if(fyForPromotion != null){
			fy = fyForPromotion;
			fiscalYearPromoted = true;
		}
		else{
			fy = ChangeAccountOwnershipHelperUtil.getCurrentFiscalYear();
		}
		fyId = fy.id;
		System.debug('fyid '+fyId);
		fyChanges = (fy.Start_Date__c <= Date.today() || fy.Activation_Date__c <= Date.today())? true : false;
		query = 'Select id, PostalCode__c, Fiscal_Year__c,PostalCodeID__c,TerritoryID__c,PostalcodeID__r.Pillar__c,PostalcodeID__r.ZipCode__c, TerritoryID__r.Region__c, TerritoryID__r.Ownerid from TerritoryAssignment__c'; 
		query += ' where Fiscal_Year__c = :fyId and Sync_Needed__c = TRUE';		
	}

	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(query);
	}

   global void execute(Database.BatchableContext BC, List<TerritoryAssignment__c> territoryAssignments) {
      Set<Id> territoryAssignIds = new Set<Id>();
   	Map<Id,Id> postalCodeToTerrirtoryAssign = new Map<Id,Id>();  
   	Map<Id,TerritoryAssignment__c> idToTerritoryAssignment = new Map<Id,TerritoryAssignment__c>();
   	Set<Id> territoryIds = new Set<Id>();
   	Set<String> shippingPostalCode = new Set<String>();

  		for(TerritoryAssignment__c ta : territoryAssignments){
  			idToTerritoryAssignment.put(ta.id,ta);
  			postalCodeToTerrirtoryAssign.put(ta.PostalCodeId__c, ta.id);
  			ta.Sync_Needed__c = FALSE;
  			territoryIds.add(ta.TerritoryID__c);
  			shippingPostalCode.add(ta.PostalCodeID__r.ZipCode__c);
  		} 	
  		List<Territory__c> listTA = [Select id,Sync_Needed__c  from Territory__c where id in :territoryIds];
     		for(Territory__c ta : listTA){
   			ta.Sync_Needed__c = false;
   		}
   	
   	System.debug('postalCodeToTerrirtoryAssign '+postalCodeToTerrirtoryAssign);
  

   	List<Account> listAccts = [ select id,PostalCodeId__c,Previous_Year_Territory_Assignment__c,Current_Year_Territory_Assignment__c,OwnerId,ShippingPostalCode, Customer_Type__c
   								from Account where ShippingPostalCode in: shippingPostalCode and Exclude_from_Territory_Assignment__c = FALSE];
   		
   	//set postalcode ids for account if they do not exist
   	AccountTriggerUtilities.setPostalCodeLookup(listAccts);
	
   	for(Account acc : listAccts){
      	if(!postalCodeToTerrirtoryAssign.containsKey(acc.PostalCodeID__c)){
   	     	continue;
   		}
   		acc.OwnerId = idToTerritoryAssignment.get(postalCodeToTerrirtoryAssign.get(acc.PostalCodeID__c)).TerritoryID__r.Ownerid;
   		if(String.isBlank(acc.Current_Year_Territory_Assignment__c)){
   		    acc.Current_Year_Territory_Assignment__c = postalCodeToTerrirtoryAssign.get(acc.PostalCodeID__c);
   		}
   		if(fyChanges){
   		    acc.Previous_Year_Territory_Assignment__c = acc.Current_Year_Territory_Assignment__c;
   			 acc.Current_Year_Territory_Assignment__c = postalCodeToTerrirtoryAssign.get(acc.PostalCodeID__c);
   		}
   		acc.Region__c = idToTerritoryAssignment.get(postalCodeToTerrirtoryAssign.get(acc.PostalCodeID__c)).TerritoryID__r.Region__c;
   	}
   	if(fiscalYearPromoted){
		    ChangeAccountOwnershipHelperUtil.promoteFiscalYearToActive(fy);
   	}
   	try{
         update listAccts;
   		update territoryAssignments;
   		update listTA;
      }
      Catch(Exception e){
            //do nothing   
      }	
   }
	
	global void finish(Database.BatchableContext BC) {
		
	}

	
}