/************************************************************************************
Version : 1.0
Name : giic_SOShipmentQueue
Created Date : 08 Oct 2018
Function : queue to create shipment and invoice
Modification Log :
Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/
public class giic_SOShipmentQueue implements Queueable, Database.AllowsCallouts {
   private Map<String, Object> mapUpdateResult;
    private Map<String, Object> mapResult;
    private Id whasId;
    private List<gii__WarehouseShipmentAdviceLinesStaging__c> lstWHSAStagingLines; 
    
    public giic_SOShipmentQueue (Map<String, Object> mapUpdateResult, Map<String, Object> mapResult, Id whasId, List<gii__WarehouseShipmentAdviceLinesStaging__c> lstWHSAStagingLines) {
            this.mapUpdateResult = mapUpdateResult;
            this.mapResult = mapResult;
            this.whasId = whasId;
            this.lstWHSAStagingLines = lstWHSAStagingLines;
    } 
    
    public void execute(QueueableContext queCont) {
       try{
           //Create Quick ship
            if(mapUpdateResult.containsKey('mapIReservsAndQuickShip') && mapUpdateResult.get('mapIReservsAndQuickShip') != null) giic_SOShipmentUtility.createQuickShip((Map<String, List<Id>>)mapUpdateResult.get('mapIReservsAndQuickShip'));
            
            String soNumber = mapResult.containsKey('soNumber') ? (String)mapResult.get('soNumber') : '';
            Date shipDate =  mapResult.containsKey('shipDate') ? Date.valueOf((String)mapResult.get('shipDate')) : System.today();
            
            if(soNumber != ''){ // create error log
                List<gii__Shipment__c> lstShipment = [Select Id, gii__SalesOrder__c,gii__SalesOrder__r.giic_SOStatus__c , gii__SalesOrder__r.giic_CustEmail__c, gii__SalesOrder__r.gii__PaymentMethod__c, ( Select Id, gii__SalesOrderLine__c, gii__SalesOrderLine__r.giic_ProductLot__c from gii__ShipmentDetails__r) From gii__Shipment__c where gii__ForwardToInvoice__c = false and gii__ShippedDate__c = :shipDate  and gii__SalesOrder__r.Name = :soNumber];
                if(lstShipment.isEmpty()){
                    update new gii__WarehouseShipmentAdviceStaging__c(id = whasId, giic_ProcessStatus__c = giic_Constants.PROCESSEDWITHERROR);  
                    giic_CyberSourceRCCCUtility.handleErrorFuture(wHASId, giic_Constants.USER_STORY_UPDATE_SOL, giic_Constants.UPDATE_SOL_ERROR_CODE, system.label.giic_ShipmentNotCreated);
                    return;
                }else{ // update status of sols
                    if(lstShipment[0].gii__SalesOrder__c != null && lstShipment[0].gii__SalesOrder__r.giic_SOStatus__c == giic_Constants.SO_PICKED){
                        update new gii__SalesOrder__c(Id = lstShipment[0].gii__SalesOrder__c, giic_SOStatus__c = giic_Constants.SO_SHIPPED);
                    } 
                    if(!lstShipment[0].gii__ShipmentDetails__r.isEmpty()){
                        List<gii__SalesOrderLine__c> lstSOL = new List<gii__SalesOrderLine__c>();
                        for(gii__ShipmentDetail__c objShipDetail : lstShipment[0].gii__ShipmentDetails__r){
                            gii__SalesOrderLine__c sol = new gii__SalesOrderLine__c(id = objShipDetail.gii__SalesOrderLine__c, giic_LineStatus__c = giic_Constants.SHIPPED);
                            lstSOL.add(sol); 
                        }
                        if(!lstSOL.isEmpty()) update lstSOL;
                    }
                }
    
                if(mapResult != null && mapResult.size() > 0 ){
                    giic_CreateInvoiceHelper.createInvoiceFromShipment(JSON.serialize(mapResult), JSON.serialize((Map<String, String>)mapResult.get('mapFulfilmentLines')));// create invoice from shipment
                }
            }
       }catch(exception e){
            System.debug('giic_SOShipmentQueue Exception::' + e.getMessage() + ' Line::' + e.getLineNumber() + ' getStackTraceString=' + e.getStackTraceString());
            update new gii__WarehouseShipmentAdviceStaging__c(id = whasId, giic_ProcessStatus__c = giic_Constants.PROCESSEDWITHERROR); 
            giic_CyberSourceRCCCUtility.handleErrorFuture(whasId, giic_Constants.USER_STORY_INVOICECREATION, giic_Constants.DML_EX_CODE, e.getMessage() + 'Line::' + e.getLineNumber());

        }
    }
}