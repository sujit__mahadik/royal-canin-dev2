@isTest
public class giic_Test_SalesOrderInvoiceBatch {
    @testSetup
    static void setup(){
        
        giic_Test_DataCreationUtility.insertWarehouse(); // to create default warehouse for systemPolicy
        giic_Test_DataCreationUtility.testSystemPolicyCreation();
        SetupTestData.createCustomSettings();
        List<gii__Warehouse__c> testWareList = giic_Test_DataCreationUtility.insertWarehouse_N(5);
        list<id> wIds = new list<id>();
        list<id> accIds = new list<id>(); 
        List<Account> testConsumerAccountList = giic_Test_DataCreationUtility.insertConsumerAccount_N(5);
        List<gii__AccountAdd__c> testAccRefList = giic_Test_DataCreationUtility.getAccountReference(testConsumerAccountList);
        system.debug('testAccRefList--->>>> ' + testAccRefList);
        List<gii__Product2Add__c> testProductList = giic_Test_DataCreationUtility.insertProduct_N(); // it will create 5 products
        giic_Test_DataCreationUtility.insertProductInventoryQTYDetails_N(5);
        integer intSOCount = 10; // Number of sales Order to create 
        List<gii__SalesOrder__c> testSOList = giic_Test_DataCreationUtility.insertSalesOrder_N(intSOCount);
        List<gii__SalesOrderLine__c> testSOLine = giic_Test_DataCreationUtility.insertSOLine_N();        
        
    }    
    public static testMethod void testExecute()
    {
        //giic_Test_DataCreationUtility.insertConsumerAccount();
        Test.startTest();            
            giic_SalesOrderInvoiceBatch obj = new giic_SalesOrderInvoiceBatch();
            Database.executeBatch(obj); 
       		System.debug('Count'+[SELECT Count() FROM gii__SalesOrder__c WHERE gii__Status__c = 'Open']);
        Test.stopTest();       
    }
}