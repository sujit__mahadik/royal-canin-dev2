global class ChangePostalCodeAccountOwnershipBatch implements Database.batchable<sObject>, Database.AllowsCallouts{

	//incoming query
	global final String query;
	global Set<id> lockedCodes = new Set<Id>();
	global final Map<Id, Id> zipAssignments;
	global final Map<Id, Id> managers;
	private List<Account> acctTeams = new List<Account>();
	
	//Constructor - set the local variable with the query string
	global ChangePostalCodeAccountOwnershipBatch(String query, Map<Id, Id> zipOwners, Map<Id, Id> managerusers) {
		this.query = query;
		this.zipAssignments = zipOwners;
		this.managers = managerusers;
	}
	
	//get Querylocator with the specitied query
	global Database.QueryLocator start(Database.BatchableContext bc) {
		return Database.getQueryLocator(query);
	}
	
	global void execute(Database.BatchableContext bc, List<sObject> accounts) {
		List<Account> updatedAccounts = new List<Account>();
		for(sObject s : accounts)
		{
			Account a = (Account)s;
			if(zipAssignments.get(a.PostalCodeId__c) != null)
			{
				a.OwnerId = zipAssignments.get(a.PostalCodeId__c);
				a.NewLead__c = true;
				a.DateAssigned__c = DateTime.now();
				updatedAccounts.add(a);
				if(managers.get(a.OwnerId) != null)
				{
					acctTeams.add(a);
					a.UnassignedLead__c = true;
				}
				else
				{
					a.UnassignedLead__c = false;
				}
			}
		}
		//update updatedAccounts;
		Database.Saveresult[] SRs = database.update(updatedAccounts, false);
		List<Account> reUpdates = new List<Account>();
		Integer counter = 0;
		for(Database.SaveResult sr:SRs)
		{
			if(!sr.isSuccess())
			{
				reUpdates.add(updatedAccounts[counter]); 
			}
			counter++;
		}		
		if(reUpdates.size() > 0)
		{
			Database.Saveresult[] nSRs = database.update(reUpdates, false);
			for(Database.SaveResult nsr:nSRs)
			{
				if(!nsr.isSuccess())
				{
					//we can do something here if necessary
				}
			}				
		}
		Utilities.AddTeamMembers(acctTeams);
	}
	
	global void finish(Database.BatchableContext bc) {
		//unlock the zip by resetting the field
	}
	
	/*private String AddTeamMembers(List<Account> accts)
	{
		Set<Id> ownerIds = new Set<Id>();
		List<AccountTeamMember> atms = new List<AccountTeamMember>();
		List<AccountShare> accss = new List<AccountShare>();
		AccountTeamMember atm;
		AccountShare accs;
 		for(Account acct : accts)
		{
			if(acct.Status__c != 'Archived')
				ownerIds.add(acct.OwnerId);
		}
		Map<id, List<Id>> managerReps = Utilities.getManagersAndSubordinates(ownerIds);
		for(Account acct : accts)
		{
			if(managerReps.get(acct.OwnerId) != null)
			{
				for(Id repId : managerReps.get(acct.OwnerId))
				{
					atm = new AccountTeamMember();
					atm.AccountId = acct.id;
					atm.UserId = repId;
					atm.TeamMemberRole = 'Sales Representative';
					atms.add(atm);
					accs = new AccountShare();
					accs.AccountAccessLevel = 'Edit';
					accs.OpportunityAccessLevel = 'Read';
					accs.ContactAccessLevel = 'Read';
					accs.CaseAccessLevel = 'Read';
					accs.AccountId = acct.id;
					accs.UserOrGroupId = repId;
					accss.add(accs);
				}
			}
		}
		
		insert atms;
		insert accss;
		return '';
	}*/

}