/*
 * Date:       		 - Developer, Company					       - Description
 * 2015-04-28        - Solutions Accelerator, Acumen Solutions     - Created
 */

public class CreateAndEditTerritory {
	
	//Selected terrotory (populated in case of edit)
	public Territory__c SelectedTerritory {get; set;}
	//Variable to determine if the action is Create or Edit
	public Boolean isNew {get; set;}
	//All availabe zipcodes that the manager can see
	public String AvailableZips {get; set;}
	//All selected Zipcodes to create territory
	public String SelectedZips {get; set;}
	//All available sales reps list
	private Map<String, User> AllAvailableSalesRepsForManager = new Map<String, User>();

	private String anyLoadErrors = '';

	public String SelectedPillar {get; set;}
	public Id SelectedFiscalYear {get; set;}
	public Id SelectedRegion {get; set;}
	public String regionalManagerName {get; set;}
	private Id selectedManagerId {get;set;}
	private Map<Id,Fiscal_Year__c> fiscalYearsIdToValue ;
	private Map<Id, Id> inactiveReps = new Map<Id, Id>();
	public Id SelectedRep{get;set;}
	public String selectedSalesRep{get;set;}
	private List<SelectOption> areaOptions = new List<SelectOption>();
	public String regSelectedArea {get;set;}

	    
	public static Integer loadSize=300;
    public Integer availableZipsCounterStart = 0;
    public Integer availableZipsCounterEnd = loadSize;
        
    public transient List<PostalCode__c> availableZipCodes;
   
    public boolean showNext {get;set;}
    public boolean showNextAssigned {get;set;}
    public boolean showPreviousAssigned {get;set;}
    public boolean showPrevious {get;set;}

	List<TerritoryAssignment__c> TerritoryZips;


	public pageReference Validate()
	{

		Id currentUserId = UserInfo.getUserId();
		Id currentUserRoleId = UserInfo.getUserRoleId();		
		Profile currentUserProfile = [select name from Profile where id = : UserInfo.getProfileId() Limit 1];
		return null;
	}
	
	//Constructor
	public CreateAndEditTerritory(ApexPages.StandardController controller)
	{
		SelectedZips='';
		SelectedTerritory = (Territory__c)controller.getRecord();	
		getAllFiscalYears();

		if(SelectedTerritory.id == null)
			isNew = true;
		else{
			isNew = false;
		}
		if(isNew){
			//GetAvailableZipCodes();
		}
		else{
			//Get additional information for display and edit
			SelectedTerritory = [Select Id, Name, OwnerId, Region__c, Region__r.Channels__c, Region__r.Area__c, Sync_Needed__c from Territory__c where id =: SelectedTerritory.Id Limit 1];
			selectedSalesRep = SelectedTerritory.OwnerId;
			SelectedRegion = SelectedTerritory.Region__c;
			SelectedPillar = SelectedTerritory.Region__r.Channels__c;
			regSelectedArea = SelectedTerritory.Region__r.Area__c;
			getRegionalManager();
			buildAllSalesrepsForAManager();
			GetAvailableZipCodes();
			GetAssignedZipCodes();
		}
	}
	

    public List<SelectOption> getallSalesReps() {
        List<SelectOption> SalesReps = new List<SelectOption>();
        SelectOption anOption; 
        User u;
        String Label;
        if(AllAvailableSalesRepsForManager.isEmpty()){
        	buildAllSalesrepsForAManager();

        }
        SalesReps.add(new SelectOption(' ', ''));
       List<String> salesrepsList = new List<String>();
       salesrepsList.addAll(AllAvailableSalesRepsForManager.KeySet());
        for(Id uid : salesrepsList)
        {
        	u = AllAvailableSalesRepsForManager.get(uid);
        	if(u.IsActive)
        		Label = u.Name;
        	else 
        		Label = u.Name + ' (InActive)';
        	anOption = new SelectOption(uid, Label);
			SalesReps.add(anOption);
		} 
		return SalesReps;
    }

    


    //Build all salesreps list and return a string in case of an error, else reutrn Blank
    private String buildAllSalesrepsForAManager(){
    	if(String.isBlank(selectedManagerId) || String.isBlank(selectedPillar)){
    		return '';
    	}
		
		
		AllAvailableSalesRepsForManager.clear();
		//get all salesreps for the manager

		//List<User> allSubordinateUsers = [select id, name, ManagerId, IsActive from User where ManagerId = : selectedManagerId and channel__c = :SelectedPillar];
		List<User> allSubordinateUsers = [select id, name, ManagerId, IsActive from User where ( (ManagerId = : selectedManagerId and channel__c = :SelectedPillar) or id = :selectedManagerId)];
		for(User u : allSubordinateUsers){
			AllAvailableSalesRepsForManager.put(u.id, u);
		}
		System.debug('AllAvailableSalesRepsForManager '+AllAvailableSalesRepsForManager);
		return '';
    }
	
	//Method to implement save process
	public pageReference SaveTerritory()
	{
		String SaveError = '';
		try
		{
			List<TerritoryAssignment__c> tas = new List<TerritoryAssignment__c>();
			//System.debug('AllAvailableSalesRepsForManager.get(SelectedTerritory.OwnerId) '+AllAvailableSalesRepsForManager.get(selectedRep));

			//List<Region__c> regions = [Select Id,Pillar__c from Region__c where User__c = :AllAvailableSalesRepsForManager.get(selectedSalesRep).ManagerId and Pillar__c = :SelectedPillar];
			
			System.debug('selectedSalesRep '+selectedSalesRep);

			if(selectedSalesRep == null || selectedSalesRep == '')
			{
				ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select a Sales Rep for the Terrtory');
				ApexPages.addMessage(myMsg);
			}
			if(SelectedZips == null || SelectedZips.trim() == '')
			{
				ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select at least one postal code to save a Territory');
				ApexPages.addMessage(myMsg);
			}
			if(SelectedTerritory.Name == null )
			{
				ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Territory Name cannot be empty');
				ApexPages.addMessage(myMsg);	
			}
			if(inactiveReps.get(SelectedSalesRep) != null)
			{
				ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select an active for user for territory assingment');
				ApexPages.addMessage(myMsg);				
			}
			if(ApexPages.getMessages().size() != 0)
				return null;
			
			

			if(isNew)
			{
				//if creating terrotory, insert new territory record
				System.debug('selectedSalesRep '+selectedSalesRep);
				SelectedTerritory.OwnerId = selectedSalesRep;
				SelectedTerritory.Region__c = SelectedRegion;
				SelectedTerritory.Sync_Needed__c = TRUE;
				upsert SelectedTerritory;
				//Get all the selected zipcodes
				List<String> selectedZipStrings = SelectedZips.trim().split(' ');
				//Create all the Zipcode assignments and save to TerritoryAssignments
				for(String sel : selectedZipStrings)
				{
					tas.add(new TerritoryAssignment__c(TerritoryId__c=SelectedTerritory.Id, PostalCodeID__c=sel.split(';')[0], OwnerId=SelectedTerritory.OwnerId,
								Fiscal_Year__c = fiscalYearsIdToValue.get(SelectedFiscalYear).Id, Sync_Needed__c = TRUE));
				}
				insert tas;
			}
			else
			{
				//Get all zipcodes selected
				List<String> selectedZipStrings = SelectedZips.trim().split(' ');
				//if editing a territoru, save edited territory details
				SelectedTerritory.OwnerId = selectedSalesRep;
				SelectedTerritory.Region__c = SelectedRegion;
				update SelectedTerritory;				
				//helper variables
				List<String> zipIds = new List<String>();
				List<String> zipsToSave = new List<String>();
				List<TerritoryAssignment__c> TAsToSave = new List<TerritoryAssignment__c>();
				Map<Id, TerritoryAssignment__c> matches = new Map<Id, TerritoryAssignment__c>();
				
				//get all selected zipcodes
				for(String sel : selectedZipStrings)
				{
					zipIds.add(sel.split(';')[0]);
				}
				
				//delete extra zips in the system
				List<TerritoryAssignment__c> existing_tas_todelete = [Select Id, Name, PostalCodeId__c from TerritoryAssignment__c where TerritoryID__c =: SelectedTerritory.Id and PostalCodeId__c not in : zipIds];
				delete(existing_tas_todelete);

				if(SelectedZips.trim() != '')
				{
					//get already existing zipcodes to match the selected zipcodes. These zipcodes will not be saved again
					List<TerritoryAssignment__c> existing_tas = [select id, Name, PostalCodeId__c from TerritoryAssignment__c where TerritoryID__c =: SelectedTerritory.Id and PostalCodeId__c in : zipIds];
					for(TerritoryAssignment__c t : existing_tas)
					{
						matches.put(t.PostalCodeId__c, t);
					}
					//if no match exists in territoryAssignment, mark record for save
					for(String zip : zipIds)
					{
						if(matches.get(zip) == null)
							zipsToSave.add(zip);
					}
					
					//If there are new zipcodes assignments to save, create a list of those assignments and insert them
					if(zipsToSave.size() > 0)
					{
						for(String zipsave : zipsToSave)
						{
							TAsToSave.add(new TerritoryAssignment__c(TerritoryId__c=SelectedTerritory.Id, PostalCodeID__c=zipsave, OwnerId=SelectedTerritory.OwnerId, 
											Fiscal_Year__c = fiscalYearsIdToValue.get(SelectedFiscalYear).Id, Sync_Needed__c = TRUE ));
						}
						insert TAsToSave;
					}
				}
			}
		}
		catch (Exception ex)
		{
			SaveError = ex.getMessage();
			if(!SaveError.contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'))
			{
				ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, SaveError);
				ApexPages.addMessage(myMsg);
			}
			return null;
		}
		//return to the saved territory
		return new pageReference('/' + SelectedTerritory.Id);
	} 

	public void populateZipsAndRegManager(){
		availableZipsCounterStart = 0;
        availableZipsCounterEnd = loadSize;
		GetAvailableZipCodes();
        GetAssignedZipCodes();
		//getRegionalManager();
		getAllRegions();
	}
	
	//Get all available zipcodes for a manager
	public void GetAvailableZipCodes()
	{
		AvailableZips = '';
		List<Id> allUsedZips = new List<Id>();

		
		//get the user id and the related towns that the user/manager is assigned to
		Id currentUserId = UserInfo.getUserId();
		Id currentUserRoleId = UserInfo.getUserRoleId();

		Id fiscalYearId = (SelectedFiscalYear == null) ? getCurrentFiscalYear() : SelectedFiscalYear;
		
		//get all the zip codes already assigned in territory assignments object
		List<TerritoryAssignment__c> ta = [select PostalcodeId__c from TerritoryAssignment__c 
										   where Fiscal_Year__c =  :fiscalYearId and  territoryID__r.Region__r.Channels__c = : SelectedPillar];
		List<Id> usedZipcodes = new List<Id>();
		for(TerritoryAssignment__c terr : ta)
		{
			usedZipCodes.add(terr.PostalCodeId__c);
		}

		//get the final list of available zipcodes by applying all filters created above
		availableZipCodes = [select id, name, Channel__c from PostalCode__c where Id not in : usedZipCodes and Channel__c = : SelectedPillar];
	
        if(availableZipCodes.size()<1){
            return;
        }
        if(availableZipsCounterEnd +loadSize <= availableZipCodes.size()){
        	showNext = true;
        }
        else{
        	showNext = false;
        }
        if(availableZipsCounterStart < 0){
        	availableZipsCounterStart = 0;
        }
        if(availableZipsCounterEnd > availableZipCodes.size()){
        	availableZipsCounterEnd = availableZipCodes.size();
        }
 		for(integer i = availableZipsCounterStart ; i < availableZipsCounterEnd; i++){
 			PostalCode__c z = availableZipCodes.get(i);
 			AvailableZips += z.ID + ';' + z.Name + ';' + z.Channel__c + ' ';
 		}
	}
	
	//get all the assigned zipcodes for a territory in case of territory editing
	private void GetAssignedZipCodes()
	{
		
		Id fiscalYearId = (SelectedFiscalYear == null) ? getCurrentFiscalYear() : SelectedFiscalYear;
		System.debug('fiscalYearId '+fiscalYearId);
		
		TerritoryZips = [Select Name, PostalCode__c, PostalCodeID__c, TerritoryName__C,PostalcodeID__r.Channel__c from TerritoryAssignment__C
													  where TerritoryId__c =: SelectedTerritory.Id and Fiscal_Year__c = :fiscalYearId and PostalCode__c != null];
		for(TerritoryAssignment__c z : TerritoryZips)
		{
			SelectedZips += z.PostalCodeID__c + ';' + z.PostalCode__c + ';' + z.PostalcodeID__r.Channel__c + ' ';
		}
	}

	
	public List<SelectOption> getAllPillars(){
		List<Channels_for_TA__c> channelsForTerritoryAssignment = Channels_for_TA__c.getAll().values();
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption('',''));
		for(Channels_for_TA__c channelForTA: channelsForTerritoryAssignment){
            options.add(new SelectOption(channelForTA.Value__c,channelForTA.Name));
		}
		return options;
	}

	public Id getCurrentFiscalYear(){
		Fiscal_Year__c fy = [Select id from Fiscal_Year__c where IsActive__c = :TRUE];
		return fy.id;
	}

	public List<SelectOption> getAllFiscalYears(){
		fiscalYearsIdToValue = new Map<Id,Fiscal_Year__c>([Select Id, Name, IsActive__c, Previous_Fiscal_Year__r.Id, Previous_Fiscal_Year__r.Name 
			 											   from Fiscal_Year__c where ( isActive__c = :true or Start_Date__c > :System.now().date()) 
			 											   order by Start_Date__c]);
		List<SelectOption> options = new List<SelectOption>();
		for(Fiscal_Year__c fy : fiscalYearsIdToValue.values()){
			options.add(new SelectOption(fy.Id,fy.Name));
			if(fy.isActive__c){
				SelectedFiscalYear = fy.id;
			}
		}
		return options;
	}


	public List<SelectOption> getAllRegions(){
		System.debug('Selected Pillar '+ selectedPillar);
		List<Region__c> regions = [Select Name, id from Region__c where Channels__c = :selectedPillar];
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption('',''));
		for(Region__c rg : regions){
			options.add(new SelectOption(rg.Id,rg.Name));
		}
		return options;
	}

	public void getRegionalManager(){
		System.debug('SelectedRegion '+SelectedRegion);
		if(SelectedRegion != null) {
			List<Region__c> regList= [Select User__c, User__r.Name from Region__c where id = :SelectedRegion];
			if(regList.size()>0){
				regionalManagerName = regList[0].User__r.Name;
				selectedManagerId = regList[0].User__c;
			}
			if(!String.isBlank(regionalManagerName)){
				anyLoadErrors = buildAllSalesrepsForAManager();
			}
		}
	}

	public void getRegionalManagerAndArea(){
		getRegionalManager();
		getSelectedArea();
	}

	public void getSelectedArea(){
		List<Region__c> regList = [Select area__c from Region__c where id = : SelectedRegion];
		if(regList.size() > 0){
			regSelectedArea = reglist[0].area__c;
		}
	}


    public PageReference previousPage(){
        availableZipsCounterStart -= loadSize;
        availableZipsCounterEnd -=   loadSize;
        if(availableZipsCounterStart == 0){
        	showPrevious = false;
        }
        return changeData();
    }
     
    public PageReference nextPage(){

        availableZipsCounterStart += loadSize;
        availableZipsCounterEnd +=   loadSize;

        if(availableZipsCounterStart > 0){
    		showPrevious = true;
    	}
        return changeData();
    }
     
    public PageReference changeData(){
            GetAvailableZipCodes();
        return null;
    }     

	
}