public with sharing class giic_SalesOrderConversionBatchController {
    private Id batchprocessid;  
    public List<BatchJob> batchJobs {get;set;}
    public boolean isExecuteAllocationCompleted {get;set;}      
    public Boolean goBack{get;set;}
    
    public giic_SalesOrderConversionBatchController(ApexPages.StandardSetController controller) {
        isExecuteAllocationCompleted=false;
        batchJobs = new List<BatchJob>();
    }
    
    public void executeSOConversion(){
        
        try{
            //Prepare setting accourding to custom metadata
            String sObjectApiName = giic_Constants.OBJ_SALES_ORDER_STAGING;
            Map<String, giic_IntegrationCustomMetaDataWrapper> mapIntegrationSettings = giic_IntegrationCustomMetaDataUtil.getIntegrationSettings(sObjectApiName);
            giic_SOStagingToSOConversionBatch batch = new giic_SOStagingToSOConversionBatch(mapIntegrationSettings, sObjectApiName );
            batchprocessid = Database.executeBatch(batch);
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM, System.Label.giic_BatchJobSubmitted));
            goBack = false;
        }catch(Exception ex){ ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getMessage())); }
    }
    
    public void findBatchJobs(){
        //Create new list of BatchJobs, a wrapper class that includes the job and percent complete.
        batchJobs = new List<BatchJob>();
    
        map<string,string> bgColorMap=new map<string,string>();
        map<string,string> fgColorMap=new map<string,string>();
         for(giic_BatchColorCode__mdt colorCode : [SELECT Id, DeveloperName, QualifiedApiName, giic_fgColor__c, giic_bgColor__c 
                                                  FROM giic_BatchColorCode__mdt]){
            bgColorMap.put(colorCode.DeveloperName, colorCode.giic_bgColor__c);
            fgColorMap.put(colorCode.DeveloperName, colorCode.giic_fgColor__c);
                                                            
        }
    
        //Query the Batch apex jobs
        for(AsyncApexJob asyncJ : [select TotalJobItems, Status, NumberOfErrors, MethodName, JobType, JobItemsProcessed, ExtendedStatus, Id, CreatedDate, CreatedById, CompletedDate, ApexClassId, ApexClass.Name From AsyncApexJob WHERE JobType=: giic_Constants.BATCH_APEX  and id=: batchprocessid]){
            Double itemsProcessed = asyncJ.JobItemsProcessed;
            Double totalItems = asyncJ.TotalJobItems;
    
            BatchJob bJob = new BatchJob();
            bJob.job = asyncJ;
            
            //Determine the pecent complete based on the number of batches complete
            if(totalItems == 0){
                //A little check here as we don't want to divide by 0.
                bJob.percentComplete = 0;
            }else{
                bJob.percentComplete = ((itemsProcessed  / totalItems) * 100.0).intValue();
            }
            bJob.bgStatusColor=bgColorMap.get(asyncJ.Status);
            bJob.fgStatusColor=fgColorMap.get(asyncJ.Status);
            if(asyncJ.status == 'Completed'){
                bJob.percentComplete = 100;
                bJob.bgStatusColor=bgColorMap.get(asyncJ.Status);
                bJob.fgStatusColor=fgColorMap.get(asyncJ.Status);
            }
            batchJobs.add(bJob);
        }

    }
    //This is the wrapper class the includes the job itself and a value for the percent complete
    public Class BatchJob{
        public AsyncApexJob job {get; set;}
        public Integer percentComplete {get; set;}
        public string bgStatusColor {get;set;}
        public string fgStatusColor {get;set;}
    
        public BatchJob(){
            this.job=null;
            this.percentComplete=0;
            bgStatusColor='';
            fgStatusColor='';
        }
    }
    
    public PageReference goBack(){
        PageReference pObj ;
        try{          
            Schema.DescribeSObjectResult result = gii__SalesOrderStaging__c.SObjectType.getDescribe();               
            pObj = new PageReference('/' + result.getKeyPrefix() + '/o'); // redirect to sales order Staging list view
            pObj.setRedirect(true);            
        }catch(Exception ex){ ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getMessage())); return null;  }
        return pObj;
    }

}