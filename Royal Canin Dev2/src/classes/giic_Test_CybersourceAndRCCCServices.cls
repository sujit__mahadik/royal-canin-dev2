/************************************************************************************
Version : 1.0
Name : giic_Test_CybersourceAndRCCCServices 
Created Date : 7 Oct 2018
Function : Tests call the Http callouts to Cybersource and RC-CC Service.
		   Main methods are written in giic_CyberSourceRCCCUtility
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/
@isTest
public class giic_Test_CybersourceAndRCCCServices{
    /*
    * Method name : setup 
    * Description : creates the test data.
    * Return Type : void
    * Parameter :   
    */
    @testSetup
    static void setup(){
        
        giic_Test_DataCreationUtility.CreateAdminUser(); 
        // Create consumer account
        List<Account> testConsumerAccountList = giic_Test_DataCreationUtility.insertConsumerAccount();
        // create warehouse
        List<gii__Warehouse__c> testWareList = giic_Test_DataCreationUtility.insertWarehouse();
        SetupTestData.createCustomSettings();
        // create system policy
        giic_Test_DataCreationUtility.testSystemPolicyCreation();
        // create  Account refrence
        List<gii__AccountAdd__c> testAccRefList = giic_Test_DataCreationUtility.getAccountReference(testConsumerAccountList);
        Database.insert(testAccRefList);
        // create products and product reference
        List<gii__Product2Add__c> testProductList = giic_Test_DataCreationUtility.insertProduct();
        //Create carrier
        List<gii__Carrier__c> testCreateCarrier = giic_Test_DataCreationUtility.createCarrier();
        // create Sales Order
        List<gii__SalesOrder__c> testSOList = giic_Test_DataCreationUtility.insertSalesOrder(testConsumerAccountList[0].Id, testAccRefList[0].Id);
        // create Sales Order line
        List<gii__SalesOrderLine__c> testSOLList = giic_Test_DataCreationUtility.insertSOLine(testSOList[0].Id, testProductList[0].Id);
        //create payment record
         giic_Test_DataCreationUtility.insertSalesOrderPayment(testSOList[0].Id);
        
        List<gii__SalesOrder__c> soList = [SELECT Id, Name, gii__Released__c, gii__PaymentMethod__c,gii__Carrier__r.giic_UniqueCarrier__c,gii__Warehouse__r.giic_WarehouseCode__c FROM gii__SalesOrder__c ];
        for(gii__SalesOrder__c objSO : soList){
            objSO.gii__Released__c = true;
        }
        update soList;
        
        //Insert shipment records. Fulfillment records are increated internally.
        List<gii__WarehouseShipmentAdviceStaging__c> shipmentRecords = giic_Test_DataCreationUtility.insertWarehouseShipmentAdviceStaging(soList);
        //Insert Order Invoice
        List<gii__OrderInvoice__c> orderInvoices = giic_Test_DataCreationUtility.createOrderInvoice(soList);
    }
     /*
    * Method name : testPayerAuthValidationCallout 
    * Description : 
    * Return Type : void
    * Parameter :   
    */
    @isTest static void testPayerAuthValidationCallout(){
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        System.Runas(u)  
        {
        Test.startTest();
        
        gii__SalesOrder__c objSO = [SELECT Id, gii__Account__r.ShipToCustomerNo__c  FROM gii__SalesOrder__c LIMIT 1];
        
        Test.setMock(HttpCalloutMock.class, new giic_Test_GenericCSResponseMock());
        
        Map<String, Object> mapReqParams = new Map<String, Object>();
        giic_CreditCardWrapper card = new giic_CreditCardWrapper();
               
        card.CardNumber = '4111111111111111';
        card.ExpiryMonth = '12';
        card.ExpiryYear = '2020';
        
        //giic_CreditCardWrapper newCard = new giic_CreditCardWrapper();
        card.SObjName = 'gii__SalesOrder__c';
        card.recordId = objSO.Id;
        card.ShipToCustomerNo = objSO.gii__Account__r.ShipToCustomerNo__c;
      
        
        mapReqParams.put('card', card);
        giic_CyberSourceRCCCUtility.payerAuthValidationCC(mapReqParams);
    }
    }
     /*
    * Method name : testProfileCreateCallout 
    * Description : 
    * Return Type : void
    * Parameter :   
    */
    @isTest static void testProfileCreateCallout(){
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
         System.Runas(u)  
         {
        Test.startTest();
        gii__SalesOrder__c objSO = [SELECT Id, gii__Account__r.ShipToCustomerNo__c  FROM gii__SalesOrder__c LIMIT 1];
        Account acc = [SELECT Id, BillingStreet, BillingState, BillingPostalCode, 
                       BillingCity, BillingCountry, Business_Email__c, ShipToCustomerNo__c 
                       FROM Account LIMIT 1];
        Test.setMock(HttpCalloutMock.class, new giic_Test_GenericCSResponseMock());
        giic_CreditCardWrapper card = new giic_CreditCardWrapper();
        Map<String, Object> profileCreateReq = new Map<String, Object>();
        giic_BillToWrapper billToWrap = new giic_BillToWrapper();
        
        card.CardNumber = '4111111111111111';
        card.ExpiryMonth = '12';
        card.ExpiryYear = '2020';
        
        //List<String> names = card.NameOnCard.split(' ');
        //Integer lenOfName = names.size();
        billToWrap.firstName = 'John';
        billToWrap.lastName = 'Doe';
        billToWrap.street = acc.BillingStreet;
        billToWrap.city = acc.BillingCity;
        billToWrap.state = acc.BillingState;
        billToWrap.postalCode = acc.BillingPostalCode;
        billToWrap.country = acc.BillingCountry;
        billToWrap.email = acc.Business_Email__c;
        
        //giic_CreditCardWrapper newCard = new giic_CreditCardWrapper();
        card.SObjName = 'gii__SalesOrder__c';
        card.recordId = objSO.Id;
        card.ShipToCustomerNo = objSO.gii__Account__r.ShipToCustomerNo__c;
        
        //profileCreateReq.put('NewCard', newCard);
        profileCreateReq.put('billTo',billToWrap);
        profileCreateReq.put('card', card);
        
        Map<String, Object> dropPaymentAuthorizationRes = new Map<String, Object>();
        dropPaymentAuthorizationRes = giic_CyberSourceRCCCUtility.profileCreateWithoutFeeCC(profileCreateReq);
        Test.stopTest();
    }
    }
     /*
    * Method name : testProfileUpdateCallout 
    * Description : 
    * Return Type : void
    * Parameter :   
    */
    @isTest static void testProfileUpdateCallout(){
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        System.Runas(u)  
         {
        Test.startTest();
        
        Test.setMock(HttpCalloutMock.class, new giic_Test_GenericCSResponseMock());
        
        Map<String, Object> mapReqParams = new Map<String, Object>();
        giic_CreditCardWrapper card = new giic_CreditCardWrapper();
               
        card.CardNumber = '4111111111111111';
        card.ExpiryMonth = '12';
        card.ExpiryYear = '2020';
        card.SubscriptionID = '77DF56B70366E57BE05341588E0A2A68';
        
        mapReqParams.put('card', card);
        
        giic_CyberSourceRCCCUtility.updateCardDetailCC(mapReqParams);
        
        Test.stopTest();
    }
    }
     /*
    * Method name : testProfileDeleteCallout 
    * Description : 
    * Return Type : void
    * Parameter :   
    */
    @isTest static void testProfileDeleteCallout(){
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        System.Runas(u)  
        {
        Test.startTest();
        
        Test.setMock(HttpCalloutMock.class, new giic_Test_GenericCSResponseMock());
        
        Map<String, Object> mapReqParams = new Map<String, Object>();
        giic_CreditCardWrapper card = new giic_CreditCardWrapper();

        card.SubscriptionID = '76AF86BDC1CB57ECE05341588E0A395B';
        
        mapReqParams.put('card', card);
        
        giic_CyberSourceRCCCUtility.deleteCardDetailCC(mapReqParams);
        
        Test.stopTest();
        
    }
    }
     /*
    * Method name : testPaymentAuthCallout 
    * Description : 
    * Return Type : void
    * Parameter :   
    */
    @isTest static void testPaymentAuthCallout(){
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
       System.Runas(u)  
       {
        Test.startTest();
        Account acc = [SELECT Id, BillingStreet, BillingState, BillingPostalCode, 
                       BillingCity, BillingCountry, Business_Email__c, ShipToCustomerNo__c 
                       FROM Account LIMIT 1];
        Test.setMock(HttpCalloutMock.class, new giic_Test_GenericCSResponseMock());
        giic_CreditCardWrapper card = new giic_CreditCardWrapper();

        Map<String, Object> mapReqParams = new Map<String, Object>();
        giic_BillToWrapper billToWrap = new giic_BillToWrapper();
        
        card.CardNumber = '4111111111111111';
        card.ExpiryMonth = '12';
        card.ExpiryYear = '2020';
        
        //List<String> names = card.NameOnCard.split(' ');
        //Integer lenOfName = names.size();
        billToWrap.firstName = 'John';
        billToWrap.lastName = 'Doe';
        billToWrap.street = acc.BillingStreet;
        billToWrap.city = acc.BillingCity;
        billToWrap.state = acc.BillingState;
        billToWrap.postalCode = acc.BillingPostalCode;
        billToWrap.country = acc.BillingCountry;
        billToWrap.email = acc.Business_Email__c;

        mapReqParams.put('billTo',billToWrap);
        mapReqParams.put('totalAmount',35.0);
        mapReqParams.put('card', card);

        Map<String, Object> PaymentAuthorizationRes = new Map<String, Object>();
        PaymentAuthorizationRes = giic_CyberSourceRCCCUtility.paymentAuthorizationCyberSource(mapReqParams);
        
        Test.stopTest();
        
    }
    }
     /*
    * Method name : testDropAuthCallout 
    * Description : 
    * Return Type : void
    * Parameter :   
    */
    @isTest static void testDropAuthCallout(){
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        System.Runas(u)  
           {
        Test.startTest();
        Account acc = [SELECT Id, BillingStreet, BillingState, BillingPostalCode, 
                       BillingCity, BillingCountry, Business_Email__c, ShipToCustomerNo__c 
                       FROM Account LIMIT 1];
        Test.setMock(HttpCalloutMock.class, new giic_Test_GenericCSResponseMock());
        giic_CreditCardWrapper card = new giic_CreditCardWrapper();
        Map<String, Object> authToCanReq = new Map<String, Object>();
        Map<String, Object> mapReqParams = new Map<String, Object>();
        giic_BillToWrapper billToWrap = new giic_BillToWrapper();
        
        card.CardNumber = '4111111111111111';
        card.ExpiryMonth = '12';
        card.ExpiryYear = '2020';
        
        //List<String> names = card.NameOnCard.split(' ');
        //Integer lenOfName = names.size();
        billToWrap.firstName = 'John';
        billToWrap.lastName = 'Doe';
        billToWrap.street = acc.BillingStreet;
        billToWrap.city = acc.BillingCity;
        billToWrap.state = acc.BillingState;
        billToWrap.postalCode = acc.BillingPostalCode;
        billToWrap.country = acc.BillingCountry;
        billToWrap.email = acc.Business_Email__c;
        
        mapReqParams.put('requestID', '5389093851166427304008');
        mapReqParams.put('requestToken', 'Ahj//wSTJCSzcP4wu9BIESDdizYuWDVvFm14bibRcJb62SZcQClxEzT5B6QM2sgWIZNJMvRiu4DrFAnJkhJZOed2w7CmAAAArwaP');
        
        authToCanReq.put('billTo',billToWrap);
        authToCanReq.put('totalAmount',35.0);
        authToCanReq.put('card', card);
        authToCanReq.put('mapReqParams', mapReqParams);
        
        Map<String, Object> dropPaymentAuthorizationRes = new Map<String, Object>();
        dropPaymentAuthorizationRes = giic_CyberSourceRCCCUtility.dropPaymentAuthorizationCyberSource(authToCanReq);
        //HttpResponse res = giic_CyberSourceRCCCUtility.getDropAuthCSResponse();
        Test.stopTest();
    }
    }
     /*
    * Method name : testPaymentCaptureCallout 
    * Description : 
    * Return Type : void
    * Parameter :   
    */
    @isTest static void testPaymentCaptureCallout(){
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
         System.Runas(u)  
         {
        Test.startTest();
        Account acc = [SELECT Id, BillingStreet, BillingState, BillingPostalCode, 
                       BillingCity, BillingCountry, Business_Email__c, ShipToCustomerNo__c 
                       FROM Account LIMIT 1];
        Test.setMock(HttpCalloutMock.class, new giic_Test_GenericCSResponseMock());
        giic_CreditCardWrapper card = new giic_CreditCardWrapper();
        Map<String, Object> authToCanReq = new Map<String, Object>();
        Map<String, Object> mapReqParams = new Map<String, Object>();
        giic_BillToWrapper billToWrap = new giic_BillToWrapper();
        
        card.CardNumber = '4111111111111111';
        card.ExpiryMonth = '12';
        card.ExpiryYear = '2020';
        
        //List<String> names = card.NameOnCard.split(' ');
        //Integer lenOfName = names.size();
        billToWrap.firstName = 'John';
        billToWrap.lastName = 'Doe';
        billToWrap.street = acc.BillingStreet;
        billToWrap.city = acc.BillingCity;
        billToWrap.state = acc.BillingState;
        billToWrap.postalCode = acc.BillingPostalCode;
        billToWrap.country = acc.BillingCountry;
        billToWrap.email = acc.Business_Email__c;
        
        mapReqParams.put('requestID', '5389093851166427304008');
        mapReqParams.put('requestToken', 'Ahj//wSTJCSzcP4wu9BIESDdizYuWDVvFm14bibRcJb62SZcQClxEzT5B6QM2sgWIZNJMvRiu4DrFAnJkhJZOed2w7CmAAAArwaP');
        
        authToCanReq.put('billTo',billToWrap);
        authToCanReq.put('totalAmount',69.12);
        authToCanReq.put('card', card);
        authToCanReq.put('mapReqParams', mapReqParams);
        
        Map<String, Object> capturePaymentRes = new Map<String, Object>();
        capturePaymentRes = giic_CyberSourceRCCCUtility.capturePaymentCyberSource(authToCanReq);
        //HttpResponse res = giic_CyberSourceRCCCUtility.getDropAuthCSResponse();
        Test.stopTest();
    }
    }
     /*
    * Method name : testPaymentRefundCallout 
    * Description : 
    * Return Type : void
    * Parameter :   
    */
    @isTest static void testPaymentRefundCallout(){
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        System.Runas(u)  
          {
        Test.startTest();
        Account acc = [SELECT Id, BillingStreet, BillingState, BillingPostalCode, 
                       BillingCity, BillingCountry, Business_Email__c, ShipToCustomerNo__c 
                       FROM Account LIMIT 1];
        Test.setMock(HttpCalloutMock.class, new giic_Test_GenericCSResponseMock());
        giic_CreditCardWrapper card = new giic_CreditCardWrapper();
        Map<String, Object> authToCanReq = new Map<String, Object>();
        Map<String, Object> mapReqParams = new Map<String, Object>();
        giic_BillToWrapper billToWrap = new giic_BillToWrapper();
        
        card.CardNumber = '4111111111111111';
        card.ExpiryMonth = '12';
        card.ExpiryYear = '2020';
        
        //List<String> names = card.NameOnCard.split(' ');
        //Integer lenOfName = names.size();
        billToWrap.firstName = 'John';
        billToWrap.lastName = 'Doe';
        billToWrap.street = acc.BillingStreet;
        billToWrap.city = acc.BillingCity;
        billToWrap.state = acc.BillingState;
        billToWrap.postalCode = acc.BillingPostalCode;
        billToWrap.country = acc.BillingCountry;
        billToWrap.email = acc.Business_Email__c;
        
        mapReqParams.put('requestID', '5389093851166427304008');
        mapReqParams.put('requestToken', 'Ahj//wSTJCSzcP4wu9BIESDdizYuWDVvFm14bibRcJb62SZcQClxEzT5B6QM2sgWIZNJMvRiu4DrFAnJkhJZOed2w7CmAAAArwaP');
        
        authToCanReq.put('billTo',billToWrap);
        authToCanReq.put('totalAmount',69.12);
        authToCanReq.put('card', card);
        authToCanReq.put('mapReqParams', mapReqParams);
        
        Map<String, Object> capturePaymentRes = new Map<String, Object>();
        capturePaymentRes = giic_CyberSourceRCCCUtility.refundPaymentCyberSource(authToCanReq);
        //HttpResponse res = giic_CyberSourceRCCCUtility.getDropAuthCSResponse();
        Test.stopTest();
    }
    }
     /*
    * Method name : testSettlementCallout 
    * Description : 
    * Return Type : void
    * Parameter :   
    */
    @isTest static void testSettlementCallout(){
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        System.Runas(u)  
          {
        Test.startTest();
        Account acc = [SELECT Id, BillingStreet, BillingState, BillingPostalCode, 
                       BillingCity, BillingCountry, Business_Email__c, ShipToCustomerNo__c 
                       FROM Account LIMIT 1];
        Test.setMock(HttpCalloutMock.class, new giic_Test_GenericCSResponseMock());
        giic_CreditCardWrapper card = new giic_CreditCardWrapper();
        Map<String, Object> authToCanReq = new Map<String, Object>();
        Map<String, Object> mapReqParams = new Map<String, Object>();
        giic_BillToWrapper billToWrap = new giic_BillToWrapper();
        
        card.CardNumber = '4111111111111111';
        card.ExpiryMonth = '12';
        card.ExpiryYear = '2020';
        
        //List<String> names = card.NameOnCard.split(' ');
        //Integer lenOfName = names.size();
        billToWrap.firstName = 'John';
        billToWrap.lastName = 'Doe';
        billToWrap.street = acc.BillingStreet;
        billToWrap.city = acc.BillingCity;
        billToWrap.state = acc.BillingState;
        billToWrap.postalCode = acc.BillingPostalCode;
        billToWrap.country = acc.BillingCountry;
        billToWrap.email = acc.Business_Email__c;
        
        mapReqParams.put('requestID', '5389093851166427304008');
        mapReqParams.put('requestToken', 'Ahj//wSTJCSzcP4wu9BIESDdizYuWDVvFm14bibRcJb62SZcQClxEzT5B6QM2sgWIZNJMvRiu4DrFAnJkhJZOed2w7CmAAAArwaP');
        
        authToCanReq.put('billTo',billToWrap);
        authToCanReq.put('totalAmount',69.12);
        authToCanReq.put('card', card);
        authToCanReq.put('mapReqParams', mapReqParams);
        
        Map<String, Object> captureSettlementRes = new Map<String, Object>();
        captureSettlementRes = giic_CyberSourceRCCCUtility.paymentSettlementCyberSource(authToCanReq);
        //HttpResponse res = giic_CyberSourceRCCCUtility.getDropAuthCSResponse();
        Test.stopTest();
    }
    }
    /*
    * Method name : testHandleError 
    * Description : tests the error handling part
    * Return Type : void
    * Parameter :   
    */
    @isTest static void testHandleError(){
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        System.Runas(u)  
          {
        
        gii__SalesOrder__c salesObj = [SELECT Id, Name FROM gii__SalesOrder__c LIMIT 1];
        gii__WarehouseShippingOrderStaging__c fulfillment = [SELECT Id FROM gii__WarehouseShippingOrderStaging__c 
                                                               WHERE giic_SalesOrder__c =: salesObj.Id
                                                               LIMIT 1];
        gii__WarehouseShipmentAdviceStaging__c shipment = [SELECT Id FROM gii__WarehouseShipmentAdviceStaging__c LIMIT 1];
        
        gii__OrderInvoice__c invoice = [SELECT Id FROM gii__OrderInvoice__c LIMIT 1];
        
        Test.startTest();
        	//void handleErrorFuture(String recId, String userStory, String code, String message)
        	giic_CyberSourceRCCCUtility.handleErrorFuture(fulfillment.Id, giic_Constants.CYBERSOURCE_USER_STORY, 'String.valueOf(res.getStatusCode())', System.Label.giic_CCProfileCreateNoRes + System.Label.giic_ErrReasonCode);
        	giic_CyberSourceRCCCUtility.handleErrorFuture(salesObj.Id, giic_Constants.CYBERSOURCE_USER_STORY, 'String.valueOf(res.getStatusCode())', System.Label.giic_CCProfileCreateNoRes + System.Label.giic_ErrReasonCode);
        	giic_CyberSourceRCCCUtility.handleErrorFuture(shipment.Id, giic_Constants.CYBERSOURCE_USER_STORY, 'String.valueOf(res.getStatusCode())', System.Label.giic_CCProfileCreateNoRes + System.Label.giic_ErrReasonCode);
        	giic_CyberSourceRCCCUtility.handleErrorFuture(invoice.Id, giic_Constants.CYBERSOURCE_USER_STORY, 'String.valueOf(res.getStatusCode())', System.Label.giic_CCProfileCreateNoRes + System.Label.giic_ErrReasonCode);
        	giic_CyberSourceRCCCUtility.handleError(fulfillment.Id, giic_Constants.CYBERSOURCE_USER_STORY, 'String.valueOf(res.getStatusCode())', System.Label.giic_CCProfileCreateNoRes + System.Label.giic_ErrReasonCode);
        	giic_CyberSourceRCCCUtility.handleError(salesObj.Id, giic_Constants.CYBERSOURCE_USER_STORY, 'String.valueOf(res.getStatusCode())', System.Label.giic_CCProfileCreateNoRes + System.Label.giic_ErrReasonCode);
        	giic_CyberSourceRCCCUtility.handleError(shipment.Id, giic_Constants.CYBERSOURCE_USER_STORY, 'String.valueOf(res.getStatusCode())', System.Label.giic_CCProfileCreateNoRes + System.Label.giic_ErrReasonCode);
        	giic_CyberSourceRCCCUtility.handleError(invoice.Id, giic_Constants.CYBERSOURCE_USER_STORY, 'String.valueOf(res.getStatusCode())', System.Label.giic_CCProfileCreateNoRes + System.Label.giic_ErrReasonCode);
        
        Test.stopTest();
    }
    }
     /*
    * Method name : testWriteHistory 
    * Description : tests the WriteHistory API
    * Return Type : void
    * Parameter :   
    */
    @isTest static void testWriteHistory(){
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        System.Runas(u)  
         {
        giic_CreditCardWrapper history = new giic_CreditCardWrapper();
        //To-dos : Remove hard coding and get the values from wrapper
        history.IsActive = true;
        history.AuthCode = '888888';
        history.BillToCustomerNo = 'POS-700478';
        history.ExpiryMonth = '6';
        history.ExpiryYear = '2025';
        history.CardNumber = '4111111111111111';
        history.CreditCardHistoryID = 0; //Need to check this field
        //public schemasDatacontractOrg200407RcComm.CreditCardType CreditCardType;
        history.CreditCardTypeID = 19;
        history.DateTimeCreate =  String.valueOf(System.now());
        history.IsDefaultCard = false;
        history.ProcessorID = 3;
        history.ShipToCustomerNo = 'POS-700478-001';
        history.SubscriptionID = '5446959589686395203012';
        history.CardType = 'Visa';
        history.CardTypeValue = '001';
        
        Test.startTest();
        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new giic_Test_GenericRCCCServiceMock());
        Map<String, Object> response = giic_CyberSourceRCCCUtility.writeHistoryRCCC(history);
        System.debug('::response::'+response);
        Test.stopTest();
    }
    }
   /*
    * Method name : testGetCardHistoryByBillToCustomerOMS 
    * Description : tests the GetCardHistoryByBillToCustomerOMS API 
    * Return Type : void
    * Parameter :   
    */
    @isTest static void testGetCardHistoryByBillToCustomerOMS(){
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
         System.Runas(u)  
         {
        gii__SalesOrder__c salesObj = [SELECT Id, Name FROM gii__SalesOrder__c LIMIT 1];
        Test.startTest();
        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new giic_Test_GenericRCCCServiceMock());
        Map<String, Object> response = giic_CyberSourceRCCCUtility.getCardHistoryByBillToCustomerRCCC('POS-700478', String.valueOf(salesObj.Id));
        System.debug('::response::'+response);
        Test.stopTest();
    }
    }
      /*
    * Method name : testUpdateCreditCardHistory 
    * Description : tests the UpdateCreditCardHistory API 
    * Return Type : void
    * Parameter :   
    */
    @isTest static void testUpdateCreditCardHistory(){
         User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
         System.Runas(u)  
         {
        gii__SalesOrder__c salesObj = [SELECT Id, Name FROM gii__SalesOrder__c LIMIT 1];
        giic_CreditCardWrapper editCardWrapper = new giic_CreditCardWrapper();
        editCardWrapper.ExpiryMonth = '5';
        editCardWrapper.ExpiryYear = '2024';
        String token = '5446959589686395203012';
        
        Test.startTest();
        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new giic_Test_GenericRCCCServiceMock());
        Map<String, Object> response = giic_CyberSourceRCCCUtility.updateCreditCardHistoryRCCC(salesObj.Id, token, editCardWrapper);
        System.debug('::response::'+response);
        Test.stopTest();
    }
    }
     /*
    * Method name : testDeleteCreditCardHistory 
    * Description : tests the DeleteCreditCardHistory API 
    * Return Type : void
    * Parameter :   
    */
    @isTest static void testDeleteCreditCardHistory(){
         User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
         System.Runas(u)  
         {
        gii__SalesOrder__c salesObj = [SELECT Id, Name FROM gii__SalesOrder__c LIMIT 1];
        String token = '5446959589686395203012';
        
         Test.startTest();
        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new giic_Test_GenericRCCCServiceMock());
        Map<String, Object> response = giic_CyberSourceRCCCUtility.deleteCreditCardHistoryRCCC(salesObj.Id, token);
        System.debug('::response::'+response);
        Test.stopTest();
         }  
    }
    /*
    * Method name : testDeleteCreditCardHistory 
    * Description : tests the DefaultCreditCardHistory API 
    * Return Type : void
    * Parameter :   
    */
    @isTest static void testSetDefaultCreditCardHistory(){
         User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
         System.Runas(u)  
         {
        gii__SalesOrder__c salesObj = [SELECT Id, Name FROM gii__SalesOrder__c LIMIT 1];
        String token = '5446959589686395203012';
        
         Test.startTest();
        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new giic_Test_GenericRCCCServiceMock());
        giic_CyberSourceRCCCUtility.setDefaultCreditCardHistoryRCCC(salesObj.Id, token);
        Test.stopTest();
         } 
    }
     /*
    * Method name : testAuthorizationOMS 
    * Description : tests the AuthorizationOMS API 
    * Return Type : void
    * Parameter :   
    */
    @isTest static void testAuthorizationOMS(){
         User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
         System.Runas(u)  
         {
        gii__SalesOrder__c salesObj = [SELECT Id, Name FROM gii__SalesOrder__c LIMIT 1];
        gii__WarehouseShippingOrderStaging__c fulfillment = [SELECT Id FROM gii__WarehouseShippingOrderStaging__c 
                                                               WHERE giic_SalesOrder__c =: salesObj.Id
                                                               LIMIT 1];
        Map<String, Object> mapParams = new Map<String, Object>();
        giic_BillToWrapper billTo = new giic_BillToWrapper();
        billTo.OrderNumber = 'SO-20181217-125040';
        giic_CreditCardWrapper card = new giic_CreditCardWrapper();
        card.SubscriptionID = '9909000232336316';
        
        mapParams.put('billTo', billTo);
        mapParams.put('totalAmount', 1.00);
        mapParams.put('card', card);
        mapParams.put('recordId', fulfillment.Id);
        
        Test.startTest();
        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new giic_Test_GenericRCCCServiceMock());
        Map<String, Object> authorizationRCCCRes =  giic_CyberSourceRCCCUtility.authorizationZeroRCCC(mapParams);
        System.debug('::response::'+authorizationRCCCRes);
        Test.stopTest();
        
    }
    }
     /*
    * Method name : testProcessCCOMS_Reverse 
    * Description : tests the ProcessCCOMS_Reverse API 
    * Return Type : void
    * Parameter :   
    */
    @isTest static void testProcessCCOMS_Reverse(){
         User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
         System.Runas(u)  
         {
        gii__SalesOrder__c salesObj = [SELECT Id, Name FROM gii__SalesOrder__c LIMIT 1];
        gii__WarehouseShippingOrderStaging__c fulfillment = [SELECT Id FROM gii__WarehouseShippingOrderStaging__c 
                                                               WHERE giic_SalesOrder__c =: salesObj.Id
                                                               LIMIT 1];
        Map<String, Object> mapParams = new Map<String, Object>();
        giic_BillToWrapper billTo = new giic_BillToWrapper();

        billTo.OrderNumber = 'SO-20181217-125040';
        billTo.FulfillmentNumber = '00032946';
        billTo.PaymentCurrency = 'USD';

        mapParams.put('billTo', billTo);
        mapParams.put('totalAmount', 5.0);
        mapParams.put('recordId', fulfillment.Id);
        
       Test.startTest();
        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new giic_Test_GenericRCCCServiceMock());
        Map<String, Object> reverseAuthorizationRCCCRes =  giic_CyberSourceRCCCUtility.reverseAuthorizationRCCC(mapParams);
        System.debug('::response::'+reverseAuthorizationRCCCRes);
        Test.stopTest();
        
    }
    }
    
    /*
    * Method name : testProcessCCOMS_Settlement 
    * Description : tests the ProcessCCOMS_Settlement API 
    * Return Type : void
    * Parameter :   
    */
    @isTest static void testProcessCCOMS_Settlement(){
         User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
         System.Runas(u)  
         {
        gii__SalesOrder__c salesObj = [SELECT Id, Name FROM gii__SalesOrder__c LIMIT 1];
        gii__WarehouseShippingOrderStaging__c fulfillment = [SELECT Id FROM gii__WarehouseShippingOrderStaging__c 
                                                               WHERE giic_SalesOrder__c =: salesObj.Id
                                                               LIMIT 1];
        Map<String, Object> mapParams = new Map<String, Object>();
        giic_BillToWrapper billTo = new giic_BillToWrapper();

        billTo.OrderNumber = 'SO-20181217-125040';
        billTo.FulfillmentNumber = '00032946';
        billTo.PaymentCurrency = 'USD';

        mapParams.put('billTo', billTo);
        mapParams.put('totalAmount', 5.0);
        mapParams.put('recordId', fulfillment.Id);
        
       Test.startTest();
        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new giic_Test_GenericRCCCServiceMock());
        Map<String, Object> rcSettlementResponse =  giic_CyberSourceRCCCUtility.settlementRequestRCCC(mapParams);
        System.debug('::response::'+rcSettlementResponse);
        Test.stopTest();
        
    }
    }
    
    /*
    * Method name : testProcessCCOMS_Refund 
    * Description : tests the ProcessCCOMS_Refund API 
    * Return Type : void
    * Parameter :   
    */
    @isTest static void testProcessCCOMS_Refund(){
         User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
         System.Runas(u)  
         {
        gii__SalesOrder__c salesObj = [SELECT Id, Name FROM gii__SalesOrder__c LIMIT 1];
        gii__WarehouseShippingOrderStaging__c fulfillment = [SELECT Id FROM gii__WarehouseShippingOrderStaging__c 
                                                               WHERE giic_SalesOrder__c =: salesObj.Id
                                                               LIMIT 1];
                                                               
        Map<String, Object> mapParams = new Map<String, Object>();
        giic_BillToWrapper billTo = new giic_BillToWrapper();

        billTo.OrderNumber = 'SO-20181217-125040';
        billTo.FulfillmentNumber = '00032946';
        billTo.PaymentCurrency = 'USD';

        mapParams.put('billTo', billTo);
        mapParams.put('totalAmount', 5.0);
        mapParams.put('recordId', fulfillment.Id);

       Test.startTest();
        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new giic_Test_GenericRCCCServiceMock());
        Map<String, Object> rcRefundResponse =  giic_CyberSourceRCCCUtility.refundRequestRCCC(mapParams);
        System.debug('::response::'+rcRefundResponse);
        Test.stopTest();
        
    }
    }
    
    /*
    * Method name : coverDataTypesOfStub 
    * Description : Covers the line of all the data types that are
    *               being used in the stub class/methods
    * Return Type : void
    * Parameter :   
    */
    @isTest static void coverDataTypesOfStub(){
         User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
         System.Runas(u)  
         {
        Test.startTest();
        //Instance of giic_schemasDatacontractOrg200407RcCommD class
        giic_schemasDatacontractOrg200407RcCommD.ArrayOfLineItem ArrayOfLineItem = new giic_schemasDatacontractOrg200407RcCommD.ArrayOfLineItem();
        giic_schemasDatacontractOrg200407RcCommD.ResponseCode ResponseCode = new giic_schemasDatacontractOrg200407RcCommD.ResponseCode();
        giic_schemasDatacontractOrg200407RcCommD.Processor Processor = new giic_schemasDatacontractOrg200407RcCommD.Processor();
        giic_schemasDatacontractOrg200407RcCommD.ArrayOfCreditCardType ArrayOfCreditCardType = new giic_schemasDatacontractOrg200407RcCommD.ArrayOfCreditCardType();
        giic_schemasDatacontractOrg200407RcCommD.CreditCardHistoryOMS CreditCardHistoryOMS = new giic_schemasDatacontractOrg200407RcCommD.CreditCardHistoryOMS();
        giic_schemasDatacontractOrg200407RcCommD.LineItem LineItem = new giic_schemasDatacontractOrg200407RcCommD.LineItem();
        giic_schemasDatacontractOrg200407RcCommD.OrderCC OrderCC = new giic_schemasDatacontractOrg200407RcCommD.OrderCC();
        giic_schemasDatacontractOrg200407RcCommD.CCLog CCLog = new giic_schemasDatacontractOrg200407RcCommD.CCLog();
        giic_schemasDatacontractOrg200407RcCommD.CreditCardType CreditCardType = new giic_schemasDatacontractOrg200407RcCommD.CreditCardType();
        giic_schemasDatacontractOrg200407RcCommD.CCStatus CCStatus = new giic_schemasDatacontractOrg200407RcCommD.CCStatus();
        giic_schemasDatacontractOrg200407RcCommD.OrderInquiry OrderInquiry = new giic_schemasDatacontractOrg200407RcCommD.OrderInquiry();
        giic_schemasDatacontractOrg200407RcCommD.ArrayOfResponseCode ArrayOfResponseCode = new giic_schemasDatacontractOrg200407RcCommD.ArrayOfResponseCode();
        giic_schemasDatacontractOrg200407RcCommD.ArrayOfCCLog ArrayOfCCLog  = new giic_schemasDatacontractOrg200407RcCommD.ArrayOfCCLog();
        
        //giic_schemasMicrosoftCom200310SerializD
        giic_schemasMicrosoftCom200310SerializD.ArrayOfstring ArrayOfstring = new giic_schemasMicrosoftCom200310SerializD.ArrayOfstring();
        Test.stopTest();
        
    }
    }
    
    
}