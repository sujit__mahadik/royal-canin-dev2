/************************************************************************************
Version : 1.0
Created Date : 18-Oct-2018
Function : Test class for giic_CreateAccountPrograms
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/
@isTest(SeeAllData = false)
private class giic_Test_CreateAccountPrograms {

    @testSetup
    static void setup(){
        //Create Admin User 
        giic_Test_DataCreationUtility.CreateAdminUser();        
        // Create consumer account
        List<Account> testConsumerAccountList = giic_Test_DataCreationUtility.insertConsumerAccount();
        // Create Carrier
        List<gii__Carrier__c> testCarrierList = giic_Test_DataCreationUtility.createCarrier();
        // create warehouse
        List<gii__Warehouse__c> testWareList = giic_Test_DataCreationUtility.insertWarehouse();
        SetupTestData.createCustomSettings();
        // create system policy
        giic_Test_DataCreationUtility.testSystemPolicyCreation();
        // create  Account refrence
        List<gii__AccountAdd__c> testAccRefList = giic_Test_DataCreationUtility.getAccountReference(testConsumerAccountList);
        Database.insert(testAccRefList);
        // create products and product reference
        List<gii__Product2Add__c> testProductList = giic_Test_DataCreationUtility.insertProduct();
        // create Sales Order
        List<gii__SalesOrder__c> testSOList = giic_Test_DataCreationUtility.insertSalesOrder(testConsumerAccountList[0].Id, testAccRefList[0].Id);
        // create Sales Order line
        List<gii__SalesOrderLine__c> testSOLList = giic_Test_DataCreationUtility.insertSOLine(testSOList[0].Id, testProductList[0].Id);
         // create promotions
        List<gii__Promotion__c> testPromotionList = giic_Test_DataCreationUtility.createPromotions();   
    }
    
    /*To test for both matching account and promotion*/
    static testMethod void  test_InsertAccountPrograms_Scenario1()
    {
        
        Map<String,List<giic_CreateAccountPrograms.AccountProgramWrapper>> mapresponse = new Map<String,List<giic_CreateAccountPrograms.AccountProgramWrapper>>();
        
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
       
        System.Runas(u)        
        {          
            List<Account> lstAccount = [select id, Bill_To_Customer_ID__c from Account limit 1];
                       
            List<gii__Promotion__c> lstPromotion = [select id,giic_PromotionCode__c,gii__EffectiveFromDate__c,gii__EffectiveToDate__c from gii__Promotion__c limit 1];      
           
            List<giic_CreateAccountPrograms.AccountProgramWrapper> lst = new List<giic_CreateAccountPrograms.AccountProgramWrapper>();
            giic_CreateAccountPrograms.AccountProgramWrapper reqst=new giic_CreateAccountPrograms.AccountProgramWrapper();
            reqst.CustomerNo = lstAccount[0].Bill_To_Customer_ID__c;            
            reqst.ELearnProgram = lstPromotion[0].giic_PromotionCode__c;
            reqst.ExpirationDate = '9/28/2018';
            reqst.DateQualified = '3/28/2018';
            lst.add(reqst);
            String JsonMsg=JSON.serialize(lst);            

            RestRequest req = new RestRequest();    
            RestResponse res = new RestResponse();    
            req.requestURI = '/services/apexrest/giic_CreateAccountPrograms';   
            req.httpMethod = 'POST';
            req.requestBody = Blob.valueof(JsonMsg);
            RestContext.request = req;        
            RestContext.response= res;
            Test.startTest();            
                mapresponse = giic_CreateAccountPrograms.InsertAccountPrograms();
                List<gii__AccountProgram__c> lstAP = new List<gii__AccountProgram__c>();
                lstAP = [select id from gii__AccountProgram__c];                
                System.assertEquals(lstAP.size(),1);                               
            Test.stopTest();
        }

    }   
    
    /*To test for only matching promotion*/
    static testMethod void  test_InsertAccountPrograms_Scenario2()
    {
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        
        System.Runas(u)
        {
            List<gii__Promotion__c> lstPromotion = [select id,giic_PromotionCode__c from gii__Promotion__c limit 1];     
            
            List<giic_CreateAccountPrograms.AccountProgramWrapper> lst = new List<giic_CreateAccountPrograms.AccountProgramWrapper>();
            giic_CreateAccountPrograms.AccountProgramWrapper reqst=new giic_CreateAccountPrograms.AccountProgramWrapper();
            reqst.CustomerNo = 'VET-0020973';
            reqst.ELearnProgram = lstPromotion[0].giic_PromotionCode__c;
            reqst.ExpirationDate = '9/28/2018';
            reqst.DateQualified = '3/28/2018';
            lst.add(reqst);
            String JsonMsg=JSON.serialize(lst);
            

            RestRequest req = new RestRequest();    
            RestResponse res = new RestResponse();    
            req.requestURI = '/services/apexrest/giic_CreateAccountPrograms';   
            req.httpMethod = 'POST';
            req.requestBody = Blob.valueof(JsonMsg);
            RestContext.request = req;        
            RestContext.response= res;
            Test.startTest();            
                giic_CreateAccountPrograms.InsertAccountPrograms();
                List<gii__AccountProgram__c> lstAP = new List<gii__AccountProgram__c>();
                lstAP = [select id from gii__AccountProgram__c ];
                System.assertnotEquals(lstAP.size(),1);   
            Test.stopTest();
        }

    }
    
    /*To test for only matching account*/
    static testMethod void  test_InsertAccountPrograms_Scenario3()
    {
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        
        System.Runas(u)
        {
            List<Account> lstAccount = [select id, Bill_To_Customer_ID__c from Account limit 1];
            
            List<giic_CreateAccountPrograms.AccountProgramWrapper> lst = new List<giic_CreateAccountPrograms.AccountProgramWrapper>();
            giic_CreateAccountPrograms.AccountProgramWrapper reqst=new giic_CreateAccountPrograms.AccountProgramWrapper();
            reqst.CustomerNo = lstAccount[0].Bill_To_Customer_ID__c;
            reqst.ELearnProgram = 'BFE50%';
            reqst.ExpirationDate = '9/28/2018';
            reqst.DateQualified = '3/28/2018';
            lst.add(reqst);
            String JsonMsg=JSON.serialize(lst);
            

            RestRequest req = new RestRequest();    
            RestResponse res = new RestResponse();    
            req.requestURI = '/services/apexrest/giic_CreateAccountPrograms';   
            req.httpMethod = 'POST';
            req.requestBody = Blob.valueof(JsonMsg);
            RestContext.request = req;        
            RestContext.response= res;
            Test.startTest();            
                giic_CreateAccountPrograms.InsertAccountPrograms();
                List<gii__AccountProgram__c> lstAP = new List<gii__AccountProgram__c>();
                lstAP = [select id from gii__AccountProgram__c ];
                System.assertEquals(lstAP.size(),1); 
            Test.stopTest();
        }

    }
    
    /*To test for non matching account and promotion*/
    static testMethod void  test_InsertAccountPrograms_Scenario4()
    {
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        
        System.Runas(u)
        {
            List<giic_CreateAccountPrograms.AccountProgramWrapper> lst = new List<giic_CreateAccountPrograms.AccountProgramWrapper>();
            giic_CreateAccountPrograms.AccountProgramWrapper reqst=new giic_CreateAccountPrograms.AccountProgramWrapper();
            reqst.CustomerNo = 'VET-0020973-001';
            reqst.ELearnProgram = 'Test50%';
            reqst.ExpirationDate = '9/28/2018';
            reqst.DateQualified = '3/28/2018';
            lst.add(reqst);
            String JsonMsg=JSON.serialize(lst);
            

            RestRequest req = new RestRequest();    
            RestResponse res = new RestResponse();    
            req.requestURI = '/services/apexrest/giic_CreateAccountPrograms';   
            req.httpMethod = 'POST';
            req.requestBody = Blob.valueof(JsonMsg);
            RestContext.request = req;        
            RestContext.response= res;
            Test.startTest();            
                giic_CreateAccountPrograms.InsertAccountPrograms();
                List<gii__AccountProgram__c> lstAP = new List<gii__AccountProgram__c>();
                lstAP = [select id from gii__AccountProgram__c ];
                System.assertnotEquals(lstAP.size(),1);   
                List<gii__Promotion__c> lstPromo = new List<gii__Promotion__c>();
                lstPromo = [select id from gii__Promotion__c where giic_PromotionCode__c = 'Test50%' ];
                System.assertEquals(lstPromo.size(),1);
            
            Test.stopTest();
        }
    }
    
    /*To test for negative scenarios*/
    static testMethod void  test_InsertAccountPrograms_Scenario5()
    {
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        
        System.Runas(u)
        {
            List<Account> lstAccount = [select id, Bill_To_Customer_ID__c from Account limit 1];
                       
            List<gii__Promotion__c> lstPromotion = [select id,giic_PromotionCode__c,gii__EffectiveFromDate__c,gii__EffectiveToDate__c from gii__Promotion__c limit 1];      
            
            List<giic_CreateAccountPrograms.AccountProgramWrapper> lst = new List<giic_CreateAccountPrograms.AccountProgramWrapper>();
            giic_CreateAccountPrograms.AccountProgramWrapper reqst=new giic_CreateAccountPrograms.AccountProgramWrapper();
            reqst.CustomerNo = lstAccount[0].Bill_To_Customer_ID__c;
            reqst.ELearnProgram = lstPromotion[0].giic_PromotionCode__c;
            reqst.ExpirationDate = String.valueof(lstPromotion[0].gii__EffectiveToDate__c);
            reqst.DateQualified = String.valueof(lstPromotion[0].gii__EffectiveFromDate__c);
            lst.add(reqst);
            String JsonMsg=JSON.serialize(lst);
            

            RestRequest req = new RestRequest();    
            RestResponse res = new RestResponse();    
            req.requestURI = '/services/apexrest/giic_CreateAccountPrograms';   
            req.httpMethod = 'POST';
            req.requestBody = Blob.valueof(JsonMsg);
            RestContext.request = req;        
            RestContext.response= res;
            Test.startTest();            
                giic_CreateAccountPrograms.InsertAccountPrograms();
                List<gii__AccountProgram__c> lstAP = new List<gii__AccountProgram__c>();
                lstAP = [select id from gii__AccountProgram__c ];                
            Test.stopTest();
        }
    }

}