/* Created by - Ranu 
* Purpose - to store constant variables 
*/

Public Class giic_Constants {
    public static final List<String> errorMessagesAvaTO = System.Label.giic_AvalaraTOErrorMsg.split(';');
    FINAL static public string STR_LOYALTY_BATCH = 'giic_LoyaltyPointsTrackerBatch%';
    FINAL static public string Batch_MSG_SCHEDULED = 'Batch Scheduled.';
    FINAL static public string Batch_MSG_ALREADY_SCHEDULED = 'Batch is already scheduled.';
    FINAL static public string SHIP_COUNTRY = 'US';
    FINAL static public string AVALARA_TNS_SO = 'SalesOrder';
    FINAL static public string AVALARA_TNS_INV = 'SalesInvoice';
    FINAL static public string CREATE_T = 'CreateTransaction';
    FINAL static public string COMMIT_T_FALSE = 'CommitTransactionFalse'; //does not save 
    FINAL static public string COMMIT_T_TRUE = 'CommitTransactionTrue';
    FINAL static public string USER_STORY_REQ_FIELD_MISSING = 'Required field Missing';
    FINAL static public string USER_STORY_TAX_CALC = 'Avalara Tax Calculation';
    FINAL static public string USER_STORY_INVOICECREATION = 'Invoice creation';
    FINAL static public string USER_STORY_SHIPMENTCREATION = 'Shipment creation';
    FINAL static public string USER_STORY_PAYMENTAUTHORIZATION = 'Payment Authorization';
    FINAL static public string PAYMENTAUTHORIZATION_ERROR_CODE = 'Error in Payment Authorization';
    FINAL static public string QUANTITY_CODE = 'Wrong ship quantity';
    FINAL static public string USER_STORY_PROMOTIONCALCULATION = 'Promotion Calculation';
    FINAL static public string TAX_CALC_ERROR_CODE = 'Error in avalara tax calculation';
    FINAL static public string CREDIT_CARD = 'Credit Card';
    FINAL static public string USER_STORY_UOM_CONVERSION = 'Unit of measure conversion';
    FINAL static public string UOM_CONVERSION_ERROR_CODE = 'Error in unit of measure conversion';
    FINAL static public string USER_STORY_TRANSFER_ORDER = 'TO Receipt Creation';
    FINAL static public string TRANSFER_ORDER_ERROR_CODE = 'Error in TO Reciept Creation';
    FINAL static public string TRANSFER_ORDER_ERRORS = Label.giic_TONotAvailable;//errorMessagesAvaTO.get(0);//'Transfer order does not exist or not valid for Reciept Creation';
    FINAL static public string TRANSFER_ORDER_ERROR_CODE_InvalidRequest = Label.giic_TOInvalid; //errorMessagesAvaTO.get(0);//'Invalid Request : Transfer Orders are not valid for Receipt Creation';
    FINAL static public string STR_SUCCESS = 'Success';
    FINAL static public string TRANSFER_ORDER_ERROR_CODE_NotFound = Label.giic_TONotFound; //errorMessagesAvaTO.get(1);//'Invalid Request : Transfer Order not found';
    FINAL static public string TRANSFER_ORDER_ERROR_CODE_NotAvailable = Label.giic_TOReceiptNotFound; //errorMessagesAvaTO.get(2);//'Invalid Request : reciepts are not available in Request';
    FINAL static public string USER_STORY_UPDATE_SOL = 'Update Sales Order Line';
    FINAL static public string UPDATE_SOL_ERROR_CODE = 'Error in Update Sales Order Line';
    FINAL static public string PROMOCODE = 'PROMOCODE';
    FINAL static public string SOIDS = 'SOIDS';
    FINAL static public string ACCOUNTIDS = 'ACCOUNTIDS';
    FINAL static public string SOLINEIDS = 'SOLINEIDS';
    FINAL static public string SOINFO = 'SOINFO';
    FINAL static public string SEARCH_STRING = 'SEARCH_STRING';
    FINAL static public String EXACT_SEARCH = 'EXACT_SEARCH';
    FINAL static public String PRODIDS = 'PRODIDS';
    FINAL static public String Reserved = 'Reserved';
    FINAL static public String ERROR_LOYALTY = 'Error in Loyalty Batch';
    FINAL static public string STR_DESERIALIZATION_FAILED = 'Deserialization Failed';
    FINAL static public string ERROR_CODE_400 = '400';
    FINAL static public string ERROR_CODE_207 = '207';
    FINAL static public string ERROR_CODE_417 = '417';
    FINAL static public string SUCCESS_CODE_200 = '200';
    FINAL static public string SUCCESS_MSG = 'Success';
    FINAL static public string PARTIAL_SUCCESS = 'Partial Success';
    FINAL static public string ERROR_MSG_FAILURE = 'Failure';
    FINAL static public string INCORRECT_WSOS_Id = ' Incorrect value for WSOS_Id =  ';
    FINAL static public string INCORRECT_WSOS_SOId = 'Incorrect value for WSOS_SOId = ';
    FINAL static public string INCORRECT_WSOS_SOLId = ' Incorrect value for WSOLS_SOLId =  ';
    FINAL static public string UPDATE_TAX_ERROR1 = 'Sales Order payment gii__SalesOrderPayment__c not found';
    FINAL static public string UPDATE_TAX_INVALID_WSOS = 'Invalid WSOS' ;
    FINAL static public string EXCEPTION_OCCURED = 'Exception Occured' ;
    FINAL static public string UPDATE_TAX_PM_NOT_AVAIL = 'Payment Method of SOP is not available in Payment Method object Records' ;
    FINAL static public string RESERVE_AND_SHIP = 'ReserveandShip' ;
    FINAL static public string TRANSFER_ORDER = 'Transfer Order';
    FINAL static public string TRANSFER_ORDER_RESERVATION_FAILED = 'Transfer Order Reservation failed';
    FINAL static public string JSON_REQUEST = 'JSON REQUEST = ';
    FINAL static public string DUE_TO = 'DUE TO ';
    FINAL static public string INCORRECT_INVENTORY_COUNT = 'Incorrect Inventory Count';
    FINAL static public string PRODUCT2 = 'Product2';
    FINAL static public string WAREHOUSE_LOC_NOT_AVALAIBLE  = 'Warehouse Location is not available';
    FINAL static public string NO_RECIEPT = 'No Reciept Queue Found to convert in Reciepts';
    FINAL static public string OBJ_SALES_ORDER_STAGING = 'gii__SalesOrderStaging__c';
    FINAL static public string BATCH_APEX = 'BatchApex';
    FINAL static public string COMPLETED = 'Completed';
    FINAL static public string USER_STORY_LIBRIX = 'Librix';
    FINAL static public string LIBRIX_ERROR_CODE = 'New Promotion Inserted';
    FINAL static public string LIBRIX_ERROR_MESSAGE = 'Matching promotion not found with librix data, inserted new promotion.';
    FINAL static public string LIBRIX_NonMatchingValueField = 'none'; 
    FINAL static public string LIBRIX_NonMatchingField = 'CustomerNo';
    FINAL static public string SUBMITTED_WITH_ERROR = 'Submitted with Error';
    FINAL static public string SALES_ORDER_PAYMENT_SETTLEMENT_CREATION = 'Sales Order Payment Settlement Creation';
    FINAL static public string SOP_MISSING = 'Sales Order Payment Missing';
    FINAL static public string USERSTORY_SORELEASE = 'Sales Order Release';
    FINAL static public String LIST_OF_ERROR = 'LIST_OF_ERROR';
    
    FINAL static public String LINE_NUMBER = 'line number:::: ';
    FINAL static public string NO_LINE_AVAILABLE = 'No Line Available';
    FINAL static public string NO_LINE_ERROR = 'No Line Error';
    
    /**************Created By Pradeep************************/
    //FINAL static public set<string> SETOPENSOSTATUS= new set<string>{'Draft','Open','Cancelled'};
    FINAL static public String ACHPROMOTYPE = 'ACH';
    FINAL static public String DROPSHIPPROMO = 'DropShip';
    FINAL static public String DROPSHIPADDIONTALCHARGE = 'DropShipCharges';
    FINAL static public String SHIPPINGPROMO = 'Shipping';
    
    FINAL static public decimal SHIPPINGCHARGE = 4.99;
    FINAL static public string SOSTATUSPROCESSWITHERROR ='Processed with Error';
    FINAL static public string SOSTATUSINPROGRESS ='In Progress';
    FINAL static public string SOSTATUSOPEN ='Open';
    FINAL static public string CARRIERROUTE ='Route';
    FINAL static public string SOSTATUSCANCELLED ='Cancelled';
    FINAL static public string PROMOTYPESHIPPING ='Shipping';
    FINAL static public string USERROLECSR ='CSR';
    FINAL static public string USERROLESALESREP ='Sales Rep';
    FINAL static public string USERROLEALL ='All';
    FINAL static public string PROMOTION='Promotion';
    
    FINAL static public string PROMOFREQTYPE_MONTHLY='Monthly';
    FINAL static public string PROMOFREQTYPE_QUARTERLY='Quarterly';
    FINAL static public string PROMOFREQTYPE_YEARLY='Yearly';
    
    
    FINAL static public string AVARALA_SUCCESS = Label.giic_TaxCalSuccess; //errorMessagesAvaTO.get(4);//'Tax calculation is completed.';
    FINAL static public string AVARALA_ERROR = Label.giic_TaxCalError; //errorMessagesAvaTO.get(5);//'Error in tax calculation, please check the error logs.';
    FINAL static public string AVARALA_ERROR_EXEMPTION = Label.giic_TaxCalAccTaxEmp;//errorMessagesAvaTO.get(6);//'You cannot calculate tax as the Account is already tax exempted.'; 
    FINAL static public string AVARALA_ERROR_TAX_CALCUATED = Label.giic_TaxCalDone; //errorMessagesAvaTO.get(7);//'You cannot calculate tax as tax is already calculated for this sales order.';
    
    
    //Sales order line tatus 
    FINAL static public string SOLI_CANCEL = 'Cancelled';
    FINAL static public string SALESORDER = 'SalesOrder';
    FINAL static public string BACKORDER = 'BackOrder';
    FINAL static public string SOL_INITIALSTATUS = 'Initial';
    FINAL static public string SOL_BACKORDERSTATUS = 'Back Order';
    FINAL static public string SOL_OPENSTATUS = 'Open';
    FINAL static public string SOL_DRAFTSTATUS = 'Draft';
    
    //Sales order  status 
    FINAL static public string SO_PICKED = System.label.giic_SOStatusAfterAllocation ;
    FINAL static public string SO_SHIPPED = 'Shipped';
    FINAL static public string SO_SETTLED = 'Settled';
    FINAL static public string SO_INVOICED = 'Invoiced';
    FINAL static public string SO_ORIGIN = 'Nav';
    
    //Sales order Payment methods 
    FINAL static public string SO_CREDITCARD = 'Credit Card';
    FINAL static public string SO_SENT_TO_WMS = 'Sent to WMS';
    
    //SO Error Messages
    //public static final List<String> SOErrorMsgList= System.Label.giic_SOErrorMessages.split(';');
    FINAL static public string SO_Released = System.Label.giic_SOErrorReleased; //SOErrorMsgList.get(0);//'You cannot add product as the order is already released.';
    FINAL static public string SO_Cancelled = System.Label.giic_SOErrorCancelled; //SOErrorMsgList.get(1);//'You cannot add product as the order is cancelled.';
    //FINAL static public string SO_Tax = 'You cannot calculate tax as the order is already released.';
    FINAL static public string SO_Tax_Released = System.Label.giic_SOErrorTaxCal;// SOErrorMsgList.get(2);//'You cannot calculate tax as the order is already released.';
    FINAL static public string SO_Tax_Cancelled = System.Label.giic_SOCancelErrorTaxCal;//SOErrorMsgList.get(3);//'You cannot calculate tax as the order is cancelled.';
    FINAL static public string SOCANCEL = System.Label.giic_SOCancelErrorCancelled;//SOErrorMsgList.get(4);//'You cannot cancel order as order is already cancelled.';
    FINAL static public string SO_CANCEL_RELEASE =  System.Label.giic_SOCancelErrorReleased;//SOErrorMsgList.get(5);//'You cannot cancel order as order is already released.';
    FINAL static public string SO_Acc_Blocked = System.Label.giic_SOErrorAccBlocked;//SOErrorMsgList.get(6);//'You cannot add product as the account is blocked.';
    //Generic error messaging
    public static final String SO_GENRIC_PRE = System.Label.giic_SOErrorGenPre;//SOErrorMsgList.get(7);//'You cannot';
    public static final String SO_GENERIC_POST_RELEASE = System.Label.giic_SOErrorGenPostRel;//SOErrorMsgList.get(8);//' as the order is already released';
    public static final String SO_GENERIC_POST_CANCEL = System.Label.giic_SOErrorGenPostCancel;//SOErrorMsgList.get(9);//' as the order is already cancelled';
    public static final String SO_ADDRESS_UPDATE = System.Label.giic_SOErrorAddUpdate;//SOErrorMsgList.get(10);//' modify address';
    public static final String SO_PAYMENT_UPDATE = System.Label.giic_SOErrorPayUpdate;//SOErrorMsgList.get(11);//' modify payment details';
    
    //Account Error Messages
    FINAL static public string Acc_Blocked = errorMessagesAvaTO.get(8);//'You cannnot create Sales Order as Account is blocked.';
    
    //Add Product Error Messages
    FINAL static public string Blank_SearchString = errorMessagesAvaTO.get(9);//'Please enter a Product SKU to add product.';
    FINAL static public string Error_SavingSOCreation = 'Error in Sales Order creation.';
    FINAL static public string Error_SavingSOUpdation = 'Error in Sales Order updation.';
    
    static public gii__GloviaSystemSetting__c SYS_SETTING ;
    
    FINAL Static public String LINESTATUS_SHIPPEDBYDC = 'Shipped by DC';
    FINAL Static public String LINESTATUS_REJECTEDBYDC = 'Rejected by DC';
    FINAL Static public String ROUTEORDER = 'route';
    FINAL Static public String HYPERLINK = 'HYPERLINK';
    FINAL Static public String CANCELSO = 'CANCELSO';
    
    //Cybersource and RC-CC constants
    public static final List<String> CSRCValidationMsg = System.Label.giic_CyberSourceRCCCValMsg.split(';');
    public static final String CYBERSOURCE_USER_STORY = 'Cybersource Services';
    public static final String RCCC_USER_STORY = 'RC-CC Services';
    public static final String PAYMENT_AUTH_FAIL = Label.giic_PaymentAuthFail;//CSRCValidationMsg.get(0);//'Cybersource payment authorization encountered error.';
    public static final String DROP_PAYMENT_AUTH_FAIL = Label.giic_AuthRevFail;//CSRCValidationMsg.get(1);//'Cybersource payment authorization reversal encountered error.';
    public static final String DML_EX_CODE = 'DML_EXCEPTION';
    //public static final String CYBERSOURCE_AUTH_ERROR =CSRCValidationMsg.get(2);//'Please check authentication details.';
    public static final String INVOICE_TAX_ERROR = CSRCValidationMsg.get(3);//'Tax is not calculated.';
    public static final String INVOICE_SHIPDATE_ERROR = CSRCValidationMsg.get(4);//'Ship date is empty.';
    public static final String INVOICE_ORDERNUMBER_ERROR = CSRCValidationMsg.get(5);//'Sales order not found.';
    public static final String INVOICE_CONVERTED_ERROR = CSRCValidationMsg.get(6);//'Record is already processed.';
    public static final String INVOICE_NONCC_ERROR = CSRCValidationMsg.get(7);//'You cannot perform settlement, since payment method is not credit card.';
    public static final String INVOICE_SETTLE_ERROR = CSRCValidationMsg.get(8);//'You cannot make settlement as invoice is already settled.';
    public static final String INVOICE_SETTLE_STATUS = 'Confirmed';
    public static final String TAX_COMMITED_ERROR = CSRCValidationMsg.get(9);//'Tax is already committed on this record.';
    public static final String TAX_CALCULATIONFAIL_ERROR = CSRCValidationMsg.get(10);//'Tax calculation failed.';
    public static final String PAYMENT_SETTLE_FAIL = 'Payment Settlement Error';
    public static final String PAYMENT_CAPTURE_FAIL = 'Payment Capture Error';
    public static final String CARD_AUTH_ERROR = Label.giic_CardNotAuthorized;//CSRCValidationMsg.get(11);//'Card is not authorized.';
    public static final String CARD_NOTAUTH_ERROR = Label.giic_AuthCC;//CSRCValidationMsg.get(12);//'Please authorize credit card.';
    public static final String REASON_CODE = 'Reason Code: ';
    public static final String ERROR_CODE = 'Http Error Code: ';
    public static final String PAYMENT_NOT_CC = Label.giic_PaymentMethodNotCC;//CSRCValidationMsg.get(13);//'Payment method selected is not credit card.';
    public static final String INVOICE_SETTLED_MSG = Label.giic_InvoiceSettled;//CSRCValidationMsg.get(14);//'Invoice is already settled.';
    FINAL static public String ERROR_STATUS = 'Error';
    //RC-CC Constants
    public static final String WriteHistory = 'WriteHistory';
    public static final String GetCardHistoryByBillToCustomer = 'GetCardHistoryByBillToCustomer';
    public static final String UpdateCreditCardHistory = 'UpdateCreditCardHistory';
    public static final String DeleteCreditCardHistory = 'DeleteCreditCardHistory';
    public static final String SetDefaultCreditCardHistory = 'SetDefaultCreditCardHistory';
    public static final String AuthorizationOMS = 'AuthorizationOMS';
    public static final String ProcessCCOMS_Settlement = 'ProcessCCOMS_Settlement';
    public static final String ProcessCCOMS_Refund = 'ProcessCCOMS_Refund';
    public static final String ProcessCCOMS_Authorization = 'ProcessCCOMS_Authorization';
    public static final String ProcessCCOMS_ReverseAuth = 'ProcessCCOMS_ReverseAuth';
    public static final String WriteHistory_NO_RES = Label.giic_WriteHistoryNoRes;//CSRCValidationMsg.get(15);//'WriteHistory returned no response';
    public static final String UpdateCreditCardHistory_NO_RES = Label.giic_UpdateCCHistory;//CSRCValidationMsg.get(16);//'UpdateCreditCardHistory returned no response';
    public static final String DeleteCreditCardHistory_NO_RES = Label.giic_DeleteCCHistory;//CSRCValidationMsg.get(17);//'DeleteCreditCardHistory returned no response';
    public static final String DEFAULT_CURRENCY = 'USD';
    public static final String AMOUNT_NON_ZERO = 'ZERO AMOUNT';
    public static final String SETTLEMENT_NOT_FOUND = 'SETTLEMENT NOT FOUND';
    //Cancel SO-Line constant
    public static final String SELECT_REC = Label.giic_SelRecToCancel;//errorMessagesAvaTO.get(12);//'Please select at least one record to cancel';
    
    
    FINAL static public String SUCCESS = 'SUCCESS';
    FINAL static public String ERROR = 'ERROR';
    FINAL static public String ERRORLIST = 'ERRORLIST';
    FINAL static public String PARTIAL_RESERVATION = 'Partial Reservation';
    
    //Loyalty Management 
    
    public static final String PAYMENT_REWARD_POINTS = 'Reward Points';
    public static final String PAYMENT_ADDITIONAL_CHARGE = 'Additional Charge';
    
    /******************************* Fullfillment Variables *********************/
    public static final String CALCULATE_TAX = 'Calculate Tax';
    public static final String AUTHORIZE_AMOUNT = 'Authorized Amount';
    public static final String DROP_AUTHORIZATION = 'Drop Authorization';
    public static final String SENT_TO_WMS = 'Sent to WMS';
    public static final String REJECTED_BY_DC = 'Rejected by DC';
    public static final String ACCEPTED_BY_DC = 'Accepted by DC';
    public static final String INITIAL = 'Initial';
    public static final String FF_STATUS_IN_PROGRESS = 'In Progress';
    public static final String FULLFILLMWNT_SUBMITTEDWITHERROR = 'Submitted with Error';
    public static final String ACCEPTED = 'Accepted';
    public static final String REJECTED = 'Rejected';
    public static final String SHIPPED = 'Shipped';
    public static final String SUBMITTED = 'Submitted';
    public static final String DC_CANCELLATION = 'DC – Cancellation';
    public static final String TAX_CALCULATION_FROM_WSOS = 'Tax Calculation From WSOS';
    public static final String TAX_CALCULATION_ERROR1 = 'Please check Warehouse / WarehouseCode / Fullfillment lines should be available.';
    public static final String TAX_CALCULATION_STATUS_ERROR = ' Tax can be calculated if Status is In Progress or Submit with error';
    public static boolean triggerStop = false;
    
    //Case Status
    public static final String DRAFT_CASE = 'Draft';
    public static final String PENDING_CASE = 'Pending';
    public static final String NONPENDING_CASE = 'NonPending';
    public static final String APPROVED_STATUS = 'Approved';
    public static final String REJECTED_STATUS = 'Rejected';
    public static final String ACCEPTED_STATUS = 'Accepted';
    public static final String CONFIRMED_STATUS = 'Confirmed';
    public static final String ARCREDIT_USERSTORY = 'Credit Return';
    
    
    /***************************Settlement ******************************************/
    //public static final String INTEGRATIONSTATUS_DROPAUTHORIZATION = 'Drop Authorization';
    //public static final String INTEGRATIONSTATUS_AUTHORIZEDAMOUNT = 'Authorized Amount';
    //public static final String INTEGRATIONSTATUS_CALCULATETAX = 'Calculate Tax';
    //public static final String ErrorFoundInSettlement = 'Error Found In Settlement';
    public static final String ORDER_TYPE_OLP = 'OLP';
    
    public static final String ERROR_INVALIDRECORD = Label.giic_InvalidRec; //errorMessagesAvaTO.get(10);//'Invalid Record - Required field missing or Wrong data';
    
    /************************Inventory Adjustment ******************************************/
    public static final String Warehouse_Not_found = 'Warehouse Not found'; 
    public static final String Product_Not_found = 'Product Not found'; 
    public static final String UOM_Not_found = 'Unit of measure Not found'; 
    public static final String UOM_Not_configured = Label.giic_UOMNotConfigured; //errorMessagesAvaTO.get(11);//'Unit of measure not configured. Check Product Unit of Measure Conversion of Product';
    public static final String INVENTORY_ADJUSTMENT = 'Inventory Adjustment';
    
    /************************INVOICE******************************************/   
    public static final String INPROGRESS = 'In Progress';
    public static final String PROCESSWITHERROR = 'Process with Error';
    public static final String PROCESSED = 'Processed';
    public static final String PROCESSEDWITHERROR = 'Processed With Error';
    public static final String CONFIRMED = 'Confirmed';
    
    /*FedEX Address Validation */
    public static final String FEDEX_STORY = 'FEDEX_ADDRESS_VAL';
    public static final String FEDEX_EXCEPTION = 'EXCEPTION';
    public static final String FEDEX_ADD_NOT_RES = 'ADDRESS_NOT_RESOLVED';
    public static final String FEDEX_NO_RES = 'NULL RESPONSE';
    public static final String FEDEX_STATE_ERROR = 'STATE ERROR';
}