/************************************************************************************
Version : 1.0
Name : giic_SOSAddressValidationBatch
Created Date : 03 Sep 2018
Function : Address validation for Sales order staging
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/

global class giic_SOSAddressValidationBatch implements Database.Batchable<sObject>, Database.AllowsCallouts{
    global Database.QueryLocator start(Database.BatchableContext BC){        
        //Get Records for Address validation
        String soql = 'Select Id, Name, giic_Account__c, giic_Status__c,giic_ShipToStreet__c, giic_ShipToCity__c, giic_ShipToCountry__c, giic_ShipToZipPostalCode__c, giic_DropShip__c From gii__SalesOrderStaging__c Where giic_AddressValidated__c !=  true and giic_Status__c !=  \''+ System.Label.giic_Converted + '\''; // and id in :ids          
        soql+= ' and giic_DropShip__c = true';        
        return Database.getQueryLocator(soql); 
    }

    global void execute(Database.BatchableContext BC, List<gii__SalesOrderStaging__c> lstSOStaging){       
        //Validate Address
        // commented by sujit as  Wednesday, December 12, 2018 11:32 PM 13.Remove the code of fedex validation for batch job. all address will be validated by MW coming on staging object
        // giic_IntegrationCommonUtil.validateAddress(lstSOStaging);         
    }

    global void finish(Database.BatchableContext BC){
        //call Sales order Staging to sales order conversion batch
        string jobName = 'giic_SOStagingToSOConversionBatch';
        string Status = 'Processing';        
        String query = 'SELECT Id,Status FROM AsyncApexJob WHERE ApexClass.Name Like \'%' + jobName + '%\'';
        query+= 'and Status =  \''+ Status + '\'' ;        
        List<AsyncApexJob > jobList = Database.query(query);
        //System.debug('jobList ***'+jobList);
        if(jobList.size()==0)
        {
            String sObjectApiName = 'gii__SalesOrderStaging__c';
            Map<String, giic_IntegrationCustomMetaDataWrapper> mapIntegrationSettings = giic_IntegrationCustomMetaDataUtil.getIntegrationSettings(sObjectApiName);
            giic_SOStagingToSOConversionBatch SOStagingToSO = new giic_SOStagingToSOConversionBatch(mapIntegrationSettings, sObjectApiName);
            Database.executeBatch(SOStagingToSO);
        }
    }
}