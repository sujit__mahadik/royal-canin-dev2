/************************************************************************************
Version : 1.0
Created Date : 23 Oct 2018
Function : Helper for class giic_ProductReferenceTriggerHelper
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/

public class giic_DataMappingUtils {
    // below method is used to process parent(source) Object and map its fields for target Object
    public static Map<String, SObject> processSObjectField (List<SObject> lstSourceObject, String sObjectApiName, Map<String, SObject> mapParentIdChildObject ){
        map<String, SObject> mapSourceObjIdTargetObj = new map<String, SObject>();
        List<giic_IntegrationSetting__mdt> lstIS = [Select giic_NoOfRecords__c, giic_UserStory__c, (Select giic_IsSourceField__c, giic_IsExternal__c, giic_ExternalSObject__r.QualifiedApiName, giic_ExternalField__r.QualifiedApiName, giic_IntegrationSetting__c, giic_SourceObject__c, giic_SourceObject__r.QualifiedApiName, giic_SourceField__r.QualifiedApiName, giic_TargetObject__c, giic_TargetObject__r.QualifiedApiName, giic_TargetField__r.QualifiedApiName, giic_IsParentField__c, giic_IsConvertedField__c, giic_IsErrorMapping__c,   giic_UseDefault__c From IntegrationMappings__r where giic_DeActivated__c = false) From giic_IntegrationSetting__mdt where giic_Object__r.QualifiedApiName =: sObjectApiName and giic_IsActive__c = true];
        string sSourceObjId = '';
        for(SObject sourceObj :lstSourceObject){
            sSourceObjId = string.valueOf(sourceObj.get('Id'));
            SObject targetObj = mapParentIdChildObject.get(sSourceObjId);
            for( giic_IntegrationSetting__mdt objIS : lstIS ){
                if(objIS.IntegrationMappings__r != null && objIS.IntegrationMappings__r.size() > 0){
                    for(giic_IntegrationMapping__mdt objIDtl : objIS.IntegrationMappings__r ){
                        
                        if(objIDtl.giic_IsExternal__c){
                            Schema.sObjectType externalObjType = Schema.getGlobalDescribe().get(objIDtl.giic_ExternalSObject__r.QualifiedApiName); 
                            SObject externalObj = externalObjType.newSObject();   
                            externalObj.put(objIDtl.giic_ExternalField__r.QualifiedApiName, sourceObj.get(objIDtl.giic_SourceField__r.QualifiedApiName));
                            targetObj.putSobject(objIDtl.giic_TargetField__r.QualifiedApiName.replace('__c','__r'), externalObj);                    
                        }
                        else {
                                targetObj.put(objIDtl.giic_TargetField__r.QualifiedApiName, sourceObj.get(objIDtl.giic_SourceField__r.QualifiedApiName));
                        }
                    }
                }
            }
            mapSourceObjIdTargetObj.put(sSourceObjId, targetObj);
        }
        return mapSourceObjIdTargetObj;
    }
    
}