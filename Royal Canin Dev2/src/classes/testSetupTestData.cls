@IsTest
private class testSetupTestData {
  
  @IsTest static void testCustomSettings() {
    Test.startTest();
    system.assertEquals([SELECT id from Global_Parameters__c].size(), 0, 'Global_Parameters__c is not empty');
    system.assertEquals([SELECT id from reg_steps__c].size(), 0, 'reg_steps__c is not empty');
    system.assertEquals([SELECT id from Registration_Record_Type_Lookup__c].size(), 0, 'Registration_Record_Type_Lookup__c is not empty');
    system.assertEquals([SELECT id from Registration_Record_Type_Mapping__c].size(), 0, 'Registration_Record_Type_Mapping__c is not empty');
    
    SetupTestData.createCustomSettings();
    
    system.assert([SELECT id from Global_Parameters__c].size()> 0, 'Global_Parameters__c is empty');
    system.assert([SELECT id from reg_steps__c].size()> 0, 'reg_steps__c is empty');
    system.assert([SELECT id from Registration_Record_Type_Lookup__c].size()>0, 'Registration_Record_Type_Lookup__c is empty');
    system.assert([SELECT id from Registration_Record_Type_Mapping__c].size()>0, 'Registration_Record_Type_Mapping__c is empty');
    
    SetupTestData.removeCustomSettings();
    
    system.assertEquals([SELECT id from Global_Parameters__c].size(), 0, 'Global_Parameters__c is not empty');
    system.assertEquals([SELECT id from reg_steps__c].size(), 0, 'reg_steps__c is not empty');
    system.assertEquals([SELECT id from Registration_Record_Type_Lookup__c].size(), 0, 'Registration_Record_Type_Lookup__c is not empty');
    system.assertEquals([SELECT id from Registration_Record_Type_Mapping__c].size(), 0, 'Registration_Record_Type_Mapping__c is not empty');


    system.assertEquals([SELECT id from channel_related_recordtypes__c].size(), 0, 'channel_related_recordtypes__c is not empty');
    SetupTestData.createChannelRelatedCustomSettings();
    system.assert([SELECT id from channel_related_recordtypes__c].size()> 0, 'channel_related_recordtypes__c is empty');
    
    system.assertEquals([SELECT id from Metric_Types_for_Sales_Summary__c].size(), 0, 'Metric_Types_for_Sales_Summary__c is not empty');
    SetupTestData.createMetricTypesforSalesSummaryCustomSettings();
    system.assert([SELECT id from Metric_Types_for_Sales_Summary__c].size()> 0, 'Metric_Types_for_Sales_Summary__c is empty');
    
    
    Test.stopTest();
  }
  
   @IsTest static void testCustomerRegistrations() {
    Test.startTest();
        Customer_Registration__c cr1 = SetupTestData.createCustomerRegistration();
        system.assertEquals(cr1.Email_Address__c, 'test@test.com', 'Email_Address__c is not correct');
        
        Customer_Registration__c cr3 = SetupTestData.createBanefieldCustomerRegistration();
        system.assertEquals(cr3.Email_Address__c, 'test@test.com', 'Email_Address__c is not correct');
        system.assertEquals(cr3.Dept_Store_Number__c, '1234', 'Dept_Store_Number__c is not correct');
        system.assertEquals(cr3.Customer_Type__c, 'Banfield Associate', 'Customer_Type__c is not correct');
        
        Customer_Registration__c cr2 = SetupTestData.createVetCustomerRegistration();
        system.assertEquals(cr2.Email_Address__c, 'jtester1@gmail.com', 'Email_Address__c is not correct');
        system.assertEquals(cr2.Name_of_Business_Organization__c, 'test reg', 'Name_of_Business_Organization__c is not correct');
        system.assertEquals(cr2.First_Name__c, 'Joe', 'First_Name__c is not correct');
        
    Test.stopTest();
   }
   
   @IsTest static void testSalesSummaryData() {
    Test.startTest();
        SetupTestData.createCustomSettings();
        system.assertEquals([SELECT id from Sales_Summary__c].size(), 0, 'Sales_Summary__c is not empty');
        SetupTestData.createSalesSummaryData(5);
        system.assert([SELECT id from Sales_Summary__c].size()>0, 'Sales_Summary__c is empty');
    Test.stopTest();
   }
}