/************************************************************************************
Version : 1.0
Created Date : 10 Dec 2018
Function : All Data related method for promotion
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/
public class giic_PromotionDataService {
    /*
    * Method name : getAccountDetails
    * Description : return account details based on ship to customer number
    * Return Type :  list<account>
    * Parameter : Set of ship to customer number
    */
    public static list<gii__AccountAdd__c> getAccountDetails(set<string> setCustomerNumber){
        return [select id,gii__PriceBookName__c,gii__Account__c,gii__Account__r.ShipToCustomerNo__c,gii__Account__r.ShippingCountry,gii__Carrier__c,gii__Carrier__r.giic_UniqueCarrier__c, gii__Account__r.Insite_Customer_Type__c from gii__AccountAdd__c where gii__Account__r.ShipToCustomerNo__c = : setCustomerNumber ];
    }
    /*
    * Method name : getAccountDetails
    * Description : return account details based on ship to customer number
    * Return Type :  list<account>
    * Parameter : Set of ship to customer number
    */
    public static list<gii__AccountProgram__c> getAccountPrograms(set<string> setAccountshipToId,set<string> setAccountId,set<string> setPromotionId){
        return [select id,giic_TotalPromoRemain__c,giic_QualifiedDate__c,giic_TotalPromoUsed__c,gii__AccountReference__r.gii__Account__c,giic_PromoEndDate__c,giic_Promotion__c from gii__AccountProgram__c where (gii__AccountReference__r.gii__Account__c in :setAccountId or gii__AccountReference__r.gii__Account__r.ShipToCustomerNo__c in :setAccountshipToId ) and giic_Promotion__c in :setPromotionId];
    }
     /*
    * Method name : getSalesOrderInfo
    * Description : return sales order info 
    * Return Type :  map<id,gii__salesorder__C>
    * Parameter : set of sales order id
    */
    public static  map<id,gii__SalesOrder__c> getSalesOrderInfoForPromoResponse(set<string> setSOIds){
        return new map<Id,gii__SalesOrder__c>([select id,gii__DropShip__c,gii__AccountReference__c,gii__Promotion__c,gii__Account__c,giic_SOStatus__c,giic_CancelReson__c,gii__Released__c,gii__account__r.Insite_Customer_Type__c,
        gii__NetAmount__c,gii__TotalAmount__c,gii__TaxAmount__c,gii__AdditionalChargeAmount__c,gii__ProductDiscountAmount__c,gii__TotalOrderAmount__c,gii__ProductAmount__c,
        gii__OrderDiscountAmount__c,gii__DiscountPercent__c,giic_HPROMO1__c,giic_HPROMO2__c,giic_FREESHIP__c,giic_HPRAMT1__c,giic_HPRAMT2__c,
        giic_HPRVAL1__c,giic_HPRVAL2__c,giic_HPROMO1__r.giic_IsAutoPromo__c,giic_HPROMO2__r.giic_IsAutoPromo__c,giic_HPROMO2__r.giic_PromotionCode__c,giic_HPROMO1__r.giic_PromotionCode__c,
        giic_FREESHIP__r.giic_PromotionCode__c,gii__Promotion__r.giic_PromotionCode__c,gii__Promotion__r.giic_IsAutoPromo__c,giic_FREESHIP__r.giic_IsAutoPromo__c,
        gii__Promotion__r.giic_IsHeaderPromo__c,giic_HPROMO1__r.giic_IsHeaderPromo__c,giic_HPROMO2__r.giic_IsHeaderPromo__c,(select id,gii__AdditionalCharge__r.name,gii__CalculatedShippingAmount__c,gii__ShippingAmountOff__c,Name from gii__SOAddlCharge__r) from gii__SalesOrder__c where id in:setSOIds]);
    }
    /*
    * Method name : getSOAdditionalCharge
    * Description : get all sales order addtional charge for the sales order and promotion 
    * Return Type :  list<gii__SalesOrderAdditionalCharge__c>
    * Parameter : setSOIds => sales order ids, setPromoIds => promotion ids //optional
    */
    public static list<gii__SalesOrderAdditionalCharge__c> getSOAdditionalCharge(set<string> setSOIds, set<string> setPromoIds){
        if(setSOIds != null && setSOIds.size() > 0) {
            string queryString='select id,gii__UnitPrice__c,gii__Promotion__c,gii__NoChargeReason__c,gii__CalculatedShippingAmount__c,gii__SalesOrder__c from gii__SalesOrderAdditionalCharge__c '+
                                ' where ' + ( setPromoIds.size() > 0 ? ' gii__Promotion__c in :setPromoIds and' : '' ) + '  gii__SalesOrder__c in :setSOIds' ;
            system.debug('***'+database.query(queryString));
            return database.query(queryString);
            
        }
        else{
            return new list<gii__SalesOrderAdditionalCharge__c>();
        }
    }
     /*
    * Method name : getSalesOrderInfo
    * Description : return sales order info 
    * Return Type :  map<id,gii__salesorder__C>
    * Parameter : set of sales order id
    */
    public static  map<id,gii__SalesOrder__c> getSalesOrderInfoForPromotion(set<string> setSOIds){
        return new map<Id,gii__SalesOrder__c>([select id,gii__Promotion__c,giic_HPRVAL1__c,gii__AccountReference__c,gii__ProductAmount__c,gii__Account__c,gii__DiscountPercent__c,giic_HPRVAL2__c,giic_HPROMO1__c,giic_HPROMO2__c,giic_FREESHIP__c,
                                                giic_FREESHIP__r.giic_PromotionCode__c,gii__Promotion__r.giic_PromotionCode__c,giic_HPROMO1__r.giic_PromotionCode__c,giic_HPROMO2__r.giic_PromotionCode__c,
                                            (select id,gii__SalesOrder__c,gii__ProductAmount__c,gii__DiscountAmount__c,giic_HPRAMT1__c,giic_HPRAMT2__c from gii__SalesOrder__r),  
                                            (select id,gii__CalculatedShippingAmount__c,gii__UnitPrice__c,gii__AdditionalCharge__c,gii__AdditionalCharge__r.giic_AdditionalChargeCode__c from gii__SOAddlCharge__r)  from gii__SalesOrder__c where id in:setSOIds]);
    }
    /*
    * Method name : getProductDetails
    * Description : return product details based on product code
    * Return Type :  list<gii__Product2Add__c>
    * Parameter : list of product code
    */
    public static list<gii__Product2Add__c> getProductDetails(list<String> listSKU){ 
        list<gii__Product2Add__c> lstProdRefs = [select id,gii__ProductReference__r.SKU__c,gii__ProductStyle__c, gii__ProductStyle__r.name from gii__Product2Add__c where gii__ProductReference__r.SKU__c IN : listSKU];
        return lstProdRefs;
    }
    /*
    * Method name : getCarrierInfo
    * Description : return carrier info for the carrier code
    * Return Type :  list<gii__Carrier__c>
    * Parameter : list of carrier 
    */
    public static list<gii__Carrier__c> getCarrierInfo(set<String> setCarrierCode){ 
        return [SELECT Id, Name,giic_UniqueCarrier__c FROM gii__Carrier__c where giic_UniqueCarrier__c in :setCarrierCode];
    }
    /*
    * Method name : getAddtionalCharge
    * Description : return addtional charge
    * Return Type :  list<gii__AdditionalCharge__c>
    * Parameter : list of addtional charge 
    */
    public static list<gii__AdditionalCharge__c> getAddtionalCharge(set<String> setAdditonalChargeCode){ 
        return [SELECT Id, Name,giic_AdditionalChargeCode__c FROM gii__AdditionalCharge__c where giic_AdditionalChargeCode__c in :setAdditonalChargeCode];
    }
    /*
    * Method name : getDiscountPercent
    * Description : return Promotion Line Style
    * Return Type :  list<gii__PromotionLineStyle__c>
    * Parameter : id of promocode in string
    */
    public static list<gii__PromotionLineStyle__c> getDiscountPercentStyle(string promocode){ 
        List<gii__PromotionLineStyle__c> lstPRS =  [Select id,name,gii__DiscountPercent__c from gii__PromotionLineStyle__c where gii__PromotionLine__r.gii__Promotion__c = : promocode and gii__DiscountPercent__c != null limit 1];
        return lstPRS;
    }
    
    public static list<gii__PromotionLineProduct__c> getDiscountPercentLinePrd(string promocode){ 
        List<gii__PromotionLineProduct__c> lstPLP = [select id,gii__DiscountPercent__c from gii__PromotionLineProduct__c where gii__PromotionLine__r.gii__Promotion__c = : promocode and gii__DiscountPercent__c !=null limit 1];
        return lstPLP;
    }
    /*
    * Method name : getSOLInfo
    * Description : return Sales Order lines for SO
    * Return Type :  map<id,gii__salesorder__c>
    * Parameter :  sales order ids
    */
    public static list<gii__SalesOrderLine__c> getSOLInfo(set<Id> setSOIds){ 
        return [select id from gii__SalesOrderLine__c where giic_LineStatus__c != :giic_Constants.SOSTATUSCANCELLED and gii__SalesOrder__c in :setSOIds];
        
    }
    /*
    * Method name : getSOLInfo
    * Description : return Sales Order lines for SO
    * Return Type :  map<id,gii__salesorder__c>
    * Parameter :  sales order ids
    */
    public static list<gii__SalesOrderLine__c> getSOLInfoforUpdate(set<Id> setSOIds){ 
        return [select id,gii__SalesOrder__c,gii__ProductAmount__c,gii__DiscountAmount__c,gii__SalesOrder__r.giic_HPRVAL1__c,gii__SalesOrder__r.giic_HPRVAL2__c from gii__SalesOrderLine__c where giic_LineStatus__c != :giic_Constants.SOSTATUSCANCELLED and gii__SalesOrder__c in :setSOIds];
        
    }
}