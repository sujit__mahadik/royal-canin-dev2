/************************************************************************************
Version : 1.0
Name : giic_Test_ProdRefTrigger
Created Date : 10/23/2018
Function : Create Test data for Trigger
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/

@isTest(seeAllData = false)
private class giic_Test_ProdRefTrigger {
     @testSetup
    static void setup()
    {
        giic_Test_DataCreationUtility.insertWarehouse();  
        giic_Test_DataCreationUtility.testSystemPolicyCreation2();   
        giic_Test_DataCreationUtility.CreateAdminUser();   
    }
    
    public static testMethod void testDataCreation() {
        User oUser = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        System.Runas(oUser)  
        {
            List<gii__Product2Add__c> testProductList = giic_Test_DataCreationUtility.insertProduct_N(); 
            
        }
    }

}