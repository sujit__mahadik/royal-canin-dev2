/************************************************************************************
Version : 1.0
Created Date : 14 Aug 2018
Function : process the Sales Order and allocate the lines
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/
@isTest
private class giic_Test_AllocationHelper {
    @testSetup
    static void setup(){
        List<Account> testConsumerAccountList = giic_Test_DataCreationUtility.insertConsumerAccount();
        List<gii__Warehouse__c> testWareList = giic_Test_DataCreationUtility.insertWarehouse();
        giic_Test_DataCreationUtility.insertExceptionRoute(new list<Id>{testConsumerAccountList[0].id},new list<Id>{testWareList[0].id},1);
        giic_Test_DataCreationUtility.insertRoute(new list<Id>{testConsumerAccountList[0].id},new list<Id>{testWareList[0].id},1);
        
        SetupTestData.createCustomSettings();
        giic_Test_DataCreationUtility.testSystemPolicyCreation();
        List<gii__AccountAdd__c> testAccRefList = giic_Test_DataCreationUtility.getAccountReference(testConsumerAccountList);
        Database.insert(testAccRefList);
        List<gii__Product2Add__c> testProductList = giic_Test_DataCreationUtility.insertProduct();
        giic_Test_DataCreationUtility.insertProductInventoryQTYDetails();
        //List<gii__SalesOrder__c> testSOList = giic_Test_DataCreationUtility.insertBulkSalesOrder();
         List<gii__Carrier__c> lstcarrier = giic_Test_DataCreationUtility.createCarrier();
        List<gii__SalesOrderLine__c> testSOLine = giic_Test_DataCreationUtility.insertBulkSOLine();
        giic_Test_DataCreationUtility.CreateAdminUser();
        
    }
    public static testMethod void testProcessNonStockAllocation(){
        User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        System.Runas(u)  
        {
        Test.startTest();
       
        List<gii__SalesOrder__c> testSOList = [SELECT Id,giic_CustomerPriority__c,gii__DropShip__c,gii__OrderDate__c,giic_NewPickDate__c,gii__Carrier__r.Name,gii__ScheduledDate__c,gii__Account__r.giic_WarehouseDistanceMapping__c, 
                                                (SELECT Id,giic_LineStatus__c,gii__NonStockQuantity__c,gii__LineStatus__c, gii__Product__c, gii__Product__r.gii__DefaultWarehouse__c,
                                                gii__Warehouse__c,gii__OrderQuantity__c,Name
                                                FROM gii__SalesOrder__r)
                                                FROM gii__SalesOrder__c];
       
        List<gii__SalesOrderLine__c>lstSalesOrderLine = [ select id, name, gii__SalesOrder__c, gii__OrderQuantity__c, gii__Product__c,  gii__Product__r.gii__SellingUnitofMeasure__c, gii__Product__r.gii__StockingUnitofMeasure__c, gii__ProductInventory__c from gii__SalesOrderLine__c];                                        
       system.assertEquals(lstSalesOrderLine.size()>0, true);
       //system.debug('lstSalesOrderLine:::'+lstSalesOrderLine);
       system.assertEquals(testSOList.size()>0,true);
       //system.debug('testSOList:::'+testSOList);
       
        giic_AllocationHelper.processNonStockAllocation(testSOList);
        gii__Warehouse__c objWarehouse = new gii__Warehouse__c(Name='Test Warehouse', giic_WarehouseCode__c = '801');
        Database.insert(objWarehouse);
        List<gii__SalesOrderLine__c> sOLineUpdate = new List<gii__SalesOrderLine__c>();
        for(gii__SalesOrder__c so : testSOList){
            for(gii__SalesOrderLine__c soL : so.gii__SalesOrder__r){
                system.assert(soL != null );
                //system.debug('soL:::'+soL);
                soL.gii__Product__r.gii__DefaultWarehouse__c = objWarehouse.Id;
                sOLineUpdate.add(soL);
            }
        }
        Database.update(sOLineUpdate);
        giic_AllocationHelper.processNonStockAllocation(testSOList);
        giic_AllocationHelper.getSalesOrderLinesForSO(new set<Id>(),new set<Id>(),new set<Id>());
        giic_AllocationHelper.processAllocation(testSOList);
        giic_AllocationHelper.deleteInventoryReserve(new map<id,gii__SalesOrderLine__c>(lstSalesOrderLine).keyset());
        giic_AllocationHelper.clearBackOrder(new map<id,gii__SalesOrderLine__c>(lstSalesOrderLine).keyset());
        Test.stopTest();
    }
}
}