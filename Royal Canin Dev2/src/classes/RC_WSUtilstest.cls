@isTest
private class RC_WSUtilstest {
    
    static testMethod void testRC_WSUtils2() {
             SetupTestData.createCustomSettings();
             
             List<Global_Parameters__c> globalParametersList = new List<Global_Parameters__c>();
             
             Global_Parameters__c dtendpoint = new Global_Parameters__c();
             dtendpoint.Name = 'RCOrderDetailAPIEndpoint';
             dtendpoint.Value__c = '12345678901234567890123456789012';//Must be exactly 32 characters
             
             globalParametersList.add(dtendpoint);
             
             Global_Parameters__c clientid = new Global_Parameters__c();
             clientid.Name = 'client_id';
             clientid.Value__c = '12345678901234567890123456789012';//Must be exactly 32 characters

             globalParametersList.add(clientid);
             
             Global_Parameters__c clientsecret = new Global_Parameters__c();
             clientsecret.Name = 'client_secret';
             clientsecret.Value__c = '12345678901234567890123456789012';//Must be exactly 32 characters
             
             globalParametersList.add(clientsecret);
             
             insert globalParametersList;
             
             Account testAccount = new Account(Name='Test Company Name123');
             testAccount.ShippingStreet = 'address';
             testAccount.ShippingCity = 'San Francisco';
             testAccount.ShippingState = 'CA';
             testAccount.ShippingPostalCode = '63301';
             testAccount.ShippingCountry = 'US';
             testAccount.Key_Account_Designation__c = 'OLP KEY ACCOUNT';
             testAccount.Client_Type_Description__c = 'ONLINE';
             
             insert testAccount;
             ApexPages.StandardController sc = new ApexPages.StandardController(testAccount);
             //OrderDetailsController c = new OrderDetailsController(sc);
        }
    
        static testMethod void testRC_WSUtils() {
             SetupTestData.createCustomSettings();
             
             List<Global_Parameters__c> globalParametersList = new List<Global_Parameters__c>();
             
             Global_Parameters__c dtendpoint = new Global_Parameters__c();
             dtendpoint.Name = 'RCOrderDetailAPIEndpoint';
             dtendpoint.Value__c = '12345678901234567890123456789012';//Must be exactly 32 characters
             
             globalParametersList.add(dtendpoint);
             
             Global_Parameters__c clientid = new Global_Parameters__c();
             clientid.Name = 'client_id';
             clientid.Value__c = '12345678901234567890123456789012';//Must be exactly 32 characters

             globalParametersList.add(clientid);
             
             Global_Parameters__c clientsecret = new Global_Parameters__c();
             clientsecret.Name = 'client_secret';
             clientsecret.Value__c = '12345678901234567890123456789012';//Must be exactly 32 characters
             
             globalParametersList.add(clientsecret);
             
             insert globalParametersList;
             
             Account testAccount = new Account(Name='Test Company Name123');
             testAccount.ShipToCustomerNo__c = '1010';
             testAccount.ShippingStreet = 'address';
             testAccount.ShippingCity = 'San Francisco';
             testAccount.ShippingState = 'CA';
             testAccount.ShippingPostalCode = '63301';
             testAccount.ShippingCountry = 'US';
             testAccount.Key_Account_Designation__c = 'OLP KEY ACCOUNT';
             testAccount.Client_Type_Description__c = 'ONLINE';
             testAccount.ShipToCustomerNo__c = 'VET-00000-000';
                 
             insert testAccount;
             ApexPages.StandardController sc = new ApexPages.StandardController(testAccount);                  
             RC_WSUtils.callOrderHistory('VET-00000-000', 1, 10, 1, 10);
            
        } 
    
    
}