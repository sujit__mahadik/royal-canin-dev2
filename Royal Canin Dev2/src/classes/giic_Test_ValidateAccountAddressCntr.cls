/************************************************************************************
Version : 1.0
Name : giic_Test_ValidateAccountAddressCntr
Created Date : 10 Sep 2018
Function : test class for giic_ValidateAccountAddressController
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/

@isTest(SeeAllData = false)
private class giic_Test_ValidateAccountAddressCntr {

@testSetup static void setup() {
    giic_Test_DataCreationUtility.insertWarehouse();
    giic_Test_DataCreationUtility.testSystemPolicyCreation();
    giic_Test_DataCreationUtility.insertGlobalParametersCustomSetting();
    giic_Test_DataCreationUtility.CreateAdminUser();
}

private static testMethod void testpositive() {
    User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
       
       System.Runas(u)        
        {
            List<Account> lstAccount = giic_Test_DataCreationUtility.insertConsumerAccount();

            Test.startTest();
             //Test.setMock(WebServiceMock.class, new giic_FedExCalloutMock());

            ApexPages.StandardController sc = new ApexPages.StandardController(lstAccount[0]);
            giic_ValidateAccountAddressController obj = new giic_ValidateAccountAddressController(sc);

            obj.onLoad();
            obj.goBack();
            obj.applySuggestedAddress();
            
            Test.stopTest();
        }
}

private static testMethod void testnegative() {
    User u = [select id,name from user where email = 'rcstandarduser@testorg.com' limit 1];
        
        System.Runas(u)        
        {
            List<Account> lstAccount = giic_Test_DataCreationUtility.insertConsumerAccount();

            Test.startTest();
            //Test.setMock(WebServiceMock.class, new giic_FedExCalloutNegativeMock());

            ApexPages.StandardController sc = new ApexPages.StandardController(lstAccount[0]);
            giic_ValidateAccountAddressController obj = new giic_ValidateAccountAddressController(sc);

            obj.onLoad();
            obj.goBack();
            obj.applySuggestedAddress();
            
            Test.stopTest();
        }
}

}