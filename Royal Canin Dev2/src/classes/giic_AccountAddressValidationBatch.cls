/************************************************************************************
Version : 1.0
Name : giic_AccountAddressValidationBatch
Created Date : 13 Sept 2018
Function : Address validation for Account
Modification Log :
* Developer Date Description
* ----------------------------------------------------------------------------
*************************************************************************************/

global class giic_AccountAddressValidationBatch implements Database.Batchable<sObject>, Database.AllowsCallouts{
    global Database.QueryLocator start(Database.BatchableContext BC){
        //Get Records for Address validation
        String soql = 'SELECT Id, ShipToCustomerNo__c, ShippingStreet, ShippingCity, ShippingPostalCode, ShippingCountry FROM Account WHERE Validated__c != true'; 
        System.debug(':::giic_AccountAddressValidationBatch soql=' + soql);
        return Database.getQueryLocator(soql); 
    }

    global void execute(Database.BatchableContext BC, List<Account> lstAccount){
        //Validate Address
        giic_IntegrationCommonUtil.validateAccountAddressAddress(lstAccount);
    }

    global void finish(Database.BatchableContext BC){
        
    }

}