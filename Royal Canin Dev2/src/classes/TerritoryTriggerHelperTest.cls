/*
 * Date:             - Developer, Company                          - Description
 * 2015-08-06        - Joe Wortman, Acumen Solutions               - Created
 * 2015-08-12        - Stevie Yakkel, Acumen Solutions             - Edited
 */
@isTest
private class TerritoryTriggerHelperTest {
    
    @isTest static void deleteTerritoryAssociationTest() {
        Territory__c testTerritory = new Territory__c();
        testTerritory.Name = 'New Territory';

        Test.startTest();
        insert testTerritory;

        TerritoryAssignment__c testAssignment = new TerritoryAssignment__c();
        testAssignment.TerritoryId__c = testTerritory.Id;
        insert testAssignment;

        delete testTerritory;   
        
        List<TerritoryAssignment__c> territoryAssignments = [SELECT Id FROM TerritoryAssignment__c WHERE TerritoryId__c = :testTerritory.Id];
        System.assert(territoryAssignments.isEmpty());
        Test.stopTest();
    }

    @isTest static void updateHiddenOwnerTest() {
        Fiscal_Year__c fyy = new Fiscal_Year__c();
        fyy.isActive__c= true;
        fyy.Start_Date__c = System.today();
        fyy.End_Date__c = System.today()+1;
        insert fyy;
        Territory__c testTerritory = new Territory__c();
        testTerritory.Name = 'New Territory';

        Test.startTest();
        insert testTerritory;
        
        TerritoryAssignment__c tass = new TerritoryAssignment__c();
        tass.Fiscal_Year__c = fyy.Id;
        tass.TerritoryID__c = testTerritory.Id;
        
        insert tass;
        
        testTerritory.Name = 'Newer Territory';
        update testTerritory;
        
        testTerritory = [SELECT Id, OwnerId, OwnerHidden__c FROM Territory__c WHERE Id = :testTerritory.Id];
        //System.assertEquals(testTerritory.OwnerHidden__c, testTerritory.OwnerId);
        Test.stopTest();
    }

    @isTest static void updateSyncNeededTest() {
        User u1 = TestHelperClass.createUser(1);
        User u2 = TestHelperClass.createUser(2);
        
        Fiscal_Year__c fyy = new Fiscal_Year__c();
        fyy.isActive__c= true;
        fyy.Start_Date__c = System.today();
        fyy.End_Date__c = System.today()+1;
        insert fyy;
        
        Territory__c testTerritory = new Territory__c();
        testTerritory.Name = 'New Territory';
        testTerritory.OwnerId = u1.Id;
        Test.startTest();
        insert testTerritory;
        
        TerritoryAssignment__c tass = new TerritoryAssignment__c();
        tass.Fiscal_Year__c = fyy.Id;
        tass.TerritoryID__c = testTerritory.Id;
        
        insert tass;
        testTerritory.OwnerId = u2.Id;
        update testTerritory;
        Test.stopTest();

        testTerritory = [SELECT Id, Sync_Needed__c FROM Territory__c WHERE Id = :testTerritory.Id];
        System.assert(testTerritory.Sync_Needed__c);
    }

    @isTest static void updateTerritoryOwnerTest() {
        User u1 = TestHelperClass.createUser(1);
        User u2 = TestHelperClass.createUser(2);
        Territory__c testTerritory = new Territory__c();
        testTerritory.Name = 'New Territory';
        testTerritory.OwnerId = u1.Id;
        
        Fiscal_Year__c fyy = new Fiscal_Year__c();
        fyy.isActive__c= true;
        fyy.Start_Date__c = System.today();
        fyy.End_Date__c = System.today()+1;
        insert fyy;
        
        Test.startTest();

        insert testTerritory;
        
        TerritoryAssignment__c testAssignment = new TerritoryAssignment__c();
        testAssignment.TerritoryId__c = testTerritory.Id;
        testAssignment.OwnerId = u1.Id;
        testAssignment.Fiscal_Year__c = fyy.Id;
        insert testAssignment;
        
        testAssignment.OwnerId = u2.Id;
        update testAssignment;

        testTerritory.OwnerId = u2.Id;
        update testTerritory;

        TerritoryAssignment__c testAssignment2 = new TerritoryAssignment__c();
        testAssignment2.TerritoryId__c = testTerritory.Id;
        insert testAssignment2;

        testTerritory.Name = 'Newer Territory';
        update testTerritory;

        Test.stopTest();
        testTerritory = [SELECT Id, OwnerId, Name FROM Territory__c WHERE Id = :testTerritory.Id];
        testAssignment = [SELECT Id, OwnerId FROM TerritoryAssignment__c WHERE Id = :testAssignment.Id];
        System.assertEquals(testTerritory.OwnerId, testAssignment.OwnerId);
    }
}