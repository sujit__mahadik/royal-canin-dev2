public with sharing class SalesSummaryTriggerHelper {


	public static void processMaxDate(List <Sales_Summary__c> salesSummaryList){
		System.debug('I am running the trigger now');
		Global_Parameters__c maxDateCs = Global_Parameters__c.getValues('maxSalesSummaryDate');

		if(maxDateCs == null) {
			Global_Parameters__c gp = new Global_Parameters__c(Name='maxSalesSummaryDate');
			insert gp;
			maxDateCs = gp;
		}
		for (Sales_Summary__c ss: salesSummaryList){
			if (maxDateCs.Date__c!=null){
				if (ss.Type__c=='Fiscal' && maxDateCs.Date__c < ss.Period_Start_Date__c){
					maxDateCs.Date__c=ss.Period_Start_Date__c;
					maxDateCs.Value__c= ss.Year__c+'-'+ss.Period__c.intValue();
					maxDateCs.Attribute1__c = string.valueOf(ss.Period__c);
					maxDateCs.Attribute2__c = ss.Quarter__c;
					maxDateCs.Attribute3__c = ss.Year__c;
				}
			}
			else {
				if (ss.Type__c=='Fiscal'){
					maxDateCs.Date__c=ss.Period_Start_Date__c;
					maxDateCs.Value__c= ss.Year__c+'-'+ss.Period__c.intValue();
					maxDateCs.Attribute1__c =  string.valueOf(ss.Period__c);
					maxDateCs.Attribute2__c = ss.Quarter__c;
					maxDateCs.Attribute3__c = ss.Year__c;
				}
			}
		}
		update maxDateCs;
	}
}