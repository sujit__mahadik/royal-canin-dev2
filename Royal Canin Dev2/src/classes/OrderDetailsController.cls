/* 
 * 2/06/2018       - Bethi Reddy                                 - Edited to change the Orderhistory services end point to support MD phase1 project
 */

global with sharing class OrderDetailsController {

    public OrderDetailsWrapper odw {get;set;}
    public Account acc {get;set;}
    public Boolean renderError {get;set;}
    public Integer startDirect {get;set;}
    public Integer endDirect {get;set;}
    public Integer startOp {get;set;}
    public Integer endOp {get;set;}
    public Boolean renderPrevDirect {get;set;}
    public Boolean renderNextDirect {get;set;}
    public Boolean renderNextOp {get;set;}
    public Boolean renderPrevOp {get;set;}
    public Boolean bOHCalloutDone {get;set;}
    public Boolean renderCallout {get;set;}
    public string  clickedOrderNumber {get;set;}
    public Boolean renderQueryButton {get;set;}
    public string nav_dir {get;set;} //variable used to navigate in mobile version
    public string nav_type {get;set;} //variable used to navigate in mobile version

    
    
    public void resetNavButtons(){
        system.debug('in reset nav'+startDirect);
        system.debug('startDirect~~~~~~'+startDirect);
        
        if(startDirect-10 < 0) {
                system.debug('renderprevdirect == false');
                renderprevdirect= false;
            } else {
                system.debug('renderprevdirect == true');
                renderprevdirect= true;
            }
        //system.debug('count~~~'+odw.OrderHistoryDetails.get(0).count);
        system.debug('odw~~~~'+odw);
        system.debug('endDirect~~~~'+endDirect);
        if(odw == null ? false : true && endDirect < (odw.OrderHistoryDetails.get(0).count ) ) {
                system.debug('rendernextdirect == true');
                rendernextdirect =true;
            } else {
                system.debug('rendernextdirect == false');
                rendernextdirect= false;
            }
            
        if(startOp-10 < 0) {
                system.debug('renderprevOp == false');
                renderprevOp= false;
            } else {
                system.debug('renderprevOp == true');
                renderprevOp= true;
            }
            
        if(odw == null ? false : true && endOp < (odw.OrderHistoryDetails.get(1).count ) ) {
                system.debug('rendernextOp == true');
                rendernextOp= true;
            } else {
                system.debug('rendernextOp == false');
                rendernextOp= false;
            }
        
    }
    
    public void filterOrder(){
        
    }
    
    public void goback(){
        clickedOrderNumber=null;
    }

    public String getPageCountDirect {
        get{
            return odw == null ? '' : 'Page ' + (Math.Floor(startDirect/10.0)+1) + ' of ' + (Math.Ceil(odw.OrderHistoryDetails.get(0).count/10.0)) + ' for ' + odw.OrderHistoryDetails.get(0).count + ' direct orders.';
        }
    }
    
    public String getPageCountOp {
        get{
            return odw == null ? '' : 'Page ' + (Math.Floor(startOp/10.0)+1) + ' of ' + (Math.Ceil(odw.OrderHistoryDetails.get(1).count/10.0)) + ' for ' + odw.OrderHistoryDetails.get(1).count + ' OPH orders.';
        }
    }

    public OrderDetailsController(ApexPages.StandardController controller) {
        system.debug('+++++constr');
        acc = [select id, Key_Account_Designation__c, Client_Type_Description__c, ShipToCustomerNo__c, shippingStreet, shippingCity, shippingState, shippingPostalCode, billingStreet, billingCity, billingState, billingPostalCode from Account where id =: controller.getId() LIMIT 1];
        startDirect = 1;
        endDirect = 10;
        startOp = 1;
        endOp = 10;
        system.debug(acc.ShipToCustomerNo__c+'test data'+controller.getId());
        if(acc.ShipToCustomerNo__c != null){
            bOHCalloutDone=false;
            renderCallout=true;
        } else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'This account has no order history.'));
            renderError = true;
            renderCallout=false;
        }
        renderQueryButton=true;
        try {
            if (acc.Key_Account_Designation__c!=null){
                if (acc.Key_Account_Designation__c.toUpperCase().contains('OLP KEY ACCOUNT')){
                renderQueryButton= false;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Order History is disabled for Online Pharmacy accounts'));
                }
            }
            if (acc.Client_Type_Description__c!=null){
                if (acc.Client_Type_Description__c.toUpperCase().contains('ONLINE')){
                renderQueryButton= false;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Order History is disabled for Online Pharmacy accounts'));
                }
            }
        }
        catch (exception e){
            system.debug('data issue occurred');
        }
        
    }
    
    
    @RemoteAction
    global static OrderDetailsWrapper remote_makeCall(string stcn, integer startDirect, integer endDirect, integer startOp, integer endOp ){
        OrderDetailsWrapper odw = RC_WSUtils.callOrderHistory(stcn, startDirect, endDirect, startOp, endOp);
        return odw;
    }
     

public void makeCall() {
        HTTPResponse res;
        if (!Test.isRunningTest()){
            HttpRequest req = new HttpRequest();
            req.setEndpoint(buildUrl(acc.ShipToCustomerNo__c, startDirect, endDirect, startOp, endOp));
            req.setTimeout(119999);
            req.setMethod('GET');
            try {
                Http http = new Http();
                res = http.send(req);
                System.debug(res.getBody());
            } catch(Exception ex) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'This account has no order history.'));
                renderError = true;
                res = new HttpResponse();
                return;
            }
        }else{
            String urlbuilt = buildUrl(acc.ShipToCustomerNo__c, startDirect, endDirect, startOp, endOp);
            res = new HttpResponse();
            res.setStatusCode(200);
            res.setBody('{"foo":"bar"}');

        }

        if(res.getStatusCode() != 200) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'This account has no order history.'));
            renderError = true;
            return;
        }
        
        try{
            odw = OrderDetailsWrapper.parse(res.getBody());
            system.debug('odw' + odw.OrderHistoryDetails.get(0));
            bOHCalloutDone=true;
        } catch(Exception ex) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'This account has no order history.'));
            renderError = true;
            return;
        }
        resetNavButtons();
    }

   private String buildUrl(String sellTo, Integer startIndexD, Integer endIndexD, Integer startIndexO, Integer endIndexO) {
        string rVal = Global_Parameters__c.getValues('RCOrderDetailAPIEndpoint').Value__c + '/v1/Ancillary/Order/GetOrderHistory/?format=json&sellToCustomerNumber=' + sellTo + '&start=' + startIndexD + '&end=' + endIndexD + '&startoph=' + startIndexO + '&endoph=' + endIndexO + '&client_id=' + Global_Parameters__c.getValues('client_id').Value__c + '&client_secret=' + Global_Parameters__c.getValues('client_secret').Value__c;
        return rVal;
    } 
    
   /*   private String buildUrl(String sellTo, Integer startIndexD, Integer endIndexD, Integer startIndexO, Integer endIndexO) {
    system.debug('BUILDstartindD:' + startIndexD);
    system.debug('BUILDendindD:' + endIndexD);
    system.debug('BUILDstartindO:' + startIndexO);
    system.debug('BUILDendindO:' + endIndexO);
    //new: http://royalcanin-us-orderhistory-api-test.cloudhub.io/api/orders?sellToCustomerNumber=POS-0000023-001&start=0&end=10&startoph=0&endoph=10
    //system.debug('url:' + Global_Parameters__c.getValues('RCOrderDetailAPIEndpoint').Value__c + '/api/orders/?sellToCustomerNumber=' + sellTo + '&start=' + startIndexD + '&end=' + endIndexD + '&startoph=' + startIndexO + '&endoph=' + endIndexO + '&client_id=' + Global_Parameters__c.getValues('client_id').Value__c + '&client_secret=' + Global_Parameters__c.getValues('client_secret').Value__c);
    return Global_Parameters__c.getValues('RCOrderDetailAPIEndpoint').Value__c + '/api/orders/?format=jason&sellToCustomerNumber=' + sellTo + '&start=' + startIndexD + '&end=' + endIndexD + '&startoph=' + startIndexO + '&endoph=' + endIndexO + '&client_id=' + Global_Parameters__c.getValues('client_id').Value__c + '&client_secret=' + Global_Parameters__c.getValues('client_secret').Value__c;
  }  */
    

    public void nav() {
        string dir = ApexPages.currentpage().getparameters().get('dir');
        string type = ApexPages.currentpage().getparameters().get('type');
        nav_util(dir,type);
    }
    
    
    public void nav_mobile() {
        string dir = nav_dir;
        string type = nav_type;
        nav_util(dir,type);
    }
    
    public void nav_util(string dir, string type){
        if(type == 'Direct'){
            if(dir == 'next'){
                startDirect += 10;
                endDirect += 10;
            } else {
                startDirect -= 10;
                endDirect -= 10;
            }
        } else if(type == 'OPH') {
            if(dir == 'next'){
                startOp += 10;
                endOp += 10;
            } else {
                startOp -= 10;
                endOp -= 10;
            }
        } else {
            return;
        }
        makeCall();
    }
}