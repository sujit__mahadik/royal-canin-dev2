({
    Init : function(component, event, helper) {
        var months = ['1','2','3','4','5','6','7','8','9','10','11','12'];
        component.set("v.months", months);
        var d = new Date();
        var years = [];
        for(var i = d.getFullYear(); i< d.getFullYear() + 16; i++){
            years.push(i);
        }
        component.set("v.years",years);
        
        var cardTypeValues = $A.get("$Label.c.giic_CardTypesAndValues");        
        var cardValueMap = {};
        var cardsList = [];
        var cards = cardTypeValues.split(";"); 
        for(var i =0; i < cards.length-1; i++){
            var cardtypevalue = cards[i].split("-->"); 
            cardsList.push(cardtypevalue[0].trim());
            cardValueMap[cardtypevalue[0].trim()] = cardtypevalue[1].trim();
        }
        component.set("v.cardValueMap", cardValueMap);
        component.set("v.cardsList", cardsList);        
        
        var action = component.get('c.getOnLoadDetails');
        action.setParams({
            'recordId' : component.get('v.recordId')
        });
        action.setCallback(this, function(response){
            var state = response.getState();    
            debugger;
            if(state === "SUCCESS"){
                
                var result = response.getReturnValue();
                if(result.isReleased || result.isCancelled){
                    var resultsToast = $A.get("e.force:showToast");
                    if(result.isReleased ){
                        resultsToast.setParams({
                            title: "Error",
                            message: result.isReleasedMsg,
                            type : "Error"
                        });
                    }
                    else{
                        resultsToast.setParams({
                            title: "Error",
                            message: result.isCancelledMsg,
                            type : "Error"
                        });
                        
                    }
                    resultsToast.fire();
                    $A.get("e.force:closeQuickAction").fire();
                }
                else if(result.isNOTCC && !$A.util.isUndefined(result.isNOTCCMsg))
                {
                    var resultsToast = $A.get("e.force:showToast");
                    resultsToast.setParams({
                        title: "Error",
                        message: result.isNOTCCMsg,
                        type : "Error"
                    });
                    resultsToast.fire();
                    $A.get("e.force:closeQuickAction").fire();
                }
                else if(result.isNOTInitial && !$A.util.isUndefined(result.isNOTInitialMsg))
                {
                    var resultsToast = $A.get("e.force:showToast");
                    resultsToast.setParams({
                        title: "Error",
                        message: result.isNOTInitialMsg,
                        type : "Error"
                    });
                    resultsToast.fire();
                    $A.get("e.force:closeQuickAction").fire();
                }
                component.set("v.paymentMethod",result.paymentMethod);
                component.set("v.TotalAmount",parseFloat(result.TotalAmount));
                component.set("v.TotalPaidAmount",parseFloat(result.TotalAmount));
                component.set("v.sObjectType", result.sObjectType);
                
                if(!$A.util.isUndefinedOrNull(result.sObjectType) && result.sObjectType == "gii__SalesOrder__c")
                {
                    var fields = ["Name","giic_TaxCal__c","giic_TaxExempt__c","gii__PaymentMethod__c","gii__Account__r.Check_if_you_are_Tax_Exempt__c","gii__Account__r.Resale_Sales_Tax_Cert_Expiration_Date__c"];
                    component.set("v.fieldsToQuery", fields);
				}
                
                //console.log('result.CardList ' + JSON.stringify(result.sObjectType));
                component.set("v.RewardsPoints", result.rewardsPoints);
                if (!$A.util.isUndefinedOrNull(result.cardList)){
                    if(result.cardList.length > 0)
                    {                    
                        component.set("v.CardList",result.cardList);                    
                    }
                    else{
                        component.set("v.isError",true);
                        component.set("v.errorMessage",$A.get("$Label.c.giic_CardsNotFound")); //"Cards Not Found."
                    } 
                }
                else{
                    component.set("v.isError",true);
                    component.set("v.errorMessage",$A.get("$Label.c.giic_GenericErrorMsgCCUI")); //Server action failed. Please check error log.
                }
                if (!$A.util.isUndefined(result.newCard)){
                    component.set("v.NewCard",result.newCard);
                    component.set("v.NewCardDefault",result.newCard);
                }
                
                var totalRewardPoints = 0;
                //console.log('rewardsPoints:: ' + JSON.stringify(result.rewardsPoints) );
                
                if ((!$A.util.isUndefined(result.accBillInfo) || !$A.util.isEmpty(result.accBillInfo)) &&
                    ($A.util.isUndefined(result.accBillInfoError) || $A.util.isEmpty(result.accBillInfoError))){
                    component.set("v.BillTo", result.accBillInfo);
                }
                else{
                    component.set("v.accBillInfoError", result.accBillInfoError);
                    //console.log("v.accBillInfoError : "+component.get("v.accBillInfoError"));
                }
                component.set("v.initDone",true);
                
            }else{
                var errors = response.getError();
                var message = 'Unknown error'; // Default error message
                // Retrieve the error message sent by the server
                if (errors && Array.isArray(errors) && errors.length > 0) {
                    message = errors[0].message;
                }
                // Display the message
                console.log(message);
                var resultsToast = $A.get("e.force:showToast");
                    resultsToast.setParams({
                        title: "Error",
                        message: message,
                        type : "Error"
                    });
                    resultsToast.fire();
                $A.get("e.force:closeQuickAction").fire();
                $A.get('e.force:refreshView').fire(); 
            }
        });
        
        $A.enqueueAction(action);
        
    },
    addNewCardHelper: function(component, event) {
        this.clearScreenHelper(component,event);
        component.set("v.isAddnewError",false);
        component.set("v.AddnewError","");
        var NewCard = component.get("v.NewCard");
        var BillTo = component.get("v.BillTo");
        var action = component.get('c.addNewCreditCard');
        action.setParams({
            'newCard' : JSON.stringify(NewCard),
            'billToWrapper' : JSON.stringify(BillTo)
        });
        action.setCallback(this, function(response){
            var state = response.getState();            
            if(state === "SUCCESS"){
                var result = response.getReturnValue();
                if (!$A.util.isUndefined(result.error) || !$A.util.isEmpty(result.error)){
                    component.set("v.isAddnewError", true);
                    component.set("v.AddnewError", result.error);
                }
                //this.cancelAddNewHelper(component, event);
                /*var resultsToast = $A.get("e.force:showToast");
                    resultsToast.setParams({
                        title: "Success",
                        message: "Card Added Successfully.",
                        type : "Success"
                    });*/
                else{
                    //debugger;
                    //component.set("v.NewCard",component.get("v.NewCardDefault"));
                    component.set("v.isAddnew",false);
                    component.set("v.isEdit",false);
                    component.set("v.isError", false);
                    component.set("v.CardList",result.cardList); 
                    //this.onLoad(component,event);
                    //$A.get('e.force:refreshView').fire(); 
                }
                
            }else{
                let errors = response.getError();
                let message = 'Unknown error'; // Default error message
                // Retrieve the error message sent by the server
                if (errors && Array.isArray(errors) && errors.length > 0) {
                    message = errors[0].message;
                }
                // Display the message
                //console.error(message);
                component.set("v.isAddnewError",true);
                component.set("v.AddnewError",message);
            }
        });
        
        $A.enqueueAction(action);
    },
    saveEditHelper: function(component, event) {
        this.clearScreenHelper(component,event);
        var EditCard = component.get("v.EditCard");
        var action = component.get('c.editCreditCardInfo');
        action.setParams({
            'editCard' : JSON.stringify(EditCard),
            'recId' : component.get("v.recordId")
        });
        action.setCallback(this, function(response){
            var state = response.getState();            
            if(state === "SUCCESS"){
                var result = response.getReturnValue();
                if (!$A.util.isUndefined(result.error) || !$A.util.isEmpty(result.error)){
                    component.set("v.isEditError", true);
                    component.set("v.EditError", result.error);
                }
                this.cancelAddNewHelper(component, event);
                /*var resultsToast = $A.get("e.force:showToast");
                    resultsToast.setParams({
                        title: "Success",
                        message: "Card Edited Successfully.",
                        type : "Success"
                    });*/
                component.set("v.CardList",result.cardList); 
                //$A.get('e.force:refreshView').fire(); 
                
            }else{
                let errors = response.getError();
                let message = 'Unknown error'; // Default error message
                // Retrieve the error message sent by the server
                if (errors && Array.isArray(errors) && errors.length > 0) {
                    message = errors[0].message;
                }
                // Display the message
                //console.error(message);
                component.set("v.isEditError",true);
                component.set("v.EditError",message);
            }
        });
        
        $A.enqueueAction(action);
    },
    deleteCardHelper : function (component, event){
        this.clearScreenHelper(component,event);
        var DeleteCard = component.get("v.DeleteCard");
        var action = component.get('c.deleteCreditCardInfo');
        action.setParams({
            'deleteCard' : JSON.stringify(DeleteCard),
            'recId' : component.get("v.recordId")
        });
        action.setCallback(this, function(response){
            var state = response.getState();            
            if(state === "SUCCESS"){
                var result = response.getReturnValue();
                if (!$A.util.isUndefined(result.error) || !$A.util.isEmpty(result.error)){
                    component.set("v.isCCOpErrDel", true);
                    component.set("v.CCOpErrMsgDel", result.error);
                }
                this.cancelAddNewHelper(component, event);
                /*var resultsToast = $A.get("e.force:showToast");
                    resultsToast.setParams({
                        title: "Success",
                        message: "Card Deleted Successfully.",
                        type : "Success"
                    });*/
                component.set("v.CardList",result.cardList); 
                //$A.get('e.force:refreshView').fire(); 
                
            }else{
                let errors = response.getError();
                let message = 'Unknown error'; // Default error message
                // Retrieve the error message sent by the server
                if (errors && Array.isArray(errors) && errors.length > 0) {
                    message = errors[0].message;
                }
                // Display the message
                //console.error(message);
                component.set("v.isCCOpErrDel",true);
                component.set("v.CCOpErrMsgDel",message);
            }
        });
        
        $A.enqueueAction(action);
    },
    
    cancelAddNewHelper : function(component, event){
        component.set("v.NewCard",component.get("v.NewCardDefault"));
        component.set("v.isAddnewError",false);
        component.set("v.AddnewError","");
        component.set("v.isAddnew",false);
        //component.set("v.isCCOpErr",false);
        //component.set("v.CCOpErrMsg","");
    },
    cancelEditHelper: function(component, event){
        component.set("v.isEditError",false);
        component.set("v.EditError","");
        component.set("v.isEdit",false);
    },
    makePaymentHelper: function(component, event, selectedCard){
        var rewardPoints = [];
        //rewardPoints = this.addRewards(component, event,helper);
        //console.log('rewardPoints :' + rewardPoints);
        var mapRequest={};
        mapRequest["REWARDSPOINTS"]=rewardPoints;
        //console.log('mapRequest==' + JSON.stringify(mapRequest));
        
        var action = component.get('c.makePaymentToCybersource');
        action.setParams({
            'selectedCard' : JSON.stringify(selectedCard),
            'recId' : component.get("v.recordId"),
            'data' : JSON.stringify(mapRequest)
            
        });
        action.setCallback(this, function(response){
            var state = response.getState();            
            if(state === "SUCCESS"){
                //console.log('makePaymentHelper: success');
                var result = response.getReturnValue();
                component.set("v.CardList",result.cardList); 
                if(result.MakepaymentResponse != 'SUCCESS'){
                    component.set("v.isMakepaymentError",true);
                    component.set("v.MakepaymentError",result.MakepaymentResponse);
                }else{
                    $A.get("e.force:closeQuickAction").fire();
                    var resultsToast = $A.get("e.force:showToast");
                    resultsToast.setParams({
                        "title": "Success",
                        "message": "Payment Successfully Done.",
                        "type": "Success"
                    });                
                    resultsToast.fire();
                }
                
            }else{
                //console.log('makePaymentHelper: fail');
                let errors = response.getError();
                let message = 'Unknown error'; // Default error message
                // Retrieve the error message sent by the server
                if (errors && Array.isArray(errors) && errors.length > 0) {
                    message = errors[0].message;
                }
                // Display the message
                console.error(message);
                component.set("v.isMakepaymentError",true);
                component.set("v.MakepaymentError",message);
            }
        });
        
        $A.enqueueAction(action);
    },
    
    
    getTotalRewards :function(component, event, helper){
        var rewards = component.get("v.RewardsPoints");
        //console.log('rewards list :' + JSON.stringify(rewards));
        
        var totalRewardPoints = 0;
        for(var i=0; i < rewards.length; i++ )
        {
            if(rewards[i].isSelected)
            {
                totalRewardPoints += rewards[i].rewardPoint.giic_AmountLeft__c;
            }
        }
        //console.log('totalRewardPoints->' + totalRewardPoints);
        return  totalRewardPoints;
    },
    
    addRewards : function(component, event, helper){
        var rewards = component.get("v.RewardsPoints");
        //console.log('rewards:' + JSON.stringify(rewards));
        var rewardPoints = [] ;
        
        for(var i=0; i < rewards.length; i++ )
        {
            if(rewards[i].isSelected)
            {
                //rewardPoints += rewardPoint.Id + ',';
                rewardPoints.push(rewards[i].rewardPoint.Id);
                //totalRewardPoints += rewards[i].rewardPoint.giic_RewardPointValue__c;
            }
        }
        //console.log('rewardPoints===' + JSON.stringify(rewardPoints));
        return rewardPoints;
        
    },
    submitOrder: function(component, event, helper){
        component.set("v.isMakepaymentError",false);
        component.set("v.isMakepaymentSuccess",false);
        component.set("v.isEditError",false);
        component.set("v.isAddnewError",false);
        var selectedCard;
        var CardList = component.get("v.CardList");
        for(var i =0; i < CardList.length; i++ ){           
            if(CardList[i].isSelectedCard == true){ selectedCard = CardList[i];}
        }
        
        //console.log('helper submit order');
        var simpleRecord =  component.get("v.simpleRecord");
        //console.log('component.get("v.TotalPaidAmount")'+component.get("v.TotalPaidAmount"));
        //console.log('component.get("v.TotalAmount")'+component.get("v.TotalAmount"));
        var rewardPoints = [];
        rewardPoints = this.addRewards(component, event,helper);
        //console.log('rewardPoints :' + rewardPoints);
        var TotalPaidAmount = component.get("v.TotalPaidAmount");
        var TotalAmount = component.get("v.TotalAmount");
        var msgString ;
        var mapRequest={};
        mapRequest["REWARDSPOINTS"]=rewardPoints;
        mapRequest["soId"]=component.get('v.recordId');
        mapRequest["paymentType"]= component.get("v.paymentMethod");
        mapRequest["totalReward"]= this.getTotalRewards(component, event, helper);
        mapRequest["totalPaidAmount"]= TotalPaidAmount;
        mapRequest["totalAmount"]= TotalAmount;
        mapRequest["selectedCard"] = JSON.stringify(selectedCard);
        //console.log($A.localizationService.formatDate(new Date(), "YYYY-MM-DD"));
        //console.log(component.get("v.simpleRecord.gii__Account__r.Resale_Sales_Tax_Cert_Expiration_Date__c"));
        
        if(!simpleRecord.giic_TaxCal__c && 
           (component.get("v.simpleRecord.gii__Account__r.Check_if_you_are_Tax_Exempt__c") != true 
            || (component.get("v.simpleRecord.gii__Account__r.Check_if_you_are_Tax_Exempt__c") == true 
                && component.get("v.simpleRecord.gii__Account__r.Resale_Sales_Tax_Cert_Expiration_Date__c") < $A.localizationService.formatDate(new Date(), "YYYY-MM-DD")
                )
                || component.get("v.simpleRecord.giic_TaxExempt__c") != true 
           )
          )
        {
            //call calculate tax
            this.calculateTax(component, event, helper);
            
        }
        
        if (simpleRecord.gii__PaymentMethod__c == $A.get("$Label.c.giic_PaymentMethod") && $A.util.isUndefinedOrNull(selectedCard)) {
            component.set("v.isMakepaymentError",true);
            component.set("v.MakepaymentError",$A.get("$Label.c.giic_SelectCardToAuth")); //"Please select at least one card for authorization."
            return;
            
            var cardValueMap = component.get("v.cardValueMap");
            var mapKey = selectedCard.CardType;
            var CardTypeValue = cardValueMap[mapKey];
            selectedCard.CardTypeValue = CardTypeValue;
        }
        
        var totalReward = this.getTotalRewards(component, event, helper);
        //console.log('totalReward:' + totalReward);
        //console.log('mapRequest:' + JSON.stringify(mapRequest));
        //Check SO Validations
        var validateSO = component.get('c.validateSalesOrder');
        validateSO.setParams({
            'soId': component.get('v.recordId')            
        });
        validateSO.setCallback(this, function(response) {
            var state = response.getState();
            if (state === 'SUCCESS') {
                var result = response.getReturnValue();                
                if (!$A.util.isUndefinedOrNull(result)) {
                    if (!$A.util.isUndefinedOrNull(result.Error)) {
                        console.log(result.Error);
                        this.displayMsg($A.get("$Label.c.giic_SalesOrderSubmissionError"),'error',result.Error);
                        $A.get("e.force:closeQuickAction").fire();
                        $A.get('e.force:refreshView').fire();
                    }
                    else if(!$A.util.isUndefinedOrNull(result.Success)) {    
                        if (!$A.util.isUndefinedOrNull(result.msgString)) {
                        	msgString = result.msgString;
                        }
                        if (!$A.util.isUndefinedOrNull(result.objSalesOrder)) {
                            if(simpleRecord.gii__PaymentMethod__c == $A.get("$Label.c.giic_PaymentMethod") && result.Success == 'SUCCESS')
                            {   
                                this.authPaymentHelper(component, event, selectedCard, result.objSalesOrder, msgString);
                                //this.submitSO(component, event, component.get('v.recordId'),result.objSalesOrder,selectedCard,msgString);
                             
                            }else{
                                this.submitSO(component, event, component.get('v.recordId'),result.objSalesOrder,selectedCard,msgString);
                            }
                        }                        
                    }
                }
            }
        });
        $A.enqueueAction(validateSO);
        
    },
    submitSO :function(component, event, soId,objSO,selectedCard,msgString){
        var simpleRecord =  component.get("v.simpleRecord");
        var TotalPaidAmount = component.get("v.TotalPaidAmount");
        var TotalAmount = component.get("v.TotalAmount");
        var rewardPoints = [];
        rewardPoints = this.addRewards(component, event,helper);
        var mapRequest={};
        var isError = false;
        if (!$A.util.isUndefinedOrNull(rewardPoints) && rewardPoints.length > 0) {
            mapRequest["REWARDSPOINTS"]=rewardPoints;
        }
        mapRequest["soId"]=component.get('v.recordId');
        mapRequest["paymentType"]=simpleRecord.gii__PaymentMethod__c;
        mapRequest["totalReward"]= this.getTotalRewards(component, event, helper);
        mapRequest["totalPaidAmount"]= TotalPaidAmount;
        mapRequest["totalAmount"]= TotalAmount;
        if (!$A.util.isUndefinedOrNull(selectedCard)) {
            mapRequest["selectedCard"] = JSON.stringify(selectedCard);
        }
        mapRequest["objSO"] = JSON.stringify(objSO);   
        debugger;
        console.log('mapRequest: submitSO' + mapRequest);
        console.log('mapRequest: submitSO' + JSON.stringify(mapRequest));
        //submit sales order 
        var submitAction = component.get('c.submitSalesOrder');
        submitAction.setParams({
            'soId': component.get('v.recordId'),
            'totalReward' : this.getTotalRewards(component, event, helper),
            'paymentType' : simpleRecord.gii__PaymentMethod__c,
            'data' : JSON.stringify(mapRequest)
        });
        
        submitAction.setCallback(this, function(response) {
            debugger;
            var state = response.getState();
            if (state === 'SUCCESS') {
                //console.log(JSON.stringify(response.getReturnValue()));                
                if(!$A.util.isUndefinedOrNull(msgString))
                {
                    this.displayMsg($A.get("$Label.c.giic_RouteOrderWeightValidationWarningTitle"),'error',$A.get("$Label.c.giic_RouteOrderWeightValidationWarning"));
                }
                else{
                    this.displayMsg($A.get("$Label.c.giic_Submitted"),'success',$A.get("$Label.c.giic_SalesOrderReleaseSuccess"));  
                }
                
            }
            else{
                var errors = response.getError();
                if (errors && Array.isArray(errors) && errors.length > 0) {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title: 'Error',
                        type: 'error',
                        message: errors[0].message
                    });
                    toastEvent.fire();
                    isError=true;
                }
                    
            }
            $A.get("e.force:closeQuickAction").fire();
            if(isError)
                $A.get('e.force:refreshView').fire();
            else
                window.parent.location = '/lightning/r/gii__SalesOrder__c/' + component.get('v.recordId') + '/view';
        });
        $A.enqueueAction(submitAction);
    },                                     
    calculateTax :function(component, event, helper){
        var caltax = component.get("c.CalculateTaxforSO");
        caltax.setParams({
            "soId" : component.get("v.recordId")
        });
        caltax.setCallback(this, function (response) {
            var state = response.getState();            
            if (state === "SUCCESS") 
            {
                console.log('calcualte tax successful');
                
            }else{
                this.handleError(component, event, helper, response);
            }        
        });
        $A.enqueueAction(caltax); 
    },
    
    
    authPaymentHelper: function(component, event, selectedCard, objSO, msgString){
        component.set("v.isMakepaymentError",false);
        component.set("v.isMakepaymentSuccess",false);
        component.set("v.isEditError",false);
        component.set("v.isAddnewError",false);
        
        var action = component.get('c.authorizePaymentToCybersource');
        action.setParams({
            'selectedCard' : JSON.stringify(selectedCard),
            'recId' : component.get("v.recordId"),
            'amountToAuth' : null
        });
        action.setCallback(this, function(response){
            var state = response.getState();            
            if(state === 'SUCCESS'){
                //debugger;
                var result = response.getReturnValue();
                component.set("v.authorizedAmount", result.authorizedAmount);
                component.set("v.CardList",result.cardList); 
                //console.log('::result::'+result);
                if(result.zeroAuthResponse != 'SUCCESS'){
                    component.set("v.isMakepaymentError",true);
                    component.set("v.MakepaymentError",result.zeroAuthResponse);
                    
                }else{
                    this.submitSO(component, event, component.get('v.recordId'),objSO,selectedCard,msgString);
                    console.log('Payment Authorized to Cybersource');
                }
                
            }else{
                let errors = response.getError();
                let message = 'Unknown error'; // Default error message
                // Retrieve the error message sent by the server
                if (errors && Array.isArray(errors) && errors.length > 0) {
                    message = errors[0].message;
                }
                // Display the message
                console.error(message);
                component.set("v.isMakepaymentError",true);
                component.set("v.MakepaymentError",message);
                
            }
        });
        
        $A.enqueueAction(action);
        
    },
    updateCardListHelper: function(component, event, subscriptionId){
        var action = component.get('c.updateCardList');
        action.setParams({
            'recordId' : component.get("v.recordId"),
            'token' : subscriptionId            
        });
        action.setCallback(this, function(response){
            var state = response.getState();            
            if(state === "SUCCESS"){
                var result = response.getReturnValue();
                /* component.set("v.CardList",result.CardList); */
                if(result.response != 'SUCCESS'){
                    component.set("v.isMakepaymentError",true);
                    component.set("v.MakepaymentError",result.response);
                }else{
                    this.Init(component, event); 
                    /*component.set("v.isMakepaymentSuccess",true);
                    component.set("v.MakepaymentSuccess","Default Card Successfully Changed.");*/
                    }
                    
                }else{   
                    let errors = response.getError();
                    let message = 'Unknown error'; // Default error message
                    // Retrieve the error message sent by the server
                    if (errors && Array.isArray(errors) && errors.length > 0) {
                        message = errors[0].message;
                    }
                    // Display the message
                    console.error(message);
                    component.set("v.isMakepaymentError",true);
                    component.set("v.MakepaymentError",message);
                }
            });
            
            $A.enqueueAction(action);
        },
    clearScreenHelper : function(component, event){
        component.set("v.isCCOpErrDel",false);
        component.set("v.CCOpErrMsgDel","");
    },
    handleError : function(component, event, helper, response){
        var errorMsg = 'Unknown Error';
        var errors = response.getError();
        if (errors && Array.isArray(errors) && errors.length > 0) {
            errorMsg = errors[0].message;
        }
        this.displayMsg('Error','error',errorMsg);
        
        $A.get("e.force:closeQuickAction").fire();
        /* $A.get('e.force:refreshView').fire();
        window.parent.location = '/lightning/r/gii__SalesOrder__c/' + component.get('v.recordId') + '/view';*/
    },
    displayMsg : function(title, type, message){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: title,
            type: type,
            message: message
        });
        toastEvent.fire();
    }
})