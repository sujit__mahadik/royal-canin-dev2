({
    doInit: function(component, event, helper) {
        //helper.getCardList(component, event);
        helper.Init(component, event, helper);
    },
    handleRecordUpdated: function(component, event, helper) {
        var eventParams = event.getParams();
        if (eventParams.changeType === "ERROR") {
            component.set("v.isMakepaymentError", true);
            console.log(component.get("v.MakepaymentError"));
        }
        if (eventParams.changeType === "LOADED") {
            console.log('record: ' + JSON.stringify(component.get(
                "v.simpleRecord")));
        }
    },
    selectCard: function(component, event, helper) {
        component.set("v.isMakepaymentError", false);
        component.set("v.isMakepaymentSuccess", false);
        component.set("v.isEditError", false);
        component.set("v.isAddnewError", false);
        var selectedItem = event.currentTarget;
        var index = selectedItem.dataset.index;
        var CardList = component.get("v.CardList");
        var subscriptionId = '';
        var recordId = '';
        for (var i = 0; i < CardList.length; i++) {
            if (i == index) {
                CardList[i].isSelectedCard = true;
                CardList[i].IsDefaultCard = true;
                subscriptionId = CardList[i].SubscriptionID;
            } else {
                CardList[i].isSelectedCard = false;
                CardList[i].IsDefaultCard = false;
            }
        }
        helper.updateCardListHelper(component, event, subscriptionId);
        component.set("v.isMakepaymentError", false);
        component.set("v.MakepaymentError", "");
        component.set("v.CardList", CardList);
    },
    makePayment: function(component, event, helper) {
        console.log('makePayment call');
        /*if(component.get("v.sObjectType") == 'gii__OrderInvoice__c' && (component.get("v.authorizedAmount") < component.get("v.TotalPaidAmount"))){
            component.set("v.isMakepaymentError",true);
            //component.set("v.MakepaymentError","Authorized amount is less than Total Amount. Please authorize the amount first."); // Add custom Label here
            component.set("v.MakepaymentError", $A.get("$Label.c.giic_AuthorizedAmountNotSufficient"));
            return;
        }*/
        component.set("v.isMakepaymentError", false);
        component.set("v.isMakepaymentSuccess", false);
        component.set("v.isEditError", false);
        component.set("v.isAddnewError", false);
        var selectedCard;
        var CardList = component.get("v.CardList");
        for (var i = 0; i < CardList.length; i++) {
            if (CardList[i].isSelectedCard == true) {
                selectedCard = CardList[i];
            }
        }
        if ($A.util.isUndefinedOrNull(selectedCard)) {
            component.set("v.isMakepaymentError", true);
            component.set("v.MakepaymentError",
                "Please select at least one card for payment.");
            return;
        }
        var cardValueMap = component.get("v.cardValueMap");
        var mapKey = selectedCard.CardType;
        var CardTypeValue = cardValueMap[mapKey];
        selectedCard.CardTypeValue = CardTypeValue;
        helper.makePaymentHelper(component, event, selectedCard);

    },
    //ranu, total of reeward points 
    consumeRewardJS: function(component, event, helper) {
        //var target = event.target;
        //var dataEle = target.getAttribute("data-selected-Index");
        var rewards = component.get("v.RewardsPoints");
        var totolRewards = 0;
        var totalAmount = 0;
        totalAmount = component.get("v.TotalAmount");
        //rewards[dataEle].isSelected = document.getElementById("lcb").checked;
        for (var i = 0; i < rewards.length; i++) {
            console.log('in fr loop' + rewards[i].isSelected);
            if (rewards[i].isSelected) {
                console.log('in if');
                totolRewards += rewards[i].rewardPoint.giic_AmountLeft__c;
            }
        }

        var TotalPaidAmount = 0.00;
        if (totolRewards > totalAmount && totolRewards > 0) {
            /*if(TotalPaidAmount > 0)
            {
                component.set("v.TotalPaidAmount",TotalPaidAmount.toFixed(2));
            }
            else
            {
                component.set("v.TotalPaidAmount",TotalPaidAmount.toFixed(2));
            }*/
            component.set("v.TotalPaidAmount", parseFloat(
                TotalPaidAmount));

        } else {
            if (totolRewards > 0) {
                //TotalPaidAmount = parseFloat(totalAmount).toFixed(2) - parseFloat(totolRewards).toFixed(2);
                TotalPaidAmount = (totalAmount - totolRewards).toFixed(
                    2);
            } else {
                //TotalPaidAmount = parseFloat(totalAmount).toFixed(2);
                TotalPaidAmount = totalAmount.toFixed(2);
            }

            component.set("v.TotalPaidAmount", parseFloat(
                TotalPaidAmount));
        }

        console.log('rewards--' + JSON.stringify(rewards));
        console.log('TotalPaidAmount--' + TotalPaidAmount);
        component.set("v.RewardsPoints", rewards);
        //component.set("v.TotalAmount",100);


    },

    SubmitOrderJS: function(component, event, helper) {

        helper.submitOrder(component, event, helper);
    },

    applyAddNew: function(component, event, helper) {
        component.set("v.NewCard", component.get("v.NewCardDefault"));
        component.set("v.isAddnewError", false);
        component.set("v.isAddnew", true);
        component.set("v.AddnewError", "");

        if (!$A.util.isEmpty(component.get("v.accBillInfoError"))) {
            component.set("v.isAddnewError", true);
            component.set("v.AddnewError", component.get(
                "v.accBillInfoError"));

        }

        helper.clearScreenHelper(component, event);

    },
    cancelAddNew: function(component, event, helper) {
        component.set("v.isMakepaymentError", false);
        component.set("v.isMakepaymentSuccess", false);
        component.set("v.isEditError", false);
        component.set("v.isAddnewError", false);
        helper.cancelAddNewHelper(component, event);
    },
    addNewCard: function(component, event, helper) {
        component.set("v.isMakepaymentError", false);
        component.set("v.isMakepaymentSuccess", false);
        component.set("v.isEditError", false);
        component.set("v.isAddnewError", false);
        var NameOnCard = component.find("NameOnCard");
        var CardNumber = component.find("CardNumber");
        var ExpiryMonth = component.find("ExpiryMonth");
        var ExpiryYear = component.find("ExpiryYear");
        NameOnCard.showHelpMessageIfInvalid();
        CardNumber.showHelpMessageIfInvalid();
        ExpiryMonth.showHelpMessageIfInvalid();
        ExpiryYear.showHelpMessageIfInvalid();
        if (NameOnCard.get("v.validity").valid && CardNumber.get(
                "v.validity").valid &&
            ExpiryMonth.get("v.validity").valid && ExpiryYear.get(
                "v.validity").valid) {
            helper.addNewCardHelper(component, event);
            //helper.cancelAddNewHelper(component, event);
        }
    },
    changeDefault: function(component, event, helper) {
        event.stopPropagation();
        var selectedItem = event.currentTarget;
        var index = selectedItem.dataset.index;
        var CardList = component.get("v.CardList");
        for (var i = 0; i < CardList.length; i++) {
            if (i == index) {
                CardList[i].IsDefaultCard = true;
                //helper.changeDefaultHelper(component, event);           
            } else {
                CardList[i].IsDefaultCard = false;
            }
        }
        component.set("v.CardList", CardList);
    },
    editCardInfo: function(component, event, helper) {
        component.set("v.isMakepaymentError", false);
        component.set("v.isMakepaymentSuccess", false);
        component.set("v.isEditError", false);
        component.set("v.isAddnew", false);
        component.set("v.isAddnewError", false);
        var selectedItem = event.currentTarget;
        var index = selectedItem.dataset.index;
        component.set("v.EditCard", component.get("v.CardList")[index]);
        component.set("v.isEditError", false);
        component.set("v.EditError", "");
        component.set("v.isEdit", true);
    },
    deleteCardInfo: function(component, event, helper) {
        component.set("v.isMakepaymentError", false);
        component.set("v.isMakepaymentSuccess", false);
        component.set("v.isEditError", false);
        component.set("v.isAddnewError", false);
        var selectedItem = event.currentTarget;
        var index = selectedItem.dataset.index;
        component.set("v.DeleteCard", component.get("v.CardList")[index]);
        component.set("v.isDeleteError", false);
        component.set("v.DeleteError", "");

        helper.deleteCardHelper(component, event);
    },
    saveEditedInfo: function(component, event, helper) {
        component.set("v.isMakepaymentError", false);
        component.set("v.isMakepaymentSuccess", false);
        component.set("v.isEditError", false);
        component.set("v.isAddnewError", false);
        helper.clearScreenHelper(component, event);
        var NameOnCardE = component.find("NameOnCardE");
        var CardNumberE = component.find("CardNumberE");
        var ExpiryMonthE = component.find("ExpiryMonthE");
        var ExpiryYearE = component.find("ExpiryYearE");
        NameOnCardE.showHelpMessageIfInvalid();
        CardNumberE.showHelpMessageIfInvalid();
        ExpiryMonthE.showHelpMessageIfInvalid();
        ExpiryYearE.showHelpMessageIfInvalid();
        if (NameOnCardE.get("v.validity").valid && CardNumberE.get(
                "v.validity").valid &&
            ExpiryMonthE.get("v.validity").valid && ExpiryYearE.get(
                "v.validity").valid) {
            helper.saveEditHelper(component, event);
            helper.cancelEditHelper(component, event);
        }
    },
    cancelEdit: function(component, event, helper) {
        component.set("v.isMakepaymentError", false);
        component.set("v.isMakepaymentSuccess", false);
        component.set("v.isEditError", false);
        component.set("v.isAddnewError", false);
        helper.cancelEditHelper(component, event);
    },
    // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for display loading spinner 
        component.set("v.flag", true);
    },

    // this function automatic call by aura:doneWaiting event 
    hideSpinner: function(component, event, helper) {
        // make Spinner attribute to false for hide loading spinner    
        component.set("v.flag", false);
    },
    cancel: function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
        $A.get('e.force:refreshView').fire();
    }
})