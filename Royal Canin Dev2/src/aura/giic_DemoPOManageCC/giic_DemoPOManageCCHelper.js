({
    onLoad : function(component, event) {
        var months = ['01','02','03','04','05','06','07','08','09','10','11','12'];
        component.set("v.months", months);
        
        var d = new Date();
        var years = [];
        
        for(var i = d.getFullYear(); i< d.getFullYear() + 16; i++){
            years.push(i);
        }
        component.set("v.years",years);
        var cardTypeValues = $A.get("$Label.c.giic_CardTypesAndValues");        
        var cardValueMap = {};
        var cardsList = [];
        var cards = cardTypeValues.split(";"); 
        for(var i =0; i < cards.length-1; i++){
            var cardtypevalue = cards[i].split("-->"); 
            cardsList.push(cardtypevalue[0].trim());
            cardValueMap[cardtypevalue[0].trim()] = cardtypevalue[1].trim();
        }
        component.set("v.cardValueMap", cardValueMap);
        component.set("v.cardsList", cardsList);
        var action = component.get('c.getCardList');
        action.setParams({
            'recordId' : component.get('v.recordId')
        });
        action.setCallback(this, function(response){
            var state = response.getState();            
            if(state === "SUCCESS"){
                var result = response.getReturnValue();
                if(result.isReleased || result.isCancelled){
                    var resultsToast = $A.get("e.force:showToast");
                    if(result.isReleased ){
                        resultsToast.setParams({
                            title: "Error",
                            message: result.isReleasedMsg,
                            type : "Error"
                        });
                    }
                    else{
                        resultsToast.setParams({
                            title: "Error",
                            message: result.isCancelledMsg,
                            type : "Error"
                        });
                        
                    }
                    resultsToast.fire();
                    $A.get("e.force:closeQuickAction").fire();
                }
                
                if(result.arListSize > 0){
                    component.set("v.arListSize", result.arListSize);
                    component.set("v.isError",true);
                    component.set("v.errorMessage",$A.get("$Label.c.giic_PaymentAlreadySettled")); 
                    return;
                }
                 if(result.paymentMethod != 'Credit Card'){
                    component.set("v.paymentMethodError", true);
                    component.set("v.isError",true);
                    component.set("v.errorMessage",$A.get("$Label.c.giic_CreditCardIsMust")); 
                    return;
                }
                
                component.set("v.TotalAmount",result.TotalAmount); 
               
                
                if(result.CardList.length > 0){                    
                    component.set("v.CardList",result.CardList);                    
                }else{
                    component.set("v.isError",true);
                    component.set("v.errorMessage","Cards Not Found."); 
                }
                
                component.set("v.NewCard",result.NewCard);
                component.set("v.NewCardDefault",result.NewCard);
                component.set("v.sObjectType", result.sObjectType);
                component.set("v.authorizedAmount", result.authorizedAmount);
                
                
                if ((!$A.util.isUndefined(result.accBillInfo) || !$A.util.isEmpty(result.accBillInfo)) &&
                    ($A.util.isUndefined(result.accBillInfoError) || $A.util.isEmpty(result.accBillInfoError))){
                    component.set("v.BillTo", result.accBillInfo);
                }
                else{
                    component.set("v.accBillInfoError", result.accBillInfoError);
                    //console.log("v.accBillInfoError : "+component.get("v.accBillInfoError"));
                }
               
            }else{
                let errors = response.getError();
                let message = 'Unknown error'; // Default error message
                // Retrieve the error message sent by the server
                if (errors && Array.isArray(errors) && errors.length > 0) {
                    message = errors[0].message;
                }
                // Display the message
                console.error(message);
                component.set("v.isError",true);
                component.set("v.errorMessage",message);
            }
        });
        
        $A.enqueueAction(action);
    },
    addNewCard: function(component, event) {
        this.clearScreenHelper(component,event);
        component.set("v.isAddnewError",false);
        component.set("v.AddnewError","");
        var NewCard = component.get("v.NewCard");
        var BillTo = component.get("v.BillTo");
        var action = component.get('c.addNewCreditCard');
        action.setParams({
            'newCard' : JSON.stringify(NewCard),
            'billToWrapper' : JSON.stringify(BillTo)
        });
        action.setCallback(this, function(response){
            var state = response.getState();            
            if(state === "SUCCESS"){
                var result = response.getReturnValue();
                if (!$A.util.isUndefined(result.error) || !$A.util.isEmpty(result.error)){
                    component.set("v.isAddnewError", true);
                    component.set("v.AddnewError", result.error);
                }
                //this.cancelAddNewHelper(component, event);
                /*var resultsToast = $A.get("e.force:showToast");
                    resultsToast.setParams({
                        title: "Success",
                        message: "Card Added Successfully.",
                        type : "Success"
                    });*/
                else{
                    //debugger;
                    //component.set("v.NewCard",component.get("v.NewCardDefault"));
                    component.set("v.isAddnew",false);
                    component.set("v.isEdit",false);
                    component.set("v.isError", false);
                    //component.set("v.CardList",result.CardList); 
                    this.onLoad(component,event);
                    //$A.get('e.force:refreshView').fire(); 
                }
                
            }else{
                let errors = response.getError();
                let message = 'Unknown error'; // Default error message
                // Retrieve the error message sent by the server
                if (errors && Array.isArray(errors) && errors.length > 0) {
                    message = errors[0].message;
                }
                // Display the message
                console.error(message);
                component.set("v.isAddnewError",true);
                component.set("v.AddnewError",message);
            }
        });
        
        $A.enqueueAction(action);
    },
    saveEditHelper: function(component, event) {
        this.clearScreenHelper(component,event);
        var EditCard = component.get("v.EditCard");
        var action = component.get('c.editCreditCardInfo');
        action.setParams({
            'editCard' : JSON.stringify(EditCard),
            'recId' : component.get("v.recordId")
        });
        action.setCallback(this, function(response){
            var state = response.getState();            
            if(state === "SUCCESS"){
                var result = response.getReturnValue();
                if (!$A.util.isUndefined(result.error) || !$A.util.isEmpty(result.error)){
                    component.set("v.isEditError", true);
                    component.set("v.EditError", result.error);
                }
                this.cancelAddNewHelper(component, event);
                /*var resultsToast = $A.get("e.force:showToast");
                    resultsToast.setParams({
                        title: "Success",
                        message: "Card Edited Successfully.",
                        type : "Success"
                    });*/
                component.set("v.CardList",result.CardList); 
                //$A.get('e.force:refreshView').fire(); 
                
            }else{
                let errors = response.getError();
                let message = 'Unknown error'; // Default error message
                // Retrieve the error message sent by the server
                if (errors && Array.isArray(errors) && errors.length > 0) {
                    message = errors[0].message;
                }
                // Display the message
                console.error(message);
                component.set("v.isEditError",true);
                component.set("v.EditError",message);
            }
        });
        
        $A.enqueueAction(action);
    },
    deleteCardHelper : function (component, event){
        this.clearScreenHelper(component,event);
        var DeleteCard = component.get("v.DeleteCard");
        var action = component.get('c.deleteCreditCardInfo');
        action.setParams({
            'deleteCard' : JSON.stringify(DeleteCard),
            'recId' : component.get("v.recordId")
        });
        action.setCallback(this, function(response){
            var state = response.getState();            
            if(state === "SUCCESS"){
                var result = response.getReturnValue();
                if (!$A.util.isUndefined(result.error) || !$A.util.isEmpty(result.error)){
                    component.set("v.isCCOpErrDel", true);
                    component.set("v.CCOpErrMsgDel", result.error);
                }
                this.cancelAddNewHelper(component, event);
                /*var resultsToast = $A.get("e.force:showToast");
                    resultsToast.setParams({
                        title: "Success",
                        message: "Card Deleted Successfully.",
                        type : "Success"
                    });*/
                component.set("v.CardList",result.CardList); 
                //$A.get('e.force:refreshView').fire(); 
                
            }else{
                let errors = response.getError();
                let message = 'Unknown error'; // Default error message
                // Retrieve the error message sent by the server
                if (errors && Array.isArray(errors) && errors.length > 0) {
                    message = errors[0].message;
                }
                // Display the message
                console.error(message);
                component.set("v.isCCOpErrDel",true);
                component.set("v.CCOpErrMsgDel",message);
            }
        });
        
        $A.enqueueAction(action);
    },
    
    cancelAddNewHelper : function(component, event){
        component.set("v.NewCard",component.get("v.NewCardDefault"));
        component.set("v.isAddnewError",false);
        component.set("v.AddnewError","");
        component.set("v.isAddnew",false);
        //component.set("v.isCCOpErr",false);
        //component.set("v.CCOpErrMsg","");
    },
    cancelEditHelper: function(component, event){
        component.set("v.isEditError",false);
        component.set("v.EditError","");
        component.set("v.isEdit",false);
    },
    makePaymentHelper: function(component, event, selectedCard){
        var action = component.get('c.makePaymentToCybersource');
        action.setParams({
            'selectedCard' : JSON.stringify(selectedCard),
            'recId' : component.get("v.recordId")
        });
        action.setCallback(this, function(response){
            var state = response.getState();            
            if(state === "SUCCESS"){
                var result = response.getReturnValue();
                component.set("v.CardList",result.CardList); 
                if(result.MakepaymentResponce != 'SUCCESS'){
                    component.set("v.isMakepaymentError",true);
                    component.set("v.MakepaymentError",result.MakepaymentResponce);
                }else{
                    $A.get("e.force:closeQuickAction").fire();
                    var resultsToast = $A.get("e.force:showToast");
                    resultsToast.setParams({
                        "title": "Success",
                        "message": "Payment Successfully Done.",
                        "type": "Success"
                    });                
                    resultsToast.fire();
                }
                
            }else{
                let errors = response.getError();
                let message = 'Unknown error'; // Default error message
                // Retrieve the error message sent by the server
                if (errors && Array.isArray(errors) && errors.length > 0) {
                    message = errors[0].message;
                }
                // Display the message
                console.error(message);
                component.set("v.isMakepaymentError",true);
                component.set("v.MakepaymentError",message);
            }
        });
        
        $A.enqueueAction(action);
    },
    
    authPaymentHelper: function(component, event, selectedCard){
                    var action = component.get('c.authorizePaymentToCybersource');
                    action.setParams({
                        'selectedCard' : JSON.stringify(selectedCard),
                        'recId' : component.get("v.recordId")
                    });
                    action.setCallback(this, function(response){
                        var state = response.getState();            
                        if(state === "SUCCESS"){
                         
                            var result = response.getReturnValue();
                            component.set("v.authorizedAmount", result.authorizedAmount);
                            component.set("v.CardList",result.CardList); 
                            if(result.MakepaymentResponce != 'SUCCESS'){
                                component.set("v.isMakepaymentError",true);
                                component.set("v.MakepaymentError",result.MakepaymentResponce);
                            }else{
                                /*component.set("v.isMakepaymentSuccess",true);
                                component.set("v.MakepaymentSuccess","Payment Authorized Successfully.");*/
                                $A.get("e.force:closeQuickAction").fire();
                                var resultsToast = $A.get("e.force:showToast");
                                resultsToast.setParams({
                                    "title": "Success",
                                    "message": "Payment Successfully Authorized.",
                                    "type": "Success"
                                });                
                                resultsToast.fire();
                                $A.get('e.force:refreshView').fire();
                            }
                            
                        }else{
                            let errors = response.getError();
                            let message = 'Unknown error'; // Default error message
                            // Retrieve the error message sent by the server
                            if (errors && Array.isArray(errors) && errors.length > 0) {
                                message = errors[0].message;
                            }
                            // Display the message
                            console.error(message);
                            component.set("v.isMakepaymentError",true);
                            component.set("v.MakepaymentError",message);
                        }
                    });
                    
                    $A.enqueueAction(action);
    },
    updateCardListHelper: function(component, event, CardList){
        var action = component.get('c.updateCardList');
        action.setParams({
            'strCardList' : JSON.stringify(CardList)            
        });
        action.setCallback(this, function(response){
            var state = response.getState();            
            if(state === "SUCCESS"){
                var result = response.getReturnValue();
                /* component.set("v.CardList",result.CardList); */
                if(result != 'SUCCESS'){
                    component.set("v.isMakepaymentError",true);
                    component.set("v.MakepaymentError",result);
                }else{
                    this.onLoad(component, event); 
                    /*component.set("v.isMakepaymentSuccess",true);
                    component.set("v.MakepaymentSuccess","Default Card Successfully Changed.");*/
                }
                
            }else{   
                let errors = response.getError();
                let message = 'Unknown error'; // Default error message
                // Retrieve the error message sent by the server
                if (errors && Array.isArray(errors) && errors.length > 0) {
                    message = errors[0].message;
                }
                // Display the message
                console.error(message);
                component.set("v.isMakepaymentError",true);
                component.set("v.MakepaymentError",message);
            }
        });
        
        $A.enqueueAction(action);
    },
    clearScreenHelper : function(component, event){
        component.set("v.isCCOpErrDel",false);
        component.set("v.CCOpErrMsgDel","");
    }
})