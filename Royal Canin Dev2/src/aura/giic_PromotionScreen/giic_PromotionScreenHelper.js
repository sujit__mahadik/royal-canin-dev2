({
    /******************************************************
     * funtion : submitOrder
     * Description - Apply auto promotions on sales order
     * *****************************************************/
	applyPromotions : function(component, event, helper) {
		 /*************************** Apply all the auto promotion on sales order ************************************/
        var action = component.get("c.applyAutoPromotion");
        
        /************************** Pass the Sales order Id and IsInit param to call the method ***********************/
        var mapRequest={};
        mapRequest["soId"] = component.get("v.recordId");
        mapRequest["isInit"] = true;
        action.setParams({"mapRequest" : mapRequest});
        
        /************************* Call the Server Side method***********************************************/
        action.setCallback(this,function(response){            
            var state = response.getState();  
            debugger;
            if (state === "SUCCESS") { 
                var mapResult = response.getReturnValue();
                component.set("v.soWrapper", mapResult["SOINFO"]);
                var orderSummarycmp = component.find("orderSummarycmp");
                orderSummarycmp.doInit(mapResult["SOINFO"]);
                
            }  
        });
        $A.enqueueAction(action);
	},
    /******************************************************
     * funtion : reloadComponents
     * Description - reload main and Order Summary component 
     * *****************************************************/
    reloadComponents : function(component, event, helper){
        var wrapTemp = event.getParams("soWrapper").soWrapper.SOINFO;
	    var orderSummarycmp = component.find("orderSummarycmp");
        var soWrapper = wrapTemp[0];
        orderSummarycmp.reFreshOrderSummary(soWrapper);  
        $A.get('e.force:refreshView').fire();
    }
})