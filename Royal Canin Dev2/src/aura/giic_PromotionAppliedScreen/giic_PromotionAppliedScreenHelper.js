({
    //Apply Manual Promotions
    applyManualPromotions : function(component, event, helper) {
        component.set("v.isLoading",true); // Set the loading screen
        /************************* Request Params*************************/
        var mapRequest={};
        mapRequest["SOIDS"]=component.get("v.soId");
        mapRequest["PROMOCODE"] =  component.get("v.promoCode");
        var action = component.get("c.applyPromotion");
        action.setParams({                    
            "mapRequest" : mapRequest
        });
        /****************************** Call the method to get the response ******************************/
        action.setCallback(this, function(response) { 
            var state = response.getState(); 
            var errorList = [];
            if (state === "SUCCESS") {
                debugger;
                var responseWrapper = response.getReturnValue();
                var sowrap = response.getReturnValue().SOINFO;
                var soWrapper = sowrap[0];
                var err = soWrapper.lstErrorInformation;
                if(typeof soWrapper.lstErrorInformation !== "undefined"){
                    for (var i = 0; i < soWrapper.lstErrorInformation.length; i++){
                        component.set("v.messageTitle",soWrapper.lstErrorInformation[i].errorMessage);
                        component.set("v.messageSeverity",'error');
                        errorList.push(soWrapper.lstErrorInformation[i].errorMessage);
                    }
                    console.log('errorList:' + errorList);
                    component.set("v.errorList",errorList);
                    component.set("v.hasMessage", true);
                    component.set("v.isLoading",false);
                }else
                {
                    component.set("v.hasMessage", false);
                    
                    /******************************* Call calculateTax method to get the Tax for Sales order from Avalara***********/
                    var caltax = component.get("c.calculateTax");
                    /***************************** Sales Order Id as Param **************************************/
                    caltax.setParams({"soId" : component.get("v.soId")});
                    
                    caltax.setCallback(this,function(response){ 
                        debugger;
                        var responseWrapperTax = response.getReturnValue();
                        //call event to refresh order summary page
                        // Get the component event by using the
                        // name value from aura:registerEvent
                        var cmpEvent = component.getEvent("refreshOrderSummaryEVT");
                        cmpEvent.setParams({
                            "soWrapper" : responseWrapperTax });
                        cmpEvent.fire();
                        component.set("v.promoCode",'');
                        component.set("v.isLoading",false);
                    });
                    $A.enqueueAction(caltax); 
                }
                
            }
            else
            {
                component.set("v.isLoading",false);
                errorList.push($A.get("$Label.c.giic_GenericErrorMessageforAuraEception"));
            }
        });
        $A.enqueueAction(action); 
    }
})