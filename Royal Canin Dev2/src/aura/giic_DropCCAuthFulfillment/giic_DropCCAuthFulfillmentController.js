({
    handleRecordUpdated: function(component, event, helper) {
        var eventParams = event.getParams();
        if (eventParams.changeType === "ERROR") {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title: 'ERROR',
                type: 'error',
                message: component.get("v.recordDataErr")
            });
            toastEvent.fire();
            $A.get("e.force:closeQuickAction").fire();
        } else if (eventParams.changeType === "LOADED") {
            helper.reverseAuth(component, event, helper);
        }
    },
    // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for display loading spinner 
        component.set("v.showSpinner", true);
    },

    // this function automatic call by aura:doneWaiting event 
    hideSpinner: function(component, event, helper) {
        // make Spinner attribute to false for hide loading spinner    
        component.set("v.showSpinner", false);
    }
})