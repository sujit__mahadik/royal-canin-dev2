({
    reverseAuth : function(component, event, helper) {
        var fulfillment = component.get("v.simpleRecord");
        console.log('fulfillment record: '+JSON.stringify(fulfillment));
        debugger;
        if(fulfillment.giic_SalesOrder__r.gii__PaymentMethod__c != $A.get("$Label.c.giic_PaymentMethodCreditCard")){
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title: 'ERROR',
                type: 'error',
                message:$A.get("$Label.c.giic_CreditCardIsMust")
            });                              
            toastEvent.fire();
            $A.get("e.force:closeQuickAction").fire();
        }
        else if(fulfillment.giic_IntegrationStatus__c != $A.get("$Label.c.giic_IntegrationSentToWMS")){
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                title: 'ERROR',
                type: 'error',
                message:$A.get("$Label.c.giic_SentToWMSMsg")
            });                              
            toastEvent.fire();
            $A.get("e.force:closeQuickAction").fire();            
        }
        else if(fulfillment.giic_Status__c == $A.get("$Label.c.giic_InProgress")
            || fulfillment.giic_Status__c == $A.get("$Label.c.giic_StatusInitial")){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title: 'ERROR',
                        type: 'error',
                        message:$A.get("$Label.c.giic_AmountAuthRevStatus")
                    });                              
                    toastEvent.fire();
                    $A.get("e.force:closeQuickAction").fire(); 
                }
                else{
                    debugger;
                    var submitAction = component.get('c.dropAmountAuth');
                    submitAction.setParams({
                        'fulfillmentId': component.get('v.recordId')
                    });
                    submitAction.setCallback(this, function(response) {
                        //debugger;
                        var state = response.getState();
                        console.log(state);
                        if (state === 'SUCCESS') {
                            console.log('Responce:: '+JSON.stringify(response.getReturnValue()));
                            var result = response.getReturnValue();
                            if($A.util.isUndefined(result.errorMsg)){
                                var toastEvent = $A.get("e.force:showToast");
                                toastEvent.setParams({
                                    title: 'SUCCESS',
                                    type: 'success',
                                    message:$A.get("$Label.c.giic_GenericAuthRevSuccess")
                                });                              
                                toastEvent.fire();
                            }else{
                                var errorMsg;
                                if(result.errorMsg != null) errorMsg = result.errorMsg;
                                else errorMsg = $A.get("$Label.c.giic_GenericAuthRevError");
                                var toastEvent = $A.get("e.force:showToast");
                                toastEvent.setParams({
                                    title: 'ERROR',
                                    type: 'error',
                                    message:errorMsg
                                });                              
                                toastEvent.fire();                   
                            } 
                            $A.get("e.force:closeQuickAction").fire();
                            $A.get('e.force:refreshView').fire();
                        }
                        else{
                            var errors = response.getError();
                            if (errors && Array.isArray(errors) && errors.length > 0) {
                                var toastEvent = $A.get("e.force:showToast");
                                toastEvent.setParams({
                                    title: 'ERROR',
                                    type: 'error',
                                    message: errors[0].message
                                });
                                toastEvent.fire();
                                $A.get("e.force:closeQuickAction").fire();
                                $A.get('e.force:refreshView').fire();
                            }
                        }
                    });
                    $A.enqueueAction(submitAction);
                }
                
            }
 
})