({
    submitSO : function(component, event, helper) {
        
        var caltax = component.get("c.CalculateTaxforSO");
        caltax.setParams({
            "soId" : component.get("v.recordId")
        });
        caltax.setCallback(this, function (response) {
            var state = response.getState();            
            if (state === "SUCCESS") 
            {
                var submitAction = component.get('c.submitSalesOrder');
                submitAction.setParams({
                    'soId': component.get('v.recordId')
                });
                
                submitAction.setCallback(this, function(response) {
                    var state = response.getState();
                    var isError=false;
                    if (state === 'SUCCESS') {
                        console.log(response.getReturnValue().split(","));
                        var toastEvent = $A.get("e.force:showToast");
                        
                        if(response.getReturnValue() == $A.get("$Label.c.giic_OrderPreReleased")){
                            toastEvent.setParams({
                                title: $A.get("$Label.c.giic_SalesOrderSubmissionError"),
                                type: 'error',
                                message: $A.get("$Label.c.giic_OrderPreReleased")
                            });
                            isError=true;
                        }
                        else if(response.getReturnValue() == $A.get("$Label.c.giic_CustomerAccountBlocked")){
                            toastEvent.setParams({
                                title: $A.get("$Label.c.giic_SalesOrderSubmissionError"),
                                type: 'error',
                                message: $A.get("$Label.c.giic_CustomerAccountBlocked")
                            });
                            isError=true;
                        }
                        else if(response.getReturnValue() == $A.get("$Label.c.giic_SalesOrderProcessingError")){
                            toastEvent.setParams({
                                title: $A.get("$Label.c.giic_SalesOrderSubmissionError"),
                                type: 'error',
                                message: $A.get("$Label.c.giic_SalesOrderProcessingError")
                            });
                            isError=true;
                        }else if(response.getReturnValue() == $A.get("$Label.c.giic_RouteOrderWeightValidationError")){
                            toastEvent.setParams({
                                title: $A.get("$Label.c.giic_SalesOrderSubmissionError"),
                                type: 'error',
                                message: $A.get("$Label.c.giic_RouteOrderWeightValidationError")
                            }); 
                            isError=true;
                        }else if(response.getReturnValue() == $A.get("$Label.c.giic_RouteOrderWeightValidationWarning")){
                            toastEvent.setParams({
                                title: $A.get("$Label.c.giic_RouteOrderWeightValidationWarningTitle"),
                                type: 'warning',
                                message: $A.get("$Label.c.giic_RouteOrderWeightValidationWarning")
                            });  
                        }else if(response.getReturnValue().includes($A.get("$Label.c.giic_TaxCalculationFailed")) || response.getReturnValue().includes($A.get("$Label.c.giic_ShipAddValidationFailed")) || response.getReturnValue().includes($A.get("$Label.c.giic_PromotionCalculationFailed")) || response.getReturnValue().includes($A.get("$Label.c.giic_PaymentAuthorizationFailed")) || response.getReturnValue().includes($A.get("$Label.c.giic_SOShippingFeeError"))){
                            toastEvent.setParams({
                                title: $A.get("$Label.c.giic_SalesOrderSubmissionError"),
                                type: 'error',
                                message: response.getReturnValue(),
                                duration: 7000
                                /*messageTemplate: 'Errors! <br> {0}',
                        messageTemplateData: response.getReturnValue().split(",")*/
                            }); 
                            isError=true;
                        }else if(response.getReturnValue().includes($A.get("$Label.c.giic_ExceptionStateError"))){
                            toastEvent.setParams({
                                title: $A.get("$Label.c.giic_SalesOrderSubmissionError"),
                                type: 'error',
                                message: response.getReturnValue()
                            });
                            isError=true;
                        }else if(response.getReturnValue() == $A.get("$Label.c.giic_OrderCancelled")){
                            toastEvent.setParams({
                                title: $A.get("$Label.c.giic_SalesOrderSubmissionError"),
                                type: 'error',
                                message: response.getReturnValue()
                            });
                            isError=true;
                        }
                        else{
                            toastEvent.setParams({
                                title: $A.get("$Label.c.giic_Submitted"),
                                type: 'success',
                                message: $A.get("$Label.c.giic_SalesOrderReleaseSuccess")
                            }); 
                        }
                        
                        toastEvent.fire();
                    }
                    else{
                        var errors = response.getError();
                        if (errors && Array.isArray(errors) && errors.length > 0) {
                            var toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams({
                                title: 'Error',
                                type: 'error',
                                message: errors[0].message
                            });
                            toastEvent.fire();
                            isError=true;
                        }
                        
                    }
                    $A.get("e.force:closeQuickAction").fire();
                    //$A.get('e.force:refreshView').fire();
                    if(isError)
                    	$A.get('e.force:refreshView').fire();
                    else
                    	window.parent.location = '/lightning/r/gii__SalesOrder__c/' + component.get('v.recordId') + '/view';
                });
                $A.enqueueAction(submitAction);
                
            }
        });
        $A.enqueueAction(caltax);       
    }
})