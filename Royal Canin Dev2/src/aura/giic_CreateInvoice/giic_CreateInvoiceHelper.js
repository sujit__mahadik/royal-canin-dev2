({
    createInvoice : function(component, event, helper) {
        var submitAction = component.get('c.createShipment');
        submitAction.setParams({
            'wHASId': component.get('v.recordId')
        });
        submitAction.setCallback(this, function(response) {
            //debugger;
            var state = response.getState();
            console.log(state);
            if (state === 'SUCCESS') {
                console.log('Responce:: '+JSON.stringify(response.getReturnValue()));
                var result = response.getReturnValue();
                if($A.util.isUndefined(result.errorMsg) && result.soNumber != null && result.shipDate != null){
                    this.createInvoiceFromShipment(component, event,result);
                }else{
                    var errorMsg;
                    if(result.errorMsg != null) errorMsg = result.errorMsg;
                    else errorMsg = $A.get("$Label.c.giic_ShipmentUIError"); //'Error occur in Shipment Creation. Please see error log';
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title: 'Invoice Create Error',
                        type: 'error',
                        message:errorMsg
                    });                              
                    toastEvent.fire();
                    $A.get("e.force:closeQuickAction").fire();
            		$A.get('e.force:refreshView').fire();
                }              
            }
            else{
                var errors = response.getError();
                if (errors && Array.isArray(errors) && errors.length > 0) {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title: 'Error',
                        type: 'error',
                        message: errors[0].message
                    });
                    toastEvent.fire();
                    $A.get("e.force:closeQuickAction").fire();
            		$A.get('e.force:refreshView').fire();
                }
            }
        });
        $A.enqueueAction(submitAction);
    },
    
    createInvoiceFromShipment : function(component, event, result){
        //debugger;
        var mapRequest = {};
        if(!$A.util.isUndefinedOrNull(result.recId) && !$A.util.isUndefinedOrNull(result.soNumber) && !$A.util.isUndefinedOrNull(result.shipDate) && !$A.util.isUndefinedOrNull(result.fulfilmentNumber)){
            mapRequest["recId"] =  result.recId;
            mapRequest["soNumber"] =  result.soNumber;
            mapRequest["shipDate"] =  result.shipDate;
            mapRequest["fulfilmentNumber"] =  result.fulfilmentNumber;
        }
        var submitAction = component.get('c.createInvoiceFromShipment');
        submitAction.setParams({
            'strmapRequest': JSON.stringify(mapRequest),
            'strmapFulfilmentLines': JSON.stringify(result.mapFulfilmentLines) 
        });
        
        submitAction.setCallback(this, function(response) {
            debugger;
            var state = response.getState();
            console.log(state);
            if (state === 'SUCCESS') {
                console.log('Responce:: '+JSON.stringify(response.getReturnValue()));
                console.log(response);
                var toastEvent = $A.get("e.force:showToast");
                if(response.getReturnValue() == true){
                    toastEvent.setParams({
                        title: 'Invoice Create Success',
                        type: 'success',
                        message: $A.get("$Label.c.giic_InvoiceUISuccess") //'Invoice Created successfully'
                    });
                }
                else if(response.getReturnValue() == false){
                    toastEvent.setParams({
                        title: 'Invoice Create Error',
                        type: 'error',
                        message: $A.get("$Label.c.giic_InvoiceUIError") //'Error occur in Invoice Creation. Please see error log'
                    });
                }                
                toastEvent.fire();
                $A.get("e.force:closeQuickAction").fire();
            	$A.get('e.force:refreshView').fire();
            }
            else{
                var errors = response.getError();
                if (errors && Array.isArray(errors) && errors.length > 0) {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title: 'Error',
                        type: 'error',
                        message: errors[0].message
                    });
                    toastEvent.fire();
                    $A.get("e.force:closeQuickAction").fire();
            		$A.get('e.force:refreshView').fire();
                }
            }            
        });
        $A.enqueueAction(submitAction);
    }
})