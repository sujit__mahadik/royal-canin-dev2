({
    doInit : function(component) {
        console.log(component.get("v.recordId"));
        var createShipment = component.get("c.convertShipmentOrder");
        createShipment.setParams({
            "recordId" : component.get("v.recordId")
        });
        
        createShipment.setCallback(this, function (response) {
            var state = response.getState();
            console.log(response.getReturnValue());
            var toastEvent = $A.get("e.force:showToast");            
            if (state === "SUCCESS") 
            {    
                    toastEvent.setParams({
                        title: "Shipment Created",
                        type: 'success',
                        message: $A.get("$Label.c.giic_ShipmentCreated")
                    });
                
            }else{
            
            let errors = response.getError();
            let message = 'Unknown error'; // Default error message
            // Retrieve the error message sent by the server
            if (errors && Array.isArray(errors) && errors.length > 0) {
                message = errors[0].message;
                } 
               toastEvent.setParams({
                        title: $A.get("$Label.c.giic_ShipmentError"),
                        type: 'Error',
                        message: message
                    });
                
            }
            $A.get("e.force:closeQuickAction").fire();
            toastEvent.fire();
        });
        
        $A.enqueueAction(createShipment);
    }
})