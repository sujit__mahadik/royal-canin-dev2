({
    initLabels : function(component, event){
        //Server side action to get the field labels
        var labelAction = component.get('c.getFieldLabels');
        labelAction.setParams({
            'recId' : component.get('v.recordId')
        });
        
        //Async response handler
        labelAction.setCallback(this, function(serverResponse){
            var state = serverResponse.getState();
            if(state === "SUCCESS"){
                component.set('v.fieldLabels', serverResponse.getReturnValue());
                console.log(component.get('v.fieldLabels'));
            }else{
                component.set("v.labelInitFail", true);
            }
        });
        
        //Response handler
        $A.enqueueAction(labelAction);
    },
    
    initFieldValues : function(component, event){
        // Prepare the action to load account record
        var action = component.get("c.getFieldValues");
        action.setParams({"recId": component.get("v.recordId")});
        
        // Configure response handler
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                var result = response.getReturnValue();
                if(result.sOIsReleased || result.sOIsCancelled){
                	component.set("v.IsReleased", true);
                    var resultsToast = $A.get("e.force:showToast");
                    resultsToast.setParams({
                        "title": "Error",
                        "message": result.sOMessage,
                        "type": "error"
                    });
                    resultsToast.fire();
                    $A.get("e.force:closeQuickAction").fire();
                }
                component.set("v.fieldValues", response.getReturnValue());
            } else {
                component.set("v.valueInitFail", true);
            }
        });
        
        //Response handler
        $A.enqueueAction(action);
    },
    
    validateForm: function(component) {
        // Show error messages if required fields are blank
        var validity = [];
        validity.push(component.find('ShippingStreet'));
        validity.push(component.find('ShippingCity'));
        validity.push(component.find('ShippingState'));
        validity.push(component.find('ShippingPostalCode'));
        validity.push(component.find('ShippingCountry'));
        var allValid = validity.reduce(function (validFields, inputCmp) {
            inputCmp.showHelpMessageIfInvalid();
            return validFields && inputCmp.get('v.validity').valid;
        }, true);
        
        if (allValid) {
            return true;
        }
        else{
            return false;
        }
    },
    
    getData : function(component,event){
        var addVarList = [];
        var objwrap ={};
        var returnedfieldValues = component.get("v.fieldValues");
        objwrap.StreetLines = component.find("ShippingStreet").get("v.value");
        objwrap.City = component.find("ShippingCity").get("v.value");
        objwrap.PostalCode = component.find("ShippingPostalCode").get("v.value");
        objwrap.CountryName = component.find("ShippingCountry").get("v.value");
        objwrap.ClientReferenceId = returnedfieldValues.sOClientRefId;
        
        addVarList.push(objwrap);
        var action = component.get("c.initAddressToValidate");
        
        action.setParams({	
            "strlstAddress": JSON.stringify(addVarList),
            "recId" : component.get("v.recordId")
        });
        
        // Configure response handler
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                //debugger;
                component.set("v.validationResponse", response.getReturnValue());
                var responseResult = component.get("v.validationResponse");
                console.log(':: Response Result :: '+JSON.stringify(responseResult));
                if ($A.util.isUndefined(responseResult) || $A.util.isEmpty(responseResult)
                    || responseResult.Resolved === false){
                    component.set("v.noServerResponse",true);
                    console.log('Server returned no value'); 
                    return;
                }
                else{
                    component.set("v.showResponseSection",true);
                    var effectiveAddressArray = [];
                    for(var i = 0; i < responseResult.length; i++){
                        effectiveAddressArray.push(responseResult[i].EffectiveAddress);
                    }
                    component.set("v.effectiveAddress", effectiveAddressArray);
                }
                
            } else {
                component.set("v.noServerResponse",true);
                var errorAction = component.get("c.handleError");
                var errInstance = component.get('v.fieldLabels');
                //handleError(String recId, String userStory, String code, String message)
                errorAction.setParams({	
                    "recId" : component.get("v.recordId"),
                    "userStory" : errInstance.stateError,
                    "code" : errInstance.stateError,
                    "message" :  $A.get("$Label.c.giic_FedExStateErrorMsg") + state
                });
                errorAction.setCallback(this, function(response) {
                    if(state === "SUCCESS") {
                       component.set("v.errorIds", response.getReturnValue()); 
                    }
                });
                $A.enqueueAction(errorAction);
                console.log( $A.get("$Label.c.giic_FedExStateErrorMsg") + state);
                //resultsToast.fire();
            }
        });
        //Response handler
        $A.enqueueAction(action);
    },
    
    updateRecord : function(component, event, selectedRows){
        var action = component.get("c.updateRecord");
        //console.log(selectedRows.City);
        action.setParams({	
            "sAsddressToUpdate" : JSON.stringify( selectedRows),
            "recordId" : component.get("v.recordId")
        });
        // Configure response handler
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                var action_2 = component.get("c.CalculateTax");
                action_2.setParams({
                    "recordId" : component.get("v.recordId")
                });
                action_2.setCallback(this, function (response) {
                    var state = response.getState();
                    
                    if (state === "SUCCESS") {
                    }
                });
                $A.enqueueAction(action_2);
                
                var message = response.getReturnValue();
                if(message === "SUCCESS"){
                    var resultsToast = $A.get("e.force:showToast");
                    resultsToast.setParams({
                        title: "SUCCESS",
                        message: $A.get("$Label.c.giic_RecordUpdateMessage"),
                        type: "success"
                    });
                    console.log('SERVER' + message);
                    resultsToast.fire();
                    
                    $A.get('e.force:refreshView').fire();
                    $A.get("e.force:closeQuickAction").fire();
                    window.parent.location = '/lightning/r/gii__SalesOrder__c/' + component.get("v.recordId") + '/view';
                }
                else{
                    /*var resultsToast = $A.get("e.force:showToast");
                    resultsToast.setParams({
                        title: "ERROR",
                        message: $A.get("$Label.c.giic_RecordUpdateFail")+message,
                        type : "error"
                    });*/
                    component.set("v.updateFail", true);
                    console.log('SERVER' + message);
                    //resultsToast.fire();
                    
                }
            }
        });
        //Response handler
        $A.enqueueAction(action);
    }
    
})