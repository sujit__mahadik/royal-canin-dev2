({
    doInit : function(component, event, helper) {
        
        helper.initLabels(component, event);
        
        helper.initFieldValues(component, event);
        
    },
    callValidation: function(component, event, helper) {
        if(helper.validateForm(component)) {
            //Remove all error message and initialization from previous operation
            component.set("v.noServerResponse",false);
            component.set("v.updateFail", false);
            component.set("v.labelInitFail",false);
            component.set("v.valueInitFail", false);
            component.set("v.showResponseSection",false);
            
            helper.getData(component,event);
        } 
    },

    handleSelect : function(component, event, helper) {
		var selectedRows = [];
        var index = event.getSource().get("v.name");  
        var allAddress=component.get("v.effectiveAddress");
        selectedRows.push(allAddress[index]);
        console.log(selectedRows);
        //debugger;
        helper.updateRecord(component, event, selectedRows);  
    },

    handleCancelBase: function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    },
    
    updateThisAddress : function(component, event, helper){
        var addList = [];
        var addressToUpdate = {};
        addressToUpdate.StreetLines = component.find("ShippingStreet").get("v.value");;
        addressToUpdate.City = component.find("ShippingCity").get("v.value");
        addressToUpdate.StateOrProvinceCode = component.find("ShippingState").get("v.value");
        addressToUpdate.PostalCode = component.find("ShippingPostalCode").get("v.value");
        addressToUpdate.CountryCode = component.find("ShippingCountry").get("v.value");
        addressToUpdate.Classification = 'Business';
        addList.push(addressToUpdate);
        
        helper.updateRecord(component, event, addList); 
        
    },
    navigateToError : function(component, event, helper) {
        var recIds  = component.get("v.errorIds");
        var sObectEvent = $A.get("e.force:navigateToSObject");
        sObectEvent .setParams({
            "recordId": recIds[0],
            "slideDevName": "detail"
        });
        sObectEvent.fire();
        
    },
    // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for display loading spinner 
        component.set("v.flag", true); 
    },
    
    // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hide loading spinner    
        component.set("v.flag", false);
    }
})