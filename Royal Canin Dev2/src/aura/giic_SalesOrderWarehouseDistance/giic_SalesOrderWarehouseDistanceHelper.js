({
    helperInit : function(component) { 
        var sOrderId  = component.get("v.recordId");
        console.log(sOrderId);
        var action = component.get("c.getData");
         action.setParams({  
            "salesOrderId" : sOrderId
        });       
        action.setCallback(this, function(response) {
            var state = response.getState();
            //console.log("--response--"+JSON.stringify(response.getState()));
            if (state === "SUCCESS") {
                var returnItems = response.getReturnValue();
                //console.log("response-->"+JSON.stringify(returnItems));
                component.set("v.accountWarehouseWrapper", returnItems);
                
            }else{
                console.log("!!error!!"+state);
            }   
        });
        $A.enqueueAction(action);  
    }
    
})