({
    onLoad : function(component, event) {
        component.set("v.show",true);
        component.set("v.totalAmountReturn",0);
        var recordId = this.getUrlParameter('c__id');
        if ($A.util.isUndefined(recordId) || $A.util.isEmpty(recordId)){
            showToast('Alert!','Please select valid record.','warning');
            return;
        }
        component.set("v.recordId", recordId);
        component.find("Id_spinner").set("v.class" , 'slds-show');
        var action = component.get('c.getProductList');
        action.setParams({
            'recordId' : recordId,
            'searchKeyword': ''            
        });
        action.setCallback(this, function(response){
            component.find("Id_spinner").set("v.class" , 'slds-hide');
            var state = response.getState();            
            if(state === "SUCCESS"){
                var result = response.getReturnValue();
                if(!$A.util.isUndefined(result.ApprovalStatus) && result.ApprovalStatus == 'Pending')
                {            
                    component.set("v.lstReturnReason", result.lstReturnReason);
                    component.set("v.productList", result.productList); 
                    component.set("v.SOPayment", result.SOPayment); 
                    var lstSOPayment = component.get("v.SOPayment");
                    var soWithAmount={};
                    
                    for (var i = 0; i < lstSOPayment.length; i++) 
                    {                 
                        var amountInfo={};
                        amountInfo.other=0;
                        amountInfo.reward=0;
                        amountInfo.rewardtotal=0;                    
                        amountInfo.total=0;
                        if(lstSOPayment[i].paymentMethod == $A.get("$Label.c.giic_PaymentMethodRewardPoints"))
                        {
                            amountInfo.reward=lstSOPayment[i].paymentAmount;
                            amountInfo.rewardtotal=lstSOPayment[i].paymentAmount;
                        }
                        else
                        {
                            amountInfo.other= lstSOPayment[i].paymentAmount;
                            amountInfo.total= lstSOPayment[i].paymentAmount;
                        }                   
                        if(!$A.util.isUndefined(soWithAmount[lstSOPayment[i].invid]) )
                        {
                            if(lstSOPayment[i].paymentMethod == $A.get("$Label.c.giic_PaymentMethodRewardPoints"))
                            {
                                soWithAmount[lstSOPayment[i].invid].reward= soWithAmount[lstSOPayment[i].invid].reward+amountInfo.reward;
                                soWithAmount[lstSOPayment[i].invid].rewardtotal= soWithAmount[lstSOPayment[i].invid].rewardtotal+amountInfo.rewardtotal;
                            }
                            else
                            {
                                soWithAmount[lstSOPayment[i].invid].other= soWithAmount[lstSOPayment[i].invid].other+amountInfo.other;
                                soWithAmount[lstSOPayment[i].invid].total= soWithAmount[lstSOPayment[i].invid].total+amountInfo.total;
                            }
                        }
                        else
                        {
                            soWithAmount[lstSOPayment[i].invid] = amountInfo ;   
                        }
                        
                    }  
                    component.set("v.soWithAmount", soWithAmount);    
                    var paymentMethod={};
                    for (var i = 0; i < result.lstPaymentMethod.length; i++) 
                    {
                        paymentMethod[result.lstPaymentMethod[i].Name] = result.lstPaymentMethod[i].giic_CreditSequence__c;
                    }
                    component.set("v.paymentMethod", paymentMethod);                           
                    component.set("v.AccountName", result.AccountName);
                    var additionalChargeList = new Array();
                    component.set("v.additionalChargeList", additionalChargeList);
                    component.set("v.showErrPanel", true);
                }
                else
                {
                    component.set("v.show",false);
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({                       
                        title: 'You cannot add return product to case, as the case is either in approval process or approved/rejected.',
                        type: 'error',
                        message: 'Click on Back button to move back to Case.'
                    });
                    toastEvent.fire();
                }
            }
            else
            {
                let errors = response.getError();
                let message = 'Unknown error'; // Default error message
                // Retrieve the error message sent by the server
                if (errors && Array.isArray(errors) && errors.length > 0) {
                    message = errors[0].message;
                }
                // Display the message
                component.set("v.isError",true);
                component.set("v.errorMessage",message);
            }
        });
        
        $A.enqueueAction(action);
    },
    getUrlParameter : function(sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;
        
        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');
            
            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    },
    SearchHelper : function(component, event) {
        component.find("Id_spinner").set("v.class" , 'slds-show');
        var recordId = component.get('v.recordId');
        var searchKeyword = component.get('v.searchKeyword');
        var action = component.get('c.getProductList');
        action.setParams({
            'recordId' : recordId,
            'searchKeyword': searchKeyword
        });
        action.setCallback(this, function(response){
            component.find("Id_spinner").set("v.class" , 'slds-hide');
            var state = response.getState();            
            if(state === "SUCCESS"){
                var result = response.getReturnValue();
                component.set("v.lstReturnReason", result.lstReturnReason);
                component.set("v.productList", result.productList);
                
            }else{
                let errors = response.getError();
                let message = 'Unknown error'; // Default error message
                // Retrieve the error message sent by the server
                if (errors && Array.isArray(errors) && errors.length > 0) {
                    message = errors[0].message;
                }
                // Display the message
                component.set("v.isError",true);
                component.set("v.errorMessage",message);
            }
        });
        
        $A.enqueueAction(action);
    },
    calTotalReturnAmnt : function(component, event){
        var productList = component.get('v.productList');
        var totalAmountReturn = 0;
        for(var i =0; i < productList.length; i++ ){
            if(productList[i].isSelected){
                totalAmountReturn += productList[i].returnAmount;  
            }
        }
        
        var additionalChargeList = component.get("v.additionalChargeList");
        for(var i =0; i < additionalChargeList.length; i++ ){
            if(additionalChargeList[i].isSelected){
                totalAmountReturn += additionalChargeList[i].additionalChargeAmount;  
            }
        }
        component.set('v.totalAmountReturn', totalAmountReturn);    
    },
    handleSelectHelper : function(component, event){
        var index =  event.getSource().get('v.labelClass');
        var productList = component.get('v.productList');
        var objIPW = productList[index];

        var additionalChargeList = component.get("v.additionalChargeList");
        if(objIPW.isSelected == true){
            var flag = true;
            for(var i=0; i < additionalChargeList.length; i++){
                var objAC = additionalChargeList[i];
                if(objAC.invoiceId == objIPW.invoiceId) {flag = false; break;}   
            }
            if(flag){
                var action = component.get('c.getInvoiceAdditionalCharge');
                action.setParams({
                    'invId' : objIPW.invoiceId
                    //,'strInvACList': JSON.stringify(component.get('v.additionalChargeList'))
                });
                action.setCallback(this, function(response){
                    component.find("Id_spinner").set("v.class" , 'slds-hide');
                    var state = response.getState();            
                    if(state === "SUCCESS"){
                        var result = response.getReturnValue();
                        if(result.additionalChargeList.length>0){  
                            additionalChargeList.push(result.additionalChargeList[0]);
                            component.set("v.additionalChargeList", additionalChargeList);
                        }
                    }else{
                        let errors = response.getError();
                        let message = 'Unknown error'; // Default error message
                        // Retrieve the error message sent by the server
                        if (errors && Array.isArray(errors) && errors.length > 0) {
                            message = errors[0].message;
                        }
                        // Display the message
                        component.set("v.isError",true);
                        component.set("v.errorMessage",message);
                    }
                });        
                $A.enqueueAction(action); 
            }
        }else if(objIPW.isSelected == false){
            objIPW.returnAmount = 0;
            objIPW.returnQty = 0;
            var removeflag = true;
            for(var i=0; i < productList.length; i++){
                var objProd = productList[i];
                if(objIPW.invoiceId == objProd.invoiceId && objProd.isSelected == true) {removeflag = false; break;}   
            }
            if(removeflag == true){
                for( var i = additionalChargeList.length-1; i>= 0; i--){
                	var objAC = additionalChargeList[i];
                	if(objAC.invoiceId == objIPW.invoiceId) additionalChargeList.splice(i, 1);
                }
                component.set("v.additionalChargeList", additionalChargeList);
            }
            component.set('v.productList', productList);
        }
        
        this.calTotalReturnAmnt(component, event);
    }, 
    handleSelectForACHelper : function(component, event){
        this.calTotalReturnAmnt(component, event);
    },
    changeUOMHelper: function(component, event, product){
        component.find("Id_spinner").set("v.class" , 'slds-show');
        var index =  event.getSource().get('v.labelClass');
        var productList = component.get('v.productList');
        var objIPW = productList[index];
        var action = component.get('c.getProductwithChangedUOM');
        action.setParams({
            'strWrapper' : JSON.stringify(objIPW)           
        });
        action.setCallback(this, function(response){
            component.find("Id_spinner").set("v.class" , 'slds-hide');
            var state = response.getState();            
            if(state === "SUCCESS"){
                var result = response.getReturnValue();
                productList[index] =  result.productWrapper;
                component.set('v.productList', productList);
            }else{
                let errors = response.getError();
                let message = 'Unknown error'; // Default error message
                // Retrieve the error message sent by the server
                if (errors && Array.isArray(errors) && errors.length > 0) {
                    message = errors[0].message;
                }
                // Display the message
               // console.error(message);
                component.set("v.isError",true);
                component.set("v.errorMessage",message);
            }
        });
        
        $A.enqueueAction(action);
    },
    saveHelper : function(component, event){
        var recordId = component.get('v.recordId');
        var productList = component.get('v.productList');
        var additionalChargeList = component.get('v.additionalChargeList');
        var action = component.get('c.saveReturnList');
        action.setParams({
            'recordId' : recordId,
            'strProdList' : JSON.stringify(productList),
            'strInvACList' : JSON.stringify(additionalChargeList)  
        });
        action.setCallback(this, function(response){
            component.find("Id_spinner").set("v.class" , 'slds-hide');
            var state = response.getState();            
            if(state === "SUCCESS"){
                var result = response.getReturnValue();
                this.goBackHelper(component, event);
                $A.get('e.force:refreshView').fire();
            }else{
                let errors = response.getError();
                let message = 'Unknown error'; // Default error message
                // Retrieve the error message sent by the server
                if (errors && Array.isArray(errors) && errors.length > 0) {
                    message = errors[0].message;
                }
                // Display the message
               // console.error(message);
                component.set("v.isError",true);
                component.set("v.errorMessage",message);
            }
        });
        
        $A.enqueueAction(action);
    },
    
    goBackHelper : function(component, event){
        var recordId = component.get('v.recordId');
        var sObectEvent = $A.get("e.force:navigateToSObject");
        sObectEvent .setParams({
            "recordId": recordId,
            "slideDevName": "detail"
        });
        sObectEvent.fire(); 
        $A.get('e.force:refreshView').fire();
    },
    showToast : function(strTitle, strMessage, strType){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : strTitle,
            message: strMessage,
            duration:' 5000',
            type: strType,//'info',
            mode: 'dismissible'
        });
        toastEvent.fire();
    },
})