({
    doInit : function(component, event, helper) {
        helper.onLoad(component, event);
    },    
    Search: function(component, event, helper) {
        var searchField = component.find('searchField');
        var isValueMissing = searchField.get('v.validity').valueMissing;
        // if value is missing show error message and focus on field
        if(isValueMissing) {
            searchField.showHelpMessageIfInvalid();
            searchField.focus();
        }else{
            // else call helper function 
            helper.SearchHelper(component, event);
        }
    },
    calculateReturnAmount:function(component, event, helper) {        
        var index =  event.getSource().get('v.labelClass');
        var productList = component.get('v.productList');
        productList[index].isSelected = productList[index].isSelected ? true : false;
        if(productList[index].returnQty > productList[index].qtyAvailableForReturn){            
            productList[index].returnQty = 0;
            productList[index].returnAmount = 0;
            component.set('v.productList', productList);
            helper.showToast('Alert!','Quantity is not available for return.','warning');
            //alert('Quantity is not available for return.');
            return;
        }
        
        productList[index].returnAmount = (productList[index].invoiceAmountStocking / productList[index].orderQuantity) * productList[index].returnQty;
        productList[index].isSelected = productList[index].returnAmount ==0 ? false : true;
        component.set('v.productList', productList);
        helper.handleSelectHelper(component, event);
        
    },
    
    changeUOM :function(component, event, helper) {
        helper.changeUOMHelper(component, event);
    },
    handleSelect : function(component, event, helper) {
        helper.handleSelectHelper(component, event);
    },
    handleSelectForAC : function(component, event, helper) {
        helper.handleSelectForACHelper(component, event);
    },
    save : function(component, event, helper) {
        var errFlag = false;
        var productList = component.get('v.productList');
        for(var i =0; i < productList.length; i++ ){
            if(productList[i].isSelected == true && productList[i].returnReason == '--None--'){
                errFlag = true;  break;  
            }
        }
        if(errFlag){
            helper.showToast('Alert!','Please select reason.','warning');    
        }else{
            var totalAmountReturn = component.get('v.totalAmountReturn');
            if(totalAmountReturn > 0){
                component.find("Id_spinner").set("v.class" , 'slds-show');
                helper.saveHelper(component, event);
            }else{
                helper.showToast('Alert!','Total return amount can not be zero.','warning');  
                return;
            }
        }
    }, 
    back : function(component, event, helper) {
        helper.goBackHelper(component, event);
    }, 
    goback : function(component, event, helper) {
         var recordId = component.get('v.recordId');
        console.log('recordId::' + recordId);
        var sObectEvent = $A.get("e.force:navigateToSObject");
        sObectEvent .setParams({
            "recordId": recordId,
            "slideDevName": "detail"
        });
        sObectEvent.fire(); 
    },   
})