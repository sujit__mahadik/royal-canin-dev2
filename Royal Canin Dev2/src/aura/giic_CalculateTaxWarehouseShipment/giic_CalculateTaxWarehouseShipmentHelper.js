({
    updateSOhelper : function(component, event, helper) {
        
        var submitAction = component.get('c.updateSOLandIR');
        submitAction.setParams({
            'wHASId': component.get('v.recordId')
        });
         submitAction.setCallback(this, function(response) {
             
             var state = response.getState();
             console.log(state);
             if (state === 'SUCCESS') {
                 var result =  response.getReturnValue();
                 console.log('reslt:' + JSON.stringify(result));
                 if($A.util.isUndefinedOrNull(result.ERROR) == false)
                {
                    var toastEvent = $A.get("e.force:showToast");
                    var msg = result.ERROR ;
                    var title = $A.get("$Label.c.giic_TaxCalError");
                    var type = 'error';
                    
                     toastEvent.setParams({
                        title: title,
                        type: type,
                        message: msg
                    });
                    toastEvent.fire();
                    $A.get("e.force:closeQuickAction").fire();
                    $A.get('e.force:refreshView').fire();
                
                }
                else
                {
                    var record = component.get("v.simpleRecord");
                    if(!record.giic_isTaxCommitted__c)
                        this.calculateTax(component, event, helper);
                }
             } else{
                
                var errors = response.getError();
                if (errors && Array.isArray(errors) && errors.length > 0) {
                    console.log('---error----' + errors);
    				var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title: 'Error',
                        type: 'error',
                        message: errors[0].message
                    });
                    toastEvent.fire();
                }
                $A.get("e.force:closeQuickAction").fire();
                $A.get('e.force:refreshView').fire();
            }
         });
         $A.enqueueAction(submitAction);

    },

	calculateTax : function(component, event, helper) {
	    console.log('In calculateTax');
		var submitAction = component.get('c.CalculateTax');
        submitAction.setParams({
            'wHASId': component.get('v.recordId')
        });
        
        submitAction.setCallback(this, function(response) {
            var state = response.getState();
            console.log(state);
            if (state === 'SUCCESS') {
                var result =  response.getReturnValue();
                var toastEvent = $A.get("e.force:showToast");
                var msg ;
                var title;
                var type;
                if( !$A.util.isUndefinedOrNull(result.ERROR ))
                {
                    msg = result.ERROR ;
                    title = $A.get("$Label.c.giic_TaxCalError");
                    type = 'error';
                }
                else
                {
                    msg = $A.get("$Label.c.giic_TaxCalSuccess");
                    title = 'Tax Calulate Success';
                    type = 'success';
                 
                }
                toastEvent.setParams({
                        title: title,
                        type: type,
                        message: msg
                    });
                
                toastEvent.fire();
                 $A.get("e.force:closeQuickAction").fire();
                $A.get('e.force:refreshView').fire();
            }
            else{
                
                var errors = response.getError();
                console.log('---error----' + errors);
                if (errors && Array.isArray(errors) && errors.length > 0) {
    				var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title: 'Error',
                        type: 'error',
                        message: errors[0].message
                    });
                    toastEvent.fire();
                }
                 $A.get("e.force:closeQuickAction").fire();
                $A.get('e.force:refreshView').fire();
            }
        });
        $A.enqueueAction(submitAction);
		
	}
})