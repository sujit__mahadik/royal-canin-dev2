({
	doInit : function(component, event, helper) {
		helper.updateSOhelper(component, event, helper);
	},
   handleRecordUpdated: function(component, event, helper) {
        var eventParams = event.getParams();
        if(eventParams.changeType === "LOADED") {
           // record is loaded (render other component which needs record data value)
            console.log("Record is loaded successfully.");
           
        }
        else if(eventParams.changeType === "ERROR"){
            console.log("LDS error!");
            console.log(component.get("v.recordError"));
        }
    } 
    
})