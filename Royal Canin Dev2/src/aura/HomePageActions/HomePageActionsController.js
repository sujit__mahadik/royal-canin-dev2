({
	goToURL : function(component, event, helper) {
		var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({                
            "url": "/new-sales-order",
            "isredirect":true
        });
        urlEvent.fire(); 	
	},
    goToCase: function(component, event, helper) {
    var createRecordEvent = $A.get("e.force:createRecord");
    createRecordEvent.setParams({
        "entityApiName": "Case"
    });
    createRecordEvent.fire();
    }
})