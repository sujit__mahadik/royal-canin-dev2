({
	authAmount : function(component, event, helper) {
        var submitAction = component.get('c.authAmountToRccc');
        submitAction.setParams({
            'fulfillmentId': component.get('v.recordId')
        });
        submitAction.setCallback(this, function(response) {
            //debugger;
            var state = response.getState();
            console.log(state);
            if (state === 'SUCCESS') {
                console.log('Responce:: '+JSON.stringify(response.getReturnValue()));
                var result = response.getReturnValue();
                if($A.util.isUndefined(result.errorMsg)){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title: 'SUCCESS',
                        type: 'success',
                        message:$A.get("$Label.c.giic_AmountAuthSuccess")
                    });                              
                    toastEvent.fire();
                }else{
                    var errorMsg;
                    if(result.errorMsg != null) errorMsg = result.errorMsg;
                    else errorMsg = $A.get("$Label.c.giic_AmountAuthError");
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title: 'ERROR',
                        type: 'error',
                        message:errorMsg
                    });                              
                    toastEvent.fire();                   
                } 
                $A.get("e.force:closeQuickAction").fire();
                $A.get('e.force:refreshView').fire();
            }
            else{
                var errors = response.getError();
                if (errors && Array.isArray(errors) && errors.length > 0) {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        title: 'ERROR',
                        type: 'error',
                        message: errors[0].message
                    });
                    toastEvent.fire();
                    $A.get("e.force:closeQuickAction").fire();
            		$A.get('e.force:refreshView').fire();
                }
            }
        });
        $A.enqueueAction(submitAction);
    },
})