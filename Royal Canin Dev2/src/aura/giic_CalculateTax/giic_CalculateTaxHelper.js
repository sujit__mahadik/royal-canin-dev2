({
	calculateTax : function(component, event, helper) {
	
		var submitAction = component.get('c.CalculateTaxCommitFalse');
        submitAction.setParams({
            'soId': component.get('v.recordId')
        });
        
        submitAction.setCallback(this, function(response) {
            var state = response.getState();
            console.log(state);
            if (state === 'SUCCESS') {
                console.log('Inside submitAction callback function '+response.getReturnValue());
                console.log(response);
                var toastEvent = $A.get("e.force:showToast");
                var str = response.getReturnValue();
                if(str.includes("error") || str.includes("already") || str.includes("exempted") || str.includes("cancelled") || str.includes("released"))
                {
                    toastEvent.setParams({                       
                        type: 'error',
                        message: response.getReturnValue()
                    });
                }
                else
                {
                    toastEvent.setParams({                        
                        type: 'success',
                        message: response.getReturnValue()
                    });
                }
               
                toastEvent.fire();
            }
            
            $A.get("e.force:closeQuickAction").fire();
            $A.get('e.force:refreshView').fire();
        });
        $A.enqueueAction(submitAction);
		
	}
})