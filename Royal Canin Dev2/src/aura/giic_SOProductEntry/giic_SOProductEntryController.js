({
    //method for component load
    doInit : function(component, event, helper) {
        component.set("v.lstSalesOrderLines", []);
        component.set('v.errmsg',null);
        var cmp1 = component.find("searchprd");
        if (!$A.util.isUndefined(cmp1) && !$A.util.isEmpty(cmp1)) 
        {        
            setTimeout(function() {cmp1.focus();}, 1)
        }
        component.set('v.noresult',false); 
        var accid = component.get("v.pageReference").state.accId;
        var soid = component.get("v.pageReference").state.soid;        
        if (!$A.util.isUndefined(accid) && !$A.util.isEmpty(accid)) 
        {
            component.set('v.accountId',accid);
            component.set('v.isNewOrder',true);              
            helper.fetchAccountData(component, event, helper);          
        }     
        if (!$A.util.isUndefined(soid) && !$A.util.isEmpty(soid)) 
        {    
            component.set('v.soId',soid);
            component.set('v.isNewOrder',false);  
            component.set("v.showdeleteall",false);	
            helper.fetchSOData(component, event, helper);     
        } 
    },	
    //method to handle qty changes
    handleqty: function(component, event, helper) {     
        helper.showSpinner(component, event, helper);
        var inp = component.get('v.prdqty');	    
        if(!$A.util.isUndefined(inp) && !$A.util.isEmpty(inp))
        {
            if(isNaN(inp))
            {
                component.set('v.prdqty', inp.substring(0, inp.length - 1));
            }
            else if(inp ==0)
            {
                component.set('v.prdqty',1);
            }
        }
        else
        {
            component.set('v.prdqty',1);
        }
        
        if(event.getParams().keyCode === 13) {	 
            helper.searchaddproduct(component, event, helper);
            var cmp1 = component.find("searchprd");            
            setTimeout(function() {cmp1.focus();}, 1)
            component.set('v.searchString', '');
            component.set('v.prdqty', '1');
            
        }	    
        helper.hideSpinner(component, event, helper);
    },
    //method to handle delete all rows
    deleteallrow: function(component, event, helper) {	    
        var RowItemList = component.get("v.lstSalesOrderLines");   
        var myarray = [];
        if(!$A.util.isUndefined(RowItemList) && !$A.util.isEmpty(RowItemList))
        {
            for(var i in RowItemList)
            {
                if(RowItemList[i].solineId != '')
                {
                    myarray.push({
                        'solineId' : RowItemList[i].solineId,
                        'prodId' : RowItemList[i].prodId,
                        'prodCode': RowItemList[i].prodCode,
                        'prodDesc': RowItemList[i].prodDesc,
                        'prodQty': RowItemList[i].prodQty,
                        'prodUnitPrice': RowItemList[i].prodUnitPrice,
                        'prodLinePrice': RowItemList[i].prodLinePrice,
                        'prodWeight': RowItemList[i].prodWeight,
                        'prodLineWeight':RowItemList[i].prodLineWeight
                        
                    });              
                }	
                
            }            
            component.set("v.lstSalesOrderLines",myarray);	
            var mylist = component.get("v.lstSalesOrderLines");
            if(!$A.util.isUndefined(mylist) && !$A.util.isEmpty(mylist))
            {                
                helper.totalcalculation(component, event, helper, mylist);
            }
        }
    },
    //method to handle delete individual rows
    deleterow: function(component, event, helper) {
        var Index = event.getSource().get("v.title");   
        var RowItemList = component.get("v.lstSalesOrderLines");
        RowItemList.splice(Index, 1);
        component.set("v.lstSalesOrderLines", RowItemList);
        var mylist = component.get("v.lstSalesOrderLines");
        helper.totalcalculation(component, event, helper, mylist);
    },
    //method to handle individual row quantity changes
    handleqtychange: function(component, event, helper) {
        var classes=    event.getSource().get("v.class")
        var indexClassName=classes.split(" ")[0];
        var indexOfElementChanged = indexClassName.replace("MyIndex","");        
        var elementList = component.get("v.lstSalesOrderLines");
        var changedItem = elementList[indexOfElementChanged];
        var prodQty = changedItem.prodQty;
        var prodWeight = changedItem.prodWeight;
        var prodUnitPrice =  changedItem.prodUnitPrice;        
        if(!$A.util.isUndefined(prodQty) && !$A.util.isEmpty(prodQty))
        {
            if(isNaN(prodQty))
            {                
                prodQty = prodQty.substring(0, prodQty.length - 1);                
            }
            else if(prodQty ==0)
            {
                prodQty = 1;
            }
        }
        else
        {
            prodQty = 1;
        }
        var prodLinePrice =  prodQty * prodUnitPrice;
        var prodLineWeight = prodQty * prodWeight;
        changedItem.prodQty = prodQty;        
        changedItem.prodLineWeight = prodLineWeight.toFixed(2);        
        changedItem.prodLinePrice = prodLinePrice.toFixed(2);
        component.set("v.lstSalesOrderLines",elementList );
        var mylist = component.get("v.lstSalesOrderLines");
        helper.totalcalculation(component, event, helper, mylist);        
    },
    //method to save so from account
    saveSO: function(component, event, helper) {
        helper.saveOrder(component, event, helper);
    },
    //method to update so and so lines from so
    updateSO: function(component, event, helper) {
        helper.updateOrder(component, event, helper);
    },  
    //method to go back to Account record
    goBack: function(component, event, helper) {       
        var accid = component.get('v.accountId');
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": accid,
            "slideDevName": "Detail"
        });
        navEvt.fire();
    },
    //method to go back to SO record
    goBacktoSO: function(component, event, helper) {        
        var soId = component.get('v.soId');
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": soId,
            "slideDevName": "Detail"
        });
        navEvt.fire();
    }
})