({
    //Helper method to make the page to default state
    makedefaultstate: function(component, event, helper) {
        var cmp1 = component.find("searchprd");
        if (!$A.util.isUndefined(cmp1) && !$A.util.isEmpty(cmp1)) 
        {        
        	setTimeout(function() {cmp1.focus();}, 1)
        }
        component.set('v.searchString', '');
        component.set('v.prdqty', '1');
        component.set("v.lstSalesOrderLines", []);
        component.set("v.ordertotalweight", 0);
        component.set("v.ordertotalamount", 0);        
    },
   //Helper method to fetch account details
   fetchAccountData: function(component, event, helper) {   
       debugger; 
       this.showSpinner(component, event, helper);     
        var accountid = component.get('v.accountId');
        if (!$A.util.isUndefined(accountid) && !$A.util.isEmpty(accountid)) 
        {
            var action = component.get("c.getAccountData");
            action.setParams({
                accid: accountid
            });
            action.setCallback(this, function(a) {
                var state = a.getState();                
                if (state == "SUCCESS") 
                {
                    var result = a.getReturnValue();   
                    component.set('v.lstAcc',result.lstAcc);                    
                    if (!$A.util.isUndefined(result) && !$A.util.isEmpty(result) && result.IsSuccess == true) 
                    {
                        var accname = result.lstAcc[0].gii__Account__r.Name;
                        var accbilltonumber = result.lstAcc[0].gii__Account__r.Bill_To_Customer_ID__c;
                        var accshiptonumber = result.lstAcc[0].gii__Account__r.ShipToCustomerNo__c;
                        var accpbname = result.lstAcc[0].gii__PriceBookName__c;  
                        var shippingStreet = result.lstAcc[0].gii__Account__r.ShippingStreet; 
                        var shippingCity = result.lstAcc[0].gii__Account__r.ShippingCity; 
                        var shippingState = result.lstAcc[0].gii__Account__r.ShippingState; 
                        var shippingPostalCode = result.lstAcc[0].gii__Account__r.ShippingPostalCode; 
                        var shippingCountry = result.lstAcc[0].gii__Account__r.ShippingCountry;   
                        component.set('v.accname', accname);
                        component.set('v.accbilltonumber',accbilltonumber);
                        component.set('v.accshiptonumber',accshiptonumber);
                        component.set('v.accpbname',accpbname);
                        component.set('v.ShippingStreet',shippingStreet);
                        component.set('v.ShippingCity',shippingCity);
						component.set('v.ShippingState',shippingState);
                        component.set('v.ShippingPostalCode',shippingPostalCode);
                        component.set('v.ShippingCountry',shippingCountry);              
                        
                    }
                    else if (!$A.util.isUndefined(result) && !$A.util.isEmpty(result) && result.IsSuccess == false) 
                    {
						component.set('v.errmsg',result.Error);
                        var errmsg = component.get('v.errmsg'); 
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({                       
                            title: errmsg,
                            type: 'error',
                            message: $A.get("$Label.c.giic_Back_to_Account")
                        });
                        toastEvent.fire();                        
                    }     
                   component.set("v.showdeleteall",true);	
                   this.hideSpinner(component, event, helper);
                }
            });
            $A.enqueueAction(action);    
        }        
    },
	//Helper method to fetch sales order and lines data
    fetchSOData: function(component, event, helper) {        
        this.showSpinner(component, event, helper);    
        debugger;
        component.set('v.errmsg',null);   
        var salesorderid = component.get('v.soId');
        var RowItemList = component.get("v.lstSalesOrderLines");    
        var myarray = [];
        if (!$A.util.isUndefined(salesorderid) && !$A.util.isEmpty(salesorderid)) 
        {
            var action = component.get("c.getSOData");
            action.setParams({
                soid: salesorderid
            });
            action.setCallback(this, function(a) {
                var state = a.getState();                
                if (state == "SUCCESS") 
                {
                    var result = a.getReturnValue();                
                    if (!$A.util.isUndefined(result) && !$A.util.isEmpty(result) && result.IsSuccess == true) 
                    {                       
                        var accname = result.lstSO[0].gii__Account__r.Name;
                        var accbilltonumber = result.lstSO[0].gii__Account__r.Bill_To_Customer_ID__c;
                        var accshiptonumber = result.lstSO[0].gii__Account__r.ShipToCustomerNo__c;
                        var accpbname = result.lstSO[0].gii__PriceBookName__c;
                        var soNumber = result.lstSO[0].Name;
                        var shippingStreet = result.lstSO[0].gii__Account__r.ShippingStreet; 
                        var shippingCity = result.lstSO[0].gii__Account__r.ShippingCity; 
                        var shippingState = result.lstSO[0].gii__Account__r.ShippingState; 
                        var shippingPostalCode = result.lstSO[0].gii__Account__r.ShippingPostalCode; 
                        var shippingCountry = result.lstSO[0].gii__Account__r.ShippingCountry;   
                       
                        component.set('v.accname', accname);
                        component.set('v.accbilltonumber',accbilltonumber);
                        component.set('v.accshiptonumber',accshiptonumber);
                        component.set('v.accpbname',accpbname);
                        component.set('v.soNumber',soNumber);
                         
                        component.set('v.ShippingStreet',shippingStreet);
                        component.set('v.ShippingCity',shippingCity);
						component.set('v.ShippingState',shippingState);
                        component.set('v.ShippingPostalCode',shippingPostalCode);
                        component.set('v.ShippingCountry',shippingCountry);              
                        
                        var lstSOLW = result.lstSOLW;	
                        if (!$A.util.isUndefined(lstSOLW) && !$A.util.isEmpty(lstSOLW)) 
                        {                            				
                            for (var i in lstSOLW) {
                                myarray.push({
                                    'solineId' : lstSOLW[i].solineId,
                                    'prodId' : lstSOLW[i].prodId,
                                    'prodCode': lstSOLW[i].prodCode,
                                    'prodDesc': lstSOLW[i].prodDesc,
                                    'prodQty': lstSOLW[i].prodQty,
                                    'prodUnitPrice': lstSOLW[i].prodUnitPrice,
                                    'prodLinePrice': lstSOLW[i].prodLinePrice,
                                    'prodWeight': lstSOLW[i].prodWeight,
                                    'prodLineWeight':lstSOLW[i].prodLineWeight.toFixed(2)
                                });                                
                            } 
                            myarray.push.apply(myarray, RowItemList);                           
                            component.set("v.lstSalesOrderLines",myarray);	
                            var mylist = component.get("v.lstSalesOrderLines");                            
                            this.totalcalculation(component, event, helper, mylist);							                            
                        }                 
                    }
                    else if(!$A.util.isUndefined(result) && !$A.util.isEmpty(result) && result.IsSuccess == false) 
                    {	
                        component.set('v.errmsg',result.Error);   
                        var errmsg = component.get('v.errmsg'); 
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({                       
                            title: errmsg,
                            type: 'error',
                            message: $A.get("$Label.c.giic_Back_to_SO")
                        });
                        toastEvent.fire();
                    }
                                        
                    helper.hideSpinner(component, event, helper);
                }
            });
            $A.enqueueAction(action);           
        }
    },
    //Helper method to search the product with entered SKU
    searchaddproduct: function(component, event, helper) {   
        this.showSpinner(component, event, helper);        
        var RowItemList = component.get("v.lstSalesOrderLines");    
        var myarray = [];
        var searchString = component.get('v.searchString');
        var accpbname = component.get('v.accpbname');
        var prdqty = component.get('v.prdqty');        
        var validrequest = false;
        if(!$A.util.isUndefined(searchString) && !$A.util.isEmpty(searchString) 
           && !$A.util.isUndefined(accpbname) && !$A.util.isEmpty(accpbname) 
           && !$A.util.isUndefined(prdqty) && !$A.util.isEmpty(prdqty))
        {
            if(!$A.util.isUndefined(RowItemList) && !$A.util.isEmpty(RowItemList))
            {
                for(var i in RowItemList)
                {
                    if(RowItemList[i].prodCode == searchString && RowItemList[i].prodQty == prdqty)
                    {
                        break;                        
                    }	
                    else
                    {
                        validrequest = true;
                    }
                }
            }
            else
            {
                validrequest = true;
            }                
            if(!$A.util.isUndefined(validrequest) && !$A.util.isEmpty(validrequest) && validrequest ==  true)
            {
                var action = component.get("c.getProductData");
                action.setParams({
                    searchString: component.get('v.searchString'),
                    pbname: component.get('v.accpbname'),
                    prdqty: component.get('v.prdqty')
                });
                action.setCallback(this, function(a) {
                    var state = a.getState();                     
                    if (state == "SUCCESS") 
                    {                   
                        var result = a.getReturnValue();                        
                        if (!$A.util.isUndefined(result) && !$A.util.isEmpty(result) && result.IsSuccess == true) 
                        {
                            var lstSOLW = result.lstSOLW;	
                            for (var i in lstSOLW) {
                                myarray.push({
                                    'solineId' : lstSOLW[i].solineId,
                                    'prodId' : lstSOLW[i].prodId,
                                    'prodCode': lstSOLW[i].prodCode,
                                    'prodDesc': lstSOLW[i].prodDesc,
                                    'prodQty': lstSOLW[i].prodQty,
                                    'prodUnitPrice': lstSOLW[i].prodUnitPrice,
                                    'prodLinePrice': lstSOLW[i].prodLinePrice,
                                    'prodWeight': lstSOLW[i].prodWeight,
                                    'prodLineWeight':lstSOLW[i].prodLineWeight.toFixed(2)
                                });                                
                            } 
                            myarray.push.apply(myarray, RowItemList);                            
                            component.set("v.lstSalesOrderLines",myarray);	
                            var mylist = component.get("v.lstSalesOrderLines");                              
                            this.totalcalculation(component, event, helper, mylist);
                            component.set('v.noresult',false);
                            component.set('v.showdeleteall',true);                      
                        }
                        else
                        {
                            component.set('v.noresult',true);
                        }
                    }
                    else
                    {
                        component.set('v.noresult',true);
                    }
                });
                $A.enqueueAction(action);
                this.hideSpinner(component, event, helper);
            }
        }
        else if($A.util.isUndefined(searchString) || $A.util.isEmpty(searchString) )
        {            
           // console.log('Please enter a search string');
        }
    },
    //Helper method to make calcualtions of total amount and weight
    totalcalculation: function(component, event, helper, mylist) {
        var totalweight = 0.00;
        var totalamount = 0.00;
        for (var i in mylist) 
        {
            if (!$A.util.isUndefined(mylist[i].prodWeight) && !$A.util.isEmpty(mylist[i].prodWeight) && !$A.util.isEmpty(mylist[i].prodQty)) {
                //totalweight += mylist[i].prodWeight*mylist[i].prodQty;
                totalweight += parseFloat(mylist[i].prodLineWeight, 10);
            }
            if (!$A.util.isUndefined(mylist[i].prodLinePrice) && !$A.util.isEmpty(mylist[i].prodLinePrice)) {
                totalamount += parseFloat(mylist[i].prodLinePrice,10);
            }            
        }
        component.set("v.ordertotalweight", totalweight);
        component.set("v.ordertotalamount", totalamount);
    },   
    //Helper method to save the order from Account
    saveOrder: function(component, event, helper) { 
        component.set('v.errmsg',null);
        this.showSpinner(component, event, helper);
        var action = component.get("c.saveSalesOrder");
        var lstAccount = JSON.stringify(component.get('v.lstAcc'));
        var lstSalesOrderLines = JSON.stringify(component.get('v.lstSalesOrderLines'));
        var allinputParam = {};
        allinputParam["lstAcc"] = lstAccount;
        allinputParam["lstSalesOrderLines"] = lstSalesOrderLines;        
        action.setParams({
            data : JSON.stringify(allinputParam)
        });
        action.setCallback(this, function(a) {
            var state = a.getState();                    
            if (state == "SUCCESS") 
            {    
                var result = a.getReturnValue();                  
                if (!$A.util.isUndefined(result) && !$A.util.isEmpty(result) && result.IsSuccess == true) 
                {    
                    this.hideSpinner(component, event, helper); 
                    var soid = result.objSalesOrder.Id;             
                    var action1 = component.get("c.CalculateTax");
                    action1.setParams({
                        SOId : soid
                    });
                     action1.setCallback(this, function(a1) {
                         var state1 = a1.getState();                    
                         if (state1 == "SUCCESS") 
                         {    
                             var result1 = a1.getReturnValue();   
                             if (!$A.util.isUndefined(result1) && !$A.util.isEmpty(result1) && result1.IsSuccess == true) 
                             {
                                 this.hideSpinner(component, event, helper);                                                         
                             }
                             else
                             {
                                 this.hideSpinner(component, event, helper);
                             }                     
                         }
                         else
                         {
                             this.hideSpinner(component, event, helper);
                         }
                     });
                    $A.enqueueAction(action1);
                    
					if (!$A.util.isUndefined(soid) && !$A.util.isEmpty(soid))
					{
						window.parent.location = '/lightning/r/gii__SalesOrder__c/' + soid + '/view';
					}     
					
                }
                else
                {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({                       
                        title: 'Error',
                        type: 'error',
                        message: $A.get("$Label.c.giic_ErrorSOCreate")
                    });
                    toastEvent.fire();
                }                
            }
            else
            {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({                       
                    title: 'Error',
                    type: 'error',
                    message: $A.get("$Label.c.giic_ErrorSOCreate")
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);        
    },   
	//Helper method to update sales order lines from sales order screen
    updateOrder: function(component, event, helper) {  
        this.showSpinner(component, event, helper);
        component.set('v.errmsg',null);
        var action = component.get("c.updateSalesOrder");        
        var lstSalesOrderLines = JSON.stringify(component.get('v.lstSalesOrderLines'));
        var soId = component.get("v.soId");  
        var allinputParam = {};        
        allinputParam["lstSalesOrderLines"] = lstSalesOrderLines;    
        allinputParam["soId"] = soId;           
        action.setParams({
            data : JSON.stringify(allinputParam)
        });
        action.setCallback(this, function(a) {
            var state = a.getState();              
            if (state == "SUCCESS") 
            {  
                var result = a.getReturnValue();                
                if (!$A.util.isUndefined(result) && !$A.util.isEmpty(result) && result.IsSuccess == true) 
                {
                   var action1 = component.get("c.CalculateTax");
                    action1.setParams({
                        SOId : soId
                    });
                     action1.setCallback(this, function(a1) {
                         var state1 = a1.getState();                    
                         if (state1 == "SUCCESS") 
                         {   
                             var result1 = a1.getReturnValue();                          
                             if (!$A.util.isUndefined(result1) && !$A.util.isEmpty(result1) && result1.IsSuccess == true) 
                             {                                 
                                 this.hideSpinner(component, event, helper);                                                               
                             }  
                             else
                             {
                                 this.hideSpinner(component, event, helper);
                             }   
                         }
                         else
                         {
                             this.hideSpinner(component, event, helper);
                         }   
                     });
                    $A.enqueueAction(action1); 
                    if (!$A.util.isUndefined(soId) && !$A.util.isEmpty(soId))
                    {
                        window.parent.location = '/lightning/r/gii__SalesOrder__c/' + soId + '/view';
                    }                    
                }
                else
                {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({                       
                        title: 'Error',
                        type: 'error',
                        message: $A.get("$Label.c.giic_ErrorSOUpdate")
                    });
                    toastEvent.fire();
                }
            }
            else
            {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({                       
                    title: 'Error',
                    type: 'error',
                    message: $A.get("$Label.c.giic_ErrorSOUpdate")
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);        
    },
    //Helper method to show spinner
    showSpinner: function(component, event, helper) {       
        component.set("v.Spinner", true);         
    },        
    //Helper method to hide spinner
    hideSpinner : function(component,event,helper){         
        component.set("v.Spinner", false);       
    },   
})