({
	getSOLines : function(component,event) {
        var action = component.get("c.getSOLineWithHeader");
        action.setParams({
            soId : component.get("v.recordId")
        });
        action.setCallback(this, function(response){
           var state = response.getState();            
           if(state === "SUCCESS"){ 
               var result = response.getReturnValue();
               if((!$A.util.isUndefined(result.HeaderInfo) || !$A.util.isEmpty(result.HeaderInfo))){
                   component.set("v.HeaderInfo", result.HeaderInfo);
               }
               if((!$A.util.isUndefined(result.LineItemInfo) || !$A.util.isEmpty(result.LineItemInfo))){
                   component.set("v.SOLines", result.LineItemInfo);
               }
               
               console.log('Header'+component.get("v.HeaderInfo"));
               console.log('Lines'+component.get("v.SOLines"));
           }
            
        });
        $A.enqueueAction(action);
	}
})