({
    doInit : function(component, event, helper) {
        component.set("v.tableCol",[
            /*{label : "Sales Order Line", fieldName : "SOLName", type: "String"},*/
            {label : "Product",  fieldName: "ProdName", type: "String"},
            {label : "Product SKU", fieldName : "ProdSKU", type: "String"},
            {label : "Order Quantity", fieldName : "OrderQty", type: "Double"},
            {label : "Shipped Quantity", fieldName : "ShippedQty", type: "Double"}
            
        ]);
        helper.getSOLines(component,event); 
    }
    
    
})