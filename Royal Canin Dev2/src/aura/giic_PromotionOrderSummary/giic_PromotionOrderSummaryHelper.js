({
    /*************************************************************** Get Order Summay Promotion wrapper to Display**********************/
    
    getOrderSummary : function(component, event, helper) {
        // debugger;
        component.set("v.soWrapper",event.getParam('arguments').soWrapper);
    },
    deletePromotionFromOrder  : function(component, event, helper,promIndex) {
        component.set("v.isLoading",true);
        //debugger;
        var soWrapper = component.get("v.soWrapper");
        var mapRequest={};
        mapRequest["SOIDS"]=component.get("v.soId");
        mapRequest["PROMOCODE"] =  soWrapper[0].lstPromoLines[promIndex].promotionName;
        mapRequest["PCDELETEIDS"] =  soWrapper[0].lstPromoLines[promIndex].promotionId;
        // debugger;
        var action = component.get("c.deletePromotion");
        action.setParams({                    
            "mapRequest" : mapRequest
        });
        action.setCallback(this, function(response) { 
            // debugger;
            var state = response.getState(); 
            var errorList = [];
            if (state === "SUCCESS") {
                var responseWrapper = response.getReturnValue();
                var sowrap = response.getReturnValue().SOINFO;
                var soWrapper = sowrap[0];
                var err = soWrapper.lstErrorInformation;
                if(typeof soWrapper.lstErrorInformation !== "undefined")
                {
                    for (var i = 0; i < soWrapper.lstErrorInformation.length; i++) 
                    {
                        component.set("v.messageTitle",soWrapper.lstErrorInformation[i].errorMessage);
                        errorList.push(soWrapper.lstErrorInformation[i].errorMessage);
                    }
                    component.set("v.errorList",errorList);
                    component.set("v.hasMessage", true);
                    component.set("v.isLoading",false);
                    
                }else
                {
                    component.set("v.hasMessage", false);
                    /******************************* Call calculateTax method to get the Tax for Sales order from Avalara***********/
                    var caltax = component.get("c.calculateTax");
                    /***************************** Sales Order Id as Param **************************************/
                    caltax.setParams({"soId" : component.get("v.soId")});
                    
                    caltax.setCallback(this,function(response){ 
                        component.set("v.soWrapper",response.getReturnValue()["SOINFO"]);
                        component.set("v.isLoading",false);
                        $A.get('e.force:refreshView').fire();
                        
                    });
                    $A.enqueueAction(caltax);
                    
                }
                
            }
            else
            {
                //Remove this code, use label
                errorList.push('Something went wrong on server side, Please check with your Admin');
                component.set("v.hasMessage", true);
                component.set("v.messageTitle",'Something went wrong on server side, Please check with your Admin');
                component.set("v.isLoading",false);
            }
        });
        $A.enqueueAction(action); 
    }
})