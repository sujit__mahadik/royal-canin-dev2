({
    // On Load method to get the Order summary
	doInit : function(component, event, helper) {
        //Order summary information to display on the screen
	    helper.getOrderSummary(component, event, helper);
	},
    // Refresh order summary when promotion applied on Aplied Screen
	reFreshOrderSummary : function(component, event, helper) {
	    component.set("v.soWrapper",event.getParam('arguments').soWrapper);
	},
    //delete promotion call to delete the promotion from sales order
	deletePromo : function(component, event, helper) {
	    var ind = event.target.closest('a').dataset.ind;
	    helper.deletePromotionFromOrder(component, event, helper,ind);
	}
})