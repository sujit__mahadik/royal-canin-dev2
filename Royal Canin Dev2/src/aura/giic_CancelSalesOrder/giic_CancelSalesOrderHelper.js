({
	cancelOrder : function(component, event, helper) {
	    
	    component.set("v.isLoading",true);
		var cancelaction  = component.get("c.cancelSalesOrder");
		var soid = component.get("v.recordId");
		    cancelaction.setParams({
            "soid" : component.get("v.recordId")
        });
		    
		    cancelaction.setCallback(this, function (response) {    
		     var state = response.getState(); 
		     var toastEvent = $A.get("e.force:showToast");
		     if (state === "SUCCESS") 
            {
                var response = response.getReturnValue();
                if ($A.util.isUndefinedOrNull(response.SUCCESS)) //error case 
                {
                    toastEvent.setParams({
                       title: 'Canel Order',
                        type: 'error',
                        message: response.ERROR[0] //'You can not Canel this Order as Order is Already Released'
                    }); 
                    $A.get("e.force:closeQuickAction").fire();
                
                }else 
                {
                    console.log('SUCCESS');
                    toastEvent.setParams({
                                title: 'Canel Order',
                                type: 'success',
                                message: 'Sales Order Cancelled Successfully'
                            });
                    
                }
                toastEvent.fire();
                $A.get("e.force:closeQuickAction").fire();
                $A.get('e.force:refreshView').fire();
                       
            }else
            {
                toastEvent.setParams({
                                title: 'Canel Order',
                                type: 'error',
                                message: 'Some error occurred, Plesae contact to System Admin'
                            });
                toastEvent.fire();
                $A.get("e.force:closeQuickAction").fire();
            }
            
            component.set("v.isLoading",false);
		 });
		  $A.enqueueAction(cancelaction);  

		
		
	}
})