({
    CallTaxCalculator : function(component, event, helper) {
        debugger;
        this.showSpinner(component, event, helper); 
        var whShippingOSId =  component.get('v.recordId');
        var action = component.get("c.CalculateTax"); 
        action.setParams({
            'sWSOSid':whShippingOSId, 
        });  
        
        $A.enqueueAction(action); 
        action.setCallback(this,function (response){
            var state = response.getState();
            var result = response.getReturnValue();
                console.log(JSON.stringify(result));
            if(state === "SUCCESS"){
                
                if (!$A.util.isUndefined(result) && !$A.util.isEmpty(result) && result.SUCCESS == true) 
                {
                    helper.CalldeleteSOPS_AndCreatenewSPOS(component);       
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({                       
                        title: 'Success',
                        type: 'Success',
                        message: 'Tax calculated successfully.'
                    });
                    toastEvent.fire();          
                    $A.get("e.force:closeQuickAction").fire();
                    $A.get('e.force:refreshView').fire();                 
                }
                else if (!$A.util.isUndefined(result) && !$A.util.isEmpty(result) && result.SUCCESS == false) 
                {                   
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({                       
                        title: 'ERROR',
                        type: 'error',
                        message:result.ERROR
                        // message:result.LIST_OF_ERROR
                    });
                    //message: 'Tax calculation Failed.'
                    toastEvent.fire();          
                    $A.get("e.force:closeQuickAction").fire();
                    $A.get('e.force:refreshView').fire();
                }   
            }
            else
            {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({                       
                    title: 'ERROR',
                    type: 'error',
                    message: 'Tax calculation Failed.'
                });
                toastEvent.fire();          
                $A.get("e.force:closeQuickAction").fire();
                $A.get('e.force:refreshView').fire();
            }
        });
        
    },
    CalldeleteSOPS_AndCreatenewSPOS : function(component) {
        var whShippingOSId =  component.get('v.recordId');        
        var action = component.get("c.deleteSOPS_AndCreatenewSPOS"); 
        action.setParams({
            'sWSOSid':whShippingOSId, 
        }); 
        $A.enqueueAction(action); 
        action.setCallback(this,function (response){
            var state = response.getState();
            var title;
            var msg;
            var type;
            if(state === "SUCCESS"){
                var result = response.getReturnValue();
                console.log(JSON.stringify(result));
                if (!$A.util.isUndefined(result) && !$A.util.isEmpty(result) && result.SUCCESS == true) 
                {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({                       
                        title: 'Success',
                        type: 'Success',
                        message: 'Tax calculated successfully.'
                    });
                    toastEvent.fire();          
                    $A.get("e.force:closeQuickAction").fire();
                    $A.get('e.force:refreshView').fire();                 
                }
                else if (!$A.util.isUndefined(result) && !$A.util.isEmpty(result) && result.SUCCESS == false) 
                {                   
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({                       
                        title: 'ERROR',
                        type: 'error',
                        message:result.ERROR
                    });
                    //message: 'Tax calculation Failed.'
                    toastEvent.fire();          
                    $A.get("e.force:closeQuickAction").fire();
                    $A.get('e.force:refreshView').fire();
                }                    
            }           
        });
    },
    showSpinner: function(component, event, helper) {       
        component.set("v.Spinner", true);         
    },  
    hideSpinner : function(component,event,helper){         
        component.set("v.Spinner", false);       
    },    
})