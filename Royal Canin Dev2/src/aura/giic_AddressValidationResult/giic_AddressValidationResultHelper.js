({
	validateAddress : function(component, event) {
		var action = component.get('c.onLoad');
        action.setParams({
            'recId' : component.get('v.recordId')
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log('state:::'+state);
            if(state === "SUCCESS"){
                var result = response.getReturnValue();
                console.log('response:::'+JSON.stringify(result));
                if(!result.isError){
                    $A.get("e.force:closeQuickAction").fire();
                    var resultsToast = $A.get("e.force:showToast");
                    resultsToast.setParams({
                        "title": "Success",
                        "message": result.errorMsg,
                        "type": "Success"
                    });                
                    resultsToast.fire();
                }else{
                    var resultsToast = $A.get("e.force:showToast");
                    resultsToast.setParams({
                        "title": "Warning",
                        "message": result.errorMsg,
                        "type": "Success"
                    });                
                    resultsToast.fire();
                    component.set("v.suggestedAddress",result);
                }                
            }else{
                let errors = response.getError();
                let message = 'Unknown error'; // Default error message
                // Retrieve the error message sent by the server
                if (errors && Array.isArray(errors) && errors.length > 0) {
                    message = errors[0].message;
                }
                // Display the message
                console.error(message);
                component.set("v.isError",true);
                component.set("v.errorMessage",message);
            }
        });
        
        $A.enqueueAction(action);
	},
    applySuggestedAddress: function(component, event) {
        var suggestedAddress = component.get('v.suggestedAddress');
        console.log('suggestedAddress:::' + suggestedAddress);
        var action = component.get('c.applySuggestedAddress');
        action.setParams({
            'objWrapper' : suggestedAddress
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log('state:::'+state);
            if(state === "SUCCESS"){
                var result = response.getReturnValue();
                console.log('response:::'+JSON.stringify(result));
                $A.get("e.force:closeQuickAction").fire();
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({
                    "title": "Success",
                    "message": result,
                    "type": "Success"
                });                
                resultsToast.fire();
            }else{
                let errors = response.getError();
                let message = 'Unknown error'; // Default error message
                // Retrieve the error message sent by the server
                if (errors && Array.isArray(errors) && errors.length > 0) {
                    message = errors[0].message;
                }
                // Display the message
                console.error(message);
                component.set("v.isError",true);
                component.set("v.errorMessage",message);
            }
        });        
        $A.enqueueAction(action);
    }
})