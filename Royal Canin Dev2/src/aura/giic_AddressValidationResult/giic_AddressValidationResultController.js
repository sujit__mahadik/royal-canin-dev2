({
	doInit : function(component, event, helper) {
		helper.validateAddress(component, event);
	},
    applyAddress: function(component, event, helper) {
		helper.applySuggestedAddress(component, event);
	}
})