<apex:page Controller="gii.HelpGetLangCon" showheader="false" sidebar="false" 
    action="{!URLFOR($Resource.gloviaOM_Help, Lang + '/ServiceOrderLine.htm')}" >
</apex:page>

<!--
<apex:page Controller="gii.HelpGetLangCon" showheader="false" sidebar="false">
<link rel="stylesheet" type="text/css" href="help.css"/>
<link rel="stylesheet" type="text/css" href="/sCSS/Theme2/default/help.css"/>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=ISO-8859-1"/>
<title>
<apex:outputtext value="{!IF(Lang = 'ja',"サービスオーダーライン","Service Order Line")}" />
</title>
</head>

<body>
<table style="font-family: Arial; width: 100%; height: 52px;"> 
<tbody><tr style="color: white;"><td style="background-color: rgb(115, 126, 150);">
<big><b>
<apex:outputtext value="{!IF(Lang = 'ja',"サービスオーダーラインホーム","Service Order Line Home")}" />
</b></big>
</td></tr></tbody> 
</table> 
<br/>

<span style="font-size: 10pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;;">
<table style="font-family: Arial; font-weight: bold; color: white; width: 100%; height: 20px;">
<tbody><tr><td style="background-color: rgb(115, 126, 150);">
<apex:outputtext value="{!IF(Lang = 'ja',"サービスオーダーラインとは","What is a Service Order Line")}" />
</td></tr></tbody>
</table>

<p class="MsoNormal">
<apex:outputtext value="{!IF(Lang = 'ja',"サービスオーダーラインは、販売オーダーの関連リストであり、サービス商品名、数量、金額等のサービス商品に関する明細情報で構成されます。","Service Order Line is one of related lists of Sales Order. Service Order Lines can contain Service Product information for Sales Orders such as Service Product Name, Quantity, Price.")}" /><br/><br/>
</p>
<fieldset class="note"><legend><strong>Note</strong></legend>
<div class="noteBody">
<img src="/img/help/helpNote_icon.gif" align="left"/>
<p class="MsoNormal">
<apex:outputtext value="{!IF(Lang = 'ja',"サービスオーダーラインは、導入作業、コンサルティング作業、サポート作業等のサービス商品についての明細情報であり、在庫品または非在庫品については、販売オーダーラインで明細情報を管理します。","Service Order Lines are used for service products, e.g. installation, consulting, support, etc. As for stock or non-Stock items, Sales Order Lines can contain the line information.")}" /></p>
</div>
</fieldset>
<br/>
<table style="color: white; font-family: Arial; font-weight: bold; height: 20px; width: 100%;">
<tbody><tr><td style="background-color: rgb(115, 126, 150);">
<apex:outputtext value="{!IF(Lang = 'ja',"サービスオーダーラインの作成","Creating Service Order Line")}" />
</td></tr></tbody>
</table>
 
<p class="MsoNormal" style="margin-top: 12pt;"><apex:outputtext value="{!IF(Lang = 'ja',"販売オーダー画面上で、「新規サービスオーダーライン」ボタンを押下し、サービスオーダーラインを作成します。","A new Service Order Line can be created by clicking New Service Order Line button in Sales Order screen.")}" /><br/></p><fieldset class="note"><legend><strong>Note</strong></legend>
<div class="noteBody">
<img src="/img/help/helpNote_icon.gif" align="left"/>
<p class="MsoNormal">
<apex:outputtext value="{!IF(Lang = 'ja',"販売オーダーが商談から変換された場合、商談に含まれている商品のうち、サービス商品の情報が自動的にサービスオーダーラインに変換されます。販売オーダーが販売見積から変換された場合、サービス見積ラインがサービスオーダーラインに変換されます。","When the Sales Order was converted from an Opportunity, service products in Opportunity Products are automatically added to Service Order Lines. When the Sales Order was converted from a Sales Quote, Service Quote lines are converted to Service Order Lines.")}" /></p>
</div>
</fieldset>
<br/>
<table style="font-weight: bold; font-family: Arial; color: white; width: 100%; hdight: 20px;">
<tbody><tr><td style="background-color: rgb(115, 126, 150);">
<apex:outputtext value="{!IF(Lang = 'ja',"サービスオーダーラインの編集","Editing Service Order Line")}" />
</td></tr></tbody>
</table>

<p class="MsoNormal" style="margin-top: 12pt;"><apex:outputtext value="{!IF(Lang = 'ja',"サービスオーダーラインは、随時編集が可能ですが、請求済等のステータスによっては、編集不可となります。","Service Order Line can be edited after creation. However, it is not allowed to edit depending on the Status, e.g. Invoice.")}" /></p><br/><br/>
<b><apex:outputtext value="{!IF(Lang = 'ja', "関連情報：", "See Also:")}" /></b><br/>
<apex:outputlink value="/apex/Help_SalesOrder"><apex:outputtext value="{!IF(Lang = 'ja',"販売オーダー","Sales Order")}" /></apex:outputlink><br/>
<apex:outputlink value="/apex/Help_SalesOrderLine"><apex:outputtext value="{!IF(Lang = 'ja',"販売オーダーライン","Sales Order Lines")}" /></apex:outputlink><br/>
<br/><br/>
<table style="color: white; font-family: Arial; font-weight: bold; height: 20px; width: 100%;">
<tbody><tr><td style="background-color: rgb(115, 126, 150);">
<apex:outputtext value="{!IF(Lang = 'ja',"サービスオーダーラインのボタン／リンク","Buttons/Links of Service Order Line")}" />
</td></tr></tbody>
</table>

<div class="p">
<table summary="" class="featureTable" cellpadding="4"
 cellspacing="0" width="100%">
<thead align="left"><tr>
<th class="featureTableHeader" valign="top" width="200">
<apex:outputtext value="{!IF(Lang = 'ja',
"ボタン／リンク",
"Button/Link")}" />
</th>
<th class="featureTableHeader" valign="top">
<apex:outputtext value="{!IF(Lang = 'ja',
"説明",
"Description")}" />
</th></tr>
</thead>

<tbody><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"代替/アップセル/クロスセル/キット","Substitute / Up Sell / Cross Sell / Kitting")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"代替、アップセル、クロスセル、キットの追加画面を開く","Opens a screen to add Substitute, Up Sell, Cross Sell and Kit Member products for the Service Order Line product.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"請求スケジュール作成","Maintain Invoice Schedules")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"請求スケジュールの作成・編集画面を開く。","Opens a screen to create and maintain Invoice Schedule.")}" />
</td>
</tr></tbody>
</table>
</div><br/><br/>
<table style="color: white; font-family: Arial; font-weight: bold; height: 20px; width: 100%;">
<tbody><tr><td style="background-color: rgb(115, 126, 150);">
<apex:outputtext value="{!IF(Lang = 'ja',"サービスオーダーラインの項目","Fields of Service Order Line")}" />
</td></tr></tbody>
</table>

<div class="p">
<table summary="" class="featureTable" cellpadding="4"
 cellspacing="0" width="100%">
<thead align="left"><tr>
<th class="featureTableHeader" valign="top" width="200">
<apex:outputtext value="{!IF(Lang = 'ja',
"項目",
"Field")}" />
</th>
<th class="featureTableHeader" valign="top">
<apex:outputtext value="{!IF(Lang = 'ja',
"説明",
"Description")}" />
</th></tr>
</thead>

<tbody><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"サービスオーダーライン","Service Order Line")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"サービスオーダーラインの番号","Service Order Line number. A unique number for the Service Order Line.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"オーダーライン単価に対する割合％","% Of Order Line Unit Price")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"当該サービスオーダーライン単価が、他販売オーダーライン単価に関連して決定される場合の単価比率（％）。商品関連情報のデフォルト値がセットされる。","When the unit price of the Service Order Line is defined as a % from another Sales Order Line, this field contains the percentage for the price calculation. Defaulted from Product Reference.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"会計グループコード","Accounting Group Code")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"販売オーダートランザクションの会計コード。販売オーダー（ヘッダー）の値がセットされる。","The ode of accounting which Sales Order transactions belongs to.  Defaulted from Sales Order header.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"キャンセル日","Cancel Date")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"サービスオーダーラインがキャンセルされた日付","Date of cancellation of Service Order Line.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"キャンセル数","Cancelled Quantity")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"サービスオーダーラインがキャンセルされた数量","Quantity of cancellation of Service Order Line.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"キャンセル理由","Cancel Reason")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"サービスオーダーラインがキャンセルされた理由","Reason of cancellation of Service Order Line.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"契約","Contract")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"サービスの対象となる契約。","The contract number which is related to the service. ")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"契約期間(月)","Contract Term (months)")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"サービスの対象となる契約の期間。","The contract term of the contract.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"顧客購買オーダー日付","Customer PO Date")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"顧客購買オーダーの日付。販売オーダーの値がセットされる。","The customer Purchase Order Date.  Defaulted from Sales Order.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"顧客購買オーダーライン","Customer PO Line")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"対応する顧客購買オーダーのライン番号。","The customer Purchase Order Line number. ")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"顧客購買オーダー番号","Customer PO Number")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"顧客購買オーダーの番号。販売オーダーの値がセットされる。","The customer Purchase Order number. ")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"値引額","Discount Amount")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"サービスオーダーラインの値引額。値引率にもとづき算出される。","The discount amount for Service Order Line. Calculated based on Discount Percent.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"値引率","Discount Percent")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"サービスオーダーラインの値引率。","Percentage of Discount applicable on price of the Service Order Line Product.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"物品税課税","Excise Taxable")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"物品税課税を有効化する。サービスオーダー追加料金の新規作成時に、取引先関連情報のデフォルト値がセットされる。販売見積からの変換時は、サービス見積ラインの情報がセットされる。","If checked, the Service Order Line is excise taxable. When creating a new Sales Order Line, this field can be defaulted from  Account Reference. When converted from  a Sales Quote, this field can be copied from Sales Quote Line.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"請求準備","Forward To Invoice")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"請求準備処理が実行された際にフラグがオンとなる。","If checked, the Service Order Line has been forwarded to Invoicing.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"請求書値引額","Invoice Discount Amount")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"請求済の商品値引額","The invoiced discount amount for the Service Order line.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"請求済数量","Invoiced Quantity")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"請求済の数量","The invoiced quantity for the Service Order line.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"請求書オーダー値引額","Invoice Order Discount Amount")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"請求済の販売オーダー全体での値引額。","The invoice discount amount for the entire order. ")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"請求スケジュール開始日","Invoice Schedule Start Date")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"請求スケジュールを作成する際の初回請求日。","The start date of invoicing when using an Invoice Schedule.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"請求書サービス金額","Invoice Service Amount")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"請求済の商品金額","The invoiced amount of Product on the Service Order Line.  ")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"請求書税額","Invoice Tax Amount")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"請求済の税額の合計。","The invoiced tax amount on Service Order Line.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"請求書合計額","Invoice Total Amount")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"請求済の値引後の合計金額。","The invoice total amount after the discounts. ")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"請求書VAT額","Invoice VAT Amount")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"請求済のVAT額。","The invoiced VAT amount on Service Order Line. ")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"キット","Kit")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"キット商品を表すフラグであり、オンの場合は当該商品がキット品であることを示す。「キット」とは、複数の商品（キットメンバー）からなる商品であり、１段階の部品表に類似している。キットは、商品関連情報内の設定で定義される。","If checked, the product is kit product.
Kitting in glovia.com Order Management lets you create group of individual products required for the selling as final single product. The concept is similar to single level Bill of Materials (BOM).
")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"キットライン","Kit Line")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"当該商品がキットメンバー商品である場合に、親キット商品のライン番号がセットされる。","When the Product in Service Order Line is a Kit Member, this field has the Order line number of the Kit Parent Product.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"キット必須メンバー商品","Kit Product Required")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"当該商品がキットメンバー商品であり、かつキット必須メンバー商品の場合に、フラグがオンにセットされる。","When the Product in Service Order Line is a mandatory Kit Member, this flag is set on.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"説明","Line Description")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"ラインごとの説明。商談から変換された場合には、商談商品の説明情報が自動的にセットされる。販売見積からの変換時は、サービス見積ラインの情報がセットされる。","Line description. When converting from an Opportunity,  this field can be copied from Opportunity Product. When converting from a Sales Quote, this field can be copied from Service Quote Line.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"正味額","Net Amount")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"商品金額、値引、税額の総合計金額。","Total estimated amount of product amount, discounts and taxes.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"正味請求額","Net Invoice Amount")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"請求済のオーダーライン合計額、追加料金合計額、税額、値引額の総合計金額","Total invoiced amount of Order, Additional Charges and Tax, including discounts.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"非請求理由","No Charge Reason")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"非請求とする理由。","No Charge Reason code if the Unit Price of the Service Order Line Product is zero.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"分割回数","Number of Installments")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"請求スケジュールが作成された際の、スケジュール全体での請求回数。","Number of invoicing When Invoice Schedule was created.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"オーダー値引額","Order Discount Amount")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"販売オーダー全体での値引額。","The estimated discount amount for the whole order. ")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"オーダー数","Order Quantity")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"サービスオーダーラインの商品のオーダー数。","Service Order Line Product order quantity.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"当初商品","Original Product")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"当該商品がアップセル品または代替品である場合に、アップセルまたは代替前の商品名が保存される。","When the Product is Up-sell or Substitute Product, this field contains the original product name.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"数量変更可","Quantity Change Allowed")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"当該商品がキットメンバー商品であり、かつ数量変更可能メンバー商品の場合に、フラグがオンにセットされる。","If checked, the quantity change allowed for the kit member product. ")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"請求間隔","Revenue Schedule Installment Period")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"請求スケジュールが作成された際の、請求頻度。","Installment period  of invoicing When Invoice Schedule was created.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"販売オーダー","Sales Order")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"販売オーダーの番号。","A Sales Order number. ")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"販売オーダークロスセルライン","Sales Order Cross Sell Line")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"当該サービスオーダーラインが他販売オーダーラインのクロスセルである場合に、他販売オーダーラインのライン番号がセットされる。","When the Service Order Line is a Cross-sell of another Sales Order Line, this field contains the line number of that Sales Order Line.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"販売オーダーライン","Sales Order Line")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"当該サービスオーダーライン単価が、他販売オーダーライン単価に関連して決定される場合の、対象となる販売オーダーラインの番号。","When the unit price of the Service Order Line is defined as a % from another Sales Order Line, this field contains the line number of the Sales Order Line.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"販売オーダーライン単価","Sales Order Line Unit Price")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"当該サービスオーダーライン単価が、他販売オーダーライン単価に関連して決定される場合の、対象となる販売オーダーラインの単価。","When the unit price of the Service Order Line is defined from another Sales Order Line, this field contains the unit price of the Sales Order Line.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"販売見積","Sales Quote")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"販売見積の番号。","Sales Quote number. ")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"売上税課税","Sales Taxable")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"売上税課税を有効化する。サービスオーダー追加料金の新規作成時に、取引先関連情報のデフォルト値がセットされる。販売見積からの変換時は、サービス見積ラインの情報がセットされる。","If checked, the Service Order Line is sales taxable. When creating a new Sales Order Line, this field can be defaulted from  Account Reference. When converted from  a Sales Quote, this field can be copied from Sales Quote Line.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"請求スケジュール作成済","Schedule")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"請求スケジュールが作成済の場合にフラグがオンとなる。","If checked, Invoice Schedule has been created.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"請求スケジュールテンプレート","Schedule Template")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"請求スケジュールを作成する際に使用するテンプレート。テンプレートはコードメンテナンス・タブ内で登録されたコードから選択する。取引先関連情報のデフォルト値がセットされる。","Template code which will be used for creating Invoice Schedule. Template codes can be added in Code Maintenance tab. Defaulted from Account Reference.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"サービス金額","Service Amount")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"サービスオーダーラインの商品金額。","The total estimated amount of service product for line. ")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"サービス料金タイプ","Service Charge Type")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"サービス見積の料金タイプ。サービスタイプのデフォルト値がセットされる。","Type of Service Charge. Defaulted from Service Type.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"サービス完了日","Service Completion Date")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"サービスの完了日。","The completion date of the service")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"サービス終了日","Service End Date")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"サービスの提供終了日。","The end date of the service")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"サービスオーダークロスセルライン","Service Order Cross Sell Line")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"当該サービスオーダーラインが他サービスオーダーラインのクロスセルである場合に、他サービスオーダーラインのライン番号がセットされる。","When the Service Order Line is a Cross-sell of another Service Order Line, this field contains the line number of that Service Order Line.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"サービス商品","Service Product")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"サービス商品名。","The name of service product")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"サービス見積キットライン","Service Quote Kit Line")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"販売オーダーが販売見積から変換された場合で、サービスオーダーラインがキットメンバーである場合、キット親商品を含むサービス見積ラインの番号がセットされる。","When the Service Order Line product is a Kit Member, this field has the Service Quote line number of the Kit Parent Product when sales order was converted from sales quote. ")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"サービス見積ライン","Service Quote Line")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"販売見積から変換された際のサービス見積ライン番号。","When Sales Order was converted from a Sales Quote, this field contains the Service Quote Line number. ")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"サービス開始日","Service Start Date")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"サービスの提供開始日。","The start date of the service")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"サービスタイプ","Service Type")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"サービスのタイプ。商品関連情報のデフォルト値がセットされる。","Type of Service. Defaulted from Product Reference.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"ステータス","Status")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"サービスオーダーラインのステータス","The status of the Service Order Line. ")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"税額","Tax Amount")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"税額の合計。","The total of estimate tax amount on Sales Order Line.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"税ロケーション","Tax Location")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"課税ロケーション。販売オーダーの値がセットされる。","A code representing a tax location. The tax location code determines the set of tax rate jurisdictions applied to the sale of this service. Defaulted from Sales Order.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"合計金額","Total Amount")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"値引後の合計金額。","The total estimate amount after the discounts. ")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"原価","Unit Cost")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"サービスオーダーラインの商品の原価。商品関連情報のデフォルト値がセットされる。","Unit Cost of the Service Order Line Product. Defaulted from Product Reference.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"単価","Unit Price")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"サービスオーダーラインの商品の単価。販売オーダーの価格表にもとづきセットされる。","Unit Price of the Service Order Line Product. Defaulted from Price Book of the Sales Order.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"利用税課税","Usage Taxable")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"利用税課税を有効化する。サービスオーダー追加料金の新規作成時に、取引先関連情報のデフォルト値がセットされる。販売見積からの変換時は、サービス見積ラインの情報がセットされる。","If checked, the Service Order Line is usage taxable. When creating a new Sales Order Line, this field can be defaulted from  Account Reference. When converted from  a Sales Quote, this field can be copied from Sales Quote Line.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"VAT","VAT")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"VAT課税の際のVATコード。販売オーダーの値がセットされる。","VAT code for VAT taxation.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"VAT額","VAT Amount")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"サービスオーダーラインのVAT額。","The total Estimate VAT amount on Service Order Line. ")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"VAT登録番号","VAT Registration")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"VAT登録番号。販売オーダーの値がセットされる。","The VAT registration number. Defaulted from Sales Order.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"VAT課税","VAT Taxable")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"VAT課税を有効化する。販売オーダーの値がセットされる。","If checked, order is VAT taxable. Defaulted from Sales Order.")}" />
</td>
</tr></tbody>
</table>
</div>
</span></body>

<div class="helpPageFooter">Copyright  2008-{!YEAR( TODAY() )} Glovia International, inc. All rights reserved.<br/>
Various trademarks held by their respective owners.</div>
</html>
</apex:page>
-->
<!-- Generation Date & Time: 5/4/2010 6:35:29 PM PST -->