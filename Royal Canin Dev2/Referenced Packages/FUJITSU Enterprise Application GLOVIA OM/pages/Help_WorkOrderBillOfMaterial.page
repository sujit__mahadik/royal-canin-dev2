<apex:page Controller="gii.HelpGetLangCon" showheader="false" sidebar="false" 
    action="{!URLFOR($Resource.gloviaOM_Help, Lang + '/WorkOrderBillOfMaterial.htm')}" >
</apex:page>

<!--
<apex:page Controller="gii.HelpGetLangCon" showheader="false" sidebar="false">
<link rel="stylesheet" type="text/css" href="help.css"/>
<link rel="stylesheet" type="text/css" href="/sCSS/Theme2/default/help.css"/>
<apex:outputpanel rendered="{!IF(Lang != 'ja',true,false)}">
<head>
<meta http-equiv="content-type" content="text/html; charset=ISO-8859-1"/>
<title>
Work Order Bill of Material
</title>
</head>
<body>
<table style="font-family: Arial; width: 100%; height: 52px;">
<tbody>
<tr style="color: white;">
<td style="background-color: rgb(115, 126, 150);"><big><b>Work Order Bill of Material Home</b></big></td></tr></tbody></table><br/>
<span style="font-size: 10pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;;">
<table style="font-family: Arial; font-weight: bold; color: white; width: 100%; height: 20px;">
<tbody>
<tr><td style="background-color: rgb(115, 126, 150);">What is a Work Order Bill of Material</td></tr></tbody></table></span>
<p class="MsoNormal">When you create work orders you can establish a work order bill of material. If you want to issue material to the work order, you can establish a work order bill of material. However you can issue materials with or without bill of material. </p>
<br/>
<span style="font-size: 10pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;;">
<table style="font-family: Arial; font-weight: bold; color: white; width: 100%; height: 20px;">
<tbody>
<tr><td style="background-color: rgb(115, 126, 150);">Creating Work Order Bill of Material</td></tr></tbody></table></span>
<p class="MsoNormal">glovia OM gives you the option to:<ul>
<li>Copy a product bill of material</li>
<li>Create a new bill of material work order.</li>
</ul></p>
<p class="MsoNormal">For example, if you manufacture a product that is always built the same way, you can simply copy the product bill of material each time to create the work order bill of material. </p>
<p class="MsoNormal">You can create a bill of material by specifying the component and component quantity. </p>
<fieldset class="note">
<legend><strong>Note</strong></legend>
<div class="noteBody">
<img src="/img/help/helpNote_icon.gif" align="left"/>
<p class="MsoNormal">When work order’s quantity is changed, then work order bill of material component’s required quantity is updated automatically.</p></div></fieldset>
<br/>
<span style="font-size: 10pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;;">
<table style="font-family: Arial; font-weight: bold; color: white; width: 100%; height: 20px;">
<tbody>
<tr><td style="background-color: rgb(115, 126, 150);">Fields of Work Order Bill of Material</td></tr></tbody></table></span>
<div class="p">
<table summary="" class="featureTable" cellpadding="4" cellspacing="0" width="100%">
<thead align="left">
<tr>
<th class="featureTableHeader" valign="top" width="200">Field Label</th>
<th class="featureTableHeader" valign="top">Data Type</th>
<th class="featureTableHeader" valign="top">Description</th></tr></thead><tbody>
<tr><td>Work Order Bill of Material</td><td>Auto Number </td><td>The work order bill of material number or identifier. (Display Format {000000})</td></tr>
<tr><td>Actual Material Cost</td><td>Number(13, 4)</td><td>The actual cost for issued component quantity </td></tr>
<tr><td>Component</td><td>Lookup(Product Reference)</td><td>The name of a product on a bill of material.</td></tr>
<tr><td>Component Quantity</td><td>Number(10, 4)</td><td>The component quantity.</td></tr>
<tr><td>Cost Method</td><td>Text(80)</td><td>Product’s cost method</td></tr>
<tr><td>Estimated Material Cost</td><td>Formula (Number)</td><td>The estimated cost for required component quantity times the unit cost of the component.</td></tr>
<tr><td>Instructions</td><td>Long Text Area(32000)</td><td>Instructions for the bill of material component</td></tr>
<tr><td>Issued Quantity</td><td>Number(10, 2)</td><td>The quantity issued for this component</td></tr>
<tr><td>Remaining Quantity</td><td>Formula (Number)</td><td>The remaining quantity to be issued for this component</td></tr>
<tr><td>Required Quantity</td><td>Number(10, 2)</td><td>The required quantity to be issued for this component</td></tr>
<tr><td>Sequence</td><td>Number(4, 0)</td><td>A sequence number for each component in a bill of material. This number is defaulted from the product’s bill of material line and can be maintained.</td></tr>
<tr><td>Unit Cost</td><td>Number(13, 4)</td><td>The standard cost per unit for a product.</td></tr>
<tr><td>Unit Of Measure</td><td>Picklist</td><td>The unit of measure for a product. Defaulted from component’s product reference record.</td></tr>
<tr><td>Work Order</td><td>Master-Detail(Work Order)</td><td>The work order number or identifier.</td></tr>
</tbody></table></div><br/><br/>
<span style="font-size: 10pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;;">
<table style="font-family: Arial; font-weight: bold; color: white; width: 100%; height: 20px;">
<tbody>
<tr><td style="background-color: rgb(115, 126, 150);">Error Messages of Work Order Bill of Material</td></tr></tbody></table></span>
<div class="p">
<table summary="" class="featureTable" cellpadding="4" cellspacing="0" width="100%">
<thead align="left">
<tr>
<th class="featureTableHeader" valign="top" width="200">Error Message</th>
<th class="featureTableHeader" valign="top">Source</th>
<th class="featureTableHeader" valign="top">Condition</th></tr></thead><tbody>
<tr><td>Component is Required.</td><td>Validation Rule </td><td>Component = null</td></tr>
<tr><td>Component Quantity is Required.</td><td>Validation Rule </td><td>Component quantity = null  </td></tr>
<tr><td>Work order is already closed.</td><td>Validation Rule </td><td>When new component is added and work order is already closed</td></tr>
</tbody></table></div><br/><br/>
</body>
</apex:outputpanel>
<apex:outputpanel rendered="{!IF(Lang = 'ja',true,false)}">
<head>
<meta http-equiv="content-type" content="text/html; charset=ISO-8859-1"/>
<title>
Work Order Bill of Material
</title>
</head>
<body>
<table style="font-family: Arial; width: 100%; height: 52px;">
<tbody>
<tr style="color: white;">
<td style="background-color: rgb(115, 126, 150);"><big><b>Work Order Bill of Material Home</b></big></td></tr></tbody></table><br/>
<span style="font-size: 10pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;;">
<table style="font-family: Arial; font-weight: bold; color: white; width: 100%; height: 20px;">
<tbody>
<tr><td style="background-color: rgb(115, 126, 150);">What is a Work Order Bill of Material</td></tr></tbody></table></span>
<p class="MsoNormal">When you create work orders you can establish a work order bill of material. If you want to issue material to the work order, you can establish a work order bill of material. However you can issue materials with or without bill of material. </p>
<br/>
<span style="font-size: 10pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;;">
<table style="font-family: Arial; font-weight: bold; color: white; width: 100%; height: 20px;">
<tbody>
<tr><td style="background-color: rgb(115, 126, 150);">Creating Work Order Bill of Material</td></tr></tbody></table></span>
<p class="MsoNormal">glovia OM gives you the option to:<ul>
<li>Copy a product bill of material</li>
<li>Create a new bill of material work order.</li>
</ul></p>
<p class="MsoNormal">For example, if you manufacture a product that is always built the same way, you can simply copy the product bill of material each time to create the work order bill of material. </p>
<p class="MsoNormal">You can create a bill of material by specifying the component and component quantity. </p>
<fieldset class="note">
<legend><strong>Note</strong></legend>
<div class="noteBody">
<img src="/img/help/helpNote_icon.gif" align="left"/>
<p class="MsoNormal">When work order’s quantity is changed, then work order bill of material component’s required quantity is updated automatically.</p></div></fieldset>
<br/>
<span style="font-size: 10pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;;">
<table style="font-family: Arial; font-weight: bold; color: white; width: 100%; height: 20px;">
<tbody>
<tr><td style="background-color: rgb(115, 126, 150);">Fields of Work Order Bill of Material</td></tr></tbody></table></span>
<div class="p">
<table summary="" class="featureTable" cellpadding="4" cellspacing="0" width="100%">
<thead align="left">
<tr>
<th class="featureTableHeader" valign="top" width="200">Field Label</th>
<th class="featureTableHeader" valign="top">Data Type</th>
<th class="featureTableHeader" valign="top">Description</th></tr></thead><tbody>
<tr><td>Work Order Bill of Material</td><td>Auto Number </td><td>The work order bill of material number or identifier. (Display Format {000000})</td></tr>
<tr><td>Actual Material Cost</td><td>Number(13, 4)</td><td>The actual cost for issued component quantity </td></tr>
<tr><td>Component</td><td>Lookup(Product Reference)</td><td>The name of a product on a bill of material.</td></tr>
<tr><td>Component Quantity</td><td>Number(10, 4)</td><td>The component quantity.</td></tr>
<tr><td>Cost Method</td><td>Text(80)</td><td>Product’s cost method</td></tr>
<tr><td>Estimated Material Cost</td><td>Formula (Number)</td><td>The estimated cost for required component quantity times the unit cost of the component.</td></tr>
<tr><td>Instructions</td><td>Long Text Area(32000)</td><td>Instructions for the bill of material component</td></tr>
<tr><td>Issued Quantity</td><td>Number(10, 2)</td><td>The quantity issued for this component</td></tr>
<tr><td>Remaining Quantity</td><td>Formula (Number)</td><td>The remaining quantity to be issued for this component</td></tr>
<tr><td>Required Quantity</td><td>Number(10, 2)</td><td>The required quantity to be issued for this component</td></tr>
<tr><td>Sequence</td><td>Number(4, 0)</td><td>A sequence number for each component in a bill of material. This number is defaulted from the product’s bill of material line and can be maintained.</td></tr>
<tr><td>Unit Cost</td><td>Number(13, 4)</td><td>The standard cost per unit for a product.</td></tr>
<tr><td>Unit Of Measure</td><td>Picklist</td><td>The unit of measure for a product. Defaulted from component’s product reference record.</td></tr>
<tr><td>Work Order</td><td>Master-Detail(Work Order)</td><td>The work order number or identifier.</td></tr>
</tbody></table></div><br/><br/>
<span style="font-size: 10pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;;">
<table style="font-family: Arial; font-weight: bold; color: white; width: 100%; height: 20px;">
<tbody>
<tr><td style="background-color: rgb(115, 126, 150);">Error Messages of Work Order Bill of Material</td></tr></tbody></table></span>
<div class="p">
<table summary="" class="featureTable" cellpadding="4" cellspacing="0" width="100%">
<thead align="left">
<tr>
<th class="featureTableHeader" valign="top" width="200">Error Message</th>
<th class="featureTableHeader" valign="top">Source</th>
<th class="featureTableHeader" valign="top">Condition</th></tr></thead><tbody>
<tr><td>Component is Required.</td><td>Validation Rule </td><td>Component = null</td></tr>
<tr><td>Component Quantity is Required.</td><td>Validation Rule </td><td>Component quantity = null  </td></tr>
<tr><td>Work order is already closed.</td><td>Validation Rule </td><td>When new component is added and work order is already closed</td></tr>
</tbody></table></div><br/><br/>
</body>
</apex:outputpanel>
<div class="helpPageFooter">
Copyright  2008-{!YEAR( TODAY() )} Glovia International, inc. All rights reserved.<br/>
Various trademarks held by their respective owners.
</div>
</apex:page>
-->
<!-- Generation Date & Time: 2/7/2011 9:23:01 PM PST -->