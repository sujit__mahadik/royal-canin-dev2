<apex:page Controller="gii.HelpGetLangCon" showheader="false" sidebar="false" 
    action="{!URLFOR($Resource.gloviaOM_Help, Lang + '/APInvoice.htm')}" >
</apex:page>

<!--
<apex:page Controller="gii.HelpGetLangCon" showheader="false" sidebar="false">
<link rel="stylesheet" type="text/css" href="help.css"/>
<link rel="stylesheet" type="text/css" href="/sCSS/Theme2/default/help.css"/>
<apex:outputpanel rendered="{!IF(Lang != 'ja',true,false)}">
<head>
<meta http-equiv="content-type" content="text/html; charset=ISO-8859-1"/>
<title>
AP Invoice
</title>
</head>
<body>
<table style="font-family: Arial; width: 100%; height: 52px;">
<tbody>
<tr style="color: white;">
<td style="background-color: rgb(115, 126, 150);"><big><b>AP Invoice Home</b></big></td></tr></tbody></table><br/>
<span style="font-size: 10pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;;">
<table style="font-family: Arial; font-weight: bold; color: white; width: 100%; height: 20px;">
<tbody>
<tr><td style="background-color: rgb(115, 126, 150);">What is a AP Invoice</td></tr></tbody></table></span>
<p class="MsoNormal">AP Invoice maintains a record of supplier invoices for the purchase of goods and services for your business.</p>
<p class="MsoNormal">AP Invoice can be created with voucher distributions for purchase order receipts and General Ledger distributions.</p>
<fieldset class="note">
<legend><strong>Note</strong></legend>
<div class="noteBody">
<img src="/img/help/helpNote_icon.gif" align="left"/>
<p class="MsoNormal">Posting Date must be marked as read only field in all page layouts. When user clicks on “Post AP Invoice” and AP Invoice is fully distributed then system sets this as today’s date. 
When user clicks on Reverse Posting button then this date is set to blank.</p></div></fieldset>
<br/>
<span style="font-size: 10pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;;">
<table style="font-family: Arial; font-weight: bold; color: white; width: 100%; height: 20px;">
<tbody>
<tr><td style="background-color: rgb(115, 126, 150);">Creating AP Invoice</td></tr></tbody></table></span>
<p class="MsoNormal">To create a new AP Invoice, select AP Invoice from the Create New drop-down list in the sidebar, or click New next to Recent AP Invoices on the AP Invoices home page. Enter the information for the AP Invoice. Click Save when you are finished, or click Save and New to save the current AP Invoice and add another.</p>
<br/>
<b>See Also:</b><br/>
<span style="font-size: 9pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;;">
<apex:outputlink value="Help_APInvoiceVoucherDistribution">AP Invoice Voucher Distribution</apex:outputlink></span><br/>
<span style="font-size: 9pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;;">
<apex:outputlink value="Help_APInvoiceGeneralLedgerDistribution">AP Invoice General Ledger Distribution</apex:outputlink></span><br/>
<br/>
<span style="font-size: 10pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;;">
<table style="font-family: Arial; font-weight: bold; color: white; width: 100%; height: 20px;">
<tbody>
<tr><td style="background-color: rgb(115, 126, 150);">Buttons of AP Invoice</td></tr></tbody></table></span>
<div class="p">
<table summary="" class="featureTable" cellpadding="4" cellspacing="0" width="100%">
<thead align="left">
<tr>
<th class="featureTableHeader" valign="top" width="200">Button</th>
<th class="featureTableHeader" valign="top">Description</th></tr></thead><tbody>
<tr><td>Post</td><td>Validate AP Invoice and set the posting date. If “Create Staging Purchase Invoice Records” policy is set then Staging Purchase Invoice Records will also be created. Staging journal line item records will be created for the PPV2 for any AP Invoice voucher distributions.</td></tr>
<tr><td>Reverse Posting</td><td>Reset the posting date. If Staging Purchase Invoice Records were created and are already processed or Staging journal line item records were created and are already processed, then AP Invoice posting cannot be reversed.</td></tr>
<tr><td>Voucher Purchase Order Receipt</td><td>Add voucher distribution details from all unvouchered lines of a purchase order receipt.</td></tr>
</tbody></table></div><br/><br/>
<span style="font-size: 10pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;;">
<table style="font-family: Arial; font-weight: bold; color: white; width: 100%; height: 20px;">
<tbody>
<tr><td style="background-color: rgb(115, 126, 150);">Fields of AP Invoice</td></tr></tbody></table></span>
<div class="p">
<table summary="" class="featureTable" cellpadding="4" cellspacing="0" width="100%">
<thead align="left">
<tr>
<th class="featureTableHeader" valign="top" width="200">Field Label</th>
<th class="featureTableHeader" valign="top">Data Type</th>
<th class="featureTableHeader" valign="top">Description</th></tr></thead><tbody>
<tr><td>AP Invoice</td><td>Auto Number</td><td>AP Invoice number auto generated in following format APINV-{YYYYMMDD}-{000000}</td></tr>
<tr><td>Accounting Group Code</td><td>Picklist</td><td>The Account Group Code</td></tr>
<tr><td>Document Amount</td><td>Currency(16, 2)</td><td>The document amount.</td></tr>
<tr><td>Document Date</td><td>Date</td><td>The document date is used to calculate the payment terms net due date.</td></tr>
<tr><td>Document Text</td><td>Long Text Area(32000)</td><td>The document text to record any memo or notes.</td></tr>
<tr><td>Due Date</td><td>Date</td><td>The day payment in full is due. This date can be overwritten; if null then it is calculated based on the net dues days of payment terms.</td></tr>
<tr><td>Federal 1099 Code</td><td>Lookup(Federal 1099 Code)</td><td>Code identifying box numbers on a federal 1099 form.</td></tr>
<tr><td>General Ledger Amount</td><td>Roll-Up Summary (SUM AP Invoice General Ledger Distribution)</td><td>The amount distributed to general ledger accounts.</td></tr>
<tr><td>GL Distribution VAT Amount</td><td>Roll-Up Summary (SUM AP Invoice General Ledger Distribution)</td><td>The total amount of value added tax owed from GL distribution.</td></tr>
<tr><td>Payment Terms</td><td>Lookup(Payment Term)</td><td>Payment term codes determine the due date for payment. </td></tr>
<tr><td>Posting Date</td><td>Date</td><td>AP Invoice Posting Date. This date is set when post button is clicked and there is no suspense amount, if VAT Taxable then VAT Amount + VAT Base Amount must match with Document Amount, If Federal 1099 distribution is specified then its sum must match with document amount.</td></tr>
<tr><td>Supplier</td><td>Lookup(Account)</td><td>A company or individual that supplies materials or services</td></tr>
<tr><td>Supplier Document</td><td>Text(80)</td><td>A supplier document number or identifier.</td></tr>
<tr><td>Suspense Amount</td><td>Formula (Currency)</td><td>The amount not yet voucherd or distributed. Document Amount ? Voucher Amount ? GL Amount ? Total VAT Amount</td></tr>
<tr><td>Total Distribution Amount</td><td>Formula (Currency)</td><td>Sum of GL Distribution Amount + Voucher Distribution Amount + Total VAT Amount</td></tr>
<tr><td>Total VAT Amount</td><td>Formula (Currency)</td><td>Voucher Distribution VAT Amount + GL Distribution VAT Amount</td></tr>
<tr><td>VAT Exempt</td><td>Checkbox</td><td>When Checked Document is exempted from VAT. Set to true by default if VAT exempt reason is specified for the supplier</td></tr>
<tr><td>VAT Exempt Reason</td><td>Text(80)</td><td>The VAT exempt reason, this field must be entered if VAT exempt flag is checked.</td></tr>
<tr><td>VAT Registration</td><td>Text(80)</td><td>The value added tax registration number that identifies a customer location to governmental agencies.</td></tr>
<tr><td>VAT Taxable</td><td>Checkbox</td><td>if Checked, AP Invoice is VAT Taxable. This flag can be checked only when VAT is enabled for the org</td></tr>
<tr><td>Voucher Amount</td><td>Roll-Up Summary (SUM AP Invoice Voucher Distribution)</td><td>The amount voucherd to order receipts.</td></tr>
<tr><td>Voucher Distribution VAT Amount</td><td>Roll-Up Summary (SUM AP Invoice Voucher Distribution)</td><td>The total amount of value added tax owed from voucher distribution.</td></tr>
</tbody></table></div><br/><br/>
<span style="font-size: 10pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;;">
<table style="font-family: Arial; font-weight: bold; color: white; width: 100%; height: 20px;">
<tbody>
<tr><td style="background-color: rgb(115, 126, 150);">Error Messages of AP Invoice</td></tr></tbody></table></span>
<div class="p">
<table summary="" class="featureTable" cellpadding="4" cellspacing="0" width="100%">
<thead align="left">
<tr>
<th class="featureTableHeader" valign="top" width="200">Error Message</th>
<th class="featureTableHeader" valign="top">Source</th>
<th class="featureTableHeader" valign="top">Condition</th></tr></thead><tbody>
<tr><td>Cannot add VAT distributions, VAT is exempted for this document.</td><td>Apex Trigger</td><td>When VAT exempt flag is set for the document and VAT amount is specified for voucher or GL distributions.</td></tr>
<tr><td>Cannot delete AP Invoice with Voucher Distributions.</td><td>Apex Trigger </td><td>When Invoice voucher distribution exists and AP Invoice is being deleted</td></tr>
<tr><td>Cannot maintain AP Invoice, Invoice is already posted.</td><td>Validation Rule </td><td>When Invoice Posting Date exists and value for any of the following field is changed: AGC, Document Amount, Supplier, Document Date, Due Date, Payment Terms, Supplier Document, Suspense, Amount, VAT Exempt, VAT Exempt Reason, VAT Registration, VAT Taxable or Federal 1099 Code.</td></tr>
<tr><td>Cannot maintain AP Invoice, Invoice is already posted.</td><td>Apex Trigger </td><td>When AP Invoice Posting Date exists and AP Invoice is being deleted</td></tr>
<tr><td>System policy record is missing for glovia OM Application or you may not have access to it. Please contact your system administrator.</td><td>Apex Trigger</td><td>System policy record does not exist.</td></tr>
<tr><td>VAT Exempt Reason must be specified, when document is VAT Exempted.</td><td>Validation Rule</td><td>When VAT Exempt Reason is blank and VAT Exempt flag is checked.</td></tr>
<tr><td>VAT related data fields should be blank for this AP Invoice; VAT is not enabled for this org.</td><td>Apex Trigger</td><td>When VAT is not enabled in system policy and any of the VAT fields are not blank.</td></tr>
<tr><td>VAT Taxable flag cannot be removed for this Invoice, VAT Distribution details exists</td><td>Validation Rule </td><td>When VAT Taxable flag is unchecked and VAT amount exists</td></tr>
</tbody></table></div><br/><br/>
</body>
</apex:outputpanel>
<apex:outputpanel rendered="{!IF(Lang = 'ja',true,false)}">
<head>
<meta http-equiv="content-type" content="text/html; charset=ISO-8859-1"/>
<title>
AP Invoice
</title>
</head>
<body>
<table style="font-family: Arial; width: 100%; height: 52px;">
<tbody>
<tr style="color: white;">
<td style="background-color: rgb(115, 126, 150);"><big><b>AP Invoice Home</b></big></td></tr></tbody></table><br/>
<span style="font-size: 10pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;;">
<table style="font-family: Arial; font-weight: bold; color: white; width: 100%; height: 20px;">
<tbody>
<tr><td style="background-color: rgb(115, 126, 150);">What is a AP Invoice</td></tr></tbody></table></span>
<p class="MsoNormal">AP Invoice maintains a record of supplier invoices for the purchase of goods and services for your business.</p>
<p class="MsoNormal">AP Invoice can be created with voucher distributions for purchase order receipts and General Ledger distributions.</p>
<fieldset class="note">
<legend><strong>Note</strong></legend>
<div class="noteBody">
<img src="/img/help/helpNote_icon.gif" align="left"/>
<p class="MsoNormal">Posting Date must be marked as read only field in all page layouts. When user clicks on “Post AP Invoice” and AP Invoice is fully distributed then system sets this as today’s date. 
When user clicks on Reverse Posting button then this date is set to blank.</p></div></fieldset>
<br/>
<span style="font-size: 10pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;;">
<table style="font-family: Arial; font-weight: bold; color: white; width: 100%; height: 20px;">
<tbody>
<tr><td style="background-color: rgb(115, 126, 150);">Creating AP Invoice</td></tr></tbody></table></span>
<p class="MsoNormal">To create a new AP Invoice, select AP Invoice from the Create New drop-down list in the sidebar, or click New next to Recent AP Invoices on the AP Invoices home page. Enter the information for the AP Invoice. Click Save when you are finished, or click Save and New to save the current AP Invoice and add another.</p>
<br/>
<b>See Also:</b><br/>
<span style="font-size: 9pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;;">
<apex:outputlink value="Help_APInvoiceVoucherDistribution">AP Invoice Voucher Distribution</apex:outputlink></span><br/>
<span style="font-size: 9pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;;">
<apex:outputlink value="Help_APInvoiceGeneralLedgerDistribution">AP Invoice General Ledger Distribution</apex:outputlink></span><br/>
<br/>
<span style="font-size: 10pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;;">
<table style="font-family: Arial; font-weight: bold; color: white; width: 100%; height: 20px;">
<tbody>
<tr><td style="background-color: rgb(115, 126, 150);">Buttons of AP Invoice</td></tr></tbody></table></span>
<div class="p">
<table summary="" class="featureTable" cellpadding="4" cellspacing="0" width="100%">
<thead align="left">
<tr>
<th class="featureTableHeader" valign="top" width="200">Button</th>
<th class="featureTableHeader" valign="top">Description</th></tr></thead><tbody>
<tr><td>Post</td><td>Validate AP Invoice and set the posting date. If “Create Staging Purchase Invoice Records” policy is set then Staging Purchase Invoice Records will also be created. Staging journal line item records will be created for the PPV2 for any AP Invoice voucher distributions.</td></tr>
<tr><td>Reverse Posting</td><td>Reset the posting date. If Staging Purchase Invoice Records were created and are already processed or Staging journal line item records were created and are already processed, then AP Invoice posting cannot be reversed.</td></tr>
<tr><td>Voucher Purchase Order Receipt</td><td>Add voucher distribution details from all unvouchered lines of a purchase order receipt.</td></tr>
</tbody></table></div><br/><br/>
<span style="font-size: 10pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;;">
<table style="font-family: Arial; font-weight: bold; color: white; width: 100%; height: 20px;">
<tbody>
<tr><td style="background-color: rgb(115, 126, 150);">Fields of AP Invoice</td></tr></tbody></table></span>
<div class="p">
<table summary="" class="featureTable" cellpadding="4" cellspacing="0" width="100%">
<thead align="left">
<tr>
<th class="featureTableHeader" valign="top" width="200">Field Label</th>
<th class="featureTableHeader" valign="top">Data Type</th>
<th class="featureTableHeader" valign="top">Description</th></tr></thead><tbody>
<tr><td>AP Invoice</td><td>Auto Number</td><td>AP Invoice number auto generated in following format APINV-{YYYYMMDD}-{000000}</td></tr>
<tr><td>Accounting Group Code</td><td>Picklist</td><td>The Account Group Code</td></tr>
<tr><td>Document Amount</td><td>Currency(16, 2)</td><td>The document amount.</td></tr>
<tr><td>Document Date</td><td>Date</td><td>The document date is used to calculate the payment terms net due date.</td></tr>
<tr><td>Document Text</td><td>Long Text Area(32000)</td><td>The document text to record any memo or notes.</td></tr>
<tr><td>Due Date</td><td>Date</td><td>The day payment in full is due. This date can be overwritten; if null then it is calculated based on the net dues days of payment terms.</td></tr>
<tr><td>Federal 1099 Code</td><td>Lookup(Federal 1099 Code)</td><td>Code identifying box numbers on a federal 1099 form.</td></tr>
<tr><td>General Ledger Amount</td><td>Roll-Up Summary (SUM AP Invoice General Ledger Distribution)</td><td>The amount distributed to general ledger accounts.</td></tr>
<tr><td>GL Distribution VAT Amount</td><td>Roll-Up Summary (SUM AP Invoice General Ledger Distribution)</td><td>The total amount of value added tax owed from GL distribution.</td></tr>
<tr><td>Payment Terms</td><td>Lookup(Payment Term)</td><td>Payment term codes determine the due date for payment. </td></tr>
<tr><td>Posting Date</td><td>Date</td><td>AP Invoice Posting Date. This date is set when post button is clicked and there is no suspense amount, if VAT Taxable then VAT Amount + VAT Base Amount must match with Document Amount, If Federal 1099 distribution is specified then its sum must match with document amount.</td></tr>
<tr><td>Supplier</td><td>Lookup(Account)</td><td>A company or individual that supplies materials or services</td></tr>
<tr><td>Supplier Document</td><td>Text(80)</td><td>A supplier document number or identifier.</td></tr>
<tr><td>Suspense Amount</td><td>Formula (Currency)</td><td>The amount not yet voucherd or distributed. Document Amount ? Voucher Amount ? GL Amount ? Total VAT Amount</td></tr>
<tr><td>Total Distribution Amount</td><td>Formula (Currency)</td><td>Sum of GL Distribution Amount + Voucher Distribution Amount + Total VAT Amount</td></tr>
<tr><td>Total VAT Amount</td><td>Formula (Currency)</td><td>Voucher Distribution VAT Amount + GL Distribution VAT Amount</td></tr>
<tr><td>VAT Exempt</td><td>Checkbox</td><td>When Checked Document is exempted from VAT. Set to true by default if VAT exempt reason is specified for the supplier</td></tr>
<tr><td>VAT Exempt Reason</td><td>Text(80)</td><td>The VAT exempt reason, this field must be entered if VAT exempt flag is checked.</td></tr>
<tr><td>VAT Registration</td><td>Text(80)</td><td>The value added tax registration number that identifies a customer location to governmental agencies.</td></tr>
<tr><td>VAT Taxable</td><td>Checkbox</td><td>if Checked, AP Invoice is VAT Taxable. This flag can be checked only when VAT is enabled for the org</td></tr>
<tr><td>Voucher Amount</td><td>Roll-Up Summary (SUM AP Invoice Voucher Distribution)</td><td>The amount voucherd to order receipts.</td></tr>
<tr><td>Voucher Distribution VAT Amount</td><td>Roll-Up Summary (SUM AP Invoice Voucher Distribution)</td><td>The total amount of value added tax owed from voucher distribution.</td></tr>
</tbody></table></div><br/><br/>
<span style="font-size: 10pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;;">
<table style="font-family: Arial; font-weight: bold; color: white; width: 100%; height: 20px;">
<tbody>
<tr><td style="background-color: rgb(115, 126, 150);">Error Messages of AP Invoice</td></tr></tbody></table></span>
<div class="p">
<table summary="" class="featureTable" cellpadding="4" cellspacing="0" width="100%">
<thead align="left">
<tr>
<th class="featureTableHeader" valign="top" width="200">Error Message</th>
<th class="featureTableHeader" valign="top">Source</th>
<th class="featureTableHeader" valign="top">Condition</th></tr></thead><tbody>
<tr><td>Cannot add VAT distributions, VAT is exempted for this document.</td><td>Apex Trigger</td><td>When VAT exempt flag is set for the document and VAT amount is specified for voucher or GL distributions.</td></tr>
<tr><td>Cannot delete AP Invoice with Voucher Distributions.</td><td>Apex Trigger </td><td>When Invoice voucher distribution exists and AP Invoice is being deleted</td></tr>
<tr><td>Cannot maintain AP Invoice, Invoice is already posted.</td><td>Validation Rule </td><td>When Invoice Posting Date exists and value for any of the following field is changed: AGC, Document Amount, Supplier, Document Date, Due Date, Payment Terms, Supplier Document, Suspense, Amount, VAT Exempt, VAT Exempt Reason, VAT Registration, VAT Taxable or Federal 1099 Code.</td></tr>
<tr><td>Cannot maintain AP Invoice, Invoice is already posted.</td><td>Apex Trigger </td><td>When AP Invoice Posting Date exists and AP Invoice is being deleted</td></tr>
<tr><td>System policy record is missing for glovia OM Application or you may not have access to it. Please contact your system administrator.</td><td>Apex Trigger</td><td>System policy record does not exist.</td></tr>
<tr><td>VAT Exempt Reason must be specified, when document is VAT Exempted.</td><td>Validation Rule</td><td>When VAT Exempt Reason is blank and VAT Exempt flag is checked.</td></tr>
<tr><td>VAT related data fields should be blank for this AP Invoice; VAT is not enabled for this org.</td><td>Apex Trigger</td><td>When VAT is not enabled in system policy and any of the VAT fields are not blank.</td></tr>
<tr><td>VAT Taxable flag cannot be removed for this Invoice, VAT Distribution details exists</td><td>Validation Rule </td><td>When VAT Taxable flag is unchecked and VAT amount exists</td></tr>
</tbody></table></div><br/><br/>
</body>
</apex:outputpanel>
<div class="helpPageFooter">
Copyright  2008-{!YEAR( TODAY() )} Glovia International, inc. All rights reserved.<br/>
Various trademarks held by their respective owners.
</div>
</apex:page>
-->
<!-- Generation Date & Time: 2/7/2011 9:27:36 PM PST -->