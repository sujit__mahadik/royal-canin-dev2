<apex:page controller="gii.PurchaseOrderPDFExtensionCon"
    showheader="false" renderAs="PDF">
    <apex:stylesheet value="{!URLFOR($Resource.gii__gloviaOM_Resources, 'CSS/JP_gloviaCSS.css')}" />
    <!-- v10.5 added while background style to avoid LE issue for blue background -->
    <div style="background-color:white"> 
    <apex:panelGrid columns="2" width="100%" id="header"
        styleClass="tableLayoutFixed">

        <apex:panelGrid columns="2" styleClass="companyTable tableLayoutFixed"
            width="85%">
            <img src="{!sitePrefix}/servlet/servlet.FileDownload?file={!LogoId}" alt="logo"
                border="0" width="100" height="50" />
            <apex:panelGroup >
                <apex:outputText value="{!$Organization.Name}"
                    styleClass="companyName tableLayoutFixed" /><br />
                <apex:outputText value="〒{!$Organization.PostalCode}"/><br/>                
                <apex:outputText value="{!$Organization.State}{!$Organization.City}{!$Organization.Street}" rendered="{!NOT($ObjectType.Organization.accessible)}" /><br />
                <apex:outputText value="{!$Organization.Phone}" /><br /> 
            </apex:panelGroup>
        </apex:panelGrid>

        <apex:panelGroup styleClass="tableLayoutFixed">
            <apex:outputtext value="{!$Label.gii__purchaseorder}"
                styleClass="title right tableLayoutFixed" />
        </apex:panelGroup>

        <!-- Supplier Address-->
        <apex:panelGroup styleClass="tableLayoutFixed">
            <apex:outputtext value="{!$ObjectType.gii__PurchaseOrder__c.Fields.gii__Supplier__c.label}"
                styleClass="companyName tableLayoutFixed" />
            <br />
            〒{!PO.gii__SupplierZipPostalcode__c} <br />
            {!PO.gii__SupplierStateProvince__c}{!PO.gii__SupplierCity__c}{!PO.gii__SupplierStreet__c}<br />
            {!PO.gii__SupplierName__c}<br />
            <br />
            </apex:panelGroup>

        <!-- Purchase Order Header Details -->
        <apex:panelGrid columns="2" width="100%" cellspacing="0"
            styleClass="tableLayoutFixed bordersTopBottom"
            columnClasses="columnBorders left">

            <apex:outputtext value="{!$ObjectType.gii__PurchaseOrder__c.Fields.Name.label}" />
            <apex:outputtext value="{!PO.Name}" />
            <apex:outputtext value="{!$Label.gii__printdate}" />
            <apex:outputtext value="{!CurrentDateTimeinUserLocale}" />
            <apex:outputtext value="{!$Label.gii__currency}"
                rendered="{!UsingMultipleCurrencies}" />
            <apex:outputtext value="{!currencyCode}"
                rendered="{!UsingMultipleCurrencies}" />
            <apex:outputtext value="{!$ObjectType.gii__PurchaseOrder__c.Fields.gii__PaymentTerms__c.label}" />
            <apex:outputField value="{!PO.gii__PaymentTerms__c}" />
            <!-- ** Uncomment following line if AP Payment Terms name needs to be printed  
            and comment out the Payment terms picklist field for sales order header  **
        <apex:outputField value="{!PO.gii__APPaymentTerms__r.name}"/><br/>
        -->
            <apex:outputtext value="{!$ObjectType.gii__PurchaseOrder__c.Fields.gii__DeliveryTerms__c.label}" />
        {!IF(PO.gii__DeliveryTerms__c = null,'',PO.gii__DeliveryTerms__c)}
        <apex:outputtext value="{!$ObjectType.gii__PurchaseOrder__c.Fields.gii__Carrier__c.label}" />
            <apex:outputField value="{!PO.gii__carrier__r.Name}" />
            <apex:outputtext value="{!$ObjectType.gii__PurchaseOrder__c.Fields.gii__Buyer__c.label}" />
            <apex:outputField value="{!PO.gii__Buyer__r.Name}" />
            <apex:outputtext value="{!$ObjectType.gii__PurchaseOrder__c.Fields.gii__SupplierContact__c.label}" />
            <apex:outputField value="{!PO.gii__SupplierContact__r.Name}" />
        </apex:panelGrid>
    </apex:panelGrid>

    <apex:panelGrid columns="2" width="100%" border="0" frame="box"
        styleClass="tableLayoutFixed">
        <!-- Ship To Address -->
        <apex:panelGroup >
            <br />
            <apex:outputtext value="{!$Label.gii__shipto}"
                styleClass="companyName tableLayoutFixed" />
            <br />           
            〒{!PO.gii__ShipToZipPostalCode__c} <br />
            {!PO.gii__ShipToStateProvince__c}{!PO.gii__ShipToCity__c}{!PO.gii__ShipToStreet__c}<br />
            {!PO.gii__ShipToName__c}<br />  
            <br />
        </apex:panelGroup>

        <!-- Invoice To Address -->
        <apex:panelGroup >
            <apex:outputtext value="{!$Label.gii__invoiceto}"
                styleClass="companyName tableLayoutFixed" />
            <br />
            〒{!PO.gii__InvoiceToZipPostalCode__c} <br />
            {!PO.gii__InvoiceToStateProvince__c}{!PO.gii__InvoiceToCity__c}{!PO.gii__InvoiceToStreet__c}<br />
            {!PO.gii__InvoiceToName__c}<br />
            <br />
        </apex:panelGroup>      
    </apex:panelGrid>

    <b><apex:outputtext value="{!$Label.gii__details}"
            styleClass="companyName" /></b>
    <br />

    <!-- Purchase Order Line Details -->
    <apex:panelGrid width="100%" id="Detail">
        <apex:dataTable width="100%" id="DataTable" value="{!POLines}"
            var="POLine" styleClass="tableLayoutFixed font10 verticalTop"
            headerClass="tableLayoutFixed" border="1" frame="box"
            rendered="{!POLinesExists}" columnsWidth="6%,,12%,3%,11%,12%,12%">
            <apex:column headervalue="{!$Label.gii__line_column_heading}"
                headerClass="left" styleClass="left">
                <apex:outputField value="{!POLine.Name}" />
            </apex:column>
            <!-- Purchase Order Line Product-->
            <apex:column headerClass="left" styleClass="left">
                <apex:facet name="header">
            {!$ObjectType.gii__PurchaseOrderLine__c.Fields.gii__Product__c.label}
      </apex:facet>
        {!IF(POLine.gii__Product__r.name = null,POLine.gii__Product__c, POLine.gii__Product__r.name)}<br />
                <apex:outputField value="{!POLine.gii__LineDescription__c}" />
                <b> {!IF(POLine.gii__Product__r.gii__ProductCode__c =
                    null,"",POLine.gii__Product__r.gii__ProductCode__c )}</b>
                <br />
                <apex:outputPanel rendered="{!POLine.gii__Service__c}">
            {!$ObjectType.gii__PurchaseOrderLine__c.Fields.gii__ServiceStartDate__c.label} : 
            <apex:outputField value="{!POLine.gii__ServiceStartDate__c}" />
            &nbsp;&nbsp;
            {!$ObjectType.gii__PurchaseOrderLine__c.Fields.gii__ServiceEndDate__c.label} :
            <apex:outputField value="{!POLine.gii__ServiceEndDate__c}" />
                </apex:outputPanel>
            </apex:column>
            <apex:column headerClass="left" styleClass="left">
                <apex:facet name="header">{!$ObjectType.gii__PurchaseOrderLine__c.Fields.gii__StockUM__c.label} </apex:facet>
                <apex:outputText value="{!POLine.gii__StockUM__c}" 
                                        rendered="{!IF(POLine.gii__BuyingUnitofMeasure__c <> null,false,true)}"/>
                <apex:outputText value="{!POLine.BuyingUnitofMeasure__r.name}" 
                                        rendered="{!IF(POLine.gii__BuyingUnitofMeasure__c <> null,true,false)}"/>     
                <br />&nbsp;
       </apex:column>
            <apex:column headerClass="left" styleClass="left">
                <apex:facet name="header">{!$Label.gii__tax} </apex:facet>
                <!-- <apex:outputField value="{!POLine.gii__Taxable__c}"/>-->
               {!IF(POLine.gii__Taxable__c,"Y"," ")}
       </apex:column>
            <apex:column styleClass="right" headerClass="right"
                headervalue="{!$ObjectType.gii__PurchaseOrderLine__c.Fields.gii__OrderQuantity__c.label}">
                <apex:outputField value="{!POLine.gii__OrderQuantity__c}" />
                <apex:outputPanel rendered="{!IF(POLine.gii__CancelReason__c <> null, true, false)}">
                    <br />**{!$Label.CANCELLED}**
           </apex:outputPanel>
            </apex:column>
            <apex:column styleClass="right" headerClass="right"
                headervalue="{!$ObjectType.gii__PurchaseOrderLine__c.Fields.gii__UnitPrice__c.label}">
                <apex:outputField rendered="{!NOT(UsingMultipleCurrencies)}"
                    value="{!POLine.gii__UnitPrice__c}" />
                <apex:outputText rendered="{!UsingMultipleCurrencies}"
                    value="{!FORMATCURRENCY(currencyCode,POLine.gii__UnitPrice__c)}" />
            </apex:column>
            <apex:column styleClass="right" headerClass="right"
                headervalue="{!$Label.gii__extension}">
                <apex:outputField rendered="{!NOT(UsingMultipleCurrencies)}"
                    value="{!POLine.gii__LineNetAmount__c}" />
                <apex:outputText rendered="{!UsingMultipleCurrencies}"
                    value="{!FORMATCURRENCY(currencyCode,POLine.gii__LineNetAmount__c)}" />
            </apex:column>
        </apex:dataTable>

        <!--   Total Order Gross Amount before discount 
 
  <apex:panelGrid columns="2" width="100%" columnClasses="right"  border="0" 
  styleClass="tableLayoutFixed" rendered="{!IF(PO.gii__GrossAmount__c > 0 ,true,false)}">
       <apex:outputText value="{!$Label.LineSubTotal}" />
       <apex:outputField rendered="{!NOT(UsingMultipleCurrencies)}" value="{!PO.gii__GrossAmount__c}"/>
       <apex:outputText rendered="{!UsingMultipleCurrencies}" value="{!FORMATCURRENCY(currencyCode,PO.gii__GrossAmount__c)}"/>
  </apex:panelGrid>


  <apex:panelGrid columns="2" width="100%" columnClasses="right" border="0"
      rendered="{!IF(PO.gii__SupplierDiscountAmount__c = 0,false,true)}"   styleClass="tableLayoutFixed" >
      <apex:outputText value="{!$Label.OrderDiscount}" />
      <apex:outputField rendered="{!NOT(UsingMultipleCurrencies)}" value="{!PO.gii__SupplierDiscountAmount__c}"/>
      <apex:outputText rendered="{!UsingMultipleCurrencies}" value="{!FORMATCURRENCY(currencyCode,PO.gii__SupplierDiscountAmount__c)}"/>
  </apex:panelGrid>

  -->
        <!-- Order total Amount with discount -->

        <br />
        <apex:panelGrid columns="2" width="100%"
            columnClasses="subtotal right" border="0"
            styleClass="subtotal tableLayoutFixed">
            <apex:outputText value="{!$Label.gii__ordertotal}" />
            <apex:outputField rendered="{!NOT(UsingMultipleCurrencies)}"
                value="{!PO.gii__OrderAmount__c}" />
            <apex:outputText rendered="{!UsingMultipleCurrencies}"
                value="{!FORMATCURRENCY(currencyCode,PO.gii__OrderAmount__c)}" />
        </apex:panelGrid>
    </apex:panelGrid>

    <!-- Purchase Order Document Text -->
    <apex:panelGrid columns="1">
        <apex:panelGroup rendered="{!PO.gii__PrintDocumentText__c}">
            <br />
            <apex:outputField value="{!PO.gii__DocumentText__c}" />
        </apex:panelGroup>
    </apex:panelGrid>
    </div>
</apex:page>