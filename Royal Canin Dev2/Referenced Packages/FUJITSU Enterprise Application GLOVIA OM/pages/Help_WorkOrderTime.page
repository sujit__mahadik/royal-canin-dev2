<apex:page Controller="gii.HelpGetLangCon" showheader="false" sidebar="false" 
    action="{!URLFOR($Resource.gloviaOM_Help, Lang + '/WorkOrderTime.htm')}" >
</apex:page>

<!--
<apex:page Controller="gii.HelpGetLangCon" showheader="false" sidebar="false">
<link rel="stylesheet" type="text/css" href="help.css"/>
<link rel="stylesheet" type="text/css" href="/sCSS/Theme2/default/help.css"/>
<apex:outputpanel rendered="{!IF(Lang != 'ja',true,false)}">
<head>
<meta http-equiv="content-type" content="text/html; charset=ISO-8859-1"/>
<title>
Work Order Time
</title>
</head>
<body>
<table style="font-family: Arial; width: 100%; height: 52px;">
<tbody>
<tr style="color: white;">
<td style="background-color: rgb(115, 126, 150);"><big><b>Work Order Time Home</b></big></td></tr></tbody></table><br/>
<span style="font-size: 10pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;;">
<table style="font-family: Arial; font-weight: bold; color: white; width: 100%; height: 20px;">
<tbody>
<tr><td style="background-color: rgb(115, 126, 150);">What is a Work Order Time</td></tr></tbody></table></span>
<p class="MsoNormal">Glovia OM provides a very flexible structure to record the work order time and cost. Work order time entry may be recorded with or without a work order routing. It can be recorded against a resource which can be an employee, machine, and tool or outside processing supplier. You can also specify the work center to calculate actual cost for setup, run and its over head based on the work center rates. You can also enter the outside processing cost. You may also record any cost adjustments.</p>
<fieldset class="note">
<legend><strong>Note</strong></legend>
<div class="noteBody">
<img src="/img/help/helpNote_icon.gif" align="left"/>
<p class="MsoNormal">Work Order time record cannot be deleted but these entries can be reversed by entering another entry with negative values.</p></div></fieldset>
<fieldset class="note">
<legend><strong>Note</strong></legend>
<div class="noteBody">
<img src="/img/help/helpNote_icon.gif" align="left"/>
<p class="MsoNormal">When enabled staging journal entry lines records are created for work order time entries.</p></div></fieldset>
<br/>
<span style="font-size: 10pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;;">
<table style="font-family: Arial; font-weight: bold; color: white; width: 100%; height: 20px;">
<tbody>
<tr><td style="background-color: rgb(115, 126, 150);">Fields of Work Order Time</td></tr></tbody></table></span>
<div class="p">
<table summary="" class="featureTable" cellpadding="4" cellspacing="0" width="100%">
<thead align="left">
<tr>
<th class="featureTableHeader" valign="top" width="200">Field Label</th>
<th class="featureTableHeader" valign="top">Data Type</th>
<th class="featureTableHeader" valign="top">Description</th></tr></thead><tbody>
<tr><td>Work Order Time</td><td>Auto Number</td><td>The Work Order Time identifier. (Display Format {000000})</td></tr>
<tr><td>Adjustment Cost</td><td>Number(13, 4)</td><td>The adjustment cost can be entered by user to make any work order cost adjustments. </td></tr>
<tr><td>Completed</td><td>Checkbox</td><td>Indicates if routing operation is completed.</td></tr>
<tr><td>Completed Quantity</td><td>Number(10, 2)</td><td>The quantity completed for routing operation.</td></tr>
<tr><td>Operation</td><td>Lookup(Operation)</td><td>An operation is a label for the manufacturing activity performed for a routing operation. Defaulted from work order routing</td></tr>
<tr><td>Outside Processing Cost</td><td>Number(13, 4)</td><td>The Outside Processing Cost. Defaulted from work order routing</td></tr>
<tr><td>Overhead Cost</td><td>Formula (Number)</td><td>The overhead cost is calculated with overhead percentage from the work center for the sum of run cost and setup cost.</td></tr>
<tr><td>Overhead Percent</td><td>Percent(3, 2)</td><td>Overhead percentage is defaulted from work center.</td></tr>
<tr><td>Purchase Order</td><td>Lookup(Purchase Order)</td><td>Purchase order number</td></tr>
<tr><td>Purchase Order Line</td><td>Lookup(Purchase Order Line)</td><td>Purchase order line number</td></tr>
<tr><td>Reference</td><td>Text Area(255)</td><td>Reference or notes for this transaction.</td></tr>
<tr><td>Resource</td><td>Lookup(Resource)</td><td>Resource that was used to conduct this operation.</td></tr>
<tr><td>Run Cost</td><td>Formula (Number)</td><td>Run cost is calculated with run rate from the work center times to run time hours.</td></tr>
<tr><td>Run End Time</td><td>Date/Time</td><td>The start date/time to setup this operation</td></tr>
<tr><td>Run Rate</td><td>Number(7, 4)</td><td>Defaulted from work center</td></tr>
<tr><td>Run Start Time</td><td>Date/Time</td><td>The start date/time to run this operation</td></tr>
<tr><td>Run Time</td><td>Number(7, 5)</td><td>The number of hours the resource worked to run this operation. Run time is calculated when start and end date time is specified.</td></tr>
<tr><td>Setup Cost</td><td>Formula (Number)</td><td>Setup cost is calculated with run rate from the work center times to run time hours.</td></tr>
<tr><td>Setup End Time</td><td>Date/Time</td><td>The end date/time to setup this operation</td></tr>
<tr><td>Setup Rate</td><td>Number(7, 4)</td><td>Defaulted from work center</td></tr>
<tr><td>Setup Start Time</td><td>Date/Time</td><td>The start date/time to setup this operation</td></tr>
<tr><td>Setup Time</td><td>Number(7, 5)</td><td>The number of hours the resource worked to setup this operation. Setup time is calculated when start and end date time is specified.</td></tr>
<tr><td>Total Time Cost</td><td>Formula (Number)</td><td></td></tr>
<tr><td>Transaction Date</td><td>Date</td><td>The transaction for work order operation. Default value is today’s date. </td></tr>
<tr><td>Work Center</td><td>Lookup(Work Center)</td><td>Defaulted from work order routing or operation</td></tr>
<tr><td>Work Order</td><td>Master-Detail(Work Order)</td><td>The work order number or identifier.</td></tr>
<tr><td>Work Order Routing</td><td>Lookup(Work Order Routing)</td><td>The work order routing number or identifier.</td></tr>
</tbody></table></div><br/><br/>
<span style="font-size: 10pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;;">
<table style="font-family: Arial; font-weight: bold; color: white; width: 100%; height: 20px;">
<tbody>
<tr><td style="background-color: rgb(115, 126, 150);">Error Messages of Work Order Time</td></tr></tbody></table></span>
<div class="p">
<table summary="" class="featureTable" cellpadding="4" cellspacing="0" width="100%">
<thead align="left">
<tr>
<th class="featureTableHeader" valign="top" width="200">Error Message</th>
<th class="featureTableHeader" valign="top">Source</th>
<th class="featureTableHeader" valign="top">Condition</th></tr></thead><tbody>
<tr><td>Cannot change work order time record.</td><td>Validation Rule</td><td>When work order time record values are modified.</td></tr>
<tr><td>Invalid entry - Work Order is already closed.</td><td>Validation Rule</td><td>When work order closed field is checked.</td></tr>
<tr><td>Run End Time cannot be less than Run Start Time</td><td>Validation Rule </td><td>Run End Time is  less than Run Start Time</td></tr>
<tr><td>Setup End Time cannot be less than Setup Start Time</td><td>Validation Rule </td><td>Setup End is less than Setup Start Time</td></tr>
<tr><td>Work Order is not Released.</td><td>Validation Rule</td><td>When work order release field is not checked</td></tr>
<tr><td>Work Order time record cannot be deleted.</td><td>Apex Trigger</td><td>When work order time record is deleted</td></tr>
</tbody></table></div><br/><br/>
</body>
</apex:outputpanel>
<apex:outputpanel rendered="{!IF(Lang = 'ja',true,false)}">
<head>
<meta http-equiv="content-type" content="text/html; charset=ISO-8859-1"/>
<title>
Work Order Time
</title>
</head>
<body>
<table style="font-family: Arial; width: 100%; height: 52px;">
<tbody>
<tr style="color: white;">
<td style="background-color: rgb(115, 126, 150);"><big><b>Work Order Time Home</b></big></td></tr></tbody></table><br/>
<span style="font-size: 10pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;;">
<table style="font-family: Arial; font-weight: bold; color: white; width: 100%; height: 20px;">
<tbody>
<tr><td style="background-color: rgb(115, 126, 150);">What is a Work Order Time</td></tr></tbody></table></span>
<p class="MsoNormal">Glovia OM provides a very flexible structure to record the work order time and cost. Work order time entry may be recorded with or without a work order routing. It can be recorded against a resource which can be an employee, machine, and tool or outside processing supplier. You can also specify the work center to calculate actual cost for setup, run and its over head based on the work center rates. You can also enter the outside processing cost. You may also record any cost adjustments.</p>
<fieldset class="note">
<legend><strong>Note</strong></legend>
<div class="noteBody">
<img src="/img/help/helpNote_icon.gif" align="left"/>
<p class="MsoNormal">Work Order time record cannot be deleted but these entries can be reversed by entering another entry with negative values.</p></div></fieldset>
<fieldset class="note">
<legend><strong>Note</strong></legend>
<div class="noteBody">
<img src="/img/help/helpNote_icon.gif" align="left"/>
<p class="MsoNormal">When enabled staging journal entry lines records are created for work order time entries.</p></div></fieldset>
<br/>
<span style="font-size: 10pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;;">
<table style="font-family: Arial; font-weight: bold; color: white; width: 100%; height: 20px;">
<tbody>
<tr><td style="background-color: rgb(115, 126, 150);">Fields of Work Order Time</td></tr></tbody></table></span>
<div class="p">
<table summary="" class="featureTable" cellpadding="4" cellspacing="0" width="100%">
<thead align="left">
<tr>
<th class="featureTableHeader" valign="top" width="200">Field Label</th>
<th class="featureTableHeader" valign="top">Data Type</th>
<th class="featureTableHeader" valign="top">Description</th></tr></thead><tbody>
<tr><td>Work Order Time</td><td>Auto Number</td><td>The Work Order Time identifier. (Display Format {000000})</td></tr>
<tr><td>Adjustment Cost</td><td>Number(13, 4)</td><td>The adjustment cost can be entered by user to make any work order cost adjustments. </td></tr>
<tr><td>Completed</td><td>Checkbox</td><td>Indicates if routing operation is completed.</td></tr>
<tr><td>Completed Quantity</td><td>Number(10, 2)</td><td>The quantity completed for routing operation.</td></tr>
<tr><td>Operation</td><td>Lookup(Operation)</td><td>An operation is a label for the manufacturing activity performed for a routing operation. Defaulted from work order routing</td></tr>
<tr><td>Outside Processing Cost</td><td>Number(13, 4)</td><td>The Outside Processing Cost. Defaulted from work order routing</td></tr>
<tr><td>Overhead Cost</td><td>Formula (Number)</td><td>The overhead cost is calculated with overhead percentage from the work center for the sum of run cost and setup cost.</td></tr>
<tr><td>Overhead Percent</td><td>Percent(3, 2)</td><td>Overhead percentage is defaulted from work center.</td></tr>
<tr><td>Purchase Order</td><td>Lookup(Purchase Order)</td><td>Purchase order number</td></tr>
<tr><td>Purchase Order Line</td><td>Lookup(Purchase Order Line)</td><td>Purchase order line number</td></tr>
<tr><td>Reference</td><td>Text Area(255)</td><td>Reference or notes for this transaction.</td></tr>
<tr><td>Resource</td><td>Lookup(Resource)</td><td>Resource that was used to conduct this operation.</td></tr>
<tr><td>Run Cost</td><td>Formula (Number)</td><td>Run cost is calculated with run rate from the work center times to run time hours.</td></tr>
<tr><td>Run End Time</td><td>Date/Time</td><td>The start date/time to setup this operation</td></tr>
<tr><td>Run Rate</td><td>Number(7, 4)</td><td>Defaulted from work center</td></tr>
<tr><td>Run Start Time</td><td>Date/Time</td><td>The start date/time to run this operation</td></tr>
<tr><td>Run Time</td><td>Number(7, 5)</td><td>The number of hours the resource worked to run this operation. Run time is calculated when start and end date time is specified.</td></tr>
<tr><td>Setup Cost</td><td>Formula (Number)</td><td>Setup cost is calculated with run rate from the work center times to run time hours.</td></tr>
<tr><td>Setup End Time</td><td>Date/Time</td><td>The end date/time to setup this operation</td></tr>
<tr><td>Setup Rate</td><td>Number(7, 4)</td><td>Defaulted from work center</td></tr>
<tr><td>Setup Start Time</td><td>Date/Time</td><td>The start date/time to setup this operation</td></tr>
<tr><td>Setup Time</td><td>Number(7, 5)</td><td>The number of hours the resource worked to setup this operation. Setup time is calculated when start and end date time is specified.</td></tr>
<tr><td>Total Time Cost</td><td>Formula (Number)</td><td></td></tr>
<tr><td>Transaction Date</td><td>Date</td><td>The transaction for work order operation. Default value is today’s date. </td></tr>
<tr><td>Work Center</td><td>Lookup(Work Center)</td><td>Defaulted from work order routing or operation</td></tr>
<tr><td>Work Order</td><td>Master-Detail(Work Order)</td><td>The work order number or identifier.</td></tr>
<tr><td>Work Order Routing</td><td>Lookup(Work Order Routing)</td><td>The work order routing number or identifier.</td></tr>
</tbody></table></div><br/><br/>
<span style="font-size: 10pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;;">
<table style="font-family: Arial; font-weight: bold; color: white; width: 100%; height: 20px;">
<tbody>
<tr><td style="background-color: rgb(115, 126, 150);">Error Messages of Work Order Time</td></tr></tbody></table></span>
<div class="p">
<table summary="" class="featureTable" cellpadding="4" cellspacing="0" width="100%">
<thead align="left">
<tr>
<th class="featureTableHeader" valign="top" width="200">Error Message</th>
<th class="featureTableHeader" valign="top">Source</th>
<th class="featureTableHeader" valign="top">Condition</th></tr></thead><tbody>
<tr><td>Cannot change work order time record.</td><td>Validation Rule</td><td>When work order time record values are modified.</td></tr>
<tr><td>Invalid entry - Work Order is already closed.</td><td>Validation Rule</td><td>When work order closed field is checked.</td></tr>
<tr><td>Run End Time cannot be less than Run Start Time</td><td>Validation Rule </td><td>Run End Time is  less than Run Start Time</td></tr>
<tr><td>Setup End Time cannot be less than Setup Start Time</td><td>Validation Rule </td><td>Setup End is less than Setup Start Time</td></tr>
<tr><td>Work Order is not Released.</td><td>Validation Rule</td><td>When work order release field is not checked</td></tr>
<tr><td>Work Order time record cannot be deleted.</td><td>Apex Trigger</td><td>When work order time record is deleted</td></tr>
</tbody></table></div><br/><br/>
</body>
</apex:outputpanel>
<div class="helpPageFooter">
Copyright  2008-{!YEAR( TODAY() )} Glovia International, inc. All rights reserved.<br/>
Various trademarks held by their respective owners.
</div>
</apex:page>
-->
<!-- Generation Date & Time: 2/7/2011 9:24:13 PM PST -->