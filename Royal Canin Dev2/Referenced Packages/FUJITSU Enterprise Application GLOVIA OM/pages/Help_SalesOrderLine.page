<apex:page Controller="gii.HelpGetLangCon" showheader="false" sidebar="false" 
    action="{!URLFOR($Resource.gloviaOM_Help, Lang + '/SalesOrderLine.htm')}" >
</apex:page>

<!--
<apex:page Controller="gii.HelpGetLangCon" showheader="false" sidebar="false">
<link rel="stylesheet" type="text/css" href="help.css"/>
<link rel="stylesheet" type="text/css" href="/sCSS/Theme2/default/help.css"/>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=ISO-8859-1"/>
<title>
<apex:outputtext value="{!IF(Lang = 'ja',"販売オーダーライン","Sales Order Line")}" />
</title>
</head>

<body>
<table style="font-family: Arial; width: 100%; height: 52px;"> 
<tbody><tr style="color: white;"><td style="background-color: rgb(115, 126, 150);">
<big><b>
<apex:outputtext value="{!IF(Lang = 'ja',"販売オーダーラインホーム","Sales Order Line Home")}" />
</b></big>
</td></tr></tbody> 
</table> 
<br/>

<span style="font-size: 10pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;;">
<table style="font-family: Arial; font-weight: bold; color: white; width: 100%; height: 20px;">
<tbody><tr><td style="background-color: rgb(115, 126, 150);">
<apex:outputtext value="{!IF(Lang = 'ja',"販売オーダーラインとは","What is a Sales Order Line")}" />
</td></tr></tbody>
</table>

<p class="MsoNormal">
<apex:outputtext value="{!IF(Lang = 'ja',"販売オーダーラインは、販売オーダーの関連リストであり、商品名、数量、金額等の商品に関する明細情報で構成されます。","Sales Order Line is one of related lists of Sales Order. Sales Order Lines can contain Product information for Sales Orders such as Product Name, Quantity, Price.")}" /><br/><br/>
</p>
<fieldset class="note"><legend><strong>Note</strong></legend>
<div class="noteBody">
<img src="/img/help/helpNote_icon.gif" align="left"/>
<p class="MsoNormal">
<apex:outputtext value="{!IF(Lang = 'ja',"販売オーダーラインは、在庫品または非在庫品についての明細情報であり、サービス商品については、サービスオーダーラインで明細情報を管理します。","Sales Order Lines are used for Stock or non-Stock items. As for service products, Service Order Lines can contain the line information for service products.")}" /></p>
</div>
</fieldset>
<br/>
<table style="color: white; font-family: Arial; font-weight: bold; height: 20px; width: 100%;">
<tbody><tr><td style="background-color: rgb(115, 126, 150);">
<apex:outputtext value="{!IF(Lang = 'ja',"販売オーダーラインの作成","Creating Sales Order Line")}" />
</td></tr></tbody>
</table>
 
<p class="MsoNormal" style="margin-top: 12pt;"><apex:outputtext value="{!IF(Lang = 'ja',"販売オーダー画面上で、「新規販売オーダーライン」ボタンを押下し、販売オーダーラインを作成します。","A new Sales Order Line can be created by clicking New Sales Order Line button in Sales Order screen.")}" /><br/></p><fieldset class="note"><legend><strong>Note</strong></legend>
<div class="noteBody">
<img src="/img/help/helpNote_icon.gif" align="left"/>
<p class="MsoNormal">
<apex:outputtext value="{!IF(Lang = 'ja',"販売オーダーが商談から変換された場合、商談に含まれている商品のうち、在庫品および非在庫品の商品情報が自動的に販売オーダーラインに変換されます。また、販売オーダーが販売見積から変換された場合、販売見積ラインが販売オーダーラインに変換されます。","When the Sales Order was converted from an Opportunity, Stock and non-Stock products in Opportunity Products are automatically added to Sales Order Lines. When the Sales Order was converted from a Sales Quote, Sales Quote lines are converted to Sales Order Lines.")}" /></p>
</div>
</fieldset>
<br/>
<table style="font-weight: bold; font-family: Arial; color: white; width: 100%; hdight: 20px;">
<tbody><tr><td style="background-color: rgb(115, 126, 150);">
<apex:outputtext value="{!IF(Lang = 'ja',"販売オーダーラインの編集","Editing Sales Order Line")}" />
</td></tr></tbody>
</table>

<p class="MsoNormal" style="margin-top: 12pt;"><apex:outputtext value="{!IF(Lang = 'ja',"販売オーダーラインは、随時編集が可能ですが、請求済等のステータスによっては、編集不可となります。","Sales Order Line can be edited after creation. However, it is not allowed to edit depending on the Status, e.g. Invoiced.")}" /></p><br/><br/>
<b><apex:outputtext value="{!IF(Lang = 'ja', "関連情報：", "See Also:")}" /></b><br/>
<apex:outputlink value="/apex/Help_SalesOrder"><apex:outputtext value="{!IF(Lang = 'ja',"販売オーダー","Sales Order")}" /></apex:outputlink><br/>
<apex:outputlink value="/apex/Help_ServiceOrderLine"><apex:outputtext value="{!IF(Lang = 'ja',"サービスオーダーライン","Service Order Lines")}" /></apex:outputlink><br/>
<br/><br/>
<table style="color: white; font-family: Arial; font-weight: bold; height: 20px; width: 100%;">
<tbody><tr><td style="background-color: rgb(115, 126, 150);">
<apex:outputtext value="{!IF(Lang = 'ja',"販売オーダーラインのボタン／リンク","Buttons/Links of Sales Order Line")}" />
</td></tr></tbody>
</table>

<div class="p">
<table summary="" class="featureTable" cellpadding="4"
 cellspacing="0" width="100%">
<thead align="left"><tr>
<th class="featureTableHeader" valign="top" width="200">
<apex:outputtext value="{!IF(Lang = 'ja',
"ボタン／リンク",
"Button/Link")}" />
</th>
<th class="featureTableHeader" valign="top">
<apex:outputtext value="{!IF(Lang = 'ja',
"説明",
"Description")}" />
</th></tr>
</thead>

<tbody><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"代替/アップセル/クロスセル/キット","Substitute / Up Sell / Cross Sell / Kitting")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"代替商品、アップセル商品、クロスセル商品、キットメンバー商品の追加画面を開く。","Opens a screen to add Substitute, Up Sell, Cross Sell or Kit Member products for the Sales Order Line product.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"請求スケジュール作成","Maintain Invoice Schedules")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"請求スケジュールの作成・編集画面を開く。","Opens a screen to create and maintain Invoice Schedule.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"預託在庫消費シリアル番号","Consumption Product Serials")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"預託在庫消費オーダーの場合に、シリアル番号の登録画面を開く。","Opens a screen to assign serials If the Sales Order is a Consignment Consumption order.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"預託在庫消費ロット番号","Consumption Product Lots")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"預託在庫消費オーダーの場合に、ロット番号の登録画面を開く。","Opens a screen to assign lot If the Sales Order is a Consignment Consumption order.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"数量・在庫場所編集","Maintain Quantity & Warehouse")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"数量および在庫場所を変更するための画面を開く。","Opens a screen to change quantities and Warehouse for the Sales Order Line.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"預託在庫消費","Consume")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"預託在庫補充オーダーの場合に、出荷済の販売オーダーラインの商品に対し、預託在庫消費処理を実行する。","Consumes products in the Consignment Warehouse when the Sales Order is a Consignment Replenishment Order.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"預託在庫返品","Consignment Return")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"預託在庫補充オーダーの場合に、出荷済の販売オーダーラインの商品に対し、預託在庫返品処理を実行する。","Returns products in the Consignment Warehouse when the Sales Order is a Consignment Replenishment Order.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"在庫予約","Inventory Reserve / Clear Backorder")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"在庫予約がされていない販売オーダーラインに対し、在庫予約処理を実行する。また、ステータスが受注残となっている販売オーダーラインに対し、受注残の消し込みを行い在庫予約を実行する。","Reserves Product Inventory for the Sales Order Lines which do not have Inventory Reservations. Or, Clears Backorders and reserves Product Inventories for Sales Order Lines.")}" />
</td>
</tr></tbody>
</table>
</div><br/><br/>
<table style="color: white; font-family: Arial; font-weight: bold; height: 20px; width: 100%;">
<tbody><tr><td style="background-color: rgb(115, 126, 150);">
<apex:outputtext value="{!IF(Lang = 'ja',"販売オーダーラインの項目","Fields of Sales Order Line")}" />
</td></tr></tbody>
</table>

<div class="p">
<table summary="" class="featureTable" cellpadding="4"
 cellspacing="0" width="100%">
<thead align="left"><tr>
<th class="featureTableHeader" valign="top" width="200">
<apex:outputtext value="{!IF(Lang = 'ja',
"項目",
"Field")}" />
</th>
<th class="featureTableHeader" valign="top">
<apex:outputtext value="{!IF(Lang = 'ja',
"説明",
"Description")}" />
</th></tr>
</thead>

<tbody><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"販売オーダーライン番号","Sales Order Line")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"販売オーダーラインの番号","Sales Order Line number. A unique number for the Sales Order Line.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"会計グループコード","Accounting Group Code")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"販売オーダートランザクションの会計コード。販売オーダー（ヘッダー）の値がセットされる。","The code of accounting which Sales Order transactions belongs to.  Defaulted from Sales Order header.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"消費・返品可能数","Available To Consume / Return")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"オーダータイプが預託在庫タイプの場合の処理可能数。預託在庫補充オーダーに対する消費可能数、または、預託在庫消費オーダーに対する返品可能数がセットされる。","The quantity which can be processed for Consignment Orders. Available quantity to consume for Consignment Replenishment Order or available quantity to return for Consignment Consumption Order.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"キャンセル日","Cancel Date")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"販売オーダーラインがキャンセルされた日付","Date of cancellation of Sales Order Line.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"キャンセル数","Cancelled Quantity")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"販売オーダーラインがキャンセルされた数量","Quantity of cancellation of Sales Order Line.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"キャンセル理由","Cancel Reason")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"販売オーダーラインがキャンセルされた理由","Reason of cancellation of Sales Order Line.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"運送会社","Carrier")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"販売オーダー出荷時の運送会社。販売オーダーラインの新規作成時に、販売オーダーの値がセットされる。販売見積からの変換時は、販売見積ラインの情報がセットされる。","Carrier for shipping products. When creating a new Sales Order Line, this field can be defaulted from Sales Order header. When converting a Sales Quote to a Sales Order, this field can be copied from Sales Quote Line.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"預託返品数","Consignment Return Quantity")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"オーダータイプが預託在庫返品の販売オーダーの場合の、預託在庫場所からの返品数。","Total quantity returned from consignment warehouse of sales order line for sales orders with order type \"Consignment Return\".")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"消費数","Consumed Quantity")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"オーダータイプが預託在庫消費の販売オーダーの場合の、預託在庫場所での消費数。","Total Consignment Consumption Quantity of sales order line for sales orders with order type \"Consignment Consumption\".")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"クロスセルライン","Cross Sell Line")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"当該販売オーダーラインが他販売オーダーラインのクロスセルである場合に、他販売オーダーラインのライン番号がセットされる。","When the Sales Order Line is a Cross-sell of another Sales Order Line, this field contains the line number of that Sales Order Line.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"顧客購買オーダー日付","Customer PO Date")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"顧客購買オーダーの日付。販売オーダーの値がセットされる。","The customer Purchase Order Date.  Defaulted from Sales Order.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"顧客購買オーダーライン","Customer PO Line")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"対応する顧客購買オーダーのライン番号。","The customer Purchase Order Line number. ")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"顧客購買オーダー番号","Customer PO Number")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"顧客購買オーダーの番号。販売オーダーの値がセットされる。","The customer Purchase Order number. ")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"値引額","Discount Amount")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"販売オーダーラインの値引額。販売オーダーラインの値引率にもとづき算出される。","The discount amount for Sales Order line. Calculated based on Discount Percent of Sales Order Line.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"値引率","Discount Percent")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"販売オーダーラインの値引率。","Percentage of Discount applicable on price of the Sales Order Line Product.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"物品税課税","Excise Taxable")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"物品税課税を有効化する。販売オーダー追加料金の新規作成時に、取引先関連情報のデフォルト値がセットされる。販売見積からの変換時は、販売見積ラインの情報がセットされる。","If checked, the Sales Order Line is excise taxable. When creating a new Sales Order Line, this field can be defaulted from  Account Reference. When converted from  a Sales Quote, this field can be copied from Sales Quote Line.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"請求書値引額","Invoice Discount Amount")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"請求済の商品値引額","The invoiced discount amount for the Sales Order line.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"請求書オーダー値引額","Invoice Order Discount Amount")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"請求済の販売オーダー全体での値引額。","The invoiced discount amount for the entire order. ")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"請求書商品金額","Invoice Product Amount")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"請求済の商品金額","The invoiced amount of Product on the Sales Order Line.  ")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"請求スケジュール開始日","Invoice Schedule Start Date")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"請求スケジュールを作成する際の初回請求日。","The start date of invoicing when using an Invoice Schedule.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"請求書税額","Invoice Tax Amount")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"請求済の税額の合計。","The invoiced tax amount on Sales Order Line.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"請求書合計額","Invoice Total Amount")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"請求済の値引後の合計金額。","The invoice total amount after the discounts. ")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"請求書VAT額","Invoice VAT Amount")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"請求済のVAT額。","The invoiced VAT amount on Sales Order Line. ")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"キット","Kit")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"キット商品を表すフラグであり、オンの場合は当該商品がキット品であることを示す。「キット」とは、複数の商品（キットメンバー）からなる商品であり、１段階の部品表に類似している。キットは、商品関連情報内の設定で定義される。","If checked, the product is kit product.
Kitting in glovia.com Order Management lets you create group of individual products required for the selling as final single product. The concept is similar to single level Bill of Materials (BOM).
")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"キットライン","Kit Line")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"当該商品がキットメンバー商品である場合に、親キット商品の販売オーダーライン番号がセットされる。","When the Product in Sales Order Line is a Kit Member, this field has the Sales Order line number of the Kit Parent Product.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"キット必須メンバー商品","Kit Product Required")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"当該商品がキットメンバー商品であり、かつキット必須メンバー商品の場合に、フラグがオンにセットされる。","When the Product in Sales Order Line is a mandatory Kit Member, this flag is set on.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"説明","Line Description")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"ラインごとの説明。商談から変換された場合には、商談商品の説明情報が自動的にセットされる。販売見積からの変換時は、販売見積ラインの情報がセットされる。","Line description. When converting from an Opportunity,  this field can be copied from Opportunity Product. When converting from a Sales Quote, this field can be copied from Sales Quote Line.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"正味額","Net Amount")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"商品金額、値引、税額の総合計金額。","Total estimated amount of product amount, discounts and taxes.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"正味請求額","Net Invoice Amount")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"請求済のオーダーライン合計額、追加料金合計額、税額、値引額の総合計金額","Total invoiced amount of Order, Additional Charges and Tax, including discounts.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"正味出荷額","Net Ship Amount")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"出荷済のオーダーライン合計額、追加料金合計額、税額、値引額の総合計金額","Total shipped amount of Order, Additional Charges and Tax, including discounts.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"非請求理由","No Charge Reason")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"非請求とする理由。","No Charge Reason code if the Unit Price of the Sales Order Line Product is zero.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"非在庫品","Non-Stock")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"当該商品が非在庫品の場合にオンとなる。商品関連情報のデフォルト値がセットされる。「非在庫品」とは、在庫管理を必要としない商品であり、商品関連情報のフラグ設定で定義される。非在庫品の場合、販売オーダー処理の際、在庫に関連する一連の処理が割愛されるが、数量把握の目的から在庫予約レコードは生成される。","If checked, the product is non stock type of product. Defaulted from Product Reference.
You can add non stock product as an order line product toSales Order.

For the order line product, a reservation will be created. This reservation will not have impact on inventory.

Process: Non stock products follow the same standard process just like stock products. For example: standard process from pick to ship and quickpack, quick ship as well.

No inventory impact - No Backorder created: while converting: Opportunity to Sales Order, Opportunity to Sales Order, Sales Order to Sales Order there will not be any backorder created. 
")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"分割回数","Number of Installments")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"請求スケジュールが作成された際の、スケジュール全体での請求回数。","Number of invoicing When Invoice Schedule was created.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"オーダー値引額","Order Discount Amount")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"販売オーダー全体での値引額。","The estimated discount amount for the whole order. ")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"オーダー数","Order Quantity")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"販売オーダーラインの商品のオーダー数。","Sales Order Line Product order quantity.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"当初商品","Original Product")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"当該商品がアップセル品または代替品である場合に、アップセルまたは代替前の商品名が保存される。","When the Product is Up-sell or Substitute Product, this field contains the original product name.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"商品","Product")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"販売オーダー対象の商品。","Product name of the product on the Sales Order Line.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"商品金額","Product Amount")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"販売オーダーラインの商品金額。","The total estimated amount of product for line. ")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"約束日","Promise Date")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"販売オーダーラインの約束日。商品のリードタイムおよび要求日にもとづいて計算される。","The promise date for Sales Order Line. Promise Date is calculated based on lead time of the product and required date.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"数量変更可","Quantity Change Allowed")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"当該商品がキットメンバー商品であり、かつ数量変更可能メンバー商品の場合に、フラグがオンにセットされる。","If checked, the quantity change was allowed for the kit member product. ")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"受入数","Receipt Quantity")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"オーダータイプが移動、預託在庫補充、預託在庫返品の場合の、対象となる在庫場所での受入数。","Total quantity received in the target warehouse for the sales order line for sales orders with order type  \"Transfer\", \"Consignment Replenshment\" or \"Consignment Return\".")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"リリース済","Released")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"販売オーダー（ヘッダー）のリリース状態。","Release status of Sales Order header.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"要求日","Required Date")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"販売オーダーラインに対する要求日。販売オーダーが商談から変換された場合は、該当の商談商品の日付が自動的にセットされる。販売オーダーラインを新規に作成した場合は、自動的にセットされるデフォルト値を使用するか、手動で入力する。","The required date for Sales Order Line. When the Sales Order was converted from an Opportunity, the date of Opportunity Product is automatically set. When converted from a Sales Quote, the date of Sales Quote Line is automatically set. When creating a new Sales Order Line, the Required Date of Sales Order can be defaulted, otherwise can be entered manually. ")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"請求間隔","Revenue Schedule Installment Period")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"請求スケジュールが作成された際の、請求頻度。","Installment period  of invoicing When Invoice Schedule was created.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"返品承認ライン","RMA Line")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"返品承認から変換された際の返品承認ライン番号。","When Sales Order was converted from a RMA, this field contains the RMA Line number.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"販売オーダー","Sales Order")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"販売オーダーの番号。","Sales Order number. ")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"販売見積キットライン","Sales Quote Kit Line")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"販売オーダーが販売見積から変換された場合で、販売オーダーラインがキットメンバーである場合、キット親商品を含む販売見積ラインの番号がセットされる。","When the Sales Order Line product is a Kit Member, this field has the Sales Quote line number of the Kit Parent Product when sales order was converted from sales quote.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"販売見積ライン","Sales Quote Line")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"販売見積から変換された際の販売見積ライン番号。","When Sales Order was converted from a Sales Quote, this field contains the Sales Quote Line number.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"売上税課税","Sales Taxable")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"売上税課税を有効化する。販売オーダー追加料金の新規作成時に、取引先関連情報のデフォルト値がセットされる。販売見積からの変換時は、販売見積ラインの情報がセットされる。","If checked, the Sales Order Line is sales taxable. When creating a new Sales Order Line, this field can be defaulted from  Account Reference. When converted from  a Sales Quote, this field can be copied from Sales Quote Line.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"請求スケジュール作成済","Schedule")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"請求スケジュールが作成済の場合にフラグがオンとなる。販売オーダーラインに対して請求スケジュールが作成されると自動的にフラグがセットされる。","If checked, Invoice Schedule has been created. This field is automatically updated when invoice schedules are creted for sales order line.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"計画日","Scheduled Date")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"納入計画日。","The scheduled delivery date")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"請求スケジュールテンプレート","Schedule Template")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"請求スケジュールを作成する際に使用するテンプレート。テンプレートはコードメンテナンス・タブ内で登録されたコードから選択する。取引先関連情報のデフォルト値がセットされる。","Template code which will be used for creating Invoice Schedule. Template codes can be added in Code Maintenance tab. Defaulted from Account Reference.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"出荷値引額","Ship Discount Amount")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"出荷済の商品値引額","The shipped discount amount for Sales Order line.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"出荷オーダー値引額","Ship Order Discount Amount")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"出荷済の商品値引額の合計","The shipped discount amount of Product discount for order discount percentage. ")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"出荷済数","Shipped Quantity")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"出荷済の商品数","The shipped quantity of Product.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"出荷商品金額","Ship Product Amount")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"出荷済の販売オーダーラインの商品金額","The shipped amount of Products on Sales Order Line.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"出荷税額","Ship Tax Amount")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"出荷済の税額の合計。","The shipped tax amount on Sales Order Line.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"出荷VAT額","Ship VAT Amount")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"出荷済のVAT額。","The shipped VAT amount on Sales Order Line. ")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"ステータス","Status")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"販売オーダーラインのステータス","The status of the sales order line. ")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"税額","Tax Amount")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"税額の合計。","The total of estimate tax amount on Sales Order Line.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"税ロケーション","Tax Location")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"課税ロケーション。販売オーダーの値がセットされる。","A code representing a tax location. The tax location code determines the set of tax rate jurisdictions applied to the sale of this product. Defaulted from Sales Order.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"合計金額","Total Amount")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"値引後の合計金額。","The total estimate amount after the discounts. ")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"合計出荷額","Total Ship Amount")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"出荷済の値引後の合計金額。","The shipped total amount after the discounts. ")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"原価","Unit Cost")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"販売オーダーラインの商品の原価。販売オーダーラインの新規作成時に、商品関連情報のデフォルト値がセットされる。販売見積からの変換時は、販売見積ラインの情報がセットされる。","Unit Cost of the Sales Order Line Product. When creating a new Sales Order Line, this field can be defaulted from Product Reference. When converted from a Sales Quote, this field can be copied from the Sales Quote Line.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"単位","Unit Of Measure")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"販売オーダーラインの商品の単位。販売オーダーラインの新規作成時に、商品関連情報のデフォルト値がセットされる。販売見積からの変換時は、販売見積ラインの情報がセットされる。","Unit Of Measure code for Sales Order Line Product. When creating a new Sales Order Line, this field can be defaulted from Product Reference. When converted from a Sales Quote, this field can be copied from the Sales Quote Line.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"単価","Unit Price")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"販売オーダーラインの商品の単価。販売オーダーラインの新規作成時に、販売オーダー（ヘッダー）の価格表の値がセットされる。販売見積からの変換時は、販売見積ラインの情報がセットされる。","Unit Price of the Sales Order Line Product. When creating a new Sales Order Line, this field can be defaulted from Sales Order Price Book. When converted from a Sales Quote, this field can be copied from the Sales Quote Line.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"納入商品更新","Update Asset")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"商品出荷時に取引先納品情報を更新する場合に、フラグをオンにする。商品関連情報のデフォルト値がセットされる。","If checked, updates the asset for the account  when sales order line is shipped. For more help on Assets click on standard Salesforce help link next to Logout, click on Help tab, and under Using the Application section, click on Assets.

")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"利用税課税","Usage Taxable")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"利用税課税を有効化する。販売オーダー追加料金の新規作成時に、取引先関連情報のデフォルト値がセットされる。販売見積からの変換時は、販売見積ラインの情報がセットされる。","If checked, the Sales Order Line is usage taxable. When creating a new Sales Order Line, this field can be defaulted from  Account Reference. When converted from  a Sales Quote, this field can be copied from Sales Quote Line.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"VAT","VAT")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"VAT課税の際のVATコード。販売オーダーの値がセットされる。","VAT code for VAT taxation.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"VAT額","VAT Amount")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"販売オーダーラインのVAT額。","The total Estimate VAT amount on Sales Order Line. ")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"VAT登録番号","VAT Registration")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"VAT登録番号。販売オーダーの値がセットされる。","The VAT registration number. Defaulted from Sales Order.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"VAT課税","VAT Taxable")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"VAT課税を有効化する。販売オーダーの値がセットされる。","If checked, Line is VAT taxable. Defaulted from Sales Order.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"在庫場所","Warehouse")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"商品出荷元となる在庫場所。販売オーダーの値がセットされる。","The warehouse you will ship the inventory from. Defaulted from Sales Order.")}" />
</td>
</tr></tbody>
</table>
</div>
</span></body>

<div class="helpPageFooter">Copyright  2008-{!YEAR( TODAY() )} Glovia International, inc. All rights reserved.<br/>
Various trademarks held by their respective owners.</div>
</html>
</apex:page>
-->
<!-- Generation Date & Time: 5/4/2010 6:32:51 PM PST -->