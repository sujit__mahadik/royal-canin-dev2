<apex:page standardController="gii__SalesOrder__c" extensions="gii.SalesOrderInventoryInquiryExtCon" lightningStylesheets="true">
    <apex:form >
        <apex:sectionHeader title="{!$Label.gii__inventoryinquiryforsalesorderproducts}" subtitle="{!salesOrder.Name}" />
        <apex:pageBlock >
            <apex:pageBlockButtons location="top" >
                <apex:commandButton action="{!cancel}" title="{!$Label.gii__cancel}" value="{!$Label.gii__cancel}" style="margin-left:20%;" />
                <apex:actionStatus id="status" style="margin-left:1%;" >
                    <apex:facet name="start">
                        <apex:image url="/img/loading.gif"/>
                    </apex:facet>
                </apex:actionStatus>
            </apex:pageBlockButtons>
            <apex:pageBlockSection columns="2" collapsible="false">
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="{!$ObjectType.gii__SalesOrder__c.Fields.Name.Label}" for="salesOrder"/>
                    <apex:outputLink value="/{!salesOrder.Id}" id="salesOrder" >
                        {!salesOrder.Name}
                    </apex:outputLink>
                </apex:pageBlockSectionItem>
                
                <apex:outputField value="{!salesOrder.gii__Account__c}" id="accountName" />
                
                <apex:outputField value="{!salesOrder.gii__OrderDate__c}" id="orderDate" />
                
                <apex:outputField value="{!salesOrder.gii__OrderStatus__c}" id="orderStatus" />
            </apex:pageBlockSection>
            <apex:pageblockSection title="{!$Label.gii__salesorderlineproducts}" columns="1" collapsible="false" >
            <!-- *** SSAHU v12.7 Added style="width:auto;" for column heading overlapping for lightningStylesheets="true" -->
                <apex:PageBlockTable border="0" frame="border" value="{!InventoryInquiryOutputList}" width="100%" style="width:auto;" var="output" id="table">
                    <apex:column headerValue="{!$Label.gii__line_column_heading}">
                        <apex:outputLink value="/{!output.transactionLineId}">
                            {!output.transactionLineName}
                        </apex:outputLink>
                    </apex:column>
                    <apex:column headerValue="{!$ObjectType.gii__ProductInventory__c.Fields.gii__Product__c.Label}">
                        <apex:outputLink value="/{!output.productId}">
                            {!output.productName}
                        </apex:outputLink>
                    </apex:column>
                    <apex:column headerValue="{!$ObjectType.gii__SalesOrderLine__c.Fields.gii__SellingUnitofMeasure__c.Label}">
                        <apex:outputText value="{!output.transactionUOMName}" />
                    </apex:column>
                    <apex:column headerValue="{!$ObjectType.gii__SalesOrderLine__c.Fields.gii__OrderQuantity__c.Label}">
                        <apex:outputText value="{!output.quantity}" />
                    </apex:column>
                    <apex:column headerValue="{!$ObjectType.gii__Product2Add__c.Fields.gii__NonStock__c.Label}">
                        <apex:outputField value="{!output.product.gii__NonStock__c}" />
                    </apex:column>
                    <apex:column headerValue="{!$ObjectType.gii__ProductInventory__c.Fields.gii__Warehouse__c.Label}">
                        <apex:outputLink value="/{!output.warehouseId}">
                            {!output.warehouseName}
                        </apex:outputLink>
                    </apex:column>
                    <apex:column headerValue="{!$ObjectType.gii__Product2Add__c.Fields.gii__StockingUnitofMeasure__c.Label}">
                        <apex:outputText value="{!output.stockingUOMName}" />
                    </apex:column>
                    <apex:column headerValue="{!$Label.gii__required}">
                        <apex:outputText value="{!output.uomConvertedQuantity}" />
                    </apex:column>
                    <apex:column headerValue="{!$ObjectType.gii__ProductInventory__c.Fields.gii__OnHandQuantity__c.Label}">
                        <apex:outputText value="{!output.productInventory.gii__OnHandQuantity__c}" />
                    </apex:column>
                    <apex:column headerValue="{!$ObjectType.gii__ProductInventory__c.Fields.gii__ReserveQuantity__c.Label}">
                        <apex:outputText value="{!output.productInventory.gii__ReserveQuantity__c}" />
                    </apex:column>
                    <apex:column headerValue="{!$ObjectType.gii__ProductInventory__c.Fields.gii__BackOrderQuantity__c.Label}">
                        <apex:outputText value="{!output.productInventory.gii__BackOrderQuantity__c}" />
                    </apex:column>
                    <apex:column headerValue="{!$ObjectType.gii__ProductInventory__c.Fields.gii__AvailableQuantity__c.Label}">
                        <apex:outputText value="{!output.productInventory.gii__AvailableQuantity__c}" />
                    </apex:column>
                    <apex:column headerValue="{!$Label.gii__shortage}">
                        <apex:outputText value="{!output.shortageQuantity}" />
                    </apex:column>
                    <apex:column headerValue="{!$ObjectType.gii__ProductInventory__c.Fields.gii__TotalOnOrder__c.Label}">
                        <apex:outputText value="{!output.productInventory.gii__TotalOnOrder__c}" />
                    </apex:column>
                    <apex:column headerValue="{!$Label.gii__status}"
                                 style="text-align:left;vertical-align:middle;" >
                        <apex:image id="red" value="{!$Resource.gii__Red_Dot}" height="20" width="20"
                                    rendered="{!IF(output.status = 'RED', true, false)}"/>
                        <apex:image id="green" value="{!$Resource.gii__Green_Dot}" height="20" width="20"
                                    rendered="{!IF(output.status ='GREEN', true, false)}"/>
                    </apex:column>
                    <apex:column >
                        <apex:commandLink value="{!$Label.gii__kitmemberproducts}" rendered="{!output.kitMemberProductExist}"
                                          action="{!kitOutputWrapperList}" reRender="relatedProducts, messages"
                                          title="{!$Label.gii__clickheretoseeproductinventorydetails}" status="status" >
                            <apex:param name="transactionId" value="{!output.transactionId}" assignTo="{!transactionId}"/>
                            <apex:param name="transactionLineId" value="{!output.transactionLineId}" assignTo="{!transactionLineId}"/>
                            <apex:param name="productId" value="{!output.productId}" assignTo="{!productId}"/>
                            <apex:param name="warehouseId" value="{!output.warehouseId}" assignTo="{!warehouseId}"/>
                            <apex:param name="quantity" value="{!output.quantity}" assignTo="{!quantity}"/>
                            <apex:param name="uomConvertedQuantity" value="{!output.uomConvertedQuantity}" assignTo="{!uomConvertedQuantity}"/>
                            <apex:param name="toGeolocationLat" value="{!output.toGeolocationLat}" assignTo="{!toGeolocationLat}"/>
                            <apex:param name="toGeolocationLon" value="{!output.toGeolocationLon}" assignTo="{!toGeolocationLon}"/>
                            <apex:param name="productName" value="{!output.productName}" assignTo="{!productName}" />
                            <apex:param name="warehouseName" value="{!output.warehouseName}" assignTo="{!warehouseName}" />
                            <apex:param name="transactionUOMId" value="{!output.transactionUOMId}" assignTo="{!transactionUOMId}" />
                            <apex:param name="stockingUOMId" value="{!output.stockingUOMId}" assignTo="{!stockingUOMId}" />
                            <apex:param name="transactionUOMName" value="{!output.transactionUOMName}" assignTo="{!transactionUOMName}" />
                            <apex:param name="stockingUOMName" value="{!output.stockingUOMName}" assignTo="{!stockingUOMName}" />
                            <apex:param name="uomConversionFactor" value="{!output.uomConversionFactor}" assignTo="{!uomConversionFactor}" />
                            <br/>
                        </apex:commandLink>
                        <apex:commandLink value="{!$Label.gii__crosssell}" rendered="{!output.crossSellExist}"
                                          action="{!crossSellOutputWrapperList}" reRender="relatedProducts, messages"
                                          title="{!$Label.gii__clickheretoseeproductinventorydetails}" status="status" >
                            <apex:param name="transactionId" value="{!output.transactionId}" assignTo="{!transactionId}"/>
                            <apex:param name="transactionLineId" value="{!output.transactionLineId}" assignTo="{!transactionLineId}"/>
                            <apex:param name="productId" value="{!output.productId}" assignTo="{!productId}"/>
                            <apex:param name="warehouseId" value="{!output.warehouseId}" assignTo="{!warehouseId}"/>
                            <apex:param name="quantity" value="{!output.quantity}" assignTo="{!quantity}"/>
                            <apex:param name="uomConvertedQuantity" value="{!output.uomConvertedQuantity}" assignTo="{!uomConvertedQuantity}"/>
                            <apex:param name="toGeolocationLat" value="{!output.toGeolocationLat}" assignTo="{!toGeolocationLat}"/>
                            <apex:param name="toGeolocationLon" value="{!output.toGeolocationLon}" assignTo="{!toGeolocationLon}"/>
                            <apex:param name="productName" value="{!output.productName}" assignTo="{!productName}" />
                            <apex:param name="warehouseName" value="{!output.warehouseName}" assignTo="{!warehouseName}" />
                            <apex:param name="transactionUOMId" value="{!output.transactionUOMId}" assignTo="{!transactionUOMId}" />
                            <apex:param name="stockingUOMId" value="{!output.stockingUOMId}" assignTo="{!stockingUOMId}" />
                            <apex:param name="transactionUOMName" value="{!output.transactionUOMName}" assignTo="{!transactionUOMName}" />
                            <apex:param name="stockingUOMName" value="{!output.stockingUOMName}" assignTo="{!stockingUOMName}" />
                            <apex:param name="uomConversionFactor" value="{!output.uomConversionFactor}" assignTo="{!uomConversionFactor}" />
                            <br/>
                        </apex:commandLink>
                        <apex:commandLink value="{!$Label.gii__upsell}" rendered="{!output.upSellExist}"
                                          action="{!upsellOutputWrapperList}" reRender="relatedProducts, messages"
                                          title="{!$Label.gii__clickheretoseeproductinventorydetails}" status="status" >
                            <apex:param name="transactionId" value="{!output.transactionId}" assignTo="{!transactionId}"/>
                            <apex:param name="transactionLineId" value="{!output.transactionLineId}" assignTo="{!transactionLineId}"/>
                            <apex:param name="productId" value="{!output.productId}" assignTo="{!productId}"/>
                            <apex:param name="warehouseId" value="{!output.warehouseId}" assignTo="{!warehouseId}"/>
                            <apex:param name="quantity" value="{!output.quantity}" assignTo="{!quantity}"/>
                            <apex:param name="uomConvertedQuantity" value="{!output.uomConvertedQuantity}" assignTo="{!uomConvertedQuantity}"/>
                            <apex:param name="toGeolocationLat" value="{!output.toGeolocationLat}" assignTo="{!toGeolocationLat}"/>
                            <apex:param name="toGeolocationLon" value="{!output.toGeolocationLon}" assignTo="{!toGeolocationLon}"/>
                            <apex:param name="productName" value="{!output.productName}" assignTo="{!productName}" />
                            <apex:param name="warehouseName" value="{!output.warehouseName}" assignTo="{!warehouseName}" />
                            <apex:param name="transactionUOMId" value="{!output.transactionUOMId}" assignTo="{!transactionUOMId}" />
                            <apex:param name="stockingUOMId" value="{!output.stockingUOMId}" assignTo="{!stockingUOMId}" />
                            <apex:param name="transactionUOMName" value="{!output.transactionUOMName}" assignTo="{!transactionUOMName}" />
                            <apex:param name="stockingUOMName" value="{!output.stockingUOMName}" assignTo="{!stockingUOMName}" />
                            <apex:param name="uomConversionFactor" value="{!output.uomConversionFactor}" assignTo="{!uomConversionFactor}" />
                            <br/>
                        </apex:commandLink>
                        <apex:commandLink value="{!$Label.gii__substitute}" rendered="{!output.substituteExist}"
                                          action="{!substituteOutputWrapperList}" reRender="relatedProducts, messages"
                                          title="{!$Label.gii__clickheretoseeproductinventorydetails}" status="status" >
                            <apex:param name="transactionId" value="{!output.transactionId}" assignTo="{!transactionId}"/>
                            <apex:param name="transactionLineId" value="{!output.transactionLineId}" assignTo="{!transactionLineId}"/>
                            <apex:param name="productId" value="{!output.productId}" assignTo="{!productId}"/>
                            <apex:param name="warehouseId" value="{!output.warehouseId}" assignTo="{!warehouseId}"/>
                            <apex:param name="quantity" value="{!output.quantity}" assignTo="{!quantity}"/>
                            <apex:param name="uomConvertedQuantity" value="{!output.uomConvertedQuantity}" assignTo="{!uomConvertedQuantity}"/>
                            <apex:param name="toGeolocationLat" value="{!output.toGeolocationLat}" assignTo="{!toGeolocationLat}"/>
                            <apex:param name="toGeolocationLon" value="{!output.toGeolocationLon}" assignTo="{!toGeolocationLon}"/>
                            <apex:param name="productName" value="{!output.productName}" assignTo="{!productName}" />
                            <apex:param name="warehouseName" value="{!output.warehouseName}" assignTo="{!warehouseName}" />
                            <apex:param name="transactionUOMId" value="{!output.transactionUOMId}" assignTo="{!transactionUOMId}" />
                            <apex:param name="stockingUOMId" value="{!output.stockingUOMId}" assignTo="{!stockingUOMId}" />
                            <apex:param name="transactionUOMName" value="{!output.transactionUOMName}" assignTo="{!transactionUOMName}" />
                            <apex:param name="stockingUOMName" value="{!output.stockingUOMName}" assignTo="{!stockingUOMName}" />
                            <apex:param name="uomConversionFactor" value="{!output.uomConversionFactor}" assignTo="{!uomConversionFactor}" />
                            <br/>
                        </apex:commandLink>
                        <apex:commandLink value="{!$Label.gii__otherwarehouses}" rendered="{!output.otherWHInventoryExist}"
                                          action="{!otherWarehouseOutputWrapperList}" reRender="relatedProducts, messages"
                                          title="{!$Label.gii__clickheretoseeproductinventorydetails}" status="status">
                            <apex:param name="transactionId" value="{!output.transactionId}" assignTo="{!transactionId}"/>
                            <apex:param name="transactionLineId" value="{!output.transactionLineId}" assignTo="{!transactionLineId}"/>
                            <apex:param name="productId" value="{!output.productId}" assignTo="{!productId}"/>
                            <apex:param name="warehouseId" value="{!output.warehouseId}" assignTo="{!warehouseId}"/>
                            <apex:param name="quantity" value="{!output.quantity}" assignTo="{!quantity}"/>
                            <apex:param name="uomConvertedQuantity" value="{!output.uomConvertedQuantity}" assignTo="{!uomConvertedQuantity}"/>
                            <apex:param name="toGeolocationLat" value="{!output.toGeolocationLat}" assignTo="{!toGeolocationLat}"/>
                            <apex:param name="toGeolocationLon" value="{!output.toGeolocationLon}" assignTo="{!toGeolocationLon}"/>
                            <apex:param name="productName" value="{!output.productName}" assignTo="{!productName}" />
                            <apex:param name="warehouseName" value="{!output.warehouseName}" assignTo="{!warehouseName}" />
                            <apex:param name="transactionUOMId" value="{!output.transactionUOMId}" assignTo="{!transactionUOMId}" />
                            <apex:param name="stockingUOMId" value="{!output.stockingUOMId}" assignTo="{!stockingUOMId}" />
                            <apex:param name="transactionUOMName" value="{!output.transactionUOMName}" assignTo="{!transactionUOMName}" />
                            <apex:param name="stockingUOMName" value="{!output.stockingUOMName}" assignTo="{!stockingUOMName}" />
                            <apex:param name="uomConversionFactor" value="{!output.uomConversionFactor}" assignTo="{!uomConversionFactor}" />
                        </apex:commandLink>
                    </apex:column>
                </apex:PageBlockTable>
            </apex:pageblockSection>
        </apex:pageBlock>
        
        <apex:pageMessages id="messages" ></apex:pageMessages>
        
        <apex:outputPanel id="relatedProducts" >
            <apex:pageBlock rendered="{!showDetail}">
                <!-- *** v12.7 PAKKINENI: Removed the extra apex:pageBlockButtons tag -->
                <!--<apex:pageBlockButtons location="top" > -->
                    <!-- <apex:commandButton value="Update Sales Order Line" title="Update Sales Order Line" /> -->
                <!--</apex:pageBlockButtons> -->
                <apex:pageBlockSection id="relatedProductsDetail" columns="1" title="{!inventoryTitle}" collapsible="false" >
                <!-- *** SSAHU v12.7 Added style="width:auto;" for column heading overlapping for lightningStylesheets="true" -->
                    <apex:PageBlockTable border="0" frame="border" value="{!relatedOutputList}" var="output" width="100%" style="width:auto;"
                                         id="DetailTable">
                        <apex:column headerValue="{!$ObjectType.gii__ProductInventory__c.Fields.gii__Product__c.Label}">
                            <apex:outputLink value="/{!output.productId}">
                                {!output.productName}
                            </apex:outputLink>
                        </apex:column>
                        <apex:column headerValue="{!$ObjectType.gii__Product2Add__c.Fields.gii__NonStock__c.Label}">
                            <apex:outputField value="{!output.product.gii__NonStock__c}" />
                        </apex:column>
                        <apex:column headerValue="{!$ObjectType.gii__ProductInventory__c.Fields.gii__Warehouse__c.Label}">
                            <apex:outputLink value="/{!output.warehouseId}">
                                {!output.warehouseName}
                            </apex:outputLink>
                        </apex:column>
                        <apex:column headerValue="{!$ObjectType.gii__Product2Add__c.Fields.gii__StockingUnitofMeasure__c.Label}">
                            <apex:outputText value="{!output.stockingUOMName}" />
                        </apex:column>
                        <apex:column headerValue="{!$Label.gii__required}">
                            <apex:outputText value="{!output.uomConvertedQuantity}" />
                        </apex:column>
                        <apex:column headerValue="{!$ObjectType.gii__ProductInventory__c.Fields.gii__OnHandQuantity__c.Label}">
                            <apex:outputText value="{!output.productInventory.gii__OnHandQuantity__c}" />
                        </apex:column>
                        <apex:column headerValue="{!$ObjectType.gii__ProductInventory__c.Fields.gii__ReserveQuantity__c.Label}">
                            <apex:outputText value="{!output.productInventory.gii__ReserveQuantity__c}" />
                        </apex:column>
                        <apex:column headerValue="{!$ObjectType.gii__ProductInventory__c.Fields.gii__BackOrderQuantity__c.Label}">
                            <apex:outputText value="{!output.productInventory.gii__BackOrderQuantity__c}" />
                        </apex:column>
                        <apex:column headerValue="{!$ObjectType.gii__ProductInventory__c.Fields.gii__AvailableQuantity__c.Label}">
                            <apex:outputText value="{!output.productInventory.gii__AvailableQuantity__c}" />
                        </apex:column>
                        <apex:column headerValue="{!$Label.gii__shortage}">
                            <apex:outputText value="{!output.shortageQuantity}" />
                        </apex:column>
                        <apex:column headerValue="{!$ObjectType.gii__ProductInventory__c.Fields.gii__TotalOnOrder__c.Label}">
                            <apex:outputText value="{!output.productInventory.gii__TotalOnOrder__c}" />
                        </apex:column>
                        <apex:column headerValue="{!$Label.gii__status}"
                                     style="text-align:left;vertical-align:middle;">
                            <apex:image id="red" value="{!$Resource.gii__Red_Dot}" height="20" width="20"
                                        rendered="{!IF(output.status = 'RED', true, false)}"/>
                            <apex:image id="green" value="{!$Resource.gii__Green_Dot}" height="20" width="20"
                                        rendered="{!IF(output.status ='GREEN', true, false)}"/>
                        </apex:column>
                    </apex:PageBlockTable>
                </apex:pageBlockSection>
            </apex:pageBlock>
        </apex:outputPanel>
    </apex:form>
    <script>
    //function to limit single selection
    function checkOne(cb){
        var inputElem = document.getElementsByTagName("input");
        for(var i=0; i<inputElem.length; i++) {
            if(inputElem[i].id.indexOf("checkedone")!=-1){
                inputElem[i].checked = false;
            }
        }
        cb.checked = true;
    }
    </script>
</apex:page>