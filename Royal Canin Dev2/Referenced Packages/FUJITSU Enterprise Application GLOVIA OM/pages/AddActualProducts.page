<apex:page standardController="gii__ServiceTicketLine__c" extensions="gii.AddActualProductsExtCon" id="AddActualProductsPage" docType="html-5.0"
           setup="false" sidebar="false" tabStyle="gii__ServiceTicket__c" action="{!pageAction}" lightningStylesheets="true">
    <style>
        body{
            font-size: {!IF(salesforce1==true,'1em','')}
        }
    </style>
    <script>
        function pageRedirect(){
            sforce.one.navigateToURL("/{!gii__ServiceTicketLine__c.Id}");
        }
    </script>
    <apex:sectionHeader Title="{!$Label.gii__addactualproduct}" subTitle="{!gii__ServiceTicketLine__c.name}"/>  
    <apex:form id="AddActualProductsFormHeader" >
        <apex:actionFunction name="refreshList" action="{!refreshEstimateProductList}"
                             rerender="productLineSection, AddActualProductsForm, pageBlock01" />
        <apex:pageBlock id="pageBlock01">
            <apex:pageBlockSection columns="2">            
                <apex:outputField value="{!gii__ServiceTicketLine__c.gii__ServiceTicket__c}" rendered="{!salesforce1!=true}"/>                                                     
                <apex:pageBlockSectionItem rendered="{!salesforce1!=true}">                  
                    <apex:outputLabel value="{!$ObjectType.gii__ServiceTicketLine__c.Fields.Name.label}" for="SrvTktln"/>
                    <apex:outputLink id="SrvTktln" value="/{!gii__ServiceTicketLine__c.id}">
                        <apex:outputField value="{!gii__ServiceTicketLine__c.Name}"/> 
                    </apex:outputLink> 
                </apex:pageBlockSectionItem>
                <apex:outputField value="{!gii__ServiceTicketLine__c.gii__ServiceTicket__r.gii__Account__c}" rendered="{!salesforce1!=true}"/>
                <apex:outputField id="gii__AssetReference__c" value="{!gii__ServiceTicketLine__c.gii__AssetReference__c}" rendered="{!salesforce1!=true}" />        
                <apex:outputField value="{!gii__ServiceTicketLine__c.gii__Site__c}" rendered="{!salesforce1!=true}"/>
                <apex:outputField value="{!gii__ServiceTicketLine__c.gii__Product__c}" rendered="{!salesforce1!=true}"/>                    
                <apex:outputField value="{!gii__ServiceTicketLine__c.gii__FieldServiceEngineer__c}" rendered="{!salesforce1!=true}"/>     
                <apex:inputField value="{!ServiceTicketProduct.gii__Warehouse__c}" required="true" onchange="refreshList();"/>                          
            </apex:pageBlockSection>
        </apex:pageBlock>
    </apex:form>
    
    <apex:form id="AddActualProductsForm">
        
        <apex:pageBlock id="pageBlock01">              
            <apex:pagemessages id="pageMsg"></apex:pagemessages>            
            
            <apex:pageBlockButtons location="Top" id="pageButton">
                <apex:actionStatus id="status2" >
                    <apex:facet name="start">
                        <apex:image url="/img/loading.gif"/>
                    </apex:facet>
                </apex:actionStatus>                
                <apex:actionStatus id="status1" >
                    <apex:facet name="start">
                        <apex:outputPanel >
                            <apex:commandButton id="Saving1" value="{!$Label.gii__saving}" rendered="{!gii__ServiceTicketLine__c.gii__DateTimeClosed__c = null}"  disabled="true" action="{!cancel}"/>
                            <apex:commandButton id="Saving2" value="{!$Label.gii__saving}" disabled="true" action="{!cancel}"/>
                            <apex:commandButton id="Saving3" value="{!$Label.gii__saving}" disabled="true" action="{!cancel}"/>  
                            <apex:commandButton id="Saving4" value="{!$Label.gii__saving}" disabled="true" action="{!cancel}"/>                              
                        </apex:outputPanel>
                    </apex:facet>
                    <apex:facet name="stop">
                        <apex:outputPanel id="buttons"> 
                             <!--  v9.7 Add button-->
                            <apex:commandButton action="{!addProductsLine}" value="{!$Label.gii__add}" disabled="{!AddNewLine}"
                                rendered="{!gii__ServiceTicketLine__c.gii__DateTimeClosed__c = null}" status="status2" rerender="buttons, productLineSection, pageMsg"/>    
                            <!-- v10.6 Removed rerender attributes,oncomplete action for lightning experience.  -->                             
                            <apex:commandButton action="{!AddActualLines}" value="{!$Label.gii__save}"
                                                rendered="{!gii__ServiceTicketLine__c.gii__DateTimeClosed__c = null}" status="status1" />
                                                <!-- rerender="pageBlock01" oncomplete="pageRedirect()" -->
                            <apex:commandButton action="{!SaveAndMore}" value="{!$Label.gii__saveandmore}" disabled="{!!AddNewLine}"
                                                rendered="{!gii__ServiceTicketLine__c.gii__DateTimeClosed__c = null}" status="status1"
                                                rerender="pageBlock01" oncomplete="pageRedirect()" />                                               
                            <apex:commandButton immediate="true" action="{!cancel}" value="{!$Label.gii__cancel}"/> 
                        </apex:outputPanel>
                    </apex:facet>
                </apex:actionStatus>
            
            </apex:pageBlockButtons>            
            
            <apex:pageBlockSection collapsible="true" id="productLineSection" columns="1" Title="{!$ObjectType.gii__ServiceTicketProduct__c.labelPlural}" rendered="{!gii__ServiceTicketLine__c.gii__DateTimeClosed__c = null}">          
               <!-- *** SSAHU v12.7 Added style="width:auto;" for column heading overlapping for lightningStylesheets="true" -->
                <apex:pageBlockTable id="productLineTable" border="3" frame="border" value="{!ProductLines}" width="100%" var="pro" style="width:auto;" > 
                    <apex:column style="width:2%">
                        <apex:facet name="header">
                            <apex:inputCheckbox id="selectallProductLines" selected="false"
                                                onclick="javascript:ProductLineSelectChecked(document.forms['AddActualProductsPage:AddActualProductsForm'],'AddActualProductsPage:AddActualProductsForm:pageBlock01:productLineSection:productLineTable:', this);"/>
                        </apex:facet>
                        <apex:inputCheckbox value="{!pro.selected}" id="ProductLineSelected" disabled="{!pro.newLine}" rendered="{!AND(pro.AvailableQuantity > 0, NOT(pro.SerialControlled), NOT(pro.LotControlled ))}" />                    
                        <apex:outputpanel rendered="{!AND(pro.SerialControlled,pro.AvailableQuantity > 0)}">
                            <apex:commandLink value="{!$Label.gii__serialissue}" action="{!assignSerials}" immediate="true">
                                <apex:param name="ServiceTicketEstimatedProductId" value="{!pro.SrvTktActualProduct.gii__ServiceTicketEstimatedProduct__c}"/>
                            </apex:commandLink>
                        </apex:outputpanel>
                        <apex:outputpanel rendered="{!AND(pro.LotControlled,pro.AvailableQuantity > 0)}">
                            <apex:commandLink value="{!$Label.gii__lotissue}" action="{!AssignLot}" immediate="true">
                                <apex:param name="ServiceTicketEstimatedProductId" value="{!pro.SrvTktActualProduct.gii__ServiceTicketEstimatedProduct__c}"/>
                            </apex:commandLink>                         
                        </apex:outputpanel>
                            <!--  v9.7  delete link-->
                            <apex:commandlink value="{!$Label.gii__del}" immediate="true" rendered="{!pro.newLine}" action="{!delProductfromWrapper}" rerender="buttons, productLineSection, pageMsg">
                                <apex:param name="toDelIdent" value="{!pro.ident}" assignTo="{!toDelIdent}"/>
                            </apex:commandlink>                         
                    </apex:column>
                    <apex:column headervalue="{!$ObjectType.gii__ServiceTicketProduct__c.Fields.gii__ServiceTicketEstimatedProduct__c.label}"
                                 value="{!pro.SrvTktActualProduct.gii__ServiceTicketEstimatedProduct__c}" rendered="{!salesforce1!=true}" />
                    <apex:column style="width:10%"  headervalue="{!$ObjectType.gii__ServiceTicketProduct__c.Fields.gii__Product__c.label}" >
                        <!-- <apex:outputField value="{!pro.SrvTktActualProduct.gii__Product__c}"/> -->
                        <!--  v9.7 render input or output field for additional charge dynamically-->
                        <apex:outputField value="{!pro.SrvTktActualProduct.gii__Product__c}" rendered="{!NOT(pro.newLine)}"/>
                        <apex:outputField value="{!pro.SrvTktActualProduct.gii__Product__c}" rendered="{!AND(pro.newLine, pro.SrvTktActualProduct.gii__Product__c != null)}"/>                        
                        <apex:inputField required="true" value="{!pro.SrvTktActualProduct.gii__Product__c}" rendered="{!AND(pro.newLine, pro.SrvTktActualProduct.gii__Product__c == null)}">
                          <apex:actionSupport event="onchange" 
                                    action="{!getProductDefaults}" 
                                    status="status2"   
                                    rerender="productLineSection, pageMsg">
                            </apex:actionSupport>
                        </apex:inputField> 
                    </apex:column>                  
                    
                    
                    <apex:column headervalue="{!$ObjectType.gii__ServiceTicketProduct__c.Fields.gii__NonStock__c.label}"
                                 value="{!pro.SrvTktActualProduct.gii__NonStock__c}" rendered="{!salesforce1!=true}"/>
                    <apex:column style="width:10%"  headervalue="{!$ObjectType.gii__ServiceTicketEstimatedProduct__c.Fields.gii__RemainingQuantity__c.label}" value="{!pro.EstimatedQuantity}"/>
                    <apex:column style="width:10%"  headervalue="{!$ObjectType.gii__ProductInventory__c.Fields.gii__AvailableQuantity__c.label}">
                     
                    <apex:outputText value="{!pro.AvailableQuantity}" rendered="{!NOT(AND(pro.SrvTktActualProduct.gii__NonStock__c, pro.newLine))}" />
                    <apex:outputText value="" rendered="{!AND(pro.SrvTktActualProduct.gii__NonStock__c, pro.newLine)}" />
                    </apex:column>
                    <apex:column style="width:10%"  headervalue="{!$ObjectType.gii__ServiceTicketProduct__c.Fields.gii__Quantity__c.label}" >
                        <apex:inputField value="{!pro.SrvTktActualProduct.gii__Quantity__c}" required="{!pro.newLine}" rendered="{!AND(pro.AvailableQuantity > 0,
                                NOT(pro.SerialControlled),
                                NOT(pro.LotControlled ))}" />
                        <apex:outputField value="{!pro.SrvTktActualProduct.gii__Quantity__c}" rendered="{!OR(pro.SerialControlled, pro.LotControlled)}"/>
                    </apex:column>
                    <apex:column style="width:10%"  headervalue="{!$ObjectType.gii__ServiceTicketProduct__c.Fields.gii__QuantitytobeInvoiced__c.label}"
                                 rendered="{!salesforce1!=true}">
                        <apex:inputField value="{!pro.SrvTktActualProduct.gii__QuantitytobeInvoiced__c}" rendered="{!AND(pro.AvailableQuantity > 0, NOT(pro.SerialControlled), NOT(pro.LotControlled ))}"/>
                        <apex:outputField value="{!pro.SrvTktActualProduct.gii__QuantitytobeInvoiced__c}" rendered="{! OR(pro.SerialControlled, pro.LotControlled)}"/>
                    </apex:column>                  
                    <apex:column style="width:10%"  headervalue="{!$ObjectType.gii__ServiceTicketProduct__c.Fields.gii__UnitPrice__c.label}"
                                 rendered="{!salesforce1!=true}">
                        <apex:inputField value="{!pro.SrvTktActualProduct.gii__UnitPrice__c}" rendered="{!AND(pro.AvailableQuantity > 0, NOT(pro.SerialControlled), NOT(pro.LotControlled ))}"/>
                        <apex:outputField value="{!pro.SrvTktActualProduct.gii__UnitPrice__c}" rendered="{!OR(pro.SerialControlled, pro.LotControlled)}"/>                              
                    </apex:column>
                    <apex:column style="width:10%"  headervalue="{!$ObjectType.gii__ServiceTicketProduct__c.Fields.gii__NoChargeReason__c.label}"
                                 rendered="{!salesforce1!=true}">
                        <apex:inputField value="{!pro.SrvTktActualProduct.gii__NoChargeReason__c}" rendered="{!AND(pro.AvailableQuantity > 0, NOT(pro.SerialControlled), NOT(pro.LotControlled ))}"/>
                        <apex:outputField value="{!pro.SrvTktActualProduct.gii__NoChargeReason__c}" rendered="{!OR(pro.SerialControlled, pro.LotControlled)}"/>                                 
                    </apex:column>
                    <apex:column style="width:10%"  headervalue="{!$ObjectType.gii__ServiceTicketProduct__c.Fields.gii__IssueDate__c.label}"
                                 rendered="{!salesforce1!=true}">
                        <apex:inputField value="{!pro.SrvTktActualProduct.gii__IssueDate__c}" rendered="{!AND(pro.AvailableQuantity > 0, NOT(pro.SerialControlled), NOT(pro.LotControlled ))}"/>
                        <apex:outputField value="{!pro.SrvTktActualProduct.gii__IssueDate__c}" rendered="{!OR(pro.SerialControlled, pro.LotControlled)}"/>                              
                    </apex:column>          
                    
                    
                </apex:pageBlockTable>
            </apex:pageBlockSection>
        </apex:pageBlock>
    </apex:form> 
    
    <script>
    function setFocusOnLoad() {}
    
    //checking or unchecking the 'select all' checkbox in the line sections
    
    function ProductLineSelectChecked(form, element_name, value) {
        var i = 0;
        for (i = 0; i < form.elements.length; i++) {
            if (form.elements[i].name.search('ProductLineSelected') > 0 && form.elements[i].disabled == false) {
                form.elements[i].checked = value.checked;
            }
        }
    }
    
    </script>    
    
</apex:page>