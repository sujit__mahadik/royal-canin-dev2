<apex:page Controller="gii.HelpGetLangCon" showheader="false" sidebar="false" 
    action="{!URLFOR($Resource.gloviaOM_Help, Lang + '/PurchaseOrder.htm')}" >
</apex:page>

<!--
<apex:page Controller="gii.HelpGetLangCon" showheader="false" sidebar="false">
<link rel="stylesheet" type="text/css" href="help.css"/>
<link rel="stylesheet" type="text/css" href="/sCSS/Theme2/default/help.css"/>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=ISO-8859-1"/>
<title>
<apex:outputtext value="{!IF(Lang = 'ja',"購買","Purchase Order")}" />
</title>
</head>

<body>
<table style="font-family: Arial; width: 100%; height: 52px;"> 
<tbody><tr style="color: white;"><td style="background-color: rgb(115, 126, 150);">
<big><b>
<apex:outputtext value="{!IF(Lang = 'ja',"購買ホーム","Purchase Order Home")}" />
</b></big>
</td></tr></tbody> 
</table> 
<br/>

<span style="font-size: 10pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;;">
<table style="font-family: Arial; font-weight: bold; color: white; width: 100%; height: 20px;">
<tbody><tr><td style="background-color: rgb(115, 126, 150);">
<apex:outputtext value="{!IF(Lang = 'ja',"購買とは","What is a Purchase Order")}" />
</td></tr></tbody>
</table>

<p class="MsoNormal">
<apex:outputtext value="{!IF(Lang = 'ja',"購買オーダーは、商品在庫の補充を行なうためのものであり、購入する商品や数量等を指定するための購買オーダーライン、および購入した商品の受入を行なうための購買受入で構成されます。","Purchase Orders can replenish product inventories. Purchase orders works with related Purchase Order Lines to specify products, quantities, etc. and Purchase Receipts to receive the purchased products. ")}" /><br/><br/>
</p>
<br/>
<table style="color: white; font-family: Arial; font-weight: bold; height: 20px; width: 100%;">
<tbody><tr><td style="background-color: rgb(115, 126, 150);">
<apex:outputtext value="{!IF(Lang = 'ja',"購買の作成","Creating Purchase Order")}" />
</td></tr></tbody>
</table>
 
<p class="MsoNormal" style="margin-top: 12pt;"><apex:outputtext value="{!IF(Lang = 'ja',"購買オーダーは次の２つ方法で作成することができます。[1] 購買タブでの新規作成、[2] 販売オーダーからの変換（販売オーダー画面上の「購買オーダーを生成」ボタン）。","Purchase Orders can be created by the following 2 way.  [1] Create in Purchase Orders tab,  [2] converting from Sales order.")}" /><br/></p><fieldset class="note"><legend><strong>Note</strong></legend>
<div class="noteBody">
<img src="/img/help/helpNote_icon.gif" align="left"/>
<p class="MsoNormal">
<apex:outputtext value="{!IF(Lang = 'ja',"購買オーダーの作成時、ヘッダー情報は対象となる取引先（ザプライヤー）に登録されている各種デフォルト値が自動的に反映されます。販売オーダーから変換された場合には、販売オーダーに含まれている商品情報が、購買オーダーラインに自動的に変換されます。","When a new Purchase Order is saved, fields on the header can be defaulted from Account Reference default values. When converted from Sales Orders, Sales Order Products are automatically copied to Purchase Order Lines.")}" /></p>
</div>
</fieldset>
<br/><br/>
<b><apex:outputtext value="{!IF(Lang = 'ja', "関連情報：", "See Also:")}" /></b><br/>
<apex:outputlink value="/apex/Help_PurchaseOrderLine"><apex:outputtext value="{!IF(Lang = 'ja',"購買オーダーライン","Purchase Order Lines")}" /></apex:outputlink><br/>
<apex:outputlink value="/apex/Help_PurchaseOrderReceipt"><apex:outputtext value="{!IF(Lang = 'ja',"購買オーダー受入","Purchase Order Receipts")}" /></apex:outputlink><br/>
<apex:outputlink value="/apex/Help_SalesOrder"><apex:outputtext value="{!IF(Lang = 'ja',"販売オーダー","Sales Orders")}" /></apex:outputlink><br/>
<br/><br/>
<table style="color: white; font-family: Arial; font-weight: bold; height: 20px; width: 100%;">
<tbody><tr><td style="background-color: rgb(115, 126, 150);">
<apex:outputtext value="{!IF(Lang = 'ja',"購買のボタン／リンク","Buttons/Links of Purchase Order")}" />
</td></tr></tbody>
</table>

<div class="p">
<table summary="" class="featureTable" cellpadding="4"
 cellspacing="0" width="100%">
<thead align="left"><tr>
<th class="featureTableHeader" valign="top" width="200">
<apex:outputtext value="{!IF(Lang = 'ja',
"ボタン／リンク",
"Button/Link")}" />
</th>
<th class="featureTableHeader" valign="top">
<apex:outputtext value="{!IF(Lang = 'ja',
"説明",
"Description")}" />
</th></tr>
</thead>

<tbody><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"購買オーダーを表示(PDF)","Purchase Order PDF")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"購買オーダー(PDF)を表示する","Generate Purchase Order  document (PDF)")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"購買オーダーを添付","Attach Purchase Order PDF")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"購買オーダー(PDF)を購買オーダーレコードに添付する","Attach Purchase Order document (PDF) to this Purchase Order")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"購買オーダーを送付(PDF)","E-Mail Purchase Order PDF")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"購買オーダー(PDF)をe-mailにて送付する","Send Purchase Order document (PDF) by e-mail")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"価格表選択画面を表示（ここをクリック）","Click here to choose Price Book")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"価格表選択画面を表示する","Open Price Book selection screen")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"価格表から商品を選択","Add Lines from Price Book")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"購買オーダー（ヘッダー）の価格表に含まれる商品を対象とした、商品選択画面を表示する。","This button will provide a list of products from purchase order price book for which standard price is defined. Order lines can be added for service and non-service products from price book name defined on the purchase order header record. Price book must be valid and active. Products can be searched by keyword and selected to create new order lines.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"過去の購買オーダーから商品を選択","Add Lines from History")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"表示中の購買オーダーの取引先（サプライヤー）から過去に購入した商品を対象とした、商品選択画面を表示する。","This button will provide a list of products and services purchased from the purchase order supplier account. New order lines can be added by searching products by keyword and no of days from history of this supplier.")}" />
</td>
</tr></tbody>
</table>
</div><br/><br/>
<table style="color: white; font-family: Arial; font-weight: bold; height: 20px; width: 100%;">
<tbody><tr><td style="background-color: rgb(115, 126, 150);">
<apex:outputtext value="{!IF(Lang = 'ja',"購買の項目","Fields of Purchase Order")}" />
</td></tr></tbody>
</table>

<div class="p">
<table summary="" class="featureTable" cellpadding="4"
 cellspacing="0" width="100%">
<thead align="left"><tr>
<th class="featureTableHeader" valign="top" width="200">
<apex:outputtext value="{!IF(Lang = 'ja',
"項目",
"Field")}" />
</th>
<th class="featureTableHeader" valign="top">
<apex:outputtext value="{!IF(Lang = 'ja',
"説明",
"Description")}" />
</th></tr>
</thead>

<tbody><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"購買オーダー","Purchase Order")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"購買オーダーの番号。","A purchase order number. A unique number for the purchase order.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"承認済","Approved")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"購買オーダーを承認状況するかどうかのフラグ。フラグがオンの場合に、承認済である。購買オーダーが未承認の場合、後続の購買受入処理を実施できない。システムポリシーの設定「購買オーダーを自動で承認」の設定に従ってフラグがセットされる。","Flag to indicate if purchase order is approved. If purchase order is not approved then it can not be received. Defaulted from System Policy.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"社内購買責任者","Buyer")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"サプライヤーからの購買における社内購買責任者。","A person responsible for purchasing activities. Defaulted from Account Reference.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"運送会社","Carrier")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"購買オーダー出荷時の運送会社。購買オーダーの新規作成時に、取引先関連情報（サプライヤー）のデフォルト値がセットされる。","Shipping carrier for products. Defaulted from Account Reference. ")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"納入条件","Delivery Terms")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"サプライヤーからの納入条件。購買オーダーの新規作成時に、取引先関連情報のデフォルト値がセットされる。","A code specifying the agreed-to terms of delivery. Defaulted from account reference record of supplier account.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"ドキュメントテキスト","Document Text")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"購買オーダー確認書に表示するテキスト情報。購買オーダーの新規作成時に、取引先関連情報のデフォルト値またはシステムポリシーのデフォルト値がセットされる。","Defaulted from system policy if it is not defined for account reference, otherwise it is defaulted from system policy.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"住所(請求先)","Invoice To Address")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"請求先住所。購買オーダーの新規作成時に、システムポリシーの請求先住所がデフォルト値としてセットされる。","The invoicing address details like Street, City, State, Zip, and Country. When creating a new Purchase Order, invoicing address can be defaulted from System Policy.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"支払条件","Payment Terms")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"サプライヤーへの支払条件。購買オーダーの新規作成時に、取引先関連情報のデフォルト値がセットされる。","A code specifying the payment terms for supplier invoice. 
This is defaulted from account reference record of supplier account.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"価格表","Price Book Name")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"購買オーダーに使用する価格表。購買オーダーの新規作成時に、取引先関連情報のデフォルト値がセットされる。","Supplier’s Price Book Name. Defaulted from Account Reference. This can be selected or overwritten after adding the purchase order header record. This price book will be used to get the unit price for any new purchase order line products.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"ドキュメントテキスト印刷","Print Document Text")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"購買オーダー確認書へのテキスト情報表示を有効化する。購買オーダーの新規作成時に、取引先関連情報のデフォルト値がセットされる。","Activate  Document Text printing for Purchase Order. When creating a new Purchase Order, this field can be defaulted from Account Reference.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"要求日","Required Date")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"購買オーダーの要求日。","The date required.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"販売オーダー","Sales Order")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"販売オーダーの番号。購買オーダーが販売オーダーから変換された場合に、変換元の販売オーダーの番号がセットされる。","A number of Sales Order. When the Purchase Order was created from a Sales Order, this field has the Sales Order number. ")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"出荷先住所","Ship To Address")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"出荷住所。購買オーダーの新規作成時に、出荷先在庫場所の住所がセットされる。","The shipping address details like City, State, Zip, Country etc are maintained here. When creating a new Purchase Order, shipping address can be defaulted from ship to warehouse address.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"オーダーステータス","Order Status")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"購買オーダーのステータス。（オープン：いずれかの購買オーダーラインに受入未の商品が存在する。クローズ：すべての購買オーダーラインが受入済またはキャンセル済）","Purchase Order Status. \"Open\" - When any of the purchase order line has open quantity. \"Closed\" - When all of the purchase order lines are closed or cancelled.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"サプライヤー","Supplier")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"取引先（サプライヤー）。","Supplier’s Account ")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"サプライヤー住所","Supplier Address")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"サプライヤーの住所。購買オーダーの新規作成時に、取引先（サプライヤー）の請求先住所がセットされる。","The Supplier address details like City, State, Zip, Country etc are maintained here. When creating a new Purchase Order, Supplier address can be defaulted from Billing Address of Supplier Account.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"サプライヤー責任者","Supplier Contact")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"サプライヤー側責任者。取引先（サプライヤー）の取引先責任者から選択する。取引先（サプライヤー）のデフォルト値がセットされる。","A contact at the supplier purchase location. Defaulted from Account Reference.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"サプライヤー名","Supplier Name")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"サプライヤーの名称。","The name of supplier.")}" />
</td>
</tr></tbody>
</table>
</div>
</span></body>

<div class="helpPageFooter">Copyright  2008-{!YEAR( TODAY() )} Glovia International, inc. All rights reserved.<br/>
Various trademarks held by their respective owners.</div>
</html>
</apex:page>
-->
<!-- Generation Date & Time: 5/4/2010 6:56:26 PM PST -->