<apex:page Controller="gii.HelpGetLangCon" showheader="false" sidebar="false" 
    action="{!URLFOR($Resource.gloviaOM_Help, Lang + '/WorkOrderIssue.htm')}" >
</apex:page>

<!--
<apex:page Controller="gii.HelpGetLangCon" showheader="false" sidebar="false">
<link rel="stylesheet" type="text/css" href="help.css"/>
<link rel="stylesheet" type="text/css" href="/sCSS/Theme2/default/help.css"/>
<apex:outputpanel rendered="{!IF(Lang != 'ja',true,false)}">
<head>
<meta http-equiv="content-type" content="text/html; charset=ISO-8859-1"/>
<title>
Work Order Issue
</title>
</head>
<body>
<table style="font-family: Arial; width: 100%; height: 52px;">
<tbody>
<tr style="color: white;">
<td style="background-color: rgb(115, 126, 150);"><big><b>Work Order Issue Home</b></big></td></tr></tbody></table><br/>
<span style="font-size: 10pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;;">
<table style="font-family: Arial; font-weight: bold; color: white; width: 100%; height: 20px;">
<tbody>
<tr><td style="background-color: rgb(115, 126, 150);">What is a Work Order Issue</td></tr></tbody></table></span>
<p class="MsoNormal">Work order material can be issued by clicking on “Material Issue” button in work order detail page or by clicking the “New Work Order Issue” button from work order issue related list.</p>
<p><b>Material Issue </b></p>
<p class="MsoNormal">This page issues unplanned material directly to a work order from the work order bill of material components. This page allows material issue for the components available quantity only in work order warehouse.</p>
<p><b>New work order issue</b></p>
<p class="MsoNormal">This page issues unplanned material directly to a work order with or without specifying the work order bill of material component. This page allows material issue for the components available quantity in work order warehouse and other warehouses.</p>
<br/>
<p class="MsoNormal">Material can be issued for serial or lot controlled product by specifying product serial or product lots. For serial controlled and lot controlled products a work order Issue record is created for every product serial and product lot assigned to the work order.</p>
<fieldset class="note">
<legend><strong>Note</strong></legend>
<div class="noteBody">
<img src="/img/help/helpNote_icon.gif" align="left"/>
<p class="MsoNormal">Work order issue record cannot be deleted but you may reverse a material issue by entering a new material issue with negative quantity.</p></div></fieldset>
<fieldset class="note">
<legend><strong>Note</strong></legend>
<div class="noteBody">
<img src="/img/help/helpNote_icon.gif" align="left"/>
<p class="MsoNormal">When enabled staging journal entry lines and inventory transaction history records are created for material issues.</p></div></fieldset>
<br/>
<span style="font-size: 10pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;;">
<table style="font-family: Arial; font-weight: bold; color: white; width: 100%; height: 20px;">
<tbody>
<tr><td style="background-color: rgb(115, 126, 150);">Fields of Work Order Issue</td></tr></tbody></table></span>
<div class="p">
<table summary="" class="featureTable" cellpadding="4" cellspacing="0" width="100%">
<thead align="left">
<tr>
<th class="featureTableHeader" valign="top" width="200">Field Label</th>
<th class="featureTableHeader" valign="top">Data Type</th>
<th class="featureTableHeader" valign="top">Description</th></tr></thead><tbody>
<tr><td>Work Order Issue Sequence</td><td>Auto Number</td><td>The work order issue sequence or identifier (Display Format {000000})</td></tr>
<tr><td>Component</td><td>Lookup(Product Reference)</td><td>A material product. Does not have to be a component on the product's bill of material.</td></tr>
<tr><td>Cost Method</td><td>Text(80)</td><td>Product cost method.</td></tr>
<tr><td>Issue Cost</td><td>Formula (Number)</td><td>The cost per unit times the quantity issued.</td></tr>
<tr><td>Issue Date</td><td>Date</td><td>The material issue date.</td></tr>
<tr><td>Issue Quantity</td><td>Number(10, 2)</td><td>The quantity issued.</td></tr>
<tr><td>Lot Controlled</td><td>Checkbox</td><td>Indicates if a product is lot controlled. If true then product lot must be specified.</td></tr>
<tr><td>Non Stock</td><td>Checkbox</td><td>Indicates if a product is non stock.</td></tr>
<tr><td>Product Inventory</td><td>Lookup(Product Inventory)</td><td>Product inventory link for work order issue product and warehouse.</td></tr>
<tr><td>Product Lot</td><td>Lookup(Product Lot)</td><td>Product lot where the material was issued.</td></tr>
<tr><td>Product Serial</td><td>Lookup(Product Serial)</td><td>Product serial for the material issued.</td></tr>
<tr><td>Reference</td><td>Text Area(255)</td><td>Reference or notes for the material issue.</td></tr>
<tr><td>Sequence</td><td>Number(4, 0)</td><td>Component sequence from the work order bill of material.</td></tr>
<tr><td>Serial Controlled</td><td>Checkbox</td><td>Indicates if a product is serial controlled. If true then product serial must be specified for the component issued.</td></tr>
<tr><td>Unit Cost</td><td>Number(13, 4)</td><td>The standard cost per unit.</td></tr>
<tr><td>Unit Of Measure</td><td>Picklist</td><td>The unit of measure of the component.</td></tr>
<tr><td>Warehouse</td><td>Lookup(Warehouse)</td><td>The inventory location where component was issued. Defaulted from work order.</td></tr>
<tr><td>Work Order</td><td>Master-Detail(Work Order)</td><td>The work order number or identifier.</td></tr>
<tr><td>Work Order Bill of Material Line</td><td>Lookup(Work Order Bill of Material)</td><td>The work order bill of material line number or identifier.</td></tr>
</tbody></table></div><br/><br/>
<span style="font-size: 10pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;;">
<table style="font-family: Arial; font-weight: bold; color: white; width: 100%; height: 20px;">
<tbody>
<tr><td style="background-color: rgb(115, 126, 150);">Error Messages of Work Order Issue</td></tr></tbody></table></span>
<div class="p">
<table summary="" class="featureTable" cellpadding="4" cellspacing="0" width="100%">
<thead align="left">
<tr>
<th class="featureTableHeader" valign="top" width="200">Error Message</th>
<th class="featureTableHeader" valign="top">Source</th>
<th class="featureTableHeader" valign="top">Condition</th></tr></thead><tbody>
<tr><td>Cannot change work order issue record.</td><td>Validation Rule</td><td>Work order issue record is changed</td></tr>
<tr><td>Cannot Issue Material for Non Stock Product</td><td>Validation Rule </td><td>When material issue component is a non-stock product.</td></tr>
<tr><td>Component is required.</td><td>Validation Rule</td><td>Component is null</td></tr>
<tr><td>Invalid entry - Work Order is already closed.</td><td>Validation Rule</td><td>When new issue record is added and work is already closed</td></tr>
<tr><td>Issue quantity cannot be more than available quantity </td><td>Apex Trigger</td><td>Issue quantity > Product inventory available quantity for component in work order warehouse.</td></tr>
<tr><td>Issue Quantity cannot be more than components remaining quantity.</td><td>Apex Trigger</td><td>Work Order Bill of Material is not null and Issue Quantity > work order bill of material component’s remaining quantity.</td></tr>
<tr><td>Issue quantity cannot be zero.</td><td>Validation Rule </td><td>When issue quantity is zero</td></tr>
<tr><td>Issue Quantity must be 1 for the Serial Controlled product</td><td>Apex Trigger</td><td>Product is serial controlled and issue quantity is > 1</td></tr>
<tr><td>Only non-service product is allowed.</td><td>Validation Rule</td><td>Component’s service flag is checked</td></tr>
<tr><td>Product Inventory record does not exists for this component and warehouse</td><td>Apex Trigger</td><td>Product Inventory record does not exists for this component and warehouse.</td></tr>
<tr><td>Product Lot does not belong to this component</td><td>Apex Trigger</td><td>Product is lot controlled and component is not equal to product lot’s product.</td></tr>
<tr><td>Product Lot is required for lot controlled product.</td><td>Validation Rule</td><td>Component is lot controlled and product lot  is null</td></tr>
<tr><td>Product Serial does not belong to this component</td><td>Apex Trigger</td><td>Product is serial controlled and component is not equal to product serial’s product.</td></tr>
<tr><td>Product Serial is required for serial controlled product.</td><td>Validation Rule</td><td>Component is serial controlled and product serial is null</td></tr>
<tr><td>Work order is already closed.</td><td>Validation Rule </td><td>When new issue record is added and work order is already closed</td></tr>
<tr><td>Work Order is not released.</td><td>Validation Rule</td><td>When new issue record is added and work is not released</td></tr>
<tr><td>Work Order Issue Component does not match with Work Order Bill of Material Component.</td><td>Apex Trigger</td><td>Work Order Bill of Material is not null and Work Order Issue Component does not equal to Work Order Bill of Material Component.</td></tr>
<tr><td>Work Order Issue record cannot be deleted.</td><td>Apex Trigger</td><td>Work Order issue record is deleted</td></tr>
</tbody></table></div><br/><br/>
</body>
</apex:outputpanel>
<apex:outputpanel rendered="{!IF(Lang = 'ja',true,false)}">
<head>
<meta http-equiv="content-type" content="text/html; charset=ISO-8859-1"/>
<title>
Work Order Issue
</title>
</head>
<body>
<table style="font-family: Arial; width: 100%; height: 52px;">
<tbody>
<tr style="color: white;">
<td style="background-color: rgb(115, 126, 150);"><big><b>Work Order Issue Home</b></big></td></tr></tbody></table><br/>
<span style="font-size: 10pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;;">
<table style="font-family: Arial; font-weight: bold; color: white; width: 100%; height: 20px;">
<tbody>
<tr><td style="background-color: rgb(115, 126, 150);">What is a Work Order Issue</td></tr></tbody></table></span>
<p class="MsoNormal">Work order material can be issued by clicking on “Material Issue” button in work order detail page or by clicking the “New Work Order Issue” button from work order issue related list.</p>
<p><b>Material Issue </b></p>
<p class="MsoNormal">This page issues unplanned material directly to a work order from the work order bill of material components. This page allows material issue for the components available quantity only in work order warehouse.</p>
<p><b>New work order issue</b></p>
<p class="MsoNormal">This page issues unplanned material directly to a work order with or without specifying the work order bill of material component. This page allows material issue for the components available quantity in work order warehouse and other warehouses.</p>
<br/>
<p class="MsoNormal">Material can be issued for serial or lot controlled product by specifying product serial or product lots. For serial controlled and lot controlled products a work order Issue record is created for every product serial and product lot assigned to the work order.</p>
<fieldset class="note">
<legend><strong>Note</strong></legend>
<div class="noteBody">
<img src="/img/help/helpNote_icon.gif" align="left"/>
<p class="MsoNormal">Work order issue record cannot be deleted but you may reverse a material issue by entering a new material issue with negative quantity.</p></div></fieldset>
<fieldset class="note">
<legend><strong>Note</strong></legend>
<div class="noteBody">
<img src="/img/help/helpNote_icon.gif" align="left"/>
<p class="MsoNormal">When enabled staging journal entry lines and inventory transaction history records are created for material issues.</p></div></fieldset>
<br/>
<span style="font-size: 10pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;;">
<table style="font-family: Arial; font-weight: bold; color: white; width: 100%; height: 20px;">
<tbody>
<tr><td style="background-color: rgb(115, 126, 150);">Fields of Work Order Issue</td></tr></tbody></table></span>
<div class="p">
<table summary="" class="featureTable" cellpadding="4" cellspacing="0" width="100%">
<thead align="left">
<tr>
<th class="featureTableHeader" valign="top" width="200">Field Label</th>
<th class="featureTableHeader" valign="top">Data Type</th>
<th class="featureTableHeader" valign="top">Description</th></tr></thead><tbody>
<tr><td>Work Order Issue Sequence</td><td>Auto Number</td><td>The work order issue sequence or identifier (Display Format {000000})</td></tr>
<tr><td>Component</td><td>Lookup(Product Reference)</td><td>A material product. Does not have to be a component on the product's bill of material.</td></tr>
<tr><td>Cost Method</td><td>Text(80)</td><td>Product cost method.</td></tr>
<tr><td>Issue Cost</td><td>Formula (Number)</td><td>The cost per unit times the quantity issued.</td></tr>
<tr><td>Issue Date</td><td>Date</td><td>The material issue date.</td></tr>
<tr><td>Issue Quantity</td><td>Number(10, 2)</td><td>The quantity issued.</td></tr>
<tr><td>Lot Controlled</td><td>Checkbox</td><td>Indicates if a product is lot controlled. If true then product lot must be specified.</td></tr>
<tr><td>Non Stock</td><td>Checkbox</td><td>Indicates if a product is non stock.</td></tr>
<tr><td>Product Inventory</td><td>Lookup(Product Inventory)</td><td>Product inventory link for work order issue product and warehouse.</td></tr>
<tr><td>Product Lot</td><td>Lookup(Product Lot)</td><td>Product lot where the material was issued.</td></tr>
<tr><td>Product Serial</td><td>Lookup(Product Serial)</td><td>Product serial for the material issued.</td></tr>
<tr><td>Reference</td><td>Text Area(255)</td><td>Reference or notes for the material issue.</td></tr>
<tr><td>Sequence</td><td>Number(4, 0)</td><td>Component sequence from the work order bill of material.</td></tr>
<tr><td>Serial Controlled</td><td>Checkbox</td><td>Indicates if a product is serial controlled. If true then product serial must be specified for the component issued.</td></tr>
<tr><td>Unit Cost</td><td>Number(13, 4)</td><td>The standard cost per unit.</td></tr>
<tr><td>Unit Of Measure</td><td>Picklist</td><td>The unit of measure of the component.</td></tr>
<tr><td>Warehouse</td><td>Lookup(Warehouse)</td><td>The inventory location where component was issued. Defaulted from work order.</td></tr>
<tr><td>Work Order</td><td>Master-Detail(Work Order)</td><td>The work order number or identifier.</td></tr>
<tr><td>Work Order Bill of Material Line</td><td>Lookup(Work Order Bill of Material)</td><td>The work order bill of material line number or identifier.</td></tr>
</tbody></table></div><br/><br/>
<span style="font-size: 10pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;;">
<table style="font-family: Arial; font-weight: bold; color: white; width: 100%; height: 20px;">
<tbody>
<tr><td style="background-color: rgb(115, 126, 150);">Error Messages of Work Order Issue</td></tr></tbody></table></span>
<div class="p">
<table summary="" class="featureTable" cellpadding="4" cellspacing="0" width="100%">
<thead align="left">
<tr>
<th class="featureTableHeader" valign="top" width="200">Error Message</th>
<th class="featureTableHeader" valign="top">Source</th>
<th class="featureTableHeader" valign="top">Condition</th></tr></thead><tbody>
<tr><td>Cannot change work order issue record.</td><td>Validation Rule</td><td>Work order issue record is changed</td></tr>
<tr><td>Cannot Issue Material for Non Stock Product</td><td>Validation Rule </td><td>When material issue component is a non-stock product.</td></tr>
<tr><td>Component is required.</td><td>Validation Rule</td><td>Component is null</td></tr>
<tr><td>Invalid entry - Work Order is already closed.</td><td>Validation Rule</td><td>When new issue record is added and work is already closed</td></tr>
<tr><td>Issue quantity cannot be more than available quantity </td><td>Apex Trigger</td><td>Issue quantity > Product inventory available quantity for component in work order warehouse.</td></tr>
<tr><td>Issue Quantity cannot be more than components remaining quantity.</td><td>Apex Trigger</td><td>Work Order Bill of Material is not null and Issue Quantity > work order bill of material component’s remaining quantity.</td></tr>
<tr><td>Issue quantity cannot be zero.</td><td>Validation Rule </td><td>When issue quantity is zero</td></tr>
<tr><td>Issue Quantity must be 1 for the Serial Controlled product</td><td>Apex Trigger</td><td>Product is serial controlled and issue quantity is > 1</td></tr>
<tr><td>Only non-service product is allowed.</td><td>Validation Rule</td><td>Component’s service flag is checked</td></tr>
<tr><td>Product Inventory record does not exists for this component and warehouse</td><td>Apex Trigger</td><td>Product Inventory record does not exists for this component and warehouse.</td></tr>
<tr><td>Product Lot does not belong to this component</td><td>Apex Trigger</td><td>Product is lot controlled and component is not equal to product lot’s product.</td></tr>
<tr><td>Product Lot is required for lot controlled product.</td><td>Validation Rule</td><td>Component is lot controlled and product lot  is null</td></tr>
<tr><td>Product Serial does not belong to this component</td><td>Apex Trigger</td><td>Product is serial controlled and component is not equal to product serial’s product.</td></tr>
<tr><td>Product Serial is required for serial controlled product.</td><td>Validation Rule</td><td>Component is serial controlled and product serial is null</td></tr>
<tr><td>Work order is already closed.</td><td>Validation Rule </td><td>When new issue record is added and work order is already closed</td></tr>
<tr><td>Work Order is not released.</td><td>Validation Rule</td><td>When new issue record is added and work is not released</td></tr>
<tr><td>Work Order Issue Component does not match with Work Order Bill of Material Component.</td><td>Apex Trigger</td><td>Work Order Bill of Material is not null and Work Order Issue Component does not equal to Work Order Bill of Material Component.</td></tr>
<tr><td>Work Order Issue record cannot be deleted.</td><td>Apex Trigger</td><td>Work Order issue record is deleted</td></tr>
</tbody></table></div><br/><br/>
</body>
</apex:outputpanel>
<div class="helpPageFooter">
Copyright  2008-{!YEAR( TODAY() )} Glovia International, inc. All rights reserved.<br/>
Various trademarks held by their respective owners.
</div>
</apex:page>
-->
<!-- Generation Date & Time: 2/7/2011 9:24:45 PM PST -->