<apex:page sidebar="true" showheader="true" standardController="gii__ServiceTicketLine__c" extensions="gii.ServiceTicketLineAssignFSEExtCon" lightningStylesheets="true">
    <!-- *** PAKKINENI v12.5  Java script function to enforce the refresh in lightning and navigate to the previous page -->
    <apex:includeScript value="{!$Resource.gii__gloviaOM_JS}"/>
    <apex:form >
        <!-- *** PAKKINENI v12.5  dummy outputpanel to rerender -->
        <apex:outPutPanel layout="block" id="dummyPanelId"> 
        </apex:outPutPanel>
        <!-- v12.5 *** --> 
        <apex:sectionHeader title="{!$Label.gii__assignfieldserviceengineer}"
                            subtitle="{!$Label.gii__serviceticket} {!serviceTicketLine.gii__ServiceTicket__r.Name} - {!$Label.gii__line} {!serviceTicketLine.Name}"/>
        <apex:pageBlock title="{!$Label.gii__searchcriteria}">
            <apex:pageBlockSection >
                <apex:selectList label="{!$Label.gii__searchdistance}" value="{!distanceLimit}" size="1">
                    <apex:selectOption itemLabel="5 mi" itemValue="5" rendered="{!!calculateDistanceinKilometers}" />
                    <apex:selectOption itemLabel="10 mi" itemValue="10" rendered="{!!calculateDistanceinKilometers}" />
                    <apex:selectOption itemLabel="25 mi" itemValue="25" rendered="{!!calculateDistanceinKilometers}" />
                    <apex:selectOption itemLabel="50 mi" itemValue="50" rendered="{!!calculateDistanceinKilometers}" />
                    <apex:selectOption itemLabel="100 mi" itemValue="100" rendered="{!!calculateDistanceinKilometers}" />
                    <apex:selectOption itemLabel=">100 mi" itemValue="24901" rendered="{!!calculateDistanceinKilometers}" />
                    <apex:selectOption itemLabel="5 km" itemValue="5" rendered="{!calculateDistanceinKilometers}" />
                    <apex:selectOption itemLabel="10 km" itemValue="10" rendered="{!calculateDistanceinKilometers}" />
                    <apex:selectOption itemLabel="25 km" itemValue="25" rendered="{!calculateDistanceinKilometers}" />
                    <apex:selectOption itemLabel="50 km" itemValue="50" rendered="{!calculateDistanceinKilometers}" />
                    <apex:selectOption itemLabel="100 km" itemValue="100" rendered="{!calculateDistanceinKilometers}" />
                    <apex:selectOption itemLabel=">100 km" itemValue="40075" rendered="{!calculateDistanceinKilometers}" />
                    <apex:actionSupport event="onchange"
                                        action="{!updateFSEWrapperList}"
                                        rerender="messages, fseList" />
                </apex:selectList>
                <apex:outputField value="{!serviceTicketLine.gii__PromiseDate__c}"/>
                <apex:inputCheckbox label="{!$Label.gii__activefsesonly}" value="{!activeFSE}" >
                    <apex:actionSupport event="onchange"
                                        action="{!updateFSEWrapperList}"
                                        rerender="messages, fseList" />
                </apex:inputCheckbox>
                <apex:inputCheckbox label="{!$Label.gii__availablefsesonly}" value="{!availableFSE}" >
                    <apex:actionSupport event="onchange"
                                        action="{!updateFSEWrapperList}"
                                        rerender="messages, fseList" />
                </apex:inputCheckbox>
                
                <apex:inputField label="{!$Label.gii__requiredcertification}" value="{!serviceTicketLine.gii__Certification__c}">
                    <apex:actionSupport event="onchange"
                                        action="{!updateFSEWrapperList}"
                                        rerender="messages, fseList" />
                </apex:inputField>
                <apex:inputCheckbox label="{!$Label.gii__recentgeolocation}" value="{!recentGeolocation}" >
                    <apex:actionSupport event="onchange"
                                        action="{!updateFSEWrapperList}"
                                        rerender="messages, fseList" />
                </apex:inputCheckbox>
            </apex:pageBlockSection>
        </apex:pageBlock>
        <apex:pageBlock id="fseList" title="{!$Label.gii__fieldserviceengineers}">
            <apex:pageBlockButtons location="top">
               <!-- *** PAKKINENI v12.5 added oncomplete and rerender to enforce the refresh in lightning
                <apex:commandButton value="{!$Label.gii__save}" title="{!$Label.gii__save}" action="{!save}" /> -->
                <apex:commandButton value="{!$Label.gii__save}" title="{!$Label.gii__save}" action="{!save}" 
                 rerender="dummyPanelId,messages" oncomplete="navBackAndRefreshWhenNoErrors(1,'{!hasError}')" />
                <apex:commandButton value="{!$Label.gii__cancel}" title="{!$Label.gii__cancel}" action="{!cancel}" />
            </apex:pageBlockButtons>
            <apex:pageMessages id="messages"></apex:pageMessages>
            <apex:pageBlockTable value="{!FSEWrapperList}" var="fseWrapper">
                <apex:column headerValue="{!$Label.gii__select}">
                    <apex:inputCheckbox id="checkedone" value="{!fseWrapper.selected}" onclick="checkOne(this)" />
                </apex:column>
                <apex:column headerValue="{!$ObjectType.gii__FieldServiceEngineer__c.Fields.Name.Label}">
                    <apex:outputlink value="/{!fseWrapper.fse.Id}">{!fseWrapper.fse.Name}</apex:outputlink>
                </apex:column>
                <apex:column headerValue="{!$Label.gii__distance} (mi)" value="{!fseWrapper.distanceToSite}" rendered="{!!calculateDistanceinKilometers}"/>
                <apex:column headerValue="{!$Label.gii__distance} (km)" value="{!fseWrapper.distanceToSite}" rendered="{!calculateDistanceinKilometers}"/>
                <apex:column value="{!fseWrapper.fse.gii__Certification__c}"/>
                <apex:column value="{!fseWrapper.fse.gii__BusinessHours__c}"/>
                <apex:column value="{!fseWrapper.fse.gii__Active__c}"/>
                <apex:column headerValue="{!$Label.gii__availability}">
                    <apex:image height="25" width="25" url="{!URLFOR($Resource.gii__gloviaOM_Resources,'IMAGES/Green_Dot.jpg')}"
                                rendered="{!if(fseWrapper.availability=='green',true,false)}" />
                    <apex:image height="25" width="25" url="{!URLFOR($Resource.gii__gloviaOM_Resources,'IMAGES/Yellow_Dot.jpg')}"
                                rendered="{!if(fseWrapper.availability=='yellow',true,false)}" />
                    <apex:image height="25" width="25" url="{!URLFOR($Resource.gii__gloviaOM_Resources,'IMAGES/Red_Dot.jpg')}"
                                rendered="{!if(fseWrapper.availability=='red',true,false)}" />
                </apex:column>
            </apex:pageBlockTable>
        </apex:pageBlock>
    </apex:form>
    <script>
    //function to limit single selection
    function checkOne(cb){
        var inputElem = document.getElementsByTagName("input");
        for(var i=0; i<inputElem.length; i++) {
            if(inputElem[i].id.indexOf("checkedone")!=-1){
                inputElem[i].checked = false;
            }
        }
        cb.checked = true;
    }
    </script>
</apex:page>