<apex:page Controller="gii.HelpGetLangCon" showheader="false" sidebar="false" 
    action="{!URLFOR($Resource.gloviaOM_Help, Lang + '/SystemPolicy.htm')}" >
</apex:page>

<!--
<apex:page Controller="gii.HelpGetLangCon" showheader="false" sidebar="false">
<link rel="stylesheet" type="text/css" href="help.css"/>
<link rel="stylesheet" type="text/css" href="/sCSS/Theme2/default/help.css"/>
<apex:outputpanel rendered="{!IF(Lang != 'ja',true,false)}">
<head>
<meta http-equiv="content-type" content="text/html; charset=ISO-8859-1"/>
<title>
System Policy
</title>
</head>
<body>
<table style="font-family: Arial; width: 100%; height: 52px;">
<tbody>
<tr style="color: white;">
<td style="background-color: rgb(115, 126, 150);"><big><b>System Policy Home</b></big></td></tr></tbody></table><br/>
<span style="font-size: 10pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;;">
<table style="font-family: Arial; font-weight: bold; color: white; width: 100%; height: 20px;">
<tbody>
<tr><td style="background-color: rgb(115, 126, 150);">What is a System Policy</td></tr></tbody></table></span>
<p class="MsoNormal">System Policy consists of system parameters and system wide default values for glovia OM application. The system can have only one System Policy record. The default values defined in the System Policy will be used when new Account References and Product References are created. Also, application behavior such as automatic order releasing for Sales Orders and automatic forward to invoicing on order shipments, user interface control such as Tab enablement, and PDF form definitions such as invoices and order acknowledgements can be defined in the System Policy.</p>
<br/>
<span style="font-size: 10pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;;">
<table style="font-family: Arial; font-weight: bold; color: white; width: 100%; height: 20px;">
<tbody>
<tr><td style="background-color: rgb(115, 126, 150);">Creating System Policy</td></tr></tbody></table></span>
<p class="MsoNormal">System Policy is created by "Create System Policy" button in a very first step of configuration after the install of glovia OM. </p>
<fieldset class="note">
<legend><strong>Note</strong></legend>
<div class="noteBody">
<img src="/img/help/helpNote_icon.gif" align="left"/>
<p class="MsoNormal">"Create System Policy" button can work only once. </p></div></fieldset>
<br/>
<span style="font-size: 10pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;;">
<table style="font-family: Arial; font-weight: bold; color: white; width: 100%; height: 20px;">
<tbody>
<tr><td style="background-color: rgb(115, 126, 150);">Editing System Policy</td></tr></tbody></table></span>
<p class="MsoNormal">System Policy can be edited as needed anytime to change default values and system parameters.</p>
<fieldset class="note">
<legend><strong>Note</strong></legend>
<div class="noteBody">
<img src="/img/help/helpNote_icon.gif" align="left"/>
<p class="MsoNormal">Since System Policy defines and changes application behavior, it is recommended to carefully analyze the impact before changing the values and parameters.</p></div></fieldset>
<br/>
<span style="font-size: 10pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;;">
<table style="font-family: Arial; font-weight: bold; color: white; width: 100%; height: 20px;">
<tbody>
<tr><td style="background-color: rgb(115, 126, 150);">Buttons of System Policy</td></tr></tbody></table></span>
<div class="p">
<table summary="" class="featureTable" cellpadding="4" cellspacing="0" width="100%">
<thead align="left">
<tr>
<th class="featureTableHeader" valign="top" width="200">Button</th>
<th class="featureTableHeader" valign="top">Description</th></tr></thead><tbody>
<tr><td>Create Account Reference</td><td>Creates Account Reference records for existing Accounts</td></tr>
<tr><td>Create Product Reference</td><td>Creates Product Reference records for existing Products</td></tr>
<tr><td>Create System Policy</td><td>Generates the System Policy Record</td></tr>
</tbody></table></div><br/><br/>
<br/>
<span style="font-size: 10pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;;">
<table style="font-family: Arial; font-weight: bold; color: white; width: 100%; height: 20px;">
<tbody>
<tr><td style="background-color: rgb(115, 126, 150);">Fields of System Policy</td></tr></tbody></table></span>
<div class="p">
<table summary="" class="featureTable" cellpadding="4" cellspacing="0" width="100%">
<thead align="left">
<tr>
<th class="featureTableHeader" valign="top" width="200">Field Label</th>
<th class="featureTableHeader" valign="top">Data Type</th>
<th class="featureTableHeader" valign="top">Description</th></tr></thead><tbody>
<tr><td>Account Default Tax Location</td><td>Lookup(Tax Location)</td><td>A code representing the default tax location for new accounts. The tax location code determines the set of tax rate jurisdictions applied to the sale of a product or service. </td></tr>
<tr><td>Account Excise Taxable</td><td>Checkbox</td><td>If checked, the order is excise taxable. When new accounts are created this value is defaulted on the account reference record.</td></tr>
<tr><td>Account Sales Taxable</td><td>Checkbox</td><td>If checked, the order is sales taxable. When new accounts are created this value is defaulted on the account reference record.</td></tr>
<tr><td>Account Usage Taxable</td><td>Checkbox</td><td>If checked, the order is usage taxable. When new accounts are created this value is defaulted on the account reference record.</td></tr>
<tr><td>Accounting Group Code</td><td>Picklist</td><td>The default accounting group code used to link order fulfillment transactions to the Accounting System. </td></tr>
<tr><td>Add Price Book to Products Page</td><td>Text(80)</td><td>System Field</td></tr>
<tr><td>Add Product To Price Books Page</td><td>Text(80)</td><td>System Field</td></tr>
<tr><td>Add Product's Standard Price Page</td><td>Text(80)</td><td>System Field</td></tr>
<tr><td>Add Purchase Order Lines Page</td><td>Text(80)</td><td>System Field</td></tr>
<tr><td>Add Sales Order Lines Page</td><td>Text(80)</td><td>System Field</td></tr>
<tr><td>Add Sales Quote Lines Page</td><td>Text(80)</td><td>System Field</td></tr>
<tr><td>Allow PO Zero Unit Price</td><td>Checkbox</td><td>If checked, allows creation of a zero price purchase order line without requiring a no charge reason code. </td></tr>
<tr><td>Allow Zero Unit Price</td><td>Checkbox</td><td>If checked, allows creation of a zero price sales order or sales quote line without requiring a no charge reason code. </td></tr>
<tr><td>Attach Invoice Page</td><td>Text(80)</td><td>Deprecated field</td></tr>
<tr><td>Attach Packlist Page</td><td>Text(80)</td><td>Deprecated field</td></tr>
<tr><td>Attach Picklist Page</td><td>Text(80)</td><td>Deprecated field</td></tr>
<tr><td>Attach Purchase Order Page</td><td>Text(80)</td><td>Deprecated field</td></tr>
<tr><td>Attach RMA Page</td><td>Text(80)</td><td>Deprecated field</td></tr>
<tr><td>Attach SO Ack Page</td><td>Text(80)</td><td>Deprecated field</td></tr>
<tr><td>Attach SQ Ack Page</td><td>Text(80)</td><td>Deprecated field</td></tr>
<tr><td>Auto Approve Purchase Orders</td><td>Checkbox</td><td>If checked, the purchase order will be approved automatically. If blank, a manual release or a work flow approval process is required. </td></tr>
<tr><td>Auto Release Order</td><td>Checkbox</td><td>If checked, automatically releases sales order. Leave it blank to release the sales order manually or through work flow approval process. 
Note: This check governs the pick, pack,and ship.</td></tr>
<tr><td>BCC</td><td>Email</td><td>System Field</td></tr>
<tr><td>Body</td><td>Long Text Area(32000)</td><td>System Field</td></tr>
<tr><td>BOL Page</td><td>Text(80)</td><td>Custom Visualforce page name for Bill of Lading PDF document</td></tr>
<tr><td>CC</td><td>Email</td><td>System Field</td></tr>
<tr><td>Create PO From SO Page</td><td>Text(80)</td><td>System Field</td></tr>
<tr><td>Default Ship-To Warehouse</td><td>Lookup(Warehouse)</td><td>Default warehouse you will ship the inventory to for Purchase Order Receipts.</td></tr>
<tr><td>Description</td><td>Text(80)</td><td>Description of the System Policy. </td></tr>
<tr><td>Document Text</td><td>Long Text Area(32000)</td><td>Default Document Text for Invoice, RMA, Sales Order, Sales Quotes and Purchase Orders.</td></tr>
<tr><td>E-Mail Page</td><td>Text(80)</td><td>System Field</td></tr>
<tr><td>Enable VAT</td><td>Checkbox</td><td>If checked, VAT (Value Added Tax) is used and defaulted for new Products. </td></tr>
<tr><td>Excise Taxable</td><td>Checkbox</td><td>If checked, excise tax is defaulted for new Products.</td></tr>
<tr><td>Forward To Invoice Additional Charges</td><td>Checkbox</td><td>If checked, the Sales Order Additional Charge record is automatically forwarded for invoice creation.
If the Forward to Invoice flag is unchecked at Sales Order Additional Charge record then the corresponding Forward To Queue record is deleted. 
Note : Invoice can be created only if Forward To Invoice Queue has a record. On creation of Invoice the Forward To Invoice queue record is deleted. </td></tr>
<tr><td>Forward To Invoice On Shipment</td><td>Checkbox</td><td>If checked, the shipment is automatically forwarded for invoice creation. 
Forward To Invoice flag at Shipment header is set and a Forward To Invoice record is created in Forward To Invoice Queue for the Shipment. 
If the flag from the Shipment is manually unchecked then the Forward to Invoice queue record for the shipment is deleted. </td></tr>
<tr><td>Forward To Invoice Service Order Line</td><td>Checkbox</td><td>If checked, the Service Order Line record is automatically forwarded for invoice creation.
Once the Service Order line is set for Forward to Invoice ? No Service Order Delivery or Invoice Schedule can be created for the Service Order Line. 
If the Forward To Invoice flag at Service Order Line is unchecked then the record for the corresponding Service Order Line is deleted from Forward To Invoice Queue. </td></tr>
<tr><td>Google Map API Key</td><td>Text Area(255)</td><td>Key for the Google Maps API.The Google Maps API lets you embed Google Maps in your own web pages. Access http://code.google.com/apis/maps/signup.html to sign up for an API key </td></tr>
<tr><td>Invoice Page</td><td>Text(80)</td><td>Custom Visualforce page name for Invoice PDF document</td></tr>
<tr><td>Invoice To City</td><td>Text(80)</td><td>Default of the Invoice To Address for new purchase orders. </td></tr>
<tr><td>Invoice To Country</td><td>Text(80)</td><td>Default of the Invoice To Address for new purchase orders. </td></tr>
<tr><td>Invoice To Name</td><td>Text(80)</td><td>Default of the Invoice To Address for new purchase orders. </td></tr>
<tr><td>Invoice To State/Province</td><td>Text(80)</td><td>Default of the Invoice To Address for new purchase orders. </td></tr>
<tr><td>Invoice To Street</td><td>Text Area(255)</td><td>Default of the Invoice To Address for new purchase orders. </td></tr>
<tr><td>Invoice To Zip/Postal Code</td><td>Text(80)</td><td>Default of the Invoice To Address for new purchase orders. </td></tr>
<tr><td>NonService Invoice Schedule Page</td><td>Text(80)</td><td>System field</td></tr>
<tr><td>Pack List Page</td><td>Text(80)</td><td>Custom Visualforce page name for Packlist PDF document</td></tr>
<tr><td>Picking</td><td>Number(4, 2)</td><td>Default picking lead time (in days) and used for Order Promising.</td></tr>
<tr><td>PickList Page</td><td>Text(80)</td><td>Custom Visualforce page name for Picklist PDF document</td></tr>
<tr><td>Prevent Multiple Opportunity Conversion</td><td>Checkbox</td><td>When checked, Prevents multiple Opportunity Conversions to sales orders and sales quotes.</td></tr>
<tr><td>Price Book Name</td><td>Text(40)</td><td>Default price book for Account, Sales Quotes and Sales Orders. If Price Book Name is not defined in the Account reference record then this will be defaulted for new Sales Order or Sales Quotes otherwise it will use Accounｔ price book name.</td></tr>
<tr><td>Primary Sort for Add Line Pages</td><td>Picklist</td><td>This field gives ability to select primary sorting field (Product Name, Service, Product Code or Product Family) for "Add Lines from Price Book" and "Add Lines from History" pages in Purchase Order, Sales Quote and Sales Order. It is also used in Add Products page for glovia price books. Default sorting field is Product Name.</td></tr>
<tr><td>Print Document Text</td><td>Checkbox</td><td>If checked, prints the text entered in Document Text field  to Sales quote acknowledgement, Sales order acknowledgement, Invoices, RMA and Purchase Order documents. </td></tr>
<tr><td>Purchase Order Page</td><td>Text(80)</td><td>Custom Visualforce page name for Purchase Order PDF document</td></tr>
<tr><td>RMA Page</td><td>Text(80)</td><td>Custom Visualforce page name for RMA PDF document</td></tr>
<tr><td>Sales Administration</td><td>Number(4, 2)</td><td>Default lead time for the Sales Administration and used for Order Promising. </td></tr>
<tr><td>Sales Taxable</td><td>Checkbox</td><td>If checked, sales tax flag is defaulted for new Products.</td></tr>
<tr><td>Schedule Template</td><td>Lookup(Invoice Schedule Template)</td><td>Default Schedule Template for Sales Order and Service Order Lines. If Schedule Template is not defined in the Account reference record then this will be defaulted for new order lines otherwise it will use Account’s Schedule Template</td></tr>
<tr><td>Service Invoice Schedule Page</td><td>Text(80)</td><td>System Field</td></tr>
<tr><td>Service Substitute UpSell CrossSell Page</td><td>Text(80)</td><td>System Field</td></tr>
<tr><td>Shipping Label Page</td><td>Text(80)</td><td>System Field</td></tr>
<tr><td>Shipping Preparation</td><td>Number(4, 2)</td><td>Default lead time for Shipping Preparation and used for Order Promising.</td></tr>
<tr><td>Show BackOrder Tab</td><td>Checkbox</td><td>When checked, shows Backorder Tab in Order Fulfillment page</td></tr>
<tr><td>Show Consumption Tab</td><td>Checkbox</td><td>When checked, shows Consumption Tab in Order Fulfillment page</td></tr>
<tr><td>Show Forward To Invoice Tab</td><td>Checkbox</td><td>When checked, shows Forward To Invoice under Shipment Tab in Order Fulfillment page</td></tr>
<tr><td>Show Packlist Creation Tab</td><td>Checkbox</td><td>When checked, shows Packlist Creation under Packlist Tab in Order Fulfillment page</td></tr>
<tr><td>Show Packlist Tab</td><td>Checkbox</td><td>When checked, shows Packlist Tab in Order Fulfillment page</td></tr>
<tr><td>Show Picklist Tab</td><td>Checkbox</td><td>When checked, shows Picklist Tab in Order Fulfillment page</td></tr>
<tr><td>Show Quick Packlist Tab</td><td>Checkbox</td><td>When checked, shows Quick Packlist under Packlist Tab in Order Fulfillment page</td></tr>
<tr><td>Show Quick Ship Tab</td><td>Checkbox</td><td>When checked, shows Quick Ship under Shipment Tab in Order Fulfillment page</td></tr>
<tr><td>Show Receipts Tab</td><td>Checkbox</td><td>When checked, shows Receipts Tab in Order Fulfillment page</td></tr>
<tr><td>Show Serial and Lot on Packlist PDF</td><td>Checkbox</td><td>When checked, shows Product Serials and Lot on Packlist PDF document.</td></tr>
<tr><td>Show Ship Confirm Tab</td><td>Checkbox</td><td>When checked, shows Ship Confirm under Shipment Tab in Order Fulfillment page</td></tr>
<tr><td>Show Shipment Tab</td><td>Checkbox</td><td>When checked, shows Shipment Tab in Order Fulfillment page</td></tr>
<tr><td>SO Acknowledgement Page</td><td>Text(80)</td><td>Custom Visualforce page name for Sales Order Acknowledgement PDF document</td></tr>
<tr><td>SQ Acknowledgement Page</td><td>Text(80)</td><td>Custom Visualforce page name for Sales Quote PDF document</td></tr>
<tr><td>Subject</td><td>Text Area(255)</td><td>System Field</td></tr>
<tr><td>Substitute UpSell CrossSell Page</td><td>Text(80)</td><td>System Field</td></tr>
<tr><td>To</td><td>Email</td><td>System Field</td></tr>
<tr><td>Unit Of Measure</td><td>Picklist</td><td>Default unit of measure code for the product.</td></tr>
<tr><td>Usage Taxable</td><td>Checkbox</td><td>If checked, usage tax flag is defaulted for new Products.</td></tr>
<tr><td>Using CRM Application</td><td>Checkbox</td><td>If using Salesforce CRM application capabilities, check this flag. By checking this flag, Glovia application will use Salesforce standard Product and Pricebook. If unchecked, Glovia application will use Product and Pricebook which are provided by Glovia. </td></tr>
<tr><td>Using Multiple Currencies</td><td>Checkbox</td><td>If your salesforce.com org is single currency, then by default it is unchecked. But if salesforce.com org is multi currency, then by default it is checked. Note: If you intend to use multi currency, you will need to install the extension package for multi currency. Please contact us (help@glovia.com) for the installation link of extension package.  IF EXTENSION PACKAGE FOR MULTI-CURRENCY IS NOT INSTALLED, THEN THIS FLAG SHOULD NOT BE CHECKED. </td></tr>
<tr><td>Validate Duplicate Product Serial</td><td>Checkbox</td><td>If checked, validates the duplication for the product serial. Note: This validation is for manually created serials in Products.</td></tr>
<tr><td>VAT</td><td>Lookup(VAT)</td><td>Default value added tax code.</td></tr>
<tr><td>Warehouse</td><td>Lookup(Warehouse)</td><td>Default warehouse you will ship the inventory from.</td></tr>
<tr><td>Work Order Page</td><td>Text(80)</td><td>Custom Visualforce page name for Work Order PDF document</td></tr>
</tbody></table></div><br/><br/>
</body>
</apex:outputpanel>
<apex:outputpanel rendered="{!IF(Lang = 'ja',true,false)}">
<head>
<meta http-equiv="content-type" content="text/html; charset=ISO-8859-1"/>
<title>
システムポリシー
</title>
</head>
<body>
<table style="font-family: Arial; width: 100%; height: 52px;">
<tbody>
<tr style="color: white;">
<td style="background-color: rgb(115, 126, 150);"><big><b>システムポリシーホーム</b></big></td></tr></tbody></table><br/>
<span style="font-size: 10pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;;">
<table style="font-family: Arial; font-weight: bold; color: white; width: 100%; height: 20px;">
<tbody>
<tr><td style="background-color: rgb(115, 126, 150);">システムポリシーとは</td></tr></tbody></table></span>
<p class="MsoNormal">システムポリシーとは、gloviaオーダーマネジメント・アプリケーションの動作やシステム全体でのデフォルト値を管理するためのもので、システム全体で１レコードのみ定義できます。システムポリシーで定義されたデフォルト値は、取引先関連情報や商品関連情報の新規作成時に使用されます。また、販売オーダー作成時の自動オーダーリリースや販売オーダー出荷時の自動請求などの動作や、タブの表示／非表示設定、請求書等の帳票定義体設定など、アプリケーションの動作を定義できます。</p>
<br/>
<span style="font-size: 10pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;;">
<table style="font-family: Arial; font-weight: bold; color: white; width: 100%; height: 20px;">
<tbody>
<tr><td style="background-color: rgb(115, 126, 150);">システムポリシーの作成</td></tr></tbody></table></span>
<p class="MsoNormal">システムポリシーは、gloviaオーダーマネジメントのインストール後の初期設定時に、「システムポリシーを作成」ボタン押下によって作成されます。</p>
<fieldset class="note">
<legend><strong>Note</strong></legend>
<div class="noteBody">
<img src="/img/help/helpNote_icon.gif" align="left"/>
<p class="MsoNormal">「システムポリシーを作成」ボタンはインストール直後の初期状態の時にのみ動作し、システムポリシーの生成後は動作しません。</p></div></fieldset>
<br/>
<span style="font-size: 10pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;;">
<table style="font-family: Arial; font-weight: bold; color: white; width: 100%; height: 20px;">
<tbody>
<tr><td style="background-color: rgb(115, 126, 150);">システムポリシーの編集</td></tr></tbody></table></span>
<p class="MsoNormal">システムポリシーは、必要に応じて変更が可能であり、システム運用中、随時更新することが可能です。</p>
<fieldset class="note">
<legend><strong>Note</strong></legend>
<div class="noteBody">
<img src="/img/help/helpNote_icon.gif" align="left"/>
<p class="MsoNormal">システムポリシーは、アプリケーション全体の動作を変更するため、設定の変更にあたっては、その影響を事前に検討の上で変更する必要があります。</p></div></fieldset>
<br/>
<span style="font-size: 10pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;;">
<table style="font-family: Arial; font-weight: bold; color: white; width: 100%; height: 20px;">
<tbody>
<tr><td style="background-color: rgb(115, 126, 150);">システムポリシーのボタン</td></tr></tbody></table></span>
<div class="p">
<table summary="" class="featureTable" cellpadding="4" cellspacing="0" width="100%">
<thead align="left">
<tr>
<th class="featureTableHeader" valign="top" width="200">ボタン</th>
<th class="featureTableHeader" valign="top">説明</th></tr></thead><tbody>
<tr><td>取引先関連情報を生成</td><td>既存の取引先について、取引先関連情報を生成する</td></tr>
<tr><td>商品関連情報を生成</td><td>既存の商品について、商品関連情報を生成する</td></tr>
<tr><td>システムポリシーを生成</td><td>システムポリシーレコードを生成する</td></tr>
</tbody></table></div><br/><br/>
<br/>
<span style="font-size: 10pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;;">
<table style="font-family: Arial; font-weight: bold; color: white; width: 100%; height: 20px;">
<tbody>
<tr><td style="background-color: rgb(115, 126, 150);">システムポリシーの項目</td></tr></tbody></table></span>
<div class="p">
<table summary="" class="featureTable" cellpadding="4" cellspacing="0" width="100%">
<thead align="left">
<tr>
<th class="featureTableHeader" valign="top" width="200">項目</th>
<th class="featureTableHeader" valign="top">データタイプ</th>
<th class="featureTableHeader" valign="top">説明</th></tr></thead><tbody>
<tr><td>BCC</td><td>電子メール </td><td>システム項目</td></tr>
<tr><td>Body</td><td>ロングテキストエリア(32000)</td><td>システム項目</td></tr>
<tr><td>CC</td><td>電子メール </td><td>システム項目</td></tr>
<tr><td>E-Mailカスタムページ</td><td>テキスト(80)</td><td>システム項目</td></tr>
<tr><td>Google Map API Key</td><td>テキスト エリア(255)</td><td>Google MapのAPIキー。APIキーをセットすることで、販売オーダーの出荷先住所をGoogle Mapにて地図表示する。APIキーは、http://code.google.com/apis/maps/signup.html にてサインナップすることで取得可能。</td></tr>
<tr><td>Quick梱包サブタブを表示</td><td>チェックボックス</td><td>出庫／梱包／出荷タブ内に、Quick梱包サブタブを表示する場合にオンにする。</td></tr>
<tr><td>Quick出荷サブタブを表示</td><td>チェックボックス</td><td>出庫／梱包／出荷タブ内に、Quick出荷サブタブを表示する場合にオンにする。</td></tr>
<tr><td>Salesforce CRM機能を利用</td><td>チェックボックス</td><td>Salesforce CRM機能を使用している場合にオンにする。フラグをオンにすることで、GloviaアプリはSalesforce標準の商品と価格表を使用して動作するようになります。フラグをオフにすると、Gloviaが提供する商品と価格表にもとづいて動作するようになります。</td></tr>
<tr><td>Subject</td><td>テキストエリア(255)</td><td>システム項目</td></tr>
<tr><td>To</td><td>電子メール </td><td>システム項目</td></tr>
<tr><td>VATを有効化</td><td>チェックボックス</td><td>VAT（Value Added Tax）を扱う場合にオンにする。新規商品登録時にデフォルト値として使用される。</td></tr>
<tr><td>クロスセル／アップセル／代替カスタムページ（サービスオーダーライン）</td><td>テキスト(80)</td><td>システム項目</td></tr>
<tr><td>クロスセル／アップセル／代替カスタムページ（販売オーダーライン）</td><td>テキスト(80)</td><td>システム項目</td></tr>
<tr><td>サービスオーダーライン作成時に自動で請求準備を実施</td><td>チェックボックス</td><td>サービスオーダーライン作成時に、自動的に請求準備処理（請求情報の生成）を実施する場合にオンにする。自動生成された請求情報を削除する場合には、対象となるサービスオーダーライン内のフラグを解除することで削除される。サービスオーダーラインに対して請求準備処理が実施されると、以後そのサービスオーダーラインに対するサービスオーダー納入処理は実施不可となる。</td></tr>
<tr><td>作業オーダーPDFカスタムページ</td><td>テキスト(80)</td><td>作業オーダーPDFにカスタムページを使用する場合にページ名を指定する。</td></tr>
<tr><td>デフォルトVAT</td><td>参照関係（VAT）</td><td>VATコードのデフォルト値。商品関連情報の作成時に使用される。</td></tr>
<tr><td>デフォルト出荷先在庫場所</td><td>参照関係（在庫場所）</td><td>購買オーダー作成時に、その入庫先となる在庫場所のデフォルト値。システムポリシー生成時の初期値として、GII-Warehouse が設定され、取引先関連情報、商品関連情報の新規作成時にデフォルトとして使用される。</td></tr>
<tr><td>デフォルト税ロケーション</td><td>参照関係（税ロケーション）</td><td>税ロケーションのデフォルト値。取引先関連情報の新規作成時にデフォルトとして使用される。</td></tr>
<tr><td>ドキュメントテキスト</td><td>ロングテキストエリア(32000)</td><td>販売見積書、販売オーダー確認書、請求書、返品確認書、購買オーダーにデフォルトで表示されるテキスト情報。</td></tr>
<tr><td>ドキュメントテキスト印刷</td><td>チェックボックス</td><td>販売見積書、販売オーダー確認書、請求書、返品確認書、購買オーダーにドキュメントテキストを表示する場合にオンにする。</td></tr>
<tr><td>宛名(請求先)</td><td>テキスト(80)</td><td>購買オーダー作成時の請求先住所のデフォルト値。</td></tr>
<tr><td>価格表</td><td>テキスト(40)</td><td>取引先関連情報、販売見積、販売オーダーの価格表のデフォルト値。取引先関連情報の作成時に使用される。販売見積、販売オーダーの新規作成時で、取引先関連情報に価格表が指定されていない場合は、システムポリシーの価格表デフォルト値がセットされる。</td></tr>
<tr><td>価格表への商品登録　カスタムページ</td><td>テキスト(80)</td><td>システム項目</td></tr>
<tr><td>会計グループコード</td><td>選択リスト</td><td>会計コードのデフォルト値。取引先関連情報、商品関連情報の新規作成時にデフォルトとして使用される。</td></tr>
<tr><td>購買オーダーカスタムページ</td><td>テキスト(80)</td><td>購買オーダーPDFにカスタムページを使用する場合にページ名を指定する。</td></tr>
<tr><td>購買オーダーカスタムページ（添付用）</td><td>テキスト(80)</td><td>未使用項目</td></tr>
<tr><td>購買オーダーライン追加　カスタムページ</td><td>テキスト(80)</td><td>システム項目</td></tr>
<tr><td>購買オーダーを自動で承認</td><td>チェックボックス</td><td>購買オーダー作成時に、ステータスを自動的に承認済とする場合にオンにする。ワークフローや手動で承認する運用の場合はオフにする。</td></tr>
<tr><td>国(請求先)</td><td>テキスト(80)</td><td>購買オーダー作成時の請求先住所のデフォルト値。</td></tr>
<tr><td>梱包指示サブタブを表示</td><td>チェックボックス</td><td>出庫／梱包／出荷タブ内に、梱包指示サブタブを表示する場合にオンにする。</td></tr>
<tr><td>梱包指示書カスタムページ</td><td>テキスト(80)</td><td>梱包指示書PDFにカスタムページを使用する場合にページ名を指定する。</td></tr>
<tr><td>梱包指示書カスタムページ（添付用）</td><td>テキスト(80)</td><td>未使用項目</td></tr>
<tr><td>梱包指示書にシリアル番号・ロット番号を表示</td><td>チェックボックス</td><td>梱包指示書（PDF）に商品シリアル番号、商品ロット番号を出力する場合にオンにする。</td></tr>
<tr><td>梱包指示書作成サブタブを表示</td><td>チェックボックス</td><td>出庫／梱包／出荷タブ内に、梱包指示書作成サブタブを表示する場合にオンにする。</td></tr>
<tr><td>在庫場所</td><td>参照関係（在庫場所）</td><td>販売オーダー作成時に、出荷元となる在庫場所のデフォルト値。システムポリシー生成時の初期値として、GII-Warehouse が設定され、取引先関連情報、商品関連情報の新規作成時にデフォルトとして使用される。</td></tr>
<tr><td>市区郡(請求先)</td><td>テキスト(80)</td><td>購買オーダー作成時の請求先住所のデフォルト値。</td></tr>
<tr><td>受注残サブタブを表示</td><td>チェックボックス</td><td>出庫／梱包／出荷タブ内に、受注残サブタブを表示する場合にオンにする。</td></tr>
<tr><td>受入サブタブを表示</td><td>チェックボックス</td><td>出庫／梱包／出荷タブ内に、受入サブタブを表示する場合にオンにする。</td></tr>
<tr><td>出荷サブタブを表示</td><td>チェックボックス</td><td>出庫／梱包／出荷タブ内に、出荷サブタブを表示する場合にオンにする。</td></tr>
<tr><td>出荷ラベルカスタムページ</td><td>テキスト(80)</td><td>システム項目</td></tr>
<tr><td>出荷リードタイム</td><td>数値(4, 2)</td><td>出荷リードタイムのデフォルト値（単位：日）。商品関連情報の作成時に使用される。</td></tr>
<tr><td>出荷確認サブタブを表示</td><td>チェックボックス</td><td>出庫／梱包／出荷タブ内に、出荷確認サブタブを表示する場合にオンにする。</td></tr>
<tr><td>出荷時に自動で請求準備を実施</td><td>チェックボックス</td><td>出荷時に、自動的に請求準備処理（請求情報の生成）を実施する場合にオンにする。自動生成された請求情報を削除する場合には、対象となる出荷レコード内のフラグを解除することで削除される。</td></tr>
<tr><td>出庫リードタイム</td><td>数値(4, 2)</td><td>出庫リードタイムのデフォルト値（単位：日）。販売オーダーの約束日の算出に使用される。</td></tr>
<tr><td>出庫指示サブタブを表示</td><td>チェックボックス</td><td>出庫／梱包／出荷タブ内に、出庫指示サブタブを表示する場合にオンにする。</td></tr>
<tr><td>出庫指示書カスタムページ</td><td>テキスト(80)</td><td>出庫指示書PDFにカスタムページを使用する場合にページ名を指定する。</td></tr>
<tr><td>出庫指示書カスタムページ（添付用）</td><td>テキスト(80)</td><td>未使用項目</td></tr>
<tr><td>商談から販売見積・販売オーダーへの変換を一回に制限</td><td>チェックボックス</td><td>商談から販売見積、販売オーダーへの変換を一回に制限する場合にオンにする。</td></tr>
<tr><td>商品シリアル番号の重複チェックを有効化</td><td>チェックボックス</td><td>商品シリアル番号の重複をチェックする場合にオンにする。重複チェックは、商品情報としてシリアル番号を入力する際に実行されます。</td></tr>
<tr><td>商品への価格表登録　カスタムページ</td><td>テキスト(80)</td><td>システム項目</td></tr>
<tr><td>商品選択画面の表示順指定</td><td>選択リスト</td><td>販売見積、販売オーダー、購買オーダーの各画面で表示される商品選択画面（価格表からの商品選択、過去データからの商品選択）、およびglovia価格表への商品追加時に表示される商品選択画面において、商品一覧の表示順の基準となる項目を指定する（商品名、サービス、商品コード、商品ファイミリのいずれか）。各商品一覧画面では、指定された項目の昇順で表示される。デフォルト値は「商品名」。</td></tr>
<tr><td>商品標準価格登録　カスタムページ</td><td>テキスト(80)</td><td>システム項目</td></tr>
<tr><td>請求スケジュールカスタムページ（サービスオーダーライン）</td><td>テキスト(80)</td><td>システム項目</td></tr>
<tr><td>請求スケジュールカスタムページ（販売オーダーライン）</td><td>テキスト(80)</td><td>システム項目</td></tr>
<tr><td>請求スケジュールテンプレート</td><td>参照関係（請求スケジュールテンプレート）</td><td>サービス見積ライン、サービスオーダーラインの請求スケジュールテンプレートのデフォルト値。サービス見積ライン、サービスオーダーラインの新規作成時で、取引先関連情報にデフォルト請求スケジュールテンプレートが指定されていない場合は、システムポリシーのデフォルト値がセットされる。</td></tr>
<tr><td>請求準備サブタブを表示</td><td>チェックボックス</td><td>出庫／梱包／出荷タブ内に、請求準備サブタブを表示する場合にオンにする。</td></tr>
<tr><td>請求書カスタムページ</td><td>テキスト(80)</td><td>請求書PDFにカスタムページを使用する場合にページ名を指定する。</td></tr>
<tr><td>請求書カスタムページ（添付用）</td><td>テキスト(80)</td><td>未使用項目</td></tr>
<tr><td>積載票カスタムページ</td><td>テキスト(80)</td><td>積載票PDFにカスタムページを使用する場合にページ名を指定する。</td></tr>
<tr><td>説明</td><td>テキスト(80)</td><td>システムポリシーの説明。システムポリシー生成時の初期値として、Glovia Policies が設定される。</td></tr>
<tr><td>多通貨機能を有効化</td><td>チェックボックス</td><td>デフォルトでは、Salesforce組織が単一通貨設定の場合にオフとなり、多通貨設定の場合にオンとなる。多通貨機能を使用する場合、gloviaオーダーマネジメント多通貨オプションが必要となります。追加オプションのインストールについては、help@glovia.comにお問い合わせください。多通貨オプションを使用しない場合は、このフラグはオフとしてください。</td></tr>
<tr><td>単位</td><td>選択リスト</td><td>商品の管理単位のデフォルト値</td></tr>
<tr><td>町名・番地(請求先)</td><td>テキストエリア(255)</td><td>購買オーダー作成時の請求先住所のデフォルト値。</td></tr>
<tr><td>追加料金レコード作成時に自動で請求準備を実施</td><td>チェックボックス</td><td>販売オーダーの追加料金レコードの作成時に、自動的に追加料金の請求準備処理（請求情報の生成）を実施する場合にオンにする。自動生成された追加料金の請求情報を削除する場合には、対象となる追加料金レコード内のフラグを解除することで削除される。</td></tr>
<tr><td>都道府県(請求先)</td><td>テキスト(80)</td><td>購買オーダー作成時の請求先住所のデフォルト値。</td></tr>
<tr><td>売上税課税（取引先）</td><td>チェックボックス</td><td>売上税課税のデフォルト値。取引先関連情報の新規作成時にデフォルトとして使用される。</td></tr>
<tr><td>売上税課税（商品）</td><td>チェックボックス</td><td>売上税課税のデフォルト値。商品関連情報の新規商作成時にデフォルトとして使用される。</td></tr>
<tr><td>販売オーダーから購買オーダー生成　カスタムページ</td><td>テキスト(80)</td><td>システム項目</td></tr>
<tr><td>販売オーダーライン追加　カスタムページ</td><td>テキスト(80)</td><td>システム項目</td></tr>
<tr><td>販売オーダーを自動でリリース</td><td>チェックボックス</td><td>販売オーダー作成時に、ステータスを自動的にリリース済とする場合にオンにする。ワークフローや手動でリリースする運用の場合にはオフにする。（参考）販売オーダーがリリース済の場合のみ、後続の業務処理である出庫／梱包／出荷が動作します。</td></tr>
<tr><td>販売オーダー確認書カスタムページ</td><td>テキスト(80)</td><td>販売オーダー確認書PDFにカスタムページを使用する場合にページ名を指定する。</td></tr>
<tr><td>販売オーダー確認書カスタムページ（添付用）</td><td>テキスト(80)</td><td>未使用項目</td></tr>
<tr><td>販売リードタイム</td><td>数値(4, 2)</td><td>販売リードタイムのデフォルト値（単位：日）。販売オーダーの約束日の算出に使用される。</td></tr>
<tr><td>販売見積ライン追加　カスタムページ</td><td>テキスト(80)</td><td>システム項目</td></tr>
<tr><td>販売見積書カスタムページ</td><td>テキスト(80)</td><td>販売見積書PDFにカスタムページを使用する場合にページ名を指定する。</td></tr>
<tr><td>販売見積書カスタムページ（添付用）</td><td>テキスト(80)</td><td>未使用項目</td></tr>
<tr><td>物品税課税（取引先）</td><td>チェックボックス</td><td>物品税課税のデフォルト値。取引先関連情報の新規作成時にデフォルトとして使用される。</td></tr>
<tr><td>物品税課税（商品）</td><td>チェックボックス</td><td>物品税課税のデフォルト値。商品関連情報の新規商作成時にデフォルトとして使用される。</td></tr>
<tr><td>返品確認書カスタムページ</td><td>テキスト(80)</td><td>返品確認書PDFにカスタムページを使用する場合にページ名を指定する。</td></tr>
<tr><td>返品確認書カスタムページ（添付用）</td><td>テキスト(80)</td><td>未使用項目</td></tr>
<tr><td>無償購買オーダーを許可</td><td>チェックボックス</td><td>購買オーダーラインの作成時、価格を無償とした場合でも非請求理由の入力を必須としない場合にオンにする。</td></tr>
<tr><td>無償販売オーダーを許可</td><td>チェックボックス</td><td>販売見積ライン、販売オーダーラインの作成時、価格を無償とした場合でも非請求理由の入力を必須としない場合にオンにする。</td></tr>
<tr><td>郵便番号(請求先)</td><td>テキスト(80)</td><td>購買オーダー作成時の請求先住所のデフォルト値。</td></tr>
<tr><td>預託在庫消費サブタブを表示</td><td>チェックボックス</td><td>出庫／梱包／出荷タブ内に、預託在庫消費サブタブを表示する場合にオンにする。</td></tr>
<tr><td>利用税課税（取引先）</td><td>チェックボックス</td><td>利用税課税のデフォルト値。取引先関連情報の新規作成時にデフォルトとして使用される。</td></tr>
<tr><td>利用税課税（商品）</td><td>チェックボックス</td><td>利用税を扱う場合にオンにする。新規商品登録時にデフォルト値として使用される。</td></tr>
</tbody></table></div><br/><br/>
</body>
</apex:outputpanel>
<div class="helpPageFooter">
Copyright  2008-{!YEAR( TODAY() )} Glovia International, inc. All rights reserved.<br/>
Various trademarks held by their respective owners.
</div>
</apex:page>
-->
<!-- Generation Date & Time: 2/7/2011 9:34:24 PM PST -->