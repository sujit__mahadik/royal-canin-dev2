<apex:page controller="gii.SalesOrderAckCon" showheader="false"
    renderAs="PDF" standardStylesheets="false">
    <!-- renderAs="PDF" -->
    <apex:stylesheet value="{!URLFOR($Resource.gii__gloviaOM_Resources, 'CSS/JP_gloviaCSS.css')}" />
    <!-- v10.5 added while background style to avoid LE issue for blue background -->
    <div style="background-color:white">     
    <apex:panelGrid columns="2" width="100%" id="header" styleClass="tableLayoutFixed">
        
        <apex:panelGrid columns="2" styleClass="companyTable tableLayoutFixed" width="85%">
            <img src="{!sitePrefix}/servlet/servlet.FileDownload?file={!LogoId}" alt="logo" border="0" width="100" height="50" />
            <apex:panelGroup >
                <apex:outputText value="{!$Organization.Name}" styleClass="companyName tableLayoutFixed" /><br />
                <apex:outputText value="〒{!$Organization.PostalCode}"/><br/>                
                <apex:outputText value="{!$Organization.State}{!$Organization.City}{!$Organization.Street}"/><br />
                <apex:outputText value="{!$Organization.Phone}" /><br /> 
            </apex:panelGroup>
        </apex:panelGrid>

        <apex:panelGroup styleClass="tableLayoutFixed">
            <apex:outputtext value="{!$Label.gii__salesorderacknowledgement}" styleClass="title right tableLayoutFixed" />
        </apex:panelGroup>

        <apex:panelGroup styleClass="tableLayoutFixed">
            <apex:outputtext value="{!$Label.gii__shipto}" styleClass="companyName tableLayoutFixed" /><br />      
            〒{!SalesOrder.gii__ShipToZipPostalCode__c} <br />
            {!SalesOrder.gii__ShipToStateProvince__c}{!SalesOrder.gii__ShipToCity__c}{!SalesOrder.gii__ShipToStreet__c}<br />
            {!accountAddress.Name}<br />            
            <br />
        </apex:panelGroup>

        <apex:panelGrid columns="2" width="100%" cellspacing="0" styleClass="tableLayoutFixed bordersTopBottom" columnClasses="columnBorders left">
            <apex:outputtext value="{!$Label.gii__ordertype}" />
            <apex:outputtext value="{!SalesOrder.gii__OrderType__c}" />
            <apex:outputtext value="{!$Label.gii__salesorder}" />
            <apex:outputtext value="{!SalesOrder.Name}" />
            <apex:outputtext value="{!$Label.gii__printdate}" />
            <apex:outputtext value="{!CurrentDateTimeinUserLocale}" />
            <apex:outputtext value="{!$Label.gii__customerpo}" />
            <apex:outputField value="{!SalesOrder.gii__CustomerPONumber__c}" />
            <apex:outputtext value="{!$Label.gii__createddate}" />
            <apex:outputField value="{!SalesOrder.CreatedDate}" />
            <apex:outputtext value="{!$Label.gii__paymentterms}" />
            <apex:outputField value="{!SalesOrder.gii__PaymentTerms__c}" />
            <apex:outputtext value="{!$Label.gii__deliveryterms}" />
            {!IF(SalesOrder.gii__DeliveryTerms__c = null,'',SalesOrder.gii__DeliveryTerms__c)}
            <apex:outputtext value="{!$Label.gii__currency}" rendered="{!UsingMultipleCurrencies}" />
            <apex:outputtext value="{!currencyCode}" rendered="{!UsingMultipleCurrencies}" />
            <!-- ** Uncomment following line if AR Payment Terms name needs to be printed  
            and comment out the Payment terms picklist field for sales order header  **
            <apex:outputField value="{!SalesOrder.gii__ARPaymentTerms__r.name}"/><br/>
            -->
        </apex:panelGrid>

        <apex:panelGrid columns="2" width="100%" border="0" styleClass="tableLayoutFixed" frame="box">
            <apex:panelGroup >
                <apex:outputtext value="{!$Label.gii__invoiceto}" styleClass="companyName tableLayoutFixed" />
                <br />              
                〒{!SalesOrder.gii__BillingZipPostalCode__c}<br />
                {!SalesOrder.gii__BillingStateProvince__c}{!SalesOrder.gii__BillingCity__c}{!SalesOrder.gii__BillingStreet__c}<br />
                {!IF(SalesOrder.gii__BillingName__c = null,SalesOrder.gii__Account__r.Name,SalesOrder.gii__BillingName__c)} <br />                  
                <br />                      
            </apex:panelGroup>
        </apex:panelGrid>

    </apex:panelGrid>

    <b><apex:outputtext value="{!$Label.gii__details}" styleClass="companyName" /></b>
    <br />
    <apex:panelGrid width="100%" id="Detail">
        <apex:dataTable width="100%" id="DataTable" value="{!SalesOrderLines}"
            var="SalesOrderLine" border="1"
            styleClass="tableLayoutFixed font10 verticalTop"
            headerClass="tableLayoutFixed"
            columnsWidth="6%,,12%,3%,12%,11%,12%,12%"
            rendered="{!IF(OrderDetailcount > 0 ,true,false)}">
            <apex:column headervalue="{!$Label.gii__line_column_heading}"
                headerClass="left" styleClass="left">
                <apex:outputField value="{!SalesOrderLine.Name}" />
            </apex:column>
            <apex:column headerClass="left" styleClass="left">
                <apex:facet name="header">
                    {!$ObjectType.gii__SalesOrderLine__c.Fields.gii__Product__c.label}
                </apex:facet>
                {!IF(SalesOrderLine.gii__Product__r.name = null,SalesOrderLine.gii__Product__c, SalesOrderLine.gii__Product__r.name)}<br />
                <b>{!IF(SalesOrderLine.gii__Product__r.gii__ProductCode__c =
                    null,"",SalesOrderLine.gii__Product__r.gii__ProductCode__c )}</b>
            </apex:column>
            <apex:column headerClass="left" styleClass="left">
                <apex:facet name="header">{!$ObjectType.gii__SalesOrderLine__c.Fields.gii__StockUM__c.label} </apex:facet>
                <apex:outputField value="{!SalesOrderLine.gii__StockUM__c}" rendered="{!IF(SalesOrderLine.gii__SellingUnitofMeasure__c <> null,false,true)}"/>
                <apex:outputText value="{!SalesOrderLine.SellingUnitofMeasure__r.name}" rendered="{!IF(SalesOrderLine.gii__SellingUnitofMeasure__c <> null,true,false)}"/>  
            </apex:column>
            <apex:column headervalue="{!$Label.gii__tax}" headerClass="left"
                styleClass="left">  
                {!IF(OR(SalesOrderLine.gii__ExciseTaxable__c = true,SalesOrderLine.gii__SalesTaxable__c = true,SalesOrderLine.gii__UsageTaxable__c = true),"Y"," ")}
            </apex:column>
            <apex:column headerClass="left" styleClass="left">
                <apex:facet name="header">{!$ObjectType.gii__SalesOrderLine__c.Fields.gii__ScheduledDate__c.label} </apex:facet>
                <apex:outputfield value="{!SalesOrderLine.gii__ScheduledDate__c}" />
                <br />{!IF(SalesOrderLine.gii__CancelReason__c != null,$Label.CANCELLED," " )} 
            </apex:column>
            <apex:column headerClass="right" styleClass="right"
                headervalue="{!$ObjectType.gii__SalesOrderLine__c.Fields.gii__OrderQuantity__c.label}">
                <apex:outputField value="{!SalesOrderLine.gii__OrderQuantity__c}" />
            </apex:column>
            <apex:column headerClass="right" styleClass="right"
                headervalue="{!$ObjectType.gii__SalesOrderLine__c.Fields.gii__UnitPrice__c.label}">
                <apex:outputField rendered="{!NOT(UsingMultipleCurrencies)}"
                    value="{!SalesOrderLine.gii__UnitPrice__c}" />
                <apex:outputText rendered="{!UsingMultipleCurrencies}"
                    value="{!FORMATCURRENCY(currencyCode,SalesOrderLine.gii__UnitPrice__c)}" />
            </apex:column>
            <apex:column headerClass="right" styleClass="right"
                headervalue="{!$ObjectType.gii__SalesOrderLine__c.Fields.gii__Extension__c.label}">
                <apex:outputField rendered="{!NOT(UsingMultipleCurrencies)}"
                    value="{!SalesOrderLine.gii__Extension__c}" />
                <apex:outputText rendered="{!UsingMultipleCurrencies}"
                    value="{!FORMATCURRENCY(currencyCode,SalesOrderLine.gii__Extension__c)}" />
            </apex:column>
        </apex:dataTable>
        <apex:panelGrid columns="2" width="100%"
            columnClasses="subtotal right" styleClass="subtotal tableLayoutFixed"
            rendered="{!IF(OrderDetailcount > 0 ,true,false)}">
            <apex:outputText value="{!$Label.gii__linesubtotal}" />
            <apex:outputField rendered="{!NOT(UsingMultipleCurrencies)}"
                value="{!SalesOrder.gii__ProductExtensionAmount__c}" />
            <apex:outputText rendered="{!UsingMultipleCurrencies}"
                value="{!FORMATCURRENCY(currencyCode,SalesOrder.gii__ProductExtensionAmount__c)}" />
        </apex:panelGrid>
        <apex:dataTable width="100%" id="ServiceDataTable"
            value="{!ServiceOrderLines}" var="ServiceOrderLine"
            styleClass="tableLayoutFixed font10 verticalTop"
            headerClass="tableLayoutFixed" border="1"
            rowclasses="font10 verticalTop" columnClasses="font10 verticalTop"
            columnsWidth="6%,,11%,3%,14%,14%,9%,11%,11%"
            rendered="{!IF(ServiceDetailcount > 0 ,true,false)}">
            <apex:column headervalue="{!$Label.gii__line_column_heading}">
                <apex:outputField value="{!ServiceOrderLine.Name}" />
            </apex:column>
            <apex:column >
                <apex:facet name="header">
                    {!$ObjectType.gii__ServiceOrderLine__c.Fields.gii__Product__c.label}<br />
                </apex:facet>
                {!IF(ServiceOrderLine.gii__Product__r.name = null,ServiceOrderLine.gii__Product__c, ServiceOrderLine.gii__Product__r.name)}<br />
                <b> {!IF(ServiceOrderLine.gii__Product__r.gii__ProductCode__c =
                    null,"",ServiceOrderLine.gii__Product__r.gii__ProductCode__c )}</b>
            </apex:column>
            <apex:column >
                <apex:facet name="header">{!$ObjectType.gii__ServiceOrderLine__c.Fields.gii__ServiceType__c.label} </apex:facet>
                <apex:outputField value="{!ServiceOrderLine.gii__ServiceType__r.name}" />
            </apex:column>
            <apex:column headervalue="{!$Label.gii__tax}">
                {!IF(OR(ServiceOrderLine.gii__ExciseTaxable__c = true,ServiceOrderLine.gii__SalesTaxable__c = true,ServiceOrderLine.gii__UsageTaxable__c = true),"Y"," ")}
            </apex:column>
            <apex:column >
                <apex:facet name="header">{!$ObjectType.gii__ServiceOrderLine__c.Fields.gii__SalesOrderLine__c.label} </apex:facet>
                {!IF(ServiceOrderLine.gii__SalesOrderLine__r.name = null,ServiceOrderLine.gii__SalesOrderLine__c, ServiceOrderLine.gii__SalesOrderLine__r.name )}
                <br />{!IF(ServiceOrderLine.gii__CancelReason__c != null,$Label.CANCELLED," " )} 
            </apex:column>
            <apex:column styleClass="right" headerClass="right"
                headervalue="{!$ObjectType.gii__ServiceOrderLine__c.Fields.gii__ServiceChargeType__c.label}">
                <apex:outputField value="{!ServiceOrderLine.gii__ServiceChargeType__c}" />
            </apex:column>
            <apex:column styleClass="right" headerClass="right"
                headervalue="{!$ObjectType.gii__ServiceOrderLine__c.Fields.gii__OrderQuantity__c.label}">
                <apex:outputField value="{!ServiceOrderLine.gii__OrderQuantity__c}" />
            </apex:column>
            <apex:column styleClass="right" headerClass="right"
                headervalue="{!$ObjectType.gii__ServiceOrderLine__c.Fields.gii__UnitPrice__c.label}">
                <apex:outputField rendered="{!NOT(UsingMultipleCurrencies)}"
                    value="{!ServiceOrderLine.gii__UnitPrice__c}" />
                <apex:outputText rendered="{!UsingMultipleCurrencies}"
                    value="{!FORMATCURRENCY(currencyCode,ServiceOrderLine.gii__UnitPrice__c)}" />
            </apex:column>
            <apex:column styleClass="right" headerClass="right"
                headervalue="{!$ObjectType.gii__ServiceOrderLine__c.Fields.gii__Extension__c.label}">
                <apex:outputfield rendered="{!NOT(UsingMultipleCurrencies)}"
                    value="{!ServiceOrderLine.gii__Extension__c}" />
                <apex:outputText rendered="{!UsingMultipleCurrencies}"
                    value="{!FORMATCURRENCY(currencyCode,ServiceOrderLine.gii__Extension__c)}" />
            </apex:column>
        </apex:dataTable>
        <apex:panelGrid columns="2" width="100%" columnClasses="right"
            styleClass="subtotal tableLayoutFixed"
            rendered="{!IF(ServiceDetailcount > 0 ,true,false)}">
            <apex:outputText value="{!$Label.gii__servicelinesubtotal}" />
            <apex:outputField rendered="{!NOT(UsingMultipleCurrencies)}"
                value="{!SalesOrder.gii__ServiceExtensionAmount__c}" />
            <apex:outputText rendered="{!UsingMultipleCurrencies}"
                value="{!FORMATCURRENCY(currencyCode,SalesOrder.gii__ServiceExtensionAmount__c)}" />
        </apex:panelGrid>
        <apex:panelGrid columns="2" width="100%"
            columnClasses="subtotal right"
            rendered="{!IF(SalesOrder.gii__TotalOrderDiscountAmount__c = 0,false,true)}"
            styleClass="subtotal tableLayoutFixed">
            <apex:outputText value="{!$Label.gii__orderdiscount}" />
            <apex:outputField rendered="{!NOT(UsingMultipleCurrencies)}"
                value="{!SalesOrder.gii__TotalOrderDiscountAmount__c}" />
            <apex:outputText rendered="{!UsingMultipleCurrencies}"
                value="{!FORMATCURRENCY(currencyCode,SalesOrder.gii__TotalOrderDiscountAmount__c)}" />
        </apex:panelGrid>
        <apex:dataTable width="100%" id="AddlChgDataTable"
            rendered="{!IF(SalesOrder.gii__AdditionalChargeAmount__c = 0,false,true)}"
            value="{!SalesOrderAdditionalCharge}" var="SOAddlChg"
            styleClass="tableLayoutFixed font10 verticalTop"
            headerClass="tableLayoutFixed" border="1"
            rowClasses="font10 verticalTop" columnClasses="font10 verticalTop"
            columnsWidth="6%,,3%,12%,12%,20%">
            <apex:column headerValue="Line">
                <apex:outputfield value="{!SOAddlChg.name}" />
            </apex:column>
            <apex:column >
                <apex:facet name="header">
                    {!$ObjectType.gii__SalesOrderAdditionalCharge__c.Fields.gii__AdditionalCharge__c.label}
                </apex:facet>
                <apex:outputfield value="{!SOAddlChg.gii__AdditionalCharge__r.name}" /> &nbsp;-&nbsp;<apex:outputfield value="{!SOAddlChg.gii__AdditionalCharge__r.gii__Description__c}"/ >
            </apex:column>
            <apex:column headervalue="{!$Label.gii__tax}">
                {!IF(OR(SOAddlChg.gii__ExciseTaxable__c = true,SOAddlChg.gii__SalesTaxable__c = true,SOAddlChg.gii__UsageTaxable__c = true),"Y"," ")}
            </apex:column>
            <apex:column styleClass="right" headerClass="right">
                <apex:facet name="header">
                    {!$ObjectType.gii__SalesOrderAdditionalCharge__c.Fields.gii__Quantity__c.label}
                </apex:facet>
                <apex:outputfield value="{!SOAddlChg.gii__Quantity__c}"/ >
            </apex:column>
            <apex:column styleClass="right" headerClass="right">
                <apex:facet name="header">
                    {!$ObjectType.gii__SalesOrderAdditionalCharge__c.Fields.gii__UnitPrice__c.label}
                </apex:facet>
                <apex:outputfield rendered="{!NOT(UsingMultipleCurrencies)}"
                    value="{!SOAddlChg.gii__UnitPrice__c}"/ >
                    <apex:outputText rendered="{!UsingMultipleCurrencies}"
                        value="{!FORMATCURRENCY(currencyCode,SOAddlChg.gii__UnitPrice__c)}" />
            </apex:column>
            <apex:column styleClass="right" headerClass="right">
                <apex:facet name="header">
                    {!$ObjectType.gii__SalesOrderAdditionalCharge__c.Fields.gii__AdditionalChargeAmount__c.label}
                </apex:facet>
                <apex:outputfield rendered="{!NOT(UsingMultipleCurrencies)}"
                    value="{!SOAddlChg.gii__AdditionalChargeAmount__c}"/ >
                    <apex:outputText rendered="{!UsingMultipleCurrencies}"
                        value="{!FORMATCURRENCY(currencyCode,SOAddlChg.gii__AdditionalChargeAmount__c)}" />
            </apex:column>
        </apex:dataTable>
        <apex:panelGrid columns="2" width="100%"
            columnClasses="subtotal right"
            rendered="{!IF(SalesOrder.gii__AdditionalChargeAmount__c = 0,false,true)}"
            styleClass="subtotal tableLayoutFixed">
            <apex:outputText value="{!$Label.gii__additionalchargessubtotal}" />
            <apex:outputField rendered="{!NOT(UsingMultipleCurrencies)}"
                value="{!SalesOrder.gii__AdditionalChargeAmount__c}" />
            <apex:outputText rendered="{!UsingMultipleCurrencies}"
                value="{!FORMATCURRENCY(currencyCode,SalesOrder.gii__AdditionalChargeAmount__c)}" />
        </apex:panelGrid>
        <apex:dataTable width="70%" border="1"
            styleClass="tableLayoutFixed font10 verticalTop"
            rendered="{!IF(OrderTaxRate.size = 0,false,true)}"
            columnsWidth="20%,,20%,20%" id="TaxDetails" value="{!OrderTaxRate}"
            var="Taxdetail" headerClass="tableLayoutFixed"
            rowclasses="font10 verticalTop" columnClasses="font10 verticalTop">
            <apex:column headerValue="{!$Label.gii__taxrate}">
                <apex:outputText value="{!Taxdetail.gii__Rate__c}" />%
            </apex:column>
            <apex:column headerValue="{!$Label.gii__type}"> 
                {!IF(Taxdetail.gii__Type__c = "Excise",Taxdetail.gii__Jurisdiction__c & " - Excise"," ")}
                {!IF(Taxdetail.gii__Type__c = "Sales",Taxdetail.gii__Jurisdiction__c & " - Sales"," ")}
                {!IF(Taxdetail.gii__Type__c = "Usage",Taxdetail.gii__Jurisdiction__c & " - Use"," ")}
            </apex:column>
            <apex:column styleClass="right" headerClass="right"
                headerValue="{!$Label.gii__taxableamount}">
                <apex:outputField rendered="{!NOT(UsingMultipleCurrencies)}"
                    value="{!Taxdetail.gii__TaxableAmount__c}" />
                <apex:outputText rendered="{!UsingMultipleCurrencies}"
                    value="{!FORMATCURRENCY(currencyCode,Taxdetail.gii__TaxableAmount__c)}" />
            </apex:column>
            <apex:column styleClass="right" headerClass="right"
                headerValue="{!$Label.gii__taxamount}">
                <apex:outputField rendered="{!NOT(UsingMultipleCurrencies)}"
                    value="{!Taxdetail.gii__TaxAmount__c}" />
                <apex:outputText rendered="{!UsingMultipleCurrencies}"
                    value="{!FORMATCURRENCY(currencyCode,Taxdetail.gii__TaxAmount__c)}" />
            </apex:column>
        </apex:dataTable>
        <apex:panelGrid columns="2" width="100%" columnClasses="right"
            rendered="{!IF(SalesOrder.gii__TotalTaxAmount__c = 0,false,true)}"
            styleClass="subtotal tableLayoutFixed">
            <apex:outputText value="{!$Label.gii__taxtotal}" />
            <apex:outputField rendered="{!NOT(UsingMultipleCurrencies)}"
                value="{!SalesOrder.gii__TotalTaxAmount__c}" />
            <apex:outputText rendered="{!UsingMultipleCurrencies}"
                value="{!FORMATCURRENCY(currencyCode,SalesOrder.gii__TotalTaxAmount__c)}" />
        </apex:panelGrid>
        <apex:dataTable width="60%" border="1"
            styleClass="tableLayoutFixed font10 verticalTop"
            rendered="{!IF(SalesOrder.gii__TotalVATAmount__c = 0,false,true)}"
            id="VATDetails" value="{!OrderVATRate}" var="VATdetail"
            headerClass="tableLayoutFixed" rowClasses="font10 verticalTop"
            columnClasses="font10 verticalTop">

            <apex:column headerValue="VAT" styleClass="left" headerClass="left">
                <apex:outputField value="{!VATdetail.gii__VAT_Name__c}" />

            </apex:column>

            <apex:column styleClass="right" headerClass="right"
                headerValue="{!$Label.gii__netsales}">
                <apex:outputField rendered="{!NOT(UsingMultipleCurrencies)}"
                    value="{!VATdetail.gii__NetSales__c}" />
                <apex:outputText rendered="{!UsingMultipleCurrencies}"
                    value="{!FORMATCURRENCY(currencyCode,VATdetail.gii__NetSales__c)}" />
            </apex:column>

            <apex:column styleClass="right" headerClass="right"
                headerValue="{!$Label.gii__vatrate}">
                <apex:outputText value="{!VATdetail.gii__VATRate__c}" />%
            </apex:column>

            <apex:column styleClass="right" headerClass="right"
                headerValue="{!$Label.gii__vatamount}">
                <apex:outputField rendered="{!NOT(UsingMultipleCurrencies)}"
                    value="{!VATdetail.gii__VATAmount__c}" />
                <apex:outputText rendered="{!UsingMultipleCurrencies}"
                    value="{!FORMATCURRENCY(currencyCode,VATdetail.gii__VATAmount__c)}" />
            </apex:column>
        </apex:dataTable>
        <apex:panelGrid columns="2"
            rendered="{!IF(SalesOrder.gii__TotalVATAmount__c = 0,false,true)}"
            width="100%" columnClasses="subtotal right"
            styleClass="subtotal tableLayoutFixed">
            <apex:outputText value="{!$Label.gii__vattotal}" />
            <apex:outputField rendered="{!NOT(UsingMultipleCurrencies)}"
                value="{!SalesOrder.gii__TotalVATAmount__c}" />
            <apex:outputText rendered="{!UsingMultipleCurrencies}"
                value="{!FORMATCURRENCY(currencyCode,SalesOrder.gii__TotalVATAmount__c)}" />
        </apex:panelGrid>

        <br />
        <apex:panelGrid columns="2" width="100%" columnClasses="right"
            styleClass="subtotal tableLayoutFixed">
            <apex:outputText value="{!$Label.gii__ordertotal}" />
            <apex:outputField rendered="{!NOT(UsingMultipleCurrencies)}"
                value="{!SalesOrder.gii__NetAmount__c}" />
            <apex:outputText rendered="{!UsingMultipleCurrencies}"
                value="{!FORMATCURRENCY(currencyCode,SalesOrder.gii__NetAmount__c)}" />
        </apex:panelGrid>
    </apex:panelGrid>
    <apex:panelGrid columns="1">
        <apex:panelGroup rendered="{!SalesOrder.gii__PrintDocumentText__c}">
            <br />
            <apex:outputField value="{!SalesOrder.gii__DocumentText__c}" />
        </apex:panelGroup>
    </apex:panelGrid>
    </div>
</apex:page>