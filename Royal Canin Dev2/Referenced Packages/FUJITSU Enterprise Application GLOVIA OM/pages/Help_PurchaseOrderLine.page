<apex:page Controller="gii.HelpGetLangCon" showheader="false" sidebar="false" 
    action="{!URLFOR($Resource.gloviaOM_Help, Lang + '/PurchaseOrderLine.htm')}" >
</apex:page>

<!--
<apex:page Controller="gii.HelpGetLangCon" showheader="false" sidebar="false">
<link rel="stylesheet" type="text/css" href="help.css"/>
<link rel="stylesheet" type="text/css" href="/sCSS/Theme2/default/help.css"/>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=ISO-8859-1"/>
<title>
<apex:outputtext value="{!IF(Lang = 'ja',"購買オーダーライン","Purchase Order Line")}" />
</title>
</head>

<body>
<table style="font-family: Arial; width: 100%; height: 52px;"> 
<tbody><tr style="color: white;"><td style="background-color: rgb(115, 126, 150);">
<big><b>
<apex:outputtext value="{!IF(Lang = 'ja',"購買オーダーラインホーム","Purchase Order Line Home")}" />
</b></big>
</td></tr></tbody> 
</table> 
<br/>

<span style="font-size: 10pt; font-family: &quot;Arial&quot;,&quot;sans-serif&quot;;">
<table style="font-family: Arial; font-weight: bold; color: white; width: 100%; height: 20px;">
<tbody><tr><td style="background-color: rgb(115, 126, 150);">
<apex:outputtext value="{!IF(Lang = 'ja',"購買オーダーラインとは","What is a Purchase Order Line")}" />
</td></tr></tbody>
</table>

<p class="MsoNormal">
<apex:outputtext value="{!IF(Lang = 'ja',"購買オーダーラインは、購買する商品の詳細情報を指定するためのものであり、在庫品、非在庫品、サービス品のいずれも指定することができます。","Purchase Order Lines can define the detail of Products to purchase. Purchase order lines can be added with stock, non-stock or service products. ")}" /><br/><br/>
</p>
<fieldset class="note"><legend><strong>Note</strong></legend>
<div class="noteBody">
<img src="/img/help/helpNote_icon.gif" align="left"/>
<p class="MsoNormal">
<apex:outputtext value="{!IF(Lang = 'ja',"在庫品に対する購買オーダーの場合は、その情報に従って、商品在庫および在庫トランザクション履歴のレコードが更新されます。更新タイミングは次の通りです。[1] 新規購買オーダーラインの追加時、[2] 購買オーダーラインの削除時、[3] オーダー数量の変更時、[4] 購買オーダーラインのキャンセル時、[5] 在庫場所の変更時。","Product inventory record is updated with purchase order quantity only for stock products. Product inventory and inventory transaction history records are updated when [1] New line is added, [2] Line is deleted, [3] Order quantity is changed, [4] Line is cancelled, and [5] Warehouse is changed.")}" /></p>
</div>
</fieldset>
<br/>
<table style="color: white; font-family: Arial; font-weight: bold; height: 20px; width: 100%;">
<tbody><tr><td style="background-color: rgb(115, 126, 150);">
<apex:outputtext value="{!IF(Lang = 'ja',"購買オーダーラインの作成","Creating Purchase Order Line")}" />
</td></tr></tbody>
</table>
 
<p class="MsoNormal" style="margin-top: 12pt;"><apex:outputtext value="{!IF(Lang = 'ja',"購買オーダーラインは次の２つ方法で作成することができます。[1] 購買オーダー画面での追加、[2] 販売オーダーからの購買オーダーの生成（販売オーダー画面上の「購買オーダーを生成」ボタン）。","Purchase Order Line can be created by the following 2 way.  [1] Add in Purchase Orders screen,  [2] Creating new purchase order from sales order.")}" /><br/></p><br/><br/>
<b><apex:outputtext value="{!IF(Lang = 'ja', "関連情報：", "See Also:")}" /></b><br/>
<apex:outputlink value="/apex/Help_PurchaseOrder"><apex:outputtext value="{!IF(Lang = 'ja',"購買オーダー","Purchase Order")}" /></apex:outputlink><br/>
<apex:outputlink value="/apex/Help_PurchaseOrderReceiptLine"><apex:outputtext value="{!IF(Lang = 'ja',"購買オーダー受入ライン","Purchase Order Receipt Lines")}" /></apex:outputlink><br/>
<br/><br/>
<table style="color: white; font-family: Arial; font-weight: bold; height: 20px; width: 100%;">
<tbody><tr><td style="background-color: rgb(115, 126, 150);">
<apex:outputtext value="{!IF(Lang = 'ja',"購買オーダーラインのボタン／リンク","Buttons/Links of Purchase Order Line")}" />
</td></tr></tbody>
</table>

<div class="p">
<table summary="" class="featureTable" cellpadding="4"
 cellspacing="0" width="100%">
<thead align="left"><tr>
<th class="featureTableHeader" valign="top" width="200">
<apex:outputtext value="{!IF(Lang = 'ja',
"ボタン／リンク",
"Button/Link")}" />
</th>
<th class="featureTableHeader" valign="top">
<apex:outputtext value="{!IF(Lang = 'ja',
"説明",
"Description")}" />
</th></tr>
</thead>

<tbody><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"受入","Receive")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"受入未の数量の受入を行なう。","Receives remaining quantity.")}" />
</td>
</tr></tbody>
</table>
</div><br/><br/>
<table style="color: white; font-family: Arial; font-weight: bold; height: 20px; width: 100%;">
<tbody><tr><td style="background-color: rgb(115, 126, 150);">
<apex:outputtext value="{!IF(Lang = 'ja',"購買オーダーラインの項目","Fields of Purchase Order Line")}" />
</td></tr></tbody>
</table>

<div class="p">
<table summary="" class="featureTable" cellpadding="4"
 cellspacing="0" width="100%">
<thead align="left"><tr>
<th class="featureTableHeader" valign="top" width="200">
<apex:outputtext value="{!IF(Lang = 'ja',
"項目",
"Field")}" />
</th>
<th class="featureTableHeader" valign="top">
<apex:outputtext value="{!IF(Lang = 'ja',
"説明",
"Description")}" />
</th></tr>
</thead>

<tbody><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"購買オーダーライン","Purchase Order Line")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"購買オーダーラインの番号。","A purchase order line number. A unique number for the purchase order line.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"会計グループコード","Accounting Group Code")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"購買トランザクションの会計コード。商品関連情報の値がセットされる。","The code of accounting which Purchase Order transactions belongs to.  Defaulted from Product Reference.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"キャンセル日","Cancel Date")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"購買オーダーラインのキャンセル日。","Cancel Date of Purchase Order Line.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"キャンセル数","Cancelled Quantity")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"キャンセル数。購買オーダーラインは部分キャンセルも可能。","Cancelled Quantity. Purchased Order line can be partially cancelled.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"キャンセル理由","Cancel Reason")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"購買オーダーライン・キャンセルの理由。","Cancel reason for the line")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"運送会社","Carrier")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"購入商品の出荷の際の運送会社。購買オーダー（ヘッダー）の値がセットされる。","Shipping carrier of purchase order line. Defaulted from Purchase Order header.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"クローズ","Close")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"購買オーダーラインをクローズする場合にオンにする。一旦クローズされると、再オープンされるまで、以降の編集が不可となる。","If checked, closes the order line. Once closed, maintenance is prevented unless the order line is reopened.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"クローズ日","Closed Date")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"購買オーダーラインのクローズ日。","Date when line was closed")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"終了日","End Date")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"サービス終了日。","Service end date")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"説明","Line Description")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"購買オーダーラインの説明。","Description for the Purchase order line")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"ラインステータス","Line Status")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"オーダー数をすべて受入れた場合（オーダー数－キャンセル数＝受入数の場合）に受入済となり、他の場合は一部受入済となる。","\"Received\" when purchase order is fully receivedy, otherwise \"Partially Received\".")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"非請求理由","No Charge Reason")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"購買オーダーラインが無償の場合の理由。","No charge reason for the purchase order line’s zero unit price")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"非在庫品","Non-Stock")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"当該商品が非在庫品の場合にオンとなる。商品関連情報のデフォルト値がセットされる。","Non-Stock indicator. Defaulted from product reference.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"オーダー数","Order Quantity")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"購入する数量。","The required quantity")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"商品","Product")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"購入する商品。","Product to purchase.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"商品在庫","Product Inventory")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"購入する商品を受入る際の商品在庫の番号。在庫品の場合に有効。","Product Inventory for the product and Ship-to warehouse of purchase order line.
This created or updated only for stock products.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"購買オーダー","Purchase Order")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"購買オーダーの番号。","Purchase Order number. ")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"受入金額","Received Amount")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"受入済の商品金額の合計。","The total amount received to date")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"受入数","Received Quantity")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"受入済の数量。購買受入処理にて更新される。","This quantity is updated from the receipt transaction.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"再オープン","Re-Open")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"クローズされている購買オーダーラインを再オープンする場合にオンにする。","If checked, reopens an order line.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"サービス商品","Service")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"当該商品がサービス品の場合にオンとなる。商品関連情報のデフォルト値がセットされる。","Service product indicator. Defaulted from Product Reference.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"サービスタイプ","Service Type")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"当該商品がサービス品の場合のサービスタイプ。商品関連情報のデフォルト値がセットされる。","Service Type (Defaulted from product reference)")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"出荷先","Ship To")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"購入する商品の出荷先となる在庫場所。購買オーダー（ヘッダー）の値がセットされる。","Ship to warehouse location. Defaulted from Ship-To Warehouse location of Purchase Order header.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"開始日","Start Date")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"サービス開始日。","Service start date")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"課税","Taxable")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"フラグがオンの場合、購買取引は課税対象。","If checked, this transaction is taxable.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"原価","Unit Cost")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"購買オーダーラインの商品の原価。商品関連情報のデフォルト値がセットされる。","Unit Cost of the Purchase Order Line Product. Defaulted from Product Reference.")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"単位","Unit Of Measure")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"購買オーダーラインの商品の単位。商品関連情報のデフォルト値がセットされる。","Unit Of Measure code for Purchase Order Line Product. Defaulted from Product Reference. ")}" />
</td>
</tr><tr>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"単価","Unit Price")}" />
</td>
<td>
<apex:outputtext value="{!IF(Lang = 'ja',"購買オーダーラインの商品の単価。購買オーダー（ヘッダー）に指定された価格表に従って値がセットされる。","Unit price for Purchase Order Line Product. Defaulted from Price Book specified at the Purchase Order.")}" />
</td>
</tr></tbody>
</table>
</div>
</span></body>

<div class="helpPageFooter">Copyright  2008-{!YEAR( TODAY() )} Glovia International, inc. All rights reserved.<br/>
Various trademarks held by their respective owners.</div>
</html>
</apex:page>
-->
<!-- Generation Date & Time: 5/4/2010 6:58:58 PM PST -->