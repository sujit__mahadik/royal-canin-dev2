<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>true</allowInChatterGroups>
    <compactLayoutAssignment>CycleCountCompact</compactLayoutAssignment>
    <compactLayouts>
        <fullName>CycleCountCompact</fullName>
        <fields>Name</fields>
        <fields>Warehouse__c</fields>
        <fields>TotalTagsCounted__c</fields>
        <fields>TotalTagsVoided__c</fields>
        <fields>TotalTagsInventoryFrozen__c</fields>
        <fields>TotalTagsApproved__c</fields>
        <fields>TotalTagswithExceptions__c</fields>
        <fields>TotalTags__c</fields>
        <label>Cycle Count Compact</label>
    </compactLayouts>
    <customHelpPage>Help_CycleCount</customHelpPage>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>Cycle Counts</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>AdditionalBlankTags__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Additional Blank Tags</label>
        <precision>3</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>AutoApproveTags__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Auto Approve Tags</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>BeginningTagNumber__c</fullName>
        <defaultValue>1</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Beginning Tag Number</label>
        <precision>8</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Closed__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Closed</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Description__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Description</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>FreezeAnyTime__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Freeze Any Time</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>FreezeBeforeCount__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Freeze Before Count</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>FreezeOnCount__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Freeze On Count</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>FreezeOnTagCreation__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Freeze On Tag Creation</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>InProcess__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>IF(AND(TotalTags__c &gt; 0, NOT(Closed__c)), TRUE, FALSE)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>In-Process</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>TotalBlankTags__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Total Blank Tags</label>
        <summaryFilterItems>
            <field>CycleCountTag__c.BlankTag__c</field>
            <operation>equals</operation>
            <value>True</value>
        </summaryFilterItems>
        <summaryForeignKey>CycleCountTag__c.CycleCount__c</summaryForeignKey>
        <summaryOperation>count</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>TotalTagsApproved__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Total Tags Approved</label>
        <summaryFilterItems>
            <field>CycleCountTag__c.Approved__c</field>
            <operation>equals</operation>
            <value>True</value>
        </summaryFilterItems>
        <summaryForeignKey>CycleCountTag__c.CycleCount__c</summaryForeignKey>
        <summaryOperation>count</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>TotalTagsCounted__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Total Tags Counted</label>
        <summaryFilterItems>
            <field>CycleCountTag__c.OriginalCountQuantity__c</field>
            <operation>notEqual</operation>
            <value></value>
        </summaryFilterItems>
        <summaryFilterItems>
            <field>CycleCountTag__c.Void__c</field>
            <operation>equals</operation>
            <value>False</value>
        </summaryFilterItems>
        <summaryForeignKey>CycleCountTag__c.CycleCount__c</summaryForeignKey>
        <summaryOperation>count</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>TotalTagsInventoryFrozen__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Total Tags Inventory Frozen</label>
        <summaryFilterItems>
            <field>CycleCountTag__c.FreezeQuantity__c</field>
            <operation>notEqual</operation>
            <value></value>
        </summaryFilterItems>
        <summaryFilterItems>
            <field>CycleCountTag__c.Void__c</field>
            <operation>equals</operation>
            <value>False</value>
        </summaryFilterItems>
        <summaryForeignKey>CycleCountTag__c.CycleCount__c</summaryForeignKey>
        <summaryOperation>count</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>TotalTagsInventoryUpdated__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Total Tags Inventory Updated</label>
        <summaryFilterItems>
            <field>CycleCountTag__c.InventoryUpdated__c</field>
            <operation>equals</operation>
            <value>True</value>
        </summaryFilterItems>
        <summaryForeignKey>CycleCountTag__c.CycleCount__c</summaryForeignKey>
        <summaryOperation>count</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>TotalTagsVoided__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Total Tags Voided</label>
        <summaryFilterItems>
            <field>CycleCountTag__c.Void__c</field>
            <operation>equals</operation>
            <value>True</value>
        </summaryFilterItems>
        <summaryForeignKey>CycleCountTag__c.CycleCount__c</summaryForeignKey>
        <summaryOperation>count</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>TotalTags__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Total Tags</label>
        <summaryForeignKey>CycleCountTag__c.CycleCount__c</summaryForeignKey>
        <summaryOperation>count</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>TotalTagswithExceptions__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Total Tags with Exceptions</label>
        <summaryFilterItems>
            <field>CycleCountTag__c.InventoryUpdateExceptionFound__c</field>
            <operation>equals</operation>
            <value>True</value>
        </summaryFilterItems>
        <summaryForeignKey>CycleCountTag__c.CycleCount__c</summaryForeignKey>
        <summaryOperation>count</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>TotalUnAccountedTags__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>TotalTags__c -  TotalTagsCounted__c -  TotalTagsVoided__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Total Un-Accounted Tags</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Warehouse__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Warehouse</label>
        <referenceTo>Warehouse__c</referenceTo>
        <relationshipLabel>Cycle Counts</relationshipLabel>
        <relationshipName>CycleCounts</relationshipName>
        <required>true</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <label>Cycle Count</label>
    <listViews>
        <fullName>All1</fullName>
        <columns>NAME</columns>
        <columns>Warehouse__c</columns>
        <columns>Closed__c</columns>
        <columns>InProcess__c</columns>
        <columns>TotalTags__c</columns>
        <columns>TotalBlankTags__c</columns>
        <columns>TotalTagsVoided__c</columns>
        <columns>TotalTagsInventoryUpdated__c</columns>
        <columns>TotalTagswithExceptions__c</columns>
        <columns>TotalTagsCounted__c</columns>
        <columns>TotalTagsApproved__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
        <language>en_US</language>
    </listViews>
    <nameField>
        <label>Cycle Count</label>
        <trackHistory>false</trackHistory>
        <type>Text</type>
    </nameField>
    <pluralLabel>Cycle Counts</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Warehouse__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Closed__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Description__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>TotalTags__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>TotalUnAccountedTags__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>TotalTagsApproved__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>TotalBlankTags__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>TotalTagsInventoryUpdated__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>TotalTagswithExceptions__c</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>Description__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Closed__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>InProcess__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>TotalTags__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>TotalTagsApproved__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>TotalTagsInventoryUpdated__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>TotalTagswithExceptions__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>TotalUnAccountedTags__c</lookupDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchFilterFields>Description__c</searchFilterFields>
        <searchFilterFields>Warehouse__c</searchFilterFields>
        <searchResultsAdditionalFields>Description__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Closed__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>InProcess__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Warehouse__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>TotalTags__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>TotalTagsApproved__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>TotalTagswithExceptions__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>TotalUnAccountedTags__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
    <validationRules>
        <fullName>AdditionalBlankTagscannotbebelowZero</fullName>
        <active>true</active>
        <description>Additional Blank Tags value cannot be less than zero.</description>
        <errorConditionFormula>AdditionalBlankTags__c &lt; 0</errorConditionFormula>
        <errorDisplayField>AdditionalBlankTags__c</errorDisplayField>
        <errorMessage>Additional Blank Tags value cannot be less than zero.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>BeginningTagNumbercannotbebelowZero</fullName>
        <active>true</active>
        <description>Beginning Tag Number value cannot be less than zero.</description>
        <errorConditionFormula>BeginningTagNumber__c &lt; 0</errorConditionFormula>
        <errorDisplayField>BeginningTagNumber__c</errorDisplayField>
        <errorMessage>Beginning Tag Number value cannot be less than zero.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>CannotChangeClosed</fullName>
        <active>true</active>
        <errorConditionFormula>AND( ISCHANGED(Closed__c ) ,  Closed__c = FALSE)</errorConditionFormula>
        <errorDisplayField>Closed__c</errorDisplayField>
        <errorMessage>Cannot reset value of closed flag to re-open the cycle count.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>CannotChangeInProcessCycleCount</fullName>
        <active>true</active>
        <errorConditionFormula>AND( 
NOT( ISNEW()),
InProcess__c, 
OR(ISCHANGED(BeginningTagNumber__c), 
ISCHANGED(FreezeAnyTime__c), 
ISCHANGED(FreezeBeforeCount__c), 
ISCHANGED(FreezeOnCount__c), 
ISCHANGED(FreezeOnTagCreation__c), 
ISCHANGED( Warehouse__c) 
) 
)</errorConditionFormula>
        <errorMessage>Cannot Change Warehouse, Freeze Options or Beginning Tag Number for In-Process Cycle Count.</errorMessage>
    </validationRules>
    <webLinks>
        <fullName>AdditionalBlankTags</fullName>
        <availability>online</availability>
        <description>Additional Blank Tags</description>
        <displayType>button</displayType>
        <linkType>page</linkType>
        <masterLabel>Additional Blank Tags</masterLabel>
        <openType>replace</openType>
        <page>CycleCountBlankTagGeneration</page>
        <protected>false</protected>
    </webLinks>
    <webLinks>
        <fullName>CreateCycleCountProducts</fullName>
        <availability>online</availability>
        <description>Create Cycle Count Products</description>
        <displayType>button</displayType>
        <linkType>page</linkType>
        <masterLabel>Create Cycle Count Products</masterLabel>
        <openType>replace</openType>
        <page>CycleCountProductQueue</page>
        <protected>false</protected>
    </webLinks>
    <webLinks>
        <fullName>FreezeInventory</fullName>
        <availability>online</availability>
        <description>Freeze Inventory</description>
        <displayType>button</displayType>
        <linkType>page</linkType>
        <masterLabel>Freeze Inventory</masterLabel>
        <openType>replace</openType>
        <page>CycleCountFreezeInventoryPage</page>
        <protected>false</protected>
    </webLinks>
    <webLinks>
        <fullName>GenerateTags</fullName>
        <availability>online</availability>
        <description>Generate Tags</description>
        <displayType>button</displayType>
        <linkType>page</linkType>
        <masterLabel>Generate Tags</masterLabel>
        <openType>replace</openType>
        <page>CycleCountTagGenerationPage</page>
        <protected>false</protected>
    </webLinks>
    <webLinks>
        <fullName>HelpforthisPage</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <encodingKey>UTF-8</encodingKey>
        <hasMenubar>false</hasMenubar>
        <hasScrollbars>true</hasScrollbars>
        <hasToolbar>false</hasToolbar>
        <height>600</height>
        <isResizable>true</isResizable>
        <linkType>url</linkType>
        <masterLabel>Help for this Page</masterLabel>
        <openType>newWindow</openType>
        <position>none</position>
        <protected>false</protected>
        <showsLocation>false</showsLocation>
        <showsStatus>false</showsStatus>
        <url>{!URLFOR($Site.Prefix + &quot;/apex/gii__Help_CycleCount&quot;)}</url>
    </webLinks>
    <webLinks>
        <fullName>InventoryCountEntry</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <height>600</height>
        <linkType>page</linkType>
        <masterLabel>Inventory Count Entry</masterLabel>
        <openType>noSidebar</openType>
        <page>CycleCountInventoryCountEntryPage</page>
        <protected>false</protected>
    </webLinks>
    <webLinks>
        <fullName>PreInventoryUpdateExceptionCheck</fullName>
        <availability>online</availability>
        <description>Pre-Inventory Update Exception Check</description>
        <displayType>button</displayType>
        <linkType>page</linkType>
        <masterLabel>Pre-Inventory Update Exception Check</masterLabel>
        <openType>replace</openType>
        <page>CycleCountPreInvUpdateCheckPage</page>
        <protected>false</protected>
    </webLinks>
    <webLinks>
        <fullName>ReverseInventoryFreeze</fullName>
        <availability>online</availability>
        <description>Reverse Inventory Freeze</description>
        <displayType>button</displayType>
        <linkType>page</linkType>
        <masterLabel>Reverse Inventory Freeze</masterLabel>
        <openType>replace</openType>
        <page>CycleCountReverseInvFreezePage</page>
        <protected>false</protected>
    </webLinks>
    <webLinks>
        <fullName>TagsPDFGeneration</fullName>
        <availability>online</availability>
        <description>Tags PDF Generation</description>
        <displayType>button</displayType>
        <linkType>page</linkType>
        <masterLabel>Tags PDF Generation</masterLabel>
        <openType>replace</openType>
        <page>CycleCountTagsPDFGeneration</page>
        <protected>false</protected>
    </webLinks>
    <webLinks>
        <fullName>UpdateInventory</fullName>
        <availability>online</availability>
        <description>Update Inventory</description>
        <displayType>button</displayType>
        <linkType>page</linkType>
        <masterLabel>Update Inventory</masterLabel>
        <openType>replace</openType>
        <page>CycleCountUpdateInventoryPage</page>
        <protected>false</protected>
    </webLinks>
</CustomObject>
