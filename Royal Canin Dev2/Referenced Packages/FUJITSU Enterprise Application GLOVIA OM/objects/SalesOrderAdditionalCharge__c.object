<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SalesOrderAdditionalChargeCompact</compactLayoutAssignment>
    <compactLayouts>
        <fullName>SalesOrderAdditionalChargeCompact</fullName>
        <fields>Name</fields>
        <fields>AdditionalCharge__c</fields>
        <fields>Quantity__c</fields>
        <fields>UnitPrice__c</fields>
        <fields>AdditionalChargeAmount__c</fields>
        <fields>VATAmount__c</fields>
        <fields>TaxAmount__c</fields>
        <fields>TotalAmount__c</fields>
        <fields>SalesOrder__c</fields>
        <fields>ForwardToInvoice__c</fields>
        <label>Sales Order Additional Charge Compact</label>
    </compactLayouts>
    <customHelpPage>Help_SalesOrderAdditionalCharge</customHelpPage>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>Sales Order Additional Charge Details</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>AGC__c</fullName>
        <deprecated>false</deprecated>
        <description>Accounting Group Code</description>
        <externalId>false</externalId>
        <label>Accounting Group Code</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Business Sales</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Consumer Sales</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>AdditionalChargeAmount__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>/* gii__Quantity__c  *  gii__UnitPrice__c */
If( RoundAmountstoZeroDecimal__c,
   IF( RoundtoZeroDecimalwithRoundOff__c,
         Floor(Round(Quantity__c  *  UnitPrice__c,2)) ,
         Round(Quantity__c  *  UnitPrice__c, 0)
     )
   ,Round(Quantity__c  *  UnitPrice__c, 2)
)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Additional Charge Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>AdditionalChargeTaxRateId__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Additional Charge Tax Rate Id</label>
        <length>80</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>AdditionalCharge__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Additional Charge</label>
        <referenceTo>AdditionalCharge__c</referenceTo>
        <relationshipLabel>Sales Order Additional Charges</relationshipLabel>
        <relationshipName>alesOrderAdditionalCharges</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>CalculatedShippingAmount__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Calculated Shipping Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>ExciseTaxable__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Excise Taxable</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>FlatFee__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Flat Fee</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>ForwardToInvoice__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Forward To Invoice</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>FreeShipping__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Free Shipping</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Invoiced__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Invoiced</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>NoChargeReason__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>No Charge Reason</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Promotion</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Sample</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Warranty Replacement</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Promotion__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Promotion</label>
        <referenceTo>Promotion__c</referenceTo>
        <relationshipLabel>Sales Order Additional Charges</relationshipLabel>
        <relationshipName>SalesOrderAdditionalCharges</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Purge__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Purge</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Quantity__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Quantity</label>
        <precision>12</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>RefreshTax__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Refresh Tax</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>RefreshVAT__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Refresh VAT</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>RoundAmountstoZeroDecimal__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Round Amounts to Zero Decimal</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>RoundtoZeroDecimalwithRoundOff__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Round to Zero Decimal with Round Off</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>SalesOrderServiceTicketLine__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Sales Order Service Ticket Line</label>
        <referenceTo>SalesOrderServiceTicketLine__c</referenceTo>
        <relationshipLabel>Sales Order Additional Charges</relationshipLabel>
        <relationshipName>SalesOrderAdditionalCharges</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SalesOrder__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Sales Order</label>
        <referenceTo>SalesOrder__c</referenceTo>
        <relationshipLabel>Sales Order Additional Charges</relationshipLabel>
        <relationshipName>SOAddlCharge</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>SalesTaxable__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Sales Taxable</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>ServiceTicketCharge__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Service Ticket Charge</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>ServiceTicketCharge__c.ServiceTicket__c</field>
                <operation>equals</operation>
                <valueField>$Source.ServiceTicket__c</valueField>
            </filterItems>
            <isOptional>true</isOptional>
        </lookupFilter>
        <referenceTo>ServiceTicketCharge__c</referenceTo>
        <relationshipLabel>Sales Order Additional Charges</relationshipLabel>
        <relationshipName>SalesOrderAdditionalCharges</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ServiceTicketLine__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Service Ticket Line</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>ServiceTicketLine__c.ServiceTicket__c</field>
                <operation>equals</operation>
                <valueField>$Source.ServiceTicket__c</valueField>
            </filterItems>
            <isOptional>true</isOptional>
        </lookupFilter>
        <referenceTo>ServiceTicketLine__c</referenceTo>
        <relationshipLabel>Sales Order Additional Charges</relationshipLabel>
        <relationshipName>SalesOrderAdditionalCharges</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ServiceTicket__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Service Ticket</label>
        <referenceTo>ServiceTicket__c</referenceTo>
        <relationshipLabel>Sales Order Additional Charges</relationshipLabel>
        <relationshipName>SalesOrderAdditionalCharges</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Shipped__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>This field is not being used</description>
        <externalId>false</externalId>
        <label>Shipped</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>ShippingAmountOff__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Shipping Amount Off</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>TaxAmount__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Tax Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>TaxLocation__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Tax Location</label>
        <referenceTo>TaxLocation__c</referenceTo>
        <relationshipLabel>Sales Order Additional Charges</relationshipLabel>
        <relationshipName>R00N40000001ai3EEAQ</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>TotalAmount__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>AdditionalChargeAmount__c  + TaxAmount__c  + VATAmount__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Total Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>UnitCost__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Unit Cost</label>
        <precision>14</precision>
        <required>false</required>
        <scale>4</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>UnitPrice__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Unit Price</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>UnitofMeasure__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Unit of Measure</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Case</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Dozen</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Each</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Gallon</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>UsageTaxable__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Usage Taxable</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>VATAmount__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>VAT Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>VATRate__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>VAT Rate</label>
        <precision>7</precision>
        <required>false</required>
        <scale>4</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>VAT__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>VAT</label>
        <referenceTo>VAT__c</referenceTo>
        <relationshipLabel>Sales Order Additional Charges</relationshipLabel>
        <relationshipName>SOAddlChgVAT</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <label>Sales Order Additional Charge</label>
    <nameField>
        <displayFormat>{000000}</displayFormat>
        <label>Additional Charge Sequence</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Sales Order Additional Charges</pluralLabel>
    <searchLayouts>
        <lookupDialogsAdditionalFields>SalesOrder__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>AdditionalCharge__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Quantity__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>UnitPrice__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>AdditionalChargeAmount__c</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>SalesOrder__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>AdditionalCharge__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Quantity__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>UnitPrice__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>AdditionalChargeAmount__c</lookupPhoneDialogsAdditionalFields>
        <searchFilterFields>AdditionalCharge__c</searchFilterFields>
        <searchFilterFields>SalesOrder__c</searchFilterFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>AlreadyInvoiced</fullName>
        <active>true</active>
        <description>Cannot change Additional Charge information, Already Invoiced</description>
        <errorConditionFormula>AND( 
     ( Invoiced__c = true ),
     OR (            
                 ISCHANGED(   AdditionalCharge__c     )  ,
                 ISCHANGED(  AdditionalChargeTaxRateId__c      )  ,
                 ISCHANGED(    ExciseTaxable__c    )  ,
                 ISCHANGED(   NoChargeReason__c     )  ,
                 ISCHANGED(    Quantity__c    )  ,
                 ISCHANGED(   SalesTaxable__c     )  ,
                 ISCHANGED(  TaxLocation__c      )  ,
                 ISCHANGED(      UnitPrice__c  )  ,
                 ISCHANGED(   UsageTaxable__c     )  ,
                 ISCHANGED(    VAT__c    )  ,
				 ISCHANGED(    RoundAmountstoZeroDecimal__c    )  ,
				 ISCHANGED(    RoundtoZeroDecimalwithRoundOff__c    )  ,
                 ISCHANGED(     ForwardToInvoice__c     )  ,
                 ISCHANGED(   TaxAmount__c     )  ,
                 ISCHANGED(   VATAmount__c     )  
    )
)</errorConditionFormula>
        <errorMessage>Cannot change Additional Charge information, Addtional Charge is already Invoiced</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>InvociedFlagCannotBeReset</fullName>
        <active>true</active>
        <errorConditionFormula>AND(PRIORVALUE( Invoiced__c )  = true, Invoiced__c = false, ForwardToInvoice__c = true)</errorConditionFormula>
        <errorDisplayField>Invoiced__c</errorDisplayField>
        <errorMessage>Cannot reset Invoiced flag.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>QuanitityMustBeGreaterThanZero</fullName>
        <active>false</active>
        <description>Quanitity must be greater than zero</description>
        <errorConditionFormula>Quantity__c &lt;= 0</errorConditionFormula>
        <errorDisplayField>Quantity__c</errorDisplayField>
        <errorMessage>Quantity must be greater than zero.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>UnitPriceCannotlessthanZero</fullName>
        <active>true</active>
        <description>This validation is OBSELETED in v2.0.
This validation should remain IN-ACTIVE.</description>
        <errorConditionFormula>Year(Today()) = 1800</errorConditionFormula>
        <errorMessage>Invalid  Year</errorMessage>
    </validationRules>
    <webLinks>
        <fullName>ForwardToInvoice</fullName>
        <availability>online</availability>
        <displayType>massActionButton</displayType>
        <linkType>page</linkType>
        <masterLabel>Forward To Invoice</masterLabel>
        <openType>replace</openType>
        <page>SalesOrderAddlChgList</page>
        <protected>false</protected>
        <requireRowSelection>true</requireRowSelection>
    </webLinks>
    <webLinks>
        <fullName>HelpforthisPage</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <encodingKey>UTF-8</encodingKey>
        <hasMenubar>false</hasMenubar>
        <hasScrollbars>true</hasScrollbars>
        <hasToolbar>false</hasToolbar>
        <height>600</height>
        <isResizable>true</isResizable>
        <linkType>url</linkType>
        <masterLabel>Help for this Page</masterLabel>
        <openType>newWindow</openType>
        <position>none</position>
        <protected>false</protected>
        <showsLocation>false</showsLocation>
        <showsStatus>false</showsStatus>
        <url>{!URLFOR($Site.Prefix + &quot;/apex/gii__Help_SalesOrderAdditionalCharge&quot;)}</url>
    </webLinks>
</CustomObject>
