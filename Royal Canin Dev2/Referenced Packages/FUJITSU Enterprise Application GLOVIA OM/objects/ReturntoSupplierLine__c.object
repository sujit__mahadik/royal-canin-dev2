<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>ReturntoSupplierLineCompact</compactLayoutAssignment>
    <compactLayouts>
        <fullName>ReturntoSupplierLineCompact</fullName>
        <fields>Name</fields>
        <fields>ReturntoSupplier__c</fields>
        <fields>Product__c</fields>
        <fields>OrderQuantity__c</fields>
        <fields>BuyingUnitofMeasure__c</fields>
        <fields>BuyingUMTotalShippedQuantity__c</fields>
        <fields>StockUM__c</fields>
        <fields>PurchaseOrder__c</fields>
        <fields>PurchaseOrderLine__c</fields>
        <fields>PurchaseOrderReceiptLine__c</fields>
        <label>Return to Supplier Line Compact</label>
    </compactLayouts>
    <customHelpPage>Help_ReturntoSupplierLine</customHelpPage>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>Return to Supplier Lines</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>AGC__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Accounting Group Code</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Business Sales</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Consumer Sales</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>BuyingUMTotalShippedQuantity__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Total Shipped Quantity (Buying UM)</label>
        <summarizedField>ReturntoSupplierLineShipment__c.BuyingUMShippedQuantity__c</summarizedField>
        <summaryForeignKey>ReturntoSupplierLineShipment__c.ReturntoSupplierLine__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>BuyingUnitofMeasure__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Buying Unit of Measure</label>
        <referenceTo>UnitofMeasure__c</referenceTo>
        <relationshipLabel>Return to Supplier Lines</relationshipLabel>
        <relationshipName>ReturntoSupplierLines</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ConversionFactor__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Conversion Factor</label>
        <precision>18</precision>
        <required>false</required>
        <scale>9</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Extension__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>/* gii__OrderQuantity__c *  gii__UnitPrice__c */

IF( RoundAmountstoZeroDecimal__c , 
 IF( RoundtoZeroDecimalwithRoundOff__c , 
   FLOOR(ROUND(OrderQuantity__c * UnitPrice__c,2)) , 
   ROUND(OrderQuantity__c * UnitPrice__c, 0) 
 ), 
 ROUND(OrderQuantity__c * UnitPrice__c, 2) 
)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Extension</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>LineDescription__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Line Description</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>OpenQuantity__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>/*gii__OrderQuantity__c -  gii__TotalShippedQuantity__c */
IF(  BuyingUMTotalShippedQuantity__c &lt;&gt; 0,   OrderQuantity__c -  BuyingUMTotalShippedQuantity__c , OrderQuantity__c -  TotalShippedQuantity__c)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Open Quantity</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>OrderQuantity__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Order Quantity</label>
        <precision>12</precision>
        <required>true</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Product__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Product</label>
        <referenceTo>Product2Add__c</referenceTo>
        <relationshipLabel>Return to Supplier Lines</relationshipLabel>
        <relationshipName>ReturntoSupplierLines</relationshipName>
        <required>true</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>PurchaseOrderLine__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Purchase Order Line</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>PurchaseOrderLine__c.PurchaseOrder__c</field>
                <operation>equals</operation>
                <valueField>$Source.PurchaseOrder__c</valueField>
            </filterItems>
            <isOptional>true</isOptional>
        </lookupFilter>
        <referenceTo>PurchaseOrderLine__c</referenceTo>
        <relationshipLabel>Return to Supplier Lines</relationshipLabel>
        <relationshipName>ReturntoSupplierLines</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>PurchaseOrderReceiptLine__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Purchase Order Receipt Line</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>PurchaseOrderReceiptLine__c.PurchaseOrderLine__c</field>
                <operation>equals</operation>
                <valueField>$Source.PurchaseOrderLine__c</valueField>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>PurchaseOrderReceiptLine__c</referenceTo>
        <relationshipLabel>Return to Supplier Lines</relationshipLabel>
        <relationshipName>ReturntoSupplierLines</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>PurchaseOrder__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Purchase Order</label>
        <referenceTo>PurchaseOrder__c</referenceTo>
        <relationshipLabel>Return to Supplier Lines</relationshipLabel>
        <relationshipName>ReturntoSupplierLines</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ReturntoSupplier__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Return to Supplier</label>
        <referenceTo>ReturntoSupplier__c</referenceTo>
        <relationshipLabel>Return to Supplier Lines</relationshipLabel>
        <relationshipName>ReturntoSupplierLines</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>ReverseConversionFactor__c</fullName>
        <defaultValue>1.0</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Reverse Conversion Factor</label>
        <precision>18</precision>
        <required>false</required>
        <scale>9</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>RoundAmountstoZeroDecimal__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Round Amounts to Zero Decimal</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>RoundtoZeroDecimalwithRoundOff__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Round to Zero Decimal with Round Off</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>ShipFrom__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Ship From</label>
        <referenceTo>Warehouse__c</referenceTo>
        <relationshipLabel>Return to Supplier Lines</relationshipLabel>
        <relationshipName>ReturntoSupplierLines</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>StockUM__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Unit Of Measure</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Case</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Dozen</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Each</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Taxable__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Taxable</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>TotalShippedQuantity__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Total Shipped Quantity</label>
        <summarizedField>ReturntoSupplierLineShipment__c.ShippedQuantity__c</summarizedField>
        <summaryForeignKey>ReturntoSupplierLineShipment__c.ReturntoSupplierLine__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>UnitCost__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Unit Cost</label>
        <precision>17</precision>
        <required>false</required>
        <scale>4</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>UnitPrice__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Unit Price</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <label>Return to Supplier Line</label>
    <nameField>
        <displayFormat>{000000}</displayFormat>
        <label>Return to Supplier Line</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Return to Supplier Lines</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>ReturntoSupplier__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>ShipFrom__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Product__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>OrderQuantity__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>UnitPrice__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Extension__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>OpenQuantity__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>PurchaseOrder__c</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>ReturntoSupplier__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>ShipFrom__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Product__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>OrderQuantity__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>UnitPrice__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Extension__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>OpenQuantity__c</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>ReturntoSupplier__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>ShipFrom__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Product__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>OrderQuantity__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>UnitPrice__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Extension__c</lookupPhoneDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchFilterFields>Product__c</searchFilterFields>
        <searchFilterFields>ReturntoSupplier__c</searchFilterFields>
        <searchFilterFields>ShipFrom__c</searchFilterFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>ValidateOrderQuantity</fullName>
        <active>true</active>
        <errorConditionFormula>OrderQuantity__c &lt;= 0</errorConditionFormula>
        <errorDisplayField>OrderQuantity__c</errorDisplayField>
        <errorMessage>Zero or negative numbers are not allowed</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>ValidatePOLineProduct</fullName>
        <active>true</active>
        <errorConditionFormula>AND(PurchaseOrderLine__c &lt;&gt; null,
Product__c !=  PurchaseOrderLine__r.Product__c)</errorConditionFormula>
        <errorDisplayField>Product__c</errorDisplayField>
        <errorMessage>Purchase Order Line does not belong to this Product</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>ValidateProductChange</fullName>
        <active>true</active>
        <description>Do not allow product change if shipments are added</description>
        <errorConditionFormula>AND( ISCHANGED( Product__c) ,  TotalShippedQuantity__c &gt; 0)</errorConditionFormula>
        <errorDisplayField>Product__c</errorDisplayField>
        <errorMessage>Cannot change the product, when shipped quantity is greater than zero</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>ValidateRoundingPolicies</fullName>
        <active>true</active>
        <description>Cannot change rounding policy field values, if shipments are added.</description>
        <errorConditionFormula>AND( 
    (TotalShippedQuantity__c &gt; 0),
     OR(            
		ISCHANGED(RoundAmountstoZeroDecimal__c),
		ISCHANGED(RoundtoZeroDecimalwithRoundOff__c) 
    )
)</errorConditionFormula>
        <errorMessage>Cannot change rounding policy field values, if shipments are added.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>ValidateServiceProduct</fullName>
        <active>true</active>
        <description>Do not allow service products</description>
        <errorConditionFormula>Product__r.Service__c = true</errorConditionFormula>
        <errorDisplayField>Product__c</errorDisplayField>
        <errorMessage>Service products are not allowed</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>ValidateShippedQuantity</fullName>
        <active>true</active>
        <errorConditionFormula>/*AND(gii__TotalShippedQuantity__c &gt; 0,gii__OrderQuantity__c &lt;  gii__TotalShippedQuantity__c) */
AND(BuyingUMTotalShippedQuantity__c &gt; 0,OrderQuantity__c &lt;  BuyingUMTotalShippedQuantity__c)</errorConditionFormula>
        <errorDisplayField>OrderQuantity__c</errorDisplayField>
        <errorMessage>Order Quantity cannot be less than Shipped Quantity</errorMessage>
    </validationRules>
    <webLinks>
        <fullName>HelpforthisPage</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <encodingKey>UTF-8</encodingKey>
        <hasMenubar>false</hasMenubar>
        <hasScrollbars>true</hasScrollbars>
        <hasToolbar>false</hasToolbar>
        <height>600</height>
        <isResizable>true</isResizable>
        <linkType>url</linkType>
        <masterLabel>Help for this Page</masterLabel>
        <openType>newWindow</openType>
        <position>none</position>
        <protected>false</protected>
        <showsLocation>false</showsLocation>
        <showsStatus>false</showsStatus>
        <url>{!URLFOR($Site.Prefix + &quot;/apex/gii__Help_ReturntoSupplierLine&quot;)}</url>
    </webLinks>
</CustomObject>
