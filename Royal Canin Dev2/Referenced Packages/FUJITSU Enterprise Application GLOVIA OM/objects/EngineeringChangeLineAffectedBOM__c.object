<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>true</allowInChatterGroups>
    <compactLayoutAssignment>EngineeringChangeLineAffectedBOMCompact</compactLayoutAssignment>
    <compactLayouts>
        <fullName>EngineeringChangeLineAffectedBOMCompact</fullName>
        <fields>Name</fields>
        <fields>EngineeringChangeLine__c</fields>
        <fields>BillofMaterialProduct__c</fields>
        <fields>BillofMaterial__c</fields>
        <fields>BillofMaterialLine__c</fields>
        <fields>Sequence__c</fields>
        <fields>Processed__c</fields>
        <label>Engineering Change Line Affected BOM Compact</label>
    </compactLayouts>
    <customHelpPage>Help_EngineeringChangeLineAffectedBOM</customHelpPage>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>Engineering Change Line Affected Bill of Materials</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>BillofMaterialLine__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Bill of Material Line</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>BillofMaterialLine__c.BillofMaterial__c</field>
                <operation>equals</operation>
                <valueField>$Source.BillofMaterial__c</valueField>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>BillofMaterialLine__c</referenceTo>
        <relationshipLabel>Engineering Change Line Affected BOMs</relationshipLabel>
        <relationshipName>EngineeringChangeLineAffectedBOMs</relationshipName>
        <required>true</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>BillofMaterialProduct__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Bill of Material Product</label>
        <referenceTo>Product2Add__c</referenceTo>
        <relationshipLabel>Engineering Change Line Affected BOMs</relationshipLabel>
        <relationshipName>EngineeringChangeLineAffectedBOMs</relationshipName>
        <required>true</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>BillofMaterial__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Bill of Material</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>BillofMaterial__c.Product__c</field>
                <operation>equals</operation>
                <valueField>$Source.BillofMaterialProduct__c</valueField>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>BillofMaterial__c</referenceTo>
        <relationshipLabel>Engineering Change Line Affected BOMs</relationshipLabel>
        <relationshipName>EngineeringChangeLineAffectedBOMs</relationshipName>
        <required>true</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>EngineeringChangeLine__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Engineering Change Line</label>
        <referenceTo>EngineeringChangeLine__c</referenceTo>
        <relationshipLabel>Engineering Change Line Affected BOMs</relationshipLabel>
        <relationshipName>EngineeringChangeLineAffectedBOMs</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>ExceptionFoundDuringExecute__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Exception Found During Execute</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Exception__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Exception</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>5</visibleLines>
    </fields>
    <fields>
        <fullName>Processed__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Processed</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Sequence__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Sequence</label>
        <precision>4</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>UniqueRecordKey__c</fullName>
        <caseSensitive>false</caseSensitive>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Unique Record Key</label>
        <length>80</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <label>Engineering Change Line Affected BOM</label>
    <nameField>
        <displayFormat>{00000000}</displayFormat>
        <label>Engineering Change Line Affected BOM</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Engineering Change Line Affected BOMs</pluralLabel>
    <searchLayouts>
        <lookupDialogsAdditionalFields>BillofMaterial__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>BillofMaterialLine__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>EngineeringChangeLine__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Processed__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>BillofMaterialProduct__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Sequence__c</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>BillofMaterial__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>BillofMaterialLine__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>EngineeringChangeLine__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Processed__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>BillofMaterialProduct__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Sequence__c</lookupPhoneDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchFilterFields>BillofMaterial__c</searchFilterFields>
        <searchFilterFields>BillofMaterialLine__c</searchFilterFields>
        <searchFilterFields>EngineeringChangeLine__c</searchFilterFields>
        <searchFilterFields>BillofMaterialProduct__c</searchFilterFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>CannotChaneProcessedFlag</fullName>
        <active>true</active>
        <description>Cannot change value for Processed field when record was already processed.</description>
        <errorConditionFormula>AND( PRIORVALUE(Processed__c ) = true ,  ISCHANGED(Processed__c) )</errorConditionFormula>
        <errorDisplayField>Processed__c</errorDisplayField>
        <errorMessage>Cannot change value for Processed field when record was already processed.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>ValidateBOMLineProduct</fullName>
        <active>true</active>
        <errorConditionFormula>BillofMaterialLine__r.Component__c &lt;&gt;  EngineeringChangeLine__r.Product__c</errorConditionFormula>
        <errorDisplayField>BillofMaterialLine__c</errorDisplayField>
        <errorMessage>Component of the BOM Line does not match with Engineering Change Line Product</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>ValidateBOMProduct</fullName>
        <active>true</active>
        <errorConditionFormula>EngineeringChangeLine__r.Product__c =  BillofMaterialProduct__c</errorConditionFormula>
        <errorMessage>The Bill of Material Product cannot be same as Engineering Change Line Product.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>ValidateBillofMatirialChange</fullName>
        <active>true</active>
        <description>Bill of Material cannot be changed.</description>
        <errorConditionFormula>AND( Processed__c = true, ISCHANGED( BillofMaterial__c ))</errorConditionFormula>
        <errorDisplayField>BillofMaterial__c</errorDisplayField>
        <errorMessage>Bill of Material cannot be changed.</errorMessage>
    </validationRules>
    <webLinks>
        <fullName>HelpforthisPage</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <encodingKey>UTF-8</encodingKey>
        <hasMenubar>false</hasMenubar>
        <hasScrollbars>true</hasScrollbars>
        <hasToolbar>false</hasToolbar>
        <height>600</height>
        <isResizable>true</isResizable>
        <linkType>url</linkType>
        <masterLabel>Help for this Page</masterLabel>
        <openType>newWindow</openType>
        <position>none</position>
        <protected>false</protected>
        <showsLocation>false</showsLocation>
        <showsStatus>false</showsStatus>
        <url>{!URLFOR($Site.Prefix + &quot;/apex/gii__Help_EngineeringChangeLineAffectedBOM&quot;)}</url>
    </webLinks>
</CustomObject>
