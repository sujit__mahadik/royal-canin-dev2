<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SupplyReceiptCompact</compactLayoutAssignment>
    <compactLayouts>
        <fullName>SupplyReceiptCompact</fullName>
        <fields>Name</fields>
        <fields>Product__c</fields>
        <fields>Warehouse__c</fields>
        <fields>Location__c</fields>
        <fields>ReceiptDate__c</fields>
        <fields>ReceivedQuantity__c</fields>
        <fields>SupplyOrder__c</fields>
        <fields>ProductInventory__c</fields>
        <fields>ProductInventorybyLocation__c</fields>
        <fields>ProductInventoryQuantityDetail__c</fields>
        <label>Supply Receipt Compact</label>
    </compactLayouts>
    <customHelpPage>Help_SupplyReceipt</customHelpPage>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>Supply Receipts</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>AGC__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Accounting Group Code</label>
        <length>80</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ConversionRate__c</fullName>
        <defaultValue>1.0</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Conversion Rate</label>
        <precision>18</precision>
        <required>false</required>
        <scale>6</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>LocationBin__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Location Bin</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>LocationBin__c.Location__c</field>
                <operation>equals</operation>
                <valueField>$Source.Location__c</valueField>
            </filterItems>
            <isOptional>true</isOptional>
        </lookupFilter>
        <referenceTo>LocationBin__c</referenceTo>
        <relationshipLabel>Supply Receipts</relationshipLabel>
        <relationshipName>SupplyReceipts</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Location__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Location</label>
        <referenceTo>Location__c</referenceTo>
        <relationshipLabel>Supply Receipts</relationshipLabel>
        <relationshipName>SupplyReceipts</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ProductInventoryQuantityDetail__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Product Inventory Quantity Detail</label>
        <referenceTo>ProductInventoryQuantityDetail__c</referenceTo>
        <relationshipLabel>Supply Receipts</relationshipLabel>
        <relationshipName>SupplyReceipts</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ProductInventory__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Product Inventory</label>
        <referenceTo>ProductInventory__c</referenceTo>
        <relationshipLabel>Supply Receipts</relationshipLabel>
        <relationshipName>R00N40000001af1WEAQ</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>ProductInventorybyLocation__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Product Inventory by Location</label>
        <referenceTo>ProductInventorybyLocation__c</referenceTo>
        <relationshipLabel>Supply Receipts</relationshipLabel>
        <relationshipName>SupplyReceipts</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ProductLot__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Product Lot</label>
        <referenceTo>ProductLot__c</referenceTo>
        <relationshipLabel>Supply Receipts</relationshipLabel>
        <relationshipName>SupplyReceipts</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ProductSerial__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Product Serial</label>
        <referenceTo>ProductSerial__c</referenceTo>
        <relationshipLabel>Supply Receipts</relationshipLabel>
        <relationshipName>SupplyReceipts</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Product__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Product</label>
        <referenceTo>Product2Add__c</referenceTo>
        <relationshipLabel>Supply Receipts</relationshipLabel>
        <relationshipName>ReceiptsProduct</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Purge__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Purge</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>ReceiptDate__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Receipt Date</label>
        <required>true</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>ReceivedQuantity__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Received Quantity</label>
        <precision>12</precision>
        <required>true</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>StockUM__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Unit Of Measure</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Case</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Dozen</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Each</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>SupplyOrder__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Supply Order</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>SupplyOrder__c.ProductInventory__c</field>
                <operation>equals</operation>
                <valueField>$Source.ProductInventory__c</valueField>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>SupplyOrder__c</referenceTo>
        <relationshipLabel>Supply Receipts</relationshipLabel>
        <relationshipName>R00N40000001aezWEAQ</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>UnitCost__c</fullName>
        <defaultValue>0</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Unit Cost</label>
        <precision>17</precision>
        <required>false</required>
        <scale>4</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>UnitPrice__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Unit Price</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Warehouse__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Warehouse</label>
        <referenceTo>Warehouse__c</referenceTo>
        <relationshipLabel>Supply Receipts</relationshipLabel>
        <relationshipName>ReceiptsWarehouse</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <label>Supply Receipt</label>
    <nameField>
        <displayFormat>RCP-{YYYYMMDD}-{000000}</displayFormat>
        <label>Supply Receipt</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Supply Receipts</pluralLabel>
    <searchLayouts>
        <lookupDialogsAdditionalFields>SupplyOrder__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>ProductInventory__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Warehouse__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Product__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>ReceiptDate__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>ReceivedQuantity__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>StockUM__c</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>SupplyOrder__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>ProductInventory__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Warehouse__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Product__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>ReceiptDate__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>ReceivedQuantity__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>StockUM__c</lookupPhoneDialogsAdditionalFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>ProductInventoryCheck</fullName>
        <active>true</active>
        <errorConditionFormula>ProductInventory__c &lt;&gt;  SupplyOrder__r.ProductInventory__c</errorConditionFormula>
        <errorDisplayField>ProductInventory__c</errorDisplayField>
        <errorMessage>Product Inventory does not match with Supply Order&apos;s Product Inventory</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>ReceiptQuantityChangeCheck</fullName>
        <active>true</active>
        <errorConditionFormula>ISCHANGED( ReceivedQuantity__c )</errorConditionFormula>
        <errorDisplayField>ReceivedQuantity__c</errorDisplayField>
        <errorMessage>Receipt Quantity cannot be modified.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>ReceivedQuantityCheck</fullName>
        <active>true</active>
        <description>Received Quantity must be &gt; zero</description>
        <errorConditionFormula>ReceivedQuantity__c  &lt;= 0</errorConditionFormula>
        <errorDisplayField>ReceivedQuantity__c</errorDisplayField>
        <errorMessage>Received Quantity must be greater  zero</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>ValidateNonStockProduct</fullName>
        <active>true</active>
        <errorConditionFormula>Product__r.NonStock__c</errorConditionFormula>
        <errorMessage>Cannot create supply receipt for non-stock product.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>checkReceiptDate</fullName>
        <active>true</active>
        <errorConditionFormula>ReceiptDate__c &gt;  TODAY()</errorConditionFormula>
        <errorDisplayField>ReceiptDate__c</errorDisplayField>
        <errorMessage>Receipt Date cannot be greater than today</errorMessage>
    </validationRules>
    <webLinks>
        <fullName>HelpforthisPage</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <encodingKey>UTF-8</encodingKey>
        <hasMenubar>false</hasMenubar>
        <hasScrollbars>true</hasScrollbars>
        <hasToolbar>false</hasToolbar>
        <height>600</height>
        <isResizable>true</isResizable>
        <linkType>url</linkType>
        <masterLabel>Help for this Page</masterLabel>
        <openType>newWindow</openType>
        <position>none</position>
        <protected>false</protected>
        <showsLocation>false</showsLocation>
        <showsStatus>false</showsStatus>
        <url>{!URLFOR($Site.Prefix + &quot;/apex/gii__Help_SupplyReceipt&quot;)}</url>
    </webLinks>
    <webLinks>
        <fullName>ProductSerials</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <linkType>page</linkType>
        <masterLabel>Product Serials</masterLabel>
        <openType>replace</openType>
        <page>SupplyReceiptProductSerials</page>
        <protected>false</protected>
    </webLinks>
    <webLinks>
        <fullName>Product_Lot</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <linkType>page</linkType>
        <masterLabel>Product Lot</masterLabel>
        <openType>replace</openType>
        <page>SupplyReceiptLot</page>
        <protected>false</protected>
    </webLinks>
</CustomObject>
