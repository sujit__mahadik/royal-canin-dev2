<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>ProductRequisitionCompact</compactLayoutAssignment>
    <compactLayouts>
        <fullName>ProductRequisitionCompact</fullName>
        <fields>Name</fields>
        <fields>Product__c</fields>
        <fields>ServiceTicket__c</fields>
        <fields>ServiceTicketLine__c</fields>
        <fields>ServiceTicketEstimatedProduct__c</fields>
        <fields>Quantity__c</fields>
        <fields>ParentProduct__c</fields>
        <fields>Account__c</fields>
        <fields>RequiredDate__c</fields>
        <fields>RequestDate__c</fields>
        <label>Product Requisition Compact</label>
    </compactLayouts>
    <customHelpPage>Help_ProductRequisition</customHelpPage>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>Product Requisitions</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Account__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Product Requisitions</relationshipLabel>
        <relationshipName>ProductRequisitions</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Case__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Case</label>
        <referenceTo>Case</referenceTo>
        <relationshipLabel>Product Requisitions</relationshipLabel>
        <relationshipName>ProductRequisitions</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ClosedDate__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Closed Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>FieldServiceEngineer__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Field Service Engineer</label>
        <referenceTo>FieldServiceEngineer__c</referenceTo>
        <relationshipLabel>Product Requisitions</relationshipLabel>
        <relationshipName>ProductRequisitions</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>FromWarehouse__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>From Warehouse</label>
        <referenceTo>Warehouse__c</referenceTo>
        <relationshipLabel>Product Requisitions</relationshipLabel>
        <relationshipName>FromWarehouseProductRequisitions</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>IssueDate__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Issue Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>IssueQuantity__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Issue Quantity</label>
        <precision>12</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>OriginalProduct__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Original Product</label>
        <referenceTo>Product2Add__c</referenceTo>
        <relationshipLabel>Product Requisitions (Original Product)</relationshipLabel>
        <relationshipName>OriginalProductProductRequisitions</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ParentProductSerial__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Parent Product Serial</label>
        <referenceTo>ProductSerial__c</referenceTo>
        <relationshipLabel>Product Requisitions (Parent Product Serial)</relationshipLabel>
        <relationshipName>ParentProductSerialProductRequisitions</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ParentProduct__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Parent Product</label>
        <referenceTo>Product2Add__c</referenceTo>
        <relationshipLabel>Product Requisitions (Parent Product)</relationshipLabel>
        <relationshipName>ParentProductProductRequisitions</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Product__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Product</label>
        <referenceTo>Product2Add__c</referenceTo>
        <relationshipLabel>Product Requisitions</relationshipLabel>
        <relationshipName>ProductRequisitions</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Quantity__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Quantity</label>
        <precision>12</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>RepairableProduct__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Repairable Product</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>RepairableReturnProduct__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Repairable Return Product</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>RequestDate__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Request Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>RequiredDate__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Required Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>ReturnDate__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Return Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>ReturnProduct__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Return Product</label>
        <referenceTo>Product2Add__c</referenceTo>
        <relationshipLabel>Product Requisitions (Return Product)</relationshipLabel>
        <relationshipName>ReturnProductProductRequisitions</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ReturnQuantity__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Return Quantity</label>
        <precision>12</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ServiceTicketEstimatedProduct__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Service Ticket Estimated Product</label>
        <referenceTo>ServiceTicketEstimatedProduct__c</referenceTo>
        <relationshipLabel>Product Requisitions</relationshipLabel>
        <relationshipName>ProductRequisitions</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ServiceTicketLine__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Service Ticket Line</label>
        <referenceTo>ServiceTicketLine__c</referenceTo>
        <relationshipLabel>Product Requisitions</relationshipLabel>
        <relationshipName>ProductRequisitions</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ServiceTicket__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Service Ticket</label>
        <referenceTo>ServiceTicket__c</referenceTo>
        <relationshipLabel>Product Requisitions</relationshipLabel>
        <relationshipName>ProductRequisitions</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ShipToCity__c</fullName>
        <deprecated>false</deprecated>
        <description>Ship To City</description>
        <externalId>false</externalId>
        <label>Ship To City</label>
        <length>80</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ShipToCountry__c</fullName>
        <deprecated>false</deprecated>
        <description>Ship To Country</description>
        <externalId>false</externalId>
        <label>Ship To Country</label>
        <length>80</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ShipToName__c</fullName>
        <deprecated>false</deprecated>
        <description>Ship To Name</description>
        <externalId>false</externalId>
        <label>Ship To Name</label>
        <length>80</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ShipToState__c</fullName>
        <deprecated>false</deprecated>
        <description>Ship To State</description>
        <externalId>false</externalId>
        <label>Ship To State</label>
        <length>80</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ShipToStreet__c</fullName>
        <deprecated>false</deprecated>
        <description>Ship To Street</description>
        <externalId>false</externalId>
        <label>Ship To Street</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>ShipToZipPostalCode__c</fullName>
        <deprecated>false</deprecated>
        <description>Ship To Zip Postal Code</description>
        <externalId>false</externalId>
        <label>Ship To Zip Postal Code</label>
        <length>80</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Site__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Site</label>
        <referenceTo>Site__c</referenceTo>
        <relationshipLabel>Product Requisitions</relationshipLabel>
        <relationshipName>ProductRequisitions</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ToWarehouse__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>To Warehouse</label>
        <referenceTo>Warehouse__c</referenceTo>
        <relationshipLabel>Product Requisitions (To Warehouse)</relationshipLabel>
        <relationshipName>ToWarehouseProductRequisitions</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <label>Product Requisition</label>
    <listViews>
        <fullName>All1</fullName>
        <columns>NAME</columns>
        <columns>Product__c</columns>
        <columns>FromWarehouse__c</columns>
        <columns>Account__c</columns>
        <columns>Quantity__c</columns>
        <columns>IssueQuantity__c</columns>
        <columns>ReturnQuantity__c</columns>
        <columns>RequestDate__c</columns>
        <columns>RequiredDate__c</columns>
        <columns>IssueDate__c</columns>
        <columns>ClosedDate__c</columns>
        <columns>ReturnDate__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
        <language>en_US</language>
    </listViews>
    <nameField>
        <displayFormat>PR-{YYYYMMDD}-{000000}</displayFormat>
        <label>Product Requisition</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Product Requisitions</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Account__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Product__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>IssueQuantity__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>IssueDate__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>RequestDate__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>RequiredDate__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>ReturnDate__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>ClosedDate__c</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>Account__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Case__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Product__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>IssueQuantity__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>IssueDate__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>RequestDate__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>RequiredDate__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>ReturnDate__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>ClosedDate__c</lookupDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchFilterFields>Account__c</searchFilterFields>
        <searchFilterFields>Case__c</searchFilterFields>
        <searchFilterFields>Product__c</searchFilterFields>
        <searchResultsAdditionalFields>Account__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Product__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Site__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>FieldServiceEngineer__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
    <validationRules>
        <fullName>ValidateProductReference</fullName>
        <active>true</active>
        <description>Validate Product Reference is not null</description>
        <errorConditionFormula>Product__c = null</errorConditionFormula>
        <errorDisplayField>Product__c</errorDisplayField>
        <errorMessage>Product is required.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>ValidateQuantity</fullName>
        <active>true</active>
        <description>Product Requisition Quantity must be greater than 0.</description>
        <errorConditionFormula>Quantity__c &lt;= 0</errorConditionFormula>
        <errorDisplayField>Quantity__c</errorDisplayField>
        <errorMessage>Product Requisition Quantity must be greater than 0.</errorMessage>
    </validationRules>
    <webLinks>
        <fullName>HelpforthisPage</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <encodingKey>UTF-8</encodingKey>
        <hasMenubar>false</hasMenubar>
        <hasScrollbars>true</hasScrollbars>
        <hasToolbar>false</hasToolbar>
        <height>600</height>
        <isResizable>true</isResizable>
        <linkType>url</linkType>
        <masterLabel>Help for this Page</masterLabel>
        <openType>newWindow</openType>
        <position>none</position>
        <protected>false</protected>
        <showsLocation>false</showsLocation>
        <showsStatus>false</showsStatus>
        <url>{!URLFOR($Site.Prefix + &quot;/apex/gii__Help_ProductRequisition&quot;)}</url>
    </webLinks>
    <webLinks>
        <fullName>InventoryInquiry</fullName>
        <availability>online</availability>
        <description>Inventory Inquiry</description>
        <displayType>button</displayType>
        <linkType>page</linkType>
        <masterLabel>Inventory Inquiry</masterLabel>
        <openType>replace</openType>
        <page>ProductRequisitionInventoryInquiry</page>
        <protected>false</protected>
    </webLinks>
</CustomObject>
