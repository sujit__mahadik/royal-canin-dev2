<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>InvoiceScheduleTemplateDetailCompact</compactLayoutAssignment>
    <compactLayouts>
        <fullName>InvoiceScheduleTemplateDetailCompact</fullName>
        <fields>Name</fields>
        <fields>TemplateName__c</fields>
        <fields>TemplateScheduleAmount__c</fields>
        <fields>TemplateSchedulePercentage__c</fields>
        <fields>Reference__c</fields>
        <label>Invoice Schedule Template Detail Compact</label>
    </compactLayouts>
    <customHelpPage>Help_InvoiceScheduleTemplateDetail</customHelpPage>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>Invoice Schedule Template Detail</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Reference__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Reference</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Trial</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Other</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>TemplateName__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Template Name</label>
        <referenceTo>InvoiceScheduleTemplate__c</referenceTo>
        <relationshipLabel>Invoice Schedule Template Details</relationshipLabel>
        <relationshipName>Invoice_Schedule_Template_Details</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>TemplateScheduleAmount__c</fullName>
        <defaultValue>0.00</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Schedule Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>TemplateSchedulePercentage__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Schedule Percentage</label>
        <precision>5</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <label>Invoice Schedule Template Detail</label>
    <nameField>
        <displayFormat>{000000}</displayFormat>
        <label>Sequence</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Invoice Schedule Template Details</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>TemplateName__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>TemplateScheduleAmount__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>TemplateSchedulePercentage__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Reference__c</customTabListAdditionalFields>
        <excludedStandardButtons>ChangeOwner</excludedStandardButtons>
        <lookupDialogsAdditionalFields>TemplateName__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>TemplateSchedulePercentage__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>TemplateScheduleAmount__c</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>TemplateName__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>TemplateSchedulePercentage__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>TemplateScheduleAmount__c</lookupPhoneDialogsAdditionalFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>NegativeAmountCheck</fullName>
        <active>true</active>
        <description>Schedule amount cannot be negative</description>
        <errorConditionFormula>TemplateScheduleAmount__c  &lt; 0</errorConditionFormula>
        <errorDisplayField>TemplateScheduleAmount__c</errorDisplayField>
        <errorMessage>Schedule Amount cannot be less than zero.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>NegativePercentageCheck</fullName>
        <active>true</active>
        <description>Schedule Percentage cannot be negative</description>
        <errorConditionFormula>TemplateSchedulePercentage__c  &lt;  0</errorConditionFormula>
        <errorDisplayField>TemplateSchedulePercentage__c</errorDisplayField>
        <errorMessage>Schedule Percentage cannot be less than zero.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>TemplateTypePercentageValueCheck</fullName>
        <active>true</active>
        <description>If Template Type Revenue - Only Schedule Percentage  to be entered.</description>
        <errorConditionFormula>IF (AND (ISPICKVAL(TemplateName__r.ScheduleTemplateType__c ,&quot;Percentage&quot;),
     TemplateScheduleAmount__c   !=  0),True,False)</errorConditionFormula>
        <errorMessage>Only Schedule Percentage is to be enterred as Schedule Template Type is Percentage</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>TemplateTypeRevenueValueCheck</fullName>
        <active>true</active>
        <description>If Template Type Revenue - Only Schedule Amount to be entered.</description>
        <errorConditionFormula>IF (AND (ISPICKVAL(TemplateName__r.ScheduleTemplateType__c ,&quot;Revenue&quot;),
       TemplateSchedulePercentage__c  &gt;  0),True,False)</errorConditionFormula>
        <errorMessage>Only Schedule Amount to be entered as Template Type is Revenue.</errorMessage>
    </validationRules>
    <webLinks>
        <fullName>HelpforthisPage</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <encodingKey>UTF-8</encodingKey>
        <hasMenubar>false</hasMenubar>
        <hasScrollbars>true</hasScrollbars>
        <hasToolbar>false</hasToolbar>
        <height>600</height>
        <isResizable>true</isResizable>
        <linkType>url</linkType>
        <masterLabel>Help for this Page</masterLabel>
        <openType>newWindow</openType>
        <position>none</position>
        <protected>false</protected>
        <showsLocation>false</showsLocation>
        <showsStatus>false</showsStatus>
        <url>{!URLFOR($Site.Prefix + &quot;/apex/gii__Help_InvoiceScheduleTemplateDetail&quot;)}</url>
    </webLinks>
</CustomObject>
