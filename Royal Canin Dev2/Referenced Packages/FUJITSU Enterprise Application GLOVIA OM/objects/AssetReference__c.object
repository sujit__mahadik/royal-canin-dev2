<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>AssetReferenceCompact</compactLayoutAssignment>
    <compactLayouts>
        <fullName>AssetReferenceCompact</fullName>
        <fields>Name</fields>
        <fields>Account__c</fields>
        <fields>Site__c</fields>
        <fields>ProductReference__c</fields>
        <fields>Quantity__c</fields>
        <fields>ServiceContract__c</fields>
        <fields>ServiceContractLine__c</fields>
        <fields>ServiceType__c</fields>
        <fields>ProductSerial__c</fields>
        <fields>Status__c</fields>
        <label>Asset Reference Compact</label>
    </compactLayouts>
    <customHelpPage>Help_AssetReference</customHelpPage>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>Asset Reference Info.</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Account__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Asset References</relationshipLabel>
        <relationshipName>AssetAccount</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>AssetCreatedfromShipment__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Internal field. Its value is set when shipment creates records in standard asset object and relates them to asset references.</description>
        <externalId>false</externalId>
        <label>Asset Created from Shipment</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>AssetDisposition__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Asset Disposition</label>
        <referenceTo>AssetDisposition__c</referenceTo>
        <relationshipLabel>Asset References</relationshipLabel>
        <relationshipName>AssetReferences</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>AssetId__c</fullName>
        <deprecated>false</deprecated>
        <description>Asset Identification</description>
        <externalId>false</externalId>
        <label>Asset Id</label>
        <length>80</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>AssetName__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Asset Name</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Asset__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Asset</label>
        <referenceTo>Asset</referenceTo>
        <relationshipLabel>Asset References</relationshipLabel>
        <relationshipName>AssetReferences</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>CompetitorProduct__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Competitor Product</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Contact__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Contact</label>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>Asset References</relationshipLabel>
        <relationshipName>AssetContact</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Contract__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Contract</label>
        <referenceTo>Contract</referenceTo>
        <relationshipLabel>Asset References</relationshipLabel>
        <relationshipName>Asset_References</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Description__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Description</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>InstallDate__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Install Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>InventoryReserve__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Inventory Reserve</label>
        <referenceTo>InventoryReserve__c</referenceTo>
        <relationshipLabel>Asset References</relationshipLabel>
        <relationshipName>AssetInventoryReserve</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Price__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Price</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>ProductId__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>ProductId</label>
        <length>80</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ProductReference__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Product</label>
        <referenceTo>Product2Add__c</referenceTo>
        <relationshipLabel>Asset</relationshipLabel>
        <relationshipName>AssetProduct</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ProductSerial__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Product Serial</label>
        <referenceTo>ProductSerial__c</referenceTo>
        <relationshipLabel>Asset References</relationshipLabel>
        <relationshipName>Asset_References</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>PurchaseDate__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Purchase Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Purge__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Purge</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Quantity__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Quantity</label>
        <precision>12</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SalesOrderLine__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Sales Order Line</label>
        <referenceTo>SalesOrderLine__c</referenceTo>
        <relationshipLabel>Asset References</relationshipLabel>
        <relationshipName>R00N40000001raTQEAY</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SalesOrderReference__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Sales Order Reference</label>
        <length>80</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SalesOrder__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Sales Order</label>
        <referenceTo>SalesOrder__c</referenceTo>
        <relationshipLabel>Asset References</relationshipLabel>
        <relationshipName>R00N40000001raUyEAI</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>SerialNumber__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Serial Number</label>
        <length>80</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ServiceContractLine__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Field Service Contract Line</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>ServiceContractLine__c.ServiceContract__c</field>
                <operation>equals</operation>
                <valueField>$Source.ServiceContract__c</valueField>
            </filterItems>
            <isOptional>true</isOptional>
        </lookupFilter>
        <referenceTo>ServiceContractLine__c</referenceTo>
        <relationshipLabel>Asset References</relationshipLabel>
        <relationshipName>Asset_References</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ServiceContract__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Field Service Contract</label>
        <referenceTo>ServiceContract__c</referenceTo>
        <relationshipLabel>Asset References</relationshipLabel>
        <relationshipName>Asset_References</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ServiceOrderLine__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>This field is Obsolete</description>
        <externalId>false</externalId>
        <label>Service Order Line</label>
        <referenceTo>ServiceOrderLine__c</referenceTo>
        <relationshipLabel>Asset References</relationshipLabel>
        <relationshipName>Asset_References</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ServiceType__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>ServiceContractLine__r.ServiceType__r.ServiceTypeName__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Service Type</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ShipmentDetail__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>This field is OBSOLETE</description>
        <externalId>false</externalId>
        <label>Shipment Detail</label>
        <referenceTo>ShipmentDetail__c</referenceTo>
        <relationshipLabel>Asset References</relationshipLabel>
        <relationshipName>Asset_References</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Shipment__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Shipment</label>
        <referenceTo>Shipment__c</referenceTo>
        <relationshipLabel>Asset References</relationshipLabel>
        <relationshipName>R00N40000001raV3EAI</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Site__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Site</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>Site__c.Account__c</field>
                <operation>equals</operation>
                <valueField>$Source.Account__c</valueField>
            </filterItems>
            <isOptional>true</isOptional>
        </lookupFilter>
        <referenceTo>Site__c</referenceTo>
        <relationshipLabel>Asset References</relationshipLabel>
        <relationshipName>AssetReferences</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Status__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Status</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Shipped</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Installed</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Registered</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Obsolete</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Purchased</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>UsageEndDate__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Usage End Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <label>Asset Reference</label>
    <nameField>
        <label>Asset Reference Name</label>
        <trackHistory>false</trackHistory>
        <type>Text</type>
    </nameField>
    <pluralLabel>Asset References</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Account__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Contract__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>ProductReference__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>ProductSerial__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>SalesOrder__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>SalesOrderLine__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Quantity__c</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>ProductSerial__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>ProductReference__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Account__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Site__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>ServiceContract__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>ServiceContractLine__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>ServiceType__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>InstallDate__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Status__c</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Account__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>AssetName__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Contract__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>ProductReference__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>ProductSerial__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>SalesOrder__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>SalesOrderLine__c</lookupPhoneDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchFilterFields>Account__c</searchFilterFields>
        <searchFilterFields>AssetName__c</searchFilterFields>
        <searchFilterFields>Contract__c</searchFilterFields>
        <searchFilterFields>ProductReference__c</searchFilterFields>
        <searchFilterFields>ProductSerial__c</searchFilterFields>
        <searchFilterFields>SalesOrder__c</searchFilterFields>
        <searchFilterFields>SalesOrderLine__c</searchFilterFields>
        <searchResultsAdditionalFields>Account__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Contract__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>ProductReference__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>ProductSerial__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>SalesOrder__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>SalesOrderLine__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Quantity__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
    <webLinks>
        <fullName>HelpforthisPage</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <encodingKey>UTF-8</encodingKey>
        <hasMenubar>false</hasMenubar>
        <hasScrollbars>true</hasScrollbars>
        <hasToolbar>false</hasToolbar>
        <height>600</height>
        <isResizable>true</isResizable>
        <linkType>url</linkType>
        <masterLabel>Help for this Page</masterLabel>
        <openType>newWindow</openType>
        <position>none</position>
        <protected>false</protected>
        <showsLocation>false</showsLocation>
        <showsStatus>false</showsStatus>
        <url>{!URLFOR($Site.Prefix + &quot;/apex/gii__Help_AssetReference&quot;)}</url>
    </webLinks>
</CustomObject>
