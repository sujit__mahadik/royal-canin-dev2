<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>ProductLotCompact</compactLayoutAssignment>
    <compactLayouts>
        <fullName>ProductLotCompact</fullName>
        <fields>Name</fields>
        <fields>Product__c</fields>
        <fields>Description__c</fields>
        <fields>LotDate__c</fields>
        <fields>LotExpirationDate__c</fields>
        <fields>Restricted__c</fields>
        <label>Product Lot Compact</label>
    </compactLayouts>
    <customHelpPage>Help_ProductLot</customHelpPage>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>Product Lot</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Description__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Description</label>
        <length>80</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>LotDate__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Lot Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>LotExpirationDate__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Lot Expiration Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Product__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Product Reference</label>
        <referenceTo>Product2Add__c</referenceTo>
        <relationshipLabel>Product Lots</relationshipLabel>
        <relationshipName>Product_Lots</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Restricted__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Restricted</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <label>Product Lot</label>
    <nameField>
        <label>Product Lot Name</label>
        <trackHistory>false</trackHistory>
        <type>Text</type>
    </nameField>
    <pluralLabel>Product Lots</pluralLabel>
    <searchLayouts>
        <lookupDialogsAdditionalFields>Product__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Description__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>LotDate__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>LotExpirationDate__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Restricted__c</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Description__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Product__c</lookupPhoneDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchFilterFields>Description__c</searchFilterFields>
        <searchFilterFields>Product__c</searchFilterFields>
        <searchFilterFields>LotDate__c</searchFilterFields>
        <searchFilterFields>LotExpirationDate__c</searchFilterFields>
        <searchFilterFields>Restricted__c</searchFilterFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
    <validationRules>
        <fullName>ServiceProductCheck</fullName>
        <active>true</active>
        <description>Only Non-Service Product allowed</description>
        <errorConditionFormula>Product__r.Service__c = true</errorConditionFormula>
        <errorMessage>Only Non-Service Product allowed</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>ThisProductIsNotLotcontrolled</fullName>
        <active>true</active>
        <description>Only Non-Service Product allowed</description>
        <errorConditionFormula>Product__r.LotControlled__c = false</errorConditionFormula>
        <errorMessage>This product is not lot controlled.</errorMessage>
    </validationRules>
    <webLinks>
        <fullName>HelpforthisPage</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <encodingKey>UTF-8</encodingKey>
        <hasMenubar>false</hasMenubar>
        <hasScrollbars>true</hasScrollbars>
        <hasToolbar>false</hasToolbar>
        <height>600</height>
        <isResizable>true</isResizable>
        <linkType>url</linkType>
        <masterLabel>Help for this Page</masterLabel>
        <openType>newWindow</openType>
        <position>none</position>
        <protected>false</protected>
        <showsLocation>false</showsLocation>
        <showsStatus>false</showsStatus>
        <url>{!URLFOR($Site.Prefix + &quot;/apex/gii__Help_ProductLot&quot;)}</url>
    </webLinks>
</CustomObject>
