<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>OrderValueDiscountCompact</compactLayoutAssignment>
    <compactLayouts>
        <fullName>OrderValueDiscountCompact</fullName>
        <fields>Name</fields>
        <fields>PriceBook__c</fields>
        <fields>Tier1DiscountPercent__c</fields>
        <fields>Tier2DiscountPercent__c</fields>
        <fields>Tier3DiscountPercent__c</fields>
        <fields>Tier4DiscountPercent__c</fields>
        <fields>Tier5DiscountPercent__c</fields>
        <label>Order Value Discount Compact</label>
    </compactLayouts>
    <customHelpPage>Help_OrderValueDiscount</customHelpPage>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>Discount Percentage based on Order Value for a PriceBook</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>PriceBook__c</fullName>
        <deprecated>false</deprecated>
        <description>Name of the PriceBook</description>
        <externalId>false</externalId>
        <label>Price Book</label>
        <referenceTo>PriceBook__c</referenceTo>
        <relationshipLabel>Order Value Discounts</relationshipLabel>
        <relationshipName>Order_Value_Discounts</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Tier1DiscountPercent__c</fullName>
        <deprecated>false</deprecated>
        <description>Tier 1 Discount Percentage</description>
        <externalId>false</externalId>
        <label>Tier 1 Discount Percent</label>
        <precision>5</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Tier1OrderAmount__c</fullName>
        <deprecated>false</deprecated>
        <description>Tier 1 Order Amount</description>
        <externalId>false</externalId>
        <label>Tier 1 Order Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Tier2DiscountPercent__c</fullName>
        <deprecated>false</deprecated>
        <description>Tier 2 Discount Percentage</description>
        <externalId>false</externalId>
        <label>Tier 2 Discount Percent</label>
        <precision>5</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Tier2OrderAmount__c</fullName>
        <deprecated>false</deprecated>
        <description>Tier 2 Order Amount</description>
        <externalId>false</externalId>
        <label>Tier 2 Order Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Tier3DiscountPercent__c</fullName>
        <deprecated>false</deprecated>
        <description>Tier 3 Discount Percentage</description>
        <externalId>false</externalId>
        <label>Tier 3 Discount Percent</label>
        <precision>5</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Tier3OrderAmount__c</fullName>
        <deprecated>false</deprecated>
        <description>Tier 3 Order Amount</description>
        <externalId>false</externalId>
        <label>Tier 3 Order Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Tier4DiscountPercent__c</fullName>
        <deprecated>false</deprecated>
        <description>Tier 4 Discount Percentage</description>
        <externalId>false</externalId>
        <label>Tier 4 Discount Percent</label>
        <precision>5</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Tier4OrderAmount__c</fullName>
        <deprecated>false</deprecated>
        <description>Tier 4 Order Amount</description>
        <externalId>false</externalId>
        <label>Tier 4 Order Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Tier5DiscountPercent__c</fullName>
        <deprecated>false</deprecated>
        <description>Tier 5 Discount Percentage</description>
        <externalId>false</externalId>
        <label>Tier 5 Discount Percent</label>
        <precision>5</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Tier5OrderAmount__c</fullName>
        <deprecated>false</deprecated>
        <description>Tier 5 Order Amount</description>
        <externalId>false</externalId>
        <label>Tier 5 Order Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Tier6DiscountPercent__c</fullName>
        <deprecated>false</deprecated>
        <description>Tier 6 Discount Percentage</description>
        <externalId>false</externalId>
        <label>Tier 6 Discount Percent</label>
        <precision>5</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Tier6OrderAmount__c</fullName>
        <deprecated>false</deprecated>
        <description>Tier 6 Order Amount</description>
        <externalId>false</externalId>
        <label>Tier 6 Order Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>UniquePriceBookId__c</fullName>
        <caseSensitive>false</caseSensitive>
        <deprecated>false</deprecated>
        <description>Unique Price Book Id + Currency ISO Code (mulit-currency only) to have a single record in Order Value Discount</description>
        <externalId>false</externalId>
        <label>UniquePriceBookId</label>
        <length>80</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <label>Order Value Discount</label>
    <nameField>
        <displayFormat>{000000}</displayFormat>
        <label>Order Value Discount Name</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Order Value Discounts</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <webLinks>
        <fullName>HelpforthisPage</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <encodingKey>UTF-8</encodingKey>
        <hasMenubar>false</hasMenubar>
        <hasScrollbars>true</hasScrollbars>
        <hasToolbar>false</hasToolbar>
        <height>600</height>
        <isResizable>true</isResizable>
        <linkType>url</linkType>
        <masterLabel>Help for this Page</masterLabel>
        <openType>newWindow</openType>
        <position>none</position>
        <protected>false</protected>
        <showsLocation>false</showsLocation>
        <showsStatus>false</showsStatus>
        <url>{!URLFOR($Site.Prefix + &quot;/apex/gii__Help_OrderValueDiscount&quot;)}</url>
    </webLinks>
</CustomObject>
