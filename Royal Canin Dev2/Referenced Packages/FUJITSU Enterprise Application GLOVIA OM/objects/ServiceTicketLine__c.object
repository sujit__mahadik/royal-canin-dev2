<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>ServiceTicketLineCompact</compactLayoutAssignment>
    <compactLayouts>
        <fullName>ServiceTicketLineCompact</fullName>
        <fields>Name</fields>
        <fields>ServiceTicket__c</fields>
        <fields>AssetReference__c</fields>
        <fields>Product__c</fields>
        <fields>Quantity__c</fields>
        <fields>ServiceDueDate__c</fields>
        <fields>FieldServiceEngineer__c</fields>
        <fields>AppointmentSet__c</fields>
        <fields>DateTimeClosed__c</fields>
        <fields>ClosedOnTime__c</fields>
        <label>Service Ticket Line Compact</label>
    </compactLayouts>
    <customHelpPage>Help_ServiceTicketLine</customHelpPage>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>Service Ticket Lines</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>AppointmentSet__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Appointment Set</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>AssetReference__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Asset Reference</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>AssetReference__c.Site__c</field>
                <operation>equals</operation>
                <valueField>$Source.Site__c</valueField>
            </filterItems>
            <isOptional>true</isOptional>
        </lookupFilter>
        <referenceTo>AssetReference__c</referenceTo>
        <relationshipLabel>Service Ticket Lines</relationshipLabel>
        <relationshipName>ServiceTicketLines</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Certification__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Certification</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Maintenance Management Professional</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Business Communication Certificate</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Technical Communication</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Project Management Professional</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
        <visibleLines>4</visibleLines>
    </fields>
    <fields>
        <fullName>ClosedOnTime__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>IF(DateValue(DateTimeClosed__c) &lt;=  ServiceDueDate__c,true,false)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Closed On Time</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>DateTimeClosed__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Date/Time Closed</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>Description__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Description</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>5</visibleLines>
    </fields>
    <fields>
        <fullName>DiagnosticsComments__c</fullName>
        <deprecated>false</deprecated>
        <description>Diagnostics comments describe the probable cause of the malfunction, as diagnosed by the field service engineer.</description>
        <externalId>false</externalId>
        <label>Diagnostics</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>5</visibleLines>
    </fields>
    <fields>
        <fullName>FieldServiceEngineer__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Field Service Engineer</label>
        <referenceTo>FieldServiceEngineer__c</referenceTo>
        <relationshipLabel>Service Ticket Lines</relationshipLabel>
        <relationshipName>ServiceTicketLines</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>FieldServiceTemplate__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Field Service Template</label>
        <referenceTo>FieldServiceTemplate__c</referenceTo>
        <relationshipLabel>Service Ticket Lines</relationshipLabel>
        <relationshipName>ServiceTicketLines</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>IsClosed__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>IF(ISNULL(DateTimeClosed__c),false,true)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Is Closed</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>ProductSerial__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>AssetReference__r.ProductSerial__r.Name</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Product Serial</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Product__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Product</label>
        <referenceTo>Product2Add__c</referenceTo>
        <relationshipLabel>Service Ticket Lines</relationshipLabel>
        <relationshipName>ServiceTicketLines</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>PromiseDate__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Promise Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Quantity__c</fullName>
        <defaultValue>1</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Quantity</label>
        <precision>12</precision>
        <required>true</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ServiceActionComments__c</fullName>
        <deprecated>false</deprecated>
        <description>Service Action Comments describe the correct actions taken by the field service engineer</description>
        <externalId>false</externalId>
        <label>Service Action</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>5</visibleLines>
    </fields>
    <fields>
        <fullName>ServiceContractLine__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Field Service Contract Line</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>ServiceContractLine__c.ServiceContract__c</field>
                <operation>equals</operation>
                <valueField>$Source.ServiceContract__c</valueField>
            </filterItems>
            <isOptional>true</isOptional>
        </lookupFilter>
        <referenceTo>ServiceContractLine__c</referenceTo>
        <relationshipLabel>Service Ticket Lines</relationshipLabel>
        <relationshipName>ServiceTicketLines</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ServiceContract__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Field Service Contract</label>
        <referenceTo>ServiceContract__c</referenceTo>
        <relationshipLabel>Service Ticket Lines</relationshipLabel>
        <relationshipName>ServiceTicketLines</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ServiceDueDate__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Service Due Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>ServiceTicket__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Service Ticket</label>
        <referenceTo>ServiceTicket__c</referenceTo>
        <relationshipLabel>Service Ticket Lines</relationshipLabel>
        <relationshipName>ServiceTicketLines</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>ServiceType__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>ServiceContractLine__r.ServiceType__r.ServiceTypeName__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Service Type</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Site__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Site</label>
        <referenceTo>Site__c</referenceTo>
        <relationshipLabel>Service Ticket Lines</relationshipLabel>
        <relationshipName>ServiceTicketLines</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>StockUM__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Unit of Measure</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Case</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Dozen</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Each</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>TotalActualAmount__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>TotalActualChargeAmount__c +  TotalActualProductAmount__c +  TotalActualServiceAmount__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Total Actual Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>TotalActualChargeAmount__c</fullName>
        <defaultValue>0</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Total Actual Charge Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>TotalActualChargeCost__c</fullName>
        <defaultValue>0</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Total Actual Charge Cost</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>TotalActualCost__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>TotalActualChargeCost__c +  TotalActualProductCost__c +  TotalActualServiceCost__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Total Actual Cost</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>TotalActualProductAmount__c</fullName>
        <defaultValue>0</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Total Actual Product Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>TotalActualProductCost__c</fullName>
        <defaultValue>0</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Total Actual Product Cost</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>TotalActualServiceAmount__c</fullName>
        <defaultValue>0</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Total Actual Service Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>TotalActualServiceCost__c</fullName>
        <defaultValue>0</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Total Actual Service Cost</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>TotalEstimatedAmount__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>TotalEstimatedChargeAmount__c + TotalEstimatedProductAmount__c  + TotalEstimatedServiceAmount__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Total Estimated Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>TotalEstimatedChargeAmount__c</fullName>
        <defaultValue>0</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Total Estimated Charge Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>TotalEstimatedChargeCost__c</fullName>
        <defaultValue>0</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Total Estimated Charge Cost</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>TotalEstimatedCost__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>TotalEstimatedChargeCost__c +  TotalEstimatedProductCost__c +  TotalEstimatedServiceCost__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Total Estimated Cost</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>TotalEstimatedProductAmount__c</fullName>
        <defaultValue>0</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Total Estimated Product Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>TotalEstimatedProductCost__c</fullName>
        <defaultValue>0</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Total Estimated Product Cost</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>TotalEstimatedServiceAmount__c</fullName>
        <defaultValue>0</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Total Estimated Service Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>TotalEstimatedServiceCost__c</fullName>
        <defaultValue>0</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Total Estimated Service Cost</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>TroubleComments__c</fullName>
        <deprecated>false</deprecated>
        <description>Trouble comments describe the trouble with the product, as reported by the customer.</description>
        <externalId>false</externalId>
        <label>Trouble</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>5</visibleLines>
    </fields>
    <label>Service Ticket Line</label>
    <nameField>
        <displayFormat>{000000}</displayFormat>
        <label>Service Ticket Line</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Service Ticket Lines</pluralLabel>
    <searchLayouts>
        <lookupDialogsAdditionalFields>ServiceTicket__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>AssetReference__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Product__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Quantity__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>ServiceDueDate__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>FieldServiceEngineer__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Description__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>ServiceContractLine__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>ServiceContract__c</lookupDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>MustInputaAssetorProduct</fullName>
        <active>true</active>
        <errorConditionFormula>AND( AssetReference__c = null ,  Product__c = NULL)</errorConditionFormula>
        <errorMessage>Asset Reference or Product must be specified for the service ticket line.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>ProductorAssetReferenceCannotbeChanged</fullName>
        <active>true</active>
        <description>Service Ticket Line&apos;s Product or Asset Referece cannot be changed.</description>
        <errorConditionFormula>OR(ISCHANGED( AssetReference__c ) ,
   ISCHANGED( Product__c) 
)</errorConditionFormula>
        <errorMessage>Service Ticket Line&apos;s Product or Asset Reference cannot be changed.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>QuantityMustBeGreaterThanZero</fullName>
        <active>true</active>
        <description>Quantity must be greater than zero.</description>
        <errorConditionFormula>Quantity__c &lt;= 0</errorConditionFormula>
        <errorDisplayField>Quantity__c</errorDisplayField>
        <errorMessage>Quantity must be greater than zero.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>ValidateAssetandProduct</fullName>
        <active>true</active>
        <description>Asset Reference does not belong to the Service Ticket Line Product.</description>
        <errorConditionFormula>AND( Product__c &lt;&gt; null,  AssetReference__c &lt;&gt; null, AssetReference__r.ProductReference__c &lt;&gt;  Product__c )</errorConditionFormula>
        <errorDisplayField>AssetReference__c</errorDisplayField>
        <errorMessage>Asset Reference does not belong to the Service Ticket Line Product.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>ValidateClosedServiceTicket</fullName>
        <active>true</active>
        <description>Service Ticket is closed and cannot add lines</description>
        <errorConditionFormula>AND( ISNEW() ,  
     NOT(
       ISNULL(ServiceTicket__r.DateTimeClosed__c)
     )
)</errorConditionFormula>
        <errorMessage>Service Ticket is already closed.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>ValidateServiceDueDate</fullName>
        <active>true</active>
        <description>Service due date is required.</description>
        <errorConditionFormula>ISNULL( ServiceDueDate__c)</errorConditionFormula>
        <errorDisplayField>ServiceDueDate__c</errorDisplayField>
        <errorMessage>Service due date is required.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>ValidateServiceTicketLineClose</fullName>
        <active>true</active>
        <errorConditionFormula>AND(  ISCHANGED( DateTimeClosed__c ) ,ISNULL(DateTimeClosed__c),
  NOT(ISNULL(ServiceTicket__r.DateTimeClosed__c ))
)</errorConditionFormula>
        <errorDisplayField>DateTimeClosed__c</errorDisplayField>
        <errorMessage>Cannot remove Date/Time Closed of service ticket line when service ticket is already closed.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>ValidateServiceTicketLineOpenCloseDate</fullName>
        <active>false</active>
        <description>Date/Time Closed cannot be less than Date / Time Opened.</description>
        <errorConditionFormula>DateTimeClosed__c &lt;  ServiceTicket__r.DateTimeOpened__c</errorConditionFormula>
        <errorDisplayField>DateTimeClosed__c</errorDisplayField>
        <errorMessage>Date/Time Closed cannot be less than service ticket&apos;s Date / Time Opened.</errorMessage>
    </validationRules>
    <webLinks>
        <fullName>AddActualCharges</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <height>600</height>
        <linkType>page</linkType>
        <masterLabel>Add Actual Charges</masterLabel>
        <openType>noSidebar</openType>
        <page>AddActualCharges</page>
        <protected>false</protected>
    </webLinks>
    <webLinks>
        <fullName>AddActualProducts</fullName>
        <availability>online</availability>
        <description>Add Actual Products</description>
        <displayType>button</displayType>
        <height>600</height>
        <linkType>page</linkType>
        <masterLabel>Add Actual Products</masterLabel>
        <openType>noSidebar</openType>
        <page>AddActualProducts</page>
        <protected>false</protected>
    </webLinks>
    <webLinks>
        <fullName>AddActualServices</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <height>600</height>
        <linkType>page</linkType>
        <masterLabel>Add Actual Services</masterLabel>
        <openType>noSidebar</openType>
        <page>AddActualServices</page>
        <protected>false</protected>
    </webLinks>
    <webLinks>
        <fullName>AssignFieldServiceEngineer</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <linkType>page</linkType>
        <masterLabel>Assign Field Service Engineer</masterLabel>
        <openType>replace</openType>
        <page>ServiceTicketLineAssignFSE</page>
        <protected>false</protected>
    </webLinks>
    <webLinks>
        <fullName>CreateProductRequisitions</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <linkType>page</linkType>
        <masterLabel>Create Product Requisitions</masterLabel>
        <openType>replace</openType>
        <page>ServiceTicketLineCreateProdReqs</page>
        <protected>false</protected>
    </webLinks>
    <webLinks>
        <fullName>CreateServiceTicketLineEstimate</fullName>
        <availability>online</availability>
        <description>Create Service Ticket Line Estimates</description>
        <displayType>button</displayType>
        <height>600</height>
        <linkType>page</linkType>
        <masterLabel>Create Service Ticket Line Estimate</masterLabel>
        <openType>noSidebar</openType>
        <page>CreateServTktEstimatefromTemplate</page>
        <protected>false</protected>
    </webLinks>
    <webLinks>
        <fullName>DirectionstoSite</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <hasMenubar>false</hasMenubar>
        <hasScrollbars>true</hasScrollbars>
        <hasToolbar>false</hasToolbar>
        <height>600</height>
        <isResizable>true</isResizable>
        <linkType>page</linkType>
        <masterLabel>Directions to Site</masterLabel>
        <openType>newWindow</openType>
        <page>ServiceTicketLineSiteMap</page>
        <position>none</position>
        <protected>false</protected>
        <showsLocation>false</showsLocation>
        <showsStatus>false</showsStatus>
    </webLinks>
    <webLinks>
        <fullName>HelpforthisPage</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <encodingKey>UTF-8</encodingKey>
        <hasMenubar>false</hasMenubar>
        <hasScrollbars>true</hasScrollbars>
        <hasToolbar>false</hasToolbar>
        <height>600</height>
        <isResizable>true</isResizable>
        <linkType>url</linkType>
        <masterLabel>Help for this Page</masterLabel>
        <openType>newWindow</openType>
        <position>none</position>
        <protected>false</protected>
        <showsLocation>false</showsLocation>
        <showsStatus>false</showsStatus>
        <url>{!URLFOR($Site.Prefix + &quot;/apex/gii__Help_ServiceTicketLine&quot;)}</url>
    </webLinks>
    <webLinks>
        <fullName>InventoryInquiry</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <linkType>page</linkType>
        <masterLabel>Inventory Inquiry</masterLabel>
        <openType>replace</openType>
        <page>ServiceTicketLineInventoryInquiry</page>
        <protected>false</protected>
    </webLinks>
    <webLinks>
        <fullName>SetAppointments</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <linkType>page</linkType>
        <masterLabel>Set Appointments</masterLabel>
        <openType>replace</openType>
        <page>ServiceTicketLineSetAppointment</page>
        <protected>false</protected>
    </webLinks>
</CustomObject>
