<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>FieldServiceTemplateServiceCompact</compactLayoutAssignment>
    <compactLayouts>
        <fullName>FieldServiceTemplateServiceCompact</fullName>
        <fields>Name</fields>
        <fields>FieldServiceTemplate__c</fields>
        <fields>ServiceProduct__c</fields>
        <fields>Quantity__c</fields>
        <label>Field Service Template Service Compact</label>
    </compactLayouts>
    <customHelpPage>Help_FieldServiceTemplateService</customHelpPage>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>Field Service Template Service Products</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>FieldServiceTemplate__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Field Service Template</label>
        <referenceTo>FieldServiceTemplate__c</referenceTo>
        <relationshipLabel>Field Service Template Services</relationshipLabel>
        <relationshipName>FieldServiceTemplateServices</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Quantity__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Quantity</label>
        <precision>12</precision>
        <required>true</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ServiceProduct__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Service Product</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>Product2Add__c.Service__c</field>
                <operation>equals</operation>
                <value>True</value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Product2Add__c</referenceTo>
        <relationshipLabel>Field Service Template Services</relationshipLabel>
        <relationshipName>FieldServiceTemplateServices</relationshipName>
        <required>true</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <label>Field Service Template Service</label>
    <nameField>
        <displayFormat>{000000}</displayFormat>
        <label>Field Service Template Service</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Field Service Template Services</pluralLabel>
    <searchLayouts>
        <lookupDialogsAdditionalFields>FieldServiceTemplate__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Quantity__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>ServiceProduct__c</lookupDialogsAdditionalFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>QuantityMustBeGreaterThanZero</fullName>
        <active>true</active>
        <description>Quantity must be greater than zero.</description>
        <errorConditionFormula>OR(Quantity__c &lt;= 0, ISNULL(Quantity__c))</errorConditionFormula>
        <errorDisplayField>Quantity__c</errorDisplayField>
        <errorMessage>Quantity must be greater than zero.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>ServiceProductCheck</fullName>
        <active>true</active>
        <description>Only Service Product allowed.</description>
        <errorConditionFormula>ServiceProduct__r.Service__c = false</errorConditionFormula>
        <errorDisplayField>ServiceProduct__c</errorDisplayField>
        <errorMessage>Only Service Product allowed.</errorMessage>
    </validationRules>
    <webLinks>
        <fullName>HelpforthisPage</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <encodingKey>UTF-8</encodingKey>
        <hasMenubar>false</hasMenubar>
        <hasScrollbars>true</hasScrollbars>
        <hasToolbar>false</hasToolbar>
        <height>600</height>
        <isResizable>true</isResizable>
        <linkType>url</linkType>
        <masterLabel>Help for this Page</masterLabel>
        <openType>newWindow</openType>
        <position>none</position>
        <protected>false</protected>
        <showsLocation>false</showsLocation>
        <showsStatus>false</showsStatus>
        <url>{!URLFOR($Site.Prefix + &quot;/apex/gii__Help_FieldServiceTemplateService&quot;)}</url>
    </webLinks>
</CustomObject>
