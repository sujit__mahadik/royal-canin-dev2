<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>ServiceOrderLineVAS2Compact</compactLayoutAssignment>
    <compactLayouts>
        <fullName>ServiceOrderLineVAS2Compact</fullName>
        <fields>Name</fields>
        <fields>ServiceOrderLine__c</fields>
        <fields>SalesOrderLine__c</fields>
        <fields>VAS2__c</fields>
        <fields>Quantity__c</fields>
        <fields>UnitPrice__c</fields>
        <fields>Extension__c</fields>
        <fields>FixedPrice__c</fields>
        <fields>FlatFee__c</fields>
        <fields>Free__c</fields>
        <label>Service Order Line VAS2 Compact</label>
    </compactLayouts>
    <customHelpPage>Help_ServiceOrderLineVAS2</customHelpPage>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>Service Order Line VAS2</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Description__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Description</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>DiscountPercent__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>DiscountPercent</label>
        <precision>5</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Extension__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>/*
IF( gii__Free__c == TRUE ,
	0, 
	IF ( gii__FlatFee__c != 0, 
		gii__Quantity__c * gii__FlatFee__c, 
		gii__Quantity__c * ROUND((gii__UnitPrice__c - ((gii__UnitPrice__c * gii__DiscountPercent__c) )),2) 
	) 
) 

*/
IF(Free__c==TRUE,
	0, 
	IF(FlatFee__c!=0, 
		IF( RoundAmountstoZeroDecimal__c , 
			IF( RoundtoZeroDecimalwithRoundOff__c , 
				FLOOR(Quantity__c * FlatFee__c) , 
				ROUND(Quantity__c * FlatFee__c, 0) 
			), 
			Quantity__c * FlatFee__c  
		), 
		IF( RoundAmountstoZeroDecimal__c , 
			IF( RoundtoZeroDecimalwithRoundOff__c , 
				FLOOR(Quantity__c * ROUND((UnitPrice__c -((UnitPrice__c * DiscountPercent__c))),2)) , 
				ROUND(Quantity__c * (UnitPrice__c -((UnitPrice__c * DiscountPercent__c))),0) 
			), 
			ROUND(Quantity__c*(UnitPrice__c-((UnitPrice__c*DiscountPercent__c))),2) 
		) 
	) 
)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Extension</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>FixedPrice__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Fixed Price</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>FlatFee__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Flat Fee</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Free__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Free</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Instructions__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Instructions</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>10</visibleLines>
    </fields>
    <fields>
        <fullName>PricingOverriddenReason__c</fullName>
        <deprecated>false</deprecated>
        <description>Reason for Pricing Discount Overridding. If set to None then Promotional discounts are refreshed.</description>
        <externalId>false</externalId>
        <label>Pricing Overridden Reason</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Bundled Deals</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Seasonal Sales</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Processed__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>System Field.</description>
        <externalId>false</externalId>
        <label>Processed</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Promotion__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Promotion</label>
        <referenceTo>Promotion__c</referenceTo>
        <relationshipLabel>Service Order Line VAS2</relationshipLabel>
        <relationshipName>ServiceOrderLineVAS2</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Quantity__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Quantity</label>
        <precision>12</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>RoundAmountstoZeroDecimal__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Round Amounts to Zero Decimal</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>RoundtoZeroDecimalwithRoundOff__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Round to Zero Decimal with Round Off</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>SalesOrderLine__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Sales Order Line</label>
        <referenceTo>SalesOrderLine__c</referenceTo>
        <relationshipLabel>Service Order Line VAS2</relationshipLabel>
        <relationshipName>ServiceOrderLineVAS2</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ServiceOrderLine__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Service Order Line</label>
        <referenceTo>ServiceOrderLine__c</referenceTo>
        <relationshipLabel>Service Order Line VAS2</relationshipLabel>
        <relationshipName>ServiceOrderLineVAS2</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>SourceId__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>SourceId</label>
        <length>80</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>UnitPrice__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Unit Price</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>VAS2__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>VAS2</label>
        <referenceTo>VAS2__c</referenceTo>
        <relationshipLabel>Service Order Line VAS2</relationshipLabel>
        <relationshipName>ServiceOrderLineVAS2</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <label>Service Order Line VAS2</label>
    <nameField>
        <displayFormat>{000000}</displayFormat>
        <label>Service Order Line VAS2</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Service Order Line VAS2</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
    <validationRules>
        <fullName>FixedPriceCheckforNegativeValue</fullName>
        <active>true</active>
        <errorConditionFormula>FixedPrice__c &lt; 0</errorConditionFormula>
        <errorDisplayField>FixedPrice__c</errorDisplayField>
        <errorMessage>Fixed Price cannot be less than zero.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>QuantityCheckforNegativeValue</fullName>
        <active>true</active>
        <errorConditionFormula>Quantity__c &lt; 0</errorConditionFormula>
        <errorDisplayField>Quantity__c</errorDisplayField>
        <errorMessage>Quantity cannot be less than zero.</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>UnitPriceCheckforNegativeValue</fullName>
        <active>true</active>
        <errorConditionFormula>UnitPrice__c &lt; 0</errorConditionFormula>
        <errorDisplayField>UnitPrice__c</errorDisplayField>
        <errorMessage>Unit Price cannot be less than zero.</errorMessage>
    </validationRules>
    <webLinks>
        <fullName>HelpforthisPage</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <encodingKey>UTF-8</encodingKey>
        <hasMenubar>false</hasMenubar>
        <hasScrollbars>true</hasScrollbars>
        <hasToolbar>false</hasToolbar>
        <height>600</height>
        <isResizable>true</isResizable>
        <linkType>url</linkType>
        <masterLabel>Help for this Page</masterLabel>
        <openType>newWindow</openType>
        <position>none</position>
        <protected>false</protected>
        <showsLocation>false</showsLocation>
        <showsStatus>false</showsStatus>
        <url>{!URLFOR($Site.Prefix + &quot;/apex/gii__Help_ServiceOrderLineVAS2&quot;)}</url>
    </webLinks>
</CustomObject>
