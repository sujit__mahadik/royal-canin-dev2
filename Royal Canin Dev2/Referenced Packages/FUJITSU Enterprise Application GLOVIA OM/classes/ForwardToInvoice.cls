/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class ForwardToInvoice {
    global ForwardToInvoice() {

    }
    global static void InvoiceSchedule(List<Id> selectedRecordIds) {

    }
    global static void SalesOrderAdditionalCharge(List<Id> selectedRecordIds) {

    }
    global static void ServiceOrderCompletion(List<Id> selectedRecordIds) {

    }
    global static void ServiceOrderLine(List<Id> selectedRecordIds) {

    }
    global static void Shipment(List<Id> selectedRecordIds) {

    }
}
