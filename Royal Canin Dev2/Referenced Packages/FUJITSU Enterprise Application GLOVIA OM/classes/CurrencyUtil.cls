/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class CurrencyUtil {
    global CurrencyUtil() {

    }
    global static Boolean UsingMultipleCurrencies() {
        return null;
    }
    global static Set<String> getActiveCurrencies() {
        return null;
    }
    global static Map<String,Decimal> getCurrencyConvertionRateMap() {
        return null;
    }
    global static String getCurrencyISOCodeValue(SObject obj) {
        return null;
    }
    global static void validateCurrencyValue(SObject currentRecord, SObject oldRecord) {

    }
}
