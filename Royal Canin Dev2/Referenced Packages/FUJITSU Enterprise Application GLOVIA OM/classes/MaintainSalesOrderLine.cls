/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class MaintainSalesOrderLine {
    global MaintainSalesOrderLine() {

    }
    global static gii.MaintainSalesOrderLine.MaintainSalesOrderLineResult UpdateSalesOrderLines(List<gii.MaintainSalesOrderLine.SalesOrderLine> listSalesOrderLineClassObj) {
        return null;
    }
    global static List<gii.MaintainSalesOrderLine.SalesOrderLine> validateSalesOrderLines(List<gii.MaintainSalesOrderLine.SalesOrderLine> listSalesOrderLineClassObj) {
        return null;
    }
global class GOMException extends Exception {
}
global class MaintainSalesOrderLineResult {
    global List<gii.MaintainSalesOrderLine.GOMException> Exceptions {
        get;
        set;
    }
    global List<gii.MaintainSalesOrderLine.SalesOrderLine> listSalesOrderLine {
        get;
        set;
    }
    global MaintainSalesOrderLineResult() {

    }
}
global class SalesOrderLine {
    global List<gii.MaintainSalesOrderLine.GOMException> Errors {
        get;
        set;
    }
    global Double NewOrderQuantity {
        get;
        set;
    }
    global Date NewRequiredDate {
        get;
        set;
    }
    global Id NewWarehouseId {
        get;
        set;
    }
    global Id SalesOrderLineId {
        get;
        set;
    }
    global Boolean UnCancelSalesOrderLine {
        get;
        set;
    }
    global SalesOrderLine() {

    }
}
}
