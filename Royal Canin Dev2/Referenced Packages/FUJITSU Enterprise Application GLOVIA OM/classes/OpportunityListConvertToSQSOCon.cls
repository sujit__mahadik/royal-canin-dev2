/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class OpportunityListConvertToSQSOCon {
    global List<Opportunity> selectedOpps {
        get;
        set;
    }
    global ApexPages.StandardSetController setCon;
    global OpportunityListConvertToSQSOCon(ApexPages.StandardSetController controller) {

    }
    global Boolean getactionEnabled() {
        return null;
    }
    global Boolean getcontinueBtn() {
        return null;
    }
    global String getcurrentAction() {
        return null;
    }
    global System.PageReference listButtonAction() {
        return null;
    }
    global System.PageReference returnToPrevious() {
        return null;
    }
    global void setselectedOpps(List<Opportunity> o) {

    }
}
