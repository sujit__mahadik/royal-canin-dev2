/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class ARCreditCreation {
    global ARCreditCreation() {

    }
    global static List<gii__ARCredit__c> createARCreditRecords(List<gii.ARCreditCreation.ARCreditHeader> lstARCreditHeader) {
        return null;
    }
    global static List<gii.ARCreditCreation.ARCreditHeader> getARCreditForInvoice(Set<Id> setInvoiceIds, Date CreditDate) {
        return null;
    }
global class ARCreditAdditionalCharge {
    global Id AdditionalChargeId {
        get;
        set;
    }
    global String AGC {
        get;
        set;
    }
    global Boolean ExciseTaxable {
        get;
        set;
    }
    global Id InvoiceAdditionalChargeId {
        get;
        set;
    }
    global String NoChargeReason {
        get;
        set;
    }
    global Decimal Quantity {
        get;
        set;
    }
    global Boolean RoundAmountstoZeroDecimal {
        get;
        set;
    }
    global Boolean RoundtoZeroDecimalwithRoundOff {
        get;
        set;
    }
    global Id SalesOrderAdditionalChargeId {
        get;
        set;
    }
    global Boolean SalesTaxable {
        get;
        set;
    }
    global Decimal TaxAmount {
        get;
        set;
    }
    global Id TaxLocationId {
        get;
        set;
    }
    global String UnitofMeasure {
        get;
        set;
    }
    global Decimal UnitPrice {
        get;
        set;
    }
    global Boolean UsageTaxable {
        get;
        set;
    }
    global Decimal VATAmount {
        get;
        set;
    }
    global Id VATId {
        get;
        set;
    }
    global ARCreditAdditionalCharge() {

    }
}
global class ARCreditHeader {
    global Id AccountId {
        get;
        set;
    }
    global String AGC {
        get;
        set;
    }
    global List<gii.ARCreditCreation.ARCreditAdditionalCharge> ARCreditAdditionalCharges {
        get;
        set;
    }
    global List<gii.ARCreditCreation.ARCreditSalesLine> ARCreditSalesLines {
        get;
        set;
    }
    global List<gii.ARCreditCreation.ARCreditServiceLine> ARCreditServiceLines {
        get;
        set;
    }
    global String BillingCity {
        get;
        set;
    }
    global String BillingCountry {
        get;
        set;
    }
    global String BillingName {
        get;
        set;
    }
    global String BillingStateProvince {
        get;
        set;
    }
    global String BillingStreet {
        get;
        set;
    }
    global String BillingZipPostalCode {
        get;
        set;
    }
    global Date CreditDate {
        get;
        set;
    }
    global String CurrencyISOCode {
        get;
        set;
    }
    global Date CustomerPODate {
        get;
        set;
    }
    global String CustomerPONumber {
        get;
        set;
    }
    global Id DivisionCodeId {
        get;
        set;
    }
    global String DocumentText {
        get;
        set;
    }
    global Boolean ExciseTaxable {
        get;
        set;
    }
    global Id InvoiceId {
        get;
        set;
    }
    global Decimal OrderDiscountPercent {
        get;
        set;
    }
    global Id OrderVATId {
        get;
        set;
    }
    global Boolean OrderVATTaxable {
        get;
        set;
    }
    global String PriceBookName {
        get;
        set;
    }
    global Boolean PrintDocumentText {
        get;
        set;
    }
    global String Reason {
        get;
        set;
    }
    global String ResaleCertificate {
        get;
        set;
    }
    global Date ResaleCertificateExpiration {
        get;
        set;
    }
    global Id SalesOrderId {
        get;
        set;
    }
    global String SalesRegion {
        get;
        set;
    }
    global Boolean SalesTaxable {
        get;
        set;
    }
    global String SalesTerritory {
        get;
        set;
    }
    global String ShipToCity {
        get;
        set;
    }
    global String ShipToCountry {
        get;
        set;
    }
    global String ShipToName {
        get;
        set;
    }
    global String ShipToStateProvince {
        get;
        set;
    }
    global String ShipToStreet {
        get;
        set;
    }
    global String ShipToZipPostalCode {
        get;
        set;
    }
    global Id TaxLocationId {
        get;
        set;
    }
    global Boolean UsageTaxable {
        get;
        set;
    }
    global String VATRegistration {
        get;
        set;
    }
    global String VATRoundingMethod {
        get;
        set;
    }
    global Boolean VATTaxable {
        get;
        set;
    }
    global Id WarehouseId {
        get;
        set;
    }
    global ARCreditHeader() {

    }
}
global class ARCreditSalesLine {
    global String AGC {
        get;
        set;
    }
    global Date CustomerPODate {
        get;
        set;
    }
    global String CustomerPOLine {
        get;
        set;
    }
    global String CustomerPONumber {
        get;
        set;
    }
    global Decimal DiscountAmount {
        get;
        set;
    }
    global Decimal DiscountPercent {
        get;
        set;
    }
    global Boolean ExciseTaxable {
        get;
        set;
    }
    global Id InvoiceDetailId {
        get;
        set;
    }
    global String LineDescription {
        get;
        set;
    }
    global String NoChargeReason {
        get;
        set;
    }
    global Decimal OrderDiscountAmount {
        get;
        set;
    }
    global Decimal OrderDiscountPercent {
        get;
        set;
    }
    global Decimal OrderQuantity {
        get;
        set;
    }
    global Decimal ProductAmount {
        get;
        set;
    }
    global Id ProductId {
        get;
        set;
    }
    global String Reason {
        get;
        set;
    }
    global Boolean RoundAmountstoZeroDecimal {
        get;
        set;
    }
    global Boolean RoundtoZeroDecimalwithRoundOff {
        get;
        set;
    }
    global Id SalesOrderLineId {
        get;
        set;
    }
    global Boolean SalesTaxable {
        get;
        set;
    }
    global String StockUM {
        get;
        set;
    }
    global Decimal TaxAmount {
        get;
        set;
    }
    global Id TaxLocationId {
        get;
        set;
    }
    global Decimal UnitPrice {
        get;
        set;
    }
    global Boolean UsageTaxable {
        get;
        set;
    }
    global Decimal VATAmount {
        get;
        set;
    }
    global Id VATId {
        get;
        set;
    }
    global String VATRegistration {
        get;
        set;
    }
    global Boolean VATTaxable {
        get;
        set;
    }
    global Id WarehouseId {
        get;
        set;
    }
    global ARCreditSalesLine() {

    }
}
global class ARCreditServiceLine {
    global String AGC {
        get;
        set;
    }
    global Boolean CustomerPODate {
        get;
        set;
    }
    global String CustomerPOLine {
        get;
        set;
    }
    global String CustomerPONumber {
        get;
        set;
    }
    global Decimal DiscountAmount {
        get;
        set;
    }
    global Decimal DiscountPercent {
        get;
        set;
    }
    global Boolean ExciseTaxable {
        get;
        set;
    }
    global Id InvoiceDetailId {
        get;
        set;
    }
    global String LineDescription {
        get;
        set;
    }
    global String NoChargeReason {
        get;
        set;
    }
    global Decimal OrderDiscountAmount {
        get;
        set;
    }
    global Decimal OrderDiscountPercent {
        get;
        set;
    }
    global Decimal OrderQuantity {
        get;
        set;
    }
    global Decimal PercentageOfLineUnitPrice {
        get;
        set;
    }
    global Boolean RoundAmountstoZeroDecimal {
        get;
        set;
    }
    global Boolean RoundtoZeroDecimalwithRoundOff {
        get;
        set;
    }
    global Id SalesOrderLineId {
        get;
        set;
    }
    global Boolean SalesTaxable {
        get;
        set;
    }
    global Decimal ServiceAmount {
        get;
        set;
    }
    global Id ServiceOrderLineId {
        get;
        set;
    }
    global Id ServiceProductId {
        get;
        set;
    }
    global Decimal TaxAmount {
        get;
        set;
    }
    global Id TaxLocationId {
        get;
        set;
    }
    global String UnitofMeasure {
        get;
        set;
    }
    global Decimal UnitPrice {
        get;
        set;
    }
    global Boolean UsageTaxable {
        get;
        set;
    }
    global Decimal VATAmount {
        get;
        set;
    }
    global Id VATId {
        get;
        set;
    }
    global String VATRegistration {
        get;
        set;
    }
    global Boolean VATTaxable {
        get;
        set;
    }
    global Id WarehouseId {
        get;
        set;
    }
    global ARCreditServiceLine() {

    }
}
}
