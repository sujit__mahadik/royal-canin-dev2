/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class InventoryTransactionHistory {
    global InventoryTransactionHistory() {

    }
    global static void CostChange(List<gii__Product2Add__c> prodRef, Map<Id,gii__Product2Add__c> oldProdRefMap, String TriggerType) {

    }
    global static void InventoryAdjustment(List<gii__InventoryAdjustment__c> invAdj) {

    }
    global static void InventoryReserve(List<gii__InventoryReserve__c> invRes, String TriggerAction) {

    }
    global static void InventoryTransfer(List<gii__InventoryTransfer__c> invTransfer) {

    }
    global static void RMADisposition(List<gii__RMADisposition__c> rmaDisposition) {

    }
    global static void RMAReceipt(List<gii__RMAReceipt__c> rmaReceipt, String TriggerAction) {

    }
    global static void ReceiptQueueCreate(List<gii__ReceiptQueue__c> receipts) {

    }
    global static void ReceiptQueueDelete(List<gii__ReceiptQueue__c> receipts) {

    }
    global static void SupplyOrder(List<gii__SupplyOrder__c> supOrd) {

    }
    global static void SupplyReceipt(List<gii__SupplyReceipt__c> suppRecpt) {

    }
    global static gii__InventoryTransactionHistory__c createIVHistory(Map<Id,gii__ProductInventory__c> piMap, Id piId) {
        return null;
    }
    global static Boolean createLog() {
        return null;
    }
}
