/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class MassUpdateSalesOrderLineHelper {
    global MassUpdateSalesOrderLineHelper() {

    }
    global static List<gii__SalesOrderLine__c> getSalesOrderLineForUpdate() {
        return null;
    }
    global static void rememberSalesOrderLineforUpdate(List<gii__SalesOrderLine__c> salesOrderLines) {

    }
}
