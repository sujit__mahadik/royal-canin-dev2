/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class PackListAmount {
    global PackListAmount() {

    }
    global static List<gii__PackListAdditionalCharge__c> AddlChargeCalculation(List<gii__PackListAdditionalCharge__c> packListAdChg, Set<Id> taxRateId, Set<Id> vatRateId, Date shippedDate, Boolean vatTaxable) {
        return null;
    }
}
