/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class Receipts {
    global Receipts() {

    }
    webService static void ConsignmentConsumption2(List<Id> selectedRecordIds, String shippedDateString) {

    }
    webService static void ConsignmentConsumption(List<Id> selectedRecordIds) {

    }
    webService static void ConsignmentReplenishmentOrTransfer2(List<Id> selectedRecordIds, String ReceiptDateString) {

    }
    webService static void ConsignmentReplenishmentOrTransfer(List<Id> selectedRecordIds) {

    }
}
