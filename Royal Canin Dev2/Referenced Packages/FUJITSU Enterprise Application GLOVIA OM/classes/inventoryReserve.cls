/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class inventoryReserve {
    global inventoryReserve() {

    }
    webService static void clearBackorder(List<Id> inventoryReserveId) {

    }
    webService static void clearPIBackorder(List<Id> productInventoryId) {

    }
    webService static void quickInvoice(List<Id> selectedRecordIds, String groupBy) {

    }
    webService static void quickShip(List<Id> selectedRecordIds, String groupByandShippedDate) {

    }
    webService static void reservation(List<Id> salesOrderLineId) {

    }
}
