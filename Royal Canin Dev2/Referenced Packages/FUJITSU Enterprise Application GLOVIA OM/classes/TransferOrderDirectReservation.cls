/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class TransferOrderDirectReservation {
    global TransferOrderDirectReservation() {

    }
    global static gii.TransferOrderDirectReservation.DirectReservationResult CreateReservation(gii.TransferOrderDirectReservation.inputDirectReservation inputInvResResultObj) {
        return null;
    }
    global static gii.TransferOrderDirectReservation.DirectReservationResult CreateReservationandQuickShip(gii.TransferOrderDirectReservation.inputDirectReservation inputInvResResultObj) {
        return null;
    }
global class DirectReservationResult {
    global List<gii.TransferOrderDirectReservation.GOMException> Exceptions {
        get;
        set;
    }
    global List<gii__InventoryReserve__c> listInvReserve {
        get;
        set;
    }
    global DirectReservationResult() {

    }
}
global class GOMException extends Exception {
}
global class inputDirectReservation {
    global String Action {
        get;
        set;
    }
    global List<gii__InventoryReserve__c> listInvReserve {
        get;
        set;
    }
    global Date ShippedDate {
        get;
        set;
    }
    global inputDirectReservation() {

    }
}
}
