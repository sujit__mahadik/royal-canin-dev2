/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class MassUpdateSalesOrderAddChgHelper {
    global MassUpdateSalesOrderAddChgHelper() {

    }
    global static List<gii__SalesOrderAdditionalCharge__c> getSalesOrderAddChgForUpdate() {
        return null;
    }
    global static void rememberSalesOrderAddChgforUpdate(List<gii__SalesOrderAdditionalCharge__c> soACs) {

    }
}
