/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class invoiceSchedulesServiceCon {
    global invoiceSchedulesServiceCon() {

    }
    global System.PageReference add() {
        return null;
    }
    global System.PageReference cancel() {
        return null;
    }
    global System.PageReference del() {
        return null;
    }
    global List<gii__ServiceOrderLine__c> getCurrentServiceInfo() {
        return null;
    }
    global Boolean getDisableButton() {
        return null;
    }
    global Boolean getDisableScheduleButton() {
        return null;
    }
    global List<gii__InvoiceScheduleDetail__c> getInvoiceSchedules() {
        return null;
    }
    global List<gii__ServiceOrderLine__c> getServiceInfo() {
        return null;
    }
    global Id getServiceOrderLineId() {
        return null;
    }
    global String getsoTitle() {
        return null;
    }
    global System.PageReference pageRedirect() {
        return null;
    }
    global System.PageReference reCreate() {
        return null;
    }
    global System.PageReference recalcByAmount() {
        return null;
    }
    global System.PageReference recalcByNetAmount() {
        return null;
    }
    global System.PageReference recalcByPercentage() {
        return null;
    }
    global System.PageReference reset() {
        return null;
    }
    global System.PageReference save() {
        return null;
    }
}
