/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class MassUpdateServiceOrderLineHelper {
    global MassUpdateServiceOrderLineHelper() {

    }
    global static List<gii__ServiceOrderLine__c> getServiceOrderLineForUpdate() {
        return null;
    }
    global static void rememberServiceOrderLineforUpdate(List<gii__ServiceOrderLine__c> serviceOrderLines) {

    }
}
