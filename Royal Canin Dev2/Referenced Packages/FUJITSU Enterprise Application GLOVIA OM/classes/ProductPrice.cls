/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class ProductPrice {
    global ProductPrice() {

    }
    global static Map<String,gii.ProductPrice.ProductRefPrice> getProductUnitPrice(List<gii.ProductPrice.ProductRefPrice> lstProductPrice) {
        return null;
    }
    global static Double getStandardPrice(Id productId, String priceBookName) {
        return null;
    }
global class ProductRefPrice {
    global String CurrencyCode {
        get;
        set;
    }
    global String Key {
        get;
        set;
    }
    global String PriceBookName {
        get;
        set;
    }
    global Id ProductReferenceId {
        get;
        set;
    }
    global Decimal Quantity {
        get;
        set;
    }
    global Decimal UnitPrice {
        get;
        set;
    }
    global ProductRefPrice() {

    }
}
}
