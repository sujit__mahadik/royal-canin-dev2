/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class ShipmentAmount {
    global ShipmentAmount() {

    }
    global static List<gii__ShipmentAdditionalCharge__c> AddlChargeCalculation(List<gii__ShipmentAdditionalCharge__c> shipmentAdChg, Set<Id> taxRateId, Set<Id> vatRateId, Date shippedDate, Boolean vatTaxable) {
        return null;
    }
    global static List<gii__ShipmentDetail__c> LineCalculation(List<gii__ShipmentDetail__c> soLine, Set<Id> taxRateId, Set<Id> VATRateId) {
        return null;
    }
    global static void SalesOrderHeaderCalculationRefresh(Map<Id,gii__SalesOrder__c> soH, List<gii__ShipmentDetail__c> shipment, List<gii__ShipmentDetail__c> oldshipment, String triggerAction) {

    }
    global static void SalesOrderHeaderCalculation(Map<Id,gii__SalesOrder__c> soH, List<gii__ShipmentDetail__c> shipment) {

    }
    global static void SalesOrderLineCalculationRefresh(Map<Id,gii__SalesOrderLine__c> soLines, List<gii__ShipmentDetail__c> shipment, List<gii__ShipmentDetail__c> oldshipment, String triggerAction) {

    }
    global static void SalesOrderLineCalculation(Map<Id,gii__SalesOrderLine__c> soLines, List<gii__ShipmentDetail__c> shipment) {

    }
}
