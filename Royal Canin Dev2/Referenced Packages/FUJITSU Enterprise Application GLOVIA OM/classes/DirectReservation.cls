/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class DirectReservation {
    global DirectReservation() {

    }
    global static gii.DirectReservation.DirectReservationResult ConsignmentConsumeOrReturn(gii.DirectReservation.inputDirectReservation inputInvResResultObj) {
        return null;
    }
    global static gii.DirectReservation.DirectReservationResult CreateReservation(gii.DirectReservation.inputDirectReservation inputInvResResultObj) {
        return null;
    }
    global static gii.DirectReservation.DirectReservationResult CreateReservationandQuickShip(gii.DirectReservation.inputDirectReservation inputInvResResultObj) {
        return null;
    }
global class DirectReservationResult {
    global List<gii.DirectReservation.GOMException> Exceptions {
        get;
        set;
    }
    global List<gii__InventoryReserve__c> listInvReserve {
        get;
        set;
    }
    global DirectReservationResult() {

    }
}
global class GOMException extends Exception {
}
global class inputDirectReservation {
    global String Action {
        get;
        set;
    }
    global List<gii__InventoryReserve__c> listInvReserve {
        get;
        set;
    }
    global Date ShippedDate {
        get;
        set;
    }
    global inputDirectReservation() {

    }
}
}
