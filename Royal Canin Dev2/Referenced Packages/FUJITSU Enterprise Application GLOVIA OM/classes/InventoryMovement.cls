/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class InventoryMovement {
    global InventoryMovement() {

    }
    global static gii.InventoryMovement.InventoryMovementResult TransferInventory(List<gii.InventoryMovement.InventoryMovementDetails> listInventoryMovementDetails) {
        return null;
    }
global class GOMException extends Exception {
}
global class InventoryMovementDetails {
    global Decimal BlockedQuantity {
        get;
        set;
    }
    global Decimal QuantitytoBeMoved {
        get;
        set;
    }
    global gii__ProductInventoryQuantityDetail__c SourceProductInventoryQuantityDetail {
        get;
        set;
    }
    global Id TargetLocationBinId {
        get;
        set;
    }
    global Id TargetLocationId {
        get;
        set;
    }
    global InventoryMovementDetails() {

    }
}
global class InventoryMovementResult {
    global List<gii.InventoryMovement.GOMException> Exceptions {
        get;
        set;
    }
    global List<gii__ProductInventoryQuantityDetail__c> listProductInventoryQuantityDetail {
        get;
        set;
    }
    global InventoryMovementResult() {

    }
}
}
