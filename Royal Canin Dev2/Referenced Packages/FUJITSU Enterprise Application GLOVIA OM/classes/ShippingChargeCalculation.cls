/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class ShippingChargeCalculation {
    global ShippingChargeCalculation() {

    }
    global static Map<String,gii.ShippingChargeCalculation.ShippingCharge> applyShippingChargeForSO(Set<Id> setSOIds) {
        return null;
    }
    global static List<gii__SalesOrderAdditionalCharge__c> clearShippingAddlChg(Map<Id,gii__SalesOrder__c> mapSO, Map<String,gii.ShippingChargeCalculation.ShippingCharge> mapSC) {
        return null;
    }
    global static List<gii__SalesOrderAdditionalCharge__c> createSalesOrderAddlChg(Map<String,gii.ShippingChargeCalculation.ShippingCharge> mapShippingCharge, List<gii__SalesOrder__c> lstSalesOrder) {
        return null;
    }
    global static Map<String,gii.ShippingChargeCalculation.ShippingCharge> getBestShippingCharge(List<gii.ShippingChargeCalculation.ShippingCharge> lstShippingCharge) {
        return null;
    }
    global static Decimal getChargeAmount(gii.ShippingChargeCalculation.ShippingCharge sc) {
        return null;
    }
    global static Map<String,gii.ShippingChargeCalculation.ShippingCharge> getLowestShippingCharge(List<gii.ShippingChargeCalculation.ShippingCharge> lstShippingCharge, Map<String,gii.ShippingChargeCalculation.ShippingCharge> mapOrderShipCharge, Map<String,gii.ShippingChargeCalculation.ShippingCharge> mapPgOrderShipCharge) {
        return null;
    }
    global static Map<Id,gii__Carrier__c> getOrderCarrierInfo(Set<Id> setCarrierIds) {
        return null;
    }
    global static Map<String,Decimal> getPromotionShippingDiscount(List<gii.ShippingChargeCalculation.ShippingCharge> lstTempShipChg, Map<String,gii__PromotionLine__c> mapPromoLine, Map<String,gii__PromotionLineShipping__c> mapPromoShip) {
        return null;
    }
    global static Map<String,Id> getRateTableIds(List<gii.ShippingChargeCalculation.ShippingCharge> lstTempShipChg, Map<Id,gii__Carrier__c> mapCarrier, Map<String,gii__PromotionLineShipping__c> mapPromoShip) {
        return null;
    }
    global static Map<String,gii__RateTableZone__c> getRateTableZone(Map<String,Id> mapRateTableIds) {
        return null;
    }
    global static Map<String,gii.ShippingChargeCalculation.TempShippingAmount> getShippingAmountBeforeDiscount(gii.ShippingChargeCalculation.ShippingCharge sc, Map<String,Id> mapRateTableIds, Map<String,gii.ShippingChargeCalculation.TempShippingZone> mapShipZone, Map<String,gii__RateTableZone__c> mapRateTableZone) {
        return null;
    }
    global static Map<String,Decimal> getShippingAmountOff(gii.ShippingChargeCalculation.ShippingCharge sc, Map<String,Decimal> mapPromoShipDisc) {
        return null;
    }
    global static Map<String,gii.ShippingChargeCalculation.ShippingCharge> getShippingCharge(List<gii.ShippingChargeCalculation.ShippingCharge> lstShippingCharge, Map<String,Id> mapValidPromotionId) {
        return null;
    }
    global static Map<String,Id> getValidPromotionIds(List<gii.ShippingChargeCalculation.ShippingCharge> lstShipChg) {
        return null;
    }
    global static Map<String,Id> getZoneTableIds(List<gii.ShippingChargeCalculation.ShippingCharge> lstTempShipChg, Map<Id,gii__Carrier__c> mapCarrier) {
        return null;
    }
    global static Map<Id,gii__ZoneTable__c> getZoneTable(Map<String,Id> mapZoneTableIds) {
        return null;
    }
    global static Map<String,gii.ShippingChargeCalculation.TempShippingZone> getZone(gii.ShippingChargeCalculation.ShippingCharge sc, Map<Id,gii__Carrier__c> mapCarrier, Map<String,Id> mapRateTableIds, Map<Id,gii__ZoneTable__c> mapZoneTable) {
        return null;
    }
    global static Map<String,List<gii.ShippingChargeCalculation.GOMException>> validateCarrier(List<gii.ShippingChargeCalculation.ShippingCharge> lstTempShipChg, Map<Id,gii__Carrier__c> mapCarrier, Map<String,Id> mapRateTableIds, Map<String,Id> mapZoneTableIds, Map<String,Decimal> mapPromoShipDisc) {
        return null;
    }
global class GOMException extends Exception {
}
global class ShippingCharge {
    global Id AdditionalChargeId {
        get;
        set;
    }
    global Id CarrierId {
        get;
        set;
    }
    global String Country {
        get;
        set;
    }
    global String CurrencyIsoCode {
        get;
        set;
    }
    global List<gii.ShippingChargeCalculation.GOMException> Errors {
        get;
        set;
    }
    global Boolean FlatFee {
        get;
        set;
    }
    global Boolean FreeShipping {
        get;
        set;
    }
    global String Key {
        get;
        set;
    }
    global String NoChargeReason {
        get;
        set;
    }
    global Decimal OrderAmount {
        get;
        set;
    }
    global Datetime OrderDate {
        get;
        set;
    }
    global Decimal OrderProductWeight {
        get;
        set;
    }
    global Id ProgramId {
        get;
        set;
    }
    global Id PromotionId {
        get;
        set;
    }
    global Boolean RatesByOrderAmount {
        get;
        set;
    }
    global Boolean RatesByOrderProductWeight {
        get;
        set;
    }
    global Decimal ShippingAmount {
        get;
        set;
    }
    global Decimal ShippingAmountOff {
        get;
        set;
    }
    global String StateProvince {
        get;
        set;
    }
    global String ZipPostalCode {
        get;
        set;
    }
    global ShippingCharge() {

    }
}
global class TempShippingAmount {
    global Decimal Amount {
        get;
        set;
    }
    global String CurrencyIsoCode {
        get;
        set;
    }
    global List<gii.ShippingChargeCalculation.GOMException> Errors {
        get;
        set;
    }
    global String Key {
        get;
        set;
    }
    global TempShippingAmount() {

    }
}
global class TempShippingZone {
    global List<gii.ShippingChargeCalculation.GOMException> Errors {
        get;
        set;
    }
    global String Key {
        get;
        set;
    }
    global String Zone {
        get;
        set;
    }
    global TempShippingZone() {

    }
}
}
