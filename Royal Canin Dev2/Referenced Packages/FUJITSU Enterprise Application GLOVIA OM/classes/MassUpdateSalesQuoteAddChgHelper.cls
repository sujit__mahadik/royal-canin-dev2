/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class MassUpdateSalesQuoteAddChgHelper {
    global MassUpdateSalesQuoteAddChgHelper() {

    }
    global static List<gii__SalesQuoteAdditionalCharge__c> getSalesQuoteAddChgForUpdate() {
        return null;
    }
    global static void rememberSalesQuoteAddChgforUpdate(List<gii__SalesQuoteAdditionalCharge__c> sqACs) {

    }
}
