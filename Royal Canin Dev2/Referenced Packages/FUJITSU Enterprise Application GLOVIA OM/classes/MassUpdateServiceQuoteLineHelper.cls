/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class MassUpdateServiceQuoteLineHelper {
    global MassUpdateServiceQuoteLineHelper() {

    }
    global static List<gii__ServiceQuoteLine__c> getServiceQuoteLineForUpdate() {
        return null;
    }
    global static void rememberServiceQuoteLineforUpdate(List<gii__ServiceQuoteLine__c> serviceQuoteLines) {

    }
}
