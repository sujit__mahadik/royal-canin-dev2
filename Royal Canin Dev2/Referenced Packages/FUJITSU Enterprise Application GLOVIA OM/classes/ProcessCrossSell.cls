/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class ProcessCrossSell {
    global ProcessCrossSell() {

    }
    global static void addAllLinesAndReserve(List<Id> crossSellProductIds, Map<Id,Double> crossSellMap, Id soLineId) {

    }
    global static void addAllSQLines(List<Id> crossSellProductIds, Map<Id,Double> crossSellMap, Id sqLineId) {

    }
    global static void addAllSQServiceLines(List<Id> crossSellProductIds, Map<Id,Double> crossSellMap, Id sqLineId) {

    }
    global static void addAllServiceOrderLines(List<Id> crossSellProductIds, Map<Id,Double> crossSellMap, Id soLineId) {

    }
    global static void addLinesandReserve(Id soLineId, Id crossSellProductId, Double quantity) {

    }
    global static void addSQLines(Id sqLineId, Id crossSellProductId, Double quantity) {

    }
}
