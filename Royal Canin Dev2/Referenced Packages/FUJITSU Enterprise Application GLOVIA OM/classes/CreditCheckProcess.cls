/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class CreditCheckProcess {
    global CreditCheckProcess() {

    }
    global static List<Id> InventoryReserve(List<Id> selectedRecordIds, String ivStatus) {
        return null;
    }
    global static List<Id> PacklistQueue(List<Id> selectedRecordIds) {
        return null;
    }
    global static List<Id> Packlist(List<Id> selectedRecordIds) {
        return null;
    }
    global static List<Id> Picklist(List<Id> selectedRecordIds) {
        return null;
    }
    global static List<Id> ServiceOrderLine(List<Id> selectedRecordIds) {
        return null;
    }
    global static List<Id> ShipmentQueue(List<Id> selectedRecordIds) {
        return null;
    }
    global static List<Id> Shipment(List<Id> selectedRecordIds) {
        return null;
    }
}
