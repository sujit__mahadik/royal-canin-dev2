/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class InvoiceAmount {
    global InvoiceAmount() {

    }
    global static List<gii__InvoiceAdditionalCharge__c> AddlChargeCalculation(List<gii__InvoiceAdditionalCharge__c> soA, Set<Id> taxRateId, Set<Id> vatRateId, Date shippedDate, Boolean vatTaxable) {
        return null;
    }
    global static List<gii__OrderInvoiceDetail__c> LineCalculation(List<gii__OrderInvoiceDetail__c> ivLine, Set<Id> taxRateId, Set<Id> VATRateId) {
        return null;
    }
    global static void SalesOrderHeaderCalculationAC(Map<Id,gii__SalesOrder__c> soH, List<gii__OrderInvoiceDetail__c> invoice, List<gii__InvoiceAdditionalCharge__c> soA) {

    }
    global static void SalesOrderHeaderCalculationRefresh(Map<Id,gii__SalesOrder__c> soH, List<gii__OrderInvoiceDetail__c> invoice, List<gii__OrderInvoiceDetail__c> oldinvoice, String triggerAction) {

    }
    global static void SalesOrderHeaderCalculation(Map<Id,gii__SalesOrder__c> soH, List<gii__OrderInvoiceDetail__c> invoice) {

    }
    global static void SalesOrderLineCalculationRefresh(Map<Id,gii__SalesOrderLine__c> soLines, List<gii__OrderInvoiceDetail__c> invoice, List<gii__OrderInvoiceDetail__c> oldinvoice, String triggerAction) {

    }
    global static void SalesOrderLineCalculation(Map<Id,gii__SalesOrderLine__c> soLines, List<gii__OrderInvoiceDetail__c> invoice) {

    }
    global static void ServiceOrderLineCalculationRefresh(Map<Id,gii__ServiceOrderLine__c> soLines, List<gii__OrderInvoiceDetail__c> invoice, List<gii__OrderInvoiceDetail__c> oldinvoice, String triggerAction) {

    }
    global static void ServiceOrderLineCalculation(Map<Id,gii__ServiceOrderLine__c> soLines, List<gii__OrderInvoiceDetail__c> invoice) {

    }
}
