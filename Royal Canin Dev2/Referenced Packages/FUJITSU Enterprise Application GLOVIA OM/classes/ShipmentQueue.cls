/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class ShipmentQueue {
    global ShipmentQueue() {

    }
    global static void buildShipmentQueue() {

    }
    global static void clearShipmentQueue() {

    }
    global static void createShipmentQueue(Set<Id> packlist) {

    }
    global static void deleteShipmentQueue(Set<Id> shipId) {

    }
}
