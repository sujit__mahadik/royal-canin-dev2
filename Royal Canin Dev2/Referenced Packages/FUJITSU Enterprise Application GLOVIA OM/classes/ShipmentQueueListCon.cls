/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class ShipmentQueueListCon {
    global List<gii__ShipmentQueue__c> selectedIVs {
        get;
        set;
    }
    global ApexPages.StandardSetController setCon;
    global ShipmentQueueListCon(ApexPages.StandardSetController controller) {

    }
    global void createShipment() {

    }
    global Boolean getactionEnabled() {
        return null;
    }
    global Boolean getcontinueBtn() {
        return null;
    }
    global String getcurrentAction() {
        return null;
    }
    global System.PageReference listButtonAction() {
        return null;
    }
    global System.PageReference returnToPrevious() {
        return null;
    }
    global void setcalledFrom(String s) {

    }
    global void setreturnURL(String r) {

    }
    global void setselectedIVs(List<gii__ShipmentQueue__c> s) {

    }
}
