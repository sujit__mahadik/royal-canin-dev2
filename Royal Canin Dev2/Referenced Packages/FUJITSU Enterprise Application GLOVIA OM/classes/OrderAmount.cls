/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class OrderAmount {
    global OrderAmount() {

    }
    global static List<gii__SalesOrderAdditionalCharge__c> AddlChargeCalculation(List<gii__SalesOrderAdditionalCharge__c> soA, String AmountType, Map<Id,gii__SalesOrder__c> so, List<gii__TaxRate__c> taxRate, List<gii__VATRate__c> vatR) {
        return null;
    }
    global static List<gii__SalesOrderLine__c> LineCalculation(List<gii__SalesOrderLine__c> soLine, Map<Id,gii__SalesOrder__c> soHeader, String AmountType, List<gii__TaxRate__c> taxRate, List<gii__VATRate__c> vatR) {
        return null;
    }
    global static List<gii__SalesQuoteAdditionalCharge__c> SQAddlChargeCalculation(List<gii__SalesQuoteAdditionalCharge__c> sqA, String AmountType, Map<Id,gii__SalesQuote__c> sq, List<gii__TaxRate__c> taxRate, List<gii__VATRate__c> vatR) {
        return null;
    }
    global static List<gii__SalesQuoteLine__c> SQLineCalculation(List<gii__SalesQuoteLine__c> sqLine, Map<Id,gii__SalesQuote__c> sqHeader, String AmountType, List<gii__TaxRate__c> taxRate, List<gii__VATRate__c> vatR) {
        return null;
    }
    global static List<gii__ServiceOrderLine__c> ServiceOrderLineCalculation(List<gii__ServiceOrderLine__c> soLine, Map<Id,gii__SalesOrder__c> soHeader, String AmountType, List<gii__TaxRate__c> taxRate, List<gii__VATRate__c> vatR) {
        return null;
    }
    global static List<gii__ServiceQuoteLine__c> ServiceQuoteLineCalculation(List<gii__ServiceQuoteLine__c> sqLine, Map<Id,gii__SalesQuote__c> sqHeader, String AmountType, List<gii__TaxRate__c> taxRate, List<gii__VATRate__c> vatR) {
        return null;
    }
}
