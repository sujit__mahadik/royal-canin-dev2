/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class InvoiceSchedule {
    global InvoiceSchedule() {

    }
    global static void Cancel(List<Id> selectedIds) {

    }
    global static void CreateForNonService(List<Id> selectedIds) {

    }
    global static void CreateForService(List<Id> selectedIds) {

    }
    global static List<gii__InvoiceScheduleDetail__c> CreateRevenue(String typeOfSchedule, String schdPeriod, Decimal noOfInstallment, Decimal amount, Decimal taxAmount, Decimal vatAmount, Date schdStartDate, Id selectedId, List<gii__InvoiceScheduleDetail__c> invSchdDetail, String productType, Id SalesOrder, Id Account, String OrderType) {
        return null;
    }
    global static Date ScheduleDate(String schdPeriod, Date schdDate) {
        return null;
    }
    global static void createInvSchedNonSrvByTemplate(Map<Id,gii__SalesOrderLine__c> solMap) {

    }
    global static void createInvSchedSrvByTemplate(Map<Id,gii__ServiceOrderLine__c> srvMap) {

    }
}
