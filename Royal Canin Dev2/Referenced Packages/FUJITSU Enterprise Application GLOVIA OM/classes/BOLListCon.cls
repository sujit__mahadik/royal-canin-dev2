/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class BOLListCon {
    global ApexPages.StandardSetController setCon;
    global BOLListCon(ApexPages.StandardSetController controller) {

    }
    global Boolean getactionEnabled() {
        return null;
    }
    global Boolean getcontinueBtn() {
        return null;
    }
    global String getcurrentAction() {
        return null;
    }
    global String getdocumentPageURL() {
        return null;
    }
    global String getreturnURL() {
        return null;
    }
    global Boolean getshowdoc() {
        return null;
    }
    global System.PageReference listButtonAction() {
        return null;
    }
    global System.PageReference returnToPrevious() {
        return null;
    }
    global void setreturnURL(String r) {

    }
    global void setselectedBOLs(List<gii__BillofLading__c> p) {

    }
}
