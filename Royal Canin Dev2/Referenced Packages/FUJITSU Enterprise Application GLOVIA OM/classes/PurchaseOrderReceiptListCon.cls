/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class PurchaseOrderReceiptListCon {
    global List<gii__PurchaseOrderReceipt__c> selectedrcps {
        get;
        set;
    }
    global ApexPages.StandardSetController setCon;
    global PurchaseOrderReceiptListCon(ApexPages.StandardSetController controller) {

    }
    global Boolean getactionEnabled() {
        return null;
    }
    global Boolean getcontinueBtn() {
        return null;
    }
    global System.PageReference listButtonAction() {
        return null;
    }
    global System.PageReference returnToPrevious() {
        return null;
    }
    global void reverse() {

    }
    global void setreturnURL(String r) {

    }
    global void setselectedrcps(List<gii__PurchaseOrderReceipt__c> r) {

    }
}
