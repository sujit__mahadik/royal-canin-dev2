/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class RMACreation {
    global RMACreation() {

    }
    global static gii.RMACreation.RMADispositionResult createRMADispositionsforReceipts(List<gii.RMACreation.RMADisposition> RMADispositions) {
        return null;
    }
    global static gii.RMACreation.RMAReceiptResult createRMAReceiptsforRMADetails(List<gii.RMACreation.RMAReceipt> RMAReceipts) {
        return null;
    }
    global static gii.RMACreation.RMACreationResult createRMARecords2(List<gii.RMACreation.RMAHeader> lstRMAHeader) {
        return null;
    }
    global static List<gii__RMA__c> createRMARecords(List<gii.RMACreation.RMAHeader> lstRMAHeader) {
        return null;
    }
    global static List<gii.RMACreation.RMAHeader> getRMADetailsForShipment(Set<Id> setShipmentIds, Date RMADate) {
        return null;
    }
global class GOMException extends Exception {
}
global class RMACreationResult {
    global List<gii.RMACreation.GOMException> Exceptions {
        get;
        set;
    }
    global List<gii__RMA__c> listRMAs {
        get;
        set;
    }
    global RMACreationResult() {

    }
}
global class RMADisposition {
    global Date DispositionDate {
        get;
        set;
    }
    global Id DispositionLocationBinId {
        get;
        set;
    }
    global Id DispositionLocationId {
        get;
        set;
    }
    global Decimal DispositionQuantity {
        get;
        set;
    }
    global Id RMAReceiptId {
        get;
        set;
    }
    global Date ScrapDate {
        get;
        set;
    }
    global Decimal ScrapQuantity {
        get;
        set;
    }
    global RMADisposition() {

    }
}
global class RMADispositionResult {
    global List<gii.RMACreation.GOMException> Exceptions {
        get;
        set;
    }
    global List<gii__RMADisposition__c> listRMADispositions {
        get;
        set;
    }
    global RMADispositionResult() {

    }
}
global class RMAHeader {
    global Id AccountId {
        get;
        set;
    }
    global String AccountName {
        get;
        set;
    }
    global Id CaseId {
        get;
        set;
    }
    global String City {
        get;
        set;
    }
    global String Country {
        get;
        set;
    }
    global String CurrencyISOCode {
        get;
        set;
    }
    global Id DivisionCodeId {
        get;
        set;
    }
    global String DocumentText {
        get;
        set;
    }
    global String Reason {
        get;
        set;
    }
    global String Reference {
        get;
        set;
    }
    global Date RMADate {
        get;
        set;
    }
    global List<gii.RMACreation.RMALine> RMALines {
        get;
        set;
    }
    global Id ShipmentId {
        get;
        set;
    }
    global String StateProvince {
        get;
        set;
    }
    global String Street {
        get;
        set;
    }
    global Id WarehouseId {
        get;
        set;
    }
    global String ZipPostalCode {
        get;
        set;
    }
    global RMAHeader() {

    }
}
global class RMALine {
    global Id ProductId {
        get;
        set;
    }
    global Id ProductLotId {
        get;
        set;
    }
    global Id ProductSerialId {
        get;
        set;
    }
    global Decimal RMAQuantity {
        get;
        set;
    }
    global Id SellingUnitofMeasureId {
        get;
        set;
    }
    global Id ShipmentDetailId {
        get;
        set;
    }
    global RMALine() {

    }
}
global class RMAReceipt {
    global Date ReceiptDate {
        get;
        set;
    }
    global Decimal ReceiptQuantity {
        get;
        set;
    }
    global Id RMADetailId {
        get;
        set;
    }
    global RMAReceipt() {

    }
}
global class RMAReceiptResult {
    global List<gii.RMACreation.GOMException> Exceptions {
        get;
        set;
    }
    global List<gii__RMAReceipt__c> listRMAReceipts {
        get;
        set;
    }
    global RMAReceiptResult() {

    }
}
}
