/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class InventoryTransactionHistoryForPO {
    global InventoryTransactionHistoryForPO() {

    }
    global static void PurchaseOrder(List<gii__PurchaseOrderLine__c> purOrd, Map<Id,gii__PurchaseOrderLine__c> oldPOLinesMap, String TriggerType) {

    }
    global static void PurchaseReceipt(List<gii__PurchaseOrderReceiptLine__c> purchRecpt, String TriggerType) {

    }
    global static gii__InventoryTransactionHistory__c createIVHistory(Map<Id,gii__ProductInventory__c> piMap, Id piId) {
        return null;
    }
}
