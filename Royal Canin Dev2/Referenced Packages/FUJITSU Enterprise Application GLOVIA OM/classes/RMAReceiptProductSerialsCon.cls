/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class RMAReceiptProductSerialsCon {
    global RMAReceiptProductSerialsCon() {

    }
    global System.PageReference add() {
        return null;
    }
    global gii__RMAReceipt__c getProductDetails() {
        return null;
    }
    global Double getProductSerialCount() {
        return null;
    }
    global Boolean getSerialcontrolled() {
        return null;
    }
    global List<gii__ProductSerial__c> getproductSerial() {
        return null;
    }
    global System.PageReference reset() {
        return null;
    }
    global System.PageReference save() {
        return null;
    }
    global void setproductSerial(List<gii__ProductSerial__c> productSerials) {

    }
    global void settotalSerials(Long t) {

    }
}
