/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class CaseDetailConvertToRMACon {
    global CaseDetailConvertToRMACon() {

    }
    global CaseDetailConvertToRMACon(ApexPages.StandardController controller) {

    }
    global System.PageReference detailButtonAction() {
        return null;
    }
    global Boolean getactionEnabled() {
        return null;
    }
    global Boolean getcontinueBtn() {
        return null;
    }
    global String getcurrentAction() {
        return null;
    }
    global System.PageReference returnToPrevious() {
        return null;
    }
}
