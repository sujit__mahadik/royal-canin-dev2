/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class WSInvoice {
    global WSInvoice() {

    }
    webService static List<SObject> getInvoiceAdditionalCharges(String InvoiceName, List<String> invoiceIds) {
        return null;
    }
    webService static List<Account> getInvoiceAddress(String InvoiceName, List<String> invoiceIds) {
        return null;
    }
    webService static List<SObject> getInvoiceDetails(String InvoiceName, List<String> invoiceIds) {
        return null;
    }
    webService static List<SObject> getInvoiceHeaderByAccount(String AccountName, String InvoiceDate) {
        return null;
    }
    webService static void setInvoiceRecDateTime(List<String> invoiceIds, String receivedDateTime) {

    }
}
