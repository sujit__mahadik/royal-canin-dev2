/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class MassUpdatePacklistAddChgHelper {
    global MassUpdatePacklistAddChgHelper() {

    }
    global static List<gii__PackListAdditionalCharge__c> getPacklistAddChgForUpdate() {
        return null;
    }
    global static void rememberPacklistAddChgforUpdate(List<gii__PackListAdditionalCharge__c> soACs) {

    }
}
