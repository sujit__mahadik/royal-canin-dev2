/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class Util {
    global static Boolean s_testMode {
        get;
        set;
    }
    global Util() {

    }
    global static String AddMessage(String Msg, String severityLevel) {
        return null;
    }
    global static String AddPageMessage(String Msg) {
        return null;
    }
    global static String ApplyWildCard(String searchText) {
        return null;
    }
    global static Boolean CreateOrderTaxRate(String TaxRateType, Boolean SalesTaxable, Boolean usageTaxable, Boolean exciseTaxable) {
        return null;
    }
    global static Boolean CreateStagingJEisEnabled(String TransactionType) {
        return null;
    }
    global static Boolean CreateStagingPurchaseCreditNote() {
        return null;
    }
    global static Boolean CreateStagingPurchaseInvoiceRecords() {
        return null;
    }
    global static Boolean CreateStagingSalesCreditNote() {
        return null;
    }
    global static void OrderLineInventoryReservation(List<Id> SalesOrderLineId) {

    }
    global static List<gii__ProductSerial__c> ProductSerial(List<gii__ProductSerial__c> productSerial, Set<String> AssignedSerials) {
        return null;
    }
    global static Integer ReleaseSalesOrder(List<gii__SalesOrder__c> selectedSO) {
        return null;
    }
    global static Boolean RoundUnitPrice(Decimal UnitPrice) {
        return null;
    }
    global static Boolean SOLineValidation(gii__SalesOrderLine__c newSOLineRecord, gii__SalesOrderLine__c oldSOLineRecord, Map<Id,gii__SalesOrderLine__c> oldSOLineMap) {
        return null;
    }
    global static Boolean UseCRMApplicationPriceBook(Boolean usingCRMApplication) {
        return null;
    }
    global static String accountCRStatus(gii__AccountAdd__c accountAdd) {
        return null;
    }
    global static String accountCreditStatus(Id AccountId) {
        return null;
    }
    global static Map<Id,String> accountCreditStatusMap(Set<Id> AccountId) {
        return null;
    }
    global static String getCustomerPortalAccountId(Id UserId) {
        return null;
    }
    global static Boolean getDisableTaxCalculationPolicyFlag() {
        return null;
    }
    global static Boolean getDisableVATCalculationPolicyFlag() {
        return null;
    }
    global static String getOrderByClause(Boolean UsePrimarySort, String sortVariable, String sortOrder) {
        return null;
    }
    global static Organization getOrg() {
        return null;
    }
    global static String getOrgStreet() {
        return null;
    }
    global static Map<String,Double> getPriceBookEntry(Set<Id> product2Ids, Set<Id> productRefIds, Boolean UsingCRMApplication) {
        return null;
    }
    global static gii__SystemPolicy__c getSystemPolicy() {
        return null;
    }
    global static Boolean getUseRoundedLineUnitDiscPolicyFlag() {
        return null;
    }
    global static String getdocumentPageURL(String DocumentType) {
        return null;
    }
    webService static void limitFlipOn() {

    }
    global static String padding(Integer repeat, String padChar) {
        return null;
    }
    global static Map<String,PricebookEntry> pbeMap(Set<Id> product2Id) {
        return null;
    }
    global static Map<Id,gii__ProductInventory__c> piMap(Set<Id> piIds) {
        return null;
    }
    global static Map<String,gii__ProductInventory__c> piToWarehouseProductMap(Set<Id> piToWarehouse, Set<Id> piToProduct) {
        return null;
    }
    global static String resetSOLinetatus(gii__SalesOrderLine__c sol) {
        return null;
    }
    global static String unreleasedError(String orderName) {
        return null;
    }
    global static void updateAccountBacklogAmount(Id AccountId, Double OldBacklogAmount, Double BacklogAmount) {

    }
    global static void updateAccountBacklogAmt(gii__AccountAdd__c accountAdd, Double OldBacklogAmount, Double BacklogAmount) {

    }
    global static void updateProductserialAndLot(Set<Id> InventoryReserveIds) {

    }
}
