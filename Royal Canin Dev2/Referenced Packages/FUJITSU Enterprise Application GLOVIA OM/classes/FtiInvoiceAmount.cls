/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class FtiInvoiceAmount {
    global FtiInvoiceAmount() {

    }
    global static List<gii__InvoiceAdditionalCharge__c> AddlChargeCalculation(List<gii__InvoiceAdditionalCharge__c> soA, Map<Id,gii__SalesOrder__c> soHMap) {
        return null;
    }
    global static List<gii__OrderInvoiceDetail__c> LineCalculation(List<gii__OrderInvoiceDetail__c> ivLine, Map<Id,gii__SalesOrder__c> soHMap, Map<Id,gii__SalesOrderLine__c> soLMap, Map<Id,gii__ServiceOrderLine__c> srLMap, Map<Id,gii__OrderInvoice__c> invMap) {
        return null;
    }
}
