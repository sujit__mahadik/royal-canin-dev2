/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class VASPromotion {
    global VASPromotion() {

    }
    global static List<gii__ServiceOrderLineVAS1__c> ApplyVAS1Promotion(Set<Id> soIds) {
        return null;
    }
    global static List<gii__ServiceOrderLineVAS2__c> ApplyVAS2Promotion(Set<Id> soIds) {
        return null;
    }
    global static List<gii__ServiceOrderLineVAS3__c> ApplyVAS3Promotion(Set<Id> soIds) {
        return null;
    }
    global static Map<String,gii.VASPromotion.TempServiceOrderLineVAS> ApplyVASOrderPromotion(Set<Id> setPromoIds, Map<String,gii.VASPromotion.OrderHeader> mapSO, Map<String,String> mapSrvSrvVAS, List<gii.VASPromotion.ServiceOrderLineVAS> lstVAS, List<gii.VASPromotion.OrderLine> lstSalesLine, Set<Id> setMerchandiseIds, Map<String,Id> mapOrderPromo, String VASType) {
        return null;
    }
    global static List<gii.VASPromotion.ServiceOrderLineVAS> ApplyVASPromotion(List<gii.VASPromotion.OrderHeader> lstOrder, String VASType) {
        return null;
    }
global class OrderHeader {
    global String CurrencyCode {
        get;
        set;
    }
    global String Key {
        get;
        set;
    }
    global Datetime OrderDate {
        get;
        set;
    }
    global List<gii.VASPromotion.OrderLine> OrderLines {
        get;
        set;
    }
    global Id ProgramId {
        get;
        set;
    }
    global Id PromotionId {
        get;
        set;
    }
    global List<gii.VASPromotion.ServiceOrderLineVAS> ServiceOrderLines {
        get;
        set;
    }
    global OrderHeader() {

    }
}
global class OrderLine {
    global Decimal CancelledQuantity {
        get;
        set;
    }
    global String Key {
        get;
        set;
    }
    global String OrderKey {
        get;
        set;
    }
    global Decimal OrderQuantity {
        get;
        set;
    }
    global Id ProductMerchandiseTypeId {
        get;
        set;
    }
    global Decimal UnitAmountOff {
        get;
        set;
    }
    global Decimal UnitPrice {
        get;
        set;
    }
    global OrderLine() {

    }
}
global class ServiceOrderLineVAS {
    global Decimal DiscountPercent {
        get;
        set;
    }
    global Decimal FlatFee {
        get;
        set;
    }
    global Boolean Free {
        get;
        set;
    }
    global String Key {
        get;
        set;
    }
    global String OrderKey {
        get;
        set;
    }
    global Id PromotionId {
        get;
        set;
    }
    global Decimal Quantity {
        get;
        set;
    }
    global String ServiceOrderLineKey {
        get;
        set;
    }
    global Decimal UnitPrice {
        get;
        set;
    }
    global ServiceOrderLineVAS() {

    }
}
global class TempServiceOrderLineVAS {
    global Decimal DiscountPercent {
        get;
        set;
    }
    global Decimal FlatFee {
        get;
        set;
    }
    global Boolean Free {
        get;
        set;
    }
    global Id Promotion {
        get;
        set;
    }
    global TempServiceOrderLineVAS() {

    }
}
}
