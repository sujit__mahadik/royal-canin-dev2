/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class CheckOrderTypeProcess {
    global CheckOrderTypeProcess() {

    }
    global static Integer ForwardToInvoiceQueue(List<Id> selectedRecordIds) {
        return null;
    }
    global static Integer InventoryReserve(List<Id> selectedRecordIds) {
        return null;
    }
    global static Integer InvoiceScheduleDetail(List<Id> selectedRecordIds) {
        return null;
    }
    global static Integer Invoice(List<Id> selectedRecordIds) {
        return null;
    }
    global static Integer PacklistQueue(List<Id> selectedRecordIds) {
        return null;
    }
    global static Integer Packlist(List<Id> selectedRecordIds) {
        return null;
    }
    global static Integer Picklist(List<Id> selectedRecordIds) {
        return null;
    }
    global static Integer ReceiptQueue(List<Id> selectedRecordIds) {
        return null;
    }
    global static Integer SalesOrderLine(List<Id> selectedRecordIds) {
        return null;
    }
    global static Integer ServiceOrderLine(List<Id> selectedRecordIds) {
        return null;
    }
    global static Integer ShipmentQueue(List<Id> selectedRecordIds) {
        return null;
    }
    global static Integer Shipment(List<Id> selectedRecordIds) {
        return null;
    }
}
