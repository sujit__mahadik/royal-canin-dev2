/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class ReceiptAssignSerial {
    global ReceiptAssignSerial() {

    }
    global static gii.ReceiptAssignSerial.AssignSerialResult AssignSerials(gii.ReceiptAssignSerial.AssignSerialInput inputObj) {
        return null;
    }
global class AssignSerialInput {
    global Boolean CalledByGOMVFPage {
        get;
        set;
    }
    global List<gii__ProductSerial__c> listProductSerials {
        get;
        set;
    }
    global Id TransactionId {
        get;
        set;
    }
    global AssignSerialInput() {

    }
}
global class AssignSerialResult {
    global List<gii.ReceiptAssignSerial.GOMException> Exceptions {
        get;
        set;
    }
    global List<gii__ProductSerial__c> listProductSerials {
        get;
        set;
    }
    global AssignSerialResult() {

    }
}
global class GOMException extends Exception {
}
}
