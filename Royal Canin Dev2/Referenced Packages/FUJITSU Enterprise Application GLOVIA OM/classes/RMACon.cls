/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class RMACon {
    global RMACon() {

    }
    global RMACon(ApexPages.StandardController c) {

    }
    global System.PageReference attachRMADocument() {
        return null;
    }
    global String getCurrentDateTimeinUserLocale() {
        return null;
    }
    global System.PageReference getDeliveredAsPDF() {
        return null;
    }
    global String getLogoId() {
        return null;
    }
    global Organization getOrg() {
        return null;
    }
    global gii__RMA__c getOrgAddress() {
        return null;
    }
    global gii__RMA__c getRMA() {
        return null;
    }
    global List<gii__RMADetail__c> getRMADetails() {
        return null;
    }
    global gii__SystemPolicy__c getSystemPolicy() {
        return null;
    }
    global gii__Warehouse__c getWareHouseAddress() {
        return null;
    }
}
