/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class POReceiptLineListCon {
    global List<gii__PurchaseOrderReceiptLine__c> selectedsols {
        get;
        set;
    }
    global ApexPages.StandardSetController setCon;
    global POReceiptLineListCon(ApexPages.StandardSetController controller) {

    }
    global Boolean getactionEnabled() {
        return null;
    }
    global Boolean getcontinueBtn() {
        return null;
    }
    global String getcurrentAction() {
        return null;
    }
    global System.PageReference pageAction() {
        return null;
    }
    global System.PageReference returnToPrevious() {
        return null;
    }
    global void setreturnURL(String r) {

    }
    global void setselectedsols(List<gii__PurchaseOrderReceiptLine__c> sl) {

    }
}
