/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class MassUpdateSalesOrderHelper {
    global MassUpdateSalesOrderHelper() {

    }
    global static List<gii__SalesOrder__c> getSalesOrderForUpdate() {
        return null;
    }
    global static void rememberSalesOrderforUpdate(List<gii__SalesOrder__c> salesOrders) {

    }
}
