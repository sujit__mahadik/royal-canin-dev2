/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class InventoryInquirySQCon {
    global InventoryInquirySQCon() {

    }
    global InventoryInquirySQCon(ApexPages.StandardController c) {

    }
    global gii__TemporaryPlaceHolder__c addInvenotyInquiryRecord(Double RequiredQty, gii__ProductInventory__c pi, Boolean OtherWarehouses) {
        return null;
    }
    global gii__TemporaryPlaceHolder__c addInvenotyInquiryRecord_1(Double RequiredQty, gii__ProductInventory__c pi, Boolean OtherWarehouses, Boolean kitMembers) {
        return null;
    }
    global Boolean getDetailExists() {
        return null;
    }
    global List<gii__ProductInventory__c> getDetailProdInv() {
        return null;
    }
    global String getDetailTitle() {
        return null;
    }
    global Map<Id,gii__ProductInventory__c> getProductInventoryForSalesQuote(Set<Id> product2AddIds) {
        return null;
    }
    global Map<Id,gii__ProductInventory__c> getProductInventory(Set<Id> ProductIds, Boolean OtherWarehouses, String DefaultWarehouseId) {
        return null;
    }
    global gii__SalesQuote__c getSalesQuote() {
        return null;
    }
    global List<gii__SalesQuoteLine__c> getSalesQuoteLine() {
        return null;
    }
    global List<gii__TemporaryPlaceHolder__c> getTPHDetailList() {
        return null;
    }
    global List<gii__TemporaryPlaceHolder__c> getTPHList() {
        return null;
    }
    global String getdetailType() {
        return null;
    }
    global Boolean getshowError() {
        return null;
    }
    global System.PageReference showDetail() {
        return null;
    }
}
