/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class PurchaseOrderLineListCon {
    global List<gii__PurchaseOrderLine__c> selectedrcps {
        get;
        set;
    }
    global ApexPages.StandardSetController setCon;
    global PurchaseOrderLineListCon(ApexPages.StandardSetController controller) {

    }
    global Boolean getactionEnabled() {
        return null;
    }
    global Boolean getcontinueBtn() {
        return null;
    }
    global System.PageReference listButtonAction() {
        return null;
    }
    global void receive() {

    }
    global System.PageReference returnToPrevious() {
        return null;
    }
    global void setreturnURL(String r) {

    }
    global void setselectedrcps(List<gii__PurchaseOrderLine__c> r) {

    }
}
