/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class TransferOrderShipment {
    global TransferOrderShipment() {

    }
    global static gii.TransferOrderShipment.TransferOrderShipmentResult quickShip(gii.TransferOrderShipment.QuickShipInput inputObj) {
        return null;
    }
global class GOMException extends Exception {
}
global class QuickShipInput {
    global List<Id> selectedRecordIds {
        get;
        set;
    }
    global Date ShippedDate {
        get;
        set;
    }
    global QuickShipInput() {

    }
}
global class TransferOrderShipmentResult {
    global List<gii.TransferOrderShipment.GOMException> Exceptions {
        get;
        set;
    }
    global List<gii__ReceiptQueue__c> listIReceiptQueue {
        get;
        set;
    }
    global TransferOrderShipmentResult() {

    }
}
}
