/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class MassUpdateShipmentAddChgHelper {
    global MassUpdateShipmentAddChgHelper() {

    }
    global static List<gii__ShipmentAdditionalCharge__c> getShipmentAddChgForUpdate() {
        return null;
    }
    global static void rememberShipmentAddChgforUpdate(List<gii__ShipmentAdditionalCharge__c> soACs) {

    }
}
