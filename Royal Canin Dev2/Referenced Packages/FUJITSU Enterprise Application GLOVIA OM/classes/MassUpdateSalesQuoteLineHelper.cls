/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class MassUpdateSalesQuoteLineHelper {
    global MassUpdateSalesQuoteLineHelper() {

    }
    global static List<gii__SalesQuoteLine__c> getSalesQuoteLineForUpdate() {
        return null;
    }
    global static void rememberSalesQuoteLineforUpdate(List<gii__SalesQuoteLine__c> salesQuoteLines) {

    }
}
