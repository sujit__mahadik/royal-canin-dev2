/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class AccountInformation {
    global AccountInformation() {

    }
    global static List<gii__SalesOrderLine__c> assignToSalesOrderLine(Map<Id,gii__SalesOrder__c> soHdrs, List<gii__SalesOrderLine__c> soLine, Map<Id,gii__Product2Add__c> prod, List<gii__TaxRate__c> TaxRate, List<gii__VATRate__c> VATRate, List<gii__ProductInventory__c> prodInventory, List<gii__SystemPolicy__c> sys, Boolean triggerIsInsert) {
        return null;
    }
    global static List<gii__SalesOrder__c> assignToSalesOrder(Map<Id,gii__AccountAdd__c> account, Map<Id,Account> act, List<gii__SalesOrder__c> soHeader) {
        return null;
    }
    global static List<gii__SalesQuoteLine__c> assignToSalesQuoteLine(Map<Id,gii__SalesQuote__c> sqHdrs, List<gii__SalesQuoteLine__c> sqLine, Map<Id,gii__Product2Add__c> prod, List<gii__TaxRate__c> TaxRate, List<gii__VATRate__c> VATRate, List<gii__ProductInventory__c> prodInventory, List<gii__SystemPolicy__c> sys, Boolean triggerIsInsert) {
        return null;
    }
    global static List<gii__SalesQuote__c> assignToSalesQuote(Map<Id,gii__AccountAdd__c> account, Map<Id,Account> act, List<gii__SalesQuote__c> sqHeader) {
        return null;
    }
    global static List<gii__ServiceOrderLine__c> assignToServiceOrderLine(Map<Id,gii__SalesOrder__c> soHdrs, List<gii__ServiceOrderLine__c> soLine, Map<Id,gii__Product2Add__c> prod, List<gii__TaxRate__c> TaxRate, List<gii__VATRate__c> VATRate, List<gii__SystemPolicy__c> sys, Boolean triggerIsInsert) {
        return null;
    }
    global static List<gii__ServiceQuoteLine__c> assignToServiceQuoteLine(Map<Id,gii__SalesQuote__c> sqHdrs, List<gii__ServiceQuoteLine__c> sqLine, Map<Id,gii__Product2Add__c> prod, List<gii__TaxRate__c> TaxRate, List<gii__VATRate__c> VATRate, List<gii__SystemPolicy__c> sys, Boolean triggerIsInsert) {
        return null;
    }
}
