/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class EMailCon {
    global EMailCon() {

    }
    global EMailCon(ApexPages.StandardController controller) {

    }
    global System.PageReference cancel() {
        return null;
    }
    global gii__ARCredit__c getARCM() {
        return null;
    }
    global String getDeliveredAsPDF() {
        return null;
    }
    global Contact getEmailId() {
        return null;
    }
    global gii__TemporaryPlaceHolder__c getEmailInfo() {
        return null;
    }
    global gii__OrderInvoice__c getInvoice() {
        return null;
    }
    global gii__PurchaseOrder__c getPO() {
        return null;
    }
    global gii__RMA__c getRMA() {
        return null;
    }
    global gii__SalesOrder__c getSO() {
        return null;
    }
    global gii__SalesQuote__c getSQ() {
        return null;
    }
    global gii__SystemPolicy__c getSystemPolicy() {
        return null;
    }
    global Boolean getTPHExists() {
        return null;
    }
    global System.PageReference save() {
        return null;
    }
}
