/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class PicklistCon {
    global PicklistCon() {

    }
    global PicklistCon(ApexPages.StandardController c) {

    }
    global System.PageReference Lot() {
        return null;
    }
    global System.PageReference add() {
        return null;
    }
    global System.PageReference addLot() {
        return null;
    }
    global System.PageReference attachPickList() {
        return null;
    }
    global System.PageReference cancel() {
        return null;
    }
    global Double getAlreadyAssignedQty() {
        return null;
    }
    global String getCurrentDateTimeinUserLocale() {
        return null;
    }
    global String getLogoId() {
        return null;
    }
    global Organization getOrg() {
        return null;
    }
    global gii__SalesOrder__c getOrgAddress() {
        return null;
    }
    global List<gii__ProductInventory__c> getPIAltBinList() {
        return null;
    }
    global Boolean getPIAltbinListExists() {
        return null;
    }
    global gii__PickList__c getPicklist() {
        return null;
    }
    global gii__PickListDetail__c getPicklistDetail() {
        return null;
    }
    global List<gii__PickListDetail__c> getPicklists() {
        return null;
    }
    global Double getProductSerialCount() {
        return null;
    }
    global Boolean getShowBin() {
        return null;
    }
    global List<gii__LotDetail__c> getlotDetail() {
        return null;
    }
    global String getpickDetTitle() {
        return null;
    }
    global List<gii__ProductSerial__c> getproductSerial() {
        return null;
    }
    global Boolean getshowLot() {
        return null;
    }
    global Boolean getshowSerial() {
        return null;
    }
    global System.PageReference reset() {
        return null;
    }
    global System.PageReference save() {
        return null;
    }
    global System.PageReference saveLot() {
        return null;
    }
    global System.PageReference saveProductSerial() {
        return null;
    }
    global void setQtytoBeAssigned(Double qty) {

    }
    global void setlotDetail(List<gii__LotDetail__c> lotDetails) {

    }
    global void setproductSerial(List<gii__ProductSerial__c> productSerials) {

    }
    global void settotalSerials(Long t) {

    }
}
