/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class OrderPromotion {
    global OrderPromotion() {

    }
    global static Map<String,gii.OrderPromotion.TempDiscount> ApplyDiscountForOrderLines(String QtyPromotionType, List<gii.OrderPromotion.OrderLine> lstSOLine, Set<Id> setPromoIds, Map<String,gii__OrderValueDiscount__c> mapOVD, Map<Id,gii__MerchandiseType__c> mapMerchandiseType, Map<String,gii__PromotionLine__c> mapPromoLine, Map<String,Id> mapOrderLinePromo, Map<String,gii.OrderPromotion.OrderHeader> mapOrderHeader) {
        return null;
    }
    global static List<gii.OrderPromotion.OrderLine> ApplyDiscountForOrder(List<gii.OrderPromotion.OrderHeader> lstOrderHeader) {
        return null;
    }
    global static List<gii__SalesOrderLine__c> ApplyDiscountForSO(Set<Id> setSOIds) {
        return null;
    }
    global static Map<String,String> BuildPromoDiscKeyMap(gii__PromotionLine__c promo, Map<String,String> mapPromoDiscKey, gii.OrderPromotion.OrderLine sol) {
        return null;
    }
    global static Map<String,String> BuildPromoKeyMap(Map<String,String> mapPromoKey, gii.OrderPromotion.OrderLine sol, Map<String,Id> mapOrderLinePromo) {
        return null;
    }
    global static gii.OrderPromotion.OrderLine GetPromoDiscountForOrderLine(gii__PromotionLine__c promotion, gii.OrderPromotion.OrderLine sol, Decimal SummaryNumber, Decimal PromoDiscountPercent, Decimal PromoUnitAmountOff, String QtyPromotionType) {
        return null;
    }
global class OrderHeader {
    global String CurrencyIsoCode {
        get;
        set;
    }
    global String Key {
        get;
        set;
    }
    global Datetime OrderDate {
        get;
        set;
    }
    global List<gii.OrderPromotion.OrderLine> OrderLines {
        get;
        set;
    }
    global String PriceBookName {
        get;
        set;
    }
    global Id ProgramId {
        get;
        set;
    }
    global Id PromotionId {
        get;
        set;
    }
    global OrderHeader() {

    }
}
global class OrderLine {
    global Decimal CancelledQuantity {
        get;
        set;
    }
    global String CurrencyIsoCode {
        get;
        set;
    }
    global Decimal DiscountPercent {
        get;
        set;
    }
    global String Key {
        get;
        set;
    }
    global String OrderKey {
        get;
        set;
    }
    global String OrderPriceBookName {
        get;
        set;
    }
    global Decimal OrderQuantity {
        get;
        set;
    }
    global Decimal OriginalUnitPrice {
        get;
        set;
    }
    global String PricingOverriddenReason {
        get;
        set;
    }
    global Decimal ProductAmountAfterDiscount {
        get;
        set;
    }
    global Id ProductId {
        get;
        set;
    }
    global Id ProductMerchandiseTypeId {
        get;
        set;
    }
    global Id ProductPromotionGroupId {
        get;
        set;
    }
    global Id ProductStyleId {
        get;
        set;
    }
    global Decimal UnitAmountOff {
        get;
        set;
    }
    global Decimal UnitPrice {
        get;
        set;
    }
    global OrderLine() {

    }
}
global class TempDiscount {
    global TempDiscount() {

    }
}
}
