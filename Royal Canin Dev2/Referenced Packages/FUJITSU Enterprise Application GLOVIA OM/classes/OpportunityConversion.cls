/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class OpportunityConversion {
    global OpportunityConversion() {

    }
    global static List<String> addSalesOrderKitMembers(String soId) {
        return null;
    }
    global static List<String> addSalesQuoteKitMembers(String sqId) {
        return null;
    }
    global static String createSalesOrder(String OppIdAndOrderType) {
        return null;
    }
    global static String createSalesQuote(String OppId) {
        return null;
    }
}
