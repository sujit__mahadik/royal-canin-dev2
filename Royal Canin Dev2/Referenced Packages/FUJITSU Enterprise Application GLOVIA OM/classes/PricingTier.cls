/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class PricingTier {
    global PricingTier() {

    }
    global static Decimal GetTierUnitPrice(gii__PriceBookEntry__c pbe, Decimal OrderQuantity) {
        return null;
    }
    global static Map<String,gii__PriceBookEntry__c> getPriceBookEntry(Set<Id> setProductRefIds, gii.PricingTier.Pricing sys) {
        return null;
    }
global class Pricing {
    global gii__SystemPolicy__c SystemPolicy {
        get;
        set;
    }
    global Pricing() {

    }
}
}
