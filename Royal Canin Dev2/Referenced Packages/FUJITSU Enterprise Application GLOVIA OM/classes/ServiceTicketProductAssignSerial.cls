/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class ServiceTicketProductAssignSerial {
    global ServiceTicketProductAssignSerial() {

    }
    global static gii.ServiceTicketProductAssignSerial.AssignSerialResult AssignSerials(gii.ServiceTicketProductAssignSerial.AssignSerialInput inputObj) {
        return null;
    }
global class AssignSerialInput {
    global String Action {
        get;
        set;
    }
    global Boolean CalledByGOMVFPage {
        get;
        set;
    }
    global Date IssueDate {
        get;
        set;
    }
    global List<gii__ProductSerial__c> listProductSerials {
        get;
        set;
    }
    global String NoChargeReason {
        get;
        set;
    }
    global Id ProductId {
        get;
        set;
    }
    global Id ServiceTicketId {
        get;
        set;
    }
    global Id ServiceTicketLineId {
        get;
        set;
    }
    global Id TransactionId {
        get;
        set;
    }
    global Decimal UnitPrice {
        get;
        set;
    }
    global Id WarehouseId {
        get;
        set;
    }
    global AssignSerialInput() {

    }
}
global class AssignSerialResult {
    global List<gii.ServiceTicketProductAssignSerial.GOMException> Exceptions {
        get;
        set;
    }
    global List<gii__ProductSerial__c> listProductSerials {
        get;
        set;
    }
    global AssignSerialResult() {

    }
}
global class GOMException extends Exception {
}
}
