/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
@isTest
global class OrderExecutionCon {
    global OrderExecutionCon() {

    }
    global OrderExecutionCon(ApexPages.StandardController controller) {

    }
    @isTest(SeeAllData=false OnInstall=false)
    global static void TestOrderExecutionCon() {

    }
    global gii__SystemPolicy__c getPolicy() {
        return null;
    }
    global String getTabInFocus() {
        return null;
    }
    global String getTabInFocus01() {
        return null;
    }
    global String getTabInFocus02() {
        return null;
    }
    global String getTabInFocus03() {
        return null;
    }
    global void setTabInFocus(String s) {

    }
    global void setTabInFocus01(String s) {

    }
    global void setTabInFocus02(String s) {

    }
    global void setTabInFocus03(String s) {

    }
}
