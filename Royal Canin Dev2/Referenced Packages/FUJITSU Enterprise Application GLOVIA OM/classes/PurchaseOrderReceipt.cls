/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class PurchaseOrderReceipt {
    global PurchaseOrderReceipt() {

    }
    global static void receive(List<Id> selectedRecordIds, String receiptDateString, String calledFrom) {

    }
    global static void reverse(List<Id> selectedRecordIds) {

    }
}
