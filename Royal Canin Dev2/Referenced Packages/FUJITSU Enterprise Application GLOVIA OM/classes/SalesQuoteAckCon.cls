/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class SalesQuoteAckCon {
    global SalesQuoteAckCon() {

    }
    global System.PageReference attachSQAck() {
        return null;
    }
    global String getCurrentDateTimeinUserLocale() {
        return null;
    }
    global String getLogoId() {
        return null;
    }
    global List<gii__OrderTaxRate__c> getOrderTaxRate() {
        return null;
    }
    global List<gii__VATRate__c> getOrderVATRate() {
        return null;
    }
    global Organization getOrg() {
        return null;
    }
    global gii__SalesQuote__c getOrgAddress() {
        return null;
    }
    global Integer getQuoteDetailcount() {
        return null;
    }
    global gii__SalesQuote__c getSalesQuote() {
        return null;
    }
    global List<gii__SalesQuoteAdditionalCharge__c> getSalesQuoteAdditionalCharge() {
        return null;
    }
    global List<gii__SalesQuoteLine__c> getSalesQuoteLines() {
        return null;
    }
    global Integer getServiceDetailcount() {
        return null;
    }
    global List<gii__ServiceQuoteLine__c> getServiceQuoteLines() {
        return null;
    }
    global Account getaccountAddress() {
        return null;
    }
}
