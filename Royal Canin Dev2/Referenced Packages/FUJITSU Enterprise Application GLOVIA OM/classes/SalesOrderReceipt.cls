/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class SalesOrderReceipt {
    global SalesOrderReceipt() {

    }
    global static gii.SalesOrderReceipt.SalesOrderReceiiptsResult cancelReceipts(Set<Id> ReceiptRecordIds) {
        return null;
    }
    global static gii.SalesOrderReceipt.SalesOrderReceiiptsResult getReceiptsforReceiptQueues(gii.SalesOrderReceipt.ReceiptCreationInput inputObj) {
        return null;
    }
global class GOMException extends Exception {
}
global class ReceiptCreationInput {
    global Map<Id,Id> mapReceiptQueueLocation {
        get;
        set;
    }
    global Map<Id,Id> mapReceiptQueueLocationBin {
        get;
        set;
    }
    global Date ReceiptDate {
        get;
        set;
    }
    global ReceiptCreationInput() {

    }
}
global class SalesOrderReceiiptsResult {
    global List<gii.SalesOrderReceipt.GOMException> Exceptions {
        get;
        set;
    }
    global List<gii__Receipt__c> listReceipt {
        get;
        set;
    }
    global SalesOrderReceiiptsResult() {

    }
}
}
