/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class InventoryTransferAssignLot {
    global InventoryTransferAssignLot() {

    }
    global static gii.InventoryTransferAssignLot.AssignLotResult AssignLots(gii.InventoryTransferAssignLot.AssignLotInput inputObj) {
        return null;
    }
global class AssignLotInput {
    global Boolean CalledByGOMVFPage {
        get;
        set;
    }
    global List<gii__LotDetail__c> listLotDetails {
        get;
        set;
    }
    global Id TransactionId {
        get;
        set;
    }
    global AssignLotInput() {

    }
}
global class AssignLotResult {
    global List<gii.InventoryTransferAssignLot.GOMException> Exceptions {
        get;
        set;
    }
    global List<gii__LotDetail__c> listLotDetails {
        get;
        set;
    }
    global AssignLotResult() {

    }
}
global class GOMException extends Exception {
}
}
