/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class ServiceOrderCompletionListCon {
    global List<gii__ServiceOrderCompletion__c> selectedIds {
        get;
        set;
    }
    global ApexPages.StandardSetController setCon;
    global ServiceOrderCompletionListCon(ApexPages.StandardSetController controller) {

    }
    global void forwardToInvoice() {

    }
    global Boolean getactionEnabled() {
        return null;
    }
    global Boolean getcontinueBtn() {
        return null;
    }
    global String getcurrentAction() {
        return null;
    }
    global System.PageReference listButtonAction() {
        return null;
    }
    global System.PageReference returnToPrevious() {
        return null;
    }
    global void setselectedIdss(List<gii__ServiceOrderCompletion__c> i) {

    }
}
