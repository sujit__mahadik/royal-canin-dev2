/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class UnitofMeasureHelper {
    global UnitofMeasureHelper() {

    }
    global static gii.UnitofMeasureHelper.ProductUnitofMeasureConversionDetail getConversionFactorandConvertedQuantity(Map<String,gii__ProductUnitofMeasureConversion__c> mapProductUnitofMeasureConversion, gii.UnitofMeasureHelper.ProductUnitofMeasureConversionDetail inputPUoMConversionObject) {
        return null;
    }
    global static Decimal getConvertedQuantity(Decimal conversionFactor, Decimal QuantityToBeConverted, Integer RoundingDecimals) {
        return null;
    }
    global static Map<String,gii__ProductUnitofMeasureConversion__c> getMapProductUnitofMeasureConversion(Set<String> setProductUMConversionUniqueIds) {
        return null;
    }
global class ProductUnitofMeasureConversionDetail {
    global Decimal conversionFactor {
        get;
        set;
    }
    global Decimal convertedQuantity {
        get;
        set;
    }
    global Id productId {
        get;
        set;
    }
    global Decimal quantityToBeConverted {
        get;
        set;
    }
    global Integer roundingDecimals {
        get;
        set;
    }
    global Id sourceUnitofMeasureId {
        get;
        set;
    }
    global Id targetUnitofMeasureId {
        get;
        set;
    }
    global ProductUnitofMeasureConversionDetail() {

    }
}
}
