/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class SalesOrderCreation {
    global SalesOrderCreation() {

    }
    global static gii.SalesOrderCreation.SalesOrderCreationResult createSalesOrders2(List<gii.SalesOrderCreation.SalesOrderHeader> listSalesOrderHeader) {
        return null;
    }
    global static List<Id> createSalesOrders(List<gii.SalesOrderCreation.SalesOrderHeader> listSalesOrderHeader) {
        return null;
    }
    global static gii.SalesOrderCreation.SalesOrderCopyVASResult createServiceOrderLineVAS2(List<gii.SalesOrderCreation.ServiceOrderLine> listServiceOrderLine) {
        return null;
    }
    global static Set<Id> createServiceOrderLineVAS(List<gii.SalesOrderCreation.ServiceOrderLine> listServiceOrderLine) {
        return null;
    }
    global static List<gii.SalesOrderCreation.SalesOrderHeader> getSalesOrderCopyInfo(List<gii.SalesOrderCreation.SalesOrderCopy> listSalesOrderCopyInfo) {
        return null;
    }
    global static List<gii.SalesOrderCreation.ServiceOrderLine> getServiceOrderLineVASfromSource(Set<Id> setNewSalesOrderIds) {
        return null;
    }
global class GOMException extends Exception {
}
global class SalesOrderAdditionalCharge {
    global Id AdditionalChargeId {
        get;
        set;
    }
    global String AGC {
        get;
        set;
    }
    global Decimal CalculatedShippingAmount {
        get;
        set;
    }
    global Boolean ExciseTaxable {
        get;
        set;
    }
    global Decimal FlatFee {
        get;
        set;
    }
    global Boolean FreeShipping {
        get;
        set;
    }
    global String NoChargeReason {
        get;
        set;
    }
    global String OrderKey {
        get;
        set;
    }
    global Id PromotionId {
        get;
        set;
    }
    global Decimal Quantity {
        get;
        set;
    }
    global String SalesOrderAdditionalChargeKey {
        get;
        set;
    }
    global Id SalesOrderServiceTicketLineId {
        get;
        set;
    }
    global Boolean SalesTaxable {
        get;
        set;
    }
    global Id ServiceTicketChargeId {
        get;
        set;
    }
    global Id ServiceTicketId {
        get;
        set;
    }
    global Id ServiceTicketLineId {
        get;
        set;
    }
    global Decimal ShippingAmountOff {
        get;
        set;
    }
    global Decimal TaxAmount {
        get;
        set;
    }
    global Id TaxLocationId {
        get;
        set;
    }
    global Decimal UnitCost {
        get;
        set;
    }
    global String UnitofMeasure {
        get;
        set;
    }
    global Decimal UnitPrice {
        get;
        set;
    }
    global Boolean UsageTaxable {
        get;
        set;
    }
    global Decimal VATAmount {
        get;
        set;
    }
    global Id VATId {
        get;
        set;
    }
    global SalesOrderAdditionalCharge() {

    }
}
global class SalesOrderCopy {
    global Id AccountIdforNewOrder {
        get;
        set;
    }
    global Boolean ExcludeAllAdditionalCharges {
        get;
        set;
    }
    global Boolean ExcludeAllSalesOrderLines {
        get;
        set;
    }
    global Boolean ExcludeAllServiceOrderLines {
        get;
        set;
    }
    global Date OrderDateforNewOrder {
        get;
        set;
    }
    global Boolean ResetAccountDefaults {
        get;
        set;
    }
    global Id SalesOrderId {
        get;
        set;
    }
    global SalesOrderCopy() {

    }
}
global class SalesOrderCopyVASResult {
    global List<gii.SalesOrderCreation.GOMException> Exceptions {
        get;
        set;
    }
    global Set<Id> serviceOrderLineIds {
        get;
        set;
    }
    global SalesOrderCopyVASResult() {

    }
}
global class SalesOrderCreationResult {
    global List<gii.SalesOrderCreation.GOMException> Exceptions {
        get;
        set;
    }
    global List<Id> NewSalesOrderIds {
        get;
        set;
    }
    global SalesOrderCreationResult() {

    }
}
global class SalesOrderHeader {
    global Id AccountId {
        get;
        set;
    }
    global String AGC {
        get;
        set;
    }
    global Id ARPaymentTerms {
        get;
        set;
    }
    global String BillingCity {
        get;
        set;
    }
    global String BillingCountry {
        get;
        set;
    }
    global String BillingName {
        get;
        set;
    }
    global String BillingStateProvince {
        get;
        set;
    }
    global String BillingStreet {
        get;
        set;
    }
    global String BillingZipPostalCode {
        get;
        set;
    }
    global Id CarrierId {
        get;
        set;
    }
    global Id ConsignmentWarehouseId {
        get;
        set;
    }
    global String CurrencyISOCode {
        get;
        set;
    }
    global Date CustomerPODate {
        get;
        set;
    }
    global String CustomerPONumber {
        get;
        set;
    }
    global String DeliveryTerms {
        get;
        set;
    }
    global String DestinationCountry {
        get;
        set;
    }
    global Decimal DiscountPercent {
        get;
        set;
    }
    global Id DivisionCodeId {
        get;
        set;
    }
    global String DocumentText {
        get;
        set;
    }
    global Boolean ExciseTaxable {
        get;
        set;
    }
    global Boolean ExcludeAllAdditionalCharges {
        get;
        set;
    }
    global Boolean ExcludeAllSalesOrderLines {
        get;
        set;
    }
    global Boolean ExcludeAllServiceOrderLines {
        get;
        set;
    }
    global Boolean FieldServiceOrder {
        get;
        set;
    }
    global String OneTimeShipCity {
        get;
        set;
    }
    global String OneTimeShipStateProvince {
        get;
        set;
    }
    global String OneTimeShipStreet {
        get;
        set;
    }
    global Boolean OneTimeShipTo {
        get;
        set;
    }
    global String OneTimeShipToCountry {
        get;
        set;
    }
    global String OneTimeShipToName {
        get;
        set;
    }
    global String OneTimeShipZipPostalCode {
        get;
        set;
    }
    global Date OrderDate {
        get;
        set;
    }
    global String OrderKey {
        get;
        set;
    }
    global String OrderType {
        get;
        set;
    }
    global Id OrderVATId {
        get;
        set;
    }
    global Boolean OrderVATTaxable {
        get;
        set;
    }
    global String PauseCode {
        get;
        set;
    }
    global String PaymentMethod {
        get;
        set;
    }
    global String PaymentTerms {
        get;
        set;
    }
    global String PriceBookName {
        get;
        set;
    }
    global Boolean PrintDocumentText {
        get;
        set;
    }
    global Id ProgramId {
        get;
        set;
    }
    global String ProgramInstructions {
        get;
        set;
    }
    global Date PromiseDate {
        get;
        set;
    }
    global Id PromotionId {
        get;
        set;
    }
    global Date RequiredDate {
        get;
        set;
    }
    global String ResaleCertificate {
        get;
        set;
    }
    global Date ResaleCertificateExpiration {
        get;
        set;
    }
    global List<gii.SalesOrderCreation.SalesOrderAdditionalCharge> SalesOrderAdditionalChargeList {
        get;
        set;
    }
    global List<gii.SalesOrderCreation.SalesOrderLine> SalesOrderLineList {
        get;
        set;
    }
    global String SalesRegion {
        get;
        set;
    }
    global Id SalesRepresentativeId {
        get;
        set;
    }
    global Boolean SalesTaxable {
        get;
        set;
    }
    global String SalesTerritory {
        get;
        set;
    }
    global Date ScheduledDate {
        get;
        set;
    }
    global List<gii.SalesOrderCreation.ServiceOrderLine> ServiceOrderLineList {
        get;
        set;
    }
    global Id ServiceTicketId {
        get;
        set;
    }
    global Boolean ShippingbyOrderValue {
        get;
        set;
    }
    global Boolean ShippingbyWeight {
        get;
        set;
    }
    global String ShipToCity {
        get;
        set;
    }
    global String ShipToCountry {
        get;
        set;
    }
    global String ShipToName {
        get;
        set;
    }
    global String ShipToStateProvince {
        get;
        set;
    }
    global String ShipToStreet {
        get;
        set;
    }
    global String ShipToZipPostalCode {
        get;
        set;
    }
    global Id TaxLocationId {
        get;
        set;
    }
    global Id TransferToWarehouseId {
        get;
        set;
    }
    global Boolean UsageTaxable {
        get;
        set;
    }
    global String VATRegistration {
        get;
        set;
    }
    global String VATRoundingMethod {
        get;
        set;
    }
    global Boolean VATTaxable {
        get;
        set;
    }
    global Id WarehouseId {
        get;
        set;
    }
    global SalesOrderHeader() {

    }
}
global class SalesOrderLine {
    global String AGC {
        get;
        set;
    }
    global Id CarrierId {
        get;
        set;
    }
    global Id CatalogId {
        get;
        set;
    }
    global String CatalogName {
        get;
        set;
    }
    global Id ColorId {
        get;
        set;
    }
    global Date CustomerPODate {
        get;
        set;
    }
    global String CustomerPOLine {
        get;
        set;
    }
    global String CustomerPONumber {
        get;
        set;
    }
    global Decimal DiscountPercent {
        get;
        set;
    }
    global Boolean ExciseTaxable {
        get;
        set;
    }
    global Id InvoiceScheduleTemplateId {
        get;
        set;
    }
    global Boolean Kit {
        get;
        set;
    }
    global Boolean KitProductRequired {
        get;
        set;
    }
    global String LineDescription {
        get;
        set;
    }
    global Decimal MaterialCost {
        get;
        set;
    }
    global String NoChargeReason {
        get;
        set;
    }
    global Boolean NonStock {
        get;
        set;
    }
    global Decimal NoOfRevSchdInstallment {
        get;
        set;
    }
    global String OneTimeShipCity {
        get;
        set;
    }
    global String OneTimeShipStateProvince {
        get;
        set;
    }
    global String OneTimeShipStreet {
        get;
        set;
    }
    global Boolean OneTimeShipTo {
        get;
        set;
    }
    global String OneTimeShipToName {
        get;
        set;
    }
    global String OneTimeShipZipPostalCode {
        get;
        set;
    }
    global String OrderKey {
        get;
        set;
    }
    global Decimal OrderQuantity {
        get;
        set;
    }
    global Id OriginalProductId {
        get;
        set;
    }
    global Decimal OriginalUnitPrice {
        get;
        set;
    }
    global String PricingOverriddenReason {
        get;
        set;
    }
    global Id ProductId {
        get;
        set;
    }
    global Date PromiseDate {
        get;
        set;
    }
    global Boolean QuantityChangeAllowed {
        get;
        set;
    }
    global Date RequiredDate {
        get;
        set;
    }
    global Decimal ReservedQuantity {
        get;
        set;
    }
    global String RevSchdInstallmentPeriod {
        get;
        set;
    }
    global String SalesOrderLineKey {
        get;
        set;
    }
    global Id SalesOrderServiceTicketLineId {
        get;
        set;
    }
    global Boolean SalesTaxable {
        get;
        set;
    }
    global Date ScheduledDate {
        get;
        set;
    }
    global Id SellingUnitofMeasureId {
        get;
        set;
    }
    global Id ServiceTicketId {
        get;
        set;
    }
    global Id ServiceTicketLineId {
        get;
        set;
    }
    global Id ServiceTicketProductId {
        get;
        set;
    }
    global Id SiteId {
        get;
        set;
    }
    global Id SizeId {
        get;
        set;
    }
    global String StockUM {
        get;
        set;
    }
    global Decimal TaxAmount {
        get;
        set;
    }
    global Id TaxLocationId {
        get;
        set;
    }
    global Decimal TierPricingQuantity {
        get;
        set;
    }
    global Decimal UnitAmountOff {
        get;
        set;
    }
    global Decimal UnitPrice {
        get;
        set;
    }
    global Decimal UnitWeight {
        get;
        set;
    }
    global Boolean UpdateAsset {
        get;
        set;
    }
    global Boolean UsageTaxable {
        get;
        set;
    }
    global Decimal VATAmount {
        get;
        set;
    }
    global Id VATId {
        get;
        set;
    }
    global String VATRegistration {
        get;
        set;
    }
    global Boolean VATTaxable {
        get;
        set;
    }
    global Id WarehouseId {
        get;
        set;
    }
    global SalesOrderLine() {

    }
}
global class ServiceOrderLine {
    global String AGC {
        get;
        set;
    }
    global Id ContractId {
        get;
        set;
    }
    global Date CustomerPODate {
        get;
        set;
    }
    global String CustomerPOLine {
        get;
        set;
    }
    global String CustomerPONumber {
        get;
        set;
    }
    global Decimal DiscountPercent {
        get;
        set;
    }
    global Boolean ExciseTaxable {
        get;
        set;
    }
    global Id InvoiceScheduleTemplateId {
        get;
        set;
    }
    global Boolean Kit {
        get;
        set;
    }
    global String LineDescription {
        get;
        set;
    }
    global String NoChargeReason {
        get;
        set;
    }
    global Decimal NoOfRevSchdInstallment {
        get;
        set;
    }
    global String OrderKey {
        get;
        set;
    }
    global Decimal OrderQuantity {
        get;
        set;
    }
    global Id OriginalProductId {
        get;
        set;
    }
    global Decimal PercentageOfLineUnitPrice {
        get;
        set;
    }
    global Id ProductId {
        get;
        set;
    }
    global Boolean QuantityChangeAllowed {
        get;
        set;
    }
    global String RevSchdInstallmentPeriod {
        get;
        set;
    }
    global String SalesOrderLineKey {
        get;
        set;
    }
    global Id SalesOrderServiceTicketLineId {
        get;
        set;
    }
    global Boolean SalesTaxable {
        get;
        set;
    }
    global String ServiceChargeType {
        get;
        set;
    }
    global Date ServiceCompletionDate {
        get;
        set;
    }
    global Date ServiceEndDate {
        get;
        set;
    }
    global String ServiceOrderLineKey {
        get;
        set;
    }
    global Boolean ServiceOrderLineVAS1Exists {
        get;
        set;
    }
    global List<gii.SalesOrderCreation.ServiceOrderLineVAS1> ServiceOrderLineVAS1List {
        get;
        set;
    }
    global Boolean ServiceOrderLineVAS2Exists {
        get;
        set;
    }
    global List<gii.SalesOrderCreation.ServiceOrderLineVAS2> ServiceOrderLineVAS2List {
        get;
        set;
    }
    global Boolean ServiceOrderLineVAS3Exists {
        get;
        set;
    }
    global List<gii.SalesOrderCreation.ServiceOrderLineVAS3> ServiceOrderLineVAS3List {
        get;
        set;
    }
    global Date ServiceStartDate {
        get;
        set;
    }
    global Id ServiceTicketId {
        get;
        set;
    }
    global Id ServiceTicketLineId {
        get;
        set;
    }
    global Id ServiceticketServiceId {
        get;
        set;
    }
    global Id ServiceTypeId {
        get;
        set;
    }
    global Decimal TaxAmount {
        get;
        set;
    }
    global Id TaxLocationId {
        get;
        set;
    }
    global Decimal UnitCost {
        get;
        set;
    }
    global String UnitofMeasure {
        get;
        set;
    }
    global Decimal UnitPrice {
        get;
        set;
    }
    global Boolean UsageTaxable {
        get;
        set;
    }
    global Boolean ValueAddedService {
        get;
        set;
    }
    global Decimal VATAmount {
        get;
        set;
    }
    global Id VATId {
        get;
        set;
    }
    global String VATRegistration {
        get;
        set;
    }
    global Boolean VATTaxable {
        get;
        set;
    }
    global Id WarehouseId {
        get;
        set;
    }
    global ServiceOrderLine() {

    }
}
global class ServiceOrderLineVAS1 {
    global String Description {
        get;
        set;
    }
    global Decimal DiscountPercent {
        get;
        set;
    }
    global Decimal FixedPrice {
        get;
        set;
    }
    global Decimal FlatFee {
        get;
        set;
    }
    global Boolean Free {
        get;
        set;
    }
    global String Instructions {
        get;
        set;
    }
    global Id PromotionId {
        get;
        set;
    }
    global Decimal Quantity {
        get;
        set;
    }
    global String SalesOrderLineId {
        get;
        set;
    }
    global String ServiceOrderLineId {
        get;
        set;
    }
    global Decimal UnitPrice {
        get;
        set;
    }
    global Id VAS1Id {
        get;
        set;
    }
    global String VAS1Key {
        get;
        set;
    }
    global ServiceOrderLineVAS1() {

    }
}
global class ServiceOrderLineVAS2 {
    global String Description {
        get;
        set;
    }
    global Decimal DiscountPercent {
        get;
        set;
    }
    global Decimal FixedPrice {
        get;
        set;
    }
    global Decimal FlatFee {
        get;
        set;
    }
    global Boolean Free {
        get;
        set;
    }
    global String Instructions {
        get;
        set;
    }
    global Id PromotionId {
        get;
        set;
    }
    global Decimal Quantity {
        get;
        set;
    }
    global String SalesOrderLineId {
        get;
        set;
    }
    global String ServiceOrderLineId {
        get;
        set;
    }
    global Decimal UnitPrice {
        get;
        set;
    }
    global Id VAS2Id {
        get;
        set;
    }
    global String VAS2Key {
        get;
        set;
    }
    global ServiceOrderLineVAS2() {

    }
}
global class ServiceOrderLineVAS3 {
    global String Description {
        get;
        set;
    }
    global Decimal DiscountPercent {
        get;
        set;
    }
    global Decimal FixedPrice {
        get;
        set;
    }
    global Decimal FlatFee {
        get;
        set;
    }
    global Boolean Free {
        get;
        set;
    }
    global String Instructions {
        get;
        set;
    }
    global Id PromotionId {
        get;
        set;
    }
    global Decimal Quantity {
        get;
        set;
    }
    global String SalesOrderLineId {
        get;
        set;
    }
    global String ServiceOrderLineId {
        get;
        set;
    }
    global Decimal UnitPrice {
        get;
        set;
    }
    global Id VAS3Id {
        get;
        set;
    }
    global String VAS3Key {
        get;
        set;
    }
    global ServiceOrderLineVAS3() {

    }
}
}
