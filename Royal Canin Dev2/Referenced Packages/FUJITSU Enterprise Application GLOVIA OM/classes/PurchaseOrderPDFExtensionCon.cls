/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class PurchaseOrderPDFExtensionCon {
    global PurchaseOrderPDFExtensionCon(ApexPages.StandardController c) {

    }
    global System.PageReference attachPurchaseOrder() {
        return null;
    }
    global String getCurrentDateTimeinUserLocale() {
        return null;
    }
    global String getLogoId() {
        return null;
    }
    global Organization getOrg() {
        return null;
    }
    global gii__PurchaseOrder__c getOrgAddress() {
        return null;
    }
    global gii__PurchaseOrder__c getPO() {
        return null;
    }
    global Boolean getPOLinesExists() {
        return null;
    }
    global List<gii__PurchaseOrderLine__c> getPOLines() {
        return null;
    }
}
