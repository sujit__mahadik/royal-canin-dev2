/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class MassUpdateInvoiceAddChgHelper {
    global MassUpdateInvoiceAddChgHelper() {

    }
    global static List<gii__InvoiceAdditionalCharge__c> getInvoiceAddChgForUpdate() {
        return null;
    }
    global static void rememberInvoiceAddChgforUpdate(List<gii__InvoiceAdditionalCharge__c> soACs) {

    }
}
