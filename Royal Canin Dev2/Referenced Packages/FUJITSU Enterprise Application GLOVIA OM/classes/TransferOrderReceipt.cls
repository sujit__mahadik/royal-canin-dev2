/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class TransferOrderReceipt {
    global TransferOrderReceipt() {

    }
    global static gii.TransferOrderReceipt.TransferOrderReceiptsResult cancelReceipts(Set<Id> ReceiptRecordIds) {
        return null;
    }
    global static gii.TransferOrderReceipt.TransferOrderReceiptsResult getReceiptsforReceiptQueues(gii.TransferOrderReceipt.ReceiptCreationInput inputObj) {
        return null;
    }
global class GOMException extends Exception {
}
global class ReceiptCreationInput {
    global Map<Id,Id> mapReceiptQueueLocation {
        get;
        set;
    }
    global Map<Id,Id> mapReceiptQueueLocationBin {
        get;
        set;
    }
    global Date ReceiptDate {
        get;
        set;
    }
    global ReceiptCreationInput() {

    }
}
global class TransferOrderReceiptsResult {
    global List<gii.TransferOrderReceipt.GOMException> Exceptions {
        get;
        set;
    }
    global List<gii__Receipt__c> listReceipt {
        get;
        set;
    }
    global TransferOrderReceiptsResult() {

    }
}
}
