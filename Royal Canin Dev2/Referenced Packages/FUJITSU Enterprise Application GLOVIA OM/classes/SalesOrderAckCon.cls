/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class SalesOrderAckCon {
    global SalesOrderAckCon() {

    }
    global System.PageReference attachSOAck() {
        return null;
    }
    global String getCurrentDateTimeinUserLocale() {
        return null;
    }
    global String getLogoId() {
        return null;
    }
    global Integer getOrderDetailcount() {
        return null;
    }
    global List<gii__OrderTaxRate__c> getOrderTaxRate() {
        return null;
    }
    global List<gii__VATRate__c> getOrderVATRate() {
        return null;
    }
    global Organization getOrg() {
        return null;
    }
    global gii__SalesOrder__c getOrgAddress() {
        return null;
    }
    global gii__SalesOrder__c getSalesOrder() {
        return null;
    }
    global List<gii__SalesOrderAdditionalCharge__c> getSalesOrderAdditionalCharge() {
        return null;
    }
    global List<gii__SalesOrderLine__c> getSalesOrderLines() {
        return null;
    }
    global Integer getServiceDetailcount() {
        return null;
    }
    global List<gii__ServiceOrderLine__c> getServiceOrderLines() {
        return null;
    }
    global Account getaccountAddress() {
        return null;
    }
}
