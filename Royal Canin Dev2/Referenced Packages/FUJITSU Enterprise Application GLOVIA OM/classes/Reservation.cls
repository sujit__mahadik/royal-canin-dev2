/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class Reservation {
    global Reservation() {

    }
    global static void ForSalesOrderLine(Map<Id,gii__SalesOrderLine__c> soLMap, Set<Id> salesOrderId, Set<Id> salesOrderLineId, String calledFrom, Double alreadyReservedQuantity) {

    }
    global static Boolean InventoryReserveCancel(Id SoLineId) {
        return null;
    }
}
