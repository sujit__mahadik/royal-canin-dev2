/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class Reserve2 {
    global Reserve2() {

    }
    global static void Reservations(Map<Id,Double> SalesOrderLineQuantityMap, Boolean NewSOLine, String Action) {

    }
}
