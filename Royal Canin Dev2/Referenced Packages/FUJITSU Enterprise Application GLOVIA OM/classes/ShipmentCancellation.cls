/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class ShipmentCancellation {
    global ShipmentCancellation() {

    }
    global static gii.ShipmentCancellation.CancellationResult cancelShipment(Set<Id> ShipmentRecordIds) {
        return null;
    }
global class CancellationResult {
    global List<gii.ShipmentCancellation.GOMException> Exceptions {
        get;
        set;
    }
    global Set<Id> setShipmentRecordIds {
        get;
        set;
    }
    global CancellationResult() {

    }
}
global class GOMException extends Exception {
}
}
