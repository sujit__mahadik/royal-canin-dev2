/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class RMALineLotCon {
    global RMALineLotCon() {

    }
    global System.PageReference addLot() {
        return null;
    }
    global Double getAlreadyAssignedQty() {
        return null;
    }
    global Boolean getLotcontrolled() {
        return null;
    }
    global gii__RMADetail__c getProductDetails() {
        return null;
    }
    global List<gii__LotDetail__c> getlotDetail() {
        return null;
    }
    global System.PageReference reset() {
        return null;
    }
    global System.PageReference save() {
        return null;
    }
    global void setQtytoBeAssigned(Double qty) {

    }
    global void setlotDetail(List<gii__LotDetail__c> lotDetails) {

    }
}
