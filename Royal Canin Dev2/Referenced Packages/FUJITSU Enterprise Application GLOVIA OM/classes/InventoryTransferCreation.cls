/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class InventoryTransferCreation {
    global InventoryTransferCreation() {

    }
    global static gii.InventoryTransferCreation.InventoryTransferResult createTransferInventory(gii.InventoryTransferCreation.InventoryTransferObj InventoryTransferObj) {
        return null;
    }
global class GOMException extends Exception {
}
global class InventoryTransferObj {
    global List<gii__InventoryTransfer__c> listInventoryTransfer {
        get;
        set;
    }
    global InventoryTransferObj() {

    }
}
global class InventoryTransferResult {
    global List<gii.InventoryTransferCreation.GOMException> Exceptions {
        get;
        set;
    }
    global List<gii__InventoryTransfer__c> listInventoryTransfer {
        get;
        set;
    }
    global InventoryTransferResult() {

    }
}
}
