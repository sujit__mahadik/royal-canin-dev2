/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class MassUpdateARCreditAddChgHelper {
    global MassUpdateARCreditAddChgHelper() {

    }
    global static List<gii__ARCreditAdditionalCharge__c> getARCreditAddChgForUpdate() {
        return null;
    }
    global static void rememberARCreditAddChgforUpdate(List<gii__ARCreditAdditionalCharge__c> ACs) {

    }
}
