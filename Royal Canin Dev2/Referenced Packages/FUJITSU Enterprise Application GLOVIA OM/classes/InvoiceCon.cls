/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class InvoiceCon {
    global InvoiceCon() {

    }
    global System.PageReference attachInvoice() {
        return null;
    }
    global String getCurrentDateTimeinUserLocale() {
        return null;
    }
    global List<gii__InvoiceAdditionalCharge__c> getInvoiceAdditionalCharge() {
        return null;
    }
    global Integer getInvoiceAdditionalChargecount() {
        return null;
    }
    global gii__OrderInvoice__c getInvoiceAddress() {
        return null;
    }
    global String getLogoId() {
        return null;
    }
    global Integer getOrderDetailcount() {
        return null;
    }
    global gii__OrderInvoice__c getOrderInvoice() {
        return null;
    }
    global List<gii__OrderInvoiceDetail__c> getOrderInvoiceDetail() {
        return null;
    }
    global List<gii__VATRate__c> getOrderVATRate() {
        return null;
    }
    global Organization getOrg() {
        return null;
    }
    global gii__OrderInvoice__c getOrgAddress() {
        return null;
    }
    global Integer getServiceDetailcount() {
        return null;
    }
    global List<gii__OrderInvoiceDetail__c> getServiceOrderInvoiceDetail() {
        return null;
    }
    global Account getaccountAddress() {
        return null;
    }
}
