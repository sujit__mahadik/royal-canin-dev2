/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class InventoryAdjustmentforLocation {
    global InventoryAdjustmentforLocation() {

    }
    global static gii.InventoryAdjustmentforLocation.InventoryAdjustmentsResult CreateInventoryAdustments(gii.InventoryAdjustmentforLocation.ProductInventoryQtyDetails inputProductInventoryQtyDetails) {
        return null;
    }
global class GOMException extends Exception {
}
global class InventoryAdjustmentsResult {
    global List<gii.InventoryAdjustmentforLocation.GOMException> Exceptions {
        get;
        set;
    }
    global List<gii__InventoryAdjustment__c> listInventoryAdjustment {
        get;
        set;
    }
    global List<gii__ProductInventoryQuantityDetail__c> listProductInventoryQuantityDetails {
        get;
        set;
    }
    global InventoryAdjustmentsResult() {

    }
}
global class ProductInventoryQtyDetails {
    global Date AdjustmentDate {
        get;
        set;
    }
    global String AdjustmentReason {
        get;
        set;
    }
    global List<gii__ProductInventoryQuantityDetail__c> listProductInventoryQuantityDetails {
        get;
        set;
    }
    global ProductInventoryQtyDetails() {

    }
}
}
