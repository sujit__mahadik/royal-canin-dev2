/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class AddProductPriceCon {
    global Boolean checkAll {
        get;
        set;
    }
    global List<gii__PriceBook__c> data {
        get;
        set;
    }
    global Boolean hasNext {
        get;
    }
    global Boolean hasPrevious {
        get;
    }
    global ApexPages.StandardSetController pgr {
        get;
        set;
    }
    global String recordsPerPage {
        get;
        set;
    }
    global String searchText {
        get;
        set;
    }
    global AddProductPriceCon(ApexPages.StandardController controller) {

    }
    global List<gii__PriceBookEntry__c> getPriceBookEntry() {
        return null;
    }
    global List<gii.AddProductPriceCon.priceBookWrapper> getPriceBooks() {
        return null;
    }
    global gii__PriceBook__c getStandardPriceBook() {
        return null;
    }
    global String getStep() {
        return null;
    }
    global String getTwentFivePlus() {
        return null;
    }
    global System.PageReference nextPage() {
        return null;
    }
    global System.PageReference prevPage() {
        return null;
    }
    global System.PageReference returnToPrev() {
        return null;
    }
    global System.PageReference saveAndMore() {
        return null;
    }
    global System.PageReference savePBE() {
        return null;
    }
    global System.PageReference search() {
        return null;
    }
    global System.PageReference selectPriceBooks() {
        return null;
    }
    global void setAllPriceBookChecked(List<gii.AddProductPriceCon.priceBookWrapper> pb) {

    }
    global void setrecordsPerPage(String s) {

    }
    global void setsearchText(String s) {

    }
global class priceBookWrapper {
    global Boolean AlreadyProcessed {
        get;
        set;
    }
    global Boolean checked {
        get;
        set;
    }
    global gii__PriceBook__c pb {
        get;
        set;
    }
    global priceBookWrapper() {

    }
    global priceBookWrapper(gii__PriceBook__c c) {

    }
}
}
