/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class PacklistQueue {
    global PacklistQueue() {

    }
    global static void buildPacklistQueue() {

    }
    global static void createPacklistQueue(List<gii__InventoryReserve__c> invRes) {

    }
    global static void deletePacklistQueue(Set<Id> invRes) {

    }
}
