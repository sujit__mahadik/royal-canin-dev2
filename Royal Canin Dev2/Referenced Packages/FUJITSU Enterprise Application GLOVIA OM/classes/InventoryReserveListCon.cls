/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class InventoryReserveListCon {
    global List<gii__InventoryReserve__c> selectedIVs {
        get;
        set;
    }
    global ApexPages.StandardSetController setCon;
    global InventoryReserveListCon(ApexPages.StandardSetController controller) {

    }
    global System.PageReference SFDCreturnToPrevious() {
        return null;
    }
    global void clearBackorder() {

    }
    global void createPacklistBySO() {

    }
    global void createPicklistBySO() {

    }
    global Boolean getactionEnabled() {
        return null;
    }
    global Boolean getcontinueBtn() {
        return null;
    }
    global String getcurrentAction() {
        return null;
    }
    global System.PageReference listButtonAction() {
        return null;
    }
    global void quickInvoiceBySO() {

    }
    global void quickPacklistBySO() {

    }
    global void quickShipBySO() {

    }
    global System.PageReference returnToPrevious() {
        return null;
    }
    global void setcalledFrom(String s) {

    }
    global void setselectedIVs(List<gii__InventoryReserve__c> i) {

    }
}
