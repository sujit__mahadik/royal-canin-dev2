/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class ShowSerialsCon {
    global ShowSerialsCon() {

    }
    global System.PageReference SearchProductSerial() {
        return null;
    }
    global List<gii__ProductLot__c> getLots() {
        return null;
    }
    global String getProduct() {
        return null;
    }
    global List<gii__ProductSerial__c> getProductSerials() {
        return null;
    }
    global String getfieldId() {
        return null;
    }
    global String getsearchString() {
        return null;
    }
    global Boolean getshowLot() {
        return null;
    }
    global Boolean getshowSerial() {
        return null;
    }
    global void setsearchString(String str) {

    }
}
