/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class ServiceTicketProductAssignLot {
    global ServiceTicketProductAssignLot() {

    }
    global static gii.ServiceTicketProductAssignLot.AssignLotResult AssignLots(gii.ServiceTicketProductAssignLot.AssignLotInput inputObj) {
        return null;
    }
global class AssignLotInput {
    global String Action {
        get;
        set;
    }
    global Boolean CalledByGOMVFPage {
        get;
        set;
    }
    global Date IssueDate {
        get;
        set;
    }
    global Double IssueQty {
        get;
        set;
    }
    global List<gii__LotDetail__c> listLotDetails {
        get;
        set;
    }
    global String NoChargeReason {
        get;
        set;
    }
    global Id ProductId {
        get;
        set;
    }
    global Id ServiceTicketId {
        get;
        set;
    }
    global Id ServiceTicketLineId {
        get;
        set;
    }
    global Id TransactionId {
        get;
        set;
    }
    global Decimal UnitPrice {
        get;
        set;
    }
    global Id WarehouseId {
        get;
        set;
    }
    global AssignLotInput() {

    }
}
global class AssignLotResult {
    global List<gii.ServiceTicketProductAssignLot.GOMException> Exceptions {
        get;
        set;
    }
    global List<gii__LotDetail__c> listLotDetails {
        get;
        set;
    }
    global AssignLotResult() {

    }
}
global class GOMException extends Exception {
}
}
