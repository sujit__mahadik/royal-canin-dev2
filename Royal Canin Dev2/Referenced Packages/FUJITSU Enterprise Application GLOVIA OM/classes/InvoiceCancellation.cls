/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class InvoiceCancellation {
    global InvoiceCancellation() {

    }
    global static gii.InvoiceCancellation.CancellationResult cancelInvoices(Set<Id> InvoiceId, Date CancellationDate) {
        return null;
    }
global class CancellationResult {
    global List<gii.InvoiceCancellation.GOMException> Exceptions {
        get;
        set;
    }
    global Set<Id> InvoiceId {
        get;
        set;
    }
    global CancellationResult() {

    }
}
global class GOMException extends Exception {
}
}
