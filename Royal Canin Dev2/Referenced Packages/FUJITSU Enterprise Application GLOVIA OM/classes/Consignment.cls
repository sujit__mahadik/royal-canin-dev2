/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class Consignment {
    global Consignment() {

    }
    global static void Consumption2(List<Id> selectedRecordIds, String ShippedDateString) {

    }
    global static void Consumption(List<Id> selectedRecordIds) {

    }
    global static void ReplenishmentOrTransferReceipt2(List<Id> selectedRecordIds, String ReceiptDateString) {

    }
    global static void ReplenishmentOrTransferReceipt(List<Id> selectedRecordIds) {

    }
    global static void UpdateInTransit(Map<Id,gii__SalesOrder__c> soMap, Map<Id,gii__SalesOrderLine__c> soLnMap, List<gii__InventoryReserve__c> invRes) {

    }
}
