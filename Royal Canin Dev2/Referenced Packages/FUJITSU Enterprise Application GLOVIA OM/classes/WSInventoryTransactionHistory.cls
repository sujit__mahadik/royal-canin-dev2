/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class WSInventoryTransactionHistory {
    global WSInventoryTransactionHistory() {

    }
    webService static List<SObject> getInventoryTransactionHistory(String transType, String TransactionDate) {
        return null;
    }
    webService static void setInvTransReceivedDateTime(List<String> ivhisIds, String ReceivedDateTime) {

    }
}
