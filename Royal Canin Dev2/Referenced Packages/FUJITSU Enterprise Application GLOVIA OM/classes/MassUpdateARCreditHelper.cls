/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class MassUpdateARCreditHelper {
    global MassUpdateARCreditHelper() {

    }
    global static List<gii__ARCredit__c> getARCreditForUpdate() {
        return null;
    }
    global static void rememberARCreditforUpdate(List<gii__ARCredit__c> ARCredits) {

    }
}
