/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class DetailButtonActionCon {
    global DetailButtonActionCon() {

    }
    global Integer getServiceScheduleCount() {
        return null;
    }
    global Boolean getactionEnabled() {
        return null;
    }
    global Boolean getcontinueBtn() {
        return null;
    }
    global String getcurrentAction() {
        return null;
    }
    global Integer getnonServiceScheduleCount() {
        return null;
    }
    global System.PageReference orderManagementAction() {
        return null;
    }
    global System.PageReference returnToPrevious() {
        return null;
    }
}
