/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class SOLineAddlMaintCon {
    global SOLineAddlMaintCon(ApexPages.StandardController controller) {

    }
    global System.PageReference UpdateSOLine() {
        return null;
    }
    global Boolean getAllowCancel() {
        return null;
    }
    global Boolean getAllowQtyChange() {
        return null;
    }
    global Boolean getAllowWarehouseChange() {
        return null;
    }
    global Double getAlreadyUsedQty() {
        return null;
    }
    global Boolean getFulfillmentStarted() {
        return null;
    }
    global List<gii__InventoryReserve__c> getInventoryReserves() {
        return null;
    }
    global gii__SalesOrderLine__c getSOL() {
        return null;
    }
    global gii__SalesOrderLine__c getSalesOrderLine() {
        return null;
    }
    global Boolean getSerialAssigned() {
        return null;
    }
    global List<gii__InventoryReserve__c> getrebuildIRList() {
        return null;
    }
    global Boolean getshowError() {
        return null;
    }
}
