/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class Substitute {
    global Substitute() {

    }
    global Substitute(ApexPages.StandardController controller) {

    }
    global System.PageReference AddKitMemebersave() {
        return null;
    }
    global System.PageReference CrossSellsave() {
        return null;
    }
    global System.PageReference UpSellsave() {
        return null;
    }
    global System.PageReference cancel() {
        return null;
    }
    global List<Product2> getCrossSell() {
        return null;
    }
    global String getErrMessage() {
        return null;
    }
    global String getErrMessage1() {
        return null;
    }
    global String getErrMessage2() {
        return null;
    }
    global String getErrMessage3() {
        return null;
    }
    global List<gii__Kit__c> getKitMember() {
        return null;
    }
    global String getName() {
        return null;
    }
    global String getOrderType() {
        return null;
    }
    global List<Product2> getSubstitute() {
        return null;
    }
    global List<Product2> getUpSell() {
        return null;
    }
    global Boolean getcheckboxValue() {
        return null;
    }
    global Double getquantity() {
        return null;
    }
    global List<gii__SalesOrderLine__c> getsoLine() {
        return null;
    }
    global List<gii__SalesQuoteLine__c> getsqLine() {
        return null;
    }
    global String gettheMainTabPanel() {
        return null;
    }
    global System.PageReference save() {
        return null;
    }
    global void setKit(List<gii__Kit__c> kk) {

    }
    global void setLineProductId(Id ii) {

    }
    global void setOrderType(String ot) {

    }
    global void setcheckboxValue(Boolean checkboxValue) {

    }
    global void setquantity(Double quantity) {

    }
    global void setsoline(List<gii__SalesOrderLine__c> sll) {

    }
    global void setsqline(List<gii__SalesQuoteLine__c> sql) {

    }
    global void settheMainTabPanel(String s) {

    }
}
