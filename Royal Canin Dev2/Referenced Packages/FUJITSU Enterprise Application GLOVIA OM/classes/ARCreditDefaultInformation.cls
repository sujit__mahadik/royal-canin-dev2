/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class ARCreditDefaultInformation {
    global ARCreditDefaultInformation() {

    }
    global static List<gii__ARCreditSalesLine__c> assignToARCreditSalesLine(Map<Id,gii__ARCredit__c> arCredits, List<gii__ARCreditSalesLine__c> arCMSalesLine, Map<Id,gii__Product2Add__c> prod, List<gii__TaxRate__c> TaxRate, List<gii__VATRate__c> VATRate, List<gii__SystemPolicy__c> sys, Boolean triggerIsInsert) {
        return null;
    }
    global static List<gii__ARCreditServiceLine__c> assignToARCreditServiceLine(Map<Id,gii__ARCredit__c> arCredits, List<gii__ARCreditServiceLine__c> arCMServiceLine, Map<Id,gii__Product2Add__c> prod, List<gii__TaxRate__c> TaxRate, List<gii__VATRate__c> VATRate, List<gii__SystemPolicy__c> sys, Boolean triggerIsInsert) {
        return null;
    }
    global static List<gii__ARCredit__c> assignToARCredit(Map<Id,gii__AccountAdd__c> account, Map<Id,Account> act, List<gii__ARCredit__c> arCMHeader) {
        return null;
    }
}
