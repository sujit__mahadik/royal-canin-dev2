/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class BOLCon {
    global BOLCon() {

    }
    global System.PageReference attachBillofLading() {
        return null;
    }
    global List<gii__BillofLadingDetail__c> getBOLDetails() {
        return null;
    }
    global gii__BillofLading__c getBillofLading() {
        return null;
    }
    global String getLogoId() {
        return null;
    }
    global Organization getOrg() {
        return null;
    }
    global gii__SalesOrder__c getOrgAddress() {
        return null;
    }
    global Account getShipAddress() {
        return null;
    }
}
