/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class PacklistListCon {
    global ApexPages.StandardSetController setCon;
    global PacklistListCon(ApexPages.StandardSetController controller) {

    }
    global void attachPacklist() {

    }
    global void confirmPacklist() {

    }
    global void confirmShip() {

    }
    global Boolean getactionEnabled() {
        return null;
    }
    global Boolean getcontinueBtn() {
        return null;
    }
    global String getcurrentAction() {
        return null;
    }
    global String getdocumentPageURL() {
        return null;
    }
    global String getreturnURL() {
        return null;
    }
    global Boolean getshowdoc() {
        return null;
    }
    global System.PageReference listButtonAction() {
        return null;
    }
    global System.PageReference returnToPrevious() {
        return null;
    }
    global void setreturnURL(String r) {

    }
    global void setselectedPacklists(List<gii__PackList__c> p) {

    }
}
