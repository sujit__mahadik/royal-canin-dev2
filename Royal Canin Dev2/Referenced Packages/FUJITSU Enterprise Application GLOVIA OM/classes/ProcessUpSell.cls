/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class ProcessUpSell {
    global ProcessUpSell() {

    }
    global static void replaceSQLine(Id sqLineId, Id substituteProductId) {

    }
    global static void replaceSQServiceLine(Id sqLineId, Id substituteProductId) {

    }
    global static void replaceServiceOrderLine(Id soLineId, Id substituteProductId) {

    }
    global static void replaceandReserve(Id soLineId, Id substituteProductId) {

    }
}
