/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class PacklistCon {
    global PacklistCon() {

    }
    global PacklistCon(ApexPages.StandardController c) {

    }
    global System.PageReference Lot() {
        return null;
    }
    global System.PageReference add() {
        return null;
    }
    global System.PageReference addLot() {
        return null;
    }
    global System.PageReference attachPackList() {
        return null;
    }
    global System.PageReference cancel() {
        return null;
    }
    global Double getAlreadyAssignedQty() {
        return null;
    }
    global String getCurrentDateTimeinUserLocale() {
        return null;
    }
    global String getLogoId() {
        return null;
    }
    global Organization getOrg() {
        return null;
    }
    global gii__SalesOrder__c getOrgAddress() {
        return null;
    }
    global gii__PackListDetail__c getPacklistDetail() {
        return null;
    }
    global Boolean getPrintLotOnPacklist() {
        return null;
    }
    global Boolean getPrintSerial() {
        return null;
    }
    global Boolean getPrintSerialOnPacklist() {
        return null;
    }
    global Double getProductSerialCount() {
        return null;
    }
    global Account getShipAddress() {
        return null;
    }
    global List<gii__LotDetail__c> getlotDetail() {
        return null;
    }
    global String getpackDetTitle() {
        return null;
    }
    global gii__PackList__c getpacklist() {
        return null;
    }
    global List<gii__PackListDetail__c> getpacklists() {
        return null;
    }
    global List<gii__ProductSerial__c> getproductSerial() {
        return null;
    }
    global List<gii.PacklistCon.cProductLots> getretProdLot() {
        return null;
    }
    global List<gii.PacklistCon.cProductSerials> getretProdSerial() {
        return null;
    }
    global Boolean getshowLot() {
        return null;
    }
    global Boolean getshowSerial() {
        return null;
    }
    global System.PageReference reset() {
        return null;
    }
    global System.PageReference save() {
        return null;
    }
    global System.PageReference saveLot() {
        return null;
    }
    global System.PageReference saveProductSerial() {
        return null;
    }
    global void setPacklistDetail(gii__PackListDetail__c p) {

    }
    global void setQtytoBeAssigned(Double qty) {

    }
    global void setlotDetail(List<gii__LotDetail__c> lotDetails) {

    }
    global void setproductSerial(List<gii__ProductSerial__c> productSerials) {

    }
    global void settotalSerials(Long t) {

    }
global class cProductLots {
    global String Line {
        get;
        set;
    }
    global List<gii__LotDetail__c> LotList {
        get;
        set;
    }
}
global class cProductSerials {
    global String Line {
        get;
        set;
    }
    global List<gii__ProductSerial__c> serialList {
        get;
        set;
    }
}
}
