<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>SetLotRestrictedtoFalse</fullName>
        <description>Set Lot Restricted to False</description>
        <field>LotRestricted__c</field>
        <literalValue>0</literalValue>
        <name>Set Lot Restricted to False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SetLotRestrictedtoTrue</fullName>
        <description>Set Lot Restricted to True</description>
        <field>LotRestricted__c</field>
        <literalValue>1</literalValue>
        <name>Set Lot Restricted to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>LotRestrictedisNotSetinProductLot</fullName>
        <actions>
            <name>SetLotRestrictedtoFalse</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set Lot Restricted from Product Lot for new Product Inventory Quantity Detail records when restricted flag is not set in Product Lot</description>
        <formula>AND(ProductLot__c &lt;&gt; null,  NOT( ProductLot__r.Restricted__c))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>LotRestrictedisSetinProductLot</fullName>
        <actions>
            <name>SetLotRestrictedtoTrue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set Lot Restricted from Product Lot for new Product Inventory Quantity Detail records when restricted flag is set in product lot</description>
        <formula>AND(ProductLot__c &lt;&gt; null,  ProductLot__r.Restricted__c)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
