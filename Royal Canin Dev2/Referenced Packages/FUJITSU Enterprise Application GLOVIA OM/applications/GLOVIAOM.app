<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <brand>
        <headerColor>#1589EE</headerColor>
        <logo>FJ</logo>
        <logoVersion>1</logoVersion>
    </brand>
    <description>GLOVIA Order Management</description>
    <formFactors>Large</formFactors>
    <label>GLOVIA OM</label>
    <navType>Standard</navType>
    <tab>standard-home</tab>
    <tab>standard-Account</tab>
    <tab>standard-Opportunity</tab>
    <tab>SalesQuote__c</tab>
    <tab>SalesOrder__c</tab>
    <tab>standard-Case</tab>
    <tab>RMA__c</tab>
    <tab>standard-Product2</tab>
    <tab>Product2Add__c</tab>
    <tab>ProductInventory__c</tab>
    <tab>Reorder_Point_Planning</tab>
    <tab>MTO_PTO</tab>
    <tab>WorkOrder__c</tab>
    <tab>PurchaseRequisition__c</tab>
    <tab>RequestforQuote__c</tab>
    <tab>SupplierQuote__c</tab>
    <tab>PurchaseOrder__c</tab>
    <tab>PurchaseOrderReceipt__c</tab>
    <tab>APInvoice__c</tab>
    <tab>ReturntoSupplier__c</tab>
    <tab>ServiceContract__c</tab>
    <tab>ServiceTicket__c</tab>
    <tab>standard-report</tab>
    <tab>standard-Dashboard</tab>
    <tab>CodeMaintenance</tab>
    <uiType>Lightning</uiType>
    <utilityBar>GLOVIA_OM_UtilityBar</utilityBar>
</CustomApplication>
