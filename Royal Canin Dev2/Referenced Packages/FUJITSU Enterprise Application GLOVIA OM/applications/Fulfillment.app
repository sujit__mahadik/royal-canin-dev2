<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <brand>
        <headerColor>#1589EE</headerColor>
        <logo>FJ_GOM_21</logo>
        <logoVersion>1</logoVersion>
    </brand>
    <description>GLOVIA OM Fulfillment</description>
    <formFactors>Large</formFactors>
    <label>Fulfillment</label>
    <navType>Standard</navType>
    <tab>standard-home</tab>
    <tab>InventoryReserve__c</tab>
    <tab>PickList__c</tab>
    <tab>PacklistQueue__c</tab>
    <tab>PackList__c</tab>
    <tab>ShipmentQueue__c</tab>
    <tab>Shipment__c</tab>
    <tab>ReceiptQueue__c</tab>
    <tab>SalesOrderLine__c</tab>
    <uiType>Lightning</uiType>
    <utilityBar>Fulfillment_UtilityBar</utilityBar>
</CustomApplication>
